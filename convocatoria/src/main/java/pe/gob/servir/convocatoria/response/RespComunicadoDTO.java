package pe.gob.servir.convocatoria.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class RespComunicadoDTO {

	private Long comunicadoId;
	
	private Long convocatoriaId;
	
	private Long perfilId;
	
	private String nombrePerfil;
	
	private String codigoConvocatoria;
	
	private String etapa;
	
	private String tipoComunicado;
	
	private String gestor;

    private String coordinador;
    
    private String estado;
    
    private Long estadoId;
    
    private String codProgEstado;
    
    /*@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "pt-PE", timezone = "Peru/East")
    private Date fechaPublicacion;
    */
    private String fechaPublicacion;
}

