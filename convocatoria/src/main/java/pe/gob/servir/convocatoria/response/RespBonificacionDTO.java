package pe.gob.servir.convocatoria.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import pe.gob.servir.convocatoria.model.Bonificacion;

@Data
@SuppressWarnings("serial")
public class RespBonificacionDTO implements Serializable {
	
    private Long bonificacionId;    
    private Long tipoInfoId;
    private Long tipoBonificacion;
    private String titulo;
    private String contenido;
	List<RespBonificacionDetalleDTO> ltaDetalleBonificacion;
	
	public RespBonificacionDTO(Bonificacion item) {
		this.bonificacionId = item.getBonificacionId();
		this.tipoInfoId = item.getTipoInfoId();
		this.tipoBonificacion = item.getTipoBonificacion();
		this.titulo = item.getTitulo();
		this.contenido = item.getContenido();
	}
}
