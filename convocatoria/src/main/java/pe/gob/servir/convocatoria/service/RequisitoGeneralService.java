package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.RequisitoGeneralDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface RequisitoGeneralService {
    RespBase<RequisitoGeneralDTO> crearRequisitoGeneral(ReqBase<RequisitoGeneralDTO> request, MyJsonWebToken token);
    RespBase<RequisitoGeneralDTO> getRequisitoGeneral(Long baseId);
    RespBase<RequisitoGeneralDTO> updateRequisitoGenral(ReqBase<RequisitoGeneralDTO> request, MyJsonWebToken token);
}
