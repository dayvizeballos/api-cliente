package pe.gob.servir.convocatoria.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReqEliminarActividad {

	private Long actividadId;
	private String estado;
}
