package pe.gob.servir.convocatoria.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_crono_actividad", schema = "sch_base")
@Getter
@Setter
public class BaseCronogramaActividad extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_cronograma_act")
    @SequenceGenerator(name = "seq_base_cronograma_act", sequenceName = "seq_base_cronograma_act", schema = "sch_base", allocationSize = 1)
    @Column(name = "crono_actividad_id")
    private Long actividadId;

    @JoinColumn(name="cronograma_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BaseCronograma baseCronograma;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_ini")
    private Date fechaIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_fin")
    private Date fechaFin;

    @Column(name = "hora_ini")
    private String horaIni;

    @Column(name = "hora_fin")
    private String horaFin;

    @Column(name = "responsable")
    private String respomsable;

    @Column(name = "descripcion")
    private String descripcion;

    @JoinColumn(name = "tipo_actividad")
    @ManyToOne()
    private MaestraDetalle tipoActividad;

    public BaseCronogramaActividad () {
    	
    }
    
    public BaseCronogramaActividad (BaseCronograma baseCronograma) {
    	this.baseCronograma = baseCronograma;
    }

}
