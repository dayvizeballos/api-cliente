package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import feign.Param;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.Convocatoria;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

import java.util.Optional;

public interface ConvocatoriaRepository  extends JpaRepository<Convocatoria, Long> , JpaSpecificationExecutor<Convocatoria> {
	
	@Query("select case when max(c.correlativo) is null then 1 else (max(c.correlativo) + 1) end "
			+ "from Convocatoria c where c.regimen = :regimen and c.anio = :anio and c.entidadId = :entidadId "
			+ "and c.estadoRegistro = '1' ")
	public Long findMaxConvocatoria(@Param("regimen")MaestraDetalle regimen, @Param("anio")Long anio, @Param("entidadId")Long entidadId);
	

    Optional<Convocatoria> findByBase(Base baseId);


}
