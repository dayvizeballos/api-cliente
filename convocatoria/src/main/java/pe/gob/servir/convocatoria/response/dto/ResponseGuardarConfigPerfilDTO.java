package pe.gob.servir.convocatoria.response.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JGlobalMap
@Getter
@Setter
@SuppressWarnings("serial")
@AllArgsConstructor
public class ResponseGuardarConfigPerfilDTO implements Serializable {
	String idPerfil;
	String cuerpo;
	String mensaje;
}
