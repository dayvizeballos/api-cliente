package pe.gob.servir.convocatoria.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "tbl_conf_perf_seccion", schema = "sch_convocatoria")
@NamedQuery(name="TblConfPerfSeccion.findAll", query="SELECT t FROM TblConfPerfSeccion t")
public class TblConfPerfSeccion extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conf_perf_seccion_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_seccion")
    @SequenceGenerator(name = "seq_conf_perf_seccion", sequenceName = "seq_conf_perf_seccion", schema = "sch_convocatoria", allocationSize = 1)
    private Long confPerfSeccionId;

    @ManyToOne
    @JoinColumn(name = "perfil_id", nullable = false)
    private Perfil perfil;

    @Column(name = "tipo_seccion")
    private Long tipoSeccion;

    @Column(name = "peso_seccion")
    private Long pesoSeccion;

    @Column(name = "descripcion_seccion")
    private String descripcionSeccion;

    @Column(name = "base_id")
    private Long baseId;

}
