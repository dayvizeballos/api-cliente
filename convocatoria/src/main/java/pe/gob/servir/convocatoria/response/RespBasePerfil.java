package pe.gob.servir.convocatoria.response;

import javax.validation.Valid;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.BasePerfilDTO;

@Getter
@Setter
public class RespBasePerfil {

	@Valid
	private BasePerfilDTO basePerfil;
}
