package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.gob.servir.convocatoria.model.MaestraDetalleEntidad;

public interface MaestraDetalleEntidadRepository extends JpaRepository<MaestraDetalleEntidad, Long>{
}
