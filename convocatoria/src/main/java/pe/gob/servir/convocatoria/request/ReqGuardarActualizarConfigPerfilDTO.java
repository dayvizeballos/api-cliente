package pe.gob.servir.convocatoria.request;

import lombok.Data;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.ConfiguracionPerfilDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class ReqGuardarActualizarConfigPerfilDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = Constantes.CAMPO + " configuracionPerfilDTO " + Constantes.ES_OBLIGATORIO)
    @Valid
    private ConfiguracionPerfilDTO configuracionPerfilDTO;

}
