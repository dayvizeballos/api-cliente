package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseEvaluacionDetalleDTO {
    private Long evaluacionDetalleId;
    private Long evaluacionEntidadId; //1
    private Long informeDetalleId; //2
    private Long baseEvaluacionId; //1
    private String estado; //1
    private Long peso;
    private Long puntajeMinimo;
    private Long puntajeMaximo;
    private String descripcion;
    private Long tipoEvaluacionId;
    private String codProg;

}
