package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.CarreraFormacionAcademica;

import java.util.List;

@Repository
public interface CarreraFormacionAcademicaRepository extends JpaRepository<CarreraFormacionAcademica, Long>{

    @Query("SELECT FO FROM CarreraFormacionAcademica FO  WHERE FO.fomarcionAcademica.formacionAcademicaId = :formacionAcademicaId")
     List<CarreraFormacionAcademica> findListByFormacionAca(@Param("formacionAcademicaId")Long formacionAcademicaId);

    @Query("SELECT FO.carreraProfesional.id FROM CarreraFormacionAcademica FO  WHERE FO.carreraAcademicaId = :carreraAcademicaId")
    Long idCarreraByCarrFormAcId(@Param("carreraAcademicaId")Long carreraAcademicaId);

}
