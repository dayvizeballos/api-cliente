package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RespUploadFile {

	private String tipoAcceso;
    private String fileName;
    private String extension;
    private String pathRelative;
}
