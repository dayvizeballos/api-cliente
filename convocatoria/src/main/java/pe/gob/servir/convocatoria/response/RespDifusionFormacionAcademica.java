package pe.gob.servir.convocatoria.response;

import lombok.Data;

@Data
public class RespDifusionFormacionAcademica {
    private Long nivelEducaId;
    private String nivelEduca;
    private String estaNivelEduca;
    private String situAcadem;
    private String estadoSituaAcadem;
    private String nombreGrado;
    private String codProgrNivelEdu;
}
