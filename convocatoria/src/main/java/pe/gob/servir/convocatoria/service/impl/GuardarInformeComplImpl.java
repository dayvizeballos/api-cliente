package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.model.ComplDetalle;
import pe.gob.servir.convocatoria.model.InformeComplement;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.repository.BaseComplDetalleRepository;
import pe.gob.servir.convocatoria.repository.BaseInformeComplRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarInformacionComp;
import pe.gob.servir.convocatoria.request.dto.InformacionComplDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.GuardarInformeCompl;

@Service
public class GuardarInformeComplImpl implements GuardarInformeCompl {

    private final BaseInformeComplRepository baseInformeComplRepository;
    private final BaseComplDetalleRepository baseComplDetalleRepository;
    private final MaestraDetalleRepository maestraDetalleRepository;
    
	@Autowired
	public GuardarInformeComplImpl(BaseInformeComplRepository baseInformeComplRepository,
			BaseComplDetalleRepository baseComplDetalleRepository, MaestraDetalleRepository maestraDetalleRepository) {
		this.baseInformeComplRepository = baseInformeComplRepository;
		this.baseComplDetalleRepository = baseComplDetalleRepository;
		this.maestraDetalleRepository = maestraDetalleRepository;
	}
    

	@Override
	public void saveListInformeDetalle(List<InformacionComplDTO> listIdInformeDetalle,
			ReqBase<ReqGuardarInformacionComp> request,
            MyJsonWebToken token) {
		
		if (CollectionUtils.isNotEmpty(listIdInformeDetalle)) {
			
			InformeComplement informeComplement = new InformeComplement();
			informeComplement.setBaseId(request.getPayload().getBaseId());
			informeComplement.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			baseInformeComplRepository.save(informeComplement);

			for (InformacionComplDTO idDetalle : listIdInformeDetalle) {

				ComplDetalle complDetalle = new ComplDetalle();
				complDetalle.setInformeDetalleId(idDetalle.getIdInforme());
				complDetalle.setInformeComplement(informeComplement);
				complDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				baseComplDetalleRepository.save(complDetalle);

			}

		}
	}

	@Override
	public void actualizarInformeCompl(List<InformacionComplDTO> listIdInformeDetalle,
			List<InformeComplement> informeCompBD, List<InformacionComplDTO> listIdBonificacion,
			ReqBase<ReqGuardarInformacionComp> request,
            MyJsonWebToken token,Long idTipoInfBonificacion) {
		List<InformacionComplDTO> listIdBonificacionSimilares = new ArrayList<>();

    	//Bonificaciones
    	informeCompBD.stream().forEach(c->{
    		if(Objects.nonNull(c.getBonificacionId())) {
    			Optional<InformacionComplDTO> objDto =  listIdBonificacion.stream().filter(b -> b.getIdInforme().equals(c.getBonificacionId())).findFirst();
        		if(objDto.isPresent()) {
        			listIdBonificacionSimilares.add(objDto.get());
        		}else {
        			//eliminar
        			InformeComplement informeComplement = c;
					informeComplement.setCampoSegUpd(Constantes.INACTIVO, token.getUsuario().getUsuario(), Instant.now());
					baseInformeComplRepository.save(informeComplement);
        		}
    		}
    	});     
		
    	List<InformacionComplDTO> listIdBonificacionNoSimilares = listIdBonificacion.stream().filter(c -> !listIdBonificacionSimilares.contains(c))
		.collect(Collectors.toList());
		
		listIdBonificacionNoSimilares.stream().filter(Objects::nonNull).forEach( x -> {
			//guardar nuevas bonificaciones
			InformeComplement informeComplement = new InformeComplement();
			informeComplement.setBonificacionId(x.getIdInforme());
			informeComplement.setBaseId(request.getPayload().getBaseId());
			informeComplement.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			informeComplement.setTipoInfoId(idTipoInfBonificacion);
			baseInformeComplRepository.save(informeComplement);
		});
    	
		Optional<InformeComplement> informeObj = informeCompBD.stream()
				.filter(c -> Objects.isNull(c.getBonificacionId())).findFirst();            		
    		
    	if(informeObj.isPresent()) {
    		InformeComplement informeComplement= informeObj.get();
    		if (CollectionUtils.isNotEmpty(listIdInformeDetalle)) {
    			List<ComplDetalle> listSimilares = new ArrayList<>();
				informeComplement.getComplDetalles().stream()
						.filter(t -> StringUtils.equals(Constantes.ACTIVO, t.getEstadoRegistro())).forEach(c ->

						listIdInformeDetalle.stream().filter(t -> t.getIdInforme().equals(c.getInformeDetalleId()))
								.forEach(w -> listSimilares.add(c))

						);
    			
    			List<ComplDetalle> listNoSimilares = informeComplement.getComplDetalles().stream()
    						.filter(c -> !listSimilares.contains(c))
    						.filter(m -> StringUtils.equals(Constantes.ACTIVO, m.getEstadoRegistro()))
						    .collect(Collectors.toList());
				
				listNoSimilares.stream().filter(Objects::nonNull).forEach(c->{
					//eliminar
					ComplDetalle complDetalle = c;
					complDetalle.setCampoSegUpd(Constantes.INACTIVO, token.getUsuario().getUsuario(), Instant.now());
					baseComplDetalleRepository.save(complDetalle);
					
				});
				
				listIdInformeDetalle.stream().filter(Objects::nonNull).forEach(c -> {
					Optional<ComplDetalle> compDet = listSimilares.stream()
							.filter(d -> d.getInformeDetalleId().equals(c.getIdInforme())).findFirst();
					if (!compDet.isPresent()) {
						// grabar
						ComplDetalle complDetalle = new ComplDetalle();
						complDetalle.setInformeDetalleId(c.getIdInforme());
						complDetalle.setInformeComplement(informeObj.get());
						complDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
						baseComplDetalleRepository.save(complDetalle);
					}
				});
				
    			
    		}else {
    			//borrar
				informeComplement.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				informeComplement.setCampoSegUpd(Constantes.INACTIVO,token.getUsuario().getUsuario(), Instant.now());
				baseInformeComplRepository.save(informeComplement);
				
				informeComplement.getComplDetalles().stream().forEach(m->{
					ComplDetalle complDetalle = m;
					complDetalle.setCampoSegUpd(Constantes.INACTIVO, token.getUsuario().getUsuario(), Instant.now());
					baseComplDetalleRepository.save(complDetalle);
				});
				
    		}
    	}else {
    		//Volver a registrar
    		saveListInformeDetalle(listIdInformeDetalle, request, token);
    	}
	}
	
	@Override
	public Map<String, Long> getMaeDetalleByCode(String codeMaeDet) {
		List<MaestraDetalle> listaMaeDetalle = maestraDetalleRepository.findDetalleByCodeCabecera(codeMaeDet);
		return listaMaeDetalle.stream().filter(Objects::nonNull)
				.collect(Collectors.toMap(MaestraDetalle::getCodProg, MaestraDetalle::getMaeDetalleId));
	}


	@Override
	public Map<Long, String> getMaeDetalleIdDescByCode(String codeMaeDet) {
		List<MaestraDetalle> listaMaeDetalle = maestraDetalleRepository.findDetalleByCodeCabecera(codeMaeDet);
		return listaMaeDetalle.stream().filter(Objects::nonNull)
				.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId,MaestraDetalle::getDescripcion));

	}
    
}
