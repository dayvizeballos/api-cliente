package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.TblConfPerfFormacCarrera;

@Repository
public interface TblConfPerfFormacCarreraRepository extends JpaRepository<TblConfPerfFormacCarrera, Long> {

}