package pe.gob.servir.convocatoria.request;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.JerarquiaDTO;

@Getter
@Setter
public class ReqCreaEvaluacion {


	@NotNull(message = Constantes.CAMPO + " listaJerarquia " + Constantes.ES_OBLIGATORIO)
	@Valid
	private List<JerarquiaDTO> listaJerarquia;
	

}
