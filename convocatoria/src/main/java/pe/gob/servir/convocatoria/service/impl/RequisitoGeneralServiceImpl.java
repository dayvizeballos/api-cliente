package pe.gob.servir.convocatoria.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.exception.ConflictException;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.DeclaracionJurada;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.RequisitoGeneral;
import pe.gob.servir.convocatoria.repository.BaseRepository;
import pe.gob.servir.convocatoria.repository.DeclaracionJuradaRepository;
import pe.gob.servir.convocatoria.repository.RequisitoGeneralRepository;
import pe.gob.servir.convocatoria.repository.RequisitoGeneralRepositoryJdbc;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.DeclaracionJuradaDTO;
import pe.gob.servir.convocatoria.request.dto.RequisitoGeneralDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.RequisitoGeneralConvert;
import pe.gob.servir.convocatoria.service.RequisitoGeneralService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RequisitoGeneralServiceImpl implements RequisitoGeneralService {

    @Autowired
    BaseRepository baseRepository;

    @Autowired
    RequisitoGeneralRepository requisitoGeneralRepository;

    @Autowired
    RequisitoGeneralRepositoryJdbc requisitoGeneralRepositoryJdbc;

    @Autowired
    DeclaracionJuradaRepository declaracionJuradaRepository;

    public RespBase<RequisitoGeneralDTO> crearRequisitoGeneral(ReqBase<RequisitoGeneralDTO> request, MyJsonWebToken token) {
        RespBase<RequisitoGeneralDTO> response = new RespBase<>();

        /*se crea una consulta JpaRepository traendo solo el id
         para no traer all object y sus relaciones de uno muchos y no se haga un loop .................
        */
        Optional<Base> base = baseRepository.findByidBase(request.getPayload().getBaseId());

        if (!base.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Base con el id " + request.getPayload().getBaseId() + " no existe");
            return response;
        }

        if (request.getPayload().getDeclaracionJuradaDTOList() == null && request.getPayload().getDeclaracionJuradaDTOList().isEmpty()){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "No se agrego declaraciones juradas");
            return response;
        }

        /*
        se se sabe que base existe, se instancia new objeto
        con su id verificado para settearlo en requisito general
         */
        Base baseBD = new Base();
        baseBD.setBaseId(request.getPayload().getBaseId());

        List<DeclaracionJurada> lstDeclaracion = new ArrayList<>();

        if (request.getPayload().getDeclaracionJuradaDTOList() != null && !request.getPayload().getDeclaracionJuradaDTOList().isEmpty()){
            lstDeclaracion = request.getPayload().getDeclaracionJuradaDTOList().stream()
                    .map(declaracionJuradaDTO -> {
                        validarDeclaracionJurada(declaracionJuradaDTO);
                        DeclaracionJurada declaracionJurada = RequisitoGeneralConvert.converDtoDeclaracionJurada.apply(declaracionJuradaDTO);
                        declaracionJurada.setBase(baseBD);
                        declaracionJurada.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        return declaracionJurada;
                    }).collect(Collectors.toList());
        }

        declaracionJuradaRepository.saveAll(lstDeclaracion);

        RequisitoGeneralDTO payload = new RequisitoGeneralDTO();
        payload.setDeclaracionJuradaDTOList(new ArrayList<>());
        payload.setBaseId(baseBD.getBaseId());
        payload.setDeclaracionJuradaDTOList(requisitoGeneralRepositoryJdbc.lisDeclaracionJuradaDtos(baseBD.getBaseId()));
        return new RespBase<RequisitoGeneralDTO>().ok(payload);

    }

    /**
     * VALIDA DECLARACION JURADA POR TIPO / BASE / ISSERVIR = 0
     * @param declaracionJuradaDTO
     */
    private void validarDeclaracionJurada(DeclaracionJuradaDTO declaracionJuradaDTO) {
        Optional<DeclaracionJurada> declaracionJurada = declaracionJuradaRepository.declaracionJuradaByTipoAndServirAndBase(declaracionJuradaDTO.getTipoId(), declaracionJuradaDTO.getIsServir(), declaracionJuradaDTO.getIdBase());
        if (declaracionJurada.isPresent()) {
            throw new ConflictException("ya registró la declaración jurada: (" + declaracionJuradaDTO.getTipoId() + ") en Base de datos");
        }
    }

    @Override
    public RespBase<RequisitoGeneralDTO> getRequisitoGeneral(Long baseId) {
        RespBase<RequisitoGeneralDTO> response = new RespBase<>();
        if (Util.isEmpty(baseId)) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "el id de base es obligatorio");
            return response;
        }

        Optional<Base> baseOptional = baseRepository.findByidBase(baseId);
        if (!baseOptional.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Requisito general no existe con el id: " + baseId);
            return response;
        }
        RequisitoGeneralDTO payload = new RequisitoGeneralDTO();
        payload.setDeclaracionJuradaDTOList(requisitoGeneralRepositoryJdbc.lisDeclaracionJuradaDtos(baseId));
        payload.setBaseId(baseId);

        return new RespBase<RequisitoGeneralDTO>().ok(payload);
    }

    @Override
    public RespBase<RequisitoGeneralDTO> updateRequisitoGenral(ReqBase<RequisitoGeneralDTO> request, MyJsonWebToken token) {
        RespBase<RequisitoGeneralDTO> response = new RespBase<>();

        Optional<Base> base = baseRepository.findByidBase(request.getPayload().getBaseId());

        if (!base.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Base con el id " + request.getPayload().getBaseId() + " no existe");
            return response;
        }

        if (request.getPayload().getDeclaracionJuradaDTOList() == null && request.getPayload().getDeclaracionJuradaDTOList().isEmpty()){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "No se agrego declaraciones juradas");
            return response;
        }

        Base baseBD = new Base();
        baseBD.setBaseId(request.getPayload().getBaseId());

        List<DeclaracionJurada> lstDeclaracion = new ArrayList<>();

        if (request.getPayload().getDeclaracionJuradaDTOList() != null && !request.getPayload().getDeclaracionJuradaDTOList().isEmpty()){
            lstDeclaracion = request.getPayload().getDeclaracionJuradaDTOList().stream()
                    .map(declaracionJuradaDTO -> {
                        DeclaracionJurada declaracionJurada = RequisitoGeneralConvert.converDtoDeclaracionJurada.apply(declaracionJuradaDTO);
                        declaracionJurada.setBase(baseBD);

                        if (declaracionJuradaDTO.getDeclaracionId() != null){
                            declaracionJurada.setCampoSegUpd(Constantes.ACTIVO , token.getUsuario().getUsuario(), Instant.now());
                        }else{
                            declaracionJurada.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        }
                        declaracionJurada.setEstadoRegistro(declaracionJuradaDTO.getEstado());
                        return declaracionJurada;
                    }).collect(Collectors.toList());
        }

        declaracionJuradaRepository.saveAll(lstDeclaracion);
        RequisitoGeneralDTO payload = new RequisitoGeneralDTO();
        payload.setBaseId(baseBD.getBaseId());
        payload.setDeclaracionJuradaDTOList(new ArrayList<>());
        payload.setDeclaracionJuradaDTOList(requisitoGeneralRepositoryJdbc.lisDeclaracionJuradaDtos(baseBD.getBaseId()));
        return new RespBase<RequisitoGeneralDTO>().ok(payload);
    }
}
