package pe.gob.servir.convocatoria.request;

import lombok.Data;
import pe.gob.servir.convocatoria.request.dto.BaseCronogramaDTO;

import java.util.List;

@Data
public class ReqCreaBaseCronograma {
	private boolean reprogramacion;
	private List<BaseCronogramaDTO> baseCronogramaDTOList;
}
