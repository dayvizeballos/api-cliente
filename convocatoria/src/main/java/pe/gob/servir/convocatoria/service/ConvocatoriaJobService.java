package pe.gob.servir.convocatoria.service;

public interface ConvocatoriaJobService {
	
	public void procesaConvocatoria(Long convocatoriaId);
	
	public void generaArchivo(Long convocatoriaId, Long baseId);
	

}
