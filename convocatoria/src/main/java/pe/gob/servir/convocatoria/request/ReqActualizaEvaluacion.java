package pe.gob.servir.convocatoria.request;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.EvaluacionDTO;

@Getter
@Setter
public class ReqActualizaEvaluacion {

	
	@NotNull(message = Constantes.CAMPO + " listaEvaluacion " + Constantes.ES_OBLIGATORIO)
	@Valid
	private List<EvaluacionDTO> listaEvaluacion;

}
