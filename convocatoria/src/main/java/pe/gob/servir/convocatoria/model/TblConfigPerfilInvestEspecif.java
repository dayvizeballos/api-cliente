package pe.gob.servir.convocatoria.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "tbl_conf_perf_invest_especif", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
@NamedQuery(name="TblConfigPerfilInvestEspecif.findAll", query="SELECT t FROM TblConfigPerfilInvestEspecif t")
public class TblConfigPerfilInvestEspecif extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_invest_especif")
    @SequenceGenerator(name = "seq_conf_perf_invest_especif", sequenceName = "seq_conf_perf_invest_especif", schema = "sch_convocatoria", allocationSize = 1)
    
    @Column(name = "conf_investigacion_especif_id")
    private Long confInvestigacionEspecifId;
    
	@JoinColumn(name = "perfil_id")
	@ManyToOne(optional = false)
	@JsonBackReference
	private Perfil perfil;

    @Column(name = "tipo_especificacion")
    private Long tipoEspecificacion;

    @Column(name = "cantidad")
    private Integer cantidad;

    @Column(name = "flag_superior")
    private Boolean  superior;
    
    @Column(name = "puntaje")
    private BigDecimal puntaje;
   	    
    @Column(name = "base_id")
    private Long baseId;
    
}
