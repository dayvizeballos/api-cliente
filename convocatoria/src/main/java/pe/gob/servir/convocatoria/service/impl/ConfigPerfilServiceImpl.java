package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.ConfigPerfilExperienciaLaboral;
import pe.gob.servir.convocatoria.model.ConfigPerfilExperienciaLaboralDetalle;
import pe.gob.servir.convocatoria.model.ConfigPerfilFormacionCarreraDetalle;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.Perfil;
import pe.gob.servir.convocatoria.model.TblConfPerfDeclaracion;
import pe.gob.servir.convocatoria.model.TblConfPerfExpeLaboral;
import pe.gob.servir.convocatoria.model.TblConfPerfFormacCarrera;
import pe.gob.servir.convocatoria.model.TblConfPerfFormacEspecial;
import pe.gob.servir.convocatoria.model.TblConfPerfFormacEspecif;
import pe.gob.servir.convocatoria.model.TblConfPerfOtrosRequisitos;
import pe.gob.servir.convocatoria.model.TblConfPerfSeccion;
import pe.gob.servir.convocatoria.model.TblConfPerfiles;
import pe.gob.servir.convocatoria.model.TblConfigPerfilInvestEspecif;
import pe.gob.servir.convocatoria.model.TblConfigPerfilInvestigacion;
import pe.gob.servir.convocatoria.repository.ConfigPerfilExperienciaDetalleRepository;
import pe.gob.servir.convocatoria.repository.ConfigPerfilExperienciaRepository;
import pe.gob.servir.convocatoria.repository.ConfigPerfilFormCarreraRepository;
import pe.gob.servir.convocatoria.repository.ConfigPerfilFormEspecialRepository;
import pe.gob.servir.convocatoria.repository.ConfigPerfilInvestEspecifRepository;
import pe.gob.servir.convocatoria.repository.ConfigPerfilInvestigacionRepository;
import pe.gob.servir.convocatoria.repository.FormacionCarreraDetalleRepository;
import pe.gob.servir.convocatoria.repository.FormacionEspecificacionesRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.PerfilRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfDeclaracionRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfExpeLaboralRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfFormacCarreraRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfFormacEspecialRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfFormacEspecifRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfOtrosRequisitosRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfSeccionRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfilesRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaFormacionAcademica;
import pe.gob.servir.convocatoria.request.ReqGuardarActualizarConfigPerfilDTO;
import pe.gob.servir.convocatoria.request.dto.CarreraFormacionAcademicaDTO;
import pe.gob.servir.convocatoria.request.dto.CarreraFormacionAcademicaOtrosGradosDTO;
import pe.gob.servir.convocatoria.request.dto.ConfigPerfilExperienciaLaboralDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.ConfiguracionPerfilDTO;
import pe.gob.servir.convocatoria.request.dto.ConfiguracionPerfilDTO.ConfigOtrosGrados;
import pe.gob.servir.convocatoria.request.dto.ConfiguracionPerfilDTO.ConfigPerfiles;
import pe.gob.servir.convocatoria.request.dto.ConocimientoDTO;
import pe.gob.servir.convocatoria.request.dto.FormacionAcademicaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDetalleDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespObtieneMaestraDetalle;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilExperienciaLaboralDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilExperienciaLaboralDTO.DetalleExperiencia;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormCarreraDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormEspecialDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormEspecificDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPesoDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPublInvestDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPublInvestDTO.DetalleInvestigacionPublicacion;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPublInvestDTO.EspecifInvestigacionPublicacion;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilesDTO;
import pe.gob.servir.convocatoria.response.dto.ResponseGuardarConfigPerfilDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.ConfigPerfilService;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;
import pe.gob.servir.convocatoria.service.PerfilExperienciaService;
import pe.gob.servir.convocatoria.service.PerfilFunction;
import pe.gob.servir.convocatoria.service.PerfilService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;

@Service
public class ConfigPerfilServiceImpl implements ConfigPerfilService {

	@Autowired
	private ConfigPerfilExperienciaRepository configPerfilExperienciaRepository;
	
	@Autowired
	private ConfigPerfilInvestigacionRepository configPerfilInvestigacionRepository;
	
	@Autowired
	private MaestraDetalleRepository maestraDetalleRepository;
	
	@Autowired
	private PerfilExperienciaService perfilExperienciaService;

	@Autowired
	private ConfigPerfilInvestEspecifRepository configPerfilInvestEspecifRepository;
	
	@Autowired
	private FormacionEspecificacionesRepository formacionEspecificacionesRepository;

	@Autowired
	private ConfigPerfilFormCarreraRepository configPerfilFormCarreraRepository;

	@Autowired
	private ConfigPerfilFormEspecialRepository configPerfilFormEspecialRepository;

	@Autowired
	private TblConfPerfFormacCarreraRepository tblConfPerfFormacCarreraRepository;

	@Autowired
	private TblConfPerfFormacEspecialRepository tblConfPerfFormacEspecialRepository;

	@Autowired
	private TblConfPerfFormacEspecifRepository tblConfPerfFormacEspecifRepository;

	@Autowired
	private TblConfPerfExpeLaboralRepository tblConfPerfExpeLaboralRepository;

	@Autowired
	private TblConfPerfDeclaracionRepository tblConfPerfDeclaracionRepository;

	@Autowired
	private TblConfPerfOtrosRequisitosRepository tblConfPerfOtrosRequisitosRepository;

	@Autowired
	private TblConfPerfSeccionRepository tblConfPerfSeccionRepository;

	@Autowired
	private PerfilRepository perfilRepository;

	@Autowired
	private PerfilService perfilService;

	@Autowired
	MaestraDetalleService maestraDetalleService;

	@Autowired
	TblConfPerfilesRepository tblConfPerfilesRepository;

	@Autowired
	private ConfigPerfilExperienciaDetalleRepository configPerfilExperienciaDetalleService;

	@Autowired
	FormacionCarreraDetalleRepository formacionCarreraDetalleRepository;



	@Override

	public RespBase<RespObtieneLista<RConfPerfilFormEspecificDTO>> listarEspecificacionesFormacion(Long perfilId, Long baseId) {
		TblConfPerfFormacEspecif configEspecificacion = new TblConfPerfFormacEspecif();
		Perfil perfil = new Perfil(perfilId);
		configEspecificacion.setPerfil(perfil);
		configEspecificacion.setBaseId(baseId);
		configEspecificacion.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfFormacEspecif> example = Example.of(configEspecificacion);
		List<TblConfPerfFormacEspecif> lstFilter = formacionEspecificacionesRepository.findAll(example);
		

		List<RConfPerfilFormEspecificDTO> itemsDetalle = new ArrayList<>();
		if (lstFilter.size() > 0 && lstFilter != null) {

			lstFilter.forEach(d -> {
				itemsDetalle.add(new RConfPerfilFormEspecificDTO(d));
			});

		}
		return new RespBase<RConfPerfilFormEspecificDTO>().okLista(itemsDetalle);
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public RespBase<RespObtieneLista<RConfPerfilFormCarreraDTO>> listarConfigFormCarrera(Long perfilId, Long baseId) {

		TblConfPerfFormacCarrera filtro = new TblConfPerfFormacCarrera();
		Perfil perfil = new Perfil(perfilId);

		filtro.setPerfil(perfil);
		filtro.setBaseId(baseId);
		filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfFormacCarrera> filexam = Example.of(filtro);
		List<TblConfPerfFormacCarrera> lista = configPerfilFormCarreraRepository.findAll(filexam);
		List<RConfPerfilFormCarreraDTO> itemsDetalle = new ArrayList<>();
		
		if (lista.size() > 0 && lista != null) {

			lista.forEach(d -> {
				itemsDetalle.add(new RConfPerfilFormCarreraDTO(d));

			});
		} else {
			RespBase<ReqCreaFormacionAcademica> oService = perfilService.obtieneFormacionAcademicaById(perfilId);
			if (oService.getStatus().getSuccess()) {
				List<FormacionAcademicaDTO> lstFormacion = oService.getPayload().getLstFormacionAcademica();

				lstFormacion.forEach(d -> {
					RConfPerfilFormCarreraDTO oFormAcademic = new RConfPerfilFormCarreraDTO();
					oFormAcademic = setearCamposCarrera(d);
					itemsDetalle.add(oFormAcademic);
				});

			}

		}

		return new RespBase<RConfPerfilFormCarreraDTO>().okLista(itemsDetalle);
	}

	public RConfPerfilFormCarreraDTO setearCamposCarrera(FormacionAcademicaDTO obj) {
		RConfPerfilFormCarreraDTO oFormAcademic = new RConfPerfilFormCarreraDTO();
		String carreras = "";
		String ids = "";
		
		oFormAcademic.setNivelEducativoId(obj.getNivelEducativoId());
		oFormAcademic.setDescNivelEducativo(obj.getNivelEducativoDesc());
		oFormAcademic.setSituacionAcademicaId(obj.getSituacionAcademicaId());
		oFormAcademic.setDescAcademica(obj.getSitAcademiDesc());
		for (CarreraFormacionAcademicaDTO tmp : obj.getLstCarreraFormaAcademica()) {
			if (tmp.getDescripcion() != null) {
				carreras = carreras + tmp.getDescripcion() + ", ";
				ids = ids + tmp.getCarreraId() + ",";
			}
		}
		carreras = StringUtils.removeEnd(carreras.trim(), ",");
		ids = StringUtils.removeEnd(ids.trim(), ",");
		oFormAcademic.setDescCarreras(carreras);
		oFormAcademic.setCarreraIds(ids);
		oFormAcademic.setDescTipo(obj.getNivelEducativoDesc() + " " + obj.getSitAcademiDesc());

		return oFormAcademic;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public RespBase<RespObtieneLista<RConfPerfilFormEspecialDTO>> listarConfigFormEspecializacion(Long perfilId, Long baseId) {
		TblConfPerfFormacEspecial filtro = new TblConfPerfFormacEspecial();
		Perfil perfil = new Perfil(perfilId);

		filtro.setPerfil(perfil);
		filtro.setBaseId(baseId);
		filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfFormacEspecial> filexam = Example.of(filtro);
		List<TblConfPerfFormacEspecial> lista = configPerfilFormEspecialRepository.findAll(filexam);
		List<RConfPerfilFormEspecialDTO> itemsDetalle = new ArrayList<>();

		if (lista.size() > 0 && lista != null) {

			lista.forEach(d -> {
				itemsDetalle.add(new RConfPerfilFormEspecialDTO(d));
			});

		} else {
			RespBase<ReqCreaFormacionAcademica> oService = perfilService.obtieneFormacionAcademicaById(perfilId);
			if (oService.getStatus().getSuccess()) {
				List<ConocimientoDTO> lstConocimiento = oService.getPayload().getLstConocimientos();

				lstConocimiento.forEach(d -> {
					RConfPerfilFormEspecialDTO oFormEspecial = new RConfPerfilFormEspecialDTO();
					oFormEspecial = setearCamposEspecializacion(d);
					itemsDetalle.add(oFormEspecial);
				});

			}
		}

		return new RespBase<RConfPerfilFormEspecialDTO>().okLista(itemsDetalle);
	}

	@Override
	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public RespBase<Object> guardarActualizarConfigPerfil(MyJsonWebToken token,
			ReqBase<ReqGuardarActualizarConfigPerfilDTO> request) {

		RespBase<Object> response = new RespBase<>();

		Optional<Perfil> oPerfil = this.perfilRepository.findById(
				request.getPayload().getConfiguracionPerfilDTO().getListConfigPerfFormacCarrera().get(0).getPerfilId());

		if (oPerfil.isPresent()) {
			this.guardarActualizarConfigPerfFormacCarrera(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListConfigPerfFormacCarrera());
			this.guardarActualizarConfPerfFormacEspecial(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListConfigPerfFormacEspecial());
			this.guardarActualizarConfPerfFormacEspecif(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListConfigPerfFormacEspecif());
			this.guardarActualizarConfPerfExpeLaboral(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListConfigPerfExpeLaboral());
			this.guardarActualizarConfPerfDeclaracion(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListConfigPerfDeclaraJurada());
			this.guardarActualizarConfPerfOtrosRequisitos(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListConfigPerfOtrosRequisitos());
			this.guardarActualizarConfPerfPesoSeccion(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListaConfigPerfPesoSeccion());
			this.guardarActualizarConfigCarreraOtrosGrados( token, request.getPayload().getConfiguracionPerfilDTO().getListaConfigOtrosGrados());
			
			this.guardarActualizarConfigInvestigacion(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListaConfigInvestigacion());
			this.guardarActualizarConfigInvestigacion(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListaConfigPublicacion());
			this.guardarActualizarConfigEspecifInvestigacion(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListaConfigEspecifInvestigacion());
			this.guardarActualizarConfigEspecifInvestigacion(oPerfil.get(), token, request.getPayload().getConfiguracionPerfilDTO().getListaConfigEspecifPublicacion());
			
			this.guardarConfigTipoModelo( token, request.getPayload().getConfiguracionPerfilDTO().getConfigPerfiles());
			response.setPayload(new ResponseGuardarConfigPerfilDTO(oPerfil.get().getPerfilId().toString(), null, Constantes.CONFIG_PERFIL_EXITOSA));
			response.getStatus().setSuccess(Boolean.TRUE);
		} else {
			return ParametrosUtil.setearResponse(response, Boolean.FALSE, Constantes.PERFIL_NO_EXISTE);
		}

		return response;
	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfigPerfFormacCarrera(Perfil perfil, MyJsonWebToken token,
			List<ConfiguracionPerfilDTO.ConfigPerfFormacCarrera> listConfigPerfFormacCarrera) {

		for (ConfiguracionPerfilDTO.ConfigPerfFormacCarrera carrera : listConfigPerfFormacCarrera) {
			TblConfPerfFormacCarrera tblConfPerfFormacCarrera = null;
			if (Objects.nonNull(carrera.getConfPerfFormacCarreraId())) {
				Optional<TblConfPerfFormacCarrera> oTblConfPerfFormacCarrera = this.configPerfilFormCarreraRepository
						.findById(carrera.getConfPerfFormacCarreraId());
				if (oTblConfPerfFormacCarrera.isPresent()) {
					tblConfPerfFormacCarrera = oTblConfPerfFormacCarrera.get();
					tblConfPerfFormacCarrera = this.setearTblConfPerfFormacCarrera(perfil, carrera, tblConfPerfFormacCarrera);
					tblConfPerfFormacCarrera.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(),
							token.getUsuario().getUsuario(), Instant.now());
				}
			} else {
				tblConfPerfFormacCarrera = this.setearTblConfPerfFormacCarrera(perfil, carrera,
						new TblConfPerfFormacCarrera());
				tblConfPerfFormacCarrera.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}
			if (Objects.nonNull(tblConfPerfFormacCarrera)) {
				this.tblConfPerfFormacCarreraRepository.save(tblConfPerfFormacCarrera);
			}
		}
	}
	
	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfigInvestigacion(Perfil perfil, MyJsonWebToken token,
			List<DetalleInvestigacionPublicacion> listaConfigInvestigacion) {

		for (DetalleInvestigacionPublicacion investigacion : listaConfigInvestigacion) {
			TblConfigPerfilInvestigacion tblConfigPerfilInvestigacion = null;
			if (Objects.nonNull(investigacion.getConfigId())) {
				Optional<TblConfigPerfilInvestigacion> oTblConfigPerfilInvestigacion = this.configPerfilInvestigacionRepository
						.findById(investigacion.getConfigId());
				if (oTblConfigPerfilInvestigacion.isPresent()) {
					tblConfigPerfilInvestigacion = oTblConfigPerfilInvestigacion.get();
					tblConfigPerfilInvestigacion.setRequisitoMinimo(investigacion.getRequisito());
					tblConfigPerfilInvestigacion.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(),
							token.getUsuario().getUsuario(), Instant.now());
				}
			} else {
				tblConfigPerfilInvestigacion = this.setearTblConfigPerfilInvestigacion(perfil, investigacion,
						new TblConfigPerfilInvestigacion());
				tblConfigPerfilInvestigacion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}
			if (Objects.nonNull(tblConfigPerfilInvestigacion)) {
				this.configPerfilInvestigacionRepository.save(tblConfigPerfilInvestigacion);
			}
		}
	}
	
	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfigEspecifInvestigacion(Perfil perfil, MyJsonWebToken token,
			List<EspecifInvestigacionPublicacion> listaConfigEspecifInvestigacion) {

		for (EspecifInvestigacionPublicacion especificacion : listaConfigEspecifInvestigacion) {
			TblConfigPerfilInvestEspecif tblConfigPerfilInvestEspecif = null;
			if (Objects.nonNull(especificacion.getConfigId())) {
				Optional<TblConfigPerfilInvestEspecif> oTblConfigPerfilInvestEspecif = this.configPerfilInvestEspecifRepository
						.findById(especificacion.getConfigId());
				if (oTblConfigPerfilInvestEspecif.isPresent()) {
					tblConfigPerfilInvestEspecif = oTblConfigPerfilInvestEspecif.get();
					tblConfigPerfilInvestEspecif.setCantidad(especificacion.getCantidad());
					tblConfigPerfilInvestEspecif.setPuntaje(especificacion.getPuntaje());
					tblConfigPerfilInvestEspecif.setSuperior(especificacion.getSuperior());
					tblConfigPerfilInvestEspecif.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(),
							token.getUsuario().getUsuario(), Instant.now());
				}
			} else {
				tblConfigPerfilInvestEspecif = this.setearTblConfigPerfilInvestEspecif(perfil, especificacion,
						new TblConfigPerfilInvestEspecif());
				tblConfigPerfilInvestEspecif.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}
			if (Objects.nonNull(tblConfigPerfilInvestEspecif)) {
				this.configPerfilInvestEspecifRepository.save(tblConfigPerfilInvestEspecif);
			}
		}
	}
	
	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfPerfFormacEspecial(Perfil perfil, MyJsonWebToken token,
			List<ConfiguracionPerfilDTO.ConfigPerfFormacEspecial> listaConfPerfFormEspecial) {

		for (ConfiguracionPerfilDTO.ConfigPerfFormacEspecial especial : listaConfPerfFormEspecial) {
			TblConfPerfFormacEspecial tblConfPerfFormacEspecial = null;
			if (Objects.nonNull(especial.getConfPerfFormacEspecialId())) {
				Optional<TblConfPerfFormacEspecial> oTblConfPerfFormacEspecial = this.tblConfPerfFormacEspecialRepository
						.findById(especial.getConfPerfFormacEspecialId());
				if (oTblConfPerfFormacEspecial.isPresent()) {
					tblConfPerfFormacEspecial = oTblConfPerfFormacEspecial.get();
					tblConfPerfFormacEspecial = this.setearTblConfPerfFormacEspecial(perfil, especial, tblConfPerfFormacEspecial);
					tblConfPerfFormacEspecial.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(),
							token.getUsuario().getUsuario(), Instant.now());
				}
			} else {
				tblConfPerfFormacEspecial = this.setearTblConfPerfFormacEspecial(perfil, especial,
						new TblConfPerfFormacEspecial());
				tblConfPerfFormacEspecial.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}

			if (Objects.nonNull(tblConfPerfFormacEspecial)) {
				this.tblConfPerfFormacEspecialRepository.save(tblConfPerfFormacEspecial);
			}
		}

	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfPerfFormacEspecif(Perfil perfil, MyJsonWebToken token,
			List<ConfiguracionPerfilDTO.ConfigPerfFormacEspecif> listaConfPerfFormacEspecif) {
		for (ConfiguracionPerfilDTO.ConfigPerfFormacEspecif especif : listaConfPerfFormacEspecif) {
			TblConfPerfFormacEspecif tblConfPerfFormacEspecif = null;
			if (Objects.nonNull(especif.getConfPerfFormacEspecifId())) {
				Optional<TblConfPerfFormacEspecif> oTblConfPerfFormacEspecif = this.tblConfPerfFormacEspecifRepository
						.findById(especif.getConfPerfFormacEspecifId());
				if (oTblConfPerfFormacEspecif.isPresent()) {
					tblConfPerfFormacEspecif = oTblConfPerfFormacEspecif.get();
					tblConfPerfFormacEspecif = this.setearTblConfPerfFormacEspecif(perfil, especif, tblConfPerfFormacEspecif);
					tblConfPerfFormacEspecif.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), token.getUsuario().getUsuario(), Instant.now());
				}
			} else {
				tblConfPerfFormacEspecif = this.setearTblConfPerfFormacEspecif(perfil, especif, new TblConfPerfFormacEspecif());
				tblConfPerfFormacEspecif.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}
			if (Objects.nonNull(tblConfPerfFormacEspecif)) {
				this.tblConfPerfFormacEspecifRepository.save(tblConfPerfFormacEspecif);
			}
		}
	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfPerfExpeLaboral(Perfil perfil, MyJsonWebToken token,
			List<ConfiguracionPerfilDTO.ConfigPerfExpeLaboral> listaConfigPerfExpeLaboral) {

		for (ConfiguracionPerfilDTO.ConfigPerfExpeLaboral laboral : listaConfigPerfExpeLaboral) {
			TblConfPerfExpeLaboral tblConfPerfExpeLaboral = null;
			if (Objects.nonNull(laboral.getConfPerfExpeLaboralId())) {
				Optional<TblConfPerfExpeLaboral> oTblConfPerfExpeLaboral = this.tblConfPerfExpeLaboralRepository.findById(laboral.getConfPerfExpeLaboralId());
				if (oTblConfPerfExpeLaboral.isPresent()) {
					tblConfPerfExpeLaboral = oTblConfPerfExpeLaboral.get();
					tblConfPerfExpeLaboral = this.setearTblConfPerfExpeLaboral(perfil, laboral, tblConfPerfExpeLaboral);
					tblConfPerfExpeLaboral.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(),
							token.getUsuario().getUsuario(), Instant.now());
				}
				
			} else {
				tblConfPerfExpeLaboral = this.setearTblConfPerfExpeLaboral(perfil, laboral, new TblConfPerfExpeLaboral());
				tblConfPerfExpeLaboral.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}

			if (Objects.nonNull(tblConfPerfExpeLaboral)) {
		
				laboral.setConfPerfExpeLaboralId(this.tblConfPerfExpeLaboralRepository.save(tblConfPerfExpeLaboral).getConfPerfExpeLaboralId());
			}

			this.guardarActualizarConfDetalleExperienciaLaboral(token,laboral) ;
		}

	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfPerfDeclaracion(Perfil perfil, MyJsonWebToken token,
			List<ConfiguracionPerfilDTO.ConfigPerfDeclaraJurada> listaConfigDeclara) {

		for (ConfiguracionPerfilDTO.ConfigPerfDeclaraJurada declara : listaConfigDeclara) {
			TblConfPerfDeclaracion tblConfPerfDeclaracion = null;
			if (Objects.nonNull(declara.getConfPerfDeclaracionId())) {
				Optional<TblConfPerfDeclaracion> oTblConfPerfDeclaracion = this.tblConfPerfDeclaracionRepository
						.findById(declara.getConfPerfDeclaracionId());
				if (oTblConfPerfDeclaracion.isPresent()) {
					tblConfPerfDeclaracion = oTblConfPerfDeclaracion.get();
					tblConfPerfDeclaracion = this.setearTblConfPerfDeclaracion(perfil, declara, tblConfPerfDeclaracion);
					tblConfPerfDeclaracion.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), token.getUsuario().getUsuario(), Instant.now());
				}
			} else {
				tblConfPerfDeclaracion = this.setearTblConfPerfDeclaracion(perfil, declara, new TblConfPerfDeclaracion());
				tblConfPerfDeclaracion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}

			if(Objects.nonNull(tblConfPerfDeclaracion)) {
				this.tblConfPerfDeclaracionRepository.save(tblConfPerfDeclaracion);
			}
		}

	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfPerfOtrosRequisitos(Perfil perfil, MyJsonWebToken token,
			List<ConfiguracionPerfilDTO.ConfigPerfOtrosRequisitos> listaConfgOtrosRequisitos) {

		for (ConfiguracionPerfilDTO.ConfigPerfOtrosRequisitos otros : listaConfgOtrosRequisitos) {
			TblConfPerfOtrosRequisitos tblConfPerfOtrosRequisitos = null;
			if (Objects.nonNull(otros.getConfPerfOtrosRequisitosId())) {
				Optional<TblConfPerfOtrosRequisitos> oTblConfPerfOtrosRequisitos = this.tblConfPerfOtrosRequisitosRepository
						.findById(otros.getConfPerfOtrosRequisitosId());
				if (oTblConfPerfOtrosRequisitos.isPresent()) {
					tblConfPerfOtrosRequisitos = oTblConfPerfOtrosRequisitos.get();
					tblConfPerfOtrosRequisitos = this.setearTblConfPerfOtrosRequisitos(perfil, otros,tblConfPerfOtrosRequisitos);
					tblConfPerfOtrosRequisitos.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(),
							token.getUsuario().getUsuario(), Instant.now());
				}
			} else {
				tblConfPerfOtrosRequisitos = this.setearTblConfPerfOtrosRequisitos(perfil, otros,
						new TblConfPerfOtrosRequisitos());
				tblConfPerfOtrosRequisitos.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}
			if (Objects.nonNull(tblConfPerfOtrosRequisitos)) {
				this.tblConfPerfOtrosRequisitosRepository.save(tblConfPerfOtrosRequisitos);
			}
		}

	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfPerfPesoSeccion(Perfil perfil, MyJsonWebToken token,
			List<ConfiguracionPerfilDTO.ConfigPerfPesoSeccion> listaConfPerfPesoSeccion) {

		for (ConfiguracionPerfilDTO.ConfigPerfPesoSeccion peso : listaConfPerfPesoSeccion) {
			TblConfPerfSeccion tblConfPerfSeccion = null;
			if (Objects.nonNull(peso.getConfPerfSeccionId())) {
				Optional<TblConfPerfSeccion> oTblConfPerfSeccion = this.tblConfPerfSeccionRepository.findById(peso.getConfPerfSeccionId());
				if (oTblConfPerfSeccion.isPresent()) {
					tblConfPerfSeccion = oTblConfPerfSeccion.get();
					tblConfPerfSeccion = this.setearTblConfPerfSeccion(perfil, peso, tblConfPerfSeccion);
					tblConfPerfSeccion.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), token.getUsuario().getUsuario(), Instant.now());
				}
			} else {
				tblConfPerfSeccion = this.setearTblConfPerfSeccion(perfil, peso, new TblConfPerfSeccion());
				tblConfPerfSeccion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}
			if (Objects.nonNull(tblConfPerfSeccion)) {
				this.tblConfPerfSeccionRepository.save(tblConfPerfSeccion);
			}
		}
	}

	private TblConfPerfSeccion setearTblConfPerfSeccion(Perfil perfil,
			ConfiguracionPerfilDTO.ConfigPerfPesoSeccion seccion, TblConfPerfSeccion tblConfSeccion) {

		tblConfSeccion.setConfPerfSeccionId(seccion.getConfPerfSeccionId());
		tblConfSeccion.setPerfil(perfil);
		tblConfSeccion.setTipoSeccion(seccion.getTipoSeccion());
		tblConfSeccion.setPesoSeccion(seccion.getPesoSeccion());
		tblConfSeccion.setDescripcionSeccion(seccion.getDescripcionSeccion());
		tblConfSeccion.setBaseId(seccion.getBaseId());
		return tblConfSeccion;
	}

	private TblConfPerfOtrosRequisitos setearTblConfPerfOtrosRequisitos(Perfil perfil,
			ConfiguracionPerfilDTO.ConfigPerfOtrosRequisitos otros, TblConfPerfOtrosRequisitos tblConfOtros) {

		tblConfOtros.setConfPerfOtrosRequisitosId(otros.getConfPerfOtrosRequisitosId());
		tblConfOtros.setPerfil(perfil);
		tblConfOtros.setDescripcion(otros.getDescripcion());
		tblConfOtros.setRequisitosAdicionales(otros.getRequisitosAdicionales());
		tblConfOtros.setExperienciaDetalleId(otros.getExperienciaDetalleId());
		tblConfOtros.setTipoDatoExperId(otros.getTipoDatoExperId());
		tblConfOtros.setRequisitoId(otros.getRequisitoId());
		tblConfOtros.setPerfilExperienciaId(otros.getPerfilExperienciaId());
		tblConfOtros.setBaseId(otros.getBaseId());
		return tblConfOtros;
	}

	private TblConfPerfDeclaracion setearTblConfPerfDeclaracion(Perfil perfil,
			ConfiguracionPerfilDTO.ConfigPerfDeclaraJurada declara, TblConfPerfDeclaracion tblConfDeclaracion) {

		tblConfDeclaracion.setConfPerfDeclaracionId(declara.getConfPerfDeclaracionId());
		tblConfDeclaracion.setPerfil(perfil);
		tblConfDeclaracion.setDescripcion(declara.getDescripcion());
		tblConfDeclaracion.setRequisitoMinimo(declara.getRequisitoMinimo());
		tblConfDeclaracion.setFlagDj(declara.getFlagDDJJ());
		tblConfDeclaracion.setDeclaracionId(declara.getDeclaracionId());
		tblConfDeclaracion.setTipoId(declara.getTipoId());
		tblConfDeclaracion.setOrden(declara.getOrden());
		tblConfDeclaracion.setEstado(declara.getEstado());
		tblConfDeclaracion.setIsServir(declara.getIsServir());
		tblConfDeclaracion.setBaseId(declara.getBaseId());
		return tblConfDeclaracion;
	}

	private TblConfPerfExpeLaboral setearTblConfPerfExpeLaboral(Perfil perfil,
			ConfiguracionPerfilDTO.ConfigPerfExpeLaboral laboral, TblConfPerfExpeLaboral tblConfExpeLaboral) {

		tblConfExpeLaboral.setConfPerfExpeLaboralId(laboral.getConfPerfExpeLaboralId());
		tblConfExpeLaboral.setPerfil(perfil);
		tblConfExpeLaboral.setTipoExperiencia(laboral.getTipoExperiencia());
		tblConfExpeLaboral.setNivelAcademico(laboral.getNivelAcademico());
		tblConfExpeLaboral.setNivelEducativoId(laboral.getNivelEducativoId());
		tblConfExpeLaboral.setNivelEducativoDesc(laboral.getNivelEducativoDesc());
		tblConfExpeLaboral.setSituacionAcademicaId(laboral.getSituacionAcademicaId());
		tblConfExpeLaboral.setSituacionAcademicaDesc(laboral.getSituacionAacademicaDesc());
		tblConfExpeLaboral.setDesdeAnios(laboral.getDesdeAnios());
		tblConfExpeLaboral.setHastaAnios(laboral.getHastaAnios());
		tblConfExpeLaboral.setRequisitoMinimo(laboral.getRequisitoMinimo());
		tblConfExpeLaboral.setPuntaje(laboral.getPuntaje());
		tblConfExpeLaboral.setBaseId(laboral.getBaseId());
		return tblConfExpeLaboral;
	}

	private TblConfPerfFormacEspecif setearTblConfPerfFormacEspecif(Perfil perfil,
			ConfiguracionPerfilDTO.ConfigPerfFormacEspecif especif, TblConfPerfFormacEspecif tblConfEspecif) {

		tblConfEspecif.setConfPerfFormacEspecifId(especif.getConfPerfFormacEspecifId());
		tblConfEspecif.setPerfil(perfil);
		tblConfEspecif.setTipoEspecificacion(especif.getTipoEspecificacion());
		tblConfEspecif.setCantidad(especif.getCantidad());
		tblConfEspecif.setPuntaje(especif.getPuntaje());
		tblConfEspecif.setDescripcion(especif.getDescripcion());
		tblConfEspecif.setRequisitoMinimo(especif.getRequisitoMinimo());
		tblConfEspecif.setBaseId(especif.getBaseId());
		return tblConfEspecif;
	}

	private TblConfPerfFormacEspecial setearTblConfPerfFormacEspecial(Perfil perfil,
			ConfiguracionPerfilDTO.ConfigPerfFormacEspecial especial, TblConfPerfFormacEspecial tblConfEspecial) {

		tblConfEspecial.setConfPerfFormacEspecialId(especial.getConfPerfFormacEspecialId());
		tblConfEspecial.setPerfil(perfil);
		tblConfEspecial.setTipoConocimientoId(especial.getTipoConocimientoId());
		tblConfEspecial.setDescripcionTipo(especial.getDescripcionTipo());
		tblConfEspecial.setConocimientoId(especial.getConocimientoId());
		tblConfEspecial.setDescripcionConocimiento(especial.getDescripcionConocimiento());
		tblConfEspecial.setRequisitoMinimo(especial.getRequisitoMinimo());
		tblConfEspecial.setPuntaje(especial.getPuntaje());
		tblConfEspecial.setBaseId(especial.getBaseId());
		return tblConfEspecial;
	}

	private TblConfPerfFormacCarrera setearTblConfPerfFormacCarrera(Perfil perfil,
			ConfiguracionPerfilDTO.ConfigPerfFormacCarrera carrera, TblConfPerfFormacCarrera tblConfCarrera) {

		tblConfCarrera.setConfPerfFormacCarreraId(carrera.getConfPerfFormacCarreraId());
		tblConfCarrera.setPerfil(perfil);
		tblConfCarrera.setNivelEducativoId(carrera.getNivelEducativoId());
		tblConfCarrera.setDescripcionAcademica(carrera.getDescripcionAcademica());
		tblConfCarrera.setDescripcionNivelEducativo(carrera.getDescripcionNivelEducativo());
		tblConfCarrera.setRequisitoMinimo(carrera.getRequisitoMinimo());
		tblConfCarrera.setPuntaje(carrera.getPuntaje());
		tblConfCarrera.setSituacionAcademicaId(carrera.getSituacionAcademicaId());
		tblConfCarrera.setDescripcionCarrera(carrera.getDescripcionCarrera());
		tblConfCarrera.setCarreraIds(carrera.getCarreraIds());
		tblConfCarrera.setBaseId(carrera.getBaseId());
		return tblConfCarrera;
	}
	
	private TblConfigPerfilInvestigacion setearTblConfigPerfilInvestigacion(Perfil perfil,
			DetalleInvestigacionPublicacion investigacion, TblConfigPerfilInvestigacion tblConfigPerfilInvestigacion) {

		tblConfigPerfilInvestigacion.setPerfil(perfil);
		tblConfigPerfilInvestigacion.setRequisitoId(investigacion.getTipoId());
		tblConfigPerfilInvestigacion.setDescripcion(investigacion.getDescripcion());
		tblConfigPerfilInvestigacion.setRequisitoMinimo(investigacion.getRequisito());
		tblConfigPerfilInvestigacion.setBaseId(investigacion.getBaseId());
		
		return tblConfigPerfilInvestigacion;
	}
	
	private TblConfigPerfilInvestEspecif setearTblConfigPerfilInvestEspecif(Perfil perfil,
			EspecifInvestigacionPublicacion investigacion, TblConfigPerfilInvestEspecif tblConfigPerfilInvestigacion) {

		tblConfigPerfilInvestigacion.setPerfil(perfil);
		tblConfigPerfilInvestigacion.setTipoEspecificacion(investigacion.getTipoId());
		tblConfigPerfilInvestigacion.setCantidad(investigacion.getCantidad());
		tblConfigPerfilInvestigacion.setSuperior(investigacion.getSuperior());
		tblConfigPerfilInvestigacion.setPuntaje(investigacion.getPuntaje());
		tblConfigPerfilInvestigacion.setBaseId(investigacion.getBaseId());
		
		return tblConfigPerfilInvestigacion;
	}

	@SuppressWarnings("unused")
	private ConfigPerfilFormacionCarreraDetalle setearTblConfOtrosGrados(ConfigOtrosGrados carreraOtros,
			ConfigPerfilFormacionCarreraDetalle tblConfCarreraOtros) {
		tblConfCarreraOtros.setConfPerfFormacCarreraDetalleId(carreraOtros.getConfPerfFormacCarreraDetalleId());
		tblConfCarreraOtros.setPerfilId(carreraOtros.getPerfilId());
		tblConfCarreraOtros.setPuntaje(carreraOtros.getPuntaje());
		tblConfCarreraOtros.setGrado(carreraOtros.getGrado());
		tblConfCarreraOtros.setBaseId(carreraOtros.getBaseId());
		return tblConfCarreraOtros;
	}

	public RConfPerfilFormEspecialDTO setearCamposEspecializacion(ConocimientoDTO obj) {
		RConfPerfilFormEspecialDTO oFormEspecial = new RConfPerfilFormEspecialDTO();

		oFormEspecial.setEspecializacionId(obj.getPerfilConocimientoId());
		oFormEspecial.setTipoConocimientoId(obj.getTipoConocimientoId());
		oFormEspecial.setDescTipo(obj.getDescriTipoConocimiento());
		oFormEspecial.setConocimientoId(obj.getMaeConocimientoId());
		oFormEspecial.setDescConocimiento(obj.getDescrConocimiento());
		return oFormEspecial;

	}

	@SuppressWarnings("unused")
	private ConfigPerfilExperienciaLaboralDetalle setarDetalleExperiencia(
			ConfigPerfilExperienciaLaboralDetalleDTO tbldetalldto, ConfigPerfilExperienciaLaboralDetalle tbldetalle) {
		tbldetalle.setConfPerfExpeLaboralDetalleId(tbldetalldto.getConfPerfExpeLaboralDetalleId());
		tbldetalle.setDesdeAnios(tbldetalldto.getDesdeAnios());
		tbldetalle.setHastaAnios(tbldetalldto.getHastaAnios());
		tbldetalle.setConfPerfExpeLaboralId(tbldetalldto.getConfPerfExpeLaboralId());
		tbldetalle.setRequisitoMinimo(tbldetalldto.getRequisitoMinimo());
		tbldetalle.setPuntaje(tbldetalldto.getPuntaje());
		tbldetalle.setTipoModelo(tbldetalldto.getTipoModelo());
		tbldetalle.setTipoExperiencia(tbldetalldto.getTipoExperiencia());
		return tbldetalle;
	}

	@SuppressWarnings("unused")
	private TblConfPerfiles setarConfiPerfiles(ConfigPerfiles configper, TblConfPerfiles tblperfil) {
		tblperfil.setTipoPerfil(configper.getTipoPerfil());
		tblperfil.setPerfil(new Perfil(configper.getPerfilId()));
		tblperfil.setBaseId(configper.getBaseId());
		return tblperfil;
	}

	public RConfPerfilExperienciaLaboralDTO.DetalleExperiencia getDetalleExperiencia(ConfigPerfilExperienciaLaboral item){
		RConfPerfilExperienciaLaboralDTO.DetalleExperiencia detalle = new RConfPerfilExperienciaLaboralDTO.DetalleExperiencia();
		detalle.setDesdeAnios(item.getDesdeAnios());
		detalle.setHastaAnios(item.getHastaAnios());
		detalle.setPuntaje(item.getPuntaje());
		detalle.setConfPerfExpeLaboralId(item.getConfPerfExpeLaboralId());
		detalle.setRequisitoMinimo(Objects.nonNull(item.getRequisitoMinimo())? item.getRequisitoMinimo() : false );
		return detalle;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public RespBase<List<RConfPerfilExperienciaLaboralDTO>> listarExperienciaLaboralByPerfil(Long perfilId, Long baseID) {
		ConfigPerfilExperienciaLaboral filtro = new ConfigPerfilExperienciaLaboral();
		List<ConfigPerfilExperienciaLaboral> lista;
		Perfil perfil = new Perfil(perfilId);
		filtro.setPerfil(perfil);
		filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		filtro.setBaseId(baseID);
		lista = configPerfilExperienciaRepository.findAll(Example.of(filtro));
		List<RConfPerfilExperienciaLaboralDTO> lstexmain = new ArrayList<>();
		if (lista.isEmpty()) {			
			RespBase<ReqCreaFormacionAcademica> listaConfig = perfilService.obtieneFormacionAcademicaById(perfilId);			
			Long nivelEducativoId = !listaConfig.getPayload().getLstFormacionAcademica().isEmpty()? listaConfig.getPayload().getLstFormacionAcademica().get(0).getNivelEducativoId() : null;
			Long situacionAcademicaId = !listaConfig.getPayload().getLstFormacionAcademica().isEmpty()? listaConfig.getPayload().getLstFormacionAcademica().get(0).getSituacionAcademicaId() : null;
			lstexmain.add(new RConfPerfilExperienciaLaboralDTO(nivelEducativoId,situacionAcademicaId,Constantes.COD_EXPERIENCIA_GENERAL,perfilId));
			lstexmain.add(new RConfPerfilExperienciaLaboralDTO(nivelEducativoId,situacionAcademicaId,Constantes.COD_EXPERIENCIA_ESPECIFICA_MATERIA,perfilId));
			lstexmain.add(new RConfPerfilExperienciaLaboralDTO(nivelEducativoId,situacionAcademicaId,Constantes.COD_EXPERIENCIA_ESPECIFICA_SECTOR_PUBLICO,perfilId));
			lstexmain.add(new RConfPerfilExperienciaLaboralDTO(nivelEducativoId,situacionAcademicaId,Constantes.COD_EXPERIENCIA_GERENCIAL,perfilId));
			lstexmain.add(new RConfPerfilExperienciaLaboralDTO(nivelEducativoId,situacionAcademicaId,Constantes.COD_EXPERIENCIA_ESPECIFICA,perfilId));
		} else {
			List<RConfPerfilExperienciaLaboralDTO.DetalleExperiencia> ltaDetalleGeneral = new ArrayList<>();
			List<RConfPerfilExperienciaLaboralDTO.DetalleExperiencia> ltaEspecificaMateria = new ArrayList<>();
			List<RConfPerfilExperienciaLaboralDTO.DetalleExperiencia> ltaEspecificaSectorPublico = new ArrayList<>();
			List<RConfPerfilExperienciaLaboralDTO.DetalleExperiencia> ltaGerencial = new ArrayList<>();
			List<RConfPerfilExperienciaLaboralDTO.DetalleExperiencia> ltaEspecifica = new ArrayList<>();
			RConfPerfilExperienciaLaboralDTO experienciaTipo1 = null;
			RConfPerfilExperienciaLaboralDTO experienciaTipo2 = null;
			RConfPerfilExperienciaLaboralDTO experienciaTipo3 = null;
			RConfPerfilExperienciaLaboralDTO experienciaTipo4 = null;
			RConfPerfilExperienciaLaboralDTO experienciaTipo5 = null;
			for (ConfigPerfilExperienciaLaboral item : lista) {
				if(item.getTipoExperiencia().longValue() == Constantes.COD_EXPERIENCIA_GENERAL) {
				    experienciaTipo1 = new RConfPerfilExperienciaLaboralDTO(item);
					ltaDetalleGeneral.add(getDetalleExperiencia(item));
				}else if(item.getTipoExperiencia().longValue() == Constantes.COD_EXPERIENCIA_ESPECIFICA_MATERIA) {
					experienciaTipo2 = new RConfPerfilExperienciaLaboralDTO(item);
					ltaEspecificaMateria.add(getDetalleExperiencia(item));
				}else if(item.getTipoExperiencia().longValue() == Constantes.COD_EXPERIENCIA_ESPECIFICA_SECTOR_PUBLICO) {
					experienciaTipo3 = new RConfPerfilExperienciaLaboralDTO(item);
					ltaEspecificaSectorPublico.add(getDetalleExperiencia(item));
				}else if(item.getTipoExperiencia().longValue() == Constantes.COD_EXPERIENCIA_GERENCIAL) {
					experienciaTipo4 = new RConfPerfilExperienciaLaboralDTO(item);
					ltaGerencial.add(getDetalleExperiencia(item));
				}else if(item.getTipoExperiencia().longValue() == Constantes.COD_EXPERIENCIA_ESPECIFICA) {
					experienciaTipo5 = new RConfPerfilExperienciaLaboralDTO(item);
					ltaEspecifica.add(getDetalleExperiencia(item));
				}
			}
			if(Objects.nonNull(experienciaTipo1)){
				experienciaTipo1.setListDetalleExperiencia(ltaDetalleGeneral);
				lstexmain.add(experienciaTipo1);
			}
			if(Objects.nonNull(experienciaTipo2)){
				experienciaTipo2.setListDetalleExperiencia(ltaEspecificaMateria);
				lstexmain.add(experienciaTipo2);
			}
			if(Objects.nonNull(experienciaTipo3)){
				experienciaTipo3.setListDetalleExperiencia(ltaEspecificaSectorPublico);
				lstexmain.add(experienciaTipo3);
			}
			if(Objects.nonNull(experienciaTipo4)){
				experienciaTipo4.setListDetalleExperiencia(ltaGerencial);
				lstexmain.add(experienciaTipo4);
			}
			if(Objects.nonNull(experienciaTipo5)){
				experienciaTipo5.setListDetalleExperiencia(ltaEspecifica);
				lstexmain.add(experienciaTipo5);
			}
			for (RConfPerfilExperienciaLaboralDTO obj : lstexmain) {
				List<ConfigPerfilExperienciaLaboralDetalleDTO> listaModelo = new ArrayList<ConfigPerfilExperienciaLaboralDetalleDTO>();
				for (DetalleExperiencia item : obj.getListDetalleExperiencia()) {
					ConfigPerfilExperienciaLaboralDetalle filtroDeta = new ConfigPerfilExperienciaLaboralDetalle();
					filtroDeta.setConfPerfExpeLaboralId(item.getConfPerfExpeLaboralId());
					filtroDeta.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
					Example<ConfigPerfilExperienciaLaboralDetalle> example = Example.of(filtroDeta);
					List<ConfigPerfilExperienciaLaboralDetalleDTO> listaExperienciaDetalled = new ArrayList<>();
					listaExperienciaDetalled = configPerfilExperienciaDetalleService.findAll(example).stream()
							.filter(val -> val.getConfPerfExpeLaboralId() != null).map(detalle -> {
								ConfigPerfilExperienciaLaboralDetalleDTO dto = PerfilFunction.convertirDtoDetalleExperiencia.apply(detalle);
								return dto;
							}).collect(Collectors.toList());
					listaModelo.addAll(listaExperienciaDetalled);	
				}
				obj.setListModeloDetalleExperiencia(listaModelo);
			}
		}
		return new RespBase<List<RConfPerfilExperienciaLaboralDTO>>().ok(lstexmain);
	}

	@Override
	public RespBase<RespObtieneLista> listarConfigFormacionAcademica(Long perfilId) {
		// TODO Auto-generated method stub
		return null;
	}

	private Map<Long, String> obtenerTipoCursoPrograma() {

		Map<Long, String> tipoCursoPrograma = new HashMap<>();
		RespBase<RespObtieneMaestraDetalle> respMaestraDetalle = maestraDetalleService.obtenerMaestraDetalle(null, null,
				null, null, "TBL_MAE_TIPO_CONO", null);

		for (MaestraDetalle detalle : respMaestraDetalle.getPayload().getMaestraDetalles()) {
			if (detalle.getCodProg().equals("2")) {
				tipoCursoPrograma.put(detalle.getMaeDetalleId(), detalle.getDescripcion());

			} else if (detalle.getCodProg().equals("3")) {
				tipoCursoPrograma.put(detalle.getMaeDetalleId(), detalle.getDescripcion());
			}
		}

		return tipoCursoPrograma;

	}

	@Override
	public RespBase<RespObtieneLista<RConfPerfilPesoDTO>> listarConfigPesos(Long perfilId, Long baseId) {

		TblConfPerfSeccion filtro = new TblConfPerfSeccion();
		Perfil perfil = new Perfil(perfilId);
		filtro.setPerfil(perfil);
		filtro.setBaseId(baseId);
		filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfSeccion> example = Example.of(filtro);
		List<TblConfPerfSeccion> lstFilter = tblConfPerfSeccionRepository.findAll(example);

		List<RConfPerfilPesoDTO> itemsDetalle = new ArrayList<>();
		if (lstFilter.size() > 0 && lstFilter != null) {

			lstFilter.forEach(d -> {
				itemsDetalle.add(new RConfPerfilPesoDTO(d));
			});

		}

		return new RespBase<RConfPerfilPesoDTO>().okLista(itemsDetalle);
	}

	public RespBase<RConfPerfilesDTO> obtenerConfiguracionPerfil(Long perfilId, Long baseId) {
		TblConfPerfiles filtro = new TblConfPerfiles();
		Perfil perfil = new Perfil(perfilId);
		filtro.setPerfil(perfil);
		filtro.setBaseId(baseId);
		filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfiles> example = Example.of(filtro);
		List<TblConfPerfiles> lstFilter = tblConfPerfilesRepository.findAll(example);

		RConfPerfilesDTO configPerfil = new RConfPerfilesDTO();
		if (!lstFilter.isEmpty()) {
			configPerfil.setConfigPerfilId(lstFilter.get(0).getConfPerfilesId());
			configPerfil.setPerfilId(lstFilter.get(0).getPerfil().getPerfilId());
			configPerfil.setTipoPerfil(lstFilter.get(0).getTipoPerfil());
		}
		return new RespBase<RConfPerfilesDTO>().ok(configPerfil);
	}
	
	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public RespBase<Object> actualizaEstadoConfiguracion(MyJsonWebToken token, Long perfilId, Long baseId) {
		RespBase<Object> response = new RespBase<>();		
		Perfil perfil = new Perfil(perfilId);

		TblConfPerfFormacCarrera filtroCarrera = new TblConfPerfFormacCarrera();
		filtroCarrera.setPerfil(perfil);
		filtroCarrera.setBaseId(baseId);
		filtroCarrera.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfFormacCarrera> examCarrera = Example.of(filtroCarrera);
		List<TblConfPerfFormacCarrera> lstCarrera = configPerfilFormCarreraRepository.findAll(examCarrera);
		if (lstCarrera.size() > 0 && lstCarrera != null) {
			lstCarrera.forEach(itemC -> {
				itemC.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
				configPerfilFormCarreraRepository.save(itemC);				
			});
		}
		
		ConfigPerfilFormacionCarreraDetalle filtroDeta = new ConfigPerfilFormacionCarreraDetalle();
		filtroDeta.setPerfilId(perfilId);
		filtroDeta.setBaseId(baseId);				
		filtroDeta.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<ConfigPerfilFormacionCarreraDetalle> examCarreraDet = Example.of(filtroDeta);
		List<ConfigPerfilFormacionCarreraDetalle> lstCarreraDet = formacionCarreraDetalleRepository.findAll(examCarreraDet);
		if (lstCarreraDet.size() > 0 && lstCarreraDet != null) {
			lstCarreraDet.forEach(itemCarDet -> {
				itemCarDet.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
				formacionCarreraDetalleRepository.save(itemCarDet);
			});

		}

		TblConfPerfFormacEspecial filtroEspecial = new TblConfPerfFormacEspecial();
		filtroEspecial.setPerfil(perfil);
		filtroEspecial.setBaseId(baseId);
		filtroEspecial.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfFormacEspecial> examEspecial = Example.of(filtroEspecial);
		List<TblConfPerfFormacEspecial> lstEspecial = configPerfilFormEspecialRepository.findAll(examEspecial);
		if (lstEspecial.size() > 0 && lstEspecial != null) {
			lstEspecial.forEach(itemEsp -> {
				itemEsp.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
				configPerfilFormEspecialRepository.save(itemEsp);
			});

		}

		TblConfPerfFormacEspecif filtroEspecif = new TblConfPerfFormacEspecif();
		filtroEspecif.setPerfil(perfil);
		filtroEspecif.setBaseId(baseId);
		filtroEspecif.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfFormacEspecif> examEspec = Example.of(filtroEspecif);
		List<TblConfPerfFormacEspecif> lstFormEspec = formacionEspecificacionesRepository.findAll(examEspec);
		if (lstFormEspec.size() > 0 && lstFormEspec != null) {
			lstFormEspec.forEach(itemEspc -> {
				itemEspc.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
				formacionEspecificacionesRepository.save(itemEspc);
			});

		}

		ConfigPerfilExperienciaLaboral filtroExp = new ConfigPerfilExperienciaLaboral();
		filtroExp.setPerfil(perfil);
		filtroExp.setBaseId(baseId);
		filtroExp.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<ConfigPerfilExperienciaLaboral> examExp = Example.of(filtroExp);
		List<ConfigPerfilExperienciaLaboral> lstExpLaboral = configPerfilExperienciaRepository.findAll(examExp);
		
		if (lstExpLaboral.size() > 0 && lstExpLaboral != null) {
			lstExpLaboral.forEach(itemExp -> {				
				itemExp.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),Instant.now());
				configPerfilExperienciaRepository.save(itemExp);
				
				ConfigPerfilExperienciaLaboralDetalle filtroDetExp = new ConfigPerfilExperienciaLaboralDetalle();
				filtroDetExp.setConfPerfExpeLaboralId(itemExp.getConfPerfExpeLaboralId());				
				filtroDetExp.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
				Example<ConfigPerfilExperienciaLaboralDetalle> examExpDet = Example.of(filtroDetExp);
				List<ConfigPerfilExperienciaLaboralDetalle> lstExpLaboralDet = configPerfilExperienciaDetalleService.findAll(examExpDet);
				if (lstExpLaboralDet.size() > 0 && lstExpLaboralDet != null) {
					lstExpLaboralDet.forEach(itemDet -> {						
						itemDet.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(), Instant.now());
						configPerfilExperienciaDetalleService.save(itemDet);
					});

				}
			});

		}

		TblConfPerfOtrosRequisitos filtroOtros = new TblConfPerfOtrosRequisitos();
		filtroOtros.setPerfil(perfil);
		filtroOtros.setBaseId(baseId);
		filtroOtros.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfOtrosRequisitos> examOtros = Example.of(filtroOtros);
		List<TblConfPerfOtrosRequisitos> lstOtrosR = tblConfPerfOtrosRequisitosRepository.findAll(examOtros);
		if (lstOtrosR.size() > 0 && lstOtrosR != null) {
			lstOtrosR.forEach(itemR -> {
				itemR.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
				tblConfPerfOtrosRequisitosRepository.save(itemR);
			});

		}

		TblConfPerfDeclaracion filtroDecl = new TblConfPerfDeclaracion();
		filtroDecl.setPerfil(perfil);
		filtroDecl.setBaseId(baseId);
		filtroDecl.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfDeclaracion> examDecl = Example.of(filtroDecl);
		List<TblConfPerfDeclaracion> lstDecl = tblConfPerfDeclaracionRepository.findAll(examDecl);
		if (lstDecl.size() > 0 && lstDecl != null) {
			lstDecl.forEach(itemD -> {
				itemD.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
				tblConfPerfDeclaracionRepository.save(itemD);
			});

		}

		TblConfPerfSeccion filtroSec = new TblConfPerfSeccion();
		filtroSec.setPerfil(perfil);
		filtroSec.setBaseId(baseId);
		filtroSec.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfSeccion> examSec = Example.of(filtroSec);
		List<TblConfPerfSeccion> lstSeccion = tblConfPerfSeccionRepository.findAll(examSec);
		if (lstSeccion.size() > 0 && lstSeccion != null) {
			lstSeccion.forEach(itemS -> {
				itemS.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
				tblConfPerfSeccionRepository.save(itemS);
			});

		}

		TblConfPerfiles filtroPerfil = new TblConfPerfiles();
		filtroPerfil.setPerfil(perfil);
		filtroPerfil.setBaseId(baseId);
		filtroPerfil.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<TblConfPerfiles> examPerfil = Example.of(filtroPerfil);
		List<TblConfPerfiles> lstPerfil = tblConfPerfilesRepository.findAll(examPerfil);
		if (lstPerfil.size() > 0 && lstPerfil != null) {
			lstPerfil.forEach(itemP -> {
				itemP.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
				tblConfPerfilesRepository.save(itemP);
			});

		}

		response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se eliminó la configuracion de Perfil Correctamente ");
		return  response;
	}

	@Override
	public RespBase<List<CarreraFormacionAcademicaOtrosGradosDTO>> obtieneFormacionOtrosGrados(Long perfilId,Long baseId) {
		List<CarreraFormacionAcademicaOtrosGradosDTO> listadetalleFormaciongeneral = new ArrayList<>();

		if (perfilId != null) {
			ConfigPerfilFormacionCarreraDetalle filtroDeta = new ConfigPerfilFormacionCarreraDetalle();
			filtroDeta.setPerfilId(perfilId);
			filtroDeta.setBaseId(baseId);
			filtroDeta.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
			Example<ConfigPerfilFormacionCarreraDetalle> example = Example.of(filtroDeta);
			List<CarreraFormacionAcademicaOtrosGradosDTO> listadetalleFormacion = new ArrayList<>();
			listadetalleFormacion = formacionCarreraDetalleRepository.findAll(example).stream()
					.filter(val -> val.getPerfilId() != null).map(detalle -> {
						CarreraFormacionAcademicaOtrosGradosDTO dto = PerfilFunction.convertirDtoFormacionAcademicaDetalle
								.apply(detalle);
						return dto;
					}).collect(Collectors.toList());
			listadetalleFormaciongeneral.addAll(listadetalleFormacion);
		}

		return new RespBase<List<CarreraFormacionAcademicaOtrosGradosDTO>>().ok(listadetalleFormaciongeneral);
	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfigCarreraOtrosGrados(MyJsonWebToken token,
			List<ConfigOtrosGrados> carreraFormacionAcademicaOtrosGradosDTO) {

		for (ConfigOtrosGrados carreraGrados : carreraFormacionAcademicaOtrosGradosDTO) {
			ConfigPerfilFormacionCarreraDetalle tblConfPerfFormacCarrera = null;
			if (Objects.nonNull(carreraGrados.getConfPerfFormacCarreraDetalleId())) {
				Optional<ConfigPerfilFormacionCarreraDetalle> oTblConfOtros = this.formacionCarreraDetalleRepository
						.findById(carreraGrados.getConfPerfFormacCarreraDetalleId());
				if (oTblConfOtros.isPresent()) {
					tblConfPerfFormacCarrera = oTblConfOtros.get();
					tblConfPerfFormacCarrera = this.setearTblConfOtrosGrados(carreraGrados, tblConfPerfFormacCarrera);
					tblConfPerfFormacCarrera.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(),
							token.getUsuario().getUsuario(), Instant.now());
				}
			} else {

				tblConfPerfFormacCarrera = this.setearTblConfOtrosGrados( carreraGrados, new ConfigPerfilFormacionCarreraDetalle());
				tblConfPerfFormacCarrera.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}
			 if(tblConfPerfFormacCarrera !=null) {
			this.formacionCarreraDetalleRepository.save(tblConfPerfFormacCarrera);
			 }
		}

	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarActualizarConfDetalleExperienciaLaboral(MyJsonWebToken token,

															   ConfiguracionPerfilDTO.ConfigPerfExpeLaboral listaConfigPerfExpeLaboral) {
		if (listaConfigPerfExpeLaboral.getListModeloDetalleExperiencia() != null) {
			for (ConfigPerfilExperienciaLaboralDetalleDTO configPerfExpeLaboral : listaConfigPerfExpeLaboral.getListModeloDetalleExperiencia()) {
				ConfigPerfilExperienciaLaboralDetalle configExperi = null;
				if (Objects.nonNull(configPerfExpeLaboral.getConfPerfExpeLaboralDetalleId())) {
					Optional<ConfigPerfilExperienciaLaboralDetalle> oTblConfPerfExpeLaboral = this.configPerfilExperienciaDetalleService
							.findById(configPerfExpeLaboral.getConfPerfExpeLaboralDetalleId());
					if (oTblConfPerfExpeLaboral.isPresent()) {
						configExperi = oTblConfPerfExpeLaboral.get();
						configExperi = this.setarDetalleExperiencia(configPerfExpeLaboral, configExperi);
						configExperi.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), token.getUsuario().getUsuario(), Instant.now());
					}

				} else {
					configPerfExpeLaboral.setConfPerfExpeLaboralId(listaConfigPerfExpeLaboral.getConfPerfExpeLaboralId());
					configExperi = this.setarDetalleExperiencia(configPerfExpeLaboral, new ConfigPerfilExperienciaLaboralDetalle());
					configExperi.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				}
				if (Objects.nonNull(configExperi)) {
					this.configPerfilExperienciaDetalleService.save(configExperi);
				}
			}
		}
	}

	@Transactional(transactionManager = Constantes.TRANSACCION_NAME, propagation = Propagation.REQUIRED)
	public void guardarConfigTipoModelo( MyJsonWebToken token,
			ConfigPerfiles    confPerfil) {
		TblConfPerfiles tblconfigperfil = null;
		if ((Objects.nonNull(confPerfil.getPerfilId())) && (Objects.nonNull(confPerfil.getBaseId()))){
			Perfil perfil = new Perfil(confPerfil.getConfigPerfilId());
			TblConfPerfiles filtroPerfil = new TblConfPerfiles();
			filtroPerfil.setPerfil(perfil);
			filtroPerfil.setBaseId(confPerfil.getBaseId());
			filtroPerfil.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
			Example<TblConfPerfiles> examPerfil = Example.of(filtroPerfil);
			List<TblConfPerfiles> lstPerfil = tblConfPerfilesRepository.findAll(examPerfil);								
			if (!lstPerfil.isEmpty()) {
				tblconfigperfil = lstPerfil.get(0);
				tblconfigperfil=this.setarConfiPerfiles(confPerfil, tblconfigperfil);
				tblconfigperfil.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), token.getUsuario().getUsuario(), Instant.now());
			} else {
				tblconfigperfil=this.setarConfiPerfiles(confPerfil, new TblConfPerfiles());
				tblconfigperfil.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			}
		}
		if (Objects.nonNull(tblconfigperfil)) {
			this.tblConfPerfilesRepository.save(tblconfigperfil);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public RespBase<RConfPerfilPublInvestDTO> listarPubicacionInvestByPerfil(Long perfilId, Long baseID) {
		TblConfigPerfilInvestigacion filtro = new TblConfigPerfilInvestigacion();
		TblConfigPerfilInvestEspecif filtro2 = new TblConfigPerfilInvestEspecif();

		Perfil perfil = new Perfil(perfilId);
		filtro.setPerfil(perfil);
		filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		filtro.setBaseId(baseID);
		
		filtro2.setPerfil(perfil);
		filtro2.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		filtro2.setBaseId(baseID);
		
		Example<TblConfigPerfilInvestigacion> investigacion = Example.of(filtro);
		Example<TblConfigPerfilInvestEspecif> especifica = Example.of(filtro2);
		List<TblConfigPerfilInvestigacion> lista = configPerfilInvestigacionRepository.findAll(investigacion);
		List<TblConfigPerfilInvestEspecif> listaEspecif = configPerfilInvestEspecifRepository.findAll(especifica);
		
		MaestraDetalle maeInvestigacion  = maestraDetalleRepository
				.findDetalleByCodProg(Constantes.COD_TABLA_TIPO_REQUISITO, Constantes.COD_INVESTIGACION).get(0);
		
		MaestraDetalle maePublicacion  = maestraDetalleRepository
				.findDetalleByCodProg(Constantes.COD_TABLA_TIPO_REQUISITO, Constantes.COD_PUBLICACION).get(0);
		
		RConfPerfilPublInvestDTO detalle = new RConfPerfilPublInvestDTO();
		List<DetalleInvestigacionPublicacion> lstInvestigacion = new ArrayList<>();
		List<DetalleInvestigacionPublicacion> lstPublicacion = new ArrayList<>();
		List<EspecifInvestigacionPublicacion> lstEspecifInvestigacion = new ArrayList<>();
		List<EspecifInvestigacionPublicacion> lstEspecifPublicacion = new ArrayList<>();
		
		if (lista.size() > 0 && lista != null) {
		
			lista.forEach(d -> {
				DetalleInvestigacionPublicacion detalleInv = new DetalleInvestigacionPublicacion(
						d.getConfInvestigacionId(), d.getRequisitoId(), d.getRequisitoId().equals(maeInvestigacion.getMaeDetalleId()) ? maeInvestigacion.getDescripcion() : 
							d.getRequisitoId().equals(maePublicacion.getMaeDetalleId()) ? maePublicacion.getDescripcion() : "", d.getDescripcion(), d.getRequisitoMinimo(), d.getBaseId());
				if(d.getRequisitoId().equals(maeInvestigacion.getMaeDetalleId())) lstInvestigacion.add(detalleInv); 
				else lstPublicacion.add(detalleInv); 

			});
			
			
		} else {
			RespBase<PerfilExperienciaDTO> oService = perfilExperienciaService.findPerfilExpereincia(perfilId);
			
			if (oService.getStatus().getSuccess()) {
				List<PerfilExperienciaDetalleDTO> lstRequisito = oService.getPayload().getDetalle2();

				lstRequisito.forEach(d -> {
					
					DetalleInvestigacionPublicacion detalleInv = setearCamposInvestigacionPublicacion(d, maeInvestigacion, maePublicacion);
					if(d.getRequisitoId().equals(maeInvestigacion.getMaeDetalleId())) lstInvestigacion.add(detalleInv); 
					else if(d.getRequisitoId().equals(maePublicacion.getMaeDetalleId())) lstPublicacion.add(detalleInv); 
				});

			}

		}
		
		if (listaEspecif.size() > 0 && listaEspecif != null) {
			
			listaEspecif.forEach(d -> {
				EspecifInvestigacionPublicacion detalleInv = new EspecifInvestigacionPublicacion(
						d.getConfInvestigacionEspecifId(), d.getTipoEspecificacion(), d.getTipoEspecificacion().equals(maeInvestigacion.getMaeDetalleId()) ? maeInvestigacion.getDescripcion() : 
							d.getTipoEspecificacion().equals(maePublicacion.getMaeDetalleId()) ? maePublicacion.getDescripcion() : "", d.getCantidad(), d.getSuperior(), d.getPuntaje(), d.getBaseId());
				if(d.getTipoEspecificacion().equals(maeInvestigacion.getMaeDetalleId())) lstEspecifInvestigacion.add(detalleInv); 
				else lstEspecifPublicacion.add(detalleInv); 

			});
			
			
		}
		
		detalle.setLstInvestigacion(lstInvestigacion);
		detalle.setLstPublicacion(lstPublicacion);
		detalle.setLstEspecifInvestigacion(lstEspecifInvestigacion);
		detalle.setLstEspecifPublicacion(lstEspecifPublicacion);
	

		return new RespBase<RConfPerfilPublInvestDTO>().ok(detalle);
	}
	
	public DetalleInvestigacionPublicacion setearCamposInvestigacionPublicacion(PerfilExperienciaDetalleDTO obj, MaestraDetalle maeInvestigacion, MaestraDetalle maePublicacion){
		DetalleInvestigacionPublicacion detalleInv = new DetalleInvestigacionPublicacion();
		detalleInv.setTipoId(obj.getRequisitoId());
		detalleInv.setTipoDescripcion(maeInvestigacion.getMaeDetalleId().equals(obj.getRequisitoId()) ? "Investigacion" : maePublicacion.getMaeDetalleId().equals(obj.getRequisitoId()) ? "Publicación" : "");
		detalleInv.setDescripcion(obj.getDescripcion());
		return detalleInv;
	}
}
