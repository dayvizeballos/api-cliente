package pe.gob.servir.convocatoria.model;


import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.BaseCronogramaDTO;
import pe.gob.servir.convocatoria.request.dto.DeclaracionJuradaDTO;

@Getter
@Setter
public class ReporteBasePDF {

	private String logo;
	private String sigla;
	private String codCabecera;
	private String codigoConvocatoria;
	private String regimen;
	private String nombreEntidad;
	private String urlWebEntidad;
	private String modalidadProceso;
	private String tipoProceso;
	private String fechaPublicacion;
	private ReporteDatosConcurso datoConcurso;
	private ReportePerfil perfil;
	private List<BaseCronogramaDTO> cronograma;
	private long diaDifusion;
	private String diaDifusionTexto;
	private String dirigido;
	private List<DeclaracionJuradaDTO> declaracion;
	private List<ReporteDesierto> declaracionServir;
	private List<ReporteDesierto> desierto;
	private List<ReporteDesierto> observacion;
	private List<ReporteDesierto> abstencion;
	private List<ReporteDetalleEvaluacion> evaluaciones;
	private List<ReporteBonificacion> bonificaciones;
}
