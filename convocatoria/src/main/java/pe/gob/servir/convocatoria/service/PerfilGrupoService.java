package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.model.PerfilGrupo;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;



public interface PerfilGrupoService {
    RespBase<RespObtieneLista<PerfilGrupo>> findAllPerfilGrupo(Long id);
    
    RespBase<RespObtieneLista<PerfilGrupo>> listAllPerfilGrupo();
}
