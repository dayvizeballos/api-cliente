package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.MaestraCebecera;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

@Getter
@Setter
public class RespObtieneMaestraDetalle {
	private MaestraCebecera maestraCebecera;
	private List<MaestraDetalle> maestraDetalles;

}
