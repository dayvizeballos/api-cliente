package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RespObtenerDDJJByPerfilIdDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPerfil;
    private List<DeclaraJuradaByPerfilIdDTO> listDeclaracionJurada;

    @Data
    public static class DeclaraJuradaByPerfilIdDTO implements Serializable{
        private static final long serialVersionUID = 1L;
        private Long confPerfDeclaracionId;
        private Long declaracionId;
        private Long tipoId;
        private Long orden;
        private String descripcion;
        private String estado;
        private String isServir;
        private Boolean requisitoMinimo;
        private Long flagDDJJ;
        private Long baseId;
    }

}
