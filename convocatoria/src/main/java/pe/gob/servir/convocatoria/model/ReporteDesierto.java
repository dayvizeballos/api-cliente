package pe.gob.servir.convocatoria.model;

 

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReporteDesierto {
	private String titulo;
	private String contenido;
}
