package pe.gob.servir.convocatoria.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;

public class PDFGeneradorUtil {

	private PDFGeneradorUtil() {
		super();
	}
 
	public static String getHtmlTemplate() {
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode(TemplateMode.HTML);

		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		Context context = new Context();

		return templateEngine.process("informes", context);
	}

	public static byte[] addSingleCodeToPdfFromHtmlTemplate(String htmlFile, String codeHtml) throws DocumentException, IOException {
		StringBuilder sb = new StringBuilder(htmlFile);
		HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, "</body>", codeHtml);
		return getPdfByte(sb);
	}

	public static byte[] getPdfByte(StringBuilder sb) throws DocumentException, IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ITextRenderer renderer = new ITextRenderer();
		renderer.setDocumentFromString(sb.toString());
		renderer.layout();
		renderer.createPDF(baos);
		byte[] byteArray = baos.toByteArray();
		baos.close();
		return byteArray;
	}

}
