package pe.gob.servir.convocatoria.queue.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import pe.gob.servir.convocatoria.common.RoutingKey;

@Component
public class RabbitProducer {

	private static final Logger logger = LoggerFactory.getLogger(RabbitProducer.class);

	private static final String EXCHANGE_NAME = "api.topic.exchange"; /*EL EXCHANGE DE LA COLA auditoria.queue RABBIT*/
	private final RabbitTemplate rabbitTemplate;

	public RabbitProducer(final RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}

	public void writeMessage(RoutingKey routingKey, String message) {
		try {
			rabbitTemplate.convertAndSend(EXCHANGE_NAME, routingKey.getKey(), message);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
