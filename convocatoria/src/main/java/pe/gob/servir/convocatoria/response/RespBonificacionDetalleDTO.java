package pe.gob.servir.convocatoria.response;

import java.io.Serializable;

import lombok.Data;
import pe.gob.servir.convocatoria.model.BonificacionDetalle;

@Data
@SuppressWarnings("serial")
public class RespBonificacionDetalleDTO implements Serializable {
	
    private Long bonificacionDetalleId;    
    private Long bonificacionId;    
    private String descripcion;    
    private Long nivelId;    
    private Long aplicaId;    
    private Long porcentajeBono;  
    private String desNivel;
    
    public RespBonificacionDetalleDTO(BonificacionDetalle item) {
    	this.bonificacionDetalleId = item.getBonificacionDetalleId();
    	this.bonificacionId = item.getBonificacion().getBonificacionId();
    	this.descripcion = item.getDescripcion();
    	this.nivelId = item.getNivelId();
    	this.aplicaId = item.getAplicaId();
    	this.porcentajeBono = item.getPorcentajeBono();
    }
}
