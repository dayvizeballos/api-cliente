package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.model.CarreraProfesional;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;

public interface CapacitacionService {

	RespBase<RespObtieneLista<CarreraProfesional>> obtenerCarreras(Long situacionId);

	RespBase<RespObtieneLista<CarreraProfesional>> listarCarreras();


}
