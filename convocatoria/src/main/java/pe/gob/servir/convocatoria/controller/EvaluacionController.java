package pe.gob.servir.convocatoria.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqActualizaEvaluacion;
import pe.gob.servir.convocatoria.request.ReqAsignaJerarquia;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaEvaluacion;
import pe.gob.servir.convocatoria.request.ReqEliminarJerarquiaEntidad;
import pe.gob.servir.convocatoria.request.dto.DetalleEvaluacionEntidadDTO;
import pe.gob.servir.convocatoria.request.dto.EvaluacionConvocatoriaDTO;
import pe.gob.servir.convocatoria.request.dto.RegimenDTO;
import pe.gob.servir.convocatoria.response.RespActualizaEvaluacion;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCreaEvaluacion;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.dto.RespObtenerPuntajesEvaluacionDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.EvaluacionEntidadService;
import pe.gob.servir.convocatoria.service.EvaluacionService;


@RestController
@Tag(name = "Evaluacion", description = "")
public class EvaluacionController {

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	EvaluacionService evaluacionService;
	
	@Autowired
	EvaluacionEntidadService evaluacionEntidadService;
	
	@Operation(summary = "Crea una evaluacion", description = "Crea una evaluacion", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
@ApiResponses(value = { 
		@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
		@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
				@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
		@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
				@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
@PostMapping(path = { Constantes.BASE_ENDPOINT+"/evaluacion" }, 
			 consumes = {MediaType.APPLICATION_JSON_VALUE },
			 produces = { MediaType.APPLICATION_JSON_VALUE })	
public ResponseEntity<RespBase<RespCreaEvaluacion>> creaEvaluacion(
		@PathVariable String access,
		@Valid @RequestBody ReqBase<ReqCreaEvaluacion> request) {
	MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");	
	RespBase<RespCreaEvaluacion> response = evaluacionService.creaEvaluacion(request,jwt);
	return ResponseEntity.ok(response);

	}
	
	@Operation(summary = "Actualiza evaluacion", description = "Actualiza evaluacion", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@PutMapping(path = { Constantes.BASE_ENDPOINT+"/evaluacion/{jerarquiaId}"},
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespActualizaEvaluacion>> actualizarEvaluacion(
			@PathVariable String access,
			@PathVariable Long jerarquiaId,
			@Valid @RequestBody ReqBase<ReqActualizaEvaluacion> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespActualizaEvaluacion> response = evaluacionService.actualizarEvaluacion(request, jwt,jerarquiaId);
		return ResponseEntity.ok(response);		

	}

	@Operation(summary = "Actualiza evaluacion Entidad", description = "Actualiza evaluacion Entidad", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT+"/evaluacionEntidad/{jerarquiaId}"},
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespActualizaEvaluacion>> actualizarEvaluacionEntidad(
			@PathVariable String access,
			@PathVariable Long jerarquiaId,
			@Valid @RequestBody ReqBase<ReqActualizaEvaluacion> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespActualizaEvaluacion> response = evaluacionService.actualizarEvaluacioneEntidad(request, jwt,jerarquiaId);
		return ResponseEntity.ok(response);

	}
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"evaluaciones para servir y/o entidad", description = Constantes.SUM_OBT_LIST+"evaluaciones para servir y/o entidad", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/evaluaciones"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<RegimenDTO>>> listarEvaluacionesServir(
			@PathVariable String access,
			@RequestParam(value = "idCabecera", required = false) Long idCabecera,
			@RequestParam(value = "modalidadId", required = false) Long modalidadId,
			@RequestParam(value = "tipoId", required = false) Long tipoId,
			@RequestParam(value = "entidadId", required = false ) Long entidadId){
		RespBase<RespObtieneLista<RegimenDTO>> response = evaluacionService.findAllEvaluacion(idCabecera, modalidadId, tipoId , entidadId);
		
		return ResponseEntity.ok(response);		
	}
	
	@Operation(summary = "Inactiva una jerarquia", description = "Inactiva una jerarquia", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@DeleteMapping(path = { Constantes.BASE_ENDPOINT + "/evaluacion/inactivarJerarquia" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> eliminarJerarquia(@PathVariable String access,
			@RequestParam(value = "codigoNivel1", required = true) @NotNull(message = "codigoNivel1 no puede ser nulo") @Valid Long codigoNivel1,
			@RequestParam(value = "codigoNivel2", required = false) Long codigoNivel2,
			@RequestParam(value = "codigoNivel3", required = false) Long codigoNivel3,
			@RequestParam(value = "estado", required = true) @NotNull(message = "estado no puede ser nulo") String estado){
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> response = evaluacionService.eliminarJerarquia(jwt, codigoNivel1, codigoNivel2, codigoNivel3, estado);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Habilitar evaluaciones", description = "Habilitar evaluaciones", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/evaluacion/entidad/asignarEvaluacion" }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> asignarEvaluacionEntidad(
			@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqAsignaJerarquia> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> response = evaluacionEntidadService.asignarEvaluacionEntidad(request, jwt);
		return ResponseEntity.ok(response);
	}
	
	
	@Operation(summary = "Elimina una jerarquia de entidad", description = "Elimina una jerarquia de entidad", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/evaluacion/entidad/eliminarJerarquia" }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> eliminarJeraquiaEntidad(
			@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqEliminarJerarquiaEntidad> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> response = evaluacionEntidadService.eliminarJerarquiaEntidad(request, jwt);
		return ResponseEntity.ok(response);
	}
	
	
	@Operation(summary = Constantes.SUM_OBT_LIST + "evaluaciones", description = Constantes.SUM_OBT_LIST
			+ "evaluaciones", tags = { "" }, security = { @SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/listarEvaluaciones/{idBase}/{entidadId}" },
		produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>>> listarEvaluaciones(@PathVariable String access,
			@PathVariable Long idBase,
			@PathVariable Long entidadId) {
		RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> response = evaluacionService.findEvaluacionEntidadById(idBase ,  entidadId);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = Constantes.LISTA+" los puntajes de la evaluacion por id perfil", description = Constantes.LISTA+" los puntajes de la evaluacion por id perfil", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/puntajes/{idPerfil}/{idRegimen}/{idEntidad}" },produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtenerPuntajesEvaluacionDTO>> obtenerPuntajesEvalByIdPerfil(@PathVariable String access,
																									@PathVariable Long idPerfil,
																									@PathVariable Long idRegimen,
																									@PathVariable Long idEntidad) {
		RespBase<RespObtenerPuntajesEvaluacionDTO> response = evaluacionService.obtenerPuntajeEvaluacion(idPerfil, idRegimen, idEntidad);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = Constantes.SUM_OBT_LIST + "de los tipos de evaluaciones por convocatoria", description = Constantes.SUM_OBT_LIST
			+ "evaluaciones", tags = { "" }, security = { @SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/listarTipoEvaluaciones/{idBase}/{entidadId}" },
		produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<EvaluacionConvocatoriaDTO>>> listarTiposEvaluacion(@PathVariable String access,
			@PathVariable Long idBase,@PathVariable Long entidadId) {
		RespBase<RespObtieneLista<EvaluacionConvocatoriaDTO>> response = evaluacionService.buscarEvaluacionesConvocatoriaEntidad(idBase,entidadId);
		return ResponseEntity.ok(response);
	}
}
