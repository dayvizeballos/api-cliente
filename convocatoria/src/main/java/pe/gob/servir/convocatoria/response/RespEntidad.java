package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.Entidad; 

@Getter
@Setter
public class RespEntidad {
	private List<Entidad> entidad;
}
