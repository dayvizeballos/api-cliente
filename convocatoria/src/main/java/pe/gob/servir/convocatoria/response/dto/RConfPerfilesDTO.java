package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;
import pe.gob.servir.convocatoria.model.TblConfPerfFormacEspecif;
import pe.gob.servir.convocatoria.model.TblConfPerfSeccion;
import pe.gob.servir.convocatoria.model.TblConfPerfiles;

@Data
public class RConfPerfilesDTO {
	private Long configPerfilId;
	private Long perfilId;
	private Long tipoPerfil;
	private String descripcion;
	public RConfPerfilesDTO (TblConfPerfiles perfil) {
		this.configPerfilId = perfil.getConfPerfilesId();
		this.perfilId = perfil.getPerfil().getPerfilId();
		this.tipoPerfil = perfil.getTipoPerfil();
	}
	
	public RConfPerfilesDTO () {
		
	}

}
