package pe.gob.servir.convocatoria.queue.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;


import pe.gob.servir.convocatoria.model.ConvocatoriaQueue;
import pe.gob.servir.convocatoria.model.Picture;
import pe.gob.servir.convocatoria.queue.producer.ConvocatoriaProducer;
import pe.gob.servir.convocatoria.service.ConvocatoriaJobService;
import pe.gob.servir.convocatoria.repository.BaseRepositoryQueue;
import pe.gob.servir.convocatoria.response.RespDifusionConvocatoria;

import java.util.List;


@Component
@Slf4j
public class ConvocatoriaConsumer {

    @Autowired
    ConvocatoriaProducer convocatoriaProducer;
    
    @Autowired
    ConvocatoriaJobService convocatoriaJob;

    @Autowired
    BaseRepositoryQueue baseRepositoryQueue;

    private ObjectMapper objectMapper = new ObjectMapper();

    @RabbitListener(queues = "q.convocatoria.tarea.1")
    public void listenTarea1(String message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {
        ConvocatoriaQueue queue = objectMapper.readValue(message, ConvocatoriaQueue.class);
      
        try {

        	log.info("ConvocatoriaQueue is {} ", queue);
        	convocatoriaJob.procesaConvocatoria(queue.getConvocatoriaId());
            convocatoriaProducer.sendMessageToQueue2(queue);

        } catch (Exception e) {
            throw new Exception("Error al procesar la queue: " + queue);
        }
    }


    @RabbitListener(queues = "q.convocatoria.tarea.2")
    public void listenTarea2(String message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {
        ConvocatoriaQueue queue = objectMapper.readValue(message, ConvocatoriaQueue.class);
        try {
        	convocatoriaJob.generaArchivo(queue.getConvocatoriaId(), queue.getBaseId());
            convocatoriaProducer.sendMessageToQueue3(queue);
        } catch (Exception e) {
            throw new Exception("Error al procesar la queue: " + queue);
        }
    }

    /**
     * CONSUME LA QUEUE q.convocatoria.tarea.3
     * BUSCA INFO CON EL ID BASE
     * ENVIA  A LA QUEUE q.convocatoria.tarea.4
     *
     * @param message
     * @param channel
     * @param tag
     * @throws Exception
     */
    @RabbitListener(queues = "q.convocatoria.tarea.3")
    public void listenTarea3(String message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {
        ConvocatoriaQueue convocatoriaQueue = null;
        int cont = 0;
        try {
            convocatoriaQueue = objectMapper.readValue(message, ConvocatoriaQueue.class);
            List<RespDifusionConvocatoria> ls = baseRepositoryQueue.listBases(convocatoriaQueue.getBaseId());
            if (!ls.isEmpty()) {
                try {
                    for (RespDifusionConvocatoria resp : ls) {
                        resp.setExperienciaList(baseRepositoryQueue.listBasesExperiencia(resp.getIdPerfil()));
                        resp.setConocimientoList(baseRepositoryQueue.listBasesConocimientos(resp.getIdPerfil()));
                        resp.setCondicionAtipicasList(baseRepositoryQueue.listBasesCondicion(resp.getIdPerfil()));
                        resp.setFormacionAcademicaList(baseRepositoryQueue.listBasesFormacion(resp.getIdPerfil()));
                        resp.setCarreras(baseRepositoryQueue.listIdCarreras(resp.getIdPerfil()));
                        resp.setFuncionesPuestoList(baseRepositoryQueue.listFuncionesPuesto(resp.getIdPerfil()));
                        resp.setHabilidadesList(baseRepositoryQueue.listHabilidades(resp.getIdPerfil()));
                        resp.setRequiAdicionalesList(baseRepositoryQueue.listRequisitosAdicionales(resp.getIdPerfil()));
                        convocatoriaProducer.sendMessageToQueue4(resp);
                    }
                } catch (Exception e) {
                    log.info(e.getMessage());
                }
            } else {
                log.info("se aborta el consumer la ls esta vacia");
                throw new Exception("ls is empty: " + convocatoriaQueue);
            }

        } catch (Exception e) {
            log.info(e.getMessage());
            throw new Exception("Error al procesar la queue: " + convocatoriaQueue);
        }
    }
   
    /**
     * CAMBIA EL ESTADO EN PROCESO
     *
     * @param message
     * @param channel
     * @param tag
     * @throws Exception
     */

    //@RabbitListener(queues = "q.convocatoria.tarea.5")
    public void listenTarea5(String message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {
        Picture queue = objectMapper.readValue(message, Picture.class);
        try {
            /**
             * LOGICA
             */

            /**
             * FINALIZA LAS COLAS EL SIGUIENTE PASO ES JOB
             */

        } catch (Exception e) {
            throw new Exception("Error al procesar la queue: " + queue);
        }
    }


    /**
     * EL JOB FINAL PARA EL ENVIO DE
     * NOTIFICACION
     */
    public void jobFinal() {

    }
}
