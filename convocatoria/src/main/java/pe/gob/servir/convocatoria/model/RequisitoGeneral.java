package pe.gob.servir.convocatoria.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbl_base_req_general", schema = "sch_base")
@Getter
@Setter
public class RequisitoGeneral extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tbl_base_req_general")
    @SequenceGenerator(name = "seq_tbl_base_req_general", sequenceName = "seq_tbl_base_req_general", schema = "sch_base", allocationSize = 1)
    @Column(name = "req_general_id")
    private Long requisitoId;

    @Column(name = "descripcion_req_gen")
    private String descripcion;

    @Column(name = "cuestionario_decl_jur")
    private String cuestionario;

    @Column(name = "informe_detalle_id")
    private Long informeDetalleId;

    @JoinColumn(name="base_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Base base;

    /*
    @OneToMany(cascade= CascadeType.ALL , mappedBy = "requisitoGeneral")
    @Column(nullable = true)
    @JsonManagedReference
    private List<DeclaracionJurada> declaracionJuradaList;
*/

}
