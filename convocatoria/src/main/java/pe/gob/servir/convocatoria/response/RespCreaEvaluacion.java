package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.Jerarquia;

@Getter
@Setter
public class RespCreaEvaluacion {

	private List<Jerarquia>  listaJerarquia;
	
	
}
