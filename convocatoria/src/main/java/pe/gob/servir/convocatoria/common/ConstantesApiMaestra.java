package pe.gob.servir.convocatoria.common;

public class ConstantesApiMaestra {
    public static final String ENDPOINT_ENVIAR_CORREO = "/v1/email/convocatoria/enviar";
    public static final String ENDPOINT_SUBIR_ARCHIVO = "/v1/file/upload";
}
