package pe.gob.servir.convocatoria.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.repository.PerfilExperienciaRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDetalleDTO;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PerfilExperienciaRepositoryJdbcImpl implements PerfilExperienciaRepositoryJdbc {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PerfilExperienciaRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_FIND_PERFIL_EXPERIENCIA = "select tpe.estado_registro as estado , tpe.experiencia_perfil_id as id , tpe.anio_experiencia_total as anioExpTotal , tpe.perfil_id as perfilId,\n" +
            "tpe.mes_experiencia_total as mesExpTotal , tpe.anio_exper_requ_puesto as anioExReqPuesto , tpe.mes_exper_requ_puesto as mesExReqPuesto,\n" +
            "tpe.anio_exper_sector_publico as anioExpSecPub , tpe.mes_exper_sector_publico as mesExpSecPub , tpe .nivel_minimo_puesto_id as nivelMinPueId,\n" +
            "tpe.aspectos_complementarios as aspectos\n" +
            "from sch_convocatoria.tbl_perfil_experiencia tpe where tpe.perfil_id = :perfilId and tpe.estado_registro = '1' order by tpe.experiencia_perfil_id desc ";

    private static final String SQL_FIND_PERFIL_EXPERIENCIA_DETALLE = "select tped.orden as orden , tped.experiencia_detalle_id as id , tped .experiencia_perfil_id as perfilExperienciaId," +
            " tped.tipo_dato_experiencia_id as tiDaExId , tped.requisito_id as requisitoId, tped.estado_registro as estado, \n" +
            "tped.descripcion as descripcion\n" +
            "from sch_convocatoria.tbl_perfil_experiencia_detalle tped where tped.experiencia_perfil_id = :perfilExperiencia and tped.tipo_dato_experiencia_id = :tipoDato and tped.estado_registro = '1' order by tped.experiencia_detalle_id asc ";


    @Override
    public List<PerfilExperienciaDTO> listPerfilExperiencia(Long perfilId) {
        List<PerfilExperienciaDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_PERFIL_EXPERIENCIA);
            if (!Util.isEmpty(perfilId)) objectParam.put("perfilId", perfilId);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(PerfilExperienciaDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<PerfilExperienciaDetalleDTO> listPerfilExperienciaDetalle(Long perfiExperiencia , Long tipo) {
        List<PerfilExperienciaDetalleDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_PERFIL_EXPERIENCIA_DETALLE);
            if (!Util.isEmpty(perfiExperiencia)) objectParam.put("perfilExperiencia", perfiExperiencia);
            if (!Util.isEmpty(tipo)) objectParam.put("tipoDato", tipo);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(PerfilExperienciaDetalleDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }
}
