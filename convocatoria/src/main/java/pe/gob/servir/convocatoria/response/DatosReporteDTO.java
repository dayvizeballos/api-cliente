package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Data;

@Data
public class DatosReporteDTO {
	private String condicion;
	private String descEtapa;
	private String modalidadAcceso;
	private String desTipo;
	private String periodo;	
	List<EtapaVsEvaluacionDTO> lstEtapas;
}
