package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.gob.servir.convocatoria.model.TblConfPerfiles;

@Repository
public interface TblConfPerfilesRepository extends JpaRepository<TblConfPerfiles, Long> {

}