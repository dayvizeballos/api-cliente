package pe.gob.servir.convocatoria.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.audit.AuditableEntity;


@Entity
@Table(name = "tbl_mae_detalle", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
public class MaestraDetalle extends AuditEntity implements AuditableEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mae_detalle")
	@SequenceGenerator(name = "seq_mae_detalle", sequenceName = "seq_mae_detalle", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "mae_detalle_id")
	private Long maeDetalleId;

	@Column(name = "mae_cabecera_id")
	private Long maeCabeceraId;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "descripcion_corta")
	private String descripcionCorta;
	
	@Column(name = "abreviatura")
	private String sigla;

	@Column(name = "referencia")
	private String referencia;
	
	@Column(name = "orden")
	private Long orden;

	@Column(name = "cod_prog")
	private String codProg;
	
	@Transient
	private String estado;
	
	@Transient
	private Long configuracionId;
	
	@Transient 
	String estadoConfiguracion;
	
	@Transient
	private List<MaestraDetalle> listaDetalleModalidad;
	
	@Transient
	private List<MaestraDetalle> listaTipo;
	
	@Transient
	private List<Evaluacion> listaEvaluacion;

	public MaestraDetalle () {
		
	}
	
	public MaestraDetalle (Long maeCabeceraId,String codProg) {
		this.maeCabeceraId = maeCabeceraId;
		this.codProg = codProg;
		
	}
	
	public MaestraDetalle (Long maeDetalleId) {
		this.maeDetalleId = maeDetalleId;
	}


}
