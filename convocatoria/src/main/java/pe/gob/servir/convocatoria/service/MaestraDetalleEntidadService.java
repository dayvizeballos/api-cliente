package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqAsigarMaeDetalleEntidad;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraDetalleEntidad;
import pe.gob.servir.convocatoria.response.RespAsignarMaeDetalleEntidad;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespComboCabecera;
import pe.gob.servir.convocatoria.response.RespMaestraDetalleEntidad;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface MaestraDetalleEntidadService {

	
	RespBase<RespAsignarMaeDetalleEntidad> asignarMaestraDetalle(ReqBase<ReqAsigarMaeDetalleEntidad> request, MyJsonWebToken token, 
					Long maeConfigMaestraId);
	
	RespBase<RespComboCabecera> obtenerMaestraDetalleEntidad(Long idEntidad,Long cabeceraId);

	RespBase<Object> habilitarMaestraDetalleEntidad(MyJsonWebToken token, Long configMaestraId,String estado);

	RespBase<RespMaestraDetalleEntidad> guardarMaestraDetalleEntidad(ReqBase<ReqMaestraDetalleEntidad> request, MyJsonWebToken token, 
			Long configMaestraId);

}
