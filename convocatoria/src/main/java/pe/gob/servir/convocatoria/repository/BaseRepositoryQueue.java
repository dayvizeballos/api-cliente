package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.request.dto.BaseDTO;
import pe.gob.servir.convocatoria.request.dto.CorreoDTO;
import pe.gob.servir.convocatoria.request.dto.MaestraDetalleDto;
import pe.gob.servir.convocatoria.response.*;

import java.util.List;

public interface BaseRepositoryQueue {
    List<Long> idEntidades(Long estadoProceso, String fechaHoy);
    List<CorreoDTO> listDetalleCorreo(Long entidadId , Long estadoProceso , String fechaHoy);
    List<MaestraDetalleDto> maestraDetalle (String codpMaDetalle , String codCabMaeCabe);
    List<CorreoDTO> getBases(Long estadoProceso, String fechaHoy);
    List<CorreoDTO> listDetalleBases( Long estadoProceso , String fechaHoy);
    String codigoConvocatoria(BaseDTO baseDTO);
    String getCodigoConvocatoria(CorreoDTO baseDTO, int anio);
    MaestraDetalleDto findMaestraDetalle(Long id);
    List<RespDifusionConvocatoria> listBases(Long idBase);
    List<RespDifusionConocimientos> listBasesConocimientos(Long idPerfil);
    List<RespDifusionExperiencia> listBasesExperiencia(Long idPerfil);
    List<RespDifusionFormacionAcademica> listBasesFormacion(Long idPerfil);
    List<RespCondicionAtipica> listBasesCondicion(Long idPerfil);
    List<Long> listIdCarreras(Long idPerfil);
    List<String > listFuncionesPuesto(Long idPerfil);
    List<String > listHabilidades(Long idPerfil);
    List<String > listRequisitosAdicionales(Long idPerfil);
}
