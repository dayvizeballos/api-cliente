package pe.gob.servir.convocatoria.repository;

import java.util.List;
import java.util.Map;

import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.BonificacionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerBonificacionDTO;

public interface BonificacionRepositoryJdbc {

	 List<ObtenerBonificacionDTO> buscarBonificacionByFilter(Map<String, Object> parametroMap);
	 BonificacionDTO getBonificacion (Long bonificacionId);
	 List<BonificacionDetalleDTO> getBonificacionDetalleList (Long bonificacionId);
}
