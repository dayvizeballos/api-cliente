package pe.gob.servir.convocatoria.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString
public class RespOptieneConfiguracionExperiencia implements Serializable{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long tipoExperiencia;
	private String descripcionNivel;
    private Long idPerfil;
    private Long  nivelEducativoId;
    private Long  situacionAcademicaId;
    private String nivelEducativoDesc;
    private String sitAcademiDesc;
    private List<DetalleExperiencia> listDetalleExperiencia;

	
	@Data
	public  class  DetalleExperiencia  implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Long desdeAnios;
		private Long hastaAnios;
		private Boolean requisitoMinimo;
		private Long puntaje;
		
		
	}

}
