package pe.gob.servir.convocatoria.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.commons.lang3.StringUtils;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TituloHtml {
	public String style() default "text-align: center;";
	public boolean strong() default true;
	public String nombre() default StringUtils.EMPTY;
}
