package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.DatosConcurso;

import java.util.Optional;

@Repository
public interface DatosConcursoRepository extends JpaRepository<DatosConcurso, Long>{

    @Query("SELECT  b.datoConcursoId FROM  DatosConcurso b WHERE b.datoConcursoId = :datoConcursoId ")
    public Optional<DatosConcurso> findByidDa(@Param("datoConcursoId")Long datoConcursoId);

    @Query("SELECT b FROM  DatosConcurso b WHERE b.base = :base ")
    public Optional<DatosConcurso> findByBase(@Param("base")Base base);

}
