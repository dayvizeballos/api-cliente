package pe.gob.servir.convocatoria.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.VariablesSistema;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.feign.client.MaestraApiClient;
import pe.gob.servir.convocatoria.model.CarreraProfesional;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.Organigrama;
import pe.gob.servir.convocatoria.model.PerfilGrupo;
import pe.gob.servir.convocatoria.model.UnidadOrganica;
import pe.gob.servir.convocatoria.request.dto.MaestraConocimientoDTO;
import pe.gob.servir.convocatoria.request.dto.ParametrosDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.service.CapacitacionService;
import pe.gob.servir.convocatoria.service.ExcelService;
import pe.gob.servir.convocatoria.service.MaestraConocimientoService;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;
import pe.gob.servir.convocatoria.service.PerfilGrupoService; 

@Service
public class ExcelServiceImpl  implements ExcelService{

	@Autowired
	private VariablesSistema variableSistema;
	
	@Autowired
	CapacitacionService capacitacionService;
	
	@Autowired
	EntidadClient entidadClient;
	
	@Autowired
	PerfilGrupoService perfilGrupoService;
	
	@Autowired
	MaestraApiClient maestraClient;
	
	@Autowired
	MaestraDetalleService maestraDetalleService;
	
	@Autowired
	MaestraConocimientoService maestraConocimientoService;
	
	@Override
	public RespBase<String> descargarPlantillaRegimen30057(Long idEntidad) throws IOException {
		//String filePath=System.getProperty("user.dir")+variableSistema.rutaExcelWindowPerfil30057;
		String filePath=System.getProperty("user.home")+variableSistema.rutaExcelLinuxPerfil30057;
		
		
		List<Organigrama> organos = listarOrganos(idEntidad, null);
		List<UnidadOrganica> unidaOrganica = listarUnidadOrganica(idEntidad);
		List<PerfilGrupo> grupoServidoresCivil = listarGrupoDeServidoresCiviles();
		List<ParametrosDTO> nivel = listarNivel();
		List<MaestraDetalle> puestoTipo = listarPuestoTipo(Constantes.TBL_PER_PTO);
		List<MaestraDetalle> periodicidad = listarPuestoTipo(Constantes.TBL_PER_PER_APL);
		List<MaestraDetalle> nivelEducativo = listarPuestoTipo(Constantes.TBL_PER_NIV_EDU);
		List<MaestraDetalle> situacionEducativo = listarPuestoTipo(Constantes.TBL_PER_EST_NIV_EDU);
		List<MaestraDetalle> gradoEducSup = listarPuestoTipo(Constantes.TBL_PER_SIT_ACA);
		
		List<MaestraDetalle> EgresadoGrado = listarPuestoTipo(Constantes.TBL_PER_EST_GRA);
		List<MaestraConocimientoDTO> conocimientoTecnico =listarConocimiento(variableSistema.conocimientoTecnico);
		List<MaestraConocimientoDTO> cursosEspecializacion =listarConocimiento(variableSistema.cursosEspecializacion);
		List<MaestraConocimientoDTO> programaEspecializacion =listarConocimiento(variableSistema.programasEspecializacion);
		List<MaestraConocimientoDTO> conocimientoOfimatica =listarConocimiento(variableSistema.conocimientoOfimatica);
		List<MaestraConocimientoDTO> conocimientoIdioma =listarConocimiento(variableSistema.conocimientoIdioma);
		List<MaestraDetalle> puestoMinimo = listarPuestoTipo(Constantes.TBL_PER_NIV_MIN_PTO);
		List<MaestraDetalle> tipoRequisito = listarPuestoTipo(Constantes.TBL_PER_TIP_REQ);
		String[] nivelIdioma = {"BASICO","INTERMEDIO","AVANZADO"};
		
		 //Open file
		  FileInputStream inputStream = new FileInputStream(new File(filePath));
		  XSSFWorkbook workBook = new XSSFWorkbook(inputStream);

	 
		  //XSSFSheet hoja1 = workBook.getSheetAt(0);
		  XSSFSheet hoja2 = workBook.getSheetAt(1);
		  
		  
		  for (int i = 0; i < organos.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(0).setCellValue(organos.get(i).getOrganigramaId());
				} else {
					hoja2.createRow(i+2).createCell(0).setCellValue(organos.get(i).getOrganigramaId());
				}
				hoja2.getRow(i+2).createCell(1).setCellValue(organos.get(i).getDescripcion());
		}
		  
		  for (int i = 0; i < unidaOrganica.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(3).setCellValue(unidaOrganica.get(i).getOrganigramaId());
				} else {
					hoja2.createRow(i+2).createCell(3).setCellValue(unidaOrganica.get(i).getOrganigramaId());
				}
				hoja2.getRow(i+2).createCell(4).setCellValue(unidaOrganica.get(i).getUnidadOrganica());
		}
		  
		  for (int i = 0; i < grupoServidoresCivil.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(6).setCellValue(grupoServidoresCivil.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(6).setCellValue(grupoServidoresCivil.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(7).setCellValue(grupoServidoresCivil.get(i).getDescripcion());
		  	}

		   
		  int celdaInicioRol=0;
		  int celdaInicioNivel=0;
		  int celdaPuestoTipo=0;
		  int celdaGpoCivilReportar=0;
		  int celdaPeriodicidad=0;
		  int celdaNivelEducativo=0;
		  int celdagradoEducSup=0;
		  int celdaCarreras=0;
		  int celdaCarreras2=0;
		  int celdaCarreras3=0;
		  int celdaCarreras4=0;
		  int celdaCarreras5=0;
		  int celdaEgresadoGradoM=0;
		  int celdaEgresadoGradoD=0;
		  int celdaColegiatura=0;
		  int celdaHabilitacion=0;
		  int celdaConocimientoTecnico=0;
		  int celdacursosEspecializacion=0;
		  int celdaprogramaEspecializacion=0;
		  int celdaconocimientoOfimatica=0;
		  int celdaconocimientoIdioma=0;
		  int celdaPuestoMinimo=0;
		  int celdatipoRequisito=0;
		  int celdaniveIdioma=0;
		  
		  List<PerfilGrupo> familia2= new ArrayList<PerfilGrupo>();
		  for (int i = 0;  i < grupoServidoresCivil.size(); i++) {
			  List<PerfilGrupo> familia= listarFamiliasRol(grupoServidoresCivil.get(i).getId());
			  for (int j = 0; j < familia.size(); j++) {
				  familia2.add(familia.get(j));
					if(hoja2.getRow(j+2)!=null) {
						hoja2.getRow(j+2).createCell(9+(i*(i==0?3:2))).setCellValue(familia.get(j).getId());
					} else {
						hoja2.createRow(i+2).createCell(9+(i*(i==0?3:2))).setCellValue(familia.get(j).getId());
					}
					hoja2.getRow(j+2).createCell(10+(i*(i==0?3:2))).setCellValue(familia.get(j).getDescripcion());
					
					celdaInicioRol=10+(i*(i==0?3:2))+2;
			  }
			  
		}
		  
	 
		   for (int l = 0; l < familia2.size(); l++) {
			  List<PerfilGrupo> rol= listarFamiliasRol(familia2.get(l).getId());
			  for (int k = 0; k < rol.size(); k++) {

					if(hoja2.getRow(k+2)!=null) {
						hoja2.getRow(k+2).createCell(celdaInicioRol+(l*2)).setCellValue(rol.get(k).getId());
					} else {
						hoja2.createRow(k+2).createCell(celdaInicioRol+(l*2)).setCellValue(rol.get(k).getId());
					}
					hoja2.getRow(k+2).createCell(celdaInicioRol+1+(l*2)).setCellValue(rol.get(k).getDescripcion());
					celdaInicioNivel=celdaInicioRol+1+(l*2)+2;
			  }
		} 
		   
			  for (int i = 0; i < nivel.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaInicioNivel).setCellValue(nivel.get(i).getParametroId());
					} else {
						hoja2.createRow(i+2).createCell(celdaInicioNivel).setCellValue(nivel.get(i).getParametroId());
					}
					hoja2.getRow(i+2).createCell(celdaInicioNivel+1).setCellValue(nivel.get(i).getDescripcion());
					celdaPuestoTipo=celdaInicioNivel+3;
			}
			  
			  for (int i = 0; i < puestoTipo.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaPuestoTipo).setCellValue(puestoTipo.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdaPuestoTipo).setCellValue(puestoTipo.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdaPuestoTipo+1).setCellValue(puestoTipo.get(i).getDescripcion());
				  celdaGpoCivilReportar=celdaPuestoTipo+3;
			}
			  
			  for (int i = 0; i < grupoServidoresCivil.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaGpoCivilReportar).setCellValue(grupoServidoresCivil.get(i).getId());
					} else {
						hoja2.createRow(i+2).createCell(celdaGpoCivilReportar).setCellValue(grupoServidoresCivil.get(i).getId());
					}
					hoja2.getRow(i+2).createCell(celdaGpoCivilReportar+1).setCellValue(grupoServidoresCivil.get(i).getDescripcion());
					celdaPeriodicidad=celdaGpoCivilReportar+3;
			  }
			  
			  for (int i = 0; i < periodicidad.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaPeriodicidad).setCellValue(periodicidad.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdaPeriodicidad).setCellValue(periodicidad.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdaPeriodicidad+1).setCellValue(periodicidad.get(i).getDescripcion());
					celdaNivelEducativo=celdaPeriodicidad+3;
			  }
			  
			  for (int i = 0; i < nivelEducativo.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaNivelEducativo).setCellValue(nivelEducativo.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdaNivelEducativo).setCellValue(nivelEducativo.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdaNivelEducativo+1).setCellValue(nivelEducativo.get(i).getDescripcion());
					celdagradoEducSup=celdaNivelEducativo+3;
			  }
			  
			  List<CarreraProfesional> carreras = new ArrayList<CarreraProfesional>();
			  List<CarreraProfesional> carreras2 = new ArrayList<CarreraProfesional>();
			  List<CarreraProfesional> carreras3 = new ArrayList<CarreraProfesional>();
			  List<CarreraProfesional> carreras4 = new ArrayList<CarreraProfesional>();
			  List<CarreraProfesional> carreras5 = new ArrayList<CarreraProfesional>();
			  for (int i = 0; i < gradoEducSup.size(); i++) {
				  	switch(i) {
				  	case 0:
				  		carreras = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
				  		break;
				  	case 1:
				  		carreras2 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
				  		break;
				  	case 2:
				  		carreras3 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
				  		break;
				  	case 3:
				  		carreras4 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
				  		break;
				  	case 4:
				  		carreras5 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
				  		break;
				  	}
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdagradoEducSup).setCellValue(gradoEducSup.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdagradoEducSup).setCellValue(gradoEducSup.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdagradoEducSup+1).setCellValue(gradoEducSup.get(i).getDescripcion());
					 celdaCarreras=celdagradoEducSup+3;
			  }
			  
			  for (int i = 0; i < carreras.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaCarreras).setCellValue(carreras.get(i).getId());
					} else {
						hoja2.createRow(i+2).createCell(celdaCarreras).setCellValue(carreras.get(i).getId());
					}
					hoja2.getRow(i+2).createCell(celdaCarreras+1).setCellValue(carreras.get(i).getDescripcion());
					celdaCarreras2=celdaCarreras+3;
			  }
			  
			  for (int i = 0; i < carreras2.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaCarreras2).setCellValue(carreras2.get(i).getId());
					} else {
						hoja2.createRow(i+2).createCell(celdaCarreras2).setCellValue(carreras2.get(i).getId());
					}
					hoja2.getRow(i+2).createCell(celdaCarreras2+1).setCellValue(carreras2.get(i).getDescripcion());
					celdaCarreras3=celdaCarreras2+3;
			  }
			  
			  for (int i = 0; i < carreras3.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaCarreras3).setCellValue(carreras3.get(i).getId());
					} else {
						hoja2.createRow(i+2).createCell(celdaCarreras3).setCellValue(carreras3.get(i).getId());
					}
					hoja2.getRow(i+2).createCell(celdaCarreras3+1).setCellValue(carreras3.get(i).getDescripcion());
					celdaCarreras4=celdaCarreras3+3;
			  }
			  
			  for (int i = 0; i < carreras4.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaCarreras4).setCellValue(carreras4.get(i).getId());
					} else {
						hoja2.createRow(i+2).createCell(celdaCarreras4).setCellValue(carreras4.get(i).getId());
					}
					hoja2.getRow(i+2).createCell(celdaCarreras4+1).setCellValue(carreras4.get(i).getDescripcion());
					celdaCarreras5=celdaCarreras4+3;
			  }
			  
			  for (int i = 0; i < carreras5.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaCarreras5).setCellValue(carreras5.get(i).getId());
					} else {
						hoja2.createRow(i+2).createCell(celdaCarreras5).setCellValue(carreras5.get(i).getId());
					}
					hoja2.getRow(i+2).createCell(celdaCarreras5+1).setCellValue(carreras5.get(i).getDescripcion());
					celdaEgresadoGradoM=celdaCarreras5+3;
			  }
			  
			  for (int i = 0; i < EgresadoGrado.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaEgresadoGradoM).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdaEgresadoGradoM).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdaEgresadoGradoM+1).setCellValue(EgresadoGrado.get(i).getDescripcion());
					celdaEgresadoGradoD=celdaEgresadoGradoM+3;
			  }
			  
			  for (int i = 0; i < EgresadoGrado.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaEgresadoGradoD).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdaEgresadoGradoD).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdaEgresadoGradoD+1).setCellValue(EgresadoGrado.get(i).getDescripcion());
					celdaColegiatura=celdaEgresadoGradoD+3;
			  }
			  
			  for (int i = 0; i < situacionEducativo.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaColegiatura).setCellValue(situacionEducativo.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdaColegiatura).setCellValue(situacionEducativo.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdaColegiatura+1).setCellValue(situacionEducativo.get(i).getDescripcion());
					celdaHabilitacion=celdaColegiatura+3;
			  }
			  
			  for (int i = 0; i < situacionEducativo.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaHabilitacion).setCellValue(i);
					} else {
						hoja2.createRow(i+2).createCell(celdaHabilitacion).setCellValue(i);
					}
					hoja2.getRow(i+2).createCell(celdaHabilitacion+1).setCellValue(situacionEducativo.get(i).getDescripcion());
					celdaConocimientoTecnico=celdaHabilitacion+3;
			  }

			  for (int i = 0; i < conocimientoTecnico.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaConocimientoTecnico).setCellValue(conocimientoTecnico.get(i).getMaeConocimientoId());
					} else {
						hoja2.createRow(i+2).createCell(celdaConocimientoTecnico).setCellValue(conocimientoTecnico.get(i).getMaeConocimientoId());
					}
					hoja2.getRow(i+2).createCell(celdaConocimientoTecnico+1).setCellValue(conocimientoTecnico.get(i).getDescripcion());
					celdacursosEspecializacion=celdaConocimientoTecnico+3;
			  }
			  
			  for (int i = 0; i < cursosEspecializacion.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdacursosEspecializacion).setCellValue(cursosEspecializacion.get(i).getMaeConocimientoId());
					} else {
						hoja2.createRow(i+2).createCell(celdacursosEspecializacion).setCellValue(cursosEspecializacion.get(i).getMaeConocimientoId());
					}
					hoja2.getRow(i+2).createCell(celdacursosEspecializacion+1).setCellValue(cursosEspecializacion.get(i).getDescripcion());
					celdaprogramaEspecializacion=celdacursosEspecializacion+3;
			  }
			  
			  for (int i = 0; i < programaEspecializacion.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaprogramaEspecializacion).setCellValue(programaEspecializacion.get(i).getMaeConocimientoId());
					} else {
						hoja2.createRow(i+2).createCell(celdaprogramaEspecializacion).setCellValue(programaEspecializacion.get(i).getMaeConocimientoId());
					}
					hoja2.getRow(i+2).createCell(celdaprogramaEspecializacion+1).setCellValue(programaEspecializacion.get(i).getDescripcion());
					celdaconocimientoOfimatica=celdaprogramaEspecializacion+3;
			  }
			  
			  for (int i = 0; i < conocimientoOfimatica.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaconocimientoOfimatica).setCellValue(conocimientoOfimatica.get(i).getMaeConocimientoId());
					} else {
						hoja2.createRow(i+2).createCell(celdaconocimientoOfimatica).setCellValue(conocimientoOfimatica.get(i).getMaeConocimientoId());
					}
					hoja2.getRow(i+2).createCell(celdaconocimientoOfimatica+1).setCellValue(conocimientoOfimatica.get(i).getDescripcion());
					celdaconocimientoIdioma=celdaconocimientoOfimatica+3;
			  }
			  
			  
			  for (int i = 0; i < conocimientoIdioma.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaconocimientoIdioma).setCellValue(conocimientoIdioma.get(i).getMaeConocimientoId());
					} else {
						hoja2.createRow(i+2).createCell(celdaconocimientoIdioma).setCellValue(conocimientoIdioma.get(i).getMaeConocimientoId());
					}
					hoja2.getRow(i+2).createCell(celdaconocimientoIdioma+1).setCellValue(conocimientoIdioma.get(i).getDescripcion());
					celdaPuestoMinimo=celdaconocimientoIdioma+3;
			  }
			  
			  for (int i = 0; i < puestoMinimo.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaPuestoMinimo).setCellValue(puestoMinimo.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdaPuestoMinimo).setCellValue(puestoMinimo.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdaPuestoMinimo+1).setCellValue(puestoMinimo.get(i).getDescripcion());
					celdatipoRequisito=celdaPuestoMinimo+3;
			  }
			  
			  for (int i = 0; i < tipoRequisito.size(); i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdatipoRequisito).setCellValue(tipoRequisito.get(i).getMaeDetalleId());
					} else {
						hoja2.createRow(i+2).createCell(celdatipoRequisito).setCellValue(tipoRequisito.get(i).getMaeDetalleId());
					}
					hoja2.getRow(i+2).createCell(celdatipoRequisito+1).setCellValue(tipoRequisito.get(i).getDescripcion());
					celdaniveIdioma=celdatipoRequisito+3;
			  }
			  
			  for (int i = 0; i < nivelIdioma.length; i++) {
					if(hoja2.getRow(i+2)!=null) {
						hoja2.getRow(i+2).createCell(celdaniveIdioma).setCellValue(i+1);
					} else {
						hoja2.createRow(i+2).createCell(celdaniveIdioma).setCellValue(i+1);
					}
					hoja2.getRow(i+2).createCell(celdaniveIdioma+1).setCellValue(nivelIdioma[i]);

			  }
			  
			 hoja2.protectSheet(variableSistema.passwordExcel);
			 workBook.setSheetHidden(1, XSSFWorkbook.SHEET_STATE_VERY_HIDDEN);
		
			  
			  ByteArrayOutputStream bos = new ByteArrayOutputStream();
			  workBook.write(bos);
			  inputStream.close();
		      bos.close();
		      byte[] byteArray = bos.toByteArray();
			  String encodedString =
		                Base64.getEncoder().withoutPadding().encodeToString(byteArray);

		  /*String filePath2 ="PLANTILLA.xlsm";
		  FileOutputStream out = new FileOutputStream(filePath2);
		  workBook.write(out);
		  inputStream.close();
		   out.close();*/
			  
		 return new RespBase<String>().ok(encodedString);
	}
	
	@Override
	public RespBase<String> descargarPlantillaOtrosRegimen(Long idEntidad) throws IOException {
		//String filePath=System.getProperty("user.dir")+variableSistema.rutaExcelWindowPerfilOtrosRegimen;
		String filePath=System.getProperty("user.home")+variableSistema.rutaExcelLinuxPerfilOtrosRegimen;
		
		 
		List<Organigrama> organos = listarOrganos(idEntidad, null);
		List<UnidadOrganica> unidaOrganica = listarUnidadOrganica(idEntidad);
		List<MaestraDetalle> nivelEducativo = listarPuestoTipo(Constantes.TBL_PER_NIV_EDU);
		List<MaestraDetalle> situacionEducativo = listarPuestoTipo(Constantes.TBL_PER_EST_NIV_EDU);
		List<MaestraDetalle> gradoEducSup = listarPuestoTipo(Constantes.TBL_PER_SIT_ACA);
		List<MaestraDetalle> EgresadoGrado = listarPuestoTipo(Constantes.TBL_PER_EST_GRA);
		List<MaestraConocimientoDTO> conocimientoTecnico =listarConocimiento(variableSistema.conocimientoTecnico);
		List<MaestraConocimientoDTO> cursosEspecializacion =listarConocimiento(variableSistema.cursosEspecializacion);
		List<MaestraConocimientoDTO> programaEspecializacion =listarConocimiento(variableSistema.programasEspecializacion);
		List<MaestraConocimientoDTO> conocimientoOfimatica =listarConocimiento(variableSistema.conocimientoOfimatica);
		List<MaestraConocimientoDTO> conocimientoIdioma =listarConocimiento(variableSistema.conocimientoIdioma);
		List<MaestraDetalle> puestoMinimo = listarPuestoTipo(Constantes.TBL_PER_NIV_MIN_PTO);
		List<MaestraDetalle> tipoRequisito = listarPuestoTipo(Constantes.TBL_PER_TIP_REQ);
		String[] nivelIdioma = {"BASICO","INTERMEDIO","AVANZADO"};
		
		 //Open file
		  FileInputStream inputStream = new FileInputStream(new File(filePath));
		  XSSFWorkbook workBook = new XSSFWorkbook(inputStream);

	 
		  //XSSFSheet hoja1 = workBook.getSheetAt(0);
		  XSSFSheet hoja2 = workBook.getSheetAt(1);
		  
		  
		  for (int i = 0; i < organos.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(0).setCellValue(organos.get(i).getOrganigramaId());
				} else {
					hoja2.createRow(i+2).createCell(0).setCellValue(organos.get(i).getOrganigramaId());
				}
				hoja2.getRow(i+2).createCell(1).setCellValue(organos.get(i).getDescripcion());
		}
		  
		  for (int i = 0; i < unidaOrganica.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(3).setCellValue(unidaOrganica.get(i).getOrganigramaId());
				} else {
					hoja2.createRow(i+2).createCell(3).setCellValue(unidaOrganica.get(i).getOrganigramaId());
				}
				hoja2.getRow(i+2).createCell(4).setCellValue(unidaOrganica.get(i).getUnidadOrganica());
		}
		  
		  int celdaNivelEducativo=6;
		  int celdagradoEducSup=0;
		  int celdaCarreras=0;
		  int celdaCarreras2=0;
		  int celdaCarreras3=0;
		  int celdaCarreras4=0;
		  int celdaCarreras5=0;
		  int celdaEgresadoGradoM=0;
		  int celdaEgresadoGradoD=0;
		  int celdaColegiatura=0;
		  int celdaHabilitacion=0;
		  int celdaConocimientoTecnico=0;
		  int celdacursosEspecializacion=0;
		  int celdaprogramaEspecializacion=0;
		  int celdaconocimientoOfimatica=0;
		  int celdaconocimientoIdioma=0;
		  int celdaPuestoMinimo=0;
		  int celdatipoRequisito=0;
		  int celdaniveIdioma = 0;
		  
		  for (int i = 0; i < nivelEducativo.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaNivelEducativo).setCellValue(nivelEducativo.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdaNivelEducativo).setCellValue(nivelEducativo.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdaNivelEducativo+1).setCellValue(nivelEducativo.get(i).getDescripcion());
				celdagradoEducSup=celdaNivelEducativo+3;
		  }
		  
		  List<CarreraProfesional> carreras = new ArrayList<CarreraProfesional>();
		  List<CarreraProfesional> carreras2 = new ArrayList<CarreraProfesional>();
		  List<CarreraProfesional> carreras3 = new ArrayList<CarreraProfesional>();
		  List<CarreraProfesional> carreras4 = new ArrayList<CarreraProfesional>();
		  List<CarreraProfesional> carreras5 = new ArrayList<CarreraProfesional>();
		  for (int i = 0; i < gradoEducSup.size(); i++) {
			  	switch(i) {
			  	case 0:
			  		carreras = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	case 1:
			  		carreras2 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	case 2:
			  		carreras3 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	case 3:
			  		carreras4 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	case 4:
			  		carreras5 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	}
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdagradoEducSup).setCellValue(gradoEducSup.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdagradoEducSup).setCellValue(gradoEducSup.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdagradoEducSup+1).setCellValue(gradoEducSup.get(i).getDescripcion());
				 celdaCarreras=celdagradoEducSup+3;
		  }
		  
		  for (int i = 0; i < carreras.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras).setCellValue(carreras.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras).setCellValue(carreras.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras+1).setCellValue(carreras.get(i).getDescripcion());
				celdaCarreras2=celdaCarreras+3;
		  }
		  
		  for (int i = 0; i < carreras2.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras2).setCellValue(carreras2.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras2).setCellValue(carreras2.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras2+1).setCellValue(carreras2.get(i).getDescripcion());
				celdaCarreras3=celdaCarreras2+3;
		  }
		  
		  for (int i = 0; i < carreras3.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras3).setCellValue(carreras3.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras3).setCellValue(carreras3.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras3+1).setCellValue(carreras3.get(i).getDescripcion());
				celdaCarreras4=celdaCarreras3+3;
		  }
		  
		  for (int i = 0; i < carreras4.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras4).setCellValue(carreras4.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras4).setCellValue(carreras4.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras4+1).setCellValue(carreras4.get(i).getDescripcion());
				celdaCarreras5=celdaCarreras4+3;
		  }
		  
		  for (int i = 0; i < carreras5.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras5).setCellValue(carreras5.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras5).setCellValue(carreras5.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras5+1).setCellValue(carreras5.get(i).getDescripcion());
				celdaEgresadoGradoM=celdaCarreras5+3;
		  }
		  
		  for (int i = 0; i < EgresadoGrado.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaEgresadoGradoM).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdaEgresadoGradoM).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdaEgresadoGradoM+1).setCellValue(EgresadoGrado.get(i).getDescripcion());
				celdaEgresadoGradoD=celdaEgresadoGradoM+3;
		  }
		  
		  for (int i = 0; i < EgresadoGrado.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaEgresadoGradoD).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdaEgresadoGradoD).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdaEgresadoGradoD+1).setCellValue(EgresadoGrado.get(i).getDescripcion());
				celdaColegiatura=celdaEgresadoGradoD+3;
		  }
		  
		  for (int i = 0; i < situacionEducativo.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaColegiatura).setCellValue(situacionEducativo.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdaColegiatura).setCellValue(situacionEducativo.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdaColegiatura+1).setCellValue(situacionEducativo.get(i).getDescripcion());
				celdaHabilitacion=celdaColegiatura+3;
		  }
		  
		  for (int i = 0; i < situacionEducativo.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaHabilitacion).setCellValue(i);
				} else {
					hoja2.createRow(i+2).createCell(celdaHabilitacion).setCellValue(i);
				}
				hoja2.getRow(i+2).createCell(celdaHabilitacion+1).setCellValue(situacionEducativo.get(i).getDescripcion());
				celdaConocimientoTecnico=celdaHabilitacion+3;
		  }
		  for (int i = 0; i < conocimientoTecnico.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaConocimientoTecnico).setCellValue(conocimientoTecnico.get(i).getMaeConocimientoId());
				} else {
					hoja2.createRow(i+2).createCell(celdaConocimientoTecnico).setCellValue(conocimientoTecnico.get(i).getMaeConocimientoId());
				}
				hoja2.getRow(i+2).createCell(celdaConocimientoTecnico+1).setCellValue(conocimientoTecnico.get(i).getDescripcion());
				celdacursosEspecializacion=celdaConocimientoTecnico+3;
		  }
		  
		  for (int i = 0; i < cursosEspecializacion.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdacursosEspecializacion).setCellValue(cursosEspecializacion.get(i).getMaeConocimientoId());
				} else {
					hoja2.createRow(i+2).createCell(celdacursosEspecializacion).setCellValue(cursosEspecializacion.get(i).getMaeConocimientoId());
				}
				hoja2.getRow(i+2).createCell(celdacursosEspecializacion+1).setCellValue(cursosEspecializacion.get(i).getDescripcion());
				celdaprogramaEspecializacion=celdacursosEspecializacion+3;
		  }
		  
		  for (int i = 0; i < programaEspecializacion.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaprogramaEspecializacion).setCellValue(programaEspecializacion.get(i).getMaeConocimientoId());
				} else {
					hoja2.createRow(i+2).createCell(celdaprogramaEspecializacion).setCellValue(programaEspecializacion.get(i).getMaeConocimientoId());
				}
				hoja2.getRow(i+2).createCell(celdaprogramaEspecializacion+1).setCellValue(programaEspecializacion.get(i).getDescripcion());
				celdaconocimientoOfimatica=celdaprogramaEspecializacion+3;
		  }
		  
		  for (int i = 0; i < conocimientoOfimatica.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaconocimientoOfimatica).setCellValue(conocimientoOfimatica.get(i).getMaeConocimientoId());
				} else {
					hoja2.createRow(i+2).createCell(celdaconocimientoOfimatica).setCellValue(conocimientoOfimatica.get(i).getMaeConocimientoId());
				}
				hoja2.getRow(i+2).createCell(celdaconocimientoOfimatica+1).setCellValue(conocimientoOfimatica.get(i).getDescripcion());
				celdaconocimientoIdioma=celdaconocimientoOfimatica+3;
		  }
		  
		  
		  for (int i = 0; i < conocimientoIdioma.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaconocimientoIdioma).setCellValue(conocimientoIdioma.get(i).getMaeConocimientoId());
				} else {
					hoja2.createRow(i+2).createCell(celdaconocimientoIdioma).setCellValue(conocimientoIdioma.get(i).getMaeConocimientoId());
				}
				hoja2.getRow(i+2).createCell(celdaconocimientoIdioma+1).setCellValue(conocimientoIdioma.get(i).getDescripcion());
				celdaPuestoMinimo=celdaconocimientoIdioma+3;
		  }
		  
		  for (int i = 0; i < puestoMinimo.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaPuestoMinimo).setCellValue(puestoMinimo.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdaPuestoMinimo).setCellValue(puestoMinimo.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdaPuestoMinimo+1).setCellValue(puestoMinimo.get(i).getDescripcion());
				celdatipoRequisito=celdaPuestoMinimo+3;
		  }
		  
		  for (int i = 0; i < tipoRequisito.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdatipoRequisito).setCellValue(tipoRequisito.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdatipoRequisito).setCellValue(tipoRequisito.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdatipoRequisito+1).setCellValue(tipoRequisito.get(i).getDescripcion());
				celdaniveIdioma=celdatipoRequisito+3;
		  }
		  
		  for (int i = 0; i < nivelIdioma.length; i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaniveIdioma).setCellValue(i+1);
				} else {
					hoja2.createRow(i+2).createCell(celdaniveIdioma).setCellValue(i+1);
				}
				hoja2.getRow(i+2).createCell(celdaniveIdioma+1).setCellValue(nivelIdioma[i]);

		  }

			 hoja2.protectSheet(variableSistema.passwordExcel);
			 workBook.setSheetHidden(1, XSSFWorkbook.SHEET_STATE_VERY_HIDDEN);
			 
		  ByteArrayOutputStream bos = new ByteArrayOutputStream();
		  workBook.write(bos);
		  inputStream.close();
	      bos.close();
	      byte[] byteArray = bos.toByteArray();
		  String encodedString =
	                Base64.getEncoder().withoutPadding().encodeToString(byteArray);

	  /*String filePath2 ="PLANTILLA.xlsm";
	  FileOutputStream out = new FileOutputStream(filePath2);
	  workBook.write(out);
	  inputStream.close();
	   out.close();*/
		   return new RespBase<String>().ok(encodedString);
	}
	

	
	public List<Organigrama> listarOrganos(Long entidadId, Long tipo){
		return entidadClient.obtieneOrgano(entidadId, tipo).getPayload().getListaOrganigrama();
	}
	
	public List<UnidadOrganica> listarUnidadOrganica(Long entidadId){
		return entidadClient.obtieneUnidadOrganica(entidadId).getPayload().getListaUnidadOrganica();
	}
	
	public List<PerfilGrupo> listarGrupoDeServidoresCiviles() {
		return perfilGrupoService.findAllPerfilGrupo(null).getPayload().getItems();	
	}
	
	public List<PerfilGrupo> listarFamiliasRol(Long id) {
		return perfilGrupoService.findAllPerfilGrupo(id).getPayload().getItems();	
	}
	
	public List<ParametrosDTO> listarNivel(){
		return maestraClient.rutaFileServerEntidad(Constantes.NIVEL_ORGANO, "").getPayload().getItems();
	}
	
	public List<MaestraDetalle> listarPuestoTipo(String codCabecera){
		return maestraDetalleService.obtenerMaestraDetalle(null, null, null, null, codCabecera, null).getPayload().getMaestraDetalles();
	}
	
	public List<CarreraProfesional> listarCarreras(Long id) {
		return capacitacionService.obtenerCarreras(id).getPayload().getItems();
	}
	
	public List<MaestraConocimientoDTO> listarConocimiento(String valor) {
		Map<String, Object> parametroMap = new HashMap<>();
		parametroMap.put("tipoConocimiento", Long.valueOf(valor));
		parametroMap.put("categoriaConocimiento", null);
		parametroMap.put("descripcion", null);
		
		return maestraConocimientoService.obtenerMaestraConocimiento(parametroMap).getPayload().getLstMaestraConocimiento();
	}

	@Override
	public RespBase<String> descargarPlantillaPracticas(Long idEntidad) throws IOException {
		//String filePath=System.getProperty("user.dir")+variableSistema.rutaExcelWindowPerfilPracticas;
		String filePath=System.getProperty("user.home")+variableSistema.rutaExcelLinuxPerfilPracticas;
		
		List<Organigrama> organos = listarOrganos(idEntidad, null);
		List<UnidadOrganica> unidaOrganica = listarUnidadOrganica(idEntidad);
		List<MaestraDetalle> tipoPracticas = listarPuestoTipo(Constantes.TBL_PER_CON_PRA);
		List<MaestraDetalle> nivelEducativo = listarPuestoTipo(Constantes.TBL_PER_NIV_EDU);
		List<MaestraDetalle> gradoEducSup = listarPuestoTipo(Constantes.TBL_PER_SIT_ACA);
		List<MaestraConocimientoDTO> conocimientoTecnico =listarConocimiento(variableSistema.conocimientoTecnico);
		List<MaestraDetalle> situacionEducativo = listarPuestoTipo(Constantes.TBL_PER_EST_NIV_EDU);
		List<MaestraDetalle> EgresadoGrado = listarPuestoTipo(Constantes.TBL_PER_EST_GRA);
		
		 //Open file
		  FileInputStream inputStream = new FileInputStream(new File(filePath));
		  XSSFWorkbook workBook = new XSSFWorkbook(inputStream);

	 
		  //XSSFSheet hoja1 = workBook.getSheetAt(0);
		  XSSFSheet hoja2 = workBook.getSheetAt(1);
		  
		  for (int i = 0; i < organos.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(0).setCellValue(organos.get(i).getOrganigramaId());
				} else {
					hoja2.createRow(i+2).createCell(0).setCellValue(organos.get(i).getOrganigramaId());
				}
				hoja2.getRow(i+2).createCell(1).setCellValue(organos.get(i).getDescripcion());
		}
		  
		  for (int i = 0; i < unidaOrganica.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(3).setCellValue(unidaOrganica.get(i).getOrganigramaId());
				} else {
					hoja2.createRow(i+2).createCell(3).setCellValue(unidaOrganica.get(i).getOrganigramaId());
				}
				hoja2.getRow(i+2).createCell(4).setCellValue(unidaOrganica.get(i).getUnidadOrganica());
		}
		  
		  for (int i = 0; i < tipoPracticas.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(6).setCellValue(tipoPracticas.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(6).setCellValue(tipoPracticas.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(7).setCellValue(tipoPracticas.get(i).getDescripcion());
		}
		  
		  
		  int celdaNivelEducativo=9;
		  int celdagradoEducSup=0;
		  int celdaCarreras=0;
		  int celdaCarreras2=0;
		  int celdaCarreras3=0;
		  int celdaCarreras4=0;
		  int celdaCarreras5=0;
		  int celdaConocimientoTecnico=0;
		  int celdaEgresadoGradoD=0;
		  int celdaColegiatura=0;
		  
		  for (int i = 0; i < nivelEducativo.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaNivelEducativo).setCellValue(nivelEducativo.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdaNivelEducativo).setCellValue(nivelEducativo.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdaNivelEducativo+1).setCellValue(nivelEducativo.get(i).getDescripcion());
				celdaEgresadoGradoD=celdaNivelEducativo+3;
		  }
		  
		  for (int i = 0; i < EgresadoGrado.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaEgresadoGradoD).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdaEgresadoGradoD).setCellValue(EgresadoGrado.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdaEgresadoGradoD+1).setCellValue(EgresadoGrado.get(i).getDescripcion());
				celdaColegiatura=celdaEgresadoGradoD+3;
		  }
		  
		  for (int i = 0; i < situacionEducativo.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaColegiatura).setCellValue(situacionEducativo.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdaColegiatura).setCellValue(situacionEducativo.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdaColegiatura+1).setCellValue(situacionEducativo.get(i).getDescripcion());
				celdagradoEducSup=celdaColegiatura+3;
		  }
		  
		  List<CarreraProfesional> carreras = new ArrayList<CarreraProfesional>();
		  List<CarreraProfesional> carreras2 = new ArrayList<CarreraProfesional>();
		  List<CarreraProfesional> carreras3 = new ArrayList<CarreraProfesional>();
		  List<CarreraProfesional> carreras4 = new ArrayList<CarreraProfesional>();
		  List<CarreraProfesional> carreras5 = new ArrayList<CarreraProfesional>();
		  for (int i = 0; i < gradoEducSup.size(); i++) {
			  	switch(i) {
			  	case 0:
			  		carreras = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	case 1:
			  		carreras2 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	case 2:
			  		carreras3 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	case 3:
			  		carreras4 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	case 4:
			  		carreras5 = listarCarreras(gradoEducSup.get(i).getMaeDetalleId());
			  		break;
			  	}
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdagradoEducSup).setCellValue(gradoEducSup.get(i).getMaeDetalleId());
				} else {
					hoja2.createRow(i+2).createCell(celdagradoEducSup).setCellValue(gradoEducSup.get(i).getMaeDetalleId());
				}
				hoja2.getRow(i+2).createCell(celdagradoEducSup+1).setCellValue(gradoEducSup.get(i).getDescripcion());
				 celdaCarreras=celdagradoEducSup+3;
		  }
		  
		  for (int i = 0; i < carreras.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras).setCellValue(carreras.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras).setCellValue(carreras.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras+1).setCellValue(carreras.get(i).getDescripcion());
				celdaCarreras2=celdaCarreras+3;
		  }
		  
		  for (int i = 0; i < carreras2.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras2).setCellValue(carreras2.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras2).setCellValue(carreras2.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras2+1).setCellValue(carreras2.get(i).getDescripcion());
				celdaCarreras3=celdaCarreras2+3;
		  }
		  
		  for (int i = 0; i < carreras3.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras3).setCellValue(carreras3.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras3).setCellValue(carreras3.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras3+1).setCellValue(carreras3.get(i).getDescripcion());
				celdaCarreras4=celdaCarreras3+3;
		  }
		  
		  for (int i = 0; i < carreras4.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras4).setCellValue(carreras4.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras4).setCellValue(carreras4.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras4+1).setCellValue(carreras4.get(i).getDescripcion());
				celdaCarreras5=celdaCarreras4+3;
		  }
		  
		  for (int i = 0; i < carreras5.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaCarreras5).setCellValue(carreras5.get(i).getId());
				} else {
					hoja2.createRow(i+2).createCell(celdaCarreras5).setCellValue(carreras5.get(i).getId());
				}
				hoja2.getRow(i+2).createCell(celdaCarreras5+1).setCellValue(carreras5.get(i).getDescripcion());
				celdaConocimientoTecnico=celdaCarreras5+3;
		  }
		  
		  for (int i = 0; i < conocimientoTecnico.size(); i++) {
				if(hoja2.getRow(i+2)!=null) {
					hoja2.getRow(i+2).createCell(celdaConocimientoTecnico).setCellValue(conocimientoTecnico.get(i).getMaeConocimientoId());
				} else {
					hoja2.createRow(i+2).createCell(celdaConocimientoTecnico).setCellValue(conocimientoTecnico.get(i).getMaeConocimientoId());
				}
				hoja2.getRow(i+2).createCell(celdaConocimientoTecnico+1).setCellValue(conocimientoTecnico.get(i).getDescripcion());
		 
		  }
		  
			 hoja2.protectSheet(variableSistema.passwordExcel);
			 workBook.setSheetHidden(1, XSSFWorkbook.SHEET_STATE_VERY_HIDDEN);
			 
		  ByteArrayOutputStream bos = new ByteArrayOutputStream();
		  workBook.write(bos);
		  inputStream.close();
	      bos.close();
	      byte[] byteArray = bos.toByteArray();
		  String encodedString =
	                Base64.getEncoder().withoutPadding().encodeToString(byteArray);

	  /*String filePath2 ="PLANTILLA.xlsm";
	  FileOutputStream out = new FileOutputStream(filePath2);
	  workBook.write(out);
	  inputStream.close();
	   out.close();*/
		   return new RespBase<String>().ok(encodedString);
	}


}
