package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class ServidorCivilDTO {

	private Long perfilServidorId;
	
	private Long servidorCivilId;
	
	private String tipoRelacion;

	private String estado;

}
