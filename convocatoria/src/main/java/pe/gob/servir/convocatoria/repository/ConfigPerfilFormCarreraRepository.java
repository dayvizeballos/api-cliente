package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.gob.servir.convocatoria.model.TblConfPerfFormacCarrera;

public interface ConfigPerfilFormCarreraRepository extends JpaRepository<TblConfPerfFormacCarrera, Long>{

}
