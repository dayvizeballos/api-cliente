package pe.gob.servir.convocatoria.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.servir.convocatoria.response.RespCargaMasiva;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface CargaMasivaPerfilService {

    RespBase<RespCargaMasiva> uploadExcel(String model, MultipartFile[] files, MyJsonWebToken token) throws JsonProcessingException;

}
