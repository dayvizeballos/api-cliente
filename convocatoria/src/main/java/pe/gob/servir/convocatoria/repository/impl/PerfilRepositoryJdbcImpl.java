package pe.gob.servir.convocatoria.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.repository.PerfilRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.ObtenerPerfilConfigDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerPerfilDTO;
import pe.gob.servir.convocatoria.request.dto.VacanteDTO;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;


@Repository
public class PerfilRepositoryJdbcImpl implements PerfilRepositoryJdbc {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PerfilRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

     private static final String SQL_FIND_ALL_PERFIL = "SELECT perfil_id as perfilId , upper(nombre_puesto) as nombrePuesto , entidad_id as entidadId , regimen_laboral_id as regimenLaboralId, tmd.cod_prog as codProg, TMD.descripcion as regimenLaboral,\n" +
            "upper(puesto_codigo) as puestoCodigo , organo_id as organoId , sigla_organo as siglaOrgano , unidad_organica_id as unidadOrganicaId,\n" +
            "unidad_organica as unidadOrganica , nro_posiciones_puesto as nroPosicionesPuesto , TP.estado_registro as estadoId, case when TP.ESTADO_REGISTRO = '1' then 'ACTIVO' else 'INACTIVO' end as  estado,TP.ind_origen_perfil origenPerfil, TP.ind_estado_revision estadoRevision, \n" +
            "TP.estado_func estadoFunc, TP.estado_forma estadoForm, TP.estado_exp estadoExp from " +
            "sch_convocatoria.tbl_perfil TP  join sch_convocatoria.tbl_mae_detalle tmd on TP.regimen_laboral_id = TMD.mae_detalle_id  where 1 = 1 ";

     private static final String SQL_CORRELATIVO_COD_PUESTO = "select lpad((COUNT(1)+1::numeric(9))::text,9,'0') from sch_convocatoria.tbl_perfil where entidad_id = :entidadId  and regimen_laboral_id = :regimenId ";
     
     private static final String SQL_FIND_lISTAR_VACANTES = "SELECT BP.BASE_PERFIL_ID AS basePerfilId, BP.BASE_ID AS baseId, BP.PERFIL_ID AS perfilId, P.NOMBRE_PUESTO AS perfilNombre, P.PUESTO_CODIGO AS codigoPuesto, BP.NRO_VACANTE AS nroVacante, BP.SEDE_ID AS sedeId, BP.SEDE_DIRECCION AS sedeDireccion, BP.JORNADA_TRABAJO AS jornadaLaboral, BP.REMUNERACION AS remuneracion, 'S/ '|| BP.REMUNERACION AS remuneracionConMoneda\r\n" + 
      		"FROM sch_base.TBL_BASE_PERFIL BP\r\n" + 
      		"INNER JOIN sch_convocatoria.TBL_PERFIL P ON P.PERFIL_ID = BP.PERFIL_ID\r\n" + 
      		"WHERE P.ESTADO_REGISTRO='1' AND BP.ESTADO_REGISTRO='1' AND BP.BASE_ID = :baseId ";
          
     private static final String SQL_COUNT_VACANTES_DISPONIBLES = "SELECT DC.NRO_VACANTES - (SELECT COALESCE(SUM(BP.NRO_VACANTE),0) AS totalVacanteUsadas\r\n" + 
     		"      		FROM sch_base.TBL_BASE_PERFIL BP INNER JOIN sch_convocatoria.TBL_PERFIL P ON P.PERFIL_ID = BP.PERFIL_ID\r\n" + 
     		"      		WHERE P.ESTADO_REGISTRO='1' AND BP.ESTADO_REGISTRO='1' AND BP.BASE_ID = :baseId) AS totalVacantes\r\n" + 
     		"      	FROM sch_base.TBL_BASE_DATOS_CONCURSO DC\r\n" + 
     		"      	INNER JOIN sch_base.TBL_BASE B ON B.BASE_ID = DC.BASE_ID\r\n" + 
     		"      	WHERE DC.ESTADO_REGISTRO='1' AND B.ESTADO_REGISTRO='1' AND DC.BASE_ID = :baseId";
  
    @Override
    public List<ObtenerPerfilDTO> findAllPerfil(String fechaIni, String fechaFin, Long entidad, String codigoPuesto,
                                                String nombrePerfil, Long regimenId, Long organoId, Long unidadOrganicaId,
                                                String origenPerfil , Long estadoRevision , String estado) {
        List<ObtenerPerfilDTO> list = new ArrayList<>();

        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_ALL_PERFIL);
            if (!Util.isEmpty(entidad)) sql.append(" and entidad_id =:entidad ");
            if (!Util.isEmpty(fechaIni) && !Util.isEmpty(fechaFin)) sql.append(" and to_char(TP.fecha_creacion , 'yyyy-mm-dd')  >= :fechaIni and  to_char(TP.fecha_creacion , 'yyyy-mm-dd')  <= :fechaFin ");
            if (!Util.isEmpty(codigoPuesto)) sql.append(" and upper(puesto_codigo) like upper('%'||:codigoPuesto||'%')");
            if (!Util.isEmpty(nombrePerfil)) sql.append(" and upper(nombre_puesto) like upper('%'||:nombrePerfil||'%')");
            if (!Util.isEmpty(regimenId)) sql.append(" and regimen_laboral_id =:regimenId");
            if (!Util.isEmpty(organoId)) sql.append(" and organo_id =:organoId");
            if (!Util.isEmpty(unidadOrganicaId)) sql.append(" and unidad_organica_id =:unidadOrganicaId");
            if (!Util.isEmpty(estado)) sql.append(" and TP.estado_registro =:estado");
            if (!Util.isEmpty(origenPerfil)) sql.append(" and TP.ind_origen_perfil =:origenPerfil");
            if (!Util.isEmpty(estadoRevision)) sql.append(" and TP.ind_estado_revision =:estadoRevision");
            sql.append(" order by perfil_id desc ");

            if (!Util.isEmpty(entidad)) objectParam.put("entidad",entidad);
            if (!Util.isEmpty(fechaIni) && !Util.isEmpty(fechaFin)) objectParam.put("fechaIni",fechaIni) ; objectParam.put("fechaFin",fechaFin);
            if (!Util.isEmpty(codigoPuesto)) objectParam.put("codigoPuesto",codigoPuesto.trim());
            if (!Util.isEmpty(nombrePerfil)) objectParam.put("nombrePerfil",nombrePerfil.trim());
            if (!Util.isEmpty(regimenId)) objectParam.put("regimenId",regimenId);
            if (!Util.isEmpty(organoId)) objectParam.put("organoId",organoId);
            if (!Util.isEmpty(unidadOrganicaId)) objectParam.put("unidadOrganicaId",unidadOrganicaId);
            if (!Util.isEmpty(estado)) objectParam.put("estado",estado.trim());
            if (!Util.isEmpty(origenPerfil)) objectParam.put("origenPerfil",origenPerfil.trim());
            if (!Util.isEmpty(estadoRevision)) objectParam.put("estadoRevision",estadoRevision);

            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ObtenerPerfilDTO.class));
        }catch (Exception e){
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
        return list;
    }


	@Override
	public String findCorrelativoCodigoPuesto(Long regimenId, Long entidadId) {
		
		Map<String, Object> objectParam = new HashMap<>();
		if (!Util.isEmpty(regimenId)) objectParam.put("regimenId",regimenId);
		if (!Util.isEmpty(entidadId)) objectParam.put("entidadId",entidadId);
		String correlativo = this.namedParameterJdbcTemplate.queryForObject(SQL_CORRELATIVO_COD_PUESTO, objectParam, String.class);
		return correlativo;
	}

	
    @Override
    public List<VacanteDTO> listarVacantes(Long baseId) {
        List<VacanteDTO> list = new ArrayList<>();
        try {
    		Map<String, Object> objectParam = new HashMap<>();
    		if (!Util.isEmpty(baseId)) objectParam.put("baseId",baseId);
            StringBuilder sql = new StringBuilder();
            sql.append(SQL_FIND_lISTAR_VACANTES);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(VacanteDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public Long calcularVacantesDisponiblesPorBase(Long baseId) {
    	Long vacantesDisponibles = 0L;
        try {
    		Map<String, Object> objectParam = new HashMap<>();
    		if (!Util.isEmpty(baseId)) objectParam.put("baseId",baseId);
            StringBuilder sql = new StringBuilder();
            sql.append(SQL_COUNT_VACANTES_DISPONIBLES);
            vacantesDisponibles = this.namedParameterJdbcTemplate.queryForObject(sql.toString(), objectParam, Long.class);
            return vacantesDisponibles;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return vacantesDisponibles;
        }
    }
    
  
    private static final String SQL_FIND_PERFILES_FILTER = "SELECT DISTINCT tp.perfil_id     AS perfilId, \r\n" +
            "                tp.nombre_puesto AS nombrePerfil, \r\n" +
            "                tp.rol_id as idRol , \r\n" +
            "                pg.descripcion        AS rol, \r\n" +
            "                tp.regimen_laboral_id AS regimenId, \r\n" +
            "                sch_convocatoria.fnc_get_desc_mae_detalle(tp.regimen_laboral_id) as regimen, \r\n" +
            "                tp.entidad_id, \r\n" +
            "                tp.estado_rm AS estadoRm, \r\n" +
            "                sch_convocatoria.fnc_get_id_mae_detalle('TIP_ESTADO_REQ_MINIMO', tp.estado_rm) as estadoRmId, \r\n" +
            "                tp.estado_ec                                                                       AS estadoEc, \r\n" +
            "                sch_convocatoria.fnc_get_id_mae_detalle('TIP_ESTADO_EVA_CURRICULAR', tp.estado_ec) as estadoEcId, \r\n" +
            "                tp.estado_registro        AS estadoRegistro, \r\n" +
            "                ex.experiencia_perfil_id  AS experienciaId, \r\n" +
            "                bp.base_id, \r\n" +
            "                cov.codigo_convocatoria \r\n" +
            "FROM sch_convocatoria.tbl_perfil tp \r\n" +
            "         LEFT JOIN sch_convocatoria.tbl_perfil_grupo pg ON tp.rol_id = pg.perfil_grupo_id \r\n" +
            "         LEFT JOIN sch_convocatoria.tbl_perfil_experiencia ex \r\n" +
            "                   ON tp.perfil_id = ex.perfil_id AND ex.estado_registro = '1' \r\n" +
            "        INNER JOIN sch_base.tbl_base_perfil bp on bp.perfil_id = tp.perfil_id \r\n" +
            "                   and bp.estado_registro = '1' \r\n" +
            "        LEFT JOIN sch_convocatoria.tbl_convocatoria cov on cov.base_id = bp.base_id \r\n" +
            "WHERE tp.estado_registro = '1'";


	@Override
	public List<ObtenerPerfilConfigDTO> buscarPerfilByFilter(Map<String, Object> parametroMap) {
		 List<ObtenerPerfilConfigDTO> list = new ArrayList<>();
	        try {
	            StringBuilder sql = new StringBuilder();
	            Map<String, Object> objectParam = new HashMap<>();
	            sql.append(SQL_FIND_PERFILES_FILTER);

	            if (!Util.isEmpty(parametroMap.get("entidadId"))) sql.append(" and tp.entidad_id =:entidadId ");
	            if (!Util.isEmpty(parametroMap.get("regimenId"))) sql.append(" and tp.regimen_laboral_id =:regimenId ");
	            if (!Util.isEmpty(parametroMap.get("nombrePerfil"))) sql.append(" and upper(tp.nombre_puesto) like upper('%'||:nombrePerfil||'%') ");
	            if (!Util.isEmpty(parametroMap.get("rolId"))) sql.append(" and tp.rol_id =:rolId ");
	            if (!Util.isEmpty(parametroMap.get("estadoRmId"))) sql.append(" and sch_convocatoria.fnc_get_id_mae_detalle('TIP_ESTADO_REQ_MINIMO', tp.estado_rm) =:estadoRm ");
	            if (!Util.isEmpty(parametroMap.get("estadoEcId"))) sql.append(" and sch_convocatoria.fnc_get_id_mae_detalle('TIP_ESTADO_EVA_CURRICULAR', tp.estado_ec) =:estadoEc ");
	            
	            if (!Util.isEmpty(parametroMap.get("entidadId"))) objectParam.put("entidadId",parametroMap.get("entidadId"));
	            if (!Util.isEmpty(parametroMap.get("regimenId"))) objectParam.put("regimenId",parametroMap.get("regimenId"));
	            if (!Util.isEmpty(parametroMap.get("nombrePerfil"))) objectParam.put("nombrePerfil",parametroMap.get("nombrePerfil"));
	            if (!Util.isEmpty(parametroMap.get("rolId"))) objectParam.put("rolId",parametroMap.get("rolId"));
	            if (!Util.isEmpty(parametroMap.get("estadoRmId"))) objectParam.put("estadoRm",parametroMap.get("estadoRmId"));
	            if (!Util.isEmpty(parametroMap.get("estadoEcId"))) objectParam.put("estadoEc",parametroMap.get("estadoEcId"));


	            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ObtenerPerfilConfigDTO.class));
	            
	            return list;
	        } catch (Exception e) {
	            LogManager.getLogger(this.getClass()).error(e);
	            return list;
	        }
	}

	
}
