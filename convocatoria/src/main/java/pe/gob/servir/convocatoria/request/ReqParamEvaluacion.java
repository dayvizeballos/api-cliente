package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@Getter
@Setter
public class ReqParamEvaluacion {

	@NotNull(message = Constantes.CAMPO + " entidadId " + Constantes.ES_OBLIGATORIO)
	private Long entidadId;
	
	@NotNull(message = Constantes.CAMPO + " regimenId " + Constantes.ES_OBLIGATORIO)
	private Long regimenId;
	
	@NotNull(message = Constantes.CAMPO + " modalidadId " + Constantes.ES_OBLIGATORIO)
	private Long modalidadId;
	
	@NotNull(message = Constantes.CAMPO + " tipoId " + Constantes.ES_OBLIGATORIO)
	private Long tipoId;
	
}
