package pe.gob.servir.convocatoria.request.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BasePerfilBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long basePerfilId;
    private Long perfilId;
    private Long baseId;
    private Long regimenId;
    private Long modalidadId;
    private Long tipoId;
    private Long entidadId;

}
