package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.ConfiguracionMaestra;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.MaestraCebecera;
import pe.gob.servir.convocatoria.model.MaestraDetalleEntidad;
import pe.gob.servir.convocatoria.repository.ConfiguracionMaestraRepository;
import pe.gob.servir.convocatoria.repository.MaestraCabeceraRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleEntidadRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.request.ReqAsigarMaeDetalleEntidad;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraDetalleEntidad;
import pe.gob.servir.convocatoria.response.RespAsignarMaeDetalleEntidad;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespComboCabecera;
import pe.gob.servir.convocatoria.response.RespMaestraDetalleEntidad;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.MaestraDetalleEntidadService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;

@Service
public class MaestraDetalleEntidadServiceImpl implements MaestraDetalleEntidadService {

	
	@Autowired
	private ConfiguracionMaestraRepository configMaestraRepository;
	
	@Autowired
	private MaestraCabeceraRepository maestraCabeceraRepository;
	
	@Autowired
	private MaestraDetalleRepository maestraDetalleRepository;
	
	@Autowired
	private MaestraDetalleEntidadRepository detalleEntidadRepository;
	
	
	
	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<RespAsignarMaeDetalleEntidad> asignarMaestraDetalle(ReqBase<ReqAsigarMaeDetalleEntidad> request,
			MyJsonWebToken token, Long maeConfigMaestraId) {
		
		RespBase<RespAsignarMaeDetalleEntidad> response = new RespBase<>();
		
		ConfiguracionMaestra configuracionMaestra = null;
		
		if(maeConfigMaestraId != null) {
			Optional<ConfiguracionMaestra> configMaestraFind = configMaestraRepository.findById(maeConfigMaestraId);
			if(configMaestraFind.isPresent()) {
				configuracionMaestra = configMaestraFind.get();
			}else {
				response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
						"No Existe el maeConfigMaestraId Ingresado");
				return response;
			}
			configuracionMaestra.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), token.getUsuario().getUsuario(),
					Instant.now());
		}else {
			ConfiguracionMaestra confMaestraFilter =  new ConfiguracionMaestra();
			confMaestraFilter.setEntidadId(request.getPayload().getEntidadId());
			confMaestraFilter.setMaeCabeceraId(request.getPayload().getMaeCabeceraId());
			confMaestraFilter.setMaeDetalleId(request.getPayload().getMaeDetalleId());
			Example<ConfiguracionMaestra> example = Example.of(confMaestraFilter);
			
			List<ConfiguracionMaestra> ltaConfMaestraFilter = configMaestraRepository.findAll(example);
			if(!ltaConfMaestraFilter.isEmpty()) {
				response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "Ya existe una maestra asignada");
				return response; 
			}
			
			configuracionMaestra = new ConfiguracionMaestra();
			configuracionMaestra.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
		}
		
		configuracionMaestra.setEntidadId(request.getPayload().getEntidadId());
		configuracionMaestra.setMaeCabeceraId(request.getPayload().getMaeCabeceraId());
		configuracionMaestra.setMaeDetalleId(request.getPayload().getMaeDetalleId());
		
		configMaestraRepository.save(configuracionMaestra);
		RespAsignarMaeDetalleEntidad payload = new RespAsignarMaeDetalleEntidad();
		payload.setConfiguracionMaestra(configuracionMaestra);
		
		return new RespBase<RespAsignarMaeDetalleEntidad>().ok(payload);
	}


	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<Object> habilitarMaestraDetalleEntidad(MyJsonWebToken token, Long configMaestraId, String estado) {

		RespBase<Object> response = new RespBase<>();
		Optional<ConfiguracionMaestra> configMaestraFind = configMaestraRepository.findById(configMaestraId);
		if (configMaestraFind.isPresent()) {
			ConfiguracionMaestra configuracionMaestra = configMaestraFind.get();

			if (configuracionMaestra.getMaeDetalleEntidadId() != null) {

				Optional<MaestraDetalleEntidad> maestraDetalleFind = detalleEntidadRepository
						.findById(configuracionMaestra.getMaeDetalleEntidadId());
				if (maestraDetalleFind.isPresent()) {
					MaestraDetalleEntidad detalleEntidad = maestraDetalleFind.get();

					if (estado.equals(EstadoRegistro.INACTIVO.getCodigo())) {
						detalleEntidad.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(),
								token.getUsuario().getUsuario(), Instant.now());
					} else if (estado.equals(EstadoRegistro.ACTIVO.getCodigo())) {
						detalleEntidad.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(),
								token.getUsuario().getUsuario(), Instant.now());
					} else {
						response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
								"El valor de estado Ingresado es incorrecto");
						return response;
					}

					detalleEntidadRepository.save(detalleEntidad);
				}

			}

			if (estado.equals(EstadoRegistro.INACTIVO.getCodigo())) {
				configuracionMaestra.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(),
						token.getUsuario().getUsuario(), Instant.now());
			} else if (estado.equals(EstadoRegistro.ACTIVO.getCodigo())) {
				configuracionMaestra.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), token.getUsuario().getUsuario(),
						Instant.now());
			} else {
				response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
						"El valor de estado Ingresado es incorrecto");
				return response;
			}
			configMaestraRepository.save(configuracionMaestra);
			return new RespBase<Object>().ok(configuracionMaestra);
		} else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, Constantes.MENSAJE_CONFIGURACION_MAESTRA);
		}
		return response;
	}
	
	@Override
	public RespBase<RespComboCabecera> obtenerMaestraDetalleEntidad(Long idEntidad,Long cabeceraId) {
		List<MaestraCebecera> lisCabeceras =  null;
		List<MaestraDetalle> lisDetalles = null;
		MaestraCebecera maestraCebecera = null;
		List<MaestraDetalleEntidad> lisDetalleEntidads = null;
		RespComboCabecera respPayload = new RespComboCabecera();
		RespBase<RespComboCabecera> response = new RespBase<>();
		
		List<MaestraDetalle> listarDetalles  = null;
		List<MaestraDetalleEntidad> listarDetalleEntidades =  null;

		if (idEntidad != null) {
			if(cabeceraId != null) {
				Optional<MaestraCebecera> oCabecera = maestraCabeceraRepository.findById(cabeceraId);
				if(oCabecera.isPresent()) {
					maestraCebecera = oCabecera.get();
				}
				MaestraDetalle maeDetalle =  new MaestraDetalle();
				maeDetalle.setMaeCabeceraId(cabeceraId);		
				Example<MaestraDetalle> example = Example.of(maeDetalle);
				listarDetalles = maestraDetalleRepository.findAll(example);
				lisDetalles = new ArrayList<>();
				for (MaestraDetalle detalle : listarDetalles) {
					MaestraDetalle maestraDetalle = null;
					ConfiguracionMaestra configDetalle = configMaestraRepository
							.findByDetalleServir(detalle.getMaeDetalleId(),idEntidad);
					if (configDetalle != null && configDetalle.getMaeDetalleId() != null) {
						maestraDetalle = new MaestraDetalle();
						maestraDetalle.setMaeDetalleId(detalle.getMaeDetalleId());
						maestraDetalle.setMaeCabeceraId(detalle.getMaeCabeceraId());
						maestraDetalle.setDescripcion(detalle.getDescripcion());
						maestraDetalle.setDescripcionCorta(detalle.getDescripcionCorta());
						maestraDetalle.setSigla(detalle.getSigla());
						maestraDetalle.setReferencia(detalle.getReferencia());
						maestraDetalle.setOrden(detalle.getOrden());
						maestraDetalle.setEstadoRegistro(configDetalle.getEstadoRegistro());
						maestraDetalle.setEstado(configDetalle.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
						maestraDetalle.setConfiguracionId(configDetalle.getConfiguracionMaestraId());
						maestraDetalle.setEstadoConfiguracion(configDetalle.getEstadoRegistro());
						maestraDetalle.setCodProg(detalle.getCodProg());
						lisDetalles.add(maestraDetalle);
					}
					maestraCebecera.setListMaestraDetalles(lisDetalles);
				}				
				
				MaestraDetalleEntidad maeDetalleEntidad =  new MaestraDetalleEntidad();
				maeDetalleEntidad.setMaeCabeceraId(cabeceraId);		
				Example<MaestraDetalleEntidad> exampleEntidad = Example.of(maeDetalleEntidad);
				listarDetalleEntidades = detalleEntidadRepository.findAll(exampleEntidad);
				lisDetalleEntidads = new ArrayList<>();
				for (MaestraDetalleEntidad detalleEntidad : listarDetalleEntidades) {
					MaestraDetalleEntidad entidad = null;
					ConfiguracionMaestra configDetalleEntidad = configMaestraRepository
							.findByDetalleEntidad(detalleEntidad.getMaeDetalleEntidadId(),idEntidad);
					if (configDetalleEntidad != null && configDetalleEntidad.getMaeDetalleEntidadId() != null) {

						entidad = new MaestraDetalleEntidad();
						entidad.setMaeDetalleEntidadId(detalleEntidad.getMaeDetalleEntidadId());
						entidad.setEntidadId(detalleEntidad.getEntidadId());
						entidad.setMaeCabeceraId(detalleEntidad.getMaeCabeceraId());
						entidad.setDescripcion(detalleEntidad.getDescripcion());
						entidad.setDescripcionCorta(detalleEntidad.getDescripcionCorta());
						entidad.setSigla(detalleEntidad.getSigla());
						entidad.setReferencia(detalleEntidad.getReferencia());
						entidad.setOrden(detalleEntidad.getOrden());
						entidad.setEstadoRegistro(configDetalleEntidad.getEstadoRegistro());
						entidad.setEstado(configDetalleEntidad.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
						entidad.setConfiguracionId(configDetalleEntidad.getConfiguracionMaestraId());
						entidad.setEstadoConfiguracion(configDetalleEntidad.getEstadoRegistro());
						lisDetalleEntidads.add(entidad);

					}
					maestraCebecera.setListaMaestraDetalleEntidads(lisDetalleEntidads);
				}				
				lisCabeceras = new ArrayList<>();
				lisCabeceras.add(maestraCebecera);
				respPayload.setListaCabecera(lisCabeceras);	
			} else {
				List<Long> listaConfiguracionMaestras = configMaestraRepository.findAllByEntidad(idEntidad);
				lisCabeceras =  new ArrayList<>();
				for (Long configuracionMaestra : listaConfiguracionMaestras) {
					MaestraCebecera maCebecera = new MaestraCebecera();
					maCebecera.setMaeCabeceraId(configuracionMaestra);
					Example<MaestraCebecera> example = Example.of(maCebecera);
					List<MaestraCebecera> listaCebeceras = maestraCabeceraRepository.findAll(example);
					for (MaestraCebecera cabecera : listaCebeceras) {
						maestraCebecera = new MaestraCebecera();
						maestraCebecera.setMaeCabeceraId(cabecera.getMaeCabeceraId());
						maestraCebecera.setCodigoCabecera(cabecera.getCodigoCabecera());
						maestraCebecera.setDescripcion(cabecera.getDescripcion());
						maestraCebecera.setDescripcionCorta(cabecera.getDescripcionCorta());
						maestraCebecera.setSoloServir(cabecera.getSoloServir());
						maestraCebecera.setEstadoRegistro(cabecera.getEstadoRegistro());

						MaestraDetalle maeDetalle = new MaestraDetalle();
						maeDetalle.setMaeCabeceraId(configuracionMaestra);
						Example<MaestraDetalle> exampleDetalle = Example.of(maeDetalle);
						listarDetalles = maestraDetalleRepository.findAll(exampleDetalle);
						lisDetalles = new ArrayList<>();
						for (MaestraDetalle detalle1 : listarDetalles) {
							MaestraDetalle maestraDetalle = null;
							ConfiguracionMaestra configDetalle = configMaestraRepository
									.findByDetalleServir(detalle1.getMaeDetalleId(),idEntidad);
							if (configDetalle != null && configDetalle.getMaeDetalleId() != null) {
								if (detalle1.getMaeDetalleId().toString()
										.equals(configDetalle.getMaeDetalleId().toString())) {
									maestraDetalle = new MaestraDetalle();
									maestraDetalle.setMaeDetalleId(detalle1.getMaeDetalleId());
									maestraDetalle.setMaeCabeceraId(detalle1.getMaeCabeceraId());
									maestraDetalle.setDescripcion(detalle1.getDescripcion());
									maestraDetalle.setDescripcionCorta(detalle1.getDescripcionCorta());
									maestraDetalle.setSigla(detalle1.getSigla());
									maestraDetalle.setReferencia(detalle1.getReferencia());
									maestraDetalle.setOrden(detalle1.getOrden());
									maestraDetalle.setEstadoRegistro(configDetalle.getEstadoRegistro());
									maestraDetalle.setEstado(configDetalle.getEstadoRegistro().equals("1")? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
									maestraDetalle.setConfiguracionId(configDetalle.getConfiguracionMaestraId());
									maestraDetalle.setEstadoConfiguracion(configDetalle.getEstadoRegistro());
									lisDetalles.add(maestraDetalle);
								}
							}
							maestraCebecera.setListMaestraDetalles(lisDetalles);
						}
						MaestraDetalleEntidad maeDetalleEntidad = new MaestraDetalleEntidad();
						maeDetalleEntidad.setMaeCabeceraId(configuracionMaestra);
						Example<MaestraDetalleEntidad> exampleDetalleEntidad = Example.of(maeDetalleEntidad);
						listarDetalleEntidades = detalleEntidadRepository
								.findAll(exampleDetalleEntidad);
						lisDetalleEntidads = new ArrayList<>();
						for (MaestraDetalleEntidad detalleEntidad1 : listarDetalleEntidades) {
							MaestraDetalleEntidad entidad = null;
							ConfiguracionMaestra configDetalleEntidad = configMaestraRepository
									.findByDetalleEntidad(detalleEntidad1.getMaeDetalleEntidadId(),idEntidad);
							if ((configDetalleEntidad != null && configDetalleEntidad.getMaeDetalleEntidadId() != null) &&
								(detalleEntidad1.getMaeDetalleEntidadId().toString()
										.equals(configDetalleEntidad.getMaeDetalleEntidadId().toString()))) {
									entidad = new MaestraDetalleEntidad();
									entidad.setMaeDetalleEntidadId(detalleEntidad1.getMaeDetalleEntidadId());
									entidad.setEntidadId(detalleEntidad1.getEntidadId());
									entidad.setMaeCabeceraId(detalleEntidad1.getMaeCabeceraId());
									entidad.setDescripcion(detalleEntidad1.getDescripcion());
									entidad.setDescripcionCorta(detalleEntidad1.getDescripcionCorta());
									entidad.setSigla(detalleEntidad1.getSigla());
									entidad.setReferencia(detalleEntidad1.getReferencia());
									entidad.setOrden(detalleEntidad1.getOrden());
									entidad.setEstadoRegistro(configDetalleEntidad.getEstadoRegistro());
									entidad.setEstado(configDetalleEntidad.getEstadoRegistro().equals("1")? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
									entidad.setConfiguracionId(configDetalleEntidad.getConfiguracionMaestraId());
									entidad.setEstadoConfiguracion(configDetalleEntidad.getEstadoRegistro());
									lisDetalleEntidads.add(entidad);
								
							}
							maestraCebecera.setListaMaestraDetalleEntidads(lisDetalleEntidads);
						}
						lisCabeceras.add(maestraCebecera);
					}
				}

				respPayload.setListaCabecera(lisCabeceras);
			}
		} else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "La entidad no existe");
			return response;
		}
		return new RespBase<RespComboCabecera>().ok(respPayload);
	}


	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<RespMaestraDetalleEntidad> guardarMaestraDetalleEntidad(ReqBase<ReqMaestraDetalleEntidad> request,
			MyJsonWebToken token, Long configMaestraId) {
		
		RespBase<RespMaestraDetalleEntidad> response = new RespBase<>();
		ConfiguracionMaestra configuracionMaestra = null;
		MaestraDetalleEntidad maestraDetalleEntidad = null;
		
		if(configMaestraId != null) {
			Optional<ConfiguracionMaestra> configMaestraFind = configMaestraRepository.findById(configMaestraId);
			if(configMaestraFind.isPresent()) {
				configuracionMaestra = configMaestraFind.get();
				Optional<MaestraDetalleEntidad> maestraDetalleFind = detalleEntidadRepository.findById(configuracionMaestra.getMaeDetalleEntidadId());
				if(maestraDetalleFind.isPresent()) {
					maestraDetalleEntidad = maestraDetalleFind.get();
					maestraDetalleEntidad.setCampoSegUpd(request.getPayload().getEstado(), token.getUsuario().getUsuario(),
							Instant.now());
					}else {
						response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
								Constantes.MENSAJE_CONFIGURACION_MAESTRA);
						return response;
					}
			}else {
				response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
						Constantes.MENSAJE_CONFIGURACION_MAESTRA);
				return response;
			}
			
			configuracionMaestra.setCampoSegUpd(request.getPayload().getEstado(), token.getUsuario().getUsuario(),
					Instant.now());
			
			configMaestraRepository.save(configuracionMaestra);
		}else {
			maestraDetalleEntidad = new MaestraDetalleEntidad();
			maestraDetalleEntidad.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
		}
		
		maestraDetalleEntidad.setEntidadId(request.getPayload().getEntidadId());
		maestraDetalleEntidad.setMaeCabeceraId(request.getPayload().getMaeCabeceraEntidadId());
		maestraDetalleEntidad.setDescripcion(request.getPayload().getNombreCompletoEntidad());
		maestraDetalleEntidad.setDescripcionCorta(request.getPayload().getNombreCortoEntidad());
		maestraDetalleEntidad.setSigla(request.getPayload().getSiglaEntidad());
		maestraDetalleEntidad.setReferencia(request.getPayload().getReferenciaEntidad());
		detalleEntidadRepository.save(maestraDetalleEntidad);
		
		if(configMaestraId == null) {
			configuracionMaestra = new ConfiguracionMaestra();
			configuracionMaestra.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			configuracionMaestra.setEntidadId(request.getPayload().getEntidadId());
			configuracionMaestra.setMaeCabeceraId(request.getPayload().getMaeCabeceraEntidadId());
			configuracionMaestra.setMaeDetalleEntidadId(maestraDetalleEntidad.getMaeDetalleEntidadId());
			configMaestraRepository.save(configuracionMaestra);
			
		}
		
		RespMaestraDetalleEntidad payload = new RespMaestraDetalleEntidad();
		payload.setMaestraDetalleEntidad(maestraDetalleEntidad);
		payload.setConfigMaestra(configuracionMaestra);
		
		return new RespBase<RespMaestraDetalleEntidad>().ok(payload);
		
	}

}
