package pe.gob.servir.convocatoria.request;

import javax.validation.Valid;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.MaestraConocimientoDTO;

@Getter
@Setter
public class ReqMaestraConocimiento {
	
	@Valid
	private MaestraConocimientoDTO maestraConocimiento;
}
