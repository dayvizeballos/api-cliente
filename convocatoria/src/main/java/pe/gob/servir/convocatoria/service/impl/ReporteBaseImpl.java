package pe.gob.servir.convocatoria.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.CssStyle;
import pe.gob.servir.convocatoria.common.HtmlConstantes;
import pe.gob.servir.convocatoria.common.VariablesSistema;
import pe.gob.servir.convocatoria.exception.NotFoundException;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BaseEvaluacion;
import pe.gob.servir.convocatoria.model.BaseEvaluacionDetalle;
import pe.gob.servir.convocatoria.model.BasePerfil;
import pe.gob.servir.convocatoria.model.Bonificacion;
import pe.gob.servir.convocatoria.model.BonificacionDetalle;
import pe.gob.servir.convocatoria.model.Convocatoria;
import pe.gob.servir.convocatoria.model.DatosConcurso;
import pe.gob.servir.convocatoria.model.Entidad;
import pe.gob.servir.convocatoria.model.InformeComplement;
import pe.gob.servir.convocatoria.model.InformeDetalle;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.MaestraDetalleEntidad;
import pe.gob.servir.convocatoria.model.Organigrama;
import pe.gob.servir.convocatoria.model.ReporteBasePDF;
import pe.gob.servir.convocatoria.model.ReporteBonificacion;
import pe.gob.servir.convocatoria.model.ReporteBonificacionDetalle;
import pe.gob.servir.convocatoria.model.ReporteDatosConcurso;
import pe.gob.servir.convocatoria.model.ReporteDesierto;
import pe.gob.servir.convocatoria.model.ReporteDetalleEvaluacion;
import pe.gob.servir.convocatoria.model.ReportePerfil;
import pe.gob.servir.convocatoria.model.UnidadOrganica;
import pe.gob.servir.convocatoria.repository.BaseEvaluacionRepository;
import pe.gob.servir.convocatoria.repository.BaseInformeComplRepository;
import pe.gob.servir.convocatoria.repository.BasePerfilRepository;
import pe.gob.servir.convocatoria.repository.BonificacionDetalleRepository;
import pe.gob.servir.convocatoria.repository.ConvocatoriaRepository;
import pe.gob.servir.convocatoria.repository.DatosConcursoRepository;
import pe.gob.servir.convocatoria.repository.InformeDetalleRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleEntidadRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.ReporteBaseRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGeneraToken;
import pe.gob.servir.convocatoria.request.dto.BaseCronogramaDTO;
import pe.gob.servir.convocatoria.request.dto.BaseEvaluacionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.BonificacionPdfDTO;
import pe.gob.servir.convocatoria.request.dto.InformacionComplDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespOrganigrama;
import pe.gob.servir.convocatoria.response.RespUnidadOrganica;
import pe.gob.servir.convocatoria.service.BaseCronogramaService;
import pe.gob.servir.convocatoria.service.BaseService;
import pe.gob.servir.convocatoria.service.ReporteBase;
import pe.gob.servir.convocatoria.service.ReportePerfilService;
import pe.gob.servir.convocatoria.service.RequisitoGeneralService;
import pe.gob.servir.convocatoria.util.HtmlGeneradorUtil;
import pe.gob.servir.convocatoria.util.PDFGeneradorUtil;
import pe.gob.servir.convocatoria.util.StringUtil;
import pe.gob.servir.convocatoria.util.Util;


@Service
public class ReporteBaseImpl implements ReporteBase {


    private static final Logger logger = LoggerFactory.getLogger(ReporteBaseImpl.class);

    @Autowired
    private MaestraDetalleRepository maeRepository;

    @Autowired
    private BaseCronogramaService baseService;
    
	@Autowired
	private BaseService baseServices;

    @Autowired
    private InformeDetalleRepository informeRepository;

    @Autowired
    private BonificacionDetalleRepository bonificacionDetalleRepository;

    @Autowired
    private ReporteBaseRepository reporteBaseRepository;

    @Autowired
    private EntidadClient entidadRepository;

    @Autowired
    private ReportePerfilService reportePerfilService;

    @Autowired
    private BasePerfilRepository basePerfilRepository;

    @Autowired
    private BaseInformeComplRepository baseInformeComplRepository;

    @Autowired
    private BaseEvaluacionRepository baseEvaluacionRepository;
    
    @Autowired
    private MaestraDetalleEntidadRepository maeDetalleEntidadRepository;
    
    @Autowired
    VariablesSistema variablesSistema;
    
    @Autowired
    private RequisitoGeneralService requisitoGeneralService;
    
    @Autowired
    private DatosConcursoRepository datosConcursoRepository;
    
    @Autowired
    ConvocatoriaRepository convocatoriaRepository;
//
//    @Override
//    public String parseThymeleafTemplate(Long id) {
//        List<Cronograma> listaC = new ArrayList<>();
//        List<DetalleEvaluacionEntidadDTO> listEva = new ArrayList<>();
//        Perfil perfil = reporteRepository.findEvaluacion(id);
//        BasePerfil bp = new BasePerfil();
//        String validadorValor = reporteRepository.findBaseId(id) + "";
//        if (!validadorValor.equals("null")) {
//
//            Base b = reporteRepository.findBase(Long.valueOf(Integer.parseInt(validadorValor)));
//            bp = reporteRepository.findBasePerfil(id);
//            ReqParamEvaluacion req = new ReqParamEvaluacion();
//            req.setEntidadId(perfil.getEntidadId());
//            req.setModalidadId(b.getModalidadId());
//            req.setTipoId(b.getTipoId());
//            req.setRegimenId(b.getRegimenId());
//
//            RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> respPayload = evaluacionRepository
//                    .findEvaluacionEntidadDto(req);
//
//            DetalleEvaluacionEntidadDTO eva;
//            if (respPayload != null) {
//                for (int i = 0; i < respPayload.getPayload().getItems().size(); i++) {
//                    eva = new DetalleEvaluacionEntidadDTO();
//                    eva.setDescripcion(respPayload.getPayload().getItems().get(i).getDescripcion());
//                    eva.setPeso(respPayload.getPayload().getItems().get(i).getPeso());
//                    eva.setPuntajeMin(respPayload.getPayload().getItems().get(i).getPuntajeMin());
//                    eva.setPuntajeMax(respPayload.getPayload().getItems().get(i).getPuntajeMax());
//                    listEva.add(eva);
//                }
//            }
//
//            int valor = Integer.parseInt(validadorValor);
//            List<BaseCronograma> listaCronograma = baseRepository.findAllCronogramas(valor);
//
//            List<MaestraDetalle> listaEtapas = maeRepository
//                    .findDetalleByCodeCabeceraEstado(Constantes.COD_TABLA_ESTADO_CRONOGRAMA, "1");
//
//            for (int i = 0; i < listaCronograma.size(); i++) {
//                Cronograma c = new Cronograma();
//                c.setBasecronogramaId(listaCronograma.get(i).getBasecronogramaId());
//                c.setEtapaId(listaCronograma.get(i).getEtapaId());
//                c.setDescripcion(listaCronograma.get(i).getDescripcion());
//                c.setResponsable(listaCronograma.get(i).getResponsable());
//                c.setPeriodoini(listaCronograma.get(i).getPeriodoini());
//                c.setPeriodofin(listaCronograma.get(i).getPeriodofin());
//                for (int j = 0; j < listaEtapas.size(); j++) {
//                    if (listaEtapas.get(j).getMaeDetalleId()
//                            .equals(Long.valueOf(listaCronograma.get(i).getEtapaId()))) {
//                        c.setDescripcionetapa(listaEtapas.get(j).getDescripcion());
//                    }
//                }
//                listaC.add(c);
//            }
//
//        }
//        // -----------------------------------------------------------------
//        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
//        templateResolver.setSuffix(".html");
//        templateResolver.setTemplateMode(TemplateMode.HTML);
//
//        TemplateEngine templateEngine = new TemplateEngine();
//        templateEngine.setTemplateResolver(templateResolver);
//
//        Context context = new Context();
//
//        String contenido = devolverInforme(Long.valueOf(7)); // IMPEDIMENTOS PARA CONTRATAR
//        String base = devolverInforme(Long.valueOf(10)); // BASE LEGAL
//        String crieva = devolverInforme(Long.valueOf(5));// CRITERIOS DE EVALUACION
//        String crisel = devolverInforme(Long.valueOf(15));// CRITERIOS DE ELECCIÓN
//        String declaracion = devolverInforme(Long.valueOf(6));// DECLARACIÓN DESIERTO
//
//        // -----------Bonificacion
//        List<MaestraDetalle> listaMaeDetalle = maeRepository.findAll();
//        List<BonificacionDetalle> listDetBonificacion = bonificacionDetalleRepository
//                .findAllByBonificacionId(Long.valueOf(1));
//        List<BonificacionPdfDTO> listFuerzas = findBonificaciones(listDetBonificacion, listaMaeDetalle);
//
//        List<BonificacionDetalle> listDetBonificacion2 = bonificacionDetalleRepository
//                .findAllByBonificacionId(Long.valueOf(2));
//        List<BonificacionPdfDTO> listDepor = findBonificaciones(listDetBonificacion2, listaMaeDetalle);
//
//        Map<Integer, String> map1 = verificarListaBonificacionDetalle(listFuerzas, Long.valueOf(1));
//        String tit1 = map1.get(1);
//        String bon1 = map1.get(2);
//
//        Map<Integer, String> map2 = verificarListaBonificacionDetalle(listDepor, Long.valueOf(2));
//        String tit2 = map2.get(1);
//        String bon2 = map2.get(2);
//
//        context.setVariable("fechaActual", new Date());
//        context.setVariable("Titbon1", tit1);
//        context.setVariable("bon1", bon1);
//        context.setVariable("listBon1", listFuerzas);
//        context.setVariable("Titbon2", tit2);
//        context.setVariable("bon2", bon2);
//        context.setVariable("listDeport", listDepor);
//        context.setVariable("cont", contenido);
//        context.setVariable("base", base);
//        context.setVariable("crieva", crieva);
//        context.setVariable("crisel", crisel);
//        context.setVariable("declaracion", declaracion);
//        context.setVariable("sede", bp.getSedeDireccion());
//        context.setVariable("contrato", bp.getTiempoContrato());
//        context.setVariable("remuneracion", bp.getRemuneracion());
//        context.setVariable("jornada", bp.getJornadaLaboral());
//        context.setVariable("cronograma", listaC);
//        context.setVariable("evaluaciones", listEva);
//
//        return templateEngine.process("BaseFinal", context);
//    }
//
//    @Override
//    public byte[] generatePdfFromHtml(Long id) throws DocumentException, IOException {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        ITextRenderer rendererx = new ITextRenderer();
//        rendererx.setDocumentFromString(parseThymeleafTemplate(id));
//        rendererx.layout();
//        rendererx.createPDF(baos);
//
//        byte[] byteArray = baos.toByteArray();
//        baos.close();
//
//        return byteArray;
//    }
//
//    @Override
//    public List<BonificacionPdfDTO> findBonificaciones(List<BonificacionDetalle> listDetBonificacion,
//                                                       List<MaestraDetalle> listaMaeDetalle) {
//
//        Map<Long, String> mapDetalle = listaMaeDetalle.stream().filter(Objects::nonNull)
//                .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
//        List<BonificacionPdfDTO> listDescripcionPdf = new ArrayList<>();
//
//        if (!listDetBonificacion.isEmpty()) {
//            listDetBonificacion.stream().forEach(c -> {
//                BonificacionPdfDTO dto = new BonificacionPdfDTO();
//                dto.setAplicaSobre(mapDetalle.get(c.getAplicaId()));
//                dto.setDescripcion(c.getDescripcion());
//                dto.setNivel(StringUtils.defaultString(mapDetalle.get(c.getNivelId())));
//                dto.setPorcentajeBonificacion(c.getPorcentajeBono().toString());
//                listDescripcionPdf.add(dto);
//            });
//        }
//        return listDescripcionPdf;
//    }
//
    public String devolverInforme(Long id) {
        String output = "";
        Optional<InformeDetalle> informeDetalle = informeRepository.findAllByIdAndEstadoRegistro(id);
        if (informeDetalle.isPresent()) {
            output = StringUtil.devolverCadena(informeDetalle.get().getContenido());
        }
        return output;
    }
//
//    public Map<Integer, String> verificarListaBonificacionDetalle(List<BonificacionPdfDTO> lista, Long id) {
//        List<BonificacionDetalle> listDetBonificacion = bonificacionDetalleRepository.findAllByBonificacionId(id);
//        Map<Integer, String> map = new HashMap<>();
//        if (!lista.isEmpty()) {
//            map.put(1, listDetBonificacion.get(0).getBonificacion().getTitulo());
//            map.put(2, listDetBonificacion.get(0).getBonificacion().getContenido());
//        } else {
//            map.put(1, "");
//            map.put(2, "");
//        }
//        return map;
//    }
//
    @Override
    public byte[] generatePdfFromHtmlBase(Long id) throws DocumentException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ITextRenderer rendererx = new ITextRenderer();
        rendererx.setDocumentFromString(StringUtil.devolverCadena(parseThymeleafTemplateBase(id)));
        rendererx.layout();
        rendererx.createPDF(baos);

        byte[] byteArray = baos.toByteArray();
        baos.close();

        return byteArray;
    }

    public byte[] generatePdfFromHtmlBaseConvocatoria(Long id) throws DocumentException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ITextRenderer rendererx = new ITextRenderer();
        rendererx.setDocumentFromString(StringUtil.devolverCadena(parseThymeleafTemplateBaseConvocatoria(id)));
        rendererx.layout();
        rendererx.createPDF(baos);

        byte[] byteArray = baos.toByteArray();
        baos.close();

        return byteArray;
    }

    @Override
    public String parseThymeleafTemplateBase(Long id) {

        ReporteBasePDF reporte = new ReporteBasePDF();
        ReportePerfil perfil = new ReportePerfil();
        ReporteDatosConcurso concurso = new ReporteDatosConcurso();
        DatosConcurso concursoBean =  new DatosConcurso(); 
        List<ReporteDetalleEvaluacion> listaEvaluacion = new ArrayList<>();
        List<BaseCronogramaDTO> cronograma = new ArrayList<>();
        Optional<Base> b = reporteBaseRepository.findById(id);
        Convocatoria convocatoria = new Convocatoria();
        String codigoConvocatoria = null;
        
        if(b.isPresent()) {
        	
        	Optional<Convocatoria> convo = convocatoriaRepository.findByBase(b.get());
        	if(convo.isPresent()) {
        		convocatoria = convo.get();
        		codigoConvocatoria = convocatoria.getCodigoConvocatoria();
        	}else {
        		codigoConvocatoria = null;
        	}		
        	
        	Optional<DatosConcurso> dc = datosConcursoRepository.findByBase(b.get());
        	if(!dc.isPresent()) {
        		throw new NotFoundException(
    					"no existe Base con el id: (" + b.get().getBaseId() + ") en base de datos");
        	}else {
        		concursoBean = dc.get();
        	}
            List<MaestraDetalle> listaMaeDetalle = maeRepository.findAll();
            List<MaestraDetalle> listaDeclaraciones = maeRepository.findDetalleByCodeCabecera("TIP_DEC_JUR");
            Map<Long, String> mapDetalle = listaMaeDetalle.stream().filter(Objects::nonNull)
                    .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
            Map<Long, String> mapDetalleCodigo = listaMaeDetalle.stream().filter(Objects::nonNull)
                    .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));
           
            Map<Long, String> mapDetalleAbreviatura=  listaMaeDetalle.stream().filter(item -> Objects.nonNull(item.getSigla()))
            		.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getSigla));
           
        	//RespBase<RespEntidad> responseEntidad = entidadRepository.obtieneEntidad(b.get().getEntidadId());
            
        	String siglaEntidad=b.get().getSiglas() == null ?"":b.get().getSiglas();
        	reporte.setCodCabecera(siglaEntidad);
        	String distrito = b.get().getDistritoEntidad() == null ? "" : b.get().getDistritoEntidad(); // b.get().getDatosConcursoList().get(0).getBase().getDistritoEntidad();
        	reporte.setFechaPublicacion(Util.Capitalizador(distrito==null?"Distrito Entidad":distrito)+
        			Util.fechaPublicacion(b.get().getFechaPublicacion()==null? new Date():b.get().getFechaPublicacion()));
        	String abreviatura="";
        	//if(b.get().getDatosConcursoList().get(0).getTipoPracticaId()==null) {
        	if(concursoBean.getTipoPracticaId()==null) {	
            	 abreviatura = mapDetalleAbreviatura.get(b.get().getModalidadId())+mapDetalleAbreviatura.get(b.get().getTipoId());
            	reporte.setSigla(abreviatura);
            	reporte.setDirigido(mapDetalle.get(b.get().getTipoId()));
        	} else {
        		abreviatura = "PRAC";
        		reporte.setSigla(abreviatura);
        		//reporte.setDirigido(mapDetalle.get(b.get().getDatosConcursoList().get(0).getTipoPracticaId()));
        		reporte.setDirigido(mapDetalle.get(concursoBean.getTipoPracticaId()));
        	}
        	
        	reporte.setLogo(variablesSistema.fileServer+"/"+ b.get().getUrlLogoEntidad());
        	reporte.setNombreEntidad(b.get().getNombreEntidad());
        	reporte.setCodigoConvocatoria(codigoConvocatoria==null?"N° XXXX-XXXX-Nombre Entidad":codigoConvocatoria.substring(codigoConvocatoria.indexOf("N°")));
        	 
        	if(reporte.getCodigoConvocatoria().contains("N°") && reporte.getCodigoConvocatoria().contains("-")) {
        		reporte.setCodCabecera(reporte.getCodigoConvocatoria().substring(reporte.getCodigoConvocatoria().indexOf("N°"),reporte.getCodigoConvocatoria().lastIndexOf("-"))+" "+siglaEntidad);
        	}
        	
        	reporte.setModalidadProceso(mapDetalle.get(b.get().getModalidadId()));
        	reporte.setTipoProceso(mapDetalle.get(b.get().getTipoId()));
        	reporte.setRegimen(mapDetalle.get(b.get().getRegimenId()));
        	reporte.setUrlWebEntidad(b.get().getUrlWebEntidad());
        	
        	// Datos concurso
            concurso.setNombre(concursoBean.getNombre());
            concurso.setObjetivo(concursoBean.getObjetivo());
            concurso.setNroVacantes(concursoBean.getNroVacantes());
            concurso.setCorreo(concursoBean.getCorreo());
            concurso.setTelefonoContacto(concursoBean.getTelefono()+" - ANEXO : "+concursoBean.getAnexo());
            
            //if (b.get().getDatosConcursoList().get(0).getUnidadOrganicaId() != null) {
            if (concursoBean.getUnidadOrganicaId() != null) {
            	concurso.setUnidadOrganica(concursoBean.getNombreUnidadOrganica());
            	/*
                RespBase<RespUnidadOrganica> responseUo = entidadRepository.obtieneUnidadOrganica(b.get().getEntidadId());
                concurso.setUnidadOrganica(devolverDescripcionUnidadOrgnica(responseUo,
                		concursoBean.getUnidadOrganicaId()));
                		*/
            }
            /*RespBase<RespOrganigrama> responseO = entidadRepository.obtieneOrgano(b.get().getEntidadId(),
                    variablesSistema.ENTIDAD_ORG_TIPO);
            concurso.setOrgano(
                    devolverDescripcionOrgano(responseO, concursoBean.getOrganoResponsableId()));
            concurso.setOrganoSeleccion(devolverDescripcionOrgano(responseO, concursoBean.getOrganoResponsableId()));
            */
            concurso.setOrgano(concursoBean.getNombreOrgano());
            concurso.setOrganoSeleccion(concursoBean.getNombreOrgano());
            
            concurso.setBaseLegal(
                    StringUtil.remplazarTag(StringUtil.devolverCadena(devolverInforme(concursoBean.getInformeDetalleId()))));

            if(!Util.isEmpty(concursoBean.getInformeEspecificaId()))
                concurso.setBaseLegalEspecifico(
                        StringUtil.remplazarTag(StringUtil.devolverCadena(devolverInforme(concursoBean.getInformeEspecificaId()))));

            reporte.setDatoConcurso(concurso);
            
            //Requisitos Generales
            
            reporte.setDeclaracion(requisitoGeneralService.getRequisitoGeneral(b.get().getBaseId()).getPayload().getDeclaracionJuradaDTOList());
            
            List<ReporteDesierto> declServir= new ArrayList<>();
            listaDeclaraciones.forEach(e ->{
            	 ReporteDesierto rep = new ReporteDesierto();
            	 rep.setTitulo(e.getMaeDetalleId().toString());
            	 rep.setContenido(e.getDescripcion());
            	 declServir.add(rep);
            } );
            reporte.setDeclaracionServir(declServir);
         // Evaluacion
            BaseEvaluacion baseEvaluacionFilter = new BaseEvaluacion();
            baseEvaluacionFilter.setBase(b.get());
            Example<BaseEvaluacion> exampleEvaluacion = Example.of(baseEvaluacionFilter);
            List<BaseEvaluacion> lstBaseEvaluacion = baseEvaluacionRepository.findAll(exampleEvaluacion);
            
            if (lstBaseEvaluacion != null && Boolean.FALSE.equals(lstBaseEvaluacion.isEmpty())) {
            	//listaEvaluacion = lstBaseEvaluacion.get(0).getBaseEvaluacionDetalleList().stream()
                listaEvaluacion =  baseEvaluacionRepository.listEvalDetalleByBase(b.get()).stream()
            			.filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
            			.map(eva ->{
            				ReporteDetalleEvaluacion evaluacion = new ReporteDetalleEvaluacion();
            				evaluacion.setDescripcion( //getEvaluacionEntidad
            						eva.getEvaluacionEntidad().getIndValidaServir() == null ? this.obtEvaluacionEntidad(eva.getEvaluacionEntidad().getTipoEvaluacionId()) 
            								: mapDetalle.get(eva.getEvaluacionEntidad().getTipoEvaluacionId()));
            				evaluacion.setPeso(eva.getPeso().intValue());	
            				evaluacion.setInforme(eva.getInformeDetalle().getContenido());
            				evaluacion.setPuntajeMin(eva.getPuntajeMinimo().intValue());
            				evaluacion.setPuntajeMax(eva.getPuntajeMaximo().intValue());
            				evaluacion.setAplica(eva.getPeso().intValue() == 0 ? "NO" : "SI"); 
            				evaluacion.setContenido(
            						StringUtil.remplazarTag(devolverInforme(eva.getInformeDetalle().getInformeDetalleId())));
            				return evaluacion;
            			}).collect(Collectors.toList());
            }
            reporte.setEvaluaciones(listaEvaluacion);
            
            //Cronograma
            cronograma=baseService.findAllCronogramas(b.get().getBaseId(), 0).getPayload().getBaseCronogramaDTOList();
            reporte.setCronograma(cronograma);
            reporte.setDiaDifusion(30L);
            reporte.setDiaDifusionTexto("Treinta");
            long diff=0;
            for (int i = 0; i < cronograma.size(); i++) {
				if(cronograma.get(i).getCodProg().equals("1")) {
					diff=cronograma.get(i).getPeriodoFin().getTime()-cronograma.get(i).getPeriodoIni().getTime();
		            reporte.setDiaDifusion(diff/86400000);
		            reporte.setDiaDifusionTexto(Util.cantidadConLetra(String.valueOf(reporte.getDiaDifusion())));
				}
			}
            
            // Perfil
            List<BasePerfil> lstBasePerfil = basePerfilRepository.findbyBaseId(b.get());
            if(!lstBasePerfil.isEmpty()) {
                BasePerfil bp = lstBasePerfil.get(0);
                perfil = reportePerfilService.findReportePerfil(bp.getPerfilId());
                // seteo valores Base Perfil
                if(perfil!= null) {
                    perfil.setJornadaTrabajo(bp.getJornadaLaboral());
                    perfil.setRemuneracion(bp.getRemuneracion());
                    perfil.setRemuneracionAnual(bp.getRemuneracion() * 12);
                    perfil.setSedeDireccion(bp.getSedeDireccion());
                    
                    perfil.setGruposervidores(perfil.getGruposervidores().isEmpty()?"Ninguno":perfil.getGruposervidores());
                	perfil.setFamilia(perfil.getFamilia().isEmpty()?"Ninguno":perfil.getFamilia());
                	perfil.setRol(perfil.getRol().isEmpty()?"Ninguno":perfil.getRol());
                	perfil.setNivel(perfil.getNivel().isEmpty()?"Ninguno":perfil.getNivel());
                	
                    if(concursoBean.getTipoPracticaId()!=null) {
                    	
                    	reporte.setModalidadProceso("CONVOCATORIA "+perfil.getCondicion());
                    	reporte.setTipoProceso("");
                    }
                }
            } else {
            	perfil.setNombrepuesto("Sin Perfil elegido.");
            	perfil.setJornadaTrabajo("Elegir Perfil para mostrar este dato.");
            	perfil.setRemuneracion(0.0);
            	perfil.setRemuneracionAnual(0.0);
            	perfil.setSedeDireccion("Elegir Perfil para mostrar este dato.");
            	perfil.setGruposervidores("Elegir Perfil para mostrar este dato.");
            	perfil.setFamilia("Elegir Perfil para mostrar este dato.");
            	perfil.setRol("Elegir Perfil para mostrar este dato.");
            	perfil.setNivel("Elegir Perfil para mostrar este dato.");
            	
                if(concursoBean.getTipoPracticaId()!=null) {
                	
                	reporte.setModalidadProceso("CONVOCATORIA PRACTICANTE PRE/PROFESIONAL");
                	reporte.setTipoProceso("");
                }
            }
            reporte.setPerfil(perfil);
            
            // Inf Complementario
            List<InformacionComplDTO> listInf = baseServices.obtenerDataEtapa6(b.get().getBaseId()).getPayload().getInformes();
            List<ReporteBonificacion> listaBonificacion = new ArrayList<>();
            List<ReporteDesierto> listaDesierto = new ArrayList<>();
            List<ReporteDesierto> listaObservacion = new ArrayList<>();
            List<ReporteDesierto> listaAbstencion = new ArrayList<>();
            
            if(listInf==null) {
            	listInf= new ArrayList<>();
            }
            
            for (int i = 0; i < listInf.size(); i++) {
				List<BonificacionDetalle> listDetBonificacion = 
						bonificacionDetalleRepository.findAllByBonificacionId(listInf.get(i).getIdInforme());
				
            	if(mapDetalleCodigo.get(listInf.get(i).getTipoInformeId()).equals(variablesSistema.TI_BONIFICACION)) {
					List<BonificacionPdfDTO> listDescripcionPdf = new ArrayList<>();
					
					StringBuilder sb = new StringBuilder(PDFGeneradorUtil.getHtmlTemplate());
					
					if(!listDetBonificacion.isEmpty()) {
					
						listDetBonificacion.stream().forEach(c-> {
							BonificacionPdfDTO dto = new BonificacionPdfDTO();
							dto.setAplicaSobre(mapDetalle.get(c.getAplicaId()));
							dto.setDescripcion(c.getDescripcion());
							dto.setNivel(StringUtils.defaultString(mapDetalle.get(c.getNivelId())));
							dto.setPorcentajeBonificacion(c.getPorcentajeBono().toString());
							listDescripcionPdf.add(dto);
						});
						
						listDescripcionPdf.sort(Comparator.comparing(BonificacionPdfDTO::getNivel));
						
						CssStyle cssStyle = new CssStyle();
						cssStyle.setTextAlign(HtmlConstantes.CENTER);
						
						Bonificacion bonificacion = listDetBonificacion.get(0).getBonificacion();
						
						try {
							HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlGeneradorUtil.buildParagraph(bonificacion.getContenido()));
							
							HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlConstantes.TAG_BR);

							String theadersHtml = HtmlGeneradorUtil.buildTableHeader(listDescripcionPdf.get(0));
							String tbodyHtml = HtmlGeneradorUtil.buildTableBodyWithCss(listDescripcionPdf,cssStyle);
							String tableHtml = HtmlGeneradorUtil.buildTableHtmlCode(theadersHtml, tbodyHtml, null);
							HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, tableHtml);
							
							HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlConstantes.TAG_BR);
						 
							ReporteBonificacion reporteBonificacion = new ReporteBonificacion();
							reporteBonificacion.setTitulo(listInf.get(i).getNombreInforme());
							reporteBonificacion.setContenido(sb.substring(sb.indexOf("<body>")+6,sb.indexOf("</body>")));
							listaBonificacion.add(reporteBonificacion);
						}
						catch(Exception e) {
							throw new NotFoundException("no existe informacion de las Bonificaciones");
						}
					}
					
				} 
            	if(mapDetalleCodigo.get(listInf.get(i).getTipoInformeId()).equals(variablesSistema.TI_DECLARACION_DESIERTO)) {
            		ReporteDesierto reporteDesierto = new ReporteDesierto();
            		reporteDesierto.setTitulo(listInf.get(i).getNombreInforme());
            		Optional<InformeDetalle> infDetalle = informeRepository.findById(listInf.get(i).getIdInforme());
            		if(infDetalle.isPresent()) { 			
            			reporteDesierto.setContenido(infDetalle.get().getContenido());
            		}
            		
            		listaDesierto.add(reporteDesierto);
            	}
            	
            	if(mapDetalleCodigo.get(listInf.get(i).getTipoInformeId()).equals(variablesSistema.TI_OBSERVACION)) {
            		ReporteDesierto reporteObservacion = new ReporteDesierto();
            		reporteObservacion.setTitulo(listInf.get(i).getNombreInforme());
            		Optional<InformeDetalle> infDetalle = informeRepository.findById(listInf.get(i).getIdInforme());
            		
            		if(infDetalle.isPresent()) {
            			reporteObservacion.setContenido(infDetalle.get().getContenido());
            		}
            		listaObservacion.add(reporteObservacion);
            	}
            	
            	if(mapDetalleCodigo.get(listInf.get(i).getTipoInformeId()).equals(variablesSistema.TI_ABSTENCION)) {
            		ReporteDesierto reporteAbstencion = new ReporteDesierto();
            		reporteAbstencion.setTitulo(listInf.get(i).getNombreInforme());
            		Optional<InformeDetalle> infDetalle = informeRepository.findById(listInf.get(i).getIdInforme());
            		
            		if(infDetalle.isPresent()) {
            			reporteAbstencion.setContenido(infDetalle.get().getContenido());
            		}
            		listaAbstencion.add(reporteAbstencion);
            	}
			}
			reporte.setBonificaciones(listaBonificacion);
            reporte.setDesierto(listaDesierto);
            reporte.setObservacion(listaObservacion);
            reporte.setAbstencion(listaAbstencion);
            
        } else {
        	 throw new NotFoundException("no existe informacion de la base");
        }
      
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = new Context();
        context.setVariable("reporte", reporte);

        return templateEngine.process("ReporteBaseN", context);
    }

 
	
  /*@Override
   *   public byte[] generatePdfFromHtmlBaseInforme(Long id) throws DocumentException, IOException {

        List<BonificacionDetalle> listDetBonificacion = bonificacionDetalleRepository
                .findAllByBonificacionId(Long.valueOf(30));

        Bonificacion bonificacion = listDetBonificacion.get(0).getBonificacion();

        StringBuilder sb = new StringBuilder(parseThymeleafTemplateBase(id));
        int indexIni = sb.indexOf("{0}");
        sb = sb.replace(indexIni, indexIni + 3, bonificacion.getContenido());

        return PDFGeneradorUtil.getPdfByte(sb);
    }
*/
    public String devolverDescripcionUnidadOrgnica(RespBase<RespUnidadOrganica> response, Long id) {
        String output = "";
        for (UnidadOrganica u : response.getPayload().getListaUnidadOrganica()) {
            if (id.equals(u.getOrganigramaId())) {
                output = u.getUnidadOrganica();
                break;
            }
        }

        return output;
    }

    public String devolverDescripcionOrgano(RespBase<RespOrganigrama> response, Long id) {
        String output = "Ninguno";
        if(response.getStatus().getSuccess().equals(true)) {
            for (Organigrama u : response.getPayload().getListaOrganigrama()) {
                if (id.equals(u.getOrganigramaId())) {
                    output = u.getDescripcion();
                    break;
                }
            }
        }

        return output;
    }

    @Override
    public String parseThymeleafTemplateBaseConvocatoria(Long id) {

    	ReporteBasePDF reporte = new ReporteBasePDF();
        ReportePerfil perfil = new ReportePerfil();
        ReporteDatosConcurso concurso = new ReporteDatosConcurso();
        List<ReporteDetalleEvaluacion> listaEvaluacion = new ArrayList<>();
        Base b = reporteBaseRepository.findBase(id);
        if (b != null) {

            // Datos principales
            // RespBase<RespEntidad> response =
            // entidadRepository.obtieneEntidad(b.getEntidadId());
            // reporte.setNombreEntidad(response.getPayload().getEntidad().get(0).getDescripcionEntidad());

            //reporte.setNombreEntidad(this.nombreEntidad(b.getEntidadId()));
            Map<String, Object> mapEntidad= this.getEntidad(b.getEntidadId());
            if(mapEntidad!=null) {
                reporte.setNombreEntidad((String)mapEntidad.get(Constantes.NOMBRE_ENTIDAD_KEY));
                reporte.setUrlWebEntidad((String)mapEntidad.get(Constantes.URL_WEB));
            }

            // Datos concurso
            List<MaestraDetalle> listaMaeDetalle = maeRepository.findAll();
            Map<Long, String> mapDetalle = listaMaeDetalle.stream().filter(Objects::nonNull)
                    .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
            Map<Long, String> mapDetalleCodigo = listaMaeDetalle.stream().filter(Objects::nonNull)
                    .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));

            concurso.setNombre(b.getDatosConcursoList().get(0).getNombre());
            concurso.setObjetivo(b.getDatosConcursoList().get(0).getObjetivo());
            concurso.setNroVacantes(b.getDatosConcursoList().get(0).getNroVacantes());
            concurso.setCorreo(b.getDatosConcursoList().get(0).getCorreo());

            /*
             * RespBase<RespUnidadOrganica> responseUo =
             * entidadRepository.obtieneUnidadOrganica(b.getEntidadId());
             * concurso.setUnidadOrganica(devolverDescripcionUnidadOrgnica(responseUo,b.
             * getDatosConcursoList().get(0).getUnidadOrganicaId()));
             *
             * RespBase<RespOrganigrama> responseO =
             * entidadRepository.obtieneOrgano(b.getEntidadId(),
             * Constantes.ENTIDAD_ORG_TIPO);
             * concurso.setOrgano(devolverDescripcionOrgano(responseO,b.getDatosConcursoList
             * ().get(0).getOrganoResponsableId()));
             */
            concurso.setOrganoSeleccion(mapDetalle.get(b.getDatosConcursoList().get(0).getOrganoEncargadoId()));
            concurso.setBaseLegal(
                    StringUtil.remplazarTag(devolverInforme(b.getDatosConcursoList().get(0).getInformeDetalleId())));
            if(!Util.isEmpty(b.getDatosConcursoList().get(0).getInformeEspecificaId()))
                concurso.setBaseLegalEspecifico(
                        StringUtil.remplazarTag(devolverInforme(b.getDatosConcursoList().get(0).getInformeEspecificaId())));

            reporte.setDatoConcurso(concurso);

            // Perfil
            List<BasePerfil> lstBasePerfil = basePerfilRepository.findbyBaseId(b);
            if(!lstBasePerfil.isEmpty()) {
                BasePerfil bp = lstBasePerfil.get(0);
                perfil = reportePerfilService.findReportePerfil(bp.getPerfilId());
                // seteo valores Base Perfil
                if(perfil!= null) {
                    perfil.setJornadaTrabajo(bp.getJornadaLaboral());
                    perfil.setRemuneracion(bp.getRemuneracion());
                    perfil.setRemuneracionAnual(bp.getRemuneracion() * 12);
                    perfil.setSedeDireccion(bp.getSedeDireccion());
                    reporte.setPerfil(perfil);
                }
            }

            // Requisitos Generales
            // RequisitoGeneral rg = requisitoGeneralRepository.findbyBase(b);

            // Evaluacion
            BaseEvaluacion baseEvaluacionFilter = new BaseEvaluacion();
            baseEvaluacionFilter.setBase(b);
            Example<BaseEvaluacion> exampleEvaluacion = Example.of(baseEvaluacionFilter);
            List<BaseEvaluacion> lstBaseEvaluacion = baseEvaluacionRepository.findAll(exampleEvaluacion);
            
            if (lstBaseEvaluacion != null && Boolean.FALSE.equals(lstBaseEvaluacion.isEmpty())) {
            	listaEvaluacion = lstBaseEvaluacion.get(0).getBaseEvaluacionDetalleList().stream()
            			.filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
            			.map(eva ->{
            				ReporteDetalleEvaluacion evaluacion = new ReporteDetalleEvaluacion();
            				evaluacion.setDescripcion( //getEvaluacionEntidad
            						eva.getEvaluacionEntidad().getIndValidaServir() == null ? this.obtEvaluacionEntidad(eva.getEvaluacionEntidad().getTipoEvaluacionId()) 
            								: mapDetalle.get(eva.getEvaluacionEntidad().getTipoEvaluacionId()));
            				evaluacion.setPeso(eva.getPeso().intValue());	
            				evaluacion.setPuntajeMin(eva.getPuntajeMinimo().intValue());
            				evaluacion.setPuntajeMax(eva.getPuntajeMaximo().intValue());
            				evaluacion.setAplica(eva.getPeso().intValue() == 0 ? "NO" : "SI"); 
            				evaluacion.setContenido(
            						StringUtil.remplazarTag(devolverInforme(eva.getInformeDetalle().getInformeDetalleId())));
            				return evaluacion;
            			}).collect(Collectors.toList());
            }
            
           /* RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> respPayload = evaluacionRepository
                    .findEvaluacionEntidadById(b.getBaseId(), b.getEntidadId());

            ReporteDetalleEvaluacion evaluacion;

            if (respPayload != null) {
                if (respPayload.getStatus().getSuccess().equals(true)) {
                    for (int i = 0; i < respPayload.getPayload().getItems().size(); i++) {
                        evaluacion = new ReporteDetalleEvaluacion();
                        evaluacion.setDescripcion(respPayload.getPayload().getItems().get(i).getDescripcion());
                        evaluacion.setPeso(respPayload.getPayload().getItems().get(i).getPeso());
                        evaluacion.setPuntajeMin(respPayload.getPayload().getItems().get(i).getPuntajeMin());
                        evaluacion.setPuntajeMax(respPayload.getPayload().getItems().get(i).getPuntajeMax());
                        evaluacion.setAplica(respPayload.getPayload().getItems().get(i).getPeso() == 0 ? "NO" : "SI");
                        listaEvaluacion.add(evaluacion);
                    }
                }
            }
*/
            reporte.setEvaluaciones(listaEvaluacion);

            // Cronograma

            // Inf Complementario
            List<ReporteBonificacion> listaBonificacion = new ArrayList<>();
            List<InformeComplement> listaInformeComplementaria = baseInformeComplRepository.findByBaseId(b.getBaseId());
            if (!listaInformeComplementaria.isEmpty()) {
                int correlativo = 0;
                for (InformeComplement infoComplementaria : listaInformeComplementaria) {
                    correlativo += 1;
                    ReporteBonificacion reporteBonificacion = new ReporteBonificacion();
                    List<BonificacionDetalle> listDetBonificacion = bonificacionDetalleRepository
                            .findAllByBonificacionId(infoComplementaria.getBonificacionId());
                    if (!listDetBonificacion.isEmpty()) {
                        Bonificacion bonificacion = listDetBonificacion.get(0).getBonificacion();
                        reporteBonificacion.setCorrelativo(correlativo);
                        reporteBonificacion.setTitulo(bonificacion.getTitulo());
                        reporteBonificacion.setContenido(
                                StringUtil.devolverCadena(StringUtil.remplazarTag(bonificacion.getContenido())));
                        reporteBonificacion
                                .setCodigoTipoBonificacion(mapDetalleCodigo.get(bonificacion.getTipoBonificacion()));
                        List<ReporteBonificacionDetalle> listadetalleBoni = new ArrayList<>();
                        if (!listDetBonificacion.isEmpty()) {
                            listDetBonificacion.stream().forEach(c -> {
                                ReporteBonificacionDetalle det = new ReporteBonificacionDetalle();
                                det.setAplica(mapDetalle.get(c.getAplicaId()));
                                det.setDescripcion(c.getDescripcion());
                                det.setNivel(StringUtils.defaultString(mapDetalle.get(c.getNivelId())));
                                det.setPorcentaje(c.getPorcentajeBono().toString() + "%");
                                listadetalleBoni.add(det);
                            });
                            reporteBonificacion.setDetalle(listadetalleBoni);

                        }
                    }
                    listaBonificacion.add(reporteBonificacion);

                }

            }
            reporte.setBonificaciones(listaBonificacion);

            reporte.setCodigoConvocatoria(b.getCodigoConvocatoria());
        }


        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = new Context();
        context.setVariable("reporte", reporte);

        return templateEngine.process("ReporteBaseN", context);

    }

    public String generateToken() {
        try {
            ReqBase<ReqGeneraToken> api = new ReqBase<>();
            ReqGeneraToken reqGeneraToken = new ReqGeneraToken();
//            reqGeneraToken.setUsuario(variablesSistema.jobUser);
//            reqGeneraToken.setPassword(variablesSistema.jobPass);
            reqGeneraToken.setGrantType(variablesSistema.jobGrant);
            api.setPayload(reqGeneraToken);
            RestTemplate restTemplate = new RestTemplate();

            String fooResourceUrl = variablesSistema.jobOauthurl;
            HttpHeaders headers = new HttpHeaders();
            headers.setBasicAuth(variablesSistema.jobBasicAuth);
            HttpEntity<ReqBase<ReqGeneraToken>> request = new HttpEntity<>(api, headers);
            ResponseEntity<RespBase> response = restTemplate.exchange(fooResourceUrl, HttpMethod.POST, request, RespBase.class);
            if (response.getBody().getPayload() != null) {
                Map<String, Object> oauth = (Map<String, Object>) response.getBody().getPayload();
                if (oauth.containsKey(Constantes.ACCESS_TOKEN_KEY)) {
                    return (String) oauth.get(Constantes.ACCESS_TOKEN_KEY);
                }
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            return null;
        }
        return null;
    }

    public String nombreEntidad(Long idEntidad) {

        try {
            RestTemplate restTemplate = new RestTemplate();
            String fooResourceUrl = variablesSistema.jobBusEntidadUrl;
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(generateToken());

            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<RespBase> response = restTemplate.exchange(fooResourceUrl + idEntidad, HttpMethod.GET, request, RespBase.class);
            HashMap<String, Object> payload = (HashMap<String, Object>) response.getBody().getPayload();
            if (payload.containsKey(Constantes.ENTIDAD_KEY)) {
                List<Map<String, Object>> listBeforeGroup = (List<Map<String, Object>>) payload.get(Constantes.ENTIDAD_KEY);
                for (Map<String, Object> entry : listBeforeGroup) {
                    if (entry.containsKey(Constantes.NOMBRE_ENTIDAD_KEY)) {
                        return (String) entry.get(Constantes.NOMBRE_ENTIDAD_KEY);
                    }
                }
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            return null;
        }
        return null;

    }
    
    public String obtEvaluacionEntidad(Long maeDetalleEntidadId) {
    	String nombre="";
    	
    	MaestraDetalleEntidad maestraDetEntidad = new MaestraDetalleEntidad();
    	Optional<MaestraDetalleEntidad>  optMaeDetEntidad = maeDetalleEntidadRepository.findById(maeDetalleEntidadId);
    	if(optMaeDetEntidad.isPresent()) {
    		 maestraDetEntidad =  optMaeDetEntidad.get();
    	}
    	
    	nombre = maestraDetEntidad.getDescripcion() == null ? "" :maestraDetEntidad.getDescripcion(); 
    	
    	return nombre;
    }

    public Map<String, Object> getEntidad (Long idEntidad) {
        //Entidad entidad = null;

        try {
            RestTemplate restTemplate = new RestTemplate();
            String fooResourceUrl = variablesSistema.jobBusEntidadUrl;
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(generateToken());

            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<RespBase> response = restTemplate.exchange(fooResourceUrl + idEntidad, HttpMethod.GET, request, RespBase.class);
            HashMap<String, Object> payload = (HashMap<String, Object>) response.getBody().getPayload();
            if (payload.containsKey(Constantes.ENTIDAD_KEY)) {
                List<Map<String, Object>> listBeforeGroup = (List<Map<String, Object>>) payload.get(Constantes.ENTIDAD_KEY);
              /*  for (Map<String, Object> entry : listBeforeGroup) {
                    /*if (entry.containsKey(Constantes.NOMBRE_ENTIDAD_KEY)) {
                        return (String) entry.get(Constantes.NOMBRE_ENTIDAD_KEY);
                    }*/
                //return (Entidad)entry.get(Constantes.ENTIDAD_KEY);
                // }
                return listBeforeGroup.get(0);
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            return null;
        }
        return null;

        //return entidad;
    }

}
