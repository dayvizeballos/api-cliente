package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.request.dto.DeclaracionJuradaDTO;
import pe.gob.servir.convocatoria.request.dto.RequisitoGeneralDTO;

import java.util.List;

public interface RequisitoGeneralRepositoryJdbc {
    List<RequisitoGeneralDTO> lisGeneralDtoList(Long baseId , Long requisitoId);
    List<DeclaracionJuradaDTO> lisDeclaracionJuradaDtos(Long idBase);

}
