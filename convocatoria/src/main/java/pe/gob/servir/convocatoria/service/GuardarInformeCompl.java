package pe.gob.servir.convocatoria.service;

import java.util.List;
import java.util.Map;

import pe.gob.servir.convocatoria.model.InformeComplement;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarInformacionComp;
import pe.gob.servir.convocatoria.request.dto.InformacionComplDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface GuardarInformeCompl {
	
	void saveListInformeDetalle(List<InformacionComplDTO> listIdInformeDetalle,
			ReqBase<ReqGuardarInformacionComp> request,
            MyJsonWebToken token);
	
	void actualizarInformeCompl(List<InformacionComplDTO> listIdInformeDetalle,
			List<InformeComplement> informeCompBD, List<InformacionComplDTO> listIdBonificacion,
			ReqBase<ReqGuardarInformacionComp> request,
            MyJsonWebToken token,Long idTipoInfBonificacion);
	
	public Map<String, Long> getMaeDetalleByCode(String codeMaeDet);
	public Map<Long, String> getMaeDetalleIdDescByCode(String codeMaeDet);

}
