package pe.gob.servir.convocatoria.repository;


import pe.gob.servir.convocatoria.request.dto.ObtenerPerfilConfigDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerPerfilDTO;
import pe.gob.servir.convocatoria.request.dto.VacanteDTO;

import java.util.List;
import java.util.Map;

public interface PerfilRepositoryJdbc {
    List<ObtenerPerfilDTO> findAllPerfil(String fechaIni, String fechaFin, Long entidad, String codigoPuesto,
                                         String nombrePerfil, Long regimenId, Long organoId, Long unidadOrganicaId,
                                         String origenPerfil , Long estadoRevision , String estado);
    
    String findCorrelativoCodigoPuesto(Long regimenID, Long entidadId);

	List<VacanteDTO> listarVacantes(Long baseId);

	Long calcularVacantesDisponiblesPorBase(Long baseId);

	List<ObtenerPerfilConfigDTO> buscarPerfilByFilter(Map<String, Object> parametroMap);
}
