package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.BaseCronograma;


@Getter
@Setter
public class RespActualizaBaseCronograma {

	private BaseCronograma basecronograma;
}
