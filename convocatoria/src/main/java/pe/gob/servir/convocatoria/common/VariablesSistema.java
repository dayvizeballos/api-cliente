package pe.gob.servir.convocatoria.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
public class VariablesSistema {
	
	@Value("${maestra.cabecera.id.regimen.laboral}")
	public Long CABECERA_REGIMEN_LABORAL;
	
	@Value("${maestra.cabecera.id.modalidad}")
	public Long CABECERA_MODALIDAD;
	
	@Value("${maestra.cabecera.id.tipo}")
	public Long CABECERA_TIPO;	
	
	@Value("${maestra.cabecera.id.evaluaciones}")
	public Long CABECERA_EVALUACIONES;	
	
	@Value("${ruta.file.server}")
	public String fileServer;

//	@Value("${job.setUsuario}")
//	public String jobUser;
//
//	@Value("${job.setPassword}")
//	public String jobPass;

	@Value("${job.setGrantType}")
	public String jobGrant;

	@Value("${job.oauth.url}")
	public String jobOauthurl;

	@Value("${job.setBasicAuth}")
	public String jobBasicAuth;

	@Value("${job.buscar.entidad.url}")
	public String jobBusEntidadUrl;

	@Value("${job.api.maestra.upload.url}")
	public String jobApiMaestraUpload;
	
	@Value("${job.api.maestra.parametro.url}")
	public String jobApiMaestraParametro;
	
	@Value("${ti.bonificacion}")
	public String TI_BONIFICACION;
	
	@Value("${ti.criterio.evaluacion}")
	public String TI_CRITERIO_EVALUACION;
	
	@Value("${ti.declaracion.desierto}")
	public String TI_DECLARACION_DESIERTO;
	
	@Value("${ti.observacion}")
	public String TI_OBSERVACION;
	
	@Value("${ti.abstencion}")
	public String TI_ABSTENCION;
	
	@Value("${entidad.organo.tipo}")
	public long ENTIDAD_ORG_TIPO;
	
	@Value("${job.dias.notificacion.etapa}")
	public int NOTIFICACION_DIAS_ETAPA;
	
	@Value("${ruta.archivo.excel.window.30057}")
	public String rutaExcelWindowPerfil30057;//NOSONAR
	
	@Value("${ruta.archivo.excel.linux.30057}")
	public String rutaExcelLinuxPerfil30057;//NOSONAR
	
	@Value("${ruta.archivo.excel.window.OtrosRegimen}")
	public String rutaExcelWindowPerfilOtrosRegimen;//NOSONAR
	
	@Value("${ruta.archivo.excel.linux.OtrosRegimen}")
	public String rutaExcelLinuxPerfilOtrosRegimen;//NOSONAR
	
	@Value("${ruta.archivo.excel.window.Practicas}")
	public String rutaExcelWindowPerfilPracticas;//NOSONAR
	
	@Value("${ruta.archivo.excel.linux.Practicas}")
	public String rutaExcelLinuxPerfilPracticas;//NOSONAR
	
	@Value("${conocimientos.tecnicos}")
	public String conocimientoTecnico;//NOSONAR
	
	@Value("${cursos.especializacion}")
	public String cursosEspecializacion;//NOSONAR
	
	@Value("${programas.especializacion}")
	public String programasEspecializacion;//NOSONAR
	
	@Value("${conocimientos.ofimatica}")
	public String conocimientoOfimatica;//NOSONAR
	
	@Value("${conocimientos.idioma}")
	public String conocimientoIdioma;//NOSONAR
	
	@Value("${password.excel}")
	public String passwordExcel;//NOSONAR

}
