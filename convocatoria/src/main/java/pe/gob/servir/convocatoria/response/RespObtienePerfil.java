package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.PerfilDTO;

@Getter
@Setter
public class RespObtienePerfil {
	
	private PerfilDTO perfil;

}
