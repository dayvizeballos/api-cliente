package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;

@Data
public class RespPerfilesConvocatoriaDTO {

	private Long perfilId;
	private String desPerfil;
	private String nombrePuesto;
	private Long remuneracion;
    private int vacantes;
    private int postulantes;
    private int calificacion;
    
}
