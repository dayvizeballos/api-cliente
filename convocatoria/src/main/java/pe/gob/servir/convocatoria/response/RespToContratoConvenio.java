package pe.gob.servir.convocatoria.response;

import lombok.Data;

import java.util.Date;

@Data
public class RespToContratoConvenio {

    private String nroResolucion;
    private Integer anio;
    private Long organoId;
    private Long unidadOrganicaId;
    private String  nombrePuesto;
    private String  nombreUniOrganica;
    private String  nombreOrgano; // se usa too en convenio
    private String  orgUnOrganica;
    private Integer numero;
    private Date fechaSubscripcion;
    private Date fechaResolucion;
    private Date fechaVinculacion;
    private Long entidadId;
    private Long familiaId;
    private Long rolId;
    private String razonSocial;
    private String  jornalaboral;
    private String  lugarPrestacion;
    private String ruc;
    private String domicilio;

    private String sede;
    private String siglas;
    private String subNivelcat;
    private String direccion;
    private String rol;
    private String responsableOrh;
    private String puestoResponsableOrh;
    private String nroDocResponsable;
    private Long tipoDocResponsable;
    private String articulo;
    private String nroNorma;
    private Long nroPosPueMeri;
    private Long  basePerfilId;
    private String nombres;
    private String normaAproProy;
    private String nroConPubMeritos;
    private String nroConPub;
    private String nroInforme;
    private String apellidos;
    private Long postulanteId;
    private String dni;
    private Integer tipoDoc;
    private String nacionalidad;
    private String nivelCategoria;
    private Long nivelCategoriaId;
    private String familia;
    private Date fechaNacimiento;
    private String fecha;
    private String areaLabores;
    private String centroEstudios;
    private String direccionCentroEstudios;
    private String  direccionLabores;
    private String rucCentroEstudios;
    private String  ciclo;
    private String  periodoPrueba;
    private Double  compeEconomica;
    private String  puestoAutoRepresentativa;
    private Long  tipoTrabajoId;
    private Long  tipoServicioId;
    private Long  tipoTramiteId;
    private Long  perfilId;
    private Long  baseId;
    private Long  posCuadPuesto;
    private Long  postulanteSelId;
    private String periodoConvenio;
    private String resolResponOrh;
    private String archivoSuscrito;
    
    private Date fechaIniPractica;
    private Date fechaFinPractica;
    private String horaIniPractica;
    private String horaFinPractica;
   
    private String puestoRepreUni;
    private String nombRepreUni;
    private Long tipoDocRepreUni;
    private String nroDocRepreUni;
    
    private Long tipoPractica; //falta setear
    private String descTipoPractica;
    private Long  tipoContratoId;
    private String codigoTipoContrato;
    private String rutaPlantilla;
    private String tipoConcurso;

}
