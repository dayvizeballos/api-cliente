package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface PerfilExperienciaService {

    RespBase<PerfilExperienciaDTO> crearPerfilExperiencia(ReqBase<PerfilExperienciaDTO> request, MyJsonWebToken token);
    RespBase<PerfilExperienciaDTO> actualizarPerfilExperiencia(ReqBase<PerfilExperienciaDTO> request, MyJsonWebToken token);
    RespBase<PerfilExperienciaDTO> findPerfilExpereincia(Long perfilId);
}
