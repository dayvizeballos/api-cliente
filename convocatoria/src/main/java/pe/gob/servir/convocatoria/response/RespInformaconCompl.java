package pe.gob.servir.convocatoria.response;

import java.util.List;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.ReqGuardarInformacionComp;

@JGlobalMap
@Getter
@Setter
public class RespInformaconCompl extends ReqGuardarInformacionComp {
	
	private List<Long> listIdBaseCompl;
	private List<Long> listIdBaseComplDetalle;
	

}
