package pe.gob.servir.convocatoria.request.dto;

import java.util.Date;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class ContratoDTO {
	
	 private String nroResolucion;
	 
	 private String orgUnOrganica;
	 
	 private Integer numero;

	 private String organoTitulo;

	 private Date fechaSubscripcion;

	 private Date fechaResolucion;

	 private Date fechaVinculacion;
	 private Long entidadId;

	 private String razonSocial;

	 private String  jornalaboral;

	 private String  lugarPrestacion;

	 private String ruc;

	 private String sede;

	 private String siglas;

	 private String subNivelcat;

	 private String direccion;

	 private String rol;

	 private String responsableOrh;

	 private String puestoResponsableOrh;

	 private String nroDocResponsable;
	 private Long tipoDocResponsable;
	 
	 private String articulo;

	 private String nroNorma;

	 private Long nroPosPueMeri;

	 private Long  basePerfilId;
	 
	 private String nombres;
	 
	 private String normaAproProy;

	 private String nroConPubMeritos;

	 private String nroConPub;

	 private String nroInforme;

	 private String apellidos;

	 private Long postulanteId;

	 private String dni;

	 private String nacionalidad;

	 private String nivelCategoria;

	 private String familia;

	 private String domicilio;

	 private Date fechaNacimiento;

	 private String fecha;

	 private String areaLabores;

	 private String centroEstudios;

	 private String  direccionLabores;

	 private String  ciclo;

	 private String  periodoPrueba;

	 private Double  compeEconomica;

	 private String  puestoAutoRepresentativa;

	 private Long  tipoTrabajoId;

	 private Long  tipoServicioId;

	 private Long  tipoTramiteId;

	 private Long  perfilId;

	 private Long  baseId;

	 private Long  posCuadPuesto;
	
}
