package pe.gob.servir.convocatoria.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReporteBonificacion {
	
	private Long id;
	private Integer correlativo;
	private String titulo;
	private String contenido;
	private Long tipoBonificacion;
	private String codigoTipoBonificacion;
	private List<ReporteBonificacionDetalle> detalle;
	

}
