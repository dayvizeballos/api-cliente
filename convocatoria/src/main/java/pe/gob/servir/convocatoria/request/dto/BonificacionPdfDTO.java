package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.annotation.AplicarCss;
import pe.gob.servir.convocatoria.annotation.ColumnaTablaHtml;

@JGlobalMap
@Getter
@Setter
public class BonificacionPdfDTO {
	
	@ColumnaTablaHtml(orden = 0,nombre="NIVEL",ignoreWhen = "No aplica",validIgnore = true)
	private String nivel;
	@ColumnaTablaHtml(orden = 1,nombre="DESCRIPCIÓN")
    private String descripcion;
	@ColumnaTablaHtml(orden = 2,nombre="APLICA SOBRE")
	private String aplicaSobre;
	@AplicarCss
	@ColumnaTablaHtml(orden = 3,concatValue="%",nombre="% DE BONIFICACION")
	private String porcentajeBonificacion;

}
