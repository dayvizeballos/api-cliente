package pe.gob.servir.convocatoria.request.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@JGlobalMap
@Getter
@Setter
public class BonificacionDTO {
    
	private Long bonificacionId;
	
	private Long tipoInfoId;
	
	@NotNull(message = Constantes.CAMPO + " tipoBonificacion " + Constantes.ES_OBLIGATORIO)
	private Long tipoBonificacion;
	
	@NotNull(message = Constantes.CAMPO + " titulo " + Constantes.ES_OBLIGATORIO)
	private String titulo;
	
	@NotNull(message = Constantes.CAMPO + " contenido " + Constantes.ES_OBLIGATORIO)
	private String contenido;
	
	private String estadoRegistro;
	
	@JsonIgnore
	private String usuario;
	
	@NotNull(message = Constantes.CAMPO + " listaBonificacionDetalle " + Constantes.ES_OBLIGATORIO)
	@Valid
	private List<BonificacionDetalleDTO> bonificacionDetalleDTOList;
}
