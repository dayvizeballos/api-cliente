package pe.gob.servir.convocatoria.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqAsigarMaeDetalleEntidad;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraDetalleEntidad;
import pe.gob.servir.convocatoria.response.RespAsignarMaeDetalleEntidad;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespComboCabecera;
import pe.gob.servir.convocatoria.response.RespMaestraDetalleEntidad;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.MaestraDetalleEntidadService;

@RestController
@Tag(name = "Maestra Detalle Entidad", description = "")
public class MaestraDetalleEntidadController {
	

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	MaestraDetalleEntidadService maestraDetalleEntiadService;
	
	
	@Operation(summary = "Asigna una maestra a la entidad", description = "Asigna una maestra a la entidad", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType =  MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType =  MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/maestraDetalleEntidad/asignaMaestraDetalle" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespAsignarMaeDetalleEntidad>> asignarMaestraDetalle(@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqAsigarMaeDetalleEntidad> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespAsignarMaeDetalleEntidad> response = maestraDetalleEntiadService.asignarMaestraDetalle(request, jwt, null);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra detalle y maestra servir", description = Constantes.SUM_OBT_LIST+"maestra detalle y maestra servir", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraDetalleEntidad"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespComboCabecera>> listarMaestraDetalleEntidad(
			@PathVariable String access,
			@RequestParam(value = "idEntidad", required = false) Long idEntidad,
			@RequestParam(value = "idCabecera", required = false) Long idCabecera) {
		RespBase<RespComboCabecera> response = maestraDetalleEntiadService.obtenerMaestraDetalleEntidad(idEntidad,idCabecera);
		
		return ResponseEntity.ok(response);		
	}

	
	@Operation(summary = "Habilita/Deshabilita una maestra detalle", description = "Habilita/Deshabilita una maestra detalle", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/maestraDetalleEntidad/configMaestraDetalle/{configMaestraId}" }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> habilitarMaestraDetalle(@PathVariable String access,
			@PathVariable Long configMaestraId,
			@RequestParam(value = "estado", required = true) @NotNull(message = "estado no puede ser nulo")  String estado){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> response = maestraDetalleEntiadService.habilitarMaestraDetalleEntidad(jwt, configMaestraId, estado);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Crea una maestra detalle entidad", description = "Crea una maestra detalle entidad", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType =  MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType =  MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/maestraDetalleEntidad" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespMaestraDetalleEntidad>> registrarMaestraDetalleEntidad(@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqMaestraDetalleEntidad> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespMaestraDetalleEntidad> response = maestraDetalleEntiadService.guardarMaestraDetalleEntidad(request, jwt, null);
		return ResponseEntity.ok(response);
	}
	
	
	@Operation(summary = "Actualiza una maestra detalle entidad", description = "Actualiza una maestra detalle entidad", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/maestraDetalleEntidad/{configMaestraId}" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespMaestraDetalleEntidad>> actualizarMaestraDetalleEntidad(@PathVariable String access,
			@PathVariable Long configMaestraId, @Valid @RequestBody ReqBase<ReqMaestraDetalleEntidad> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespMaestraDetalleEntidad> response = maestraDetalleEntiadService.guardarMaestraDetalleEntidad(request, jwt, configMaestraId);
		return ResponseEntity.ok(response);	
	}	
}
