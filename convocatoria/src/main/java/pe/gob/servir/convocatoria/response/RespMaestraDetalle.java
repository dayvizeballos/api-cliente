package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.MaestraDetalle;


@Getter
@Setter
public class RespMaestraDetalle {

	private MaestraDetalle maestraDetalle;
}
