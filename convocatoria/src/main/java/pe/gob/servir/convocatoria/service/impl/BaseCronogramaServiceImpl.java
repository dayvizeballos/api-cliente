package pe.gob.servir.convocatoria.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.exception.ConflictException;
import pe.gob.servir.convocatoria.exception.NotFoundException;
import pe.gob.servir.convocatoria.feign.client.SeleccionClient;
import pe.gob.servir.convocatoria.model.*;
import pe.gob.servir.convocatoria.queue.producer.ReporteConvocatoriaProducer;
import pe.gob.servir.convocatoria.repository.*;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaBaseCronograma;
import pe.gob.servir.convocatoria.request.ReqEliminarActividad;
import pe.gob.servir.convocatoria.request.dto.ActividadDTO;
import pe.gob.servir.convocatoria.request.dto.BaseCronogramaDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCreaBaseCronograma;
import pe.gob.servir.convocatoria.response.RespEtapaActualVacanteBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespPerfilVacante;
import pe.gob.servir.convocatoria.response.dto.ReporteConvocatoriaDTO;
import pe.gob.servir.convocatoria.response.dto.RespPeriodoCronogramaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.BaseCronogramaService;
import pe.gob.servir.convocatoria.util.DateUtil;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BaseCronogramaServiceImpl implements BaseCronogramaService {

    public static final Logger logger = Logger.getLogger(BaseCronogramaServiceImpl.class);
    private static final String yyyy_MM_dd = "yyyy-MM-dd";
    private static final String COD_PROG_DIFUSION = "1";

    @Autowired
    private BaseCronogramaRepository baseCronogramaRepository;

    @Autowired
    private MaestraDetalleRepository maeRepository;

    @Autowired
    private BaseRepository baseRepository;

    @Autowired
    private MaestraDetalleRepository maestraDetalleRepository;

    @Autowired
    private BaseCronoActividadRepository baseCronoActividadRepository;

    @Autowired
    private BaseCronogramaHistRepository baseCronogramaHistRepository;

    @Autowired
    private BaseCronogramaActividadHistRepository baseCronogramaActividadHistRepository;

    @Autowired
    private ConvocatoriaRepository convocatoriaRepository;

    @Autowired
    SeleccionClient seleccionClient;


    @Autowired
    private ReporteConvocatoriaProducer reporteConvocatoriaProducer;

    /**
     * VALIDAR RANGO DE PERIODOS
     * EL FIN ES MENOR QUE EL INI
     * guardarBaseCronograma
     *
     * @param ls
     */
    private void validarPeriodosCronograma(List<BaseCronogramaDTO> ls) {
        if (!ls.isEmpty()) {
            int valorSiguiente = 0;
            for (int i = 0; i < ls.size(); i++) {
                try {
                    valorSiguiente++;
                    BaseCronogramaDTO fechaFin = ls.get(i);
                    if (valorSiguiente == ls.size()) return;
                    BaseCronogramaDTO fechaIni = ls.get(valorSiguiente);
                    validarPeriodos(fechaFin.getPeriodoFin(), fechaIni.getPeriodoIni(), fechaFin, fechaIni);
                } catch (ArrayIndexOutOfBoundsException e) {
                    logger.info(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * guardarBaseCronograma
     *
     * @param fechaFin
     * @param fechaini
     */
    private void validarPeriodos(Date fechaFin, Date fechaini, BaseCronogramaDTO primero, BaseCronogramaDTO siguiente) {
        if (!fechaFin.before(fechaini)) {
            throw new ConflictException("la fecha incial de la etapa  (" + siguiente.getEtapaId() + ") debe ser mayor de la fecha final de la etapa: (" + primero.getEtapaId() + ") ");

        }
    }


    /**
     * VALIDA BASE
     * guardarBaseCronograma
     *
     * @param baseId
     * @return
     */
    public Optional<Base> validarCronogramaBase(Long baseId) {
        Optional<Base> base = baseRepository.findById(baseId);
        if (!base.isPresent()) {
            throw new NotFoundException("no existe la base con el id: (" + baseId + ") en base de datos");
        }
        return base;
    }

    /**
     * VALIDA ETAPA
     * guardarBaseCronograma
     *
     * @return
     */
    public Optional<MaestraDetalle> validarCronogramaEtapa(Long  idEtapa) {
        Optional<MaestraDetalle> etapa = maestraDetalleRepository.findById(idEtapa);
        if (!etapa.isPresent()) {
            throw new NotFoundException("no existe la etapa con el id: (" + idEtapa + ") en base de datos");
        }
        return etapa;
    }

    /**
     * SETT VALORES ACTIVADES PARA
     * CRONOGRAMA UPDATE
     * guardarBaseCronograma
     *
     * @param ls
     * @param baseCronograma
     * @param token
     * @return
     */
    private List<BaseCronogramaActividad> settActividadesUpdate(List<ActividadDTO> ls, BaseCronograma baseCronograma, MyJsonWebToken token) {
        return ls.stream()
                .map(dto -> {
                    Optional<BaseCronogramaActividad> actividad = baseCronoActividadRepository.findById(dto.getActividadId());
                    if (!actividad.isPresent())
                        throw new NotFoundException("no existe la actividad con el id: (" + dto.getActividadId() + ") en base de datos");
                    actividad.ifPresent(val -> {
                        actividad.get().setBaseCronograma(baseCronograma);
                        actividad.get().setFechaFin(dto.getFechaFin());
                        actividad.get().setFechaIni(dto.getFechaIni());
                        actividad.get().setHoraFin(dto.getHoraFin());
                        actividad.get().setHoraIni(dto.getHoraIni());
                        actividad.get().setRespomsable(dto.getResponsable());
                        actividad.get().setCampoSegUpd(dto.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
                        actividad.get().setEstadoRegistro(dto.getEstadoRegistro());
                        actividad.get().setDescripcion(dto.getDescripcion());

                        if (!Util.isEmpty(dto.getTipoActividad())) {
                            Optional<MaestraDetalle> tipoActividad = maestraDetalleRepository.findById(dto.getTipoActividad());
                            if (!tipoActividad.isPresent())
                                throw new NotFoundException("no existe tipoActividad con el id: (" + dto.getTipoActividad() + ") en base de datos");
                            tipoActividad.ifPresent(actividad.get()::setTipoActividad);
                        }

                    });
                    return actividad.get();
                }).collect(Collectors.toList());
    }

    /**
     * SETT VALORES ACTIVADES PARA
     * CRONOGRAMA
     * guardarBaseCronograma
     *
     * @param ls
     * @param baseCronograma
     * @param token
     * @return
     */
    private List<BaseCronogramaActividad> settActividades(List<ActividadDTO> ls, BaseCronograma baseCronograma, MyJsonWebToken token) {
        return ls.stream()
                .map(dto -> {
                    BaseCronogramaActividad actividad = new BaseCronogramaActividad();
                    actividad.setBaseCronograma(baseCronograma);
                    actividad.setFechaFin(dto.getFechaFin());
                    actividad.setFechaIni(dto.getFechaIni());
                    actividad.setHoraFin(dto.getHoraFin());
                    actividad.setHoraIni(dto.getHoraIni());
                    actividad.setRespomsable(dto.getResponsable());
                    actividad.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    actividad.setDescripcion(dto.getDescripcion());

                    if (!Util.isEmpty(dto.getTipoActividad())) {
                        Optional<MaestraDetalle> tipoActividad = maestraDetalleRepository.findById(dto.getTipoActividad());
                        if (!tipoActividad.isPresent())
                            throw new NotFoundException("no existe tipoActividad con el id: (" + dto.getTipoActividad() + ") en base de datos");
                        tipoActividad.ifPresent(actividad::setTipoActividad);
                    }
                    return actividad;
                }).collect(Collectors.toList());
    }

    /**
     * SETT LIST CRONOGRAMA DTO
     * guardarBaseCronograma
     *
     * @param ls
     * @return
     */
    private List<BaseCronogramaDTO> settListCronogramaDto(List<BaseCronograma> ls , boolean fromfindAllCronogramas) {
        return ls.stream()
                .map(b -> {
                    BaseCronogramaDTO dto = new BaseCronogramaDTO();
                    dto.setCronogramaId(Long.valueOf(b.getBasecronogramaId()));
                    dto.setBaseId(b.getBaseId().getBaseId());
                    dto.setDescripcion(b.getDescripcion());
                    dto.setPeriodoFin(b.getPeriodofin());
                    dto.setPeriodoIni(b.getPeriodoini());
                    dto.setEtapaId(b.getEtapaId().getMaeDetalleId());
                    dto.setCodProg(b.getEtapaId().getCodProg());
                    if (fromfindAllCronogramas){
                        dto.setActividadDTOList(settActividadesDto(baseCronogramaRepository.listActividades(b.getBasecronogramaId())));
                    }else{
                        dto.setActividadDTOList(settActividadesDto(ls.get(0).getBaseCronogramaActividadList()));
                    }
                    return dto;
                }).collect(Collectors.toList());
    }

    /**
     * SETT ACTIVIDADES DTO
     * guardarBaseCronograma
     *
     * @param ls
     * @return
     */
    private List<ActividadDTO> settActividadesDto(List<BaseCronogramaActividad> ls) {
        return ls.stream()
                .filter(actividad -> !actividad.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
                .map(actividad -> {
                    ActividadDTO actividadDTO = new ActividadDTO();
                    actividadDTO.setActividadId(actividad.getActividadId());
                    actividadDTO.setFechaFin(actividad.getFechaFin());
                    actividadDTO.setFechaIni(actividad.getFechaIni());
                    actividadDTO.setHoraFin(actividad.getHoraFin());
                    actividadDTO.setHoraIni(actividad.getHoraIni());
                    actividadDTO.setResponsable(actividad.getRespomsable());
                    actividadDTO.setEstadoRegistro(actividad.getEstadoRegistro());
                    actividadDTO.setDescripcion(actividad.getDescripcion());
                    actividadDTO.setTipoActividad(actividad.getTipoActividad() == null ? null : actividad.getTipoActividad().getMaeDetalleId());
                    return actividadDTO;
                }).collect(Collectors.toList());
    }


    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
    public RespBase<RespCreaBaseCronograma> guardarBaseCronograma(ReqBase<ReqCreaBaseCronograma> request, MyJsonWebToken token,
                                                                  Long basecronogramaId) {
        List<BaseCronograma> ls = new ArrayList<>();

        if (!request.getPayload().getBaseCronogramaDTOList().isEmpty()) {
            validarPeriodosCronograma(request.getPayload().getBaseCronogramaDTOList());
            BaseCronograma b = findBaseCronograma(request.getPayload().getBaseCronogramaDTOList().get(0).getBaseId(), request.getPayload().getBaseCronogramaDTOList().get(0).getEtapaId());
            Optional<MaestraDetalle> etapa = validarCronogramaEtapa(request.getPayload().getBaseCronogramaDTOList().get(0).getEtapaId());
            ls = request.getPayload().getBaseCronogramaDTOList().stream()
                    .map(dto -> {
                        if (b.getBasecronogramaId() == null) {
                            etapa.ifPresent(b::setEtapaId);
                            Optional<Base> base = validarCronogramaBase(dto.getBaseId());
                            base.ifPresent(b::setBaseId);
                            b.setDescripcion(dto.getDescripcion());
                            b.setResponsable(dto.getResponsable());
                            b.setPeriodoini(request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList().get(0).getFechaIni());
                            b.setPeriodofin(request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList().get(0).getFechaFin());

                        } else {
                            List<BaseCronogramaActividad> lt = b.getBaseCronogramaActividadList()
                                    .stream().filter(actividad -> !actividad.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO)).collect(Collectors.toList());
                            BaseCronogramaActividad act = new BaseCronogramaActividad();
                            act.setFechaIni(request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList().get(0).getFechaIni());
                            act.setFechaFin(request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList().get(0).getFechaFin());
                            lt.add(act);

                            lt = orderIni(lt);
                            b.setPeriodoini(lt.get(0).getFechaIni());

                            lt = orderFin(lt);
                            b.setPeriodofin(lt.get(lt.size() - 1).getFechaFin());
                        }

                        b.setBaseCronogramaActividadList(new ArrayList<>());
                        b.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        b.setBaseCronogramaActividadList(settActividades(dto.getActividadDTOList(), b, token));
                        return b;
                    }).collect(Collectors.toList());


            baseCronogramaRepository.saveAll(ls);

            /**
             * QUEUE ETAPA 2
             */
            if (!ls.isEmpty()){
                if (ls.get(0).getBasecronogramaId()!= null){
                    sendQueueEtapa2(ls , b);
                }
            }


            Base base = settFechaPublicacion(b);
            if (base != null) baseRepository.save(base);
        }
        RespCreaBaseCronograma respCreaBaseCronograma = new RespCreaBaseCronograma();
        List<BaseCronogramaDTO> lt = settListCronogramaDto(ls , false);
        respCreaBaseCronograma.setBaseCronogramaDTOList(lt);

        return new RespBase<RespCreaBaseCronograma>().ok(respCreaBaseCronograma);
    }

    /**
     * QUUEE ETAPA 2
     * @param ls
     * @return
     */
      private void sendQueueEtapa2 (List<BaseCronograma> ls , BaseCronograma b) {
          try {
              ReporteConvocatoriaDTO dto = new ReporteConvocatoriaDTO();
              Optional<BaseCronograma> periodoIni = ls.stream().
                      filter(p -> p.getEtapaId().getCodProg().equalsIgnoreCase(Constantes.COD_EST_DIFUSION))
                      .findFirst();
              periodoIni.ifPresent(val -> {
                  dto.setPeriodoIniEtapa(periodoIni.get().getPeriodoini());
              });

              Optional<BaseCronograma> periodoFin = ls.stream().
                      filter(p -> p.getEtapaId().getCodProg().equalsIgnoreCase(Constantes.COD_EST_ELECCION))
                      .findFirst();
              periodoFin.ifPresent(val -> {
                  dto.setPeriodoFinEtapa(periodoFin.get().getPeriodofin());
              });

              dto.setBaseId(b.getBaseId().getBaseId());
              dto.setEtapaQueue(Constantes.ETAPA_BASE_CRONOGRAMA);
              reporteConvocatoriaProducer.sendMessageBase(dto);
          } catch (Exception ex) {
              logger.error(ex.getMessage());
          }
      }


    /**
     * @param b
     * @return
     */
    private Base settFechaPublicacion(BaseCronograma b) {
        if (b.getEtapaId().getCodProg().equalsIgnoreCase(COD_PROG_DIFUSION)) {
            Optional<Base> base = baseRepository.findById(b.getBaseId().getBaseId());
            if (base.isPresent()) {
                base.get().setFechaPublicacion(b.getPeriodoini());
                return base.get();
            }
        }
        return null;
    }

    /**
     * FIND CRONOGRAMA
     *
     * @return
     */
    private BaseCronograma findBaseCronograma(Long idbase, Long idetapa) {
        BaseCronograma filtro = new BaseCronograma();
        Base base = new Base();
        MaestraDetalle maestraDetalle = new MaestraDetalle();
        base.setBaseId(idbase);
        maestraDetalle.setMaeDetalleId(idetapa);
        filtro.setBaseId(base);
        filtro.setEtapaId(maestraDetalle);
        filtro.setEstadoRegistro(Constantes.ACTIVO);
        Example<BaseCronograma> example = Example.of(filtro);
        List<BaseCronograma> listaCronograma = this.baseCronogramaRepository.findAll(example);
        if (!listaCronograma.isEmpty()) return listaCronograma.get(0);
        return new BaseCronograma();
    }

    /**
     * FIND CRONOGRAMA
     *
     * @return
     */
//    private BaseCronogramaHist findBaseCronogramaHistorico(Long idbase, Long idetapa) {
//        BaseCronogramaHist filtro = new BaseCronogramaHist();
//        Base base = new Base();
//        MaestraDetalle maestraDetalle = new MaestraDetalle();
//        base.setBaseId(idbase);
//        maestraDetalle.setMaeDetalleId(idetapa);
//        filtro.setBase(base);
//        filtro.setEtapa(maestraDetalle);
//        filtro.setEstadoRegistro(Constantes.INACTIVO);
//        Example<BaseCronogramaHist> example = Example.of(filtro);
//        List<BaseCronogramaHist> listaCronograma = baseCronogramaHistRepository.findAll(example);
//        if (!listaCronograma.isEmpty()) return listaCronograma.get(0);
//        return new BaseCronogramaHist();
//    }

    /**
     * RETURN LIST BASE CRONOGRAMA
     * CON LOS PERIODOS SETTEADOS
     * SEGUN LAS NUEVAS ACTIVIDADES
     *
     * @return
     */
    private List<BaseCronograma> lstBaseCronoGrama(List<BaseCronogramaActividad> lstActivity, Long idBase) {
        return baseCronogramaRepository.listBaseCronograma(idBase).stream()
                .map(baseCronograma -> {
                    baseCronograma.setPeriodoini(settPeriodoIni(lstActivity, baseCronograma.getEtapaId().getCodProg()));
                    baseCronograma.setPeriodofin(settPeriodoFin(lstActivity, baseCronograma.getEtapaId().getCodProg()));
                    return baseCronograma;
                }).collect(Collectors.toList());
    }

    /**
     * SETT PERIODO ACTIVIDAD INI
     *
     * @param lstActivity
     * @param codProEtapa
     * @return
     */
    private Date settPeriodoIni(List<BaseCronogramaActividad> lstActivity, String codProEtapa) {
        Date ini;
        for (BaseCronogramaActividad it : lstActivity) {
            if (it.getBaseCronograma().getEtapaId().getCodProg().equalsIgnoreCase(codProEtapa)) {
                List<BaseCronogramaActividad> lsIni = lstActivity.stream()
                        .filter(act -> act.getBaseCronograma().getEtapaId().getCodProg().equalsIgnoreCase(codProEtapa))
                        .collect(Collectors.toList());
                lsIni = orderIni(lsIni);
                ini = lsIni.get(0).getFechaIni();
                return ini;
            }
        }

        return null;
    }

    /**
     * SETT PERIODO ACTIVIDAD  FIN
     *
     * @param lstActivity
     * @param codProEtapa
     * @return
     */
    private Date settPeriodoFin(List<BaseCronogramaActividad> lstActivity, String codProEtapa) {
        Date fin;
        for (BaseCronogramaActividad it : lstActivity) {
            if (it.getBaseCronograma().getEtapaId().getCodProg().equalsIgnoreCase(codProEtapa)) {
                List<BaseCronogramaActividad> lsFin = lstActivity.stream()
                        .filter(act -> act.getBaseCronograma().getEtapaId().getCodProg().equalsIgnoreCase(codProEtapa))
                        .collect(Collectors.toList());
                lsFin = orderFin(lsFin);
                fin = lsFin.get(lsFin.size() - 1).getFechaFin();
                return fin;
            }
        }

        return null;
    }


    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
    public RespBase<RespCreaBaseCronograma> updateBaseCronograma(ReqBase<ReqCreaBaseCronograma> request, MyJsonWebToken token, Long basecronogramaId) {
        validarPeriodosCronograma(request.getPayload().getBaseCronogramaDTOList());
        List<BaseCronograma> ls;
        List<BaseCronograma> ltDaysCron = new ArrayList<>();
        Date dateIni = request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList().get(0).getFechaIni();
        Date dateFin = request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList().get(0).getFechaFin();
        List<BaseCronogramaActividad> ltDaysActivity = new ArrayList<>();
        Optional<Base> base = validarCronogramaBase(request.getPayload().getBaseCronogramaDTOList().get(0).getBaseId());
        Optional<MaestraDetalle> etapa = validarCronogramaEtapa(request.getPayload().getBaseCronogramaDTOList().get(0).getEtapaId());
        RespCreaBaseCronograma respCreaBaseCronograma = new RespCreaBaseCronograma();
        if (!request.getPayload().getBaseCronogramaDTOList().isEmpty() && !request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList().isEmpty()) {

            ltDaysActivity = ltDaysActiv(request.getPayload().getBaseCronogramaDTOList());

            if (!ltDaysActivity.isEmpty()) {
                if (etapa.isPresent()) {
                    dateIni = settPeriodoIni(ltDaysActivity, etapa.get().getCodProg());
                    dateFin = settPeriodoFin(ltDaysActivity, etapa.get().getCodProg());
                    if (base.isPresent()) ltDaysCron = lstBaseCronoGrama(ltDaysActivity, base.get().getBaseId());
                }
            }
        }


        Date finalDateIni = dateIni;
        Date finalUltimaActivityDaysAggregates = dateFin;

        ls = request.getPayload().getBaseCronogramaDTOList()
                .stream()
                .map(dto -> {
                    Optional<BaseCronograma> cronograma = baseCronogramaRepository.findById(dto.getCronogramaId().intValue());
                    if (!cronograma.isPresent())
                        throw new NotFoundException("no existe el cronograma con el id: (" + dto.getCronogramaId().intValue() + ") en base de datos");
                    cronograma.ifPresent(baseCronograma -> {
                        baseCronograma.setPeriodoini(finalDateIni);
                        baseCronograma.setPeriodofin(finalUltimaActivityDaysAggregates);
                        etapa.ifPresent(baseCronograma::setEtapaId);
                        base.ifPresent(baseCronograma::setBaseId);
                        baseCronograma.setDescripcion(dto.getDescripcion());
                        baseCronograma.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
                        baseCronograma.setBaseCronogramaActividadList(new ArrayList<>());
                        baseCronograma.setBaseCronogramaActividadList(settActividadesUpdate(dto.getActividadDTOList(), cronograma.get(), token));
                    });
                    return cronograma.get();
                }).collect(Collectors.toList());


        if (!ltDaysActivity.isEmpty()) baseCronoActividadRepository.saveAll(ltDaysActivity);
        if (!ltDaysCron.isEmpty()) baseCronogramaRepository.saveAll(ltDaysCron);
        if (!ls.isEmpty()) baseCronogramaRepository.saveAll(ls);

        List<BaseCronogramaDTO> lt = settListCronogramaDto(ls , false);
        respCreaBaseCronograma.setBaseCronogramaDTOList(lt);
        if (base.isPresent())base.get().setFechaPublicacion(settPeriodoIni(ltDaysActivity, COD_PROG_DIFUSION));
        base.ifPresent(value -> baseRepository.save(value));


        if (request.getPayload().isReprogramacion()) {
            if (base.isPresent()) {
                List<BaseCronogramaHist> lstCronHistory = lstCronHistory(base.get(), token, ltDaysActivity, ltDaysCron);
                baseCronogramaHistRepository.saveAll(lstCronHistory);
            }

        }

        return new RespBase<RespCreaBaseCronograma>().ok(respCreaBaseCronograma);

    }

//    @Override
//    @Transactional(transactionManager = "convocatoriaTransactionManager")
//    public RespBase<RespCreaBaseCronograma> updateBaseCronograma(ReqBase<ReqCreaBaseCronograma> request, MyJsonWebToken token, Long basecronogramaId) {
//        validarPeriodosCronograma(request.getPayload().getBaseCronogramaDTOList());
//        List<BaseCronograma> ls;
//        List<BaseCronograma> ltDaysCrono = new ArrayList<>();
//        Date dateIni = null;
//        Date datefin = null;
//        List<BaseCronogramaActividad> ltDaysActiv = new ArrayList<>();
//        Optional<Base> base = Optional.empty();
//        RespCreaBaseCronograma respCreaBaseCronograma = new RespCreaBaseCronograma();
//        if (!request.getPayload().getBaseCronogramaDTOList().isEmpty() && !request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList().isEmpty()) {
//            Optional<MaestraDetalle> etapa = maestraDetalleRepository.findById(request.getPayload().getBaseCronogramaDTOList().get(0).getEtapaId());
//            base = validarCronogramaBase(request.getPayload().getBaseCronogramaDTOList().get(0).getBaseId());
//            ltDaysActiv = ltDaysActiv(request.getPayload().getBaseCronogramaDTOList());
//
//            if (!ltDaysActiv.isEmpty()) {
//                //dateIni = dateIniCronograma(request.getPayload().getBaseCronogramaDTOList());
//                //datefin = dateFinCronograma(ltDaysActiv, request.getPayload().getBaseCronogramaDTOList().get(0).getActividadDTOList());
//
//                //ltDaysCrono = diasAgregadosCronograma(request.getPayload().getBaseCronogramaDTOList().get(0).getBaseId(),
//                //request.getPayload().getBaseCronogramaDTOList().get(0).getEtapaId(), datefin);
//                if (etapa.isPresent()) {
//                    dateIni = settPeriodoIni(ltDaysActiv, etapa.get().getCodProg());
//                    datefin = settPeriodoFin(ltDaysActiv, etapa.get().getCodProg());
//                }
//
//                ltDaysCrono = lstBaseCronoGrama(ltDaysActiv, base.get().getBaseId());
//
//
//            } else {
////                List<BaseCronogramaActividad> lt = baseCronoActividadRepository.
////                        findAllActividadesXCronograma(Integer.valueOf(request.getPayload().getBaseCronogramaDTOList().get(0).getCronogramaId().toString())).stream()
////                        .filter(actividad -> !actividad.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO)).collect(Collectors.toList());
////
////                lt = orderIni(lt);
////                dateIni = (lt.get(0).getFechaIni());
////
////                lt = orderFin(lt);
////                datefin = (lt.get(lt.size() - 1).getFechaFin());
//            }
//
//        }
//
//        Date finalDateIni = dateIni;
//        Date finalUltimaActividadDiasAgregados = datefin;
//
//        Optional<Base> finalBase = base;
//        ls = request.getPayload().getBaseCronogramaDTOList()
//                .stream()
//                .map(dto -> {
//                    Optional<MaestraDetalle> etapa = validarCronogramaEtapa(dto);
//                    Optional<BaseCronograma> cronograma = baseCronogramaRepository.findById(dto.getCronogramaId().intValue());
//                    if (!cronograma.isPresent())
//                        throw new NotFoundException("no existe el cronograma con el id: (" + dto.getCronogramaId().intValue() + ") en base de datos");
//                    cronograma.ifPresent(baseCronograma -> {
//                        baseCronograma.setPeriodoini(finalDateIni);
//                        baseCronograma.setPeriodofin(finalUltimaActividadDiasAgregados);
//                        baseCronograma.setEtapaId(etapa.get());
//                        baseCronograma.setBaseId(finalBase.get());
//                        baseCronograma.setDescripcion(dto.getDescripcion());
//                        baseCronograma.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
//                        baseCronograma.setBaseCronogramaActividadList(new ArrayList<>());
//                        baseCronograma.setBaseCronogramaActividadList(settActividadesUpdate(dto.getActividadDTOList(), cronograma.get(), token));
//                    });
//                    return cronograma.get();
//                }).collect(Collectors.toList());
//
//        List<BaseCronogramaDTO> lt = settListCronogramaDto(ls);
//        respCreaBaseCronograma.setBaseCronogramaDTOList(lt);
//
//        // if (!ltDaysActiv.isEmpty()) baseCronoActividadRepository.saveAll(ltDaysActiv);
//
//        //if (!ltDaysCrono.isEmpty()) baseCronogramaRepository.saveAll(ltDaysCrono);
//
//        Optional<BaseCronograma> cro = baseCronogramaRepository.findById(request.getPayload().getBaseCronogramaDTOList().get(0).getCronogramaId().intValue());
//
//        if (cro.isPresent()) {
//            Base baseDatePublication = settFechaPublicacion(cro.get());
//            if (baseDatePublication != null) {
//                //baseRepository.save(base);
//            }
//        }
//
//        //baseCronogramaRepository.saveAll(ls);
//
//        if (request.getPayload().isReprogramacion()) {
//            List<BaseCronogramaHist> lstCronHistory = lstCronHistory(base.get(), token, ltDaysActiv, ltDaysCrono);
//            //baseCronogramaHistRepository.saveAll(lstCronHistory);
//        }
//
//        return new RespBase<RespCreaBaseCronograma>().ok(respCreaBaseCronograma);
//
//    }

    /**
     * @param lt
     * @return
     */
    private List<BaseCronogramaActividad> orderIni(List<BaseCronogramaActividad> lt) {
        Collections.sort(lt, new Comparator<BaseCronogramaActividad>() {
            public int compare(BaseCronogramaActividad o1, BaseCronogramaActividad o2) {
                if (o1.getFechaIni() == null || o2.getFechaIni() == null)
                    return 0;
                return o1.getFechaIni().compareTo(o2.getFechaIni());
            }
        });

        return lt;
    }

    /**
     * @param lt
     * @return
     */
    private List<BaseCronogramaActividad> orderFin(List<BaseCronogramaActividad> lt) {
        Collections.sort(lt, new Comparator<BaseCronogramaActividad>() {
            public int compare(BaseCronogramaActividad o1, BaseCronogramaActividad o2) {
                if (o1.getFechaFin() == null || o2.getFechaFin() == null)
                    return 0;
                return o1.getFechaFin().compareTo(o2.getFechaFin());
            }
        });

        return lt;
    }


    /**
     * @param lt
     * @return
     */
    private List<BaseCronogramaActividad> ltDaysActiv(List<BaseCronogramaDTO> lt ) {
        List<BaseCronogramaActividad> ltDaysActiv = diasAgregadosActividades(lt.get(0).getCronogramaId().intValue(),
                lt.get(0).getActividadDTOList().get(0).getActividadId(),
                lt.get(0).getActividadDTOList().get(0).getFechaFin(), lt.get(0).getActividadDTOList().get(0).getFechaIni(), lt.get(0).getBaseId());
        return ltDaysActiv;
    }

//    /**
//     * @param ltdo
//     * @return
//     */
//    private Date dateIniCronograma(List<BaseCronogramaDTO> ltdo) {
//        Date dateIni = null;
//        List<BaseCronogramaActividad> lsBd = baseCronoActividadRepository.findAllActividadesXCronograma(ltdo.get(0).getCronogramaId().intValue());
//        if (!lsBd.isEmpty()) {
//            if (lsBd.size() == 1) {
//                dateIni = ltdo.get(0).getActividadDTOList().get(0).getFechaIni();
//            } else {
//                lsBd = orderIni(lsBd);
//                dateIni = lsBd.get(0).getFechaIni();
//            }
//        }
//        return dateIni;
//    }

//    /**
//     * @param ltDaysActiv
//     * @param ls
//     * @return
//     */
//    private Date dateFinCronograma(List<BaseCronogramaActividad> ltDaysActiv, List<ActividadDTO> ls) {
//        Date ultimaActividadDiasAgregados;
//        if (!ltDaysActiv.isEmpty()) {
//            ultimaActividadDiasAgregados = ltDaysActiv.get(ltDaysActiv.size() - 1).getFechaFin();
//        } else {
//            ultimaActividadDiasAgregados = ls.get(0).getFechaFin();
//        }
//        return ultimaActividadDiasAgregados;
//    }

    /**
     * RETURN LIST DE BASE CRONOGRAMA HISOTRY
     * PARA CREAR UN NUEVO REGISTRO EN LA TABLA
     * DE HISTORICOS
     *
     * @param base
     * @param token
     * @param lstActivity
     * @param lstCrono
     * @return
     */
    private List<BaseCronogramaHist> lstCronHistory(Base base, MyJsonWebToken token, List<BaseCronogramaActividad> lstActivity, List<BaseCronograma> lstCrono) {

        /*SE INACTIVA LOS HISTORICOS QUE ESTAN EN BD*/
        List<BaseCronogramaHist> listHisBaseDatos = inactivarCronogramaHistorico(base);
        if (!listHisBaseDatos.isEmpty())baseCronogramaHistRepository.saveAll(listHisBaseDatos);

         return lstCrono.stream()
                .map(baseCronograma -> {
                    BaseCronogramaHist his = new BaseCronogramaHist();
                    his.setBasecronogramaId(null);
                    his.setBase(baseCronograma.getBaseId());
                    his.setDescripcion(baseCronograma.getDescripcion());
                    his.setPeriodoini(baseCronograma.getPeriodoini());
                    his.setPeriodofin(baseCronograma.getPeriodofin());
                    his.setEtapa(baseCronograma.getEtapaId());
                    his.setResponsable(baseCronograma.getResponsable());
                    his.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    his.setBaseCronogramaActividadHistList(new ArrayList<>());
                    his.setBaseCronogramaActividadHistList(settListActividadHist(lstActivity , token , his , his.getEtapa().getCodProg()));

                    return his;
                }).collect(Collectors.toList());

    }



    /**
     * SE COMPARA EL PERIODO FINAL DE LA ETAPA
     * EN BACK CON EL QUE VIENE DEL FRONT (BASECRONOGRAMA)
     * ESE NUMERO DE DIAS DE DIFERENCIA SE AGREGA CADA ETAPA
     * SGTE DE LA ETAPA MODIFICADA TANTO AL PERIODO INI AND FIN
     *
     * @param idBase
     * @param idEtapa
     * @param periodoFinFront
     * @return
     */


//    private List<BaseCronograma> diasAgregadosCronograma(Long idBase, Long idEtapa, Date periodoFinFront) {
//        BaseCronograma baseCronograma = findBaseCronograma(idBase, idEtapa);
//        List<BaseCronograma> lt = new ArrayList<>();
//        int days = 0;
//        if (baseCronograma.getBasecronogramaId() != null) {
//            days = DateUtil.diffDias(baseCronograma.getPeriodofin(), periodoFinFront);
//            if (days > 0) {
//                lt = baseCronogramaRepository.findAll((root, cq, cb) -> {
//                    Predicate p = cb.conjunction();
//                    Join<BaseCronograma, Base> baseJ = root.join("baseId");
//                    if (!Util.isEmpty(baseCronograma.getPeriodofin())) {
//                        p = cb.and(p, cb.greaterThan(
//                                cb.function("to_char", String.class, root.get("periodofin"), cb.literal(yyyy_MM_dd)),
//                                DateUtil.fmtDt2(baseCronograma.getPeriodofin(), yyyy_MM_dd)));
//
//                        p = cb.and(p, cb.equal(baseJ.get("baseId"), idBase));
//                        p = cb.and(p, cb.equal(root.get("estadoRegistro"), Constantes.ACTIVO));
//
//                    }
//                    return p;
//                });
//            }
//            if (!lt.isEmpty()) {
//                int finalDays = days;
//                lt.stream()
//                        .map(cronograma -> {
//                            cronograma.setPeriodoini(DateUtil.lessOrMoreDay(cronograma.getPeriodoini(), finalDays));
//                            cronograma.setPeriodofin(DateUtil.lessOrMoreDay(cronograma.getPeriodofin(), finalDays));
//                            return baseCronograma;
//                        }).collect(Collectors.toList());
//            }
//        }
//
//
//        return lt;
//    }

    /**
     * SE COMPARA LA FECHA FINAL DE LA ACTIVIDAD
     * EN BACK CON EL QUE VIENE DEL FRONT (BASECRONOGRAMAACTIVIDAD)
     * ESE NUMERO DE DIAS DE DIFERENCIA SE AGREGA CADA ACTIVIDAD
     * SGTE DE LA ACTIVIDAD MODIFICADA TANTO A LA FECHA  INI AND FIN
     *
     * @param idCronograma
     * @param idActividad
     * @param fechaFinFront
     * @return
     */
    private List<BaseCronogramaActividad> diasAgregadosActividades(Integer idCronograma, Long idActividad, Date fechaFinFront, Date fechaIniFront, Long idBase) {
        Optional<BaseCronogramaActividad> baseCronogramaActividad = baseCronoActividadRepository.findById(idActividad);
        List<BaseCronogramaActividad> ls = new ArrayList<>();
        int days = 0;
        if (baseCronogramaActividad.isPresent()) {
            days = DateUtil.diffDias(baseCronogramaActividad.get().getFechaFin(), fechaFinFront);
            if (days > 0) {
                ls = baseCronoActividadRepository.findAll((root, cq, cb) -> {
                    Predicate p = cb.conjunction();
                    Join<BaseCronogramaActividad, BaseCronograma> cronoJ = root.join("baseCronograma");
                    Base base = new Base();
                    base.setBaseId(idBase);

                    p = cb.and(p, cb.greaterThan(
                            cb.function("to_char", String.class, root.get("fechaFin"), cb.literal(yyyy_MM_dd)),
                            DateUtil.fmtDt2(baseCronogramaActividad.get().getFechaFin(), yyyy_MM_dd)));
                    p = cb.and(p, cb.equal(cronoJ.get("baseId"), base));
                    p = cb.and(p, cb.equal(root.get("estadoRegistro"), Constantes.ACTIVO));
                    return p;
                });

                if (!ls.isEmpty()) {
                    int finalDays = days;
                    ls.stream()
                            .map(actividad -> {
                                actividad.setFechaIni(DateUtil.lessOrMoreDay(actividad.getFechaIni(), finalDays));
                                actividad.setFechaFin(DateUtil.lessOrMoreDay(actividad.getFechaFin(), finalDays));
                                return actividad;
                            }).collect(Collectors.toList());
                }
                ls.addAll(lstLessToActivity(baseCronogramaActividad.get()));
                baseCronogramaActividad.get().setFechaIni(fechaIniFront);
                baseCronogramaActividad.get().setFechaFin(fechaFinFront);
                ls.add(baseCronogramaActividad.get());
            }
        }
        return ls;
    }

    /**
     * LIST OF BASECRONOGRAMAACTIVIDAD MENOR
     * DE LA ACTIVIDAD A REPROGRAMAR
     *
     * @return
     */
    private List<BaseCronogramaActividad> lstLessToActivity(BaseCronogramaActividad baseCronogramaActividad) {
        return baseCronoActividadRepository.findAll((root, cq, cb) -> {
            Predicate p = cb.conjunction();
            Join<BaseCronogramaActividad, BaseCronograma> cronoJ = root.join("baseCronograma");
            Base base = new Base();
            base.setBaseId(baseCronogramaActividad.getBaseCronograma().getBaseId().getBaseId());
            p = cb.and(p, cb.lessThan(
                    cb.function("to_char", String.class, root.get("fechaFin"), cb.literal(yyyy_MM_dd)),
                    DateUtil.fmtDt2(baseCronogramaActividad.getFechaFin(), yyyy_MM_dd)));
            p = cb.and(p, cb.equal(cronoJ.get("baseId"), base));
            p = cb.and(p, cb.equal(root.get("estadoRegistro"), Constantes.ACTIVO));
            return p;
        });
    }


    /**
     * BUSCAR LOS CRONOGRAMAS HISTORICOS(antiguos)
     * SETT EL ESTADO A 0 / PARA DEJAR
     * LOS ACTUALES CON ESTADO 1
     * guardarBaseCronograma
     *
     * @param baseId
     * @return
     */
    private List<BaseCronogramaHist> inactivarCronogramaHistorico(Base baseId) {
        return baseCronogramaHistRepository.findByBase(baseId).stream().peek(cro -> cro.setEstadoRegistro(Constantes.INACTIVO)).collect(Collectors.toList());
    }

    /**
     * SETT VALORES DE CRONOGRAMA HIST
     * guardarBaseCronograma
     *
     * @param ls
     * @param token
     */
//    private List<BaseCronogramaHist> listHistoricoCronograma(List<BaseCronograma> ls, MyJsonWebToken token) {
//        BaseCronogramaHist hist = findBaseCronogramaHistorico(ls.get(0).getBaseId().getBaseId(), ls.get(0).getEtapaId().getMaeDetalleId());
//        return ls.stream()
//                .map(cronograma -> {
//                    if (hist.getBasecronogramaId() == null) {
//                        hist.setBase(cronograma.getBaseId());
//                        hist.setDescripcion(cronograma.getDescripcion());
//                        hist.setEtapa(cronograma.getEtapaId());
//                        Optional<Convocatoria> convocatoria = convocatoriaRepository.findByBase(cronograma.getBaseId());
//                        convocatoria.ifPresent(hist::setConvocatoria);
//                        hist.setDescripcion(cronograma.getDescripcion());
//                        hist.setResponsable(cronograma.getResponsable());
//                        hist.setPeriodofin(cronograma.getPeriodofin());
//                        hist.setPeriodoini(cronograma.getPeriodoini());
//                        hist.setPeriodofin(cronograma.getPeriodofin());
//                    }
//
//                    hist.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
//                    hist.setBaseCronogramaActividadHistList(new ArrayList<>());
//                   // hist.setBaseCronogramaActividadHistList(settListActividadHist(cronograma.getBaseCronogramaActividadList(), token, hist));
//                    return hist;
//                }).collect(Collectors.toList());
//    }

    /**
     * SETT VALORES ACTIVIDADES HIST
     * guardarBaseCronograma
     *
     * @param ls
     * @return
     */
    private List<BaseCronogramaActividadHist> settListActividadHist(List<BaseCronogramaActividad> ls, MyJsonWebToken token, BaseCronogramaHist baseCronogramaHist , String codProEtapa) {
        return ls.stream()
                .map(actividad -> {
                    BaseCronogramaActividadHist hist = new BaseCronogramaActividadHist();
                    if (actividad.getBaseCronograma().getEtapaId().getCodProg().equalsIgnoreCase(codProEtapa)){
                        hist.setFechaIni(actividad.getFechaIni());
                        hist.setFechaFin(actividad.getFechaFin());
                        hist.setHoraFin(actividad.getHoraFin());
                        hist.setHoraIni(actividad.getHoraIni());
                        hist.setRespomsable(actividad.getRespomsable());
                        hist.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        hist.setDescripcion(actividad.getDescripcion());
                        hist.setEstadoRegistro(actividad.getEstadoRegistro());
                        hist.setBaseCronogramaHist(baseCronogramaHist);
                        hist.setTipoActividad(actividad.getTipoActividad());
                        return hist;
                    }
                    return null;

                }).collect(Collectors.toList());
    }

    @Override
    public RespBase<ReqEliminarActividad> eliminarBaseCronograma(ReqBase<ReqEliminarActividad> request, MyJsonWebToken token) {
        ReqEliminarActividad payload = new ReqEliminarActividad();
        Optional<BaseCronogramaActividad> actividad = baseCronoActividadRepository.findById(request.getPayload().getActividadId());
        if (!actividad.isPresent()) {
            throw new NotFoundException("no existe la actividad con el id: (" + request.getPayload().getActividadId() + ") en base de datos");
        }


        actividad.get().setEstadoRegistro(request.getPayload().getEstado());
        baseCronoActividadRepository.save(actividad.get());

        List<BaseCronogramaActividad> ls = listActividades(actividad.get());

        Date DateIniFinal = ls.get(0).getFechaIni();
        Date DateFinFinal = ls.get(ls.size() - 1).getFechaFin();

        Optional<BaseCronograma> baseCronograma = baseCronogramaRepository.findById(actividad.get().getBaseCronograma().getBasecronogramaId());
        baseCronograma.ifPresent(b -> {
            b.setPeriodoini(DateIniFinal);
            b.setPeriodofin(DateFinFinal);
            baseCronogramaRepository.save(baseCronograma.get());
        });

        payload.setActividadId(actividad.get().getActividadId());
        payload.setEstado(actividad.get().getEstadoRegistro());
        return new RespBase<ReqEliminarActividad>().ok(payload);
    }


    /**
     * RETURN LIST ACTIVIDADES
     *
     * @param actividad
     * @return
     */
    private List<BaseCronogramaActividad> listActividades(BaseCronogramaActividad actividad) {
        List<BaseCronogramaActividad> ls = baseCronoActividadRepository.findAll((root, cq, cb) -> {
            Predicate p = cb.conjunction();
            Join<BaseCronogramaActividad, BaseCronograma> cronoJ = root.join("baseCronograma");
            p = cb.and(p, cb.equal(cronoJ.get("basecronogramaId"), actividad.getBaseCronograma().getBasecronogramaId()));
            p = cb.and(p, cb.notEqual(root.get("actividadId"), actividad.getActividadId()));
            p = cb.and(p, cb.equal(root.get("estadoRegistro"), Constantes.ACTIVO));
            cq.orderBy(cb.asc(root.get("actividadId")));
            return p;
        });

        if (ls.isEmpty()) {
            ls = baseCronoActividadRepository.findBybaseCronogramaAndEstadoRegistro(actividad.getBaseCronograma(), Constantes.ACTIVO);
        }
        return ls;
    }

    @Override
    public RespBase<RespCreaBaseCronograma> findAllCronogramas(Long baseId , int history) {
        validarCronogramaBase(baseId);
        RespCreaBaseCronograma respCreaBaseCronograma = new RespCreaBaseCronograma();
        if (history == 1){
            Base base = new Base();
            base.setBaseId(baseId);
            List<BaseCronogramaDTO> lt = baseCronogramaHistRepository.findByBase(base).stream().map(baseCronogramaHist -> {
                BaseCronogramaDTO dto = new BaseCronogramaDTO();
                dto.setCronogramaId(Long.valueOf(baseCronogramaHist.getBasecronogramaId()));
                dto.setBaseId(baseCronogramaHist.getBase().getBaseId());
                dto.setDescripcion(baseCronogramaHist.getDescripcion());
                dto.setPeriodoFin(baseCronogramaHist.getPeriodofin());
                dto.setPeriodoIni(baseCronogramaHist.getPeriodoini());
                dto.setEtapaId(baseCronogramaHist.getEtapa().getMaeDetalleId());
                dto.setCodProg(baseCronogramaHist.getEtapa().getCodProg());
                dto.setActividadDTOList(settActivityDtoFromHistory(baseCronogramaActividadHistRepository.findByBaseCronogramaHistAndEstadoRegistro(baseCronogramaHist ,Constantes.ACTIVO)));
                return dto;
            }).collect(Collectors.toList());
            if (lt.isEmpty()){
                 lt = settListCronogramaDto(baseCronogramaRepository.listBaseCronograma(baseId) , true);
            }
            respCreaBaseCronograma.setBaseCronogramaDTOList(lt);
        }else{
            List<BaseCronogramaDTO> lt = settListCronogramaDto(baseCronogramaRepository.listBaseCronograma(baseId) , true);
            respCreaBaseCronograma.setBaseCronogramaDTOList(lt);
        }

        return new RespBase<RespCreaBaseCronograma>().ok(respCreaBaseCronograma);
    }

    /**
     * SETT ACTIVIDADES FROM HISTORY
     * TO DTO PARA EL SERVICIO DE LOS
     * HISTORICOS
     * @param ls
     * @return
     */
    private List<ActividadDTO> settActivityDtoFromHistory(List<BaseCronogramaActividadHist> ls) {
        return ls.stream()
                .filter(actividad -> !actividad.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
                .map(actividad -> {
                    ActividadDTO actividadDTO = new ActividadDTO();
                    actividadDTO.setActividadId(actividad.getActividadId());
                    actividadDTO.setFechaFin(actividad.getFechaFin());
                    actividadDTO.setFechaIni(actividad.getFechaIni());
                    actividadDTO.setHoraFin(actividad.getHoraFin());
                    actividadDTO.setHoraIni(actividad.getHoraIni());
                    actividadDTO.setResponsable(actividad.getRespomsable());
                    actividadDTO.setEstadoRegistro(actividad.getEstadoRegistro());
                    actividadDTO.setDescripcion(actividad.getDescripcion());
                    actividadDTO.setTipoActividad(actividad.getTipoActividad() == null ? null : actividad.getTipoActividad().getMaeDetalleId());
                    return actividadDTO;
                }).collect(Collectors.toList());
    }
    
    public RespBase<RespPeriodoCronogramaDTO> obtenerCronogramaEtapaBase(Long etapaId, Long baseId){
    	Optional<MaestraDetalle> option = maestraDetalleRepository.findById(etapaId);
    	boolean flagEvaluacion = false;
    	    	
    	if(option.isPresent()) {
    		flagEvaluacion = option.get().getCodProg().equals("3") ? true : false;
    	}
    	
    	RespPeriodoCronogramaDTO response = new RespPeriodoCronogramaDTO();
    	BaseCronograma cronograma = new BaseCronograma(
    										new MaestraDetalle(etapaId), new Base(baseId));
    	Example<BaseCronograma> example = Example.of(cronograma);
		List<BaseCronograma> lstCronograma = baseCronogramaRepository.findAll(example);
		
		if(lstCronograma != null && lstCronograma.size() >0) {
			if(flagEvaluacion) {
				BaseCronogramaActividad actividad = new BaseCronogramaActividad(
								new BaseCronograma(lstCronograma.get(0).getBasecronogramaId()));
						
				Example<BaseCronogramaActividad> exampleDetalle = Example.of(actividad);
				List<BaseCronogramaActividad> lstCronogramaActividad = baseCronoActividadRepository.findAll(exampleDetalle);
				
				if(lstCronogramaActividad != null && lstCronogramaActividad.size() >0){
					for(BaseCronogramaActividad tmp: lstCronogramaActividad) {
						Date fechaActual = ParametrosUtil.getFechaActualToDate();
						Date inicio = new Date(tmp.getFechaIni().getTime());
						Date inicioFormat = ParametrosUtil.getOnlyDate(inicio);
						
						Date fin = new Date(tmp.getFechaFin().getTime());
						Date finFormat = ParametrosUtil.getOnlyDate(fin);
						
						if((fechaActual.after(inicioFormat) || fechaActual.compareTo(inicioFormat)==0) && 
								(fechaActual.before(finFormat) || fechaActual.compareTo(finFormat)==0)) {
							response.setDesEtapa(lstCronograma.get(0).getDescripcion().toUpperCase());
							response.setTipoEvaluacion(tmp.getTipoActividad() == null ? "" : tmp.getTipoActividad().getCodProg());
							response.setDesEvaluacion(tmp.getTipoActividad() == null ? "" : tmp.getTipoActividad().getDescripcion());
							response.setFechaInicio(ParametrosUtil.convertirDateToString(tmp.getFechaIni(),Constantes.FORMATO_FECHA_DD_MM_YYYY));
							response.setFechaFin(ParametrosUtil.convertirDateToString(tmp.getFechaFin(),Constantes.FORMATO_FECHA_DD_MM_YYYY));
						}
					}
				}
				
			}else {
			response.setDesEtapa(lstCronograma.get(0).getDescripcion());
			response.setFechaInicio(ParametrosUtil.convertirDateToString(lstCronograma.get(0).getPeriodoini(),Constantes.FORMATO_FECHA_DD_MM_YYYY));
			response.setFechaFin(ParametrosUtil.convertirDateToString(lstCronograma.get(0).getPeriodofin(),Constantes.FORMATO_FECHA_DD_MM_YYYY));
			}
		}
    	
    	return new RespBase<RespPeriodoCronogramaDTO>().ok(response);
    }

	@Override
	public RespBase<RespEtapaActualVacanteBase> obtenerEtapaActualBase(Long baseId) {
		    	
    	RespEtapaActualVacanteBase response = new RespEtapaActualVacanteBase();
    	
    	BaseCronograma cronograma = new BaseCronograma();
    	cronograma.setBaseId( new Base(baseId));
    	Example<BaseCronograma> example = Example.of(cronograma);
		List<BaseCronograma> lstCronograma = baseCronogramaRepository.findAll(example);
		boolean dentroRango = false;
		
		if(lstCronograma != null && lstCronograma.size() >0) {

			Date fechaActual = ParametrosUtil.getFechaActualToDate();
						
			for (BaseCronograma baseCr : lstCronograma) {
				
				Date inicio = new Date(baseCr.getPeriodoini().getTime());
				Date inicioFormat = ParametrosUtil.getOnlyDate(inicio);
				
				Date fin = new Date(baseCr.getPeriodofin().getTime());
				Date finFormat = ParametrosUtil.getOnlyDate(fin);
				
				if((fechaActual.after(inicioFormat) || fechaActual.compareTo(inicioFormat)==0) && 
						(fechaActual.before(finFormat) || fechaActual.compareTo(finFormat)==0)) {
					response.setEtapaId(baseCr.getEtapaId().getMaeDetalleId());
					response.setEtapa(baseCr.getDescripcion());
					response.setFechaInicio(ParametrosUtil.convertirDateToString(baseCr.getPeriodoini(),Constantes.FORMATO_FECHA_DD_MM_YYYY));
					response.setFechaFin(ParametrosUtil.convertirDateToString(baseCr.getPeriodofin(),Constantes.FORMATO_FECHA_DD_MM_YYYY));
					dentroRango = Boolean.TRUE;
					break;
				}
			}
			
			/*if(!dentroRango) {
				response.setEtapaId(115L);
				response.setEtapa("ELECCIÓN");
			}*/
			
			/** set # de vacantes*/
			RespBase<RespObtieneLista<RespPerfilVacante>> listPerfilVacante = seleccionClient.obtenerPerfilVacante(baseId);
			if (listPerfilVacante.getStatus().getSuccess()) {
				int numVacante = 0;
				for (RespPerfilVacante perfilVacante : listPerfilVacante.getPayload().getItems()) {
					numVacante +=perfilVacante.getVacantes();
				}
				response.setVacante(numVacante);
	        }
			
		}
    	
    	return new RespBase<RespEtapaActualVacanteBase>().ok(response);
	}
}
