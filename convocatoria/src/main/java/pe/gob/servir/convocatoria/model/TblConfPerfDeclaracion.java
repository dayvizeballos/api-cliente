package pe.gob.servir.convocatoria.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tbl_conf_perf_declaracion", schema = "sch_convocatoria")
@NamedQuery(name="TblConfPerfDeclaracion.findAll", query="SELECT t FROM TblConfPerfDeclaracion t")
public class TblConfPerfDeclaracion extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conf_perf_declaracion_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_declaracion")
    @SequenceGenerator(name = "seq_conf_perf_declaracion", sequenceName = "seq_conf_perf_declaracion", schema = "sch_convocatoria", allocationSize = 1)
    private Long confPerfDeclaracionId;

    @ManyToOne
    @JoinColumn(name = "perfil_id", nullable = false)
    private Perfil perfil;

    @Column(name = "descripcion", nullable = false)
    private String descripcion;

    @Column(name = "requisito_minimo")
    private Boolean requisitoMinimo;

    @Column(name = "flag_dj")
    private Long flagDj;

    @Column(name = "declaracion_id")
    private Long declaracionId;

    @Column(name = "tipo_id")
    private Long tipoId;

    @Column(name = "orden")
    private Long orden;

    @Column(name = "estado")
    private String estado;

    @Column(name = "is_servir")
    private String isServir;

    @Column(name = "base_id")
    private Long baseId;

    public TblConfPerfDeclaracion() { super(); }
}
