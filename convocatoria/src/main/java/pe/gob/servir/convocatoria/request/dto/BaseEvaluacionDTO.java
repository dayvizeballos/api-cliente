package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JGlobalMap
@Getter
@Setter
public class BaseEvaluacionDTO {

	private Long baseEvaluacionId;
	private Long baseId;
	private String observacion;
	private List<BaseEvaluacionDetalleDTO> baseEvaluacionDetalleDTOList;
}
