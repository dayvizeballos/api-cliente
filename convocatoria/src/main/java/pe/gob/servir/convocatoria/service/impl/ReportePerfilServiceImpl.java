package pe.gob.servir.convocatoria.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;

import ch.qos.logback.core.joran.conditional.IfAction;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.common.VariablesSistema;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.model.CarreraFormacionAcademica;
import pe.gob.servir.convocatoria.model.CarreraProfesional;
import pe.gob.servir.convocatoria.model.Conocimiento;
import pe.gob.servir.convocatoria.model.FormacionAcademica;
import pe.gob.servir.convocatoria.model.FuncionDetalle;
import pe.gob.servir.convocatoria.model.MaestraConocimiento;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.Perfil;
import pe.gob.servir.convocatoria.model.PerfilExperiencia;
import pe.gob.servir.convocatoria.model.PerfilExperienciaDetalle;
import pe.gob.servir.convocatoria.model.PerfilFuncion;
import pe.gob.servir.convocatoria.model.PerfilGrupo;
import pe.gob.servir.convocatoria.model.ReportePerfil;
import pe.gob.servir.convocatoria.model.ReportePerfilActividades;
import pe.gob.servir.convocatoria.model.ReportePerfilConocimiento;
import pe.gob.servir.convocatoria.model.ReportePerfilExDetalle;
import pe.gob.servir.convocatoria.model.ReportePerfilFormacion;
import pe.gob.servir.convocatoria.repository.CarreraFormacionAcademicaRepository;
import pe.gob.servir.convocatoria.repository.ConocimientoRepository;
import pe.gob.servir.convocatoria.repository.FormacionAcademicaRepository;
import pe.gob.servir.convocatoria.repository.FuncionDetalleRepository;
import pe.gob.servir.convocatoria.repository.MaestraConocimientoRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.PerfilExperienciaRepository;
import pe.gob.servir.convocatoria.repository.PerfilFuncionRepository;
import pe.gob.servir.convocatoria.repository.PerfilGrupoRepository;
import pe.gob.servir.convocatoria.repository.ReportePerfilRepository;
import pe.gob.servir.convocatoria.request.dto.ConocimientoDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespEntidad;
import pe.gob.servir.convocatoria.service.ReportePerfilService;
import pe.gob.servir.convocatoria.util.Util;

@Service

public class ReportePerfilServiceImpl  implements ReportePerfilService{

	
	@Autowired
	private ReportePerfilRepository reporteRepository;
	
	@Autowired
	private MaestraDetalleRepository maeRepository;
	
	@Autowired
	private PerfilGrupoRepository grupoRepository;
	
	@Autowired
	private EntidadClient entidadRepository;
	
	@Autowired
	private PerfilExperienciaRepository perfilExperienciaRepository;
	
	@Autowired
	private PerfilFuncionRepository perfilFuncionRepository;
	
	@Autowired
	private ConocimientoRepository conocimientoRepository;
	
	@Autowired
	private MaestraConocimientoRepository maeConocimientoRepository;
	
	@Autowired
	private FormacionAcademicaRepository formacionAcademicaRepository;
	
	@Autowired
	private CarreraFormacionAcademicaRepository carreraFormacionRepository;
	
	@Autowired
	VariablesSistema variablesSistema;
	
	public byte[] generatePdfFromHtml(Long id) throws DocumentException, IOException {
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ITextRenderer rendererx = new ITextRenderer();
		rendererx.setDocumentFromString(parseThymeleafTemplate(id));
		rendererx.layout();
		rendererx.createPDF(baos);
		 
		byte[] byteArray = baos.toByteArray();
		baos.close();
		return byteArray;
	}
	

	public String parseThymeleafTemplate(Long id) {
		ReportePerfil reporte =findReportePerfil(id);
		String html="";
		List<MaestraDetalle> lstMaestraRegimen;
			 //-----------------------------------------------------------------
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);

	    Context context = new Context();
	    
	    
	    lstMaestraRegimen = maeRepository.findDetalleByCodeCabecera(Constantes.COD_REGIMEN);

		Map<Long, String> mapDetalleCodigo = lstMaestraRegimen.stream().filter(Objects::nonNull)
					.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));
	    
	   // switch(reporte.getRegimenId().intValue()) {
	    switch(mapDetalleCodigo.get(reporte.getRegimenId())) {
	    		case Constantes.REGIMEN_LABORAL_30057:
	    			html = "Reporte30057";break;
	    			
	    		case Constantes.REGIMEN_LABORAL_1401://practica
	    			html = "ReportePractica";break;

	    		default:
	    			html = "ReporteDL";break;
	    }
	    
	    context.setVariable("fechaActual", new Date());
	    context.setVariable("reporte", reporte);

	    return templateEngine.process(html, context);
	}
	
	@Override
	public ReportePerfil findReportePerfil(Long id) {
		Perfil perfil = null;
		
		Optional<Perfil> perfilFind = reporteRepository.findById(id);
		if(perfilFind.isPresent()){
			 perfil =  perfilFind.get();
		}
		
		 //Perfil perfil = reporteRepository.findEvaluacion(id);
		 ReportePerfil reporte = new ReportePerfil(); 
		 List<MaestraDetalle> listamae = new ArrayList<>();
		 
		 
		 List<FormacionAcademica> listaformacionAcademica = new ArrayList<>();
		 List<CarreraFormacionAcademica> listaCarreraFormacion = new ArrayList<>();
		 List<FuncionDetalle> listafuncion;
		 List<CarreraProfesional> listacarrera= new ArrayList<>();
		 List<Conocimiento> listaconocimientos = new ArrayList<>();
		 List<PerfilExperienciaDetalle> listaexpdet;
		 List<PerfilExperiencia> perfilListexp;
		 List<PerfilFuncion> perfilListFuncion;
		 PerfilFuncion perfilfun = null;
		 PerfilExperiencia perfilexp = null;
		 String nn= "Ninguno";

		 if(perfil!=null) {
			
			reporte.setRegimenId(perfil.getRegimenLaboralId()); 
			//reporte.setLogo(obtenerLogoPorEntidadId(perfil.getEntidadId())); jleone
			reporte.setEntidadId(perfil.getEntidadId());
			
			 listamae = maeRepository.findAll();
			 Map<Long, String> mapDetalle = listamae.stream().filter(Objects::nonNull)
                   .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
			 
		    //listamae.addAll(maeRepository.findAll());
			
		    //Obtener Experiencia
		    PerfilExperiencia perfilExp = new PerfilExperiencia();
		    perfilExp.setPerfilId(perfil.getPerfilId());
		    perfilExp.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		    Example<PerfilExperiencia> exampleExperiencia = Example.of(perfilExp);
		    perfilListexp = perfilExperienciaRepository.findAll(exampleExperiencia);
			if(perfilListexp != null && !perfilListexp.isEmpty()) {
				perfilexp = perfilListexp.get(0);
			} 
		    //Obtener Funcion
			PerfilFuncion perfilFun =  new PerfilFuncion();
			perfilFun.setPerfilId(perfil.getPerfilId());
			perfilFun.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
			Example<PerfilFuncion> exampleFuncion = Example.of(perfilFun);
			perfilListFuncion = perfilFuncionRepository.findAll(exampleFuncion);
			if(perfilListFuncion != null && !perfilListFuncion.isEmpty()) {
				perfilfun = perfilListFuncion.get(0);
			} 
			 
			// perfilexp = reporteRepository.findExperiencia(id);
			 //perfilfun = reporteRepository.findCondiciones(id);
			 
			 String practica="";
			 String condicion="";
			 String nivel="";
			 String puesto="";
			 String nivelmin="";
			 String periodicidad="";
		 for (int i = 0; i < listamae.size(); i++) {
			 	practica=devolverDescripcion(listamae, perfil.getTipoPracticaId(), i,practica);
			 	reporte.setTipopracticas(practica);

			 	condicion=devolverDescripcion(listamae, perfil.getCondicionPracticaId(), i ,condicion);
			 	reporte.setCondicion(condicion);

			 	nivelmin=(perfilexp!=null)?devolverDescripcion(listamae, perfilexp.getNivelMinPueId(), i,nivelmin):"";
				 reporte.setNivelminimo(nivelmin);

				 periodicidad=(perfilfun!=null)?devolverDescripcion(listamae, perfilfun.getPeriocidadCondicionAtipicaId(), i,periodicidad):"";
				 reporte.setCondperiodicidad(periodicidad);

				 nivel=devolverDescripcion(listamae, perfil.getNivelCategoriaId(), i,nivel);
				 reporte.setNivel(nivel);

				 puesto=devolverDescripcion(listamae, perfil.getPuestoTipoId(), i,puesto);
				 reporte.setPuestotipo(puesto);	  
		}
		 reporte.setColegiatura(validacionSINO(perfil.getIndColegiatura()));
		 reporte.setHabprofesional(validacionSINO(perfil.getIndHabilitacionProf()));
		 
		 reporte.setPuestoestructural(perfil.getPuestoEstructural());
		 reporte.setNombrepuesto(perfil.getNombrePuesto());
		 reporte.setOrgano(perfil.getNombreOrgano());
		 reporte.setUnidadorganica(perfil.getUnidadOrganica());
		 reporte.setUnidadfuncional(perfil.getUnidadFuncional());
		 reporte.setNivelorganizacional(perfil.getNivelOrganizacional());
		 
		 
		 reporte.setServidores(devolverDescripcionGrupo(perfil.getServidorCivilId()));
		 reporte.setFamilia(devolverDescripcionGrupo(perfil.getFamiliaPuestoId()));
		 reporte.setRol(devolverDescripcionGrupo(perfil.getRolId()));
	 
		 
		 
		 reporte.setSubnivel(perfil.getSubNivelsubCategoria());
		 reporte.setCodigopuesto(perfil.getPuestoCodigo());
		  
		  reporte.setPosicionespuesto((perfil.getNroPosicionesPuesto()!=null)?devolverCantidad(perfil.getNroPosicionesPuesto().toString()):nn);
	  
		  
		 reporte.setCodigoposiciones(perfil.getCodigoPosicion());
		 reporte.setDependenciajerarquica(perfil.getDependenciaJerarquica());
		 reporte.setDependenciafuncional(perfil.getDependenciaFuncional());
		 reporte.setServidoresreporta(devolverDescripcionGrupo(perfil.getServidorCivilReporteId()));
		  
		  reporte.setNroposicionescargo((perfil.getNroPosicionesCargo()!=null)?devolverCantidad(perfil.getNroPosicionesCargo().toString()):nn);
		  reporte.setPosicionescargo((perfil.getPuestosCargo()!=null)?devolverCantidad(perfil.getPuestosCargo()):nn);		   
		  reporte.setMision(perfil.getMisionPuesto());

		  //listafuncion=reporteRepository.findActividades(id);
		  //listafuncion =  perfilfun.getFuncionDetalles();
		  if(perfilfun!=null) {
			  listafuncion = perfilFuncionRepository.listFuncDetalleByFuncion(perfilfun.getPerfilFuncionId());
			  reporte.setActividades(getActividades(listafuncion));
		  }
		
		   Map <Integer, String> mapcond= getCondicion(perfilfun);
		   
		   	 reporte.setCondAtipica(mapcond.get(1));
			 reporte.setSustentoperiodicidad(mapcond.get(2));	
			 reporte.setCondcoorinternas(mapcond.get(3));
			 reporte.setCondcoorexternas(mapcond.get(4));
			 reporte.setGruposervidores(mapcond.get(5));
			 
			 /*listaformacionAcademica.addAll(null);///ROOLBACK
			 listacarrera.addAll(null); //ROOLBACK
			  */
			 	FormacionAcademica formacion = new FormacionAcademica();
			 	formacion.setPerfil(perfil);
			 	formacion.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
			    Example<FormacionAcademica> exampleFormacion = Example.of(formacion);
			    listaformacionAcademica = formacionAcademicaRepository.findAll(exampleFormacion);
			    
			 //listaformacionAcademica.addAll(reporteRepository.findFormacionAcademica(id));
			 //listacarrera.addAll(reporteRepository.findFormacionAcademicaCarrera(id));
			 //jleone
			 //reporte.setFormaciones(devolverFormacionAcademica(listaformacionAcademica, listamae, listacarrera));
			  reporte.setFormaciones(devolverFormacionAcademica(listaformacionAcademica, mapDetalle));
			 
		  
			 listaconocimientos = conocimientoRepository.findListByPerfil(perfil.getPerfilId());
			 //listaconocimientos.addAll(reporteRepository.findConocimiento(id));
			 
			 Map<Integer, List<ReportePerfilConocimiento>> mapConocimientos= devolverConocimientos(listaconocimientos);
			 
			 /*reporte.setConocimientos(mapConocimientos.get(1));
			 reporte.setIdiomas(mapConocimientos.get(2));
			 */
			 reporte.setConocimientosTecnicos(mapConocimientos.get(1));
			 reporte.setConocimientosCursos(mapConocimientos.get(2));
			 reporte.setConocimientosProgramas(mapConocimientos.get(3));
			 reporte.setConocimientosOfimatica(mapConocimientos.get(4));
			 reporte.setConocimientosIdioma(mapConocimientos.get(5));
			 
			    Map<Integer, String> map = perfilExp(perfilexp);
			 	 reporte.setAnioexptotal(Integer.parseInt(map.get(1)));
				 reporte.setMesexptotal(Integer.parseInt(map.get(2)));
				 reporte.setAnioexprequerida(Integer.parseInt(map.get(3)));
				 reporte.setMesexprequerida(Integer.parseInt(map.get(4))); 
				 reporte.setAspectoscomplementarios(map.get(5));
				 reporte.setAnioexppublico(Integer.parseInt(map.get(6)));	
				 reporte.setMesexppublico(Integer.parseInt(map.get(7)));
			 
			 
			 //listaexpdet= perfilexpreporteRepository.findExpDetalle(id);// 1 habilidades 2 requisitos
				if(perfilexp!=null) {				
					listaexpdet = perfilexp.getPerfilExperienciaDetalleList1();
					reporte.setExpdetalleHabilidades(getHabReq(listaexpdet, Long.valueOf(1)));
					reporte.setExpdetalleRequisitos(getHabReq(listaexpdet, Long.valueOf(2)));
				}
			 
		 
			 
		 }
		
			
		return reporte;
	}
	
	private String devolverDescripcionGrupo(Long id) {
		 
		String output="";
		 if(id!=null) {
			 
			Optional<PerfilGrupo> serv= grupoRepository.findById(id);
			 if(serv.isPresent()) {
				 output=serv.get().getDescripcion();
			 } 
		 } 
		 return output;
	}
	
	private String devolverDescripcion(List<MaestraDetalle> listamae,Long id,int index, String valor) {
		String output="";
		if(valor.isEmpty()) {	
			if(id!=null && listamae.get(index).getMaeDetalleId().equals(id)) {
					output=listamae.get(index).getDescripcion();
			}
		}else {
			output=valor;
		}
		return output;
	}
	
	private String validacionSINO(String id) {
		String output="Si";
		if(id!=null && id.equals("0")) {
				 output="No";
		 }
		return output;
	}
	
	private String devolverCantidad(String valor) {
		String output="Ninguno";
		  
			  if(valor!=null && Util.isNumeric(valor) && Integer.parseInt(valor)!= 0) {
				   output=valor;  
			  }
 
		  return output;
	}
	
	private StringBuilder devolverGrupoServidores(String gposervidores) {
		StringBuilder salidaServidores=new StringBuilder();
		if(gposervidores!=null) {
			for (int i = 0; i < gposervidores.length(); i++) {
				String a = gposervidores.charAt(i)+"";
				if(Util.isNumeric(a)) { 
					Optional<PerfilGrupo> gpo= grupoRepository.findById(Long.valueOf(a));
					if(gpo.isPresent()) {
						salidaServidores.append(gpo.get().getDescripcion());
					}
					
				}else {
					salidaServidores.append(a+" ");
				}
			}
		}
		return salidaServidores;
	}	
	
	private List<ReportePerfilActividades> getActividades(List<FuncionDetalle> listafuncion) {
		ReportePerfilActividades actividades;
		List<ReportePerfilActividades> listaactividades = new ArrayList<>();
		 if(!listafuncion.isEmpty()) {
			  for(int i=0;i<listafuncion.size();i++) {
					 actividades = new ReportePerfilActividades();
					 actividades.setDescripcion((i+1)+". "+listafuncion.get(i).getDescripcion());
					 listaactividades.add(actividades);
				 }			 
		  }
		 return listaactividades;

	}
	
	private Map<Integer,String> getCondicion(PerfilFuncion perfilfun) {
		Map<Integer, String> map = new HashMap<>();
		 if(perfilfun!=null) {
			 map.put(1, perfilfun.getCondicionAticipica());
			 map.put(2, perfilfun.getSustentoCondicionAtipica());
			 map.put(3, perfilfun.getCoordinacionInterna());
			 map.put(4, perfilfun.getCoordinacionExterna());
			 map.put(5, devolverGrupoServidores(perfilfun.getServidorCiviles()).toString());
		 }else {
			 map.put(1, "");
			 map.put(2, "");
			 map.put(3, "");
			 map.put(4, "");
			 map.put(5, "");
		 }
		 return map;
	}
	
	private List<ReportePerfilFormacion> devolverFormacionAcademica(List<FormacionAcademica> listaformacionAcademica,
			 Map<Long, String> mapDetalle ){
			/*List<MaestraDetalle> listamae,
			List<CarreraProfesional> listacarrera) {*/
		/*ReportePerfilFormacion formacion;
		String carreras = null;
		
		List<CarreraFormacionAcademica> listaCarreraPorFormacion = new ArrayList<>();
		List<CarreraFormacionAcademica> listaCarreraFormacion = new ArrayList<>();
		  CarreraFormacionAcademica carrera = new CarreraFormacionAcademica();
		  */
		  List<ReportePerfilFormacion> listaformacion = new ArrayList<>();

		 /* for(int i=0;i<listaformacionAcademica.size();i++) {
			  for(CarreraFormacionAcademica c : listaformacionAcademica.get(i).getCarreraFormacionAcademicas()) {
				  carreras += c.getCarreraProfesional().getDescripcion() + "," ;
				  carreras = carreras.substring(carreras.length() - 1, carreras.length()).equals(",") ? carreras.substring(0, carreras.length() - 1) : carreras;
			  }
			  
			  //listaCarreraPorFormacion = carreraFormacionRepository.findListByFormacionAca(listaformacionAcademica.get(i).getFormacionAcademicaId());
			  //listaCarreraFormacion.addAll(listaCarreraPorFormacion);
		  }  */
		 
		 listaformacion = listaformacionAcademica.stream()
				  .filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
						  .map(f -> {
							  ReportePerfilFormacion reporteformacion = new ReportePerfilFormacion();
							  reporteformacion.setGrado(f.getNombreGrado());
							  reporteformacion.setCarreras(this.getCarreras(f.getCarreraFormacionAcademicas()));
							  reporteformacion.setNiveleducativo(mapDetalle.get(f.getNivelEducativoId()));
							  reporteformacion.setSituacion(mapDetalle.get(f.getSituacionAcademicaId()));
							  reporteformacion.setEstado(mapDetalle.get(f.getEstadoNivelEducativoId()));
							  return reporteformacion;  
						  }).collect(Collectors.toList());
	   	    		
	/*	listaformacion = listaformacionAcademica.stream()
				.filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
				.map(con -> {
					ReportePerfilConocimiento conocimiento = new ReportePerfilConocimiento();
					 conocimiento.setDescripcion(mapMaeConocimiento.get(con.getMaeConocimiento().getMaeConocimentoId()));
					 conocimiento.setHoras(conocimiento.getHoras() == null ? null : conocimiento.getHoras());
					 conocimiento.setNiveldominio(conocimiento.getNiveldominio() == null ? null : this.getNivelDominio(conocimiento.getNiveldominio()));
					 conocimiento.setTipo(mapDetalleCodigo.get(con.getMaeConocimiento().getTipoConocimientoId()));
					 return conocimiento;
				}).collect(Collectors.toList());
	*/	
		
		 /*for(int i=0;i<listaformacionAcademica.size();i++) {
			 formacion = new ReportePerfilFormacion();
			 formacion.setGrado(listaformacionAcademica.get(i).getNombreGrado());
			 formacion.setCarreras(listacarrera.get(i).getDescripcion());
			 for (int j = 0; j < listamae.size(); j++) {
				 if(listamae.get(j).getMaeDetalleId().equals(listaformacionAcademica.get(i).getNivelEducativoId()) ) {
					 formacion.setNiveleducativo(listamae.get(j).getDescripcion());
				 }
				 if(listamae.get(j).getMaeDetalleId().equals(listaformacionAcademica.get(i).getEstadoNivelEducativoId()) ) {
					 formacion.setEstado(listamae.get(j).getDescripcion());
				 }
				 if(listamae.get(j).getMaeDetalleId().equals(listaformacionAcademica.get(i).getEstadoSituacionAcademicaId()) ) {
					 formacion.setSituacion(listamae.get(j).getDescripcion());
				 }
				 
			}
			 listaformacion.add(formacion);
		 }	
		 
		 */
		 return listaformacion;
	}
	
	private String getCarreras (List<CarreraFormacionAcademica> lstFormacionCarrera) {
		String carreras = "";
		List<CarreraFormacionAcademica> lstCarreraActivas;
		lstCarreraActivas = lstFormacionCarrera.stream().filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
				.collect(Collectors.toList());
		 for(CarreraFormacionAcademica c : lstCarreraActivas) {
			  carreras += c.getCarreraProfesional().getDescripcion() + "," ;
		  }
		 if(!carreras.isEmpty()) {
			 carreras = carreras.substring(carreras.length() - 1, carreras.length()).equals(",") ? carreras.substring(0, carreras.length() - 1) : carreras;
		 }
		return carreras;
	}
	
	private Map<Integer, List<ReportePerfilConocimiento>> devolverConocimientos(List<Conocimiento> listaconocimientos){
		Map<Integer, List<ReportePerfilConocimiento>> map = new HashMap<>();
		//ReportePerfilConocimiento conocimiento;
		List<MaestraDetalle> lstMaestraDetalle;
		List<ReportePerfilConocimiento> lstConocimiento = new ArrayList<>();
		
		/*List<ReportePerfilConocimiento> lstConTecnico = new ArrayList<>();
		List<ReportePerfilConocimiento> lstConCursor = new ArrayList<>();
		List<ReportePerfilConocimiento> lstConOfimatica = new ArrayList<>();
		List<ReportePerfilConocimiento> lstConPrograma = new ArrayList<>();
		List<ReportePerfilConocimiento> lstConIdioma = new ArrayList<>();
*/
		 List<MaestraConocimiento> listaMaeConocimiento = maeConocimientoRepository.findAll();
		 Map<Long, String> mapMaeConocimiento = listaMaeConocimiento.stream().filter(Objects::nonNull)
				 .collect(Collectors.toMap(MaestraConocimiento::getMaeConocimientoId,MaestraConocimiento::getDescripcion));
		 
		 lstMaestraDetalle = maeRepository.findDetalleByCodeCabecera(Constantes.COD_TIPO_CONOCIMIENTO);

		Map<Long, String> mapDetalleCodigo = lstMaestraDetalle.stream().filter(Objects::nonNull)
					.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));
		 
//       Map<Long, String> mapDetalle = listaMaeDetalle.stream().filter(Objects::nonNull)
//               .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
		
		if(!listaconocimientos.isEmpty()) {
			
			lstConocimiento = listaconocimientos.stream()
					.filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
					.map(con -> {
						ReportePerfilConocimiento conocimiento = new ReportePerfilConocimiento();
						 conocimiento.setDescripcion(con.getMaeConocimiento().getMaeConocimentoId() == null ? null : mapMaeConocimiento.get(con.getMaeConocimiento().getMaeConocimentoId()));
						 conocimiento.setHoras(con.getHoras() == null ? null : con.getHoras());
						 conocimiento.setNiveldominio(con.getNivelDominioId() == null ? null : this.getNivelDominio(con.getNivelDominioId().toString()));
						 conocimiento.setTipo(mapDetalleCodigo.get(con.getMaeConocimiento().getTipoConocimientoId()));
						 return conocimiento;
					}).collect(Collectors.toList());
			
			
		/*	 for(int i=0;i<listaconocimientos.size();i++) {
				 conocimiento = new ReportePerfilConocimiento();
				 conocimiento.setDescripcion(listaconocimientos.get(i).getDescripcionConocimiento());
				 String v="";
				 int valor =0 ;
				 if(listaconocimientos.get(i).getNivelDominioId()!=null) {
					  valor= listaconocimientos.get(i).getNivelDominioId().intValue();
				 }
				 switch(valor) {
				 case 1:
					 v="Básico";
					 break;
				 case 2 :
					 v="Intermedio";
					 break;
				 case 3:
					 v="Avanzado";
					 break;
				default:
					 v="No Aplica";
					 break;
					 }
				 
				 conocimiento.setNiveldominio(v);
				 if(listaconocimientos.get(i).getTipoConocimientoId()==5) {
					 listaconocimientoIdiomas.add(conocimiento);
				 }else {
					 listaconocimiento.add(conocimiento);
				 }

			 	}*/
			 }
				map.put(1,
						lstConocimiento.stream()
								.filter(val -> val.getTipo().equalsIgnoreCase(Constantes.COD_CONOCIMIENTO_TECNICO))
								.collect(Collectors.toList()));
				map.put(2,
						lstConocimiento.stream()
								.filter(val -> val.getTipo().equalsIgnoreCase(Constantes.COD_CONOCIMIENTO_CURSOS))
								.collect(Collectors.toList()));
				
				map.put(3,
						lstConocimiento.stream()
								.filter(val -> val.getTipo().equalsIgnoreCase(Constantes.COD_CONOCIMIENTO_PROGRAMA))
								.collect(Collectors.toList()));
				
				map.put(4,
						lstConocimiento.stream()
								.filter(val -> val.getTipo().equalsIgnoreCase(Constantes.COD_CONOCIMIENTO_OFIMATICA))
								.collect(Collectors.toList()));
				
				map.put(5,
						lstConocimiento.stream()
								.filter(val -> val.getTipo().equalsIgnoreCase(Constantes.COD_CONOCIMIENTO_IDIOMA))
								.collect(Collectors.toList()));
		
		return map;	
	}
	
	public String getNivelDominio (String nivelDominioId) {
		
		 String valor="";
		
		 switch(nivelDominioId) {
		 case "1":
			 valor="Básico";
			 break;
		 case "2" :
			 valor="Intermedio";
			 break;
		 case "3":
			 valor="Avanzado";
			 break;
		default:
			valor="No Aplica";
			 break;
			 }
		 
		 return valor;
	}
	
	private Map<Integer, String> perfilExp(PerfilExperiencia perfilexp){
		Map<Integer, String> map = new HashMap<>();
		 if(perfilexp!=null) {
			 map.put(1, perfilexp.getAnioExpTotal().toString());
			 map.put(2,perfilexp.getMesExpTotal().toString());
			 map.put(3,perfilexp.getAnioExReqPuesto().toString());
			 map.put(4,perfilexp.getMesExReqPuesto().toString());
			 map.put(5,perfilexp.getAspectos());
			 map.put(6,perfilexp.getAnioExpSecPub().toString());	
			 map.put(7,perfilexp.getMesExpSecPub().toString());
		 }else {
			 map.put(1, "0");
			 map.put(2,"0");
			 map.put(3,"0");
			 map.put(4,"0");
			 map.put(5,"");
			 map.put(6,"0");	
			 map.put(7,"0");
		 }
		return map;	
	}
	
	private List<ReportePerfilExDetalle> getHabReq(List<PerfilExperienciaDetalle> listaexpdet, Long id){
		List<ReportePerfilExDetalle> lista = new ArrayList<>();
		 if(!listaexpdet.isEmpty()) {
			 for (int j = 0; j < listaexpdet.size(); j++) {
				 ReportePerfilExDetalle p = new ReportePerfilExDetalle();
				 p.setDescripcion(listaexpdet.get(j).getDescripcion());
				if(listaexpdet.get(j).getTiDaExId().equals(id)) { // 1 habilidades 2 requisitos	
					lista.add(p);
				}
			}
		 }
		 return lista;
	}
	
/*
	 @Override
	public ReportePerfil findReportePerfil(Long id) {



		 Perfil perfil = reporteRepository.findEvaluacion(id);
		 ReportePerfil reporte = new ReportePerfil(); 
		 List<MaestraDetalle> listamae = new ArrayList<>();
		 
		 
		 List<FormacionAcademica> listaformacionAcademica = new ArrayList<>();
		 List<FuncionDetalle> listafuncion;
		 List<CarreraProfesional> listacarrera= new ArrayList<>();
		 List<Conocimiento> listaconocimientos = new ArrayList<>();
		 List<PerfilExperienciaDetalle> listaexpdet;
		 PerfilExperiencia perfilexp;
		 PerfilFuncion perfilfun;
		 String nn= "Ninguno";

		 if(perfil!=null) {
			
			reporte.setRegimenId(perfil.getRegimenLaboralId()); 
			//reporte.setLogo(obtenerLogoPorEntidadId(perfil.getEntidadId()));
			reporte.setEntidadId(perfil.getEntidadId());
			 
			 listamae.addAll(maeRepository.findAll());
			 perfilexp = reporteRepository.findExperiencia(id);
			 perfilfun = reporteRepository.findCondiciones(id);
			 
			 String practica="";
			 String condicion="";
			 String nivel="";
			 String puesto="";
			 String nivelmin="";
			 String periodicidad="";
		 for (int i = 0; i < listamae.size(); i++) {
			 	practica=devolverDescripcion(listamae, perfil.getTipoPracticaId(), i,practica);
			 	reporte.setTipopracticas(practica);

			 	condicion=devolverDescripcion(listamae, perfil.getCondicionPracticaId(), i ,condicion);
			 	reporte.setCondicion(condicion);

			 	nivelmin=(perfilexp!=null)?devolverDescripcion(listamae, perfilexp.getNivelMinPueId(), i,nivelmin):"";
				 reporte.setNivelminimo(nivelmin);

				 periodicidad=(perfilfun!=null)?devolverDescripcion(listamae, perfilfun.getPeriocidadCondicionAtipicaId(), i,periodicidad):"";
				 reporte.setCondperiodicidad(periodicidad);

				 nivel=devolverDescripcion(listamae, perfil.getNivelCategoriaId(), i,nivel);
				 reporte.setNivel(nivel);

				 puesto=devolverDescripcion(listamae, perfil.getPuestoTipoId(), i,puesto);
				 reporte.setPuestotipo(puesto);	  
		}
		 reporte.setColegiatura(validacionSINO(perfil.getIndColegiatura()));
		 reporte.setHabprofesional(validacionSINO(perfil.getIndHabilitacionProf()));
		 
		 reporte.setPuestoestructural(perfil.getPuestoEstructural());
		 reporte.setNombrepuesto(perfil.getNombrePuesto());
		 reporte.setOrgano(perfil.getNombreOrgano());
		 reporte.setUnidadorganica(perfil.getUnidadOrganica());
		 reporte.setUnidadfuncional(perfil.getUnidadFuncional());
		 reporte.setNivelorganizacional(perfil.getNivelOrganizacional());
		 
		 
		 reporte.setServidores(devolverDescripcionGrupo(perfil.getServidorCivilId()));
		 reporte.setFamilia(devolverDescripcionGrupo(perfil.getFamiliaPuestoId()));
		 reporte.setRol(devolverDescripcionGrupo(perfil.getRolId()));
	 
		 
		 
		 reporte.setSubnivel(perfil.getSubNivelsubCategoria());
		 reporte.setCodigopuesto(perfil.getPuestoCodigo());
		  
		  reporte.setPosicionespuesto((perfil.getNroPosicionesPuesto()!=null)?devolverCantidad(perfil.getNroPosicionesPuesto().toString()):nn);
	  
		  
		 reporte.setCodigoposiciones(perfil.getCodigoPosicion());
		 reporte.setDependenciajerarquica(perfil.getDependenciaJerarquica());
		 reporte.setDependenciafuncional(perfil.getDependenciaFuncional());
		 reporte.setServidoresreporta(devolverDescripcionGrupo(perfil.getServidorCivilReporteId()));
		  
		  reporte.setNroposicionescargo((perfil.getNroPosicionesCargo()!=null)?devolverCantidad(perfil.getNroPosicionesCargo().toString()):nn);
		  
		  
		  
		  reporte.setPosicionescargo((perfil.getPuestosCargo()!=null)?devolverCantidad(perfil.getPuestosCargo()):nn);
		   
		  reporte.setMision(perfil.getMisionPuesto());
		 
	
		  
	 
		  listafuncion=reporteRepository.findActividades(id);
		  reporte.setActividades(getActividades(listafuncion));
		   Map <Integer, String> mapcond= getCondicion(perfilfun);
		   
		   	 reporte.setCondAtipica(mapcond.get(1));
			 reporte.setSustentoperiodicidad(mapcond.get(2));	
			 reporte.setCondcoorinternas(mapcond.get(3));
			 reporte.setCondcoorexternas(mapcond.get(4));
			 reporte.setGruposervidores(mapcond.get(5));
			 
			 listaformacionAcademica.addAll(null);/// 	ROOLBACK
			 listacarrera.addAll(null); //ROOLBACK
			  
			 reporte.setFormaciones(devolverFormacionAcademica(listaformacionAcademica, listamae, listacarrera));
			 
		  
			  
			 listaconocimientos.addAll(reporteRepository.findConocimiento(id));
			 Map<Integer, List<ReportePerfilConocimiento>> mapConocimientos= devolverConocimientos(listaconocimientos);
			 
			 reporte.setConocimientos(mapConocimientos.get(1));
			 reporte.setIdiomas(mapConocimientos.get(2));
			 
			    Map<Integer, String> map = perfilExp(perfilexp);
			 	 reporte.setAnioexptotal(Integer.parseInt(map.get(1)));
				 reporte.setMesexptotal(Integer.parseInt(map.get(2)));
				 reporte.setAnioexprequerida(Integer.parseInt(map.get(3)));
				 reporte.setMesexprequerida(Integer.parseInt(map.get(4))); 
				 reporte.setAspectoscomplementarios(map.get(5));
				 reporte.setAnioexppublico(Integer.parseInt(map.get(6)));	
				 reporte.setMesexppublico(Integer.parseInt(map.get(7)));
			 
			 
			 listaexpdet= reporteRepository.findExpDetalle(id);// 1 habilidades 2 requisitos	
			 reporte.setExpdetalleHabilidades(getHabReq(listaexpdet, Long.valueOf(1)));
			 reporte.setExpdetalleRequisitos(getHabReq(listaexpdet, Long.valueOf(2)));
		 
			 
		 }
		
			
		return reporte;
	}

	
	public String parseThymeleafTemplate(Long id) {
		ReportePerfil reporte =findReportePerfil(id);
		String html="";
			 //-----------------------------------------------------------------
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);

	    Context context = new Context();
	    
	    switch(reporte.getRegimenId().intValue()) {
	    		case 77:
	    			html = "Reporte30057";break;
	    			
	    		case 80:
	    			html = "ReportePractica";break;

	    		default:
	    			html = "ReporteDL";break;
	    }
	    
	    context.setVariable("fechaActual", new Date());
	    context.setVariable("reporte", reporte);

	    return templateEngine.process(html, context);
	}
	
	public byte[] generatePdfFromHtml(Long id) throws DocumentException, IOException {
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ITextRenderer rendererx = new ITextRenderer();
		rendererx.setDocumentFromString(parseThymeleafTemplate(id));
		rendererx.layout();
		rendererx.createPDF(baos);
		 
		byte[] byteArray = baos.toByteArray();
		baos.close();
		return byteArray;
	}
	
	
	
	private String obtenerLogoPorEntidadId(Long entidadId) {
		 RespBase<RespEntidad> response = entidadRepository.obtieneEntidad(entidadId);
		 String logo="src/main/resources/nologo.png";
		 if(response.getPayload().getEntidad().get(0).getLogo()!=null)
				logo = variablesSistema.fileServer + response.getPayload().getEntidad().get(0).getLogo();
		 return logo;
	}
	 
 
	
	private String devolverDescripcion(List<MaestraDetalle> listamae,Long id,int index, String valor) {
		String output="";
		if(valor.isEmpty()) {	
			if(id!=null && listamae.get(index).getMaeDetalleId().equals(id)) {
					output=listamae.get(index).getDescripcion();
			}
		}else {
			output=valor;
		}
		return output;
	}
	
	private String validacionSINO(String id) {
		String output="Si";
		if(id!=null && id.equals("0")) {
				 output="No";
		 }
		return output;
	}
	
	private String devolverDescripcionGrupo(Long id) {
		 
		String output="";
		 if(id!=null) {
			 
			Optional<PerfilGrupo> serv= grupoRepository.findById(id);
			 if(serv.isPresent()) {
				 output=serv.get().getDescripcion();
			 } 
		 } 
		 return output;
	}
	
	private List<ReportePerfilExDetalle> getHabReq(List<PerfilExperienciaDetalle> listaexpdet, Long id){
		List<ReportePerfilExDetalle> lista = new ArrayList<>();
		 if(!listaexpdet.isEmpty()) {
			 for (int j = 0; j < listaexpdet.size(); j++) {
				 ReportePerfilExDetalle p = new ReportePerfilExDetalle();
				 p.setDescripcion(listaexpdet.get(j).getDescripcion());
				if(listaexpdet.get(j).getTiDaExId().equals(id)) { // 1 habilidades 2 requisitos	
					lista.add(p);
				}
			}
		 }
		 return lista;
	}
	
	private Map<Integer, String> perfilExp(PerfilExperiencia perfilexp){
		Map<Integer, String> map = new HashMap<>();
		 if(perfilexp!=null) {
			 map.put(1, perfilexp.getAnioExpTotal().toString());
			 map.put(2,perfilexp.getMesExpTotal().toString());
			 map.put(3,perfilexp.getAnioExReqPuesto().toString());
			 map.put(4,perfilexp.getMesExReqPuesto().toString());
			 map.put(5,perfilexp.getAspectos());
			 map.put(6,perfilexp.getAnioExpSecPub().toString());	
			 map.put(7,perfilexp.getMesExpSecPub().toString());
		 }else {
			 map.put(1, "0");
			 map.put(2,"0");
			 map.put(3,"0");
			 map.put(4,"0");
			 map.put(5,"");
			 map.put(6,"0");	
			 map.put(7,"0");
		 }
		return map;	
	}

	private String devolverCantidad(String valor) {
		String output="Ninguno";
		  
			  if(valor!=null && Util.isNumeric(valor) && Integer.parseInt(valor)!= 0) {
				   output=valor;  
			  }
 
		  return output;
	}
	
	private StringBuilder devolverGrupoServidores(String gposervidores) {
		StringBuilder salidaServidores=new StringBuilder();
		if(gposervidores!=null) {
			for (int i = 0; i < gposervidores.length(); i++) {
				String a = gposervidores.charAt(i)+"";
				if(Util.isNumeric(a)) { 
					Optional<PerfilGrupo> gpo= grupoRepository.findById(Long.valueOf(a));
					if(gpo.isPresent()) {
						salidaServidores.append(gpo.get().getDescripcion());
					}
					
				}else {
					salidaServidores.append(a+" ");
				}
			}
		}
		return salidaServidores;
	}
	
	private Map<Integer, List<ReportePerfilConocimiento>> devolverConocimientos(List<Conocimiento> listaconocimientos){
		Map<Integer, List<ReportePerfilConocimiento>> map = new HashMap<>();
		ReportePerfilConocimiento conocimiento;
		List<ReportePerfilConocimiento> listaconocimiento = new ArrayList<>();
		 List<ReportePerfilConocimiento> listaconocimientoIdiomas = new ArrayList<>();
		
		if(!listaconocimientos.isEmpty()) { 
			
			 for(int i=0;i<listaconocimientos.size();i++) {
				 conocimiento = new ReportePerfilConocimiento();
				 conocimiento.setDescripcion(listaconocimientos.get(i).getDescripcionConocimiento());
				 String v="";
				 int valor =0 ;
				 if(listaconocimientos.get(i).getNivelDominioId()!=null) {
					  valor= listaconocimientos.get(i).getNivelDominioId().intValue();
				 }
				 switch(valor) {
				 case 1:
					 v="Básico";
					 break;
				 case 2 :
					 v="Intermedio";
					 break;
				 case 3:
					 v="Avanzado";
					 break;
				default:
					 v="No Aplica";
					 break;
					 }
				 
				 conocimiento.setNiveldominio(v);
				 if(listaconocimientos.get(i).getTipoConocimientoId()==5) {
					 listaconocimientoIdiomas.add(conocimiento);
				 }else {
					 listaconocimiento.add(conocimiento);
				 }

			 	}
			 }
		map.put(1, listaconocimiento);
		map.put(2, listaconocimientoIdiomas);
		return map;	
	}
	
	private List<ReportePerfilFormacion> devolverFormacionAcademica(List<FormacionAcademica> listaformacionAcademica,
			List<MaestraDetalle> listamae,
			List<CarreraProfesional> listacarrera) {
		ReportePerfilFormacion formacion;
		List<ReportePerfilFormacion> listaformacion = new ArrayList<>();	
		 for(int i=0;i<listaformacionAcademica.size();i++) {
			 formacion = new ReportePerfilFormacion();
			 formacion.setGrado(listaformacionAcademica.get(i).getNombreGrado());
			 formacion.setCarreras(listacarrera.get(i).getDescripcion());
			 for (int j = 0; j < listamae.size(); j++) {
				 if(listamae.get(j).getMaeDetalleId().equals(listaformacionAcademica.get(i).getNivelEducativoId()) ) {
					 formacion.setNiveleducativo(listamae.get(j).getDescripcion());
				 }
				 if(listamae.get(j).getMaeDetalleId().equals(listaformacionAcademica.get(i).getEstadoNivelEducativoId()) ) {
					 formacion.setEstado(listamae.get(j).getDescripcion());
				 }
				 if(listamae.get(j).getMaeDetalleId().equals(listaformacionAcademica.get(i).getEstadoSituacionAcademicaId()) ) {
					 formacion.setSituacion(listamae.get(j).getDescripcion());
				 }
				 
			}
			 listaformacion.add(formacion);
		 }	
		 return listaformacion;
	}
	
	private List<ReportePerfilActividades> getActividades(List<FuncionDetalle> listafuncion) {
		ReportePerfilActividades actividades;
		List<ReportePerfilActividades> listaactividades = new ArrayList<>();
		 if(!listafuncion.isEmpty()) {
			  for(int i=0;i<listafuncion.size();i++) {
					 actividades = new ReportePerfilActividades();
					 actividades.setDescripcion((i+1)+". "+listafuncion.get(i).getDescripcion());
					 listaactividades.add(actividades);
				 }			 
		  }
		 return listaactividades;

	}
	
	private Map<Integer,String> getCondicion(PerfilFuncion perfilfun) {
		Map<Integer, String> map = new HashMap<>();
		 if(perfilfun!=null) {
			 map.put(1, perfilfun.getCondicionAticipica());
			 map.put(2, perfilfun.getSustentoCondicionAtipica());
			 map.put(3, perfilfun.getCoordinacionInterna());
			 map.put(4, perfilfun.getCoordinacionExterna());
			 map.put(5, devolverGrupoServidores(perfilfun.getServidorCiviles()).toString());
		 }else {
			 map.put(1, "");
			 map.put(2, "");
			 map.put(3, "");
			 map.put(4, "");
			 map.put(5, "");
		 }
		 return map;
	}
*/
}