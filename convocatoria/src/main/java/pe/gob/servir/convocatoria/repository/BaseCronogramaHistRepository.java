package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BaseCronogramaHist;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

import java.util.List;

public interface BaseCronogramaHistRepository extends JpaRepository<BaseCronogramaHist , Integer> {
    List<BaseCronogramaHist> findByBase(Base base);
    List<BaseCronogramaHist> findByBaseAndEtapa(Base base , MaestraDetalle maestraDetalle );
    List<BaseCronogramaHist> findByBaseAndEstadoRegistro(Base base , String estado);
}
