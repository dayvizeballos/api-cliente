package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.gob.servir.convocatoria.model.ComplDetalle;

public interface BaseComplDetalleRepository extends JpaRepository<ComplDetalle, Long>{

}
