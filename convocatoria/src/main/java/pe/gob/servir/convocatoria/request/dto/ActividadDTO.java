package pe.gob.servir.convocatoria.request.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ActividadDTO {
    private Long actividadId;
   //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date fechaIni;
   //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date fechaFin;
    private String horaIni;
    private String horaFin;
    private String responsable;
    private String estadoRegistro;
    private String descripcion;
    private Long tipoActividad;

}
