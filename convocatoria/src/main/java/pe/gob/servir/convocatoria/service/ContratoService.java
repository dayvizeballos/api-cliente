package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqContrato;
import pe.gob.servir.convocatoria.request.ReqConvenio;
import pe.gob.servir.convocatoria.request.ReqDesestimaContrato;
import pe.gob.servir.convocatoria.request.ReqGeneraContrato;
import pe.gob.servir.convocatoria.request.ReqSuscribirContrato;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

import java.util.Date;

public interface ContratoService {

	RespBase<Object> registrarContrato(ReqBase<ReqGeneraContrato> request, MyJsonWebToken token);

	RespBase<RespToContratoConvenio> actualizaContrato(ReqBase<ReqContrato> request, MyJsonWebToken token,
			Long contratoId);

	RespBase<RespToContratoConvenio> getContratoConvenio(Long contratoId);

	RespBase<RespObtieneLista<RespContratoConvenio>> findContratoaConveniosFiltro(Long tipoPractica, Long tipo,
			Long entidad, Long regimen, Long perfil, Date fechaIni, Date fechaFin, Long estado, int page, int size);

	RespBase<Object> desestimarContrato(ReqBase<ReqDesestimaContrato> request, MyJsonWebToken token, Long contratoId);

	RespBase<Object> suscribirContrato(ReqBase<ReqSuscribirContrato> request, MyJsonWebToken token, Long contratoId);

	RespBase<RespToContratoConvenio> actualizaConvenio(ReqBase<ReqConvenio> request, MyJsonWebToken token,
			Long convenioId);

	RespBase<RespTipoContrato> listaTipoContrato(Long regimenId);

	byte[] descargaContrato(Long contratoId);

}
