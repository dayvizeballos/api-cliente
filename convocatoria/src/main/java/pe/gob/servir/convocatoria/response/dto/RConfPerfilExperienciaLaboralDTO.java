package pe.gob.servir.convocatoria.response.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import pe.gob.servir.convocatoria.model.ConfigPerfilExperienciaLaboral;
import pe.gob.servir.convocatoria.request.dto.ConfigPerfilExperienciaLaboralDetalleDTO;
@SuppressWarnings("serial")
@Data 
public class RConfPerfilExperienciaLaboralDTO  implements Serializable{

	private Long perfilId;
	private Long tipoExperiencia;
	private String descripcionNivel;
    private Long  nivelEducativoId;
    private Long  situacionAcademicaId;
    private String nivelEducativoDesc;
    private String sitAcademiDesc;
	private Long baseId;
    private List<DetalleExperiencia> listDetalleExperiencia;
    private List<ConfigPerfilExperienciaLaboralDetalleDTO> listModeloDetalleExperiencia;
	
    public RConfPerfilExperienciaLaboralDTO() {
    	
    }
    public RConfPerfilExperienciaLaboralDTO(ConfigPerfilExperienciaLaboral item) {
    	this.tipoExperiencia = item.getTipoExperiencia();
		this.sitAcademiDesc = item.getSitAcademiDesc();
		this.nivelEducativoDesc = item.getNivelEducativoDesc();
		this.nivelEducativoId = item.getNivelEducativoId();
		this.descripcionNivel = item.getNivelEducativoDesc() + " " + item.getSitAcademiDesc();
		this.situacionAcademicaId = item.getSituacionAcademicaId();
		this.baseId = item.getBaseId();
		this.perfilId = item.getPerfilId();
		this.listDetalleExperiencia = new ArrayList<>();
		this.listModeloDetalleExperiencia = new ArrayList<>();
	}
    public RConfPerfilExperienciaLaboralDTO(Long nivelEducativoId, Long situacionAcademicaId, Long tipoExperiencia, Long perfilId) {
    	this.perfilId = perfilId;
    	this.nivelEducativoId = nivelEducativoId;
    	this.situacionAcademicaId = situacionAcademicaId;
    	this.tipoExperiencia = tipoExperiencia;
    	this.listDetalleExperiencia = new ArrayList<>();
    	this.listModeloDetalleExperiencia = new ArrayList<>();
    }
    
	@Data
	public  static class  DetalleExperiencia  implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Long desdeAnios;
		private Long hastaAnios;
		private Boolean requisitoMinimo;
		private Long puntaje;
		private Long confPerfExpeLaboralId;
	}
}



