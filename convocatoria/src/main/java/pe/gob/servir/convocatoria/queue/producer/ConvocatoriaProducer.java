package pe.gob.servir.convocatoria.queue.producer;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.gob.servir.convocatoria.model.ConvocatoriaQueue;
import pe.gob.servir.convocatoria.model.Picture;
import pe.gob.servir.convocatoria.response.RespDifusionConvocatoria;
import pe.gob.servir.convocatoria.response.dto.ReporteConvocatoriaDTO;

@Component
@Slf4j
public class ConvocatoriaProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    /*
    public void sendMessageToQueue1(Picture p) throws JsonProcessingException {
        var json = objectMapper.writeValueAsString(p);
        rabbitTemplate.convertAndSend("x.convocatoria.tarea", "tarea.1", json);

    }
     */


    public void sendMessageToQueue2(ConvocatoriaQueue p) throws JsonProcessingException {
        var json = objectMapper.writeValueAsString(p);
        rabbitTemplate.convertAndSend("x.convocatoria.tarea", "tarea.2", json);

    }

    public void sendMessageToQueue3(ConvocatoriaQueue p) throws JsonProcessingException {
        var json = objectMapper.writeValueAsString(p);
        rabbitTemplate.convertAndSend("x.convocatoria.tarea", "tarea.3", json);

    }

    public void sendMessageToQueue4(RespDifusionConvocatoria p) throws JsonProcessingException {
        var json = objectMapper.writeValueAsString(p);
        rabbitTemplate.convertAndSend("x.convocatoria.tarea", "tarea.4", json);

    }


    public void sendMessageToQueueReporte(ReporteConvocatoriaDTO p) throws JsonProcessingException {
        var json = objectMapper.writeValueAsString(p);
        rabbitTemplate.convertAndSend("x.reporte", "reporte", json);

    }


}
