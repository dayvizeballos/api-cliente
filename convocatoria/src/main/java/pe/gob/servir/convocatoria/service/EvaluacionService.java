package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqActualizaEvaluacion;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaEvaluacion;
import pe.gob.servir.convocatoria.request.ReqParamEvaluacion;
import pe.gob.servir.convocatoria.request.dto.DetalleEvaluacionEntidadDTO;
import pe.gob.servir.convocatoria.request.dto.EvaluacionConvocatoriaDTO;
import pe.gob.servir.convocatoria.request.dto.RegimenDTO;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.response.dto.RespObtenerPuntajesEvaluacionDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface EvaluacionService {

	RespBase<RespCreaEvaluacion> creaEvaluacion(ReqBase<ReqCreaEvaluacion> request,MyJsonWebToken token);
	
	RespBase<RespActualizaEvaluacion> actualizarEvaluacion(ReqBase<ReqActualizaEvaluacion> request,
			MyJsonWebToken token, Long jerarquiaId);

	RespBase<RespActualizaEvaluacion> actualizarEvaluacioneEntidad(ReqBase<ReqActualizaEvaluacion> request,
														   MyJsonWebToken token, Long jerarquiaId);
	
	//RespBase<RespObtenerEvaluacion> obtenerEvaluacion(Long cabeceraId,Long modalidadId,Long tipoId, String estadoId);
	
	RespBase<Object> eliminarJerarquia(MyJsonWebToken token, Long codigoNivel1, Long codigoNivel2, Long codigoNivel3, String estado);

	RespBase<RespObtieneLista<RegimenDTO>> findAllEvaluacion(Long regimenLaboral , Long modalidad , Long tipoAcceso , Long entidadId);
	
	RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> findEvaluacionEntidadDto(ReqParamEvaluacion request);
	
	RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> findEvaluacionEntidadById(Long idBase , Long entidadId);

	RespBase<RespObtenerPuntajesEvaluacionDTO> obtenerPuntajeEvaluacion(Long perfilId, Long regimenId, Long entidadId);

	RespBase<RespObtieneLista<EvaluacionConvocatoriaDTO>> buscarEvaluacionesConvocatoriaEntidad(Long idBase,
			Long entidadId);

}
