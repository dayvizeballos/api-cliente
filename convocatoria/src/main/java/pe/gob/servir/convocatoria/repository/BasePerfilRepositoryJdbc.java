package pe.gob.servir.convocatoria.repository;

import java.util.List;

import pe.gob.servir.convocatoria.request.dto.BaseHorarioDTO;
import pe.gob.servir.convocatoria.request.dto.BasePerfilDTO;

public interface BasePerfilRepositoryJdbc {

	
	BasePerfilDTO getBasePerfil (Long basePerfilId);
	List<BaseHorarioDTO> getBaseHorarioList (Long basePerfilId);
}
