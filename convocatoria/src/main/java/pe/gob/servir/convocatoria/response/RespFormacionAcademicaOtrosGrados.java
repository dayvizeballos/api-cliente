package pe.gob.servir.convocatoria.response;

import lombok.Data;

@Data
public class RespFormacionAcademicaOtrosGrados {
    private Long confPerfFormacCarreraDetalleId;
    private Long confPerfFormacCarreraId;
    private Long grado;
    private Boolean requisitoMinimo;
    private Long  puntaje;
    private Long  tipoModelo;
    private Long tipoExperiencia;

	public RespFormacionAcademicaOtrosGrados() {
		
	}
}
