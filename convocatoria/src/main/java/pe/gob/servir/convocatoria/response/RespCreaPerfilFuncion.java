package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.PerfilFuncionDTO;

@Getter
@Setter
public class RespCreaPerfilFuncion {

	private PerfilFuncionDTO perfilFuncion;
	//private List<FuncionDetalle> lstFuncionDetalle;
	//private List<ServidorCivil> lstServidorCivil;
}
