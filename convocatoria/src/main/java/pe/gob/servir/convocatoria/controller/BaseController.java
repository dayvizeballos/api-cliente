package pe.gob.servir.convocatoria.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarBaseEvaluacion;
import pe.gob.servir.convocatoria.request.ReqGuardarInformacionComp;
import pe.gob.servir.convocatoria.request.dto.*;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.BasePerfilService;
import pe.gob.servir.convocatoria.service.BaseService;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;
import pe.gob.servir.convocatoria.service.PerfilService;
import pe.gob.servir.convocatoria.service.ReporteBase;
import pe.gob.servir.convocatoria.service.RequisitoGeneralService;

import com.lowagie.text.DocumentException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@RestController
@Tag(name = "Base", description = "")
public class BaseController {


	@Autowired
	private HttpServletRequest httpServletRequest;

	@Autowired
	private BaseService baseService;
	
	@Autowired
	private BasePerfilService basePerfilService;
	
	@Autowired
	private RequisitoGeneralService requisitoGeneralService;

	@Autowired
	private PerfilService perfilService;

	@Autowired
	private MaestraDetalleService maestraDetalleService;
	
	@Autowired
	private ReporteBase reporteBaseService;
	

	@Operation(summary = "Crea una base y concurso ", description = "Crea una base y concurso", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@PostMapping(path = {Constantes.BASE_ENDPOINT + "/base"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<DatosConcursoDTO>> crearBaseAndDatosConcurso(@PathVariable String access, @Valid
	@RequestBody ReqBase<DatosConcursoDTO> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<DatosConcursoDTO> response = baseService.crearBaseConcursoDto(request, jwt);
		return ResponseEntity.ok(response);

	}

	@Operation(summary = Constantes.SUM_OBT_LIST + "bases", description = Constantes.SUM_OBT_LIST + "bases", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/bases/filter"},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RespBase<RespObtieneLista<ObtenerBaseDTO>>> obtenerBases(
			@PathVariable String access,
			//@PathVariable Long idEntidad,
			@RequestParam(value = "entidadId", required = true) Long entidadId,
			@RequestParam(value = "codigoConvocatoria", required = false) String codigoConvocatoria,
			@RequestParam(value = "fechaIni", required = false) String fechaIni,
	        @RequestParam(value = "fechaFin", required = false) String fechaFin,
			@RequestParam(value = "etapaId", required = false) Long etapaId,
			@RequestParam(value = "regimenId", required = false) Long regimenId,
			@RequestParam(value = "modalidadId", required = false) Long modalidadId,
			@RequestParam(value = "tipoId", required = false) Long tipoId,
			@RequestParam(value = "practicaId", required = false) Long practicaId,
			@RequestParam(value = "condicionId", required = false) Long condicionId,
			@RequestParam(value = "nombrePerfil", required = false) String nombrePerfil,
			@RequestParam(value = "tipoRol", required = false) String tipoRol,
			@RequestParam(value = "personaId", required = false) Long personaId) {
		RespBase<RespObtieneLista<ObtenerBaseDTO>> response = null;
		Map<String, Object> parametroMap = new HashMap<>();
		parametroMap.put("entidadId", entidadId);
		parametroMap.put("codigoConvocatoria", codigoConvocatoria);
		parametroMap.put("fechaIni", fechaIni);
		parametroMap.put("fechaFin", fechaFin);
		parametroMap.put("etapaId", etapaId);
		parametroMap.put("regimenId", regimenId);
		parametroMap.put("modalidadId", modalidadId);
		parametroMap.put("tipoId", tipoId);
		parametroMap.put("practicaId", practicaId);
		parametroMap.put("condicionId", condicionId);
		parametroMap.put("nombrePerfil", nombrePerfil);
		parametroMap.put("tipoRol", tipoRol);
		parametroMap.put("personaId", personaId);

		response = baseService.buscarBaseByFilter(parametroMap);

		return ResponseEntity.ok(response);
	}


	@Operation(summary = "get datos concurso ", description = "get datos concurso ", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/base/concurso"},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<DatosConcursoDTO>> getConcursoDto(@PathVariable String access, @RequestParam(value = "baseId") Long baseId , @RequestParam(value = "perfilId",required = false) Long perfilId) {
		RespBase<DatosConcursoDTO> response = baseService.getConcursoDto(baseId , perfilId);
		return ResponseEntity.ok(response);


	}	
	
	@Operation(summary = "Cambiar estado base ", description = "Cambiar estado base", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@DeleteMapping(path = { Constantes.BASE_ENDPOINT + "/base/estado" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<BaseDTO>> cambiarEstadoBase(@PathVariable String access, 
																			   @RequestParam(value = "baseId", required = false) Long baseId,
																			   @RequestParam(value = "estado", required = false) String estado) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<BaseDTO> response = baseService.cambiarEstado(baseId , estado.trim() , jwt);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = "Crea Requisito General ", description = "Crea Requisito General", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@PostMapping(path = {Constantes.BASE_ENDPOINT + "/base/requisitoGeneral"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RequisitoGeneralDTO>> crearRequisitoGeneral(@PathVariable String access, @Valid
	@RequestBody ReqBase<RequisitoGeneralDTO> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RequisitoGeneralDTO> response = requisitoGeneralService.crearRequisitoGeneral(request , jwt);
		return ResponseEntity.ok(response);

	}

	@Operation(summary = "get requisito general ", description = "get requisito general ", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/base/requisitoGeneral"},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RequisitoGeneralDTO>> getRequisitoGeneral(@PathVariable String access, @RequestParam(value = "baseId") Long baseId) {
		RespBase<RequisitoGeneralDTO> response = requisitoGeneralService.getRequisitoGeneral(baseId);
		return ResponseEntity.ok(response);

	}

	@Operation(summary = "guardar evaluación ", description = "Guardar evaluación paso 4 ", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@PostMapping(path = {Constantes.BASE_ENDPOINT + "/base/guardarEvaluacion"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RespBase<BaseEvaluacionDTO>> guardarEvaluacion(@PathVariable String access, @Valid
	@RequestBody ReqBase<ReqGuardarBaseEvaluacion> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<BaseEvaluacionDTO> response = baseService.guardarEvaluacion(request, jwt);
		return ResponseEntity.ok(response);

	}
	

	@Operation(summary = "Crea una base perfil ", description = "Crea una base perfil", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@PostMapping(path = {Constantes.BASE_ENDPOINT + "/base/perfil"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RespBasePerfil>> crearBasePerfil(@PathVariable String access, @Valid
	@RequestBody ReqBase<BasePerfilDTO> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespBasePerfil> response = basePerfilService.creaBasePerfil(request, jwt);
		return ResponseEntity.ok(response);

	}
	
//	@Operation(summary = "actualizar evaluación ", description = "Actualizar evaluación paso 4 ", tags = {""},
//			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
//	@PutMapping(path = {Constantes.BASE_ENDPOINT + "/base/actualizarEvaluacion/{baseEvaluacionId}"},
//			consumes = {MediaType.APPLICATION_JSON_VALUE},
//			produces = {MediaType.APPLICATION_JSON_VALUE})
//	public ResponseEntity<RespBase<BaseEvaluacionDTO>> actualizarEvaluacion(@PathVariable String access,
//			@PathVariable Long baseEvaluacionId, @Valid @RequestBody ReqBase<ReqActualizarBaseEvaluacion> request
//	) {
//		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
//		RespBase<BaseEvaluacionDTO> response = baseService.actualizarEvaluacion(request, jwt, baseEvaluacionId);
//		return ResponseEntity.ok(response);
//
//	}
	
	@Operation(summary = "get base perfil ", description = "get base perfil ", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/base/perfil/{basePerfilId}"},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RespBasePerfil>> getBasePerfil(
				@PathVariable String access, 
				@PathVariable Long basePerfilId) {
		RespBase<RespBasePerfil> response = basePerfilService.obtenerBasePerfil(basePerfilId);
		return ResponseEntity.ok(response);

	}
	
	
	@Operation(summary = "Consultar las Vacantes", description = "Consultar las Vacantes", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/base/vacante/{baseId}" }, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespListaVacante>> listarVacantes(@PathVariable String access, @PathVariable Long baseId) {
		RespBase<RespListaVacante> response = new RespBase<>();
		response = perfilService.listarVacantes(baseId);
		
		return ResponseEntity.ok(response);		
	}	

	@Operation(summary = "Cambiar estado a una Vacante de un Perfil de una Base ", description = "Cambiar estado a una Vacante de un Perfil de una Base", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/base/vacante/estado" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<VacanteDTO>> cambiarEstadoVacante(@PathVariable String access, 
																			   @RequestParam(value = "basePerfilId", required = false) Long basePerfilId,
																			   @RequestParam(value = "estado", required = false) String estado) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<VacanteDTO> response = perfilService.cambiarEstadoVacante(basePerfilId , estado.trim() , jwt);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Actualizar base perfil ", description = "Actualizar base perfil", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})	
	@PutMapping(path = {Constantes.BASE_ENDPOINT + "/base/perfil/{basePerfilId}"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RespBase<RespBasePerfil>> actualizarBasePerfil(
			@PathVariable String access,
			@PathVariable Long basePerfilId, 
			@Valid @RequestBody ReqBase<BasePerfilDTO> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespBasePerfil> response = basePerfilService.actualizarBasePerfil(request, jwt, basePerfilId);
		return ResponseEntity.ok(response);

	}
	
	@Operation(summary = "Actualizar Dato Concurso", description = "Actualizar Dato Concurso", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/base/datoconcurso"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})

    public ResponseEntity<RespBase<DatosConcursoDTO>> actualizarDatoConcurso(@PathVariable String access, @Valid
    @RequestBody ReqBase<DatosConcursoDTO> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");

        RespBase<DatosConcursoDTO> response = baseService.updateConcursoDto(request, jwt);
        return ResponseEntity.ok(response);

    }

    @Operation(summary = "Actualizar Requisito General", description = "Actualizar Requisito General", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/base/requisitoGeneral"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})

    public ResponseEntity<RespBase<RequisitoGeneralDTO>> actualizarRequisitoGeneral(@PathVariable String access, @Valid
    @RequestBody ReqBase<RequisitoGeneralDTO> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");

        RespBase<RequisitoGeneralDTO> response = requisitoGeneralService.updateRequisitoGenral(request, jwt);
        return ResponseEntity.ok(response);

    }
	
	@Operation(summary = "Cambiar condicion base ", description = "Cambiar condicion base", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@PostMapping(path = {Constantes.BASE_ENDPOINT + "/base/condicion"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<CondicionDto>> cambioCondicion( @PathVariable String access, @Valid
	@RequestBody ReqBase<CondicionDto> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<CondicionDto> response = baseService.cambioCondicion(request , jwt);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = "Guarda/Actualiza Informacion complemetaria - paso6 ", description = "Crea un registro de Informacion complemetaria - Paso 6", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@PostMapping(path = {Constantes.BASE_ENDPOINT + "/base/guardarInfoCompl"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RespSaveUpdateInfCompl>> guardarActualizarInformacionCompl(@PathVariable String access, @Valid
	@RequestBody ReqBase<ReqGuardarInformacionComp> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespSaveUpdateInfCompl> response = baseService.guardarInformacionComp(request, jwt);
		return ResponseEntity.ok(response);
	}
    
	@Operation(summary = "Get condicion / observacion Base", description = "Get condicion / observacion Base", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/base/condicion/{baseId}" },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<CondicionDto>> getCondicion(@PathVariable String access, @PathVariable Long baseId , @RequestParam(value = "etapa") Long etapa)  {
		RespBase<CondicionDto> response = baseService.getCambioCondicion(baseId , etapa);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Get BaseEvaluacion", description = "Get BaseEvaluacion", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/base/evaluacion/{baseId}" },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<BaseEvaluacionDTO>> getBaseEvaluacion(@PathVariable String access, @PathVariable Long baseId)  {
		RespBase<BaseEvaluacionDTO> response = baseService.ObtenerBaseEvaluacion(baseId);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Consultar datos Etapa 6", description = "Consultar datos Etapa 6", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/base/etapa6/{baseId}" }, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespGuardarInfComplementaria>> obtenerDatosEtapa6(@PathVariable String access, @PathVariable Long baseId) {
		RespBase<RespGuardarInfComplementaria> response = baseService.obtenerDataEtapa6(baseId);
		
		return ResponseEntity.ok(response);		
	}


	@Operation(summary = "Registra los movimientos / observaciones ", description = "Registra los movimientos / observaciones", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@PostMapping(path = {Constantes.BASE_ENDPOINT + "/base/movimiento"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<MovimientoDTO>> registrarMovimiento( @PathVariable String access, @Valid
	@RequestBody ReqBase<MovimientoDTO> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<MovimientoDTO> response = baseService.registrarMovimiento(request , jwt);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = "get movimientos / observaciones por  id base", description = "get movimientos / observaciones por  id base", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/base/movimiento/{baseId}" },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<MovimientoDTO>>> getMovimientos(@PathVariable String access, @PathVariable Long baseId) {
		RespBase<RespObtieneLista<MovimientoDTO>> response = baseService.getMovimientos(baseId);
		return ResponseEntity.ok(response);
	}

	
	@Operation(summary = "Generar Pdf Base", description = "Generar Pdf Base", tags = {""},	
			   security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", 
			content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/base/pdf/{baseId}" })
	public @ResponseBody ResponseEntity<RespBase<String>> generarPdfBase
						   (@PathVariable String access,@PathVariable Long baseId) throws DocumentException, IOException {
		byte[] bytes = reporteBaseService.generatePdfFromHtmlBase(baseId); //generatePdfFromHtmlBase(baseId);
		String encodedString =
             Base64.getEncoder().withoutPadding().encodeToString(bytes);
     RespBase<String> payload = new RespBase<>();
     payload.setPayload(encodedString);
     
     return ResponseEntity.ok(payload);
	}

	@Operation(summary = Constantes.LISTA + " de los requisitos adicionales By baseId", description = Constantes.LISTA + " de los requisitos adicionales By baseId", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/requisitos/base/{baseId}" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista>> listarRequiAdicByBaseId(@PathVariable String access,
																			  @PathVariable Long baseId) {
		RespBase<RespObtieneLista> response = this.baseService.listarRequisGeneralByBaseId(baseId);
		return ResponseEntity.ok(response);
	}
	
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"Regimen/Modalidad/Tipo", description = Constantes.SUM_OBT_LIST+"Regimen/Modalidad/Tipo", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/base/maestra"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespListaMaestraEntidad>> listarMaestraEntidad(
			@PathVariable String access,
			@RequestParam(value = "entidad", required = true) Long entidad,
			@RequestParam(value = "regimen", required = false) Long regimen,
			@RequestParam(value = "modalidad", required = false) Long modalidad
			) {
		RespBase<RespListaMaestraEntidad> response = maestraDetalleService.obtenerMaestraPorEntidad(entidad, regimen, modalidad);
		return ResponseEntity.ok(response);		
	}
	
}
