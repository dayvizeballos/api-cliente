package pe.gob.servir.convocatoria.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.repository.EvaluacionRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.*;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EvaluacionRepositoryJdbcImpl implements EvaluacionRepositoryJdbc {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final String SQL_FIND_REGIMEN = "select mae_detalle_id as maeDetalleId, referencia , estado_registro as  estadoRegistro , descripcion_corta as descripcionCorta ,  case when estado_registro = '1' then 'activo' else 'inactivo' end as estado  \n" +
            "from sch_convocatoria.tbl_mae_detalle tmd  where  estado_registro = '1'  and tmd.mae_cabecera_id = (select mae_cabecera_id from sch_convocatoria.tbl_mae_cabecera tmc where tmc.codigo_cabecera = 'TBL_REGIMEN') " ;

    private static final String SQL_FIND_MODALIDAD = "select distinct tj.codigo_nivel1 as codNivel1, tmd.mae_cabecera_id as maeCabeceraId, tj.estado_registro as estadoRegistro, tj.codigo_nivel2 as codNivel2 , " +
            "tmd.mae_detalle_id as maeDetalleId, tmd.descripcion  as descripcion, tmd.descripcion_corta as descripcionCorta\n" +
            "from sch_convocatoria.tbl_jerarquia tj join sch_convocatoria.tbl_mae_detalle tmd on tj.codigo_nivel2 = tmd.mae_detalle_id \n" +
            "where tj.codigo_nivel1 = :modalidad  and tj.estado_registro = :estado " ;

    private static final String SQL_FIND_MODALIDAD_2 = "select distinct tj.codigo_nivel1 as codNivel1, tmd.mae_cabecera_id as maeCabeceraId, tj.estado_registro  as estadoRegistro ," +
            " tj.codigo_nivel2 as codNivel2 , tmd.mae_detalle_id as maeDetalleId, tmd.descripcion as descripcion, tmd.descripcion_corta as  descripcionCorta\n" +
            "from sch_convocatoria.tbl_jerarquia tj \n" +
            "join sch_convocatoria.tbl_mae_detalle tmd on tj.codigo_nivel2 = tmd.mae_detalle_id \n" +
            "join sch_convocatoria.tbl_evaluacion te on tj.jerarquia_id = te.jerarquia_id \n" +
            "where tmd.mae_detalle_id = :modalidad   and te.estado_registro = '1' and te.estado_registro = '1' " ;

    private static final String SQL_FIND_TIPO = "select distinct tj.jerarquia_id as jerarquia , tj.estado_registro as estadoRegistro  ,tmd.mae_detalle_id as maeDetalleId, tmd.mae_cabecera_id as maeCabeceraId, " +
            "tmd.descripcion as descripcion , tmd.descripcion_corta as descripcionCorta \n" +
            "from  sch_convocatoria.tbl_mae_detalle tmd   join sch_convocatoria.tbl_jerarquia tj on tj.codigo_nivel3 = tmd.mae_detalle_id \n" +
            "where tj.codigo_nivel1 = :tipoAcceso and tj.codigo_nivel2 = :codigo_nivel2 and tj.estado_registro = :estado " ;

    private static final String SQL_FIND_TIPO_2 = "select distinct tj.jerarquia_id as jerarquia , tj.codigo_nivel1 as codigoNivel1, tj.estado_registro as estadoRegistro ,tmd.mae_detalle_id as maeDetalleId, tmd.mae_cabecera_id as maeCabeceraId, " +
            "tmd.descripcion as descripcion, tmd.descripcion_corta as descripcionCorta , tj.codigo_nivel2 as codigoNivel2 \n" +
            "from sch_convocatoria.tbl_mae_detalle tmd \n" +
            "join sch_convocatoria.tbl_jerarquia tj on tj.codigo_nivel3 = tmd.mae_detalle_id \n" +
            "join sch_convocatoria.tbl_evaluacion te on tj.jerarquia_id = te.jerarquia_id \n" +
            "where tmd.mae_detalle_id = :tipo and tj.estado_registro = '1' and te.estado_registro = '1'" ;

    private static final String SQL_FIND_EVALUACION = "select te.estado_registro  , te.evaluacion_id as  evaluacionId , te.jerarquia_id  as jerarquiaId , te.estado_registro as estado" +
            " , te.tipo_evaluacion_id  as tipoEvaluacionId, te.orden as orden  , te.peso as peso, te.puntaje_maximo as puntajeMaximo , te.puntaje_minimo as puntajeMinimo , tmd.descripcion  as detalleEvaluacion\n" +
            "from sch_convocatoria.tbl_evaluacion te join sch_convocatoria.tbl_mae_detalle tmd on te.tipo_evaluacion_id = tmd.mae_detalle_id \n" +
            "where te.estado_registro = :estado and te.jerarquia_id  = :jerarquia order by te.evaluacion_id desc " ;



    private static final String SQL_FIND_EVALUACION_MASTER = "select distinct tj.codigo_nivel1 as codigoNivel1, tj.codigo_nivel2 as codigoNivel2  , tj.codigo_nivel3 as codigoNivel3 , " +
            "tj.jerarquia_id as jerarquia  , (select descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id =  tj.codigo_nivel1 ) as descripcionCorta " +
            "from sch_convocatoria.tbl_evaluacion_entidad tee \n" +
            "join sch_convocatoria.tbl_jerarquia tj on tee.jerarquia_id = tj.jerarquia_id where tee.entidad_id = :entidad and tj.estado_registro = '1' " ;



    /*regimen entidad*/
    private static final String SQL_FIND_EVALUACION_MASTER_2 = "select distinct tj.codigo_nivel1,\n" +
            "(select descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id =  tj.codigo_nivel1 ) \n" +
            "as descripcionCorta from sch_convocatoria.tbl_evaluacion_entidad tee \n" +
            "join sch_convocatoria.tbl_jerarquia tj on tee.jerarquia_id = tj.jerarquia_id\n" +
            "where tee.entidad_id = :entidad and tee.estado_registro = '1' and tj.estado_registro = '1' " ;

    /*modalidad entida*/
    private static final String SQL_FIND_EVALUACION_MASTER_3 = "select distinct tj.codigo_nivel2 as codNivel2 , tj.codigo_nivel1 as codNivel1,\n" +
            "(select descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id =  tj.codigo_nivel2 ) \n" +
            "as descripcionCorta from sch_convocatoria.tbl_evaluacion_entidad tee \n" +
            "join sch_convocatoria.tbl_jerarquia tj on tee.jerarquia_id = tj.jerarquia_id\n" +
            "where tee.entidad_id = :entidad and tee.estado_registro = '1' and tj.estado_registro = '1'  and tj.codigo_nivel1 = :codNivel1 " ;

    /*tipo acceso entidad*/
    private static final String SQL_FIND_EVALUACION_MASTER_4 = "select distinct tj.codigo_nivel3 as codigoNivel3 , tj.jerarquia_id as jerarquia,\n" +
            "(select descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id =  tj.codigo_nivel3 ) \n" +
            "as descripcionCorta from sch_convocatoria.tbl_evaluacion_entidad tee \n" +
            "join sch_convocatoria.tbl_jerarquia tj on tee.jerarquia_id = tj.jerarquia_id\n" +
            "where tee.entidad_id = :entidad and tee.estado_registro = '1' and tj.estado_registro = '1'  and  tj.codigo_nivel1 = :codNivel1 and tj.codigo_nivel2 = :codNivel2  " ;

    /*evaluacion entidad*/
    private static final String SQL_FIND_EVALUACION_ENTIDAD = "select te.evaluacion_entidad_id as evaluacionEntidad , te.tipo_evaluacion_id  as tipoEvaluacionId, te.entidad_id as entidadId, te.evaluacion_origen_id as evaluacionOrigenId, \n" +
            "case when te.evaluacion_origen_id is null then (select descripcion  from sch_convocatoria.tbl_mae_detalle_entidad tmd where tmd.mae_detalle_entidad_id = te.tipo_evaluacion_id )\n" +
            "else (select descripcion  from sch_convocatoria.tbl_mae_detalle  tmd where tmd.mae_detalle_id = te.tipo_evaluacion_id) end  as detalleEvaluacion ,\n" +
            "te.jerarquia_id as jerarquiaId , te.orden as orden , te.peso as peso , te.puntaje_maximo as puntajeMaximo, te.puntaje_minimo as puntajeMinimo, \n" +
            "te.estado_registro as estado " +
            "from sch_convocatoria.tbl_evaluacion_entidad te \n" +
            "where te.estado_registro = :estado and te.jerarquia_id  = :jerarquia and te.entidad_id = :entidad order by te.evaluacion_entidad_id desc";


    /*regimen servir*/
    private static final String SQL_FIND_EVALUACION_MASTER_2_SERVIR = " select distinct tj.codigo_nivel1 ,\n" +
            " (select descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id =  tj.codigo_nivel1 ) \n" +
            "  as descripcionCorta from sch_convocatoria.tbl_evaluacion tee\n" +
            "  join sch_convocatoria.tbl_jerarquia tj on tee.jerarquia_id = tj.jerarquia_id\n" +
            "  where tj.estado_registro = '1' and tee.estado_registro = '1' and tj.codigo_nivel1 \n" +
            "  in (select mae_detalle_id from sch_convocatoria.tbl_mae_detalle tmc where tmc.mae_cabecera_id = \n" +
            "  (select tmc2.mae_cabecera_id from sch_convocatoria.tbl_mae_cabecera tmc2 where tmc2.codigo_cabecera = 'TBL_REGIMEN'))  " ;

    /*modalidad servir*/
    private static final String SQL_FIND_EVALUACION_MASTER_3_SERVIR = " select distinct tj.codigo_nivel2 as codNivel2 , tj.codigo_nivel1 as codNivel1,\n" +
            "  (select descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id =  tj.codigo_nivel2 ) \n" +
            "   as descripcionCorta from sch_convocatoria.tbl_evaluacion tee \n" +
            "   join sch_convocatoria.tbl_jerarquia tj on tee.jerarquia_id = tj.jerarquia_id\n" +
            "   where tj.estado_registro = '1' and tee.estado_registro = '1' and tj.codigo_nivel1 = :codNivel1 " ;

    /*tipo acceso servir*/
    private static final String SQL_FIND_EVALUACION_MASTER_4_SERVIR = "select distinct tj.codigo_nivel3 as codigoNivel3 , tj.jerarquia_id as jerarquia,\n" +
            " (select descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id =  tj.codigo_nivel3 ) \n" +
            " as descripcionCorta from sch_convocatoria.tbl_evaluacion tee \n" +
            " join sch_convocatoria.tbl_jerarquia tj on tee.jerarquia_id = tj.jerarquia_id\n" +
            " where  tj.estado_registro = '1' and tee.estado_registro = '1' and  tj.codigo_nivel1 = :codNivel1 and tj.codigo_nivel2 = :codNivel2 " ;

    /*evaluacion servir*/
    private static final String SQL_FIND_EVALUACION_MASTER_EVALUACION_SERVIR = "  select te.estado_registro  , te.evaluacion_id as  evaluacionId , te.jerarquia_id  as jerarquiaId , te.estado_registro as estado ,\n" +
            " (select descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id = te.tipo_evaluacion_id) as detalleEvaluacion\n" +
            "  , te.tipo_evaluacion_id  as tipoEvaluacionId, te.orden as orden  , te.peso as peso, te.puntaje_maximo as puntajeMaximo , te.puntaje_minimo as puntajeMinimo\n" +
            "   from sch_convocatoria.tbl_evaluacion te\n" +
            "   where te.estado_registro = '1' and te.jerarquia_id  = :jerarquia order by te.evaluacion_id desc" ;



    private static final String SQL_FIND_EVALUACION_MAESTRA_DETALLE = " select tmd.descripcion as descripcionCorta from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id = :ids " ;




    @Autowired
    public EvaluacionRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;

    }

    @Override
    public List<RegimenDTO> listallRegimenLaboralDtos(Long regimenLaboral, String estado) {
        List<RegimenDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_REGIMEN);
            if (!Util.isEmpty(regimenLaboral)) sql.append(" and tmd.mae_detalle_id = :regimenLaboral ");

            if (!Util.isEmpty(regimenLaboral)) objectParam.put("regimenLaboral", regimenLaboral);
            if (!Util.isEmpty(estado)) objectParam.put("estado", estado);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(RegimenDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<ModalidadDTO> listallModalidadDtos(Long modalidad, String estado , Long codigoNivel2) {
        List<ModalidadDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_MODALIDAD);

            if (!Util.isEmpty(codigoNivel2)) sql.append(" and tj.codigo_nivel2 = :codigoNivel2 ");

            if (!Util.isEmpty(modalidad)) objectParam.put("modalidad", modalidad);
            if (!Util.isEmpty(codigoNivel2)) objectParam.put("codigoNivel2", codigoNivel2);
            if (!Util.isEmpty(estado)) objectParam.put("estado", estado);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ModalidadDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<ModalidadDTO> listallModalidadDtos2(Long modalidad, String estado) {
        List<ModalidadDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_MODALIDAD_2);
            if (!Util.isEmpty(modalidad)) objectParam.put("modalidad", modalidad);
            if (!Util.isEmpty(estado)) objectParam.put("estado", estado);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ModalidadDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<TipoDTO> listallTipoDtos(Long tipoAcceso , Long codigoNivel2 ,  String estado , Long codigoNivel3) {
        List<TipoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_TIPO);
            if (!Util.isEmpty(codigoNivel3)) sql.append(" and TJ.codigo_nivel3 = :codigoNivel3 ");

            if (!Util.isEmpty(tipoAcceso)) objectParam.put("tipoAcceso", tipoAcceso);
            if (!Util.isEmpty(codigoNivel2)) objectParam.put("codigo_nivel2", codigoNivel2);
            if (!Util.isEmpty(estado)) objectParam.put("estado", estado);
            if (!Util.isEmpty(codigoNivel3)) objectParam.put("codigoNivel3", codigoNivel3);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(TipoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<TipoDTO> listallTipoDtos2(Long tipo , String estado) {
        List<TipoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_TIPO_2);
            if (!Util.isEmpty(tipo)) objectParam.put("tipo", tipo);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(TipoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<EvaluacionDTO> listallTiEvaluacionDtos(Long jerarquia, String estado ) {
        List<EvaluacionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION);
            if (!Util.isEmpty(jerarquia)) objectParam.put("jerarquia", jerarquia);
            if (!Util.isEmpty(estado)) objectParam.put("estado", estado);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(EvaluacionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<EvaluacionDTO> listallTiEvaluacionDtosServir(Long jerarquia, String estado) {
        List<EvaluacionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_MASTER_EVALUACION_SERVIR);
            if (!Util.isEmpty(jerarquia)) objectParam.put("jerarquia", jerarquia);
            if (!Util.isEmpty(estado)) objectParam.put("estado", estado);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(EvaluacionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    /**
     * se escoge EvaluacionDTO para evaluacion entidad too
     * para no modifcar y reusar la lista debe traer el id
     * de evaluacion entidad para poder hacer alguna transaccion
     * sobre evaluacion entidad
     * @param jerarquia
     * @param estado
     * @return
     */
    @Override
    public List<EvaluacionDTO> listallTiEvaluacionEntidadDtos(Long jerarquia, String estado ,  Long entidad) {
        List<EvaluacionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_ENTIDAD);
            if (!Util.isEmpty(jerarquia)) objectParam.put("jerarquia", jerarquia);
            if (!Util.isEmpty(entidad)) objectParam.put("entidad", entidad);
            if (!Util.isEmpty(estado)) objectParam.put("estado", estado);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(EvaluacionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<MasterEvaluacionDTO> listMasterEvaluacionDtos(Long entidad) {
        List<MasterEvaluacionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_MASTER);
            if (!Util.isEmpty(entidad)) objectParam.put("entidad", entidad);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(MasterEvaluacionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<MasterEvaluacionDTO> listMasterEvaluacionDtos2(Long entidad , Long codNivel1 , Long codNivel2 , Long codNivel3) {
        List<MasterEvaluacionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_MASTER_2);
            if (!Util.isEmpty(codNivel1)) sql.append(" and tj.codigo_nivel1 = :codigoNivel1 ");
            if (!Util.isEmpty(codNivel2)) sql.append(" and tj.codigo_nivel2 = :codigoNivel2 ");
            if (!Util.isEmpty(codNivel3)) sql.append(" and tj.codigo_nivel3 = :codigoNivel3 ");


            if (!Util.isEmpty(entidad)) objectParam.put("entidad", entidad);
            if (!Util.isEmpty(codNivel1)) objectParam.put("codigoNivel1", codNivel1);
            if (!Util.isEmpty(codNivel2)) objectParam.put("codigoNivel2", codNivel2);
            if (!Util.isEmpty(codNivel3)) objectParam.put("codigoNivel3", codNivel3);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(MasterEvaluacionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<MasterEvaluacionDTO> listMasterEvaluacionDtos2Servir(Long entidad, Long codNivel1, Long codNivel2, Long codNivel3) {
        List<MasterEvaluacionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_MASTER_2_SERVIR);
            if (!Util.isEmpty(codNivel1)) sql.append(" and tj.codigo_nivel1 = :codigoNivel1 ");
            if (!Util.isEmpty(codNivel2)) sql.append(" and tj.codigo_nivel2 = :codigoNivel2 ");
            if (!Util.isEmpty(codNivel3)) sql.append(" and tj.codigo_nivel3 = :codigoNivel3 ");


            if (!Util.isEmpty(codNivel1)) objectParam.put("codigoNivel1", codNivel1);
            if (!Util.isEmpty(codNivel2)) objectParam.put("codigoNivel2", codNivel2);
            if (!Util.isEmpty(codNivel3)) objectParam.put("codigoNivel3", codNivel3);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(MasterEvaluacionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<ModalidadDTO> listMasterEvaluacionDtos3(Long entidad, Long codNivel1 ,  Long codNivel2 , Long codNivel3) {
        List<ModalidadDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_MASTER_3);

            if (!Util.isEmpty(codNivel2)) sql.append(" and tj.codigo_nivel2 = :codigoNivel2 ");
            if (!Util.isEmpty(codNivel3)) sql.append(" and tj.codigo_nivel3 = :codigoNivel3 ");

            if (!Util.isEmpty(entidad)) objectParam.put("entidad", entidad);
            if (!Util.isEmpty(codNivel1)) objectParam.put("codNivel1", codNivel1);
            if (!Util.isEmpty(codNivel2)) objectParam.put("codigoNivel2", codNivel2);
            if (!Util.isEmpty(codNivel3)) objectParam.put("codigoNivel3", codNivel3);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ModalidadDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<ModalidadDTO> listMasterEvaluacionDtos3Servir(Long entidad, Long codNivel1, Long codNivel2, Long codNivel3) {
        List<ModalidadDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_MASTER_3_SERVIR);

            if (!Util.isEmpty(codNivel2)) sql.append(" and tj.codigo_nivel2 = :codigoNivel2 ");
            if (!Util.isEmpty(codNivel3)) sql.append(" and tj.codigo_nivel3 = :codigoNivel3 ");


            if (!Util.isEmpty(codNivel1)) objectParam.put("codNivel1", codNivel1);
            if (!Util.isEmpty(codNivel2)) objectParam.put("codigoNivel2", codNivel2);
            if (!Util.isEmpty(codNivel3)) objectParam.put("codigoNivel3", codNivel3);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ModalidadDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<TipoDTO> listMasterEvaluacionDtos4(Long entidad, Long codNivel1, Long codNivel2 , Long codNivel3) {
        List<TipoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_MASTER_4);

            if (!Util.isEmpty(codNivel3)) sql.append(" and tj.codigo_nivel3 = :codigoNivel3 ");

            if (!Util.isEmpty(entidad)) objectParam.put("entidad", entidad);
            if (!Util.isEmpty(codNivel1)) objectParam.put("codNivel1", codNivel1);
            if (!Util.isEmpty(codNivel2)) objectParam.put("codNivel2", codNivel2);
            if (!Util.isEmpty(codNivel3)) objectParam.put("codigoNivel3", codNivel3);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(TipoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<TipoDTO> listMasterEvaluacionDtos4Servir(Long entidad, Long codNivel1, Long codNivel2, Long codNivel3) {
        List<TipoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_EVALUACION_MASTER_4_SERVIR);

            if (!Util.isEmpty(codNivel3)) sql.append(" and tj.codigo_nivel3 = :codigoNivel3 ");


            if (!Util.isEmpty(codNivel1)) objectParam.put("codNivel1", codNivel1);
            if (!Util.isEmpty(codNivel2)) objectParam.put("codNivel2", codNivel2);
            if (!Util.isEmpty(codNivel3)) objectParam.put("codigoNivel3", codNivel3);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(TipoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

}
