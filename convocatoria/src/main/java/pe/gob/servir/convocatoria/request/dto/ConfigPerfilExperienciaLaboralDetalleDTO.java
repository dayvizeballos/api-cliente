package pe.gob.servir.convocatoria.request.dto;

import lombok.Data;

@Data
public class ConfigPerfilExperienciaLaboralDetalleDTO  {
	    private Long confPerfExpeLaboralDetalleId;
	    private Long confPerfExpeLaboralId;
	    private Long desdeAnios;
	    private Long hastaAnios;
	    private Boolean  requisitoMinimo;
	    private Long  puntaje;
	    private Long  tipoModelo;
	    private Long tipoExperiencia;
	}