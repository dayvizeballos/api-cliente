package pe.gob.servir.convocatoria.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.commons.lang3.StringUtils;

/**
 * ColumnaTablaHtml que permite designar un atributo como equivalente de una columna html
 * tiene 5 campos: orden, nombre, concatValue, ignoreWhen y validIgnore.<br>
 * orden identifica el orden de la columna.<br>
 * nombre identifica el header de la columna.<br>
 * concatValue representa el valor que se le concatenará a todos los valores de la columna.<br>
 * ignoreWhen identifica el valor que se validará para ocultar una columna.<br>
 * validIgnore valor que confirma que se aplicará el filtro de ignore a la columna anotada.
 * 
 * @author projas
 *
 */

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ColumnaTablaHtml{
	public int orden();
	public String nombre() default StringUtils.EMPTY;
	public String concatValue() default StringUtils.EMPTY;
	public String ignoreWhen() default StringUtils.EMPTY;
	public boolean validIgnore() default false;
}
