package pe.gob.servir.convocatoria.request.dto;


import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class BonificacionDetalleDTO {
	
    private Long bonificacionDetalleId;
	
    private Long bonificacionId;
	
    private String descripcion;
	
	@NotNull(message = Constantes.CAMPO + " nivelId " + Constantes.ES_OBLIGATORIO)
    private Long nivelId;
	
	@NotNull(message = Constantes.CAMPO + " aplicaId " + Constantes.ES_OBLIGATORIO)
    private Long aplicaId;
	
	@NotNull(message = Constantes.CAMPO + " porcentajeBono " + Constantes.ES_OBLIGATORIO)
    private Long porcentajeBono;
	
    private String nombreNivel;
    private String nombreAplica;
	private String estadoRegistro;
	
}
