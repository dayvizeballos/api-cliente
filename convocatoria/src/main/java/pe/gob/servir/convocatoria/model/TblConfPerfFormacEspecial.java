package pe.gob.servir.convocatoria.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tbl_conf_perf_formac_especial", schema = "sch_convocatoria")
@NamedQuery(name="TblConfPerfFormacEspecial.findAll", query="SELECT t FROM TblConfPerfFormacEspecial t")
public class TblConfPerfFormacEspecial extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conf_perf_formac_especial_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_formac_especial")
    @SequenceGenerator(name = "seq_conf_perf_formac_especial", sequenceName = "seq_conf_perf_formac_especial", schema = "sch_convocatoria", allocationSize = 1)
    private Long confPerfFormacEspecialId;

    @ManyToOne
    @JoinColumn(name = "perfil_id", nullable = false)
    private Perfil perfil;

    @Column(name = "tipo_conocimiento_id", nullable = false)
    private Long tipoConocimientoId;

    @Column(name = "descripcion_tipo")
    private String descripcionTipo;

    @Column(name = "conocimiento_id")
    private Long conocimientoId;

    @Column(name = "descripcion_conocimiento")
    private String descripcionConocimiento;

    @Column(name = "requisito_minimo")
    private Boolean requisitoMinimo;

    @Column(name = "puntaje")
    private Integer puntaje;
    
    @Column(name = "base_id")
    private Long baseId;   

}
