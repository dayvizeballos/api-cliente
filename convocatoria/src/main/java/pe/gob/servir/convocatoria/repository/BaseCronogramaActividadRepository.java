package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.gob.servir.convocatoria.model.BaseCronogramaActividad;

public interface BaseCronogramaActividadRepository extends JpaRepository<BaseCronogramaActividad, Long> {

}
