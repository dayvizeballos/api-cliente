package pe.gob.servir.convocatoria.audit;

/**
 * Interface que debe ser implementada por las clases Entities para habilitar la
 * auditoria
 * 
 * @author ttorres
 *
 */
public interface AuditableEntity {

}
