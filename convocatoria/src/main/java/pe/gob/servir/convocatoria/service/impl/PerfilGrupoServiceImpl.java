package pe.gob.servir.convocatoria.service.impl;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.servir.convocatoria.model.PerfilGrupo;
import pe.gob.servir.convocatoria.repository.PerfilGrupoRepository;
import pe.gob.servir.convocatoria.repository.PerfilGrupoRepositoryJdbc;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.service.PerfilGrupoService;

import java.util.List;

@Service
public class PerfilGrupoServiceImpl implements PerfilGrupoService {
    public static final Logger logger = Logger.getLogger(PerfilGrupoServiceImpl.class);

    @Autowired
    PerfilGrupoRepositoryJdbc perfilGrupoRepositoryJdbc;

    @Autowired
    PerfilGrupoRepository perfilGrupoRepository;
    
    @Override
    public RespBase<RespObtieneLista<PerfilGrupo>> findAllPerfilGrupo(Long id) {

        RespObtieneLista<PerfilGrupo> respPayload = new RespObtieneLista<>();
        List<PerfilGrupo> listarPerfiles = perfilGrupoRepositoryJdbc.findAllPerfilGrupo(id);
        respPayload.setCount(listarPerfiles.size());
        respPayload.setItems(listarPerfiles);
        return new RespBase<RespObtieneLista<PerfilGrupo>>().ok(respPayload);
    }

	@Override
	public RespBase<RespObtieneLista<PerfilGrupo>> listAllPerfilGrupo() {
		 RespObtieneLista<PerfilGrupo> respPayload = new RespObtieneLista<>();
	     List<PerfilGrupo> listarPerfiles = perfilGrupoRepository.listarPerfilesActivos();
	     respPayload.setCount(listarPerfiles.size());
	     respPayload.setItems(listarPerfiles);
	     return new RespBase<RespObtieneLista<PerfilGrupo>>().ok(respPayload);
	}
}
