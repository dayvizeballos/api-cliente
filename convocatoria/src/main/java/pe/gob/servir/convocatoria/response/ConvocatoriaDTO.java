package pe.gob.servir.convocatoria.response;


import java.util.Date;

import lombok.Data;


@Data
public class ConvocatoriaDTO {
    private Long convocatoriaId;

    private String codigoConvocatoria;

    private String codConvAndCodRegimen;

    private Long baseId;

    private String etapa;

    private String codProgEtapa;

    private String estadoConvocatoria;

    private String url;

    private Long correlativo;

    private Long anio;

    private Long vacantes;

    private String postulantesAndCalifican;

    private String postulantes;

    private String califican;

    private String gestor;

    private String coordinador;
    
    private Long etapaId;
    
    private Long estadoConvocatoriaId;
    
    private String codProgEstado;

    private String fechaPublicacion;

    private String codProRegimen;

    private Long idRegimen;
    
    private Long etapaNewId;
    private Date fechaFinEleccion;


}
