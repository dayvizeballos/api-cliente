package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.DeclaracionJurada;

import java.util.List;
import java.util.Optional;

@Component
public interface DeclaracionJuradaRepository extends JpaRepository<DeclaracionJurada, Long> {

    @Query("SELECT p FROM  DeclaracionJurada p WHERE p.base.baseId = :idBase")
    List<DeclaracionJurada> listDeclaracionJuradaxBase(Long idBase);

    @Query("SELECT p FROM DeclaracionJurada p WHERE p.tipoId = :tipo and p.isServir = :isServir and p.base.baseId = :idBase")
    Optional<DeclaracionJurada> declaracionJuradaByTipoAndServirAndBase(@Param("tipo") Long tipo, @Param("isServir") String isServir, @Param("idBase") Long idBase);
}
