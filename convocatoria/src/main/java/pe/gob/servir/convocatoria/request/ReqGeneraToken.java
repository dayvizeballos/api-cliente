package pe.gob.servir.convocatoria.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReqGeneraToken {
    private String usuario;
    private String password;
    private String refreshToken;
    private String grantType;


}
