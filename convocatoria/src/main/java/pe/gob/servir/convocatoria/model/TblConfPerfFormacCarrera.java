package pe.gob.servir.convocatoria.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "tbl_conf_perf_formac_carrera", schema = "sch_convocatoria")
@NamedQuery(name="TblConfPerfFormacCarrera.findAll", query="SELECT t FROM TblConfPerfFormacCarrera t")
public class TblConfPerfFormacCarrera extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conf_perf_formac_carrera_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_formac_especif")
    @SequenceGenerator(name = "seq_conf_perf_formac_especif", sequenceName = "seq_conf_perf_formac_especif", schema = "sch_convocatoria", allocationSize = 1)
    private Long confPerfFormacCarreraId;

    @ManyToOne
    @JoinColumn(name = "perfil_id", nullable = false)
    private Perfil perfil;

    @Column(name = "nivel_educativo_id", nullable = false)
    private Long nivelEducativoId;

    @Column(name = "descripcion_nivel_educativo")
    private String descripcionNivelEducativo;
    
    @Column(name = "situacion_academica_id")
    private Long situacionAcademicaId;
    
    @Column(name = "descripcion_academica")
    private String descripcionAcademica;

    @Column(name = "descripcion_carrera")
    private String descripcionCarrera;
    
    @Column(name = "carrera_ids")
    private String carreraIds;
    
    @Column(name = "requisito_minimo")
    private Boolean requisitoMinimo;

    @Column(name = "puntaje")
    private Integer puntaje;
    
    @Column(name = "base_id")
    private Long baseId;   

}
