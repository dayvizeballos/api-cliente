package pe.gob.servir.convocatoria.common;

import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public enum GrupoServidor {
	
	
	//FUNCIONARIO_PUBLICO("1","FUNCIONARIO_PUBLICO"), 
	DIRECTIVO_PUBLICO("DP","DIRECTIVO_PUBLICO"),
	SERVIDORES_CARRERA("CA","SERVIDORES_CARRERA"),
	ACTIVIDADES_COMPLEMENTARIAS("CO","ACTIVIDADES_COMPLEMENTARIAS");
	
	private String codigo;
	private String descripcion;
	
	 public static Stream<GrupoServidor> stream() {
	        return Stream.of(GrupoServidor.values()); 
	    }

}
