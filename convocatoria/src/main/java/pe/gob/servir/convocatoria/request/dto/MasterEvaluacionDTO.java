package pe.gob.servir.convocatoria.request.dto;


import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class MasterEvaluacionDTO {
    private Long codigoNivel1;
    private Long codigoNivel2;
    private Long codigoNivel3;
    private Long jerarquia;
    private String descripcionCorta;

}

