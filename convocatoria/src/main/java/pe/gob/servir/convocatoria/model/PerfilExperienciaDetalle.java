package pe.gob.servir.convocatoria.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;

@Entity
@Table(name = "tbl_perfil_experiencia_detalle", schema = "sch_convocatoria")
@Getter
@Setter
public class PerfilExperienciaDetalle  extends  AuditEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil_experiencia_detalle")
    @SequenceGenerator(name = "seq_perfil_experiencia_detalle", sequenceName = "seq_perfil_experiencia_detalle", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "experiencia_Detalle_id")
    private Long id;

    @Column(name = "tipo_dato_experiencia_id")
    private Long  tiDaExId;

    @Column(name = "descripcion ")
    private String descripcion;

    @Column(name = "orden")
    private Long  orden;

    @Column(name = "requisito_id")
    private Long  requisitoId;


    @JoinColumn(name="experiencia_perfil_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private PerfilExperiencia perfilExperiencia;

}
