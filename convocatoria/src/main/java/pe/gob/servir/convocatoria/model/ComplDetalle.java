package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.audit.AuditableEntity;


/**
 * The persistent class for the tbl_base_compl_detalle database table.
 * 
 */
@Entity
@Table(name="tbl_base_compl_detalle",schema = "sch_base")
@NamedQuery(name="ComplDetalle.findAll", query="SELECT t FROM ComplDetalle t")
@Getter
@Setter
@ToString
public class ComplDetalle extends AuditEntity implements AuditableEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tbl_base_compl_detalle")
	@SequenceGenerator(name = "seq_tbl_base_compl_detalle", sequenceName = "seq_tbl_base_compl_detalle", schema = "sch_base", allocationSize = 1)
	@Column(name="base_compl_detalle_id")
	private Long baseComplDetalleId;

	@Column(name="informe_detalle_id")
	private Long informeDetalleId;

	//bi-directional many-to-one association to TblBaseInfComplement
	@ManyToOne
	@JoinColumn(name="base_comple_id")
	private InformeComplement informeComplement;

	public ComplDetalle() {
		super();
	}

}