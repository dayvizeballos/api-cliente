package pe.gob.servir.convocatoria.repository.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import pe.gob.servir.convocatoria.repository.EvaluacionEntidadRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.DetalleEvaluacionEntidadDTO;
import pe.gob.servir.convocatoria.util.Util;

@Repository
public class EvaluacionEntidadRepositoryJdbcImpl implements EvaluacionEntidadRepositoryJdbc {

	private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public EvaluacionEntidadRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
	        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	        this.jdbcTemplate = jdbcTemplate;
	}
	
	
	private static final String SQL_FIND_DATO_EVALUACION_ENTIDAD = "SELECT  ee.peso as peso ,ee.puntaje_minimo as puntajeMin,ee.puntaje_maximo as puntajeMax,ee.evaluacion_entidad_id as evaluacionEntidadId, md.descripcion \n"
			+ " FROM sch_convocatoria.TBL_EVALUACION_ENTIDAD ee \n"
			+ " LEFT JOIN sch_convocatoria.tbl_mae_detalle md ON ee.tipo_evaluacion_id = md.mae_detalle_id \n"
			+ " WHERE ee.estado_registro = '1' ";
	@Override
	public List<DetalleEvaluacionEntidadDTO> buscarEEntidad(Long idEntidad, Long idJerarquia) {
		List<DetalleEvaluacionEntidadDTO> base = null;
		try {
			StringBuilder sql = new StringBuilder();
			Map<String, Object> objectParam = new HashMap<>();
			sql.append(SQL_FIND_DATO_EVALUACION_ENTIDAD);
			if (!Util.isEmpty(idEntidad))sql.append(" AND ee.entidad_id = :idEntidad ");
			if (!Util.isEmpty(idJerarquia))sql.append(" AND ee.jerarquia_id = :idJerarquia ");

			if (!Util.isEmpty(idEntidad))objectParam.put("idEntidad", idEntidad);
			if (!Util.isEmpty(idJerarquia))objectParam.put("idJerarquia", idJerarquia);
			
			base = new ArrayList<>();
			base = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam,
					BeanPropertyRowMapper.newInstance(DetalleEvaluacionEntidadDTO.class));
			
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
		}
		return base;
	}

}
