package pe.gob.servir.convocatoria.service;

import java.util.Map;

import pe.gob.servir.convocatoria.request.ReqActualizarBaseEvaluacion;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarBaseEvaluacion;
import pe.gob.servir.convocatoria.request.ReqGuardarInformacionComp;
import pe.gob.servir.convocatoria.request.dto.*;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespGuardarInfComplementaria;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespSaveUpdateInfCompl;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface BaseService {
    RespBase<DatosConcursoDTO> crearBaseConcursoDto(ReqBase<DatosConcursoDTO> request, MyJsonWebToken token);
    RespBase<DatosConcursoDTO> getConcursoDto(Long baseId , Long perfilId);
    RespBase<RespObtieneLista<ObtenerBaseDTO>> buscarBaseByFilter(Map<String, Object> parametroMap);
	RespBase<BaseDTO> cambiarEstado(Long baseId , String estado , MyJsonWebToken token);
	RespBase<BaseEvaluacionDTO> guardarEvaluacion(ReqBase<ReqGuardarBaseEvaluacion> request, MyJsonWebToken token);
	//RespBase<BaseEvaluacionDTO> actualizarEvaluacion(ReqBase<ReqActualizarBaseEvaluacion> request, MyJsonWebToken token, Long baseEvaluacionId );
    RespBase<DatosConcursoDTO> updateConcursoDto(ReqBase<DatosConcursoDTO> request, MyJsonWebToken token);
    RespBase<CondicionDto> cambioCondicion(ReqBase<CondicionDto> request, MyJsonWebToken token);
    RespBase<RespSaveUpdateInfCompl> guardarInformacionComp(ReqBase<ReqGuardarInformacionComp> request, MyJsonWebToken token);
    RespBase<CondicionDto> getCambioCondicion(Long baseId , Long etapa);
    
    RespBase<BaseEvaluacionDTO> ObtenerBaseEvaluacion(Long baseId);
    RespBase<RespGuardarInfComplementaria> obtenerDataEtapa6(Long baseId);
    RespBase<MovimientoDTO> registrarMovimiento(ReqBase<MovimientoDTO> request, MyJsonWebToken token);
    RespBase<RespObtieneLista<MovimientoDTO>> getMovimientos(Long baseId);

    RespBase<RespObtieneLista> listarRequisGeneralByBaseId (Long baseId);

}
