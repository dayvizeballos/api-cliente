package pe.gob.servir.convocatoria.model;



import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "tbl_conf_perf_formac_especif", schema = "sch_convocatoria")
@NamedQuery(name = "TblConfPerfFormacEspecif.findAll", query = "SELECT t FROM TblConfPerfFormacEspecif t")
public class TblConfPerfFormacEspecif extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conf_perf_formac_especif_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_formac_especif")
    @SequenceGenerator(name = "seq_conf_perf_formac_especif", sequenceName = "seq_conf_perf_formac_especif", schema = "sch_convocatoria", allocationSize = 1)
    private Long confPerfFormacEspecifId;

    @ManyToOne
    @JoinColumn(name = "perfil_id", nullable = false)
    private Perfil perfil;

    @Column(name = "tipo_especificacion", nullable = false)
    private Long tipoEspecificacion;

    @Column(name = "cantidad")
    private Long cantidad;

    @Column(name = "puntaje")
    private Long puntaje;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "requisito_minimo")
    private Boolean requisitoMinimo;
    
    @Column(name = "base_id")
    private Long baseId;    

    public TblConfPerfFormacEspecif() {
        super();
    }
}
