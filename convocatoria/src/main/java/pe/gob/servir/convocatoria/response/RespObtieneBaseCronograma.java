package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.Cronograma;

@Getter
@Setter
public class RespObtieneBaseCronograma {
	private boolean flag;
	private String informacion;
	private List<Cronograma> basecronograma;
	
}
