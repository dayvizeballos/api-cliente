package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_tipo_contrato", schema = "sch_convocatoria")
@Getter
@Setter
public class TipoContrato extends AuditEntity implements AuditableEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tipo_contrato")
	@SequenceGenerator(name = "seq_tipo_contrato", sequenceName = "seq_tipo_contrato", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "tipo_contrato_id")
	private Long tipoContratoId;

	@Column(name = "descripcion_plantilla")
	private String descPlantilla;
	
	@Column(name = "codigo_plantilla")
	private String codigoPlantilla;

	@Column(name = "ruta_plantilla")
	private String ruta;
	
	@JoinColumn(name = "regimen_id")
	@ManyToOne(optional = false)
	private MaestraDetalle regimenId;

}
