package pe.gob.servir.convocatoria.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import java.util.List;

@Entity
@Table(name = "tbl_base_evaluacion", schema = "sch_base")
@Getter
@Setter
public class BaseEvaluacion extends AuditEntity implements AuditableEntity {


	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_evaluacion")
    @SequenceGenerator(name = "seq_base_evaluacion", sequenceName = "seq_base_evaluacion", schema = "sch_base", allocationSize = 1)

    @Column(name = "base_evaluacion_id")
    private Long baseEvaluacionId;

    @JoinColumn(name="base_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Base base;
    
    @Column(name = "observacion")
    private String observacion;

    @OneToMany(cascade= CascadeType.ALL , fetch = FetchType.LAZY, mappedBy = "baseEvaluacion")
    @Column(nullable = true)
    @JsonManagedReference
    private List<BaseEvaluacionDetalle> baseEvaluacionDetalleList;
}
