package pe.gob.servir.convocatoria.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbl_mae_conocimiento", schema = "sch_convocatoria")
@Getter
@Setter
public class MaeConocimiento extends AuditEntity implements AuditableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mae_conocimiento")
    @SequenceGenerator(name = "seq_mae_conocimiento", sequenceName = "seq_mae_conocimiento", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "mae_conocimiento_id")
    private Long maeConocimentoId;

    @Column(name = "tipo_conocimiento_id")
    private Long tipoConocimientoId;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "entidad_id")
    private Long entidadId;

    /*
    @OneToOne(mappedBy = "maeConocimiento")
    private Conocimiento conocimiento;
    */

    /*
    @OneToMany(cascade= CascadeType.ALL , mappedBy = "maeConocimiento")
    @Column(nullable = true)
    @JsonManagedReference
    private List<Conocimiento> perfilExperienciaDetalleList2;

     */

    public MaeConocimiento() {
    }
}
