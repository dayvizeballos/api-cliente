package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;

@Data
public class RespPeriodoCronogramaDTO {
	
	private String tipoEvaluacion; //solo para cuando la etapa es evaluacion
	private String desEvaluacion;
	private String desEtapa;
	private String fechaInicio;
	private String fechaFin;

}
