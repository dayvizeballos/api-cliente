package pe.gob.servir.convocatoria.response;

import lombok.Data;

@Data
public class RespCondicionAtipica {
    private String condicion;
    private String periodo;
}
