package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.gob.servir.convocatoria.model.ConfigPerfilExperienciaLaboralDetalle;
public interface ConfigPerfilExperienciaDetalleRepository extends JpaRepository<ConfigPerfilExperienciaLaboralDetalle, Long>{

}
