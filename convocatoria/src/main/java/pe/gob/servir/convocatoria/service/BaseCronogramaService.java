package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaBaseCronograma;
import pe.gob.servir.convocatoria.request.ReqEliminarActividad;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCreaBaseCronograma;
import pe.gob.servir.convocatoria.response.RespEtapaActualVacanteBase;
import pe.gob.servir.convocatoria.response.dto.RespPeriodoCronogramaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface BaseCronogramaService {
	
	RespBase<RespCreaBaseCronograma> guardarBaseCronograma(ReqBase<ReqCreaBaseCronograma> request, MyJsonWebToken token, Long basecronogramaId);

	RespBase<RespCreaBaseCronograma> updateBaseCronograma(ReqBase<ReqCreaBaseCronograma> request, MyJsonWebToken token, Long basecronogramaId);

	RespBase<ReqEliminarActividad> eliminarBaseCronograma(ReqBase<ReqEliminarActividad> request, MyJsonWebToken token);
	
	RespBase<RespCreaBaseCronograma> findAllCronogramas(Long baseId , int history);
	
	RespBase<RespPeriodoCronogramaDTO> obtenerCronogramaEtapaBase(Long etapaId , Long baseId);
	
	RespBase<RespEtapaActualVacanteBase> obtenerEtapaActualBase(Long baseId);
}
