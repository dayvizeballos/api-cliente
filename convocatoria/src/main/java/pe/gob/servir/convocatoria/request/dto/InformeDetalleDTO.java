package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class InformeDetalleDTO {
    private Long informeDetalleId;
    private Long tipoInfo;
    private String nombretipoInfo;
    private String titulo;
    private String contenido;
    private Long entidadId;
    private String estado;
    private String codProg;

}
