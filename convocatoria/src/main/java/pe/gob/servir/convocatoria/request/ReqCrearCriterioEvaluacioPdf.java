package pe.gob.servir.convocatoria.request;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class ReqCrearCriterioEvaluacioPdf {
	
	private Long idInformeDetalle;	
	private Long baseId;

}
