package pe.gob.servir.convocatoria.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_convocatoria", schema = "sch_convocatoria")
@Getter
@Setter
public class Convocatoria extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_convocatoria")
    @SequenceGenerator(name = "seq_convocatoria", sequenceName = "seq_convocatoria", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "convocatoria_id")
    private Long convocatoriaId;

    @Column(name = "codigo_convocatoria")
    private String codigoConvocatoria;

    @JoinColumn(name = "base_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Base base;
    
    @JoinColumn(name = "etapa_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private MaestraDetalle etapa;

    @JoinColumn(name = "estado_convocatoria_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private MaestraDetalle estadoConvocatoria;

    @Column(name = "url_convocatoria")
    private String url;

    @Column(name = "correlativo_convocatoria")
    private Long correlativo;

    @Column(name = "anio")
    private Long anio;
    
    @Column(name = "entidad_id")
    private Long entidadId;
    
    @JoinColumn(name = "regimen_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private MaestraDetalle regimen;

    /*SE PUEDE AGRUPAR UN TIPO EN MAESTRA DETALLE  CABECERA: TIPO_JOB :DETALLE: 1:ind_archivo, 2:ind_notificacion*/
    @Column(name = "ind_archivo")
    private String indiArchivo;

    @Column(name = "ind_notificacion")
    private String indiNotificacion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_publicacion")
    private Date fechaPublicacion;

    @Column(name = "fecha_publi_string")
    private Date fechaString;

    /*@Column(name = "regimen")
    private String regimen;
*/
    @Column(name = "vacantes")
    private Long vacantes;

    @Column(name = "postulantes")
    private String postulantes;

    @Column(name = "califican")
    private String califican;
    
    @Column(name = "sigla_entidad")
    private String siglaEntidad;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_etapa")
    private Date fechaCambioEtapa;

}
