package pe.gob.servir.convocatoria.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCargaMasiva;
import pe.gob.servir.convocatoria.response.RespCreaBaseCronograma;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.CargaMasivaPerfilService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;

import javax.servlet.http.HttpServletRequest;

@RestController
@Tag(name = "Carga", description = "")
public class CargaMasivaController {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private CargaMasivaPerfilService cargaMasivaPerfilService;

    @Operation(summary = "Carga masiva perfiles", description = "Crea perfiles desde un excel masivo", tags = { "" }, security = {
            @SecurityRequirement(name = Constantes.BEARER_JWT) })
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
            }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
            }) })
    @PostMapping(path = { Constantes.BASE_ENDPOINT + "/cargamasiva/perfil/upload" }, consumes = {
            MediaType.MULTIPART_FORM_DATA_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })

    public ResponseEntity<RespBase<RespCargaMasiva>> loaderMasivaPerfil(@PathVariable String access,
                                                                             @RequestParam("files") MultipartFile[] files ,
                                                                             @RequestParam("model") String model
                                                                                    ) throws JsonProcessingException {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<RespCargaMasiva> response = cargaMasivaPerfilService.uploadExcel(model, files, jwt);

        return ResponseEntity.ok(response);
    }
}
