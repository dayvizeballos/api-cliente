package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

@Getter
@Setter
public class RespObtenerEvaluacion {
	private List<MaestraDetalle> listaRegimenLaboral;

}
