package pe.gob.servir.convocatoria.response;

import java.util.Date;

import javax.persistence.Column;

import lombok.Data;

@Data
public class RespComunicado {

    private Long comunicadoId;

    private Long convocatoriaId;

    private Long perfilId;

    private String nombrePerfil;

    private Long etapaId;

    private String nombreEtapa;

    private Long estadoId;

    private String nombreEstado;

    private Long tipoComunicadoId;

    private String tipoComunicadoNombre;

    private String nombreGestor;

    private String nombreCoordinador;

    private String fechaPublicacion;

    private String estadoRegistro;
    
    private Long gestorId;
    private Long coordinadorId;
    private String url;
    private String nombreArchivo;
    private String observacion;
   
}
