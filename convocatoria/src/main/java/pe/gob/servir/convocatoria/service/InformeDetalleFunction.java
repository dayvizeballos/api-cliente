package pe.gob.servir.convocatoria.service;

import java.util.function.Function;

import pe.gob.servir.convocatoria.model.Bonificacion;
import pe.gob.servir.convocatoria.model.BonificacionDetalle;
import pe.gob.servir.convocatoria.model.InformeDetalle;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.BonificacionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.InformeDetalleDTO;

@FunctionalInterface
public interface InformeDetalleFunction {
	
	public String main ();
	
	Function<InformeDetalle, InformeDetalleDTO> convertirDTOInformeDetalle = (InformeDetalle  p) -> {
		InformeDetalleDTO informeDetalleDTO = new InformeDetalleDTO();
		informeDetalleDTO.setInformeDetalleId(p.getInformeDetalleId());
		informeDetalleDTO.setEntidadId(p.getEntidadId());
		informeDetalleDTO.setTipoInfo(p.getTipoInfo());
		informeDetalleDTO.setTitulo(p.getTitulo());
		informeDetalleDTO.setContenido(p.getContenido());
		informeDetalleDTO.setEstado(p.getEstadoRegistro());
		
		return informeDetalleDTO;
	};
	
	Function<InformeDetalleDTO,InformeDetalle > convertirInformeDetalle = (InformeDetalleDTO  p) -> {
		InformeDetalle informeDetalle = new InformeDetalle();
		informeDetalle.setInformeDetalleId(p.getInformeDetalleId());
		informeDetalle.setEntidadId(p.getEntidadId());
		informeDetalle.setTipoInfo(p.getTipoInfo());
		informeDetalle.setTitulo(p.getTitulo());
		informeDetalle.setContenido(p.getContenido());
		
		return informeDetalle;
	};
	
	Function<BonificacionDTO, Bonificacion> convertirBonificacion = (BonificacionDTO p) -> {
		Bonificacion bonificacion = new Bonificacion();
		bonificacion.setBonificacionId(p.getBonificacionId() == null ? null : p.getBonificacionId());
		bonificacion.setTipoBonificacion(p.getTipoBonificacion());
		bonificacion.setTitulo(p.getTitulo());
		bonificacion.setContenido(p.getContenido());
		/*bonificacion.setBonificacionNombre(p.getBonificacionNombre());
		bonificacion.setNivel(p.getNivel());
		bonificacion.setAplica(p.getAplica());
		bonificacion.setNivelId(p.getNivelId());
		bonificacion.setAplicalId(p.getAplicaId());
		bonificacion.setPorcentaje(p.getPorcentaje());
		*/
		return bonificacion;
	};
	
	Function<Bonificacion, BonificacionDTO> convertirDTOBonificacion = (Bonificacion p) -> {
		BonificacionDTO bonificacionDTO = new BonificacionDTO();
		bonificacionDTO.setBonificacionId(p.getBonificacionId());
		bonificacionDTO.setTipoBonificacion(p.getTipoBonificacion());
		bonificacionDTO.setTitulo(p.getTitulo());
		bonificacionDTO.setContenido(p.getContenido());
		/*bonificacionDTO.setBonificacionNombre(p.getBonificacionNombre());
		bonificacionDTO.setNivel(p.getNivel());
		bonificacionDTO.setAplica(p.getAplica());
		bonificacionDTO.setNivelId(p.getNivelId());
		bonificacionDTO.setAplicaId(p.getAplicalId());
		bonificacionDTO.setPorcentaje(p.getPorcentaje());
		*/
		return bonificacionDTO;
	};
	
	Function<BonificacionDetalleDTO, BonificacionDetalle> convertirBonificacionDetalle = (BonificacionDetalleDTO p) -> {
		BonificacionDetalle bonificacionDetalle = new BonificacionDetalle();
		bonificacionDetalle.setBonificacionDetalleId(p.getBonificacionDetalleId() == null ? null : p.getBonificacionDetalleId());
		bonificacionDetalle.setDescripcion(p.getDescripcion() == null ? null : p.getDescripcion().toString());
		bonificacionDetalle.setNivelId(p.getNivelId() == null ? null : p.getNivelId());
		bonificacionDetalle.setAplicaId(p.getAplicaId() == null ? null : p.getAplicaId());
		bonificacionDetalle.setPorcentajeBono(p.getPorcentajeBono() == null ? null : p.getPorcentajeBono());
		return bonificacionDetalle;
	};
	
	Function<BonificacionDetalle, BonificacionDetalleDTO> convertirDTOBonificacionDetalle = (BonificacionDetalle p) -> {
		BonificacionDetalleDTO bonificacionDetalleDTO = new BonificacionDetalleDTO();
		bonificacionDetalleDTO.setBonificacionDetalleId(p.getBonificacionDetalleId() == null ? null :p.getBonificacionDetalleId());
		bonificacionDetalleDTO.setDescripcion(p.getDescripcion() == null ? null : p.getDescripcion().trim());
		bonificacionDetalleDTO.setNivelId(p.getNivelId() == null ? null : p.getNivelId());
		bonificacionDetalleDTO.setAplicaId(p.getAplicaId() == null ? null : p.getAplicaId());
		bonificacionDetalleDTO.setPorcentajeBono(p.getPorcentajeBono() == null ? null : p.getPorcentajeBono());
		return bonificacionDetalleDTO;
	};
	
}
	

