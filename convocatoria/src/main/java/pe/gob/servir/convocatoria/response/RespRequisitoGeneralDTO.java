package pe.gob.servir.convocatoria.response;

import lombok.Data;
import pe.gob.servir.convocatoria.model.RequisitoGeneral;

import java.io.Serializable;

@Data
public class RespRequisitoGeneralDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long requisitoGeneralId;
    private Long baseId;
    private String descripcion;
    private String cuestionarioDDJJ;

    public RespRequisitoGeneralDTO(RequisitoGeneral req) {
        this.requisitoGeneralId = req.getRequisitoId();
        this.baseId = req.getBase().getBaseId();
        this.descripcion = req.getDescripcion();
        this.cuestionarioDDJJ = req.getCuestionario();
    }
}
