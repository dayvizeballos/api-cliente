package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VacanteDTO {

	private Long basePerfilId;
	private Long baseId;
	private Long perfilId;
	private String perfilNombre;
	private String codigoPuesto;
	private Integer nroVacante;
	private Long sedeId;
	private String sedeDireccion;
	private String jornadaLaboral;
	private Double remuneracion;
	private String remuneracionConMoneda;
	private String estadoRegistro;
	
}
