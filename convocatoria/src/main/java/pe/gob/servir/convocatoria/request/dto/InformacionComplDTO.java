package pe.gob.servir.convocatoria.request.dto;

import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class InformacionComplDTO {
	
	@NotNull(message = Constantes.CAMPO + " tipoInformeId " + Constantes.ES_OBLIGATORIO)
	private Long tipoInformeId;
	@NotNull(message = Constantes.CAMPO + " idInforme " + Constantes.ES_OBLIGATORIO)
	private Long idInforme;
	private String nombreInforme;

}
