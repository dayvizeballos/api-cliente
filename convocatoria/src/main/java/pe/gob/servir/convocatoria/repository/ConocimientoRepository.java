package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.Conocimiento;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConocimientoRepository extends JpaRepository<Conocimiento, Long>{

    @Query("SELECT FO FROM Conocimiento FO  WHERE FO.perfil.perfilId = :perfilId")
     List<Conocimiento> findListByPerfil(@Param("perfilId")Long perfilId);

    @Query("SELECT FO.maeConocimiento.maeConocimentoId FROM Conocimiento FO  WHERE FO.perfilConocimientoId = :perfilConocimientoId")
    Optional<Long> maeConoId (@Param("perfilConocimientoId")Long perfilConocimientoId);

}
