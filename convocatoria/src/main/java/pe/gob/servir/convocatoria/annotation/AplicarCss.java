package pe.gob.servir.convocatoria.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * AplicarCss que permite indicar a cual columna se le aplicará un css style
 *
 * @author projas
 *
 */

@Target({ElementType.FIELD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AplicarCss {

}
