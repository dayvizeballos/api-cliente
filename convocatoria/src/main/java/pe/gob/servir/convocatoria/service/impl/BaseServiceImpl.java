package pe.gob.servir.convocatoria.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.common.VariablesSistema;
import pe.gob.servir.convocatoria.exception.ConflictException;
import pe.gob.servir.convocatoria.exception.NotFoundException;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.model.*;
import pe.gob.servir.convocatoria.queue.producer.ReporteConvocatoriaProducer;
import pe.gob.servir.convocatoria.repository.*;
import pe.gob.servir.convocatoria.repository.impl.MovimientoRepositoryJdbcImpl;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarBaseEvaluacion;
import pe.gob.servir.convocatoria.request.ReqGuardarInformacionComp;
import pe.gob.servir.convocatoria.request.dto.*;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.response.dto.ReporteConvocatoriaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.BaseService;
import pe.gob.servir.convocatoria.service.*;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;
import pe.gob.servir.convocatoria.service.PerfilService;
import pe.gob.servir.convocatoria.service.RequisitoGeneralService;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BaseServiceImpl implements BaseService {

    //private static final Logger logger = LoggerFactory.getLogger(BaseServiceImpl.class);

    @Autowired
    BaseRepository baseRepository;

    @Autowired
    BaseRepositoryJdbc baseRepositoryJdbc;

    @Autowired
    BaseEvaluacionRepository baseEvaluacionRepository;

    @Autowired
    BasePerfilRepository basePerfilRepository;

    @Autowired
    DatosConcursoRepository datosConcursoRepository;

    @Autowired
    MaestraDetalleRepository maestraDetalleRepository;

    @Autowired
    BaseInformeComplRepository baseInformeComplRepository;

    @Autowired
    BaseComplDetalleRepository baseComplDetalleRepository;

    @Autowired
    InformeDetalleRepository informeDetalleRepository;
    
    @Autowired
    GuardarInformeCompl guardarInformeCompl ; 
    
    @Autowired
    BonificacionRepository bonificacionRepository;

    @Autowired
    MovimientoRepository movimientoRepository;

    @Autowired
    MovimientoRepositoryJdbcImpl movimientoRepositoryJdbc;

    @Autowired
    EvaluacionEntidadRepository evaluacionEntidadRepository;

    @Autowired
    BaseEvaluacionDetalleRepository baseEvaluacionDetalleRepository;

    @Autowired
    private JerarquiaRepository jerarquiaRepository;

    @Autowired
    private EvaluacionEntidadRepositoryJdbc evaluacionEntidadRepositoryJdbc;

    @Autowired
    PerfilRepository perfilRepository;

    @Autowired
    private RequisitoGeneralRepository requisitoGeneralRepository;
    
    @Autowired
    private ConvocatoriaService convocatoriaService;

    @Autowired
    private EntidadClient entidadRepository;
    
    @Autowired
	private PerfilService perfilService;
    
    @Autowired
	private RequisitoGeneralService requisitoGeneralService;
    
    @Autowired
    VariablesSistema variablesSistema;

    @Autowired
    private ReporteConvocatoriaProducer reporteConvocatoriaProducer;
    
    @Override
    public RespBase<DatosConcursoDTO> crearBaseConcursoDto(ReqBase<DatosConcursoDTO> request, MyJsonWebToken token) {
        RespBase<DatosConcursoDTO> response = new RespBase<>();
        Base base = BaseConverIF.converDtoConcursoBase.apply(request.getPayload());
        List<MaestraDetalleDto> ls = null;
        
        if (!Util.isEmpty(request.getPayload().getBaseId())) {
            
            List<DatosConcursoDTO> lista = baseRepositoryJdbc.listDatosConcursoDtos(request.getPayload().getBaseId(), null);            
            if (!lista.isEmpty()) {
	            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
	                    "La base ya se encuentra registrada");
	            return response;
            }
        }

        validarPerfilAndJerarquiaAndEvaluacion(base);

        ls = baseRepositoryJdbc.maestraDetalle(Constantes.COD_PRO_MA_DET_EN_PROCESO, Constantes.COD_TABLA_SITUACION_ETAPA);
        if (!ls.isEmpty()) {
            base.setEtapaId(ls.get(0).getMaestraDetalleId());
        }
        base.setUserId(token.getUsuario().getUsuarioId());
        base.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        base.setDatosConcursoList(new ArrayList<>());
        RespBase<RespEntidad> responseEntidad = entidadRepository.obtieneEntidad(request.getPayload().getBaseEntidadId());
		if (responseEntidad.getStatus().getSuccess()) {
			if (responseEntidad.getPayload() != null) {
				base.setSiglas(responseEntidad.getPayload().getEntidad().get(0).getSigla() == null ? null
						: responseEntidad.getPayload().getEntidad().get(0).getSigla());
				base.setDistritoEntidad(responseEntidad.getPayload().getEntidad().get(0).getDistrito() == null ? null 
						: responseEntidad.getPayload().getEntidad().get(0).getDistrito());
				base.setUrlLogoEntidad(responseEntidad.getPayload().getEntidad().get(0).getLogo() == null ? null 
						: responseEntidad.getPayload().getEntidad().get(0).getLogo());
				base.setNombreEntidad(responseEntidad.getPayload().getEntidad().get(0).getDescripcionEntidad() == null ? null 
						: responseEntidad.getPayload().getEntidad().get(0).getDescripcionEntidad());
				base.setUrlWebEntidad(responseEntidad.getPayload().getEntidad().get(0).getUrlWeb() == null ? null
						: responseEntidad.getPayload().getEntidad().get(0).getUrlWeb());
			}
		}
        DatosConcurso datosConcurso = BaseConverIF.converDtoConcurso.apply(request.getPayload());
        
        if(!Util.isEmpty(request.getPayload().getUnidadOrganicaId())) {
        	 RespBase<RespUnidadOrganica> responseUo = entidadRepository.obtieneUnidadOrganica(request.getPayload().getBaseEntidadId());
             if (responseUo.getStatus().getSuccess()) {
             	if (responseUo.getPayload() != null) {
             		datosConcurso.setNombreUnidadOrganica(responseUo.getPayload().getListaUnidadOrganica() == null ? null 
             				: devolverDescripcionUnidadOrgnica(responseUo, request.getPayload().getUnidadOrganicaId()));
             	}
             }
        }
       
        if(!Util.isEmpty(request.getPayload().getOrganoResponsableId())) {
	        RespBase<RespOrganigrama> responseO = entidadRepository.obtieneOrgano(request.getPayload().getBaseEntidadId(),variablesSistema.ENTIDAD_ORG_TIPO);
	        if (responseO.getStatus().getSuccess()) {
	        	if (responseO.getPayload() != null) {
	        		datosConcurso.setNombreOrgano(responseO.getPayload().getListaOrganigrama() == null ? null 
	        				: devolverDescripcionOrgano(responseO, request.getPayload().getOrganoResponsableId()));
	        	}
	        }
        }
        datosConcurso.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        datosConcurso.setBase(base);

        base.getDatosConcursoList().add(datosConcurso);
        baseRepository.save(base);

        /**
         * QUEUE REPORTE ETAPA 1
         */
        if (base.getBaseId() != null){
            sendQueueEtapa1(base);
        }

        Long idDatoConcurso = base.getDatosConcursoList().stream()
                .mapToLong(DatosConcurso::getDatoConcursoId)
                .findFirst()
                .orElseThrow(NoSuchElementException::new);

        List<DatosConcursoDTO> lsout = baseRepositoryJdbc.listDatosConcursoDtos(base.getBaseId() , idDatoConcurso);
        DatosConcursoDTO payload = new DatosConcursoDTO();
        if (!lsout.isEmpty())  payload = lsout.get(0);
        return new RespBase<DatosConcursoDTO>().ok(payload);


    }

    /**
     *QUEUE ETAPA 1
     *
     * @param base
     * @return
     */
    private void  sendQueueEtapa1(Base base) {
        try {
            ReporteConvocatoriaDTO dto = new ReporteConvocatoriaDTO();
            dto.setEtapaQueue(Constantes.ETAPA_SAVE_BASE);
            dto.setUsuarioCreacion(base.getUsuarioCreacion());
            dto.setBaseId(base.getBaseId());
            dto.setGestorId(base.getGestorId());
            dto.setNombreGestor(base.getNombreGestor());
            dto.setEntidadId(base.getEntidadId());


            dto.setEstadoBaseId(base.getTipoId());

            Optional<MaestraDetalle> etapa = maestraDetalleRepository.findById(base.getEtapaId()); // estado en sch_reporte
            etapa.ifPresent(val -> {
                dto.setEstadoBase(val.getDescripcion());
            });

            dto.setRegimenId(base.getRegimenId());

            Optional<MaestraDetalle> regimen = maestraDetalleRepository.findById(base.getRegimenId());
            regimen.ifPresent(val -> {
                dto.setRegimen(val.getDescripcion());
                dto.setDenominacionPuesto(settDenominacionPuesto(regimen.get()));
            });


            dto.setModalidadId(base.getModalidadId());

            Optional<MaestraDetalle> modalidad = maestraDetalleRepository.findById(base.getModalidadId());
            modalidad.ifPresent(val -> {
                dto.setModalidad(val.getDescripcion());
            });

            dto.setTipoId(base.getTipoId());
            Optional<MaestraDetalle> tipo = maestraDetalleRepository.findById(base.getTipoId());
            tipo.ifPresent(val -> {
                dto.setTipo(val.getDescripcion());
            });

            if (!base.getDatosConcursoList().isEmpty()) {
                dto.setNroVacantes(base.getDatosConcursoList().get(0).getNroVacantes());
                if (base.getDatosConcursoList().get(0).getTipoPracticaId()!= null){
                    Optional<MaestraDetalle> tipoPractica = maestraDetalleRepository.findById(base.getDatosConcursoList().get(0).getTipoPracticaId());
                     tipoPractica.ifPresent(val ->{
                         dto.setTipoPractica(setTipoPractica(val));
                     });
                }
            }

             dto.setFechaCreacionBase(base.getFechaCreacion().toString());
            reporteConvocatoriaProducer.sendMessageBase(dto);
        } catch (Exception e) {

        }


    }

    /**
     * VERIFICAR SI ES EMPLEO / PRACTICA
     *
     * @return
     */
    private String settDenominacionPuesto(MaestraDetalle regimen) {
        if (regimen.getCodProg().equalsIgnoreCase(Constantes.REGIMEN_LABORAL_1401)) {
            return "P";
        } else {
            return "E";
        }
    }

    /**
     * VERIFICAR SI ES PRE / PRO / practica
     *
     * @return
     */
    private String setTipoPractica(MaestraDetalle regimen) {
        if (regimen.getCodProg().equalsIgnoreCase(Constantes.ESTUDIANTE)) {
            return "PRE";
        } else {
            return "PRO";
        }
    }


    public String devolverDescripcionUnidadOrgnica(RespBase<RespUnidadOrganica> response, Long id) {
        String output = "";
        if (id != null) {
			for (UnidadOrganica u : response.getPayload().getListaUnidadOrganica()) {
	            if (id.equals(u.getOrganigramaId())) {
	                output = u.getUnidadOrganica();
	                break;
	            }
	        }
		}
       
        return output;
    }
    
    public String devolverDescripcionOrgano(RespBase<RespOrganigrama> response, Long id) {
        String output = "Ninguno";
        if(response.getStatus().getSuccess().equals(true)) {
            for (Organigrama u : response.getPayload().getListaOrganigrama()) {
                if (id.equals(u.getOrganigramaId())) {
                    output = u.getDescripcion();
                    break;
                }
            }
        }

        return output;
    }
    
    /**
     * VALIDAR QUE LA BASE TENGA
     * PERFIL / JERARQUIA / EVALUACIONES
     * @param base
     */
    private void validarPerfilAndJerarquiaAndEvaluacion(Base base) {
        List<Jerarquia> jerarquia = jerarquiaRepository.getJerarquia(base.getRegimenId(), base.getModalidadId(), base.getTipoId());
        if (jerarquia == null || jerarquia.isEmpty())
            throw new NotFoundException("no existe jerarquia con la combinacion regimen: " + base.getRegimenId() + " modalidad: " + base.getModalidadId() + " tipo: " + base.getTipoId());

        List<DetalleEvaluacionEntidadDTO> lsPrev = evaluacionEntidadRepositoryJdbc.buscarEEntidad(base.getEntidadId(), jerarquia.get(0).getJerarquiaId());
        if (lsPrev.isEmpty())
            throw new NotFoundException("no existe evaluaciones asignadas a la entidad : " + base.getEntidadId());

        List<Perfil> perfil = perfilRepository.findByEntidadIdAndRegimenLaboralId(base.getEntidadId(),  base.getRegimenId());
        if (perfil.isEmpty())
            throw new NotFoundException("la entidad: " + base.getEntidadId() + " no tiene perfiles asignados");
    }

    @Override
    public RespBase<RespObtieneLista<ObtenerBaseDTO>> buscarBaseByFilter(Map<String, Object> parametroMap) {
        List<ObtenerBaseDTO> listaBaseDTO = baseRepositoryJdbc.buscarBaseByFilter(parametroMap);
        RespObtieneLista<ObtenerBaseDTO> respPayload = new RespObtieneLista<>();
        respPayload.setCount(listaBaseDTO.size());
        respPayload.setItems(listaBaseDTO);
        return new RespBase<RespObtieneLista<ObtenerBaseDTO>>().ok(respPayload);
    }

    @Override
    public RespBase<DatosConcursoDTO> getConcursoDto(Long baseId , Long perfilId) {
        RespBase<DatosConcursoDTO> response = new RespBase<>();
        if (Util.isEmpty(baseId)) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "el id de base es obligatorio");
            return response;
        }

        Optional<Base> baseOptional = baseRepository.findByidBase(baseId);
        if (!baseOptional.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "dato concurso no existe con el id: " + baseId);
            return response;
        }
        List<DatosConcursoDTO> lsout = baseRepositoryJdbc.listDatosConcursoDtos(baseId , null);

        DatosConcursoDTO payload = new DatosConcursoDTO();
        if (!lsout.isEmpty())  payload = lsout.get(0);
        if(!Util.isEmpty(perfilId)) {
            List<BasePerfil> basePerfils = basePerfilRepository.findbyBaseIdAndPerfil(baseId, perfilId);
            if (!basePerfils.isEmpty()) {
                payload.setNroVacantesPerfil(basePerfils.get(0).getVacante());
            }
        }

        return new RespBase<DatosConcursoDTO>().ok(payload);
    }

    @Override
    public RespBase<BaseDTO> cambiarEstado(Long baseId, String estado, MyJsonWebToken token) {
        BaseDTO payload = new BaseDTO();
        RespBase<BaseDTO> response = new RespBase<>();

        if (Util.isEmpty(baseId)) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "perfilId es obligatorio");
            return response;
        }

        if (Util.isEmpty(estado)) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "estado es obligatorio");
            return response;
        }

        Optional<Base> baseOptional = baseRepository.findById(baseId);
        if (baseOptional.isPresent()) {
            Base base = baseOptional.get();
            base.setEstadoRegistro(estado);
            base.setCampoSegUpd(estado, token.getUsuario().getUsuario(), Instant.now());
            baseRepository.save(base);

            payload.setBaseId(baseOptional.get().getBaseId());
            payload.setEstadoRegistro(estado);
        } else {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "no se encontro la base con el id: " + baseId);
            return response;
        }

        return new RespBase<BaseDTO>().ok(payload);

    }

    /**
     * GUARDAR EVALUACION POST/PUT
     * @param request
     * @param token
     * @return
     */
    @Override
    public RespBase<BaseEvaluacionDTO> guardarEvaluacion(ReqBase<ReqGuardarBaseEvaluacion> request, MyJsonWebToken token) {
        if (request.getPayload() == null) {
            throw new NotFoundException("el payload es null");
        }
        Optional<Base> base = baseRepository.findById(request.getPayload().getBaseId());
        if (!base.isPresent())
            throw new NotFoundException("no existe la base con el id: (" + request.getPayload().getBaseId() + ") en base de datos");

        Optional<BaseEvaluacion> baseEvaluacion = baseEvaluacionRepository.findBaseEvaluacion(request.getPayload().getBaseId());
        BaseEvaluacion bEvaluacion;

        if (!baseEvaluacion.isPresent()) {
            bEvaluacion = BaseConverIF.converReqBaseEvaluacion.apply(request.getPayload());
            bEvaluacion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        } else {
            bEvaluacion = baseEvaluacion.get();
            bEvaluacion.setObservacion(request.getPayload().getObservacion());
            bEvaluacion.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
        }
        bEvaluacion.setBase(base.get());
        bEvaluacion.setBaseEvaluacionDetalleList(new ArrayList<>());
        bEvaluacion.setBaseEvaluacionDetalleList(settListBaseEvaluacionDetalle(request, bEvaluacion, token));
        bEvaluacion = baseEvaluacionRepository.save(bEvaluacion);

        BaseEvaluacionDTO payload = BaseConverIF.converBaseToDto.apply(bEvaluacion);
        List<BaseEvaluacionDetalleDTO> ls = baseEvaluacionRepository.listEvalDetalleByEval(bEvaluacion.getBaseEvaluacionId()).stream()
                .filter(baseEvaluacionDetalle -> !baseEvaluacionDetalle.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
                .map(baseEvaluacionDetalle -> {
                    BaseEvaluacionDetalleDTO baseEvaluacionDetalleDTO = new BaseEvaluacionDetalleDTO();
                    if (!Util.isEmpty(baseEvaluacionDetalle.getInformeDetalle())) baseEvaluacionDetalleDTO.setInformeDetalleId(baseEvaluacionDetalle.getInformeDetalle().getInformeDetalleId());
                    baseEvaluacionDetalleDTO.setEvaluacionEntidadId(baseEvaluacionDetalle.getEvaluacionEntidad().getEvaluacionEntidadId());
                    baseEvaluacionDetalleDTO.setEvaluacionDetalleId(baseEvaluacionDetalle.getEvaluacionDetalleId());
                    baseEvaluacionDetalleDTO.setBaseEvaluacionId(baseEvaluacionDetalle.getBaseEvaluacion().getBaseEvaluacionId());
                    baseEvaluacionDetalleDTO.setEstado(baseEvaluacionDetalle.getEstadoRegistro());
                    baseEvaluacionDetalleDTO.setPeso(baseEvaluacionDetalle.getPeso());
                    baseEvaluacionDetalleDTO.setPuntajeMinimo(baseEvaluacionDetalle.getPuntajeMinimo());
                    baseEvaluacionDetalleDTO.setPuntajeMaximo(baseEvaluacionDetalle.getPuntajeMaximo());
                    Optional<MaestraDetalle> maestraDetalle = maestraDetalleRepository.findById(baseEvaluacionDetalle.getEvaluacionEntidad().getTipoEvaluacionId());
                    if (maestraDetalle.isPresent()){
                        baseEvaluacionDetalleDTO.setDescripcion(maestraDetalle.get().getDescripcion());
                    }
                    return baseEvaluacionDetalleDTO;
                }).collect(Collectors.toList());
        payload.setBaseEvaluacionDetalleDTOList(ls);
        return new RespBase<BaseEvaluacionDTO>().ok(payload);
    }

    /**
     * VALIDA EVALUACIÓN POR BASE
     */
    private void validacionBaseEvaluacion(Long idbase) {
        Optional<BaseEvaluacion> baseEvaluacion = baseEvaluacionRepository.findBaseEvaluacion(idbase);
        if (baseEvaluacion.isPresent())
            throw new ConflictException("ya existe una evaluación relacionada con el id base: (" + idbase + ") en base de datos");
    }

    /**
     * VALIDAR DUPLICADOS  EN EVALUACION_DETALLE
     * POR EVALUACION_ID / EVALUACION_ENTIDAD_ID
     * @param baseEvaluacion
     */
    private void  validarEvalEntidadConEvaluacionId(BaseEvaluacion baseEvaluacion , EvaluacionEntidad evaluacionEntidad) {
        Optional<BaseEvaluacionDetalle> baseEvaluacionDetalle = baseEvaluacionDetalleRepository.findBaseEvaluacionDetalleByEvalEntAndEval(
                evaluacionEntidad.getEvaluacionEntidadId(), baseEvaluacion.getBaseEvaluacionId());
        if (baseEvaluacionDetalle.isPresent()) {
            throw new ConflictException("ya se registró evaluacionEntidadId: (" + baseEvaluacion.getBaseEvaluacionId() + " en base de datos ");
        }

    }

    /**
     * RETURN BaseEvaluacionDetalle
     * @param evalEntId
     * @param evalId
     * @return
     */
    private BaseEvaluacionDetalle findEvaluacion(Long evalEntId , Long evalId) {
        Optional<BaseEvaluacionDetalle> baseEvaluacionDetalled = baseEvaluacionDetalleRepository.findBaseEvaluacionDetalleByEvalEntAndEval(
                evalEntId, evalId);
        if (baseEvaluacionDetalled.isPresent()) {
            return baseEvaluacionDetalled.get();
        }
        return null;
    }


    /**
     * SETT lIST BaseEvaluacionDetalle
     * PARA POST / UPDATE
     * @param request
     * @return
     */
    private List<BaseEvaluacionDetalle> settListBaseEvaluacionDetalle(ReqBase<ReqGuardarBaseEvaluacion> request , BaseEvaluacion bEvaluacion , MyJsonWebToken token) {
        return request.getPayload().getBaseEvaluacionDetalleDTOList().stream()
                .map(baseEvaluacionDetalleDTO -> {
                    BaseEvaluacionDetalle baseEvaluacionDetalle = new BaseEvaluacionDetalle();
                    baseEvaluacionDetalle.setBaseEvaluacion(bEvaluacion);
                    baseEvaluacionDetalle.setEvaluacionDetalleId(baseEvaluacionDetalleDTO.getEvaluacionDetalleId());
                    Optional<InformeDetalle> informeDetalle = null;
                    Optional<EvaluacionEntidad> evaluacionEntidad = null;

                    if (!Util.isEmpty(baseEvaluacionDetalleDTO.getInformeDetalleId())) {
                        informeDetalle = informeDetalleRepository.findById(baseEvaluacionDetalleDTO.getInformeDetalleId());
                        if (!informeDetalle.isPresent()) {
                            throw new NotFoundException("informeDetalleId: (" + baseEvaluacionDetalleDTO.getInformeDetalleId() + ") no existe en base de datos");
                        } else {
                            baseEvaluacionDetalle.setInformeDetalle(informeDetalle.get());
                        }
                    }

                    if (!Util.isEmpty(baseEvaluacionDetalleDTO.getEvaluacionEntidadId())) {
                        evaluacionEntidad = evaluacionEntidadRepository.findById(baseEvaluacionDetalleDTO.getEvaluacionEntidadId());
                        if (!evaluacionEntidad.isPresent()) {
                            throw new NotFoundException("evaluacionEntidadId: (" + baseEvaluacionDetalleDTO.getEvaluacionEntidadId() + ") no existe en base de datos");
                        } else {
                            baseEvaluacionDetalle.setEvaluacionEntidad(evaluacionEntidad.get());
                        }
                    }

                    if (baseEvaluacionDetalleDTO.getEvaluacionDetalleId() == null) {
                        if (evaluacionEntidad.isPresent())
                            validarEvalEntidadConEvaluacionId(bEvaluacion, evaluacionEntidad.get());
                        baseEvaluacionDetalle.setPeso(Long.valueOf(evaluacionEntidad.get().getPeso()));
                        baseEvaluacionDetalle.setPuntajeMinimo(Long.valueOf(evaluacionEntidad.get().getPuntajeMinimo()));
                        baseEvaluacionDetalle.setPuntajeMaximo(Long.valueOf(evaluacionEntidad.get().getPuntajeMaximo()));
                        baseEvaluacionDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    } else {
                        BaseEvaluacionDetalle evaluacionDetalle = findEvaluacion(baseEvaluacionDetalleDTO.getEvaluacionEntidadId(), bEvaluacion.getBaseEvaluacionId());
                        baseEvaluacionDetalle.setCampoSegUpd(baseEvaluacionDetalleDTO.getEstado(), token.getUsuario().getUsuario(), Instant.now());
                        if (evaluacionDetalle != null) {
                            baseEvaluacionDetalle.setPeso(evaluacionDetalle.getPeso() == null ? null : evaluacionDetalle.getPeso());
                            baseEvaluacionDetalle.setPuntajeMinimo(evaluacionDetalle.getPuntajeMinimo() == null ? null : evaluacionDetalle.getPuntajeMinimo());
                            baseEvaluacionDetalle.setPuntajeMaximo(evaluacionDetalle.getPuntajeMaximo() == null ? null : evaluacionDetalle.getPuntajeMaximo());
                        }


                    }

                    return baseEvaluacionDetalle;
                }).collect(Collectors.toList());
    }

//    @Override
//    @Transactional(transactionManager = "convocatoriaTransactionManager")
//    public RespBase<BaseEvaluacionDTO> actualizarEvaluacion(ReqBase<ReqActualizarBaseEvaluacion> request, MyJsonWebToken token,
//                                                            Long baseEvaluacionId) {
//
//        RespBase<BaseEvaluacionDTO> response = new RespBase<>();
//        BaseEvaluacionDTO payload = new BaseEvaluacionDTO();
//        try {
//            if (request.getPayload() == null) {
//                response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
//                        "Base Evaluación es nulo");
//                return response;
//            }
//
//            BaseEvaluacion bEvaluacion = baseEvaluacionRepository.getOne(baseEvaluacionId);
//            if (!Util.isEmpty(bEvaluacion)) {
//                bEvaluacion.setBaseId(request.getPayload().getBaseId());
//                bEvaluacion.setInformeDetalleId(request.getPayload().getInformeDetalleId());
//                bEvaluacion.setObservacion(request.getPayload().getObservacion());
//                bEvaluacion.setCampoSegUpd(request.getPayload().isEstadoRegistro() ? Constantes.ACTIVO : Constantes.INACTIVO, token.getUsuario().getUsuario(), Instant.now());
//                bEvaluacion = baseEvaluacionRepository.save(bEvaluacion);
//
//                Base base = baseRepository.getOne(bEvaluacion.getBaseId());
//                if (request.getPayload().isEstadoCondicion()) {
//                    base.setCondicionEtapa4(Constantes.CONDICION_CONCLUIDO);
//                    base.setComentarioEtapa4(null);
//                } else {
//                    base.setCondicionEtapa4(Constantes.CONDICION_OBSERVADO);
//                    base.setComentarioEtapa4(request.getPayload().getComentarioCondicion());
//                }
//                base.setCampoSegUpd(base.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
//                baseRepository.save(base);
//
//                payload = BaseConverIF.converBaseToDto.apply(bEvaluacion);
//            } else {
//                response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
//                        "Base Evaluación no existe");
//                return response;
//            }
//        } catch (Exception e) {
//            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
//                    "Error al actualizar evaluación");
//            return response;
//        }
//
//        return new RespBase<BaseEvaluacionDTO>().ok(payload);
//    }

    @Override
    public RespBase<DatosConcursoDTO> updateConcursoDto(ReqBase<DatosConcursoDTO> request, MyJsonWebToken token) {

        RespBase<DatosConcursoDTO> response = new RespBase<>();

        if (Util.isEmpty(request.getPayload().getDatoConcursoId())) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "el id de dato concurso es null");
            return response;
        }


        Optional<DatosConcurso> datosConcursoOp = datosConcursoRepository.findByidDa(request.getPayload().getDatoConcursoId());
        if (!datosConcursoOp.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "dato concurso no existe con el id: " + request.getPayload().getDatoConcursoId());
            return response;
        }

        if (Util.isEmpty(request.getPayload().getBaseId())) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "el id de base es null");
            return response;
        }

        Optional<Base> base = baseRepository.findByidBase(request.getPayload().getBaseId());

        if (!base.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Base con el id " + request.getPayload().getBaseId() + " no existe");
            return response;
        }


        Base baseBD = new Base();
        baseBD.setBaseId(request.getPayload().getBaseId());

        DatosConcurso datosConcurso = BaseConverIF.converDtoConcurso.apply(request.getPayload());
        
        RespBase<RespUnidadOrganica> responseUo = entidadRepository.obtieneUnidadOrganica(request.getPayload().getBaseEntidadId());
        if (responseUo.getStatus().getSuccess()) {
        	if (responseUo.getPayload() != null) {
        		datosConcurso.setNombreUnidadOrganica(responseUo.getPayload().getListaUnidadOrganica() == null ? null 
        				: devolverDescripcionUnidadOrgnica(responseUo, request.getPayload().getUnidadOrganicaId()));
        	}
        }
        RespBase<RespOrganigrama> responseO = entidadRepository.obtieneOrgano(request.getPayload().getBaseEntidadId(),variablesSistema.ENTIDAD_ORG_TIPO);
        if (responseO.getStatus().getSuccess()) {
        	if (responseO.getPayload() != null) {
        		datosConcurso.setNombreOrgano(responseO.getPayload().getListaOrganigrama() == null ? null 
        				: devolverDescripcionOrgano(responseO, request.getPayload().getOrganoResponsableId()));
        	}
        }     
        datosConcurso.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
        datosConcurso.setBase(baseBD);
        datosConcursoRepository.save(datosConcurso);

        List<DatosConcursoDTO> lsout = baseRepositoryJdbc.listDatosConcursoDtos(baseBD.getBaseId() , datosConcurso.getDatoConcursoId());
        DatosConcursoDTO payload = new DatosConcursoDTO();
        if (!lsout.isEmpty())  payload = lsout.get(0);
        return new RespBase<DatosConcursoDTO>().ok(payload);
    }

    @Override
    public RespBase<CondicionDto> cambioCondicion(ReqBase<CondicionDto> request, MyJsonWebToken token) {
        RespBase<CondicionDto> response = new RespBase<>();
        Optional<Base> base = baseRepository.findByidBase(request.getPayload().getBaseId());

        if (!base.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Base con el id " + request.getPayload().getBaseId() + " no existe");
            return response;
        }

        /*temporal*/
        Base baseBD = new Base();
        baseBD.setBaseId(request.getPayload().getBaseId());
        baseBD.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
        baseBD.setCondicionEtapa1(request.getPayload().getCondicion());
        baseBD.setComentarioEtapa1(request.getPayload().getComentario());

        /*OBSERVADO*/
        if (request.getPayload().getCondicion().equalsIgnoreCase(Constantes.ETAPA_BASE_OBSERVADO)) {
            List<MaestraDetalleDto> ls = baseRepositoryJdbc.maestraDetalle(Constantes.COD_PRO_MA_DET_OBSERVADO, Constantes.COD_TABLA_SITUACION_ETAPA);
            if (ls.isEmpty()) {
                response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                        "no se encontro id de observado");
                return response;
            }
            baseBD.setEtapaId(ls.get(0).getMaestraDetalleId());

        }


        /*EN PROCESO // ESTA PARTE YA NO IRIA / EL SERVICIO ESTA EN EVALUACION A VER SI VA
        if (request.getPayload().getCondicion().equalsIgnoreCase(Constantes.ETAPA_BASE_EN_PROCESO)) {
            List<MaestraDetalleDto> ls = baseRepositoryJdbc.maestraDetalle(COD_PRO_MA_DET_PROCESO, COD_PRO_MAESTRA);
            if (ls.isEmpty()) {
                response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                        "no se encontro id en proceso ");
                return response;
            }
            baseBD.setEtapaId(ls.get(0).getMaestraDetalleId());

        }
       */
        boolean resp = baseRepositoryJdbc.updateBase(baseBD, request.getPayload().getEtapa());

        if (!resp) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Error al cambio de condicion en base: " + request.getPayload().getBaseId());
            return response;
        }
        CondicionDto payload = new CondicionDto();
        payload.setBaseId(baseBD.getBaseId());
        payload.setComentario(baseBD.getComentarioEtapa1());

        Optional<MaestraDetalle> op = maestraDetalleRepository.findById(baseBD.getEtapaId());
        if (op.isPresent()) {
            payload.setEtapaNombre(op.get().getDescripcion());
            payload.setEtapaId(op.get().getMaeDetalleId());
        }

        payload.setEtapa(request.getPayload().getEtapa());
        payload.setCondicion(request.getPayload().getCondicion());

        return new RespBase<CondicionDto>().ok(payload);
    }

    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
    public RespBase<RespSaveUpdateInfCompl> guardarInformacionComp(ReqBase<ReqGuardarInformacionComp> request,
                                                                MyJsonWebToken token) {
        RespBase<RespSaveUpdateInfCompl> response = new RespBase<>();
        RespSaveUpdateInfCompl payload = new RespSaveUpdateInfCompl();
        try {
        	String mensajeError = ValidateReqBase.validatePayloadInformacionComp.apply(request.getPayload());
            if (!StringUtils.isEmpty(mensajeError)) {
				return ParametrosUtil.setearResponse(response, Boolean.FALSE, mensajeError);
            }

            Long idTipoInfBonificacion = guardarInformeCompl.getMaeDetalleByCode(Constantes.COD_TABLA_TIPO_INFORME).get(Constantes.COD_PRO_MA_DET_BONIFICACION);
            
            List<InformeComplement> informeCompBD =baseInformeComplRepository.findByBaseId(request.getPayload().getBaseId());
            
            List<InformacionComplDTO> informes = request.getPayload().getInformes();
			List<InformacionComplDTO> listIdBonificacion = informes.stream().filter(c-> c.getTipoInformeId().equals(idTipoInfBonificacion)).collect(Collectors.toList());
            List<InformacionComplDTO> listIdInformeDetalle = informes.stream().filter(c -> !listIdBonificacion.contains(c)).collect(Collectors.toList());
            if(CollectionUtils.isNotEmpty(informeCompBD)) {
            	//Actualizar
            	guardarInformeCompl.actualizarInformeCompl(listIdInformeDetalle, informeCompBD, listIdBonificacion, request, token,
						idTipoInfBonificacion);
            		
            }else {
            	//Registrar
                if(CollectionUtils.isNotEmpty(listIdBonificacion)) {
                	for (InformacionComplDTO bonificacion : listIdBonificacion) {
    					
    					InformeComplement informeComplement = new InformeComplement();
    					informeComplement.setBonificacionId(bonificacion.getIdInforme());
    					informeComplement.setBaseId(request.getPayload().getBaseId());
    					informeComplement.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
    					informeComplement.setTipoInfoId(idTipoInfBonificacion);
    					baseInformeComplRepository.save(informeComplement);
    				}
                	
                }
                
                guardarInformeCompl.saveListInformeDetalle(listIdInformeDetalle, request, token);

            }

            /*MOVIMIENTOS INICIO*/
            if (validarBaseObservado(request.getPayload().getBaseId()) == 0) {
                BaseDTO baseDTO = baseRepositoryJdbc.getBase(request.getPayload().getBaseId() , null);
                                
                List<MaestraDetalle> listaMaeDetalle = maestraDetalleRepository.findDetalleByCodeCabeceraEstado(Constantes.COD_TABLA_SITUACION_ETAPA, "1");
    			Map<String, Long> mapDetalle = listaMaeDetalle.stream().filter(Objects :: nonNull).collect(Collectors.toMap(MaestraDetalle::getCodProg, MaestraDetalle::getMaeDetalleId));
                
    			Long idPorRevisar = mapDetalle.get(Constantes.COD_PRO_MA_DET_POR_REVISAR);
                Long idProceso = mapDetalle.get(Constantes.COD_PRO_MA_DET_EN_PROCESO);
    			
                if (baseDTO == null) {
                    response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                            "error Base no existe con el id: " + request.getPayload().getBaseId());
                    return response;
                }

                int rpta = baseRepository.updateTipoIdEtapa6(token.getUsuario().getUsuario(),
                        Instant.now(), null, null, null, null, null, null, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO,
                        Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO, request.getPayload().getObservaciones(), idPorRevisar, request.getPayload().getBaseId()
                );

                if (rpta <= 0) {
                    response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                            "error al actualizar Base: " + request.getPayload().getBaseId());
                    return response;
                }
                
                Movimiento mov = new Movimiento();
                mov.setBaseId(request.getPayload().getBaseId());
                mov.setEstadoOldId(idProceso);
                mov.setEstadoNewId(idPorRevisar);
                mov.setEntidadId(baseDTO.getEntidadId());
                mov.setRolId(null);
                mov.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                movimientoRepository.save(mov);
            }


            /*MOVIMIENTOS FIN*/

            /*
            Long idPorAprobar = guardarInformeCompl.getMaeDetalleByCode("TIP_ETA_RE").get("POR APROBAR");
            Base base = baseRepository.getOne(request.getPayload().getBaseId());
            base.setCondicionEtapa6(Constantes.CONDICION_CONCLUIDO);
            base.setCampoSegUpd(base.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
            base.setObservacionEtapa6(request.getPayload().getObservaciones());
            base.setEtapaId(idPorAprobar);
            baseRepository.save(base);
            */

        } catch (Exception e) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Error al guardar informacion complementaria");
            return response;
        }
        payload.setResultado("Se registró correctamente");
        return new RespBase<RespSaveUpdateInfCompl>().ok(payload);
    }

    /**
     * VALIDA QUE LA BASE NO TENGA NINGUNA ETAPA OBSERVADO: 1
     * @param idBase
     * @return
     */
    private Long validarBaseObservado(Long idBase){
     return baseRepositoryJdbc.isObservado(idBase);
    }

    /**
     * TRAE LA ACTUAL OBSERVACION
     * DE TBL_BASE (LA ULTIMA)
     * @param baseId
     * @param etapa
     * @return
     */
    @Override
    public RespBase<CondicionDto> getCambioCondicion(Long baseId, Long etapa) {
        RespBase<CondicionDto> response = new RespBase<>();
        CondicionDto payload = new CondicionDto();
        List<CondicionDto> ls = baseRepositoryJdbc.getCambioCondicion(baseId, etapa);
        if (ls.isEmpty()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "sin resultados base id: " + baseId);
            return response;
        }


        payload = ls.get(0);

        return new RespBase<CondicionDto>().ok(payload);
    }
    
	@Override
	public RespBase<BaseEvaluacionDTO> ObtenerBaseEvaluacion(Long baseId) {
        RespBase<ObtenerBaseEvaluacionDTO> response = new RespBase<>();
        BaseEvaluacionDTO payload = new BaseEvaluacionDTO();

        Optional<Base> base = baseRepository.findById(baseId);
        if (!base.isPresent())
            throw new NotFoundException("no existe la base con el id: (" + baseId + ") en base de datos");

        payload.setBaseId(baseId);
        Optional<BaseEvaluacion> baseEvaluacion = baseEvaluacionRepository.findBaseEvaluacion(baseId);

        if (baseEvaluacion.isPresent()) {
            payload.setBaseEvaluacionId(baseEvaluacion.get().getBaseEvaluacionId());
            payload.setObservacion(baseEvaluacion.get().getObservacion());
            payload.setBaseId(baseEvaluacion.get().getBase().getBaseId());
        }
        payload.setBaseEvaluacionDetalleDTOList(setListBaseEvaluacionDetalleDTO(baseId , baseEvaluacion , base.get()));
        return new RespBase<BaseEvaluacionDTO>().ok(payload);

    }

    /**
     * SETT LISTA DE BaseEvaluacionDetalleDTO
     * CUANDO NO HAYA DATA TRAE DE JERARQUIAS
     * @param idBase
     * @param baseEvaluacion
     * @return
     */
    private List<BaseEvaluacionDetalleDTO> setListBaseEvaluacionDetalleDTO(Long idBase , Optional<BaseEvaluacion> baseEvaluacion , Base base) {
        List<BaseEvaluacionDetalleDTO> ls = new ArrayList<>();
        if (baseEvaluacion.isPresent()) {
            List<BaseEvaluacionDetalleDTO> lsPrev = baseEvaluacionRepository.listEvalDetalleByEval(baseEvaluacion.get().getBaseEvaluacionId()).stream()
                    .filter(baseEvaluacionDetalle -> !baseEvaluacionDetalle.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
                    .map(baseEvaluacionDetalle -> {
                        BaseEvaluacionDetalleDTO baseEvaluacionDetalleDTO = new BaseEvaluacionDetalleDTO();
                        if (!Util.isEmpty(baseEvaluacionDetalle.getInformeDetalle()))baseEvaluacionDetalleDTO.setInformeDetalleId(baseEvaluacionDetalle.getInformeDetalle().getInformeDetalleId());
                        baseEvaluacionDetalleDTO.setEvaluacionEntidadId(baseEvaluacionDetalle.getEvaluacionEntidad().getEvaluacionEntidadId());
                        baseEvaluacionDetalleDTO.setBaseEvaluacionId(baseEvaluacionDetalle.getBaseEvaluacion().getBaseEvaluacionId());
                        baseEvaluacionDetalleDTO.setEvaluacionDetalleId(baseEvaluacionDetalle.getEvaluacionDetalleId());
                        baseEvaluacionDetalleDTO.setPeso(baseEvaluacionDetalle.getPeso());
                        baseEvaluacionDetalleDTO.setPuntajeMinimo(baseEvaluacionDetalle.getPuntajeMinimo());
                        baseEvaluacionDetalleDTO.setPuntajeMaximo(baseEvaluacionDetalle.getPuntajeMaximo());
                        baseEvaluacionDetalleDTO.setEstado(baseEvaluacionDetalle.getEstadoRegistro());
                        baseEvaluacionDetalleDTO.setTipoEvaluacionId(baseEvaluacionDetalle.getEvaluacionEntidad().getTipoEvaluacionId());
                        Optional<MaestraDetalle> maestraDetalle = maestraDetalleRepository.findById(baseEvaluacionDetalle.getEvaluacionEntidad().getTipoEvaluacionId());

                        if (maestraDetalle.isPresent()){
                            baseEvaluacionDetalleDTO.setDescripcion(maestraDetalle.get().getDescripcion());
                            baseEvaluacionDetalleDTO.setCodProg(maestraDetalle.get().getCodProg());
                        }
                        return baseEvaluacionDetalleDTO;
                    }).collect(Collectors.toList());
            ls.addAll(lsPrev);
        } else {
            List<Jerarquia> jerarquia = jerarquiaRepository.getJerarquia(base.getRegimenId(), base.getModalidadId(), base.getTipoId());
            if (jerarquia != null && !jerarquia.isEmpty()) {
                List<BaseEvaluacionDetalleDTO> lsPrev = evaluacionEntidadRepositoryJdbc.buscarEEntidad(base.getEntidadId(), jerarquia.get(0).getJerarquiaId()).stream()
                        .map(detalleEvaluacionEntidadDTO -> {
                            BaseEvaluacionDetalleDTO baseEvaluacionDetalleDTO = new BaseEvaluacionDetalleDTO();
                            baseEvaluacionDetalleDTO.setPeso(Long.valueOf(detalleEvaluacionEntidadDTO.getPeso()));
                            baseEvaluacionDetalleDTO.setPuntajeMinimo(Long.valueOf(detalleEvaluacionEntidadDTO.getPuntajeMin()));
                            baseEvaluacionDetalleDTO.setPuntajeMaximo(Long.valueOf(detalleEvaluacionEntidadDTO.getPuntajeMax()));
                            baseEvaluacionDetalleDTO.setEvaluacionEntidadId(detalleEvaluacionEntidadDTO.getEvaluacionEntidadId());
                            baseEvaluacionDetalleDTO.setDescripcion(detalleEvaluacionEntidadDTO.getDescripcion());
                            baseEvaluacionDetalleDTO.setEstado(Constantes.ACTIVO);
                            return baseEvaluacionDetalleDTO;
                        }).collect(Collectors.toList());

                ls.addAll(lsPrev);
            }

        }

        return ls;
    }
	
	@Override
	public RespBase<RespGuardarInfComplementaria> obtenerDataEtapa6(Long baseId) {
        List<InformeComplement> informeCompBD =baseInformeComplRepository.findByBaseId(baseId);
        Base base = baseRepository.getOne(baseId);
        List<InformacionComplDTO> listaInfoDto = new ArrayList<>();
        
        RespGuardarInfComplementaria payload = new RespGuardarInfComplementaria();
        
        if(CollectionUtils.isNotEmpty(informeCompBD)) {
        	
        	informeCompBD.stream().filter(c->StringUtils.equals(Constantes.ACTIVO,c.getEstadoRegistro())).forEach(c->{
        		InformacionComplDTO obj1 = new InformacionComplDTO();
        		
        		if(Objects.nonNull(c.getBonificacionId()) && Objects.nonNull(c.getTipoInfoId())) {
        			Bonificacion bonificacion =   bonificacionRepository.getOne(c.getBonificacionId());
        			obj1.setIdInforme(c.getBonificacionId());
            		obj1.setTipoInformeId(c.getTipoInfoId());
            		obj1.setNombreInforme(bonificacion.getTitulo());
            		listaInfoDto.add(obj1);
        		}
        		
				if (CollectionUtils.isNotEmpty(c.getComplDetalles())) {
					c.getComplDetalles().stream()
							.filter(x -> StringUtils.equals(Constantes.ACTIVO, x.getEstadoRegistro())).forEach(m -> {
								InformacionComplDTO obj2 = new InformacionComplDTO();
								obj2.setIdInforme(m.getInformeDetalleId());
								InformeDetalle detalle = informeDetalleRepository.getOne(m.getInformeDetalleId());
								if (Objects.nonNull(detalle)) {
									obj2.setTipoInformeId(detalle.getTipoInfo());
									obj2.setNombreInforme(detalle.getTitulo());
								}
								listaInfoDto.add(obj2);
							});
				}
        	});
        	
        }
        
        payload.setObservaciones(base.getObservacionEtapa6());
        payload.setInformes(CollectionUtils.isEmpty(listaInfoDto) ? null : listaInfoDto);
		return new RespBase<RespGuardarInfComplementaria>().ok(payload);
	}

    /**
     * REGISTRA LOS MOVIMIENTOS Y SUS OBSERVACIONES
     * EDITA BASE EL CAMPO ETAPAID SEGUN EL ESTADO DEL PROCESO
     * GUARDA LAS OBSERVACIONES EN EL CAMPO COMENTARIO SEGUN SEA LA ETAPA
     * @param request
     * @param token
     * @return
     */
    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
    public RespBase<MovimientoDTO> registrarMovimiento(ReqBase<MovimientoDTO> request, MyJsonWebToken token) {
        RespBase<MovimientoDTO> response = new RespBase<>();
        Optional<Base> op = baseRepository.findByidBase(request.getPayload().getBaseId());
        List<Long> ls = Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L);
        List<MaestraDetalle> lstDetEstConvocatoria = new ArrayList<>();
        
        if (!op.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "error Base no existe con el id: " + request.getPayload().getBaseId());
            return response;
        }
        if (request.getPayload().getObservacionDTOList() != null && !request.getPayload().getObservacionDTOList().isEmpty() ) {
				for (ObservacionDTO it : request.getPayload().getObservacionDTOList()) {
	                if (!ls.contains(it.getEtapa())) {
	                    response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
	                            "error las etapas son del 1 al 6: el valor " + it.getEtapa() + " es incorrecto");
	                    return response;
	                }
	            }
	
            boolean rpta = settUpdatebase(request.getPayload(), token);
            
            if (!rpta) {
                response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                        "error al actualizar Base: " + request.getPayload().getBaseId());

                return response;
            }
        }else {
        	 int rpta=0;
        	if (request.getPayload().getCoordinadorId() == null) {
        		rpta = baseRepository.updateTipoId(token.getUsuario().getUsuario(),
                        Instant.now() ,null, null, null, null, null, null, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO,
                        Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO ,request.getPayload().getEstadoNewId() ,request.getPayload().getBaseId()
                );
            }else {
            	rpta = baseRepository.updateTipoIdAndCordinador(token.getUsuario().getUsuario(),
                        Instant.now() ,null, null, null, null, null, null, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO,
                        Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO, Constantes.ETAPA_BASE_NO_OBSERVADO ,request.getPayload().getEstadoNewId() ,
                        request.getPayload().getBaseId(),request.getPayload().getCoordinadorId(),request.getPayload().getNombreCoordinador()
                );
            }
      
            
            if (rpta <= 0) {
                response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                        "error al actualizar Base: " + request.getPayload().getBaseId());
                return response;
            }
        }
        //Logica para insertar en convocatoria si es por publicar
        lstDetEstConvocatoria = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_SITUACION_ETAPA);
		Map<Long, String> mapDetalleEstConvo = lstDetEstConvocatoria.stream().filter(Objects::nonNull)
                 .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));
		
		if(mapDetalleEstConvo.get(request.getPayload().getEstadoNewId()).equals(Constantes.COD_PRO_MA_DET_POR_PUBLICAR)) {
			convocatoriaService.registrarConvocatoria(request.getPayload().getBaseId(), token);
		}
        
        
        Movimiento mov = BaseConverIF.converMovimiento.apply(request.getPayload());
        mov.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        mov.setObservacionList(new ArrayList<>());
        mov.setObservacionList(settObserList(request.getPayload(), mov, token));
        movimientoRepository.save(mov);

        /**
         *  QUEUE REPORTE ETAPA 4
         */
        if (mov.getMovimientoId()!= null){
            sendQueueEtapa4(request.getPayload().getCoordinadorId() , token.getUsuario().getUsuario() ,  mov);
        }



        MovimientoDTO payload = BaseConverIF.converMovimientoDto.apply(mov);
        payload.setObservacionDTOList(settObserListDto(mov));
        return new RespBase<MovimientoDTO>().ok(payload);
    }

    /**
     * QUUEUE REPORTE ETAPA 4
     * @param coordId
     * @param coordinador
     */
    private void sendQueueEtapa4 (Long coordId , String coordinador ,  Movimiento mov) {
        try {
            ReporteConvocatoriaDTO dto = new ReporteConvocatoriaDTO();
            dto.setCoordinadorId(coordId);
            dto.setNombreCoordinador(coordinador);
            dto.setBaseId(mov.getBaseId());
            dto.setEtapaQueue(Constantes.ETAPA_REG_MOVI);
            reporteConvocatoriaProducer.sendMessageBase(dto);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

    }
    /**
     * TRAE LOS MOVIMIENTOS CON SUS OBSERVACIONES
     * @param baseId
     * @return
     */
    @Override
    public RespBase<RespObtieneLista<MovimientoDTO>> getMovimientos(Long baseId) {
        List<MovimientoDTO> ls = movimientoRepositoryJdbc.listMovimientos(baseId , null);
        ls.forEach((e) -> {
            e.setObservacionDTOList(movimientoRepositoryJdbc.listObservacionDtos(e.getMovimientoId()));
        });
        RespObtieneLista<MovimientoDTO> respPayload = new RespObtieneLista<>();
        respPayload.setCount(ls.size());
        respPayload.setItems(ls);
        return new RespBase<RespObtieneLista<MovimientoDTO>>().ok(respPayload);
    }

    @Override
    public RespBase<RespObtieneLista> listarRequisGeneralByBaseId(Long baseId) {
        RespObtieneLista response = new RespObtieneLista();
        RequisitoGeneral filtro = new RequisitoGeneral();
        filtro.setBase(new Base(baseId));
        filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
        Example<RequisitoGeneral> example = Example.of(filtro);

        List<RequisitoGeneral> listaReqGen = this.requisitoGeneralRepository.findAll(example);

        List<RespRequisitoGeneralDTO> lista = new ArrayList<>();
        listaReqGen.forEach( item -> { lista.add(new RespRequisitoGeneralDTO(item)); } );

        response.setCount(lista.size());
        response.setItems(lista);

        return new RespBase<RespObtieneLista>().ok(response);
    }


    /**
     * LOGO POR ENTIDAD
     * @return
     */
//    private String obtenerLogoPorEntidadId(String logoEntidad) {
//        String logo = "src/main/resources/nologo.png";
//        if (!Util.isEmpty(logoEntidad))
//            logo = variablesSistema.fileServer + logoEntidad;
//        return logo;
//    }

    /**
     * SETT VALORES DE OBSERVACIÓN TO LIST A MOVIMIENTO
     * @param mov
     * @return
     */
    private List<Observacion> settObserList(MovimientoDTO mov , Movimiento movi ,MyJsonWebToken token) {
        List<Observacion> ls = new ArrayList<>();
        if (mov.getObservacionDTOList() != null && !mov.getObservacionDTOList().isEmpty()) {
            mov.getObservacionDTOList().forEach((e) -> {
                Observacion obs = BaseConverIF.converObservacion.apply(e);
                obs.setMovimiento(movi);
                obs.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                ls.add(obs);
            });

        }

        return ls;
    }

    /**
     * SETT LOS CAMPOS DE BASE PARA UPDATE
     * AL HACER UN MOVIMIENTO
     */
    private boolean settUpdatebase(MovimientoDTO dto , MyJsonWebToken token) {
        boolean rpta = true;
        if (dto.getObservacionDTOList() != null && !dto.getObservacionDTOList().isEmpty()) {
            for (ObservacionDTO it : dto.getObservacionDTOList()) {
                Base baseBD = new Base();
                baseBD.setBaseId(dto.getBaseId());
                baseBD.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
                baseBD.setCondicionEtapa1(Constantes.ETAPA_BASE_OBSERVADO);
                baseBD.setComentarioEtapa1(it.getObservacion().trim().toUpperCase());
                baseBD.setCoordinadorId(dto.getCoordinadorId());
                baseBD.setNombreCoordinador(dto.getNombreCoordinador());
                baseBD.setEtapaId(dto.getEstadoNewId());
                baseBD.setCoordinadorId(dto.getCoordinadorId());
                baseBD.setNombreCoordinador(dto.getNombreCoordinador());
                rpta = baseRepositoryJdbc.updateBase(baseBD, it.getEtapa());
                if (!rpta) return false;
            }
        }
        return rpta;
    }

    /**
     * SETT LIST OBSERVACION PARA EL DTO DE
     * SALIDA DEL REGISTRO DE MOVIMIENTO
     * @param movi
     * @return
     */
    private List<ObservacionDTO> settObserListDto(Movimiento movi) {
        List<ObservacionDTO> ls = new ArrayList<>();
        if (movi.getObservacionList() != null && !movi.getObservacionList().isEmpty()) {
            movi.getObservacionList().forEach((e) -> {
                ObservacionDTO obs = BaseConverIF.converObservacionDto.apply(e);
                obs.setMovimientoId(movi.getMovimientoId());
                ls.add(obs);
            });
        }
        return ls;
    }
}
