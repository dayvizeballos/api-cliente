package pe.gob.servir.convocatoria.repository.impl;


import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.repository.BaseRepositoryQueue;
import pe.gob.servir.convocatoria.request.dto.BaseDTO;
import pe.gob.servir.convocatoria.request.dto.CorreoDTO;
import pe.gob.servir.convocatoria.request.dto.MaestraDetalleDto;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BaseRepositoryQueueImpl implements BaseRepositoryQueue {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BaseRepositoryQueueImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }
    private static final String SQL_FIND_BASES_MIGRAR_MONGO = "  select  tb.base_id as idBase, \n" +
            "   tbp.perfil_id  as idPerfil,\n" +
            "   tbp.base_perfil_id  as basePerfilId,\n" +
            "   (translate((select p.nombre_puesto from sch_convocatoria.tbl_perfil p where p.perfil_id = tbp.perfil_id),'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜ','aeiouAEIOUaeiouAEIOU')) as nombrePuesto,\n" +
            "   tb.regimen_id as idRegimen, \n" +
            "   sch_convocatoria.fnc_get_desc_mae_detalle(tb.regimen_id) as regimen,\n" +
            "   tb.entidad_id as idEntidad,\n" +
            "   tbp.remuneracion as remuneracion, \n" +
            "   sch_convocatoria.fnc_get_desc_mae_detalle(tbp.condicion_trabajo_id) as prestacionServicio,\n" +
            "   tbp.nro_vacante as vacantes,\n" +
            "   (select tpe.anio_experiencia_total from sch_convocatoria.tbl_perfil_experiencia tpe  where tpe.perfil_id = tbp.perfil_id) as aniosExpTotal,\n" +
            "   tbp.sede_id as idSede,\n" +
            "   tbp.sede_direccion as sede,\n" +
            "   (select d.correo from sch_base.tbl_base_datos_concurso d where d.base_id = tb.base_id) as correo,\n" +
            "   '' as direccion,\n" +
            "   '' as telefono,\n" +
            "   '' as anexo,\n" +
            "   '' as contacto,\n" +
            "   tc.url_convocatoria as urlPdfBase,\n" +
            "   tc.codigo_convocatoria as codigoConvocatoria,\n" +
            "   (select tmd.mae_detalle_id from sch_convocatoria.tbl_mae_detalle tmd where tmd.cod_prog = '1'\n" +
            "    and tmd.mae_cabecera_id = (select tmc.mae_cabecera_id from sch_convocatoria.tbl_mae_cabecera tmc where tmc.codigo_cabecera = 'TIP_EST_DIF')) as idEstado,\n" +
            "   (select tmd.descripcion from sch_convocatoria.tbl_mae_detalle tmd where tmd.cod_prog = '1'\n" +
            "    and tmd.mae_cabecera_id = (select tmc.mae_cabecera_id from sch_convocatoria.tbl_mae_cabecera tmc where tmc.codigo_cabecera = 'TIP_EST_DIF')) as estado,\n" +
            "   (select tmd.cod_prog from sch_convocatoria.tbl_mae_detalle tmd where tmd.cod_prog = '1'\n" +
            "    and tmd.mae_cabecera_id = (select tmc.mae_cabecera_id from sch_convocatoria.tbl_mae_cabecera tmc where tmc.codigo_cabecera = 'TIP_EST_DIF')) as codProEstado,\n" +
            "   tbp.depa_id as idDepartamento ,\n" +
            "   tbp.descdepa as departamento ,\n" +
            "   tbp.prov_id as idProvincia,\n" +
            "   tbp.descprov as provincia,\n" +
            "   tbp.dist_id as idDistrito,\n" +
            "   tbp.descdist as distrito,\n" +
            "  (select mision_puesto from sch_convocatoria.tbl_perfil tp where tp.perfil_id =  tbp.perfil_id) as misionPuesto,\n" +
            "  (select to_char(periodo_fin,'dd/mm/yyyy') from sch_base.tbl_base_cronograma c inner join sch_convocatoria.tbl_mae_detalle de on de.mae_detalle_id = c.etapa_id \n" +
            "	where  c.base_id = tb.base_id and de.cod_prog ='2') as fechaHasta, \n" +
            "  (select to_char(periodo_fin,'dd/mm/yyyy') from sch_base.tbl_base_cronograma c inner join sch_convocatoria.tbl_mae_detalle de on de.mae_detalle_id = c.etapa_id \n" +
            "	where  c.base_id = tb.base_id and de.cod_prog ='4') as fechaCierre \n" +
            "   from sch_base.tbl_base_perfil tbp join sch_base.tbl_base tb on tbp.base_id = tb.base_id \n" +
            "   join sch_convocatoria.tbl_convocatoria tc on tc.base_id = tb.base_id \n" +
            "   where tb.base_id = :idBase" ;

    private static final String SQL_FIND_BASE_CONOCIMIENTOS = "select \n" +
            " tmc.descripcion as nombreConocimiento,\n" +
            " tpc.horas as horas , \n" +
            " case \n" +
            " when tpc.nivel_dominio_id = 1 then 'BASICO'  --Identificador de Nivel de Dominio del Conocimiento (1:Basico 2:Intermedio 3:Avanzado)\n" +
            " when tpc.nivel_dominio_id = 2 then 'INTERMEDIO'\n" +
            " when tpc.nivel_dominio_id = 3 then 'AVANZADO' end as nivelDominio\n" +
            " from sch_convocatoria.tbl_perfil_conocimiento tpc join sch_convocatoria.tbl_mae_conocimiento tmc \n" +
            " on tpc.mae_cono_id = tmc.mae_conocimiento_id \n" +
            " where tpc.perfil_id = :idPerfil and tpc.estado_registro = '1' and tmc.estado_registro = '1' ";

    private static final String SQL_FIND_BASE_EXPERIENCIA = "select anio_experiencia_total as anioExperTotal,\n" +
            "  anio_exper_sector_publico as anioExpSecPu,\n" +
            "  sch_convocatoria.fnc_get_desc_mae_detalle(nivel_minimo_puesto_id) as nivelMiniPues\n" +
            "  from sch_convocatoria.tbl_perfil_experiencia tpe where tpe.perfil_id = :idPerfil and estado_registro = '1' ";

    private static final String SQL_FIND_BASE_FORMACION_ACADEMICA = "select nivel_educativo_id as nivelEducaId , sch_convocatoria.fnc_get_desc_mae_detalle(nivel_educativo_id) as nivelEduca,\n" +
            "  sch_convocatoria.fnc_get_desc_mae_detalle(estado_nivel_educativo_id) as estaNivelEduca,\n" +
            " sch_convocatoria.fnc_get_desc_mae_detalle(situacion_academica_id) as situAcadem,\n" +
            " sch_convocatoria.fnc_get_desc_mae_detalle(estado_situacion_academica_id) as estadoSituaAcadem,\n" +
            " nombre_grado as nombreGrado\n" +
            "from sch_convocatoria.tbl_perfil_formacion_academica tpfa where tpfa.perfil_id = :idPerfil and estado_registro = '1' ";


    private static final String SQL_FIND_BASE_CONDICIONES_ATIPICA = " select tpf.condicion_atipica as condicion, \n" +
            "  sch_convocatoria.fnc_get_desc_mae_detalle( tpf.periocidad_condicion_atipica_id) as periodo\n" +
            "  from sch_convocatoria.tbl_perfil_funcion tpf where tpf.perfil_id = :idPerfil and tpf.estado_registro = '1' ";

    private static final String SQL_FIND_BASE_HABILIDADES = " select tped.descripcion from sch_convocatoria.tbl_perfil_experiencia_detalle tped join sch_convocatoria.tbl_perfil_experiencia tpe\n" +
            " on tped.experiencia_perfil_id = tpe.experiencia_perfil_id \n" +
            " where tpe.perfil_id = ? and tped.estado_registro = '1' and tpe.estado_registro = '1' and tped.tipo_dato_experiencia_id = 1 ";

    private static final String SQL_FIND_BASE_REQUISITOS_ADICIONALES = "select tped.descripcion from sch_convocatoria.tbl_perfil_experiencia_detalle tped join sch_convocatoria.tbl_perfil_experiencia tpe \n" +
            "on tped.experiencia_perfil_id = tpe.experiencia_perfil_id \n" +
            "where tpe.perfil_id = ? and tped.estado_registro = '1'  and tpe.estado_registro = '1' and tped.tipo_dato_experiencia_id = 2  ";

    private static final String SQL_FIND_BASE_FUNCIONES_PUESTO_ACTIV_PRACTI = " select tpfd.descripcion from sch_convocatoria.tbl_perfil_funcion_detalle tpfd join sch_convocatoria.tbl_perfil_funcion tpf on tpfd.perfil_funcion_id = tpf.perfil_funcion_id \n" +
            " where tpf.perfil_id = ? and tpfd.estado_registro = '1' and tpf.estado_registro = '1' ";

    private static final String SQL_FIND_BASE_IDS_CARRERAS = " select tpcfa.carrera_id from sch_convocatoria.tbl_perfil_formacion_academica tpfa  \n" +
            "  join sch_convocatoria.tbl_perfil_carrera_form_academ tpcfa on tpcfa.perfil_formacion_academica_id = tpfa.perfil_formacion_academica_id \n" +
            " where  tpfa.perfil_id = ? and tpfa.estado_registro = '1' and tpcfa.estado_registro = '1' ";

    private static final String SQL_FIND_BASE_ENTIDADES_IDS = "select distinct entidad_id from sch_base.tbl_base tb where tb.estado_registro = '1' and tb.entidad_id is not null ";

    private static final String SQL_FIND_BASE_IDS = "select row_number() OVER (order by b.fecha_creacion ) as rnum , b.entidad_id as entidadId,  b.base_id as baseId , b.etapa_id as etapaId , b.codigo_convocatoria as codigoConvocatoria  , " +
            " TO_CHAR(b.fecha_creacion :: DATE, 'dd/mm/yyyy') as fechaCreacion  , TO_CHAR(b.fecha_publicacion :: DATE, 'dd/mm/yyyy') as fechaPublicacion,  \n" +
            " b.nombre_gestor as gestor , b.nombre_coordinador as coordinador, b.regimen_id  as regimenId, \n" +
            " (select tm.descripcion from sch_convocatoria.tbl_mae_detalle tm where tm.mae_detalle_id = b.regimen_id) as regimen,  \n" +
            " (select c.nro_vacantes from sch_base.tbl_base_datos_concurso c where c.base_id = b.base_id)  as vacantes,\n" +
            " (select d.nombre from sch_base.tbl_base_datos_concurso d where d.base_id = b.base_id) as denominacion \n" +
            " from sch_base.tbl_base b where b.estado_registro = '1' ";

    private static final String SQL_FIND_MAESTRA_DETALLE = " select tmd.mae_detalle_id  as maestraDetalleId , tmd.descripcion as condicionNombre, tmd.abreviatura as sigla from sch_convocatoria.tbl_mae_detalle tmd  join sch_convocatoria.tbl_mae_cabecera tmc \n" +
            " on tmd.mae_cabecera_id  = tmc.mae_cabecera_id where tmd.cod_prog = :codpMaDetalle \n" +
            " and tmc.mae_cabecera_id  = (select tmc2.mae_cabecera_id  from sch_convocatoria.tbl_mae_cabecera tmc2 where tmc2.codigo_cabecera = :codCabMaeCabe) ";


    private static final String SQL_FIND_BASE_CORREOS = "select row_number() OVER (order by b.fecha_creacion ) as rnum , b.entidad_id as entidadId,  b.base_id as baseId , b.etapa_id as etapaId , b.codigo_convocatoria as codigoConvocatoria  , " +
            " TO_CHAR(b.fecha_creacion :: DATE, 'dd/mm/yyyy') as fechaCreacion  , TO_CHAR(b.fecha_publicacion :: DATE, 'dd/mm/yyyy') as fechaPublicacion,  \n" +
            " b.nombre_gestor as gestor , b.nombre_coordinador as coordinador, b.regimen_id  as regimenId, \n" +
            " (select tm.descripcion from sch_convocatoria.tbl_mae_detalle tm where tm.mae_detalle_id = b.regimen_id) as regimen,  \n" +
            " (select c.nro_vacantes from sch_base.tbl_base_datos_concurso c where c.base_id = b.base_id)  as vacantes,\n" +
            " (select d.nombre from sch_base.tbl_base_datos_concurso d where d.base_id = b.base_id) as denominacion, \n" +
            " b.url as url \n" +
            " from sch_base.tbl_base b where b.entidad_id = :entidadId  and b.estado_registro = '1' ";

    private static final String SQL_FIND_COD_CONVOCATORIA = "select case when  max(tb.correlativo) is null then lpad(1::text,5,'0') else lpad((max(tb.correlativo) + 1)::text,5,'0') end as \n" +
            "correlativo  from sch_base.tbl_base tb where tb.anio = :anio and regimen_id = :regimen and tb.entidad_id = :entidad and estado_registro = '1' ";

    private static final String SQL_FIND_COD_CONVOCATORIA_2 = "select case when  max(tb.correlativo_convocatoria) is null then lpad(1::text,5,'0') else lpad((max(tb.correlativo_convocatoria) + 1)::text,5,'0') end as \n" +
            "correlativo  from sch_convocatoria.tbl_convocatoria tb where tb.anio = :anio and regimen_id = :regimen and tb.entidad_id = :entidad and estado_registro = '1' ";


    private static final String SQL_FIND_MAESTRA_DETALLE_ID = "select mae_detalle_id as maestraDetalleId, mae_cabecera_id as maeCabeceraId , descripcion as descripcion, descripcion_corta as descripcionCorta ,\n" +
            "cod_prog as codProg, abreviatura as sigla from sch_convocatoria.tbl_mae_detalle tmd \n" +
            "where tmd.mae_detalle_id = :id";

    @Override
    public List<Long> idEntidades(Long estadoProceso, String fechaHoy) {
        List<Long> list = new ArrayList<>();

        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_ENTIDADES_IDS);

            if (!Util.isEmpty(estadoProceso)) sql.append(" and tb.etapa_id = :estadoProceso ");
            if (!Util.isEmpty(fechaHoy))
                sql.append(" and TO_CHAR(tb.fecha_publicacion :: DATE, 'dd/mm/yyyy') =  :fechaHoy ");


            if (!Util.isEmpty(estadoProceso)) objectParam.put("estadoProceso", estadoProceso);
            if (!Util.isEmpty(fechaHoy)) objectParam.put("fechaHoy", fechaHoy);
            list = namedParameterJdbcTemplate.queryForList(sql.toString(), objectParam, Long.class); // (sql.toString(), objectParam) ;//(sql.toString(), objectParam);//  query(sql.toString(),objectParam, Long.class);
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<CorreoDTO> listDetalleCorreo(Long entidadId, Long estadoProceso, String fechaHoy) {

        List<CorreoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_CORREOS);
            if (!Util.isEmpty(estadoProceso)) sql.append(" and b.etapa_id = :estadoProceso ");
            if (!Util.isEmpty(fechaHoy))
                sql.append(" and TO_CHAR(b.fecha_publicacion :: DATE, 'dd/mm/yyyy') =  :fechaHoy ");

            sql.append(" order by rnum , b.fecha_creacion asc ");

            if (!Util.isEmpty(entidadId)) objectParam.put("entidadId", entidadId);
            if (!Util.isEmpty(estadoProceso)) objectParam.put("estadoProceso", estadoProceso);
            if (!Util.isEmpty(fechaHoy)) objectParam.put("fechaHoy", fechaHoy);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(CorreoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }

    }

    @Override
    public List<CorreoDTO> getBases(Long estadoProceso, String fechaHoy) {
        List<CorreoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_IDS);
            if (!Util.isEmpty(estadoProceso)) sql.append(" and b.etapa_id = :estadoProceso ");
            if (!Util.isEmpty(fechaHoy))
                sql.append(" and TO_CHAR(b.fecha_publicacion :: DATE, 'dd/mm/yyyy') =  :fechaHoy ");

            if (!Util.isEmpty(estadoProceso)) objectParam.put("estadoProceso", estadoProceso);
            if (!Util.isEmpty(fechaHoy)) objectParam.put("fechaHoy", fechaHoy);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(CorreoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }

    }

    @Override
    public List<CorreoDTO> listDetalleBases(Long estadoProceso, String fechaHoy) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<MaestraDetalleDto> maestraDetalle(String codpMaDetalle, String codCabMaeCabe) {
        List<MaestraDetalleDto> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_MAESTRA_DETALLE);
            if (!Util.isEmpty(codpMaDetalle)) objectParam.put("codpMaDetalle", codpMaDetalle);
            if (!Util.isEmpty(codCabMaeCabe)) objectParam.put("codCabMaeCabe", codCabMaeCabe);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(MaestraDetalleDto.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }

    }

    @Override
    public String codigoConvocatoria(BaseDTO baseDTO) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("anio", baseDTO.getAnio())
                .addValue("regimen", baseDTO.getRegimenId())
                .addValue("entidad", baseDTO.getEntidadId());
        return namedParameterJdbcTemplate.queryForObject(
                SQL_FIND_COD_CONVOCATORIA, namedParameters, String.class);
    }




    @Override
    public MaestraDetalleDto findMaestraDetalle(Long id) {
        List<MaestraDetalleDto> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_MAESTRA_DETALLE_ID);
            if (!Util.isEmpty(id)) objectParam.put("id", id);

            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(MaestraDetalleDto.class));
            if (!list.isEmpty()) return list.get(0);
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return new MaestraDetalleDto();
        }
        return null;
    }

    @Override
    public List<RespDifusionConvocatoria> listBases(Long idBase) {
        List<RespDifusionConvocatoria> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASES_MIGRAR_MONGO);

            if (!Util.isEmpty(idBase)) objectParam.put("idBase", idBase);

            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(RespDifusionConvocatoria.class));
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
        return list;
    }

    @Override
    public List<RespDifusionConocimientos> listBasesConocimientos(Long idPerfil) {
        List<RespDifusionConocimientos> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_CONOCIMIENTOS);
            if (!Util.isEmpty(idPerfil)) objectParam.put("idPerfil", idPerfil);

            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(RespDifusionConocimientos.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<RespDifusionExperiencia> listBasesExperiencia(Long idPerfil) {
        List<RespDifusionExperiencia> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_EXPERIENCIA);
            if (!Util.isEmpty(idPerfil)) objectParam.put("idPerfil", idPerfil);

            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(RespDifusionExperiencia.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<RespDifusionFormacionAcademica> listBasesFormacion(Long idPerfil) {
        List<RespDifusionFormacionAcademica> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_FORMACION_ACADEMICA);
            if (!Util.isEmpty(idPerfil)) objectParam.put("idPerfil", idPerfil);

            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(RespDifusionFormacionAcademica.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<RespCondicionAtipica> listBasesCondicion(Long idPerfil) {
        List<RespCondicionAtipica> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_CONDICIONES_ATIPICA);
            if (!Util.isEmpty(idPerfil)) objectParam.put("idPerfil", idPerfil);

            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(RespCondicionAtipica.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<Long> listIdCarreras(Long idPerfil) {
        List<Long> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(SQL_FIND_BASE_IDS_CARRERAS);
            list = this.jdbcTemplate.queryForList(sql.toString(), Long.class, idPerfil);
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<String> listFuncionesPuesto(Long idPerfil) {
        List<String> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(SQL_FIND_BASE_FUNCIONES_PUESTO_ACTIV_PRACTI);
            list = this.jdbcTemplate.queryForList(sql.toString(), String.class, idPerfil);
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<String> listHabilidades(Long idPerfil) {
        List<String> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(SQL_FIND_BASE_HABILIDADES);
            list = this.jdbcTemplate.queryForList(sql.toString(), String.class, idPerfil);
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<String> listRequisitosAdicionales(Long idPerfil) {
        List<String> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(SQL_FIND_BASE_REQUISITOS_ADICIONALES);
            list = this.jdbcTemplate.queryForList(sql.toString(), String.class, idPerfil);
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public String getCodigoConvocatoria(CorreoDTO baseDTO, int anio) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("anio", anio)
                .addValue("regimen", baseDTO.getRegimenId())
                .addValue("entidad", baseDTO.getEntidadId());
        return namedParameterJdbcTemplate.queryForObject(
                SQL_FIND_COD_CONVOCATORIA_2, namedParameters, String.class);
    }
}
