package pe.gob.servir.convocatoria.request.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class MaestraConocimientoDTO {

	private Long maeConocimientoId;
	
	@NotNull(message = Constantes.CAMPO + " tipoConocimientoId " + Constantes.ES_OBLIGATORIO)
	private Long tipoConocimientoId;
	
	@NotNull(message = Constantes.CAMPO + " categoriaConocimientoId " + Constantes.ES_OBLIGATORIO)
	private Long categoriaConocimientoId;
	
	@NotNull(message = Constantes.CAMPO + " descripcion" + Constantes.ES_OBLIGATORIO)
	@Size(max = 500, message = Constantes.CAMPO + " descripcion " + Constantes.ES_INVALIDO + ", máximo 500 ")
	private String descripcion;
	
	private Long entidadId;
	
	private String codigoTipoConocimiento;
	
	private String descripcionTipo;
	
	private String descripcionCategoria;
	
	private String estado;
}
