package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReporteBonificacionDetalle {
	
	private String descripcion;
	private String nivel;
	private String aplica;
	private String porcentaje;

}
