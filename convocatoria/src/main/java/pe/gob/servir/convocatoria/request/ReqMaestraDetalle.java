package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;


@Getter
@Setter
public class ReqMaestraDetalle {

	@NotNull(message = Constantes.CAMPO + " maeCabeceraId " + Constantes.ES_OBLIGATORIO)
	private Long maeCabeceraId;
	
	@NotNull(message = Constantes.CAMPO + " descripcion completa" + Constantes.ES_OBLIGATORIO)
	@Size(max = 250, message = Constantes.CAMPO + " nombreCompleto " + Constantes.ES_INVALIDO + ", máximo 250 ")
	private String nombreCompleto;
	
	@Size(max = 40, message = Constantes.CAMPO + " nombreCorto " + Constantes.ES_INVALIDO + ", máximo 40 ")
	private String nombreCorto;
	
	@Size(max = 15, message = Constantes.CAMPO + " sigla " + Constantes.ES_INVALIDO + ", máximo 15 ")
	private String sigla;
	
	@Size(max = 100, message = Constantes.CAMPO + " referencia " + Constantes.ES_INVALIDO + ", máximo 100 ")
	private String referencia;
	
	@NotNull(message = Constantes.CAMPO + " estado " + Constantes.ES_OBLIGATORIO)
	@Pattern(regexp = "1|0", message = Constantes.CAMPO + " estado " + Constantes.ES_INVALIDO + ", valores permitidos 1(ACTIVO), 0(INACTIVO)")
	private String estado;
}
