package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.BaseEvaluacionDetalleDTO;

import java.util.List;

@Getter
@Setter
public class ReqGuardarBaseEvaluacion {

    @NotNull(message = Constantes.CAMPO + " baseId " + Constantes.ES_OBLIGATORIO)
    private Long baseId;

    private String observacion;

    private List<BaseEvaluacionDetalleDTO> baseEvaluacionDetalleDTOList;

}
