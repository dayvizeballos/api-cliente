package pe.gob.servir.convocatoria.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.exception.NotFoundException;
import pe.gob.servir.convocatoria.feign.client.MaestraApiClient;
import pe.gob.servir.convocatoria.model.*;
import pe.gob.servir.convocatoria.repository.ComunicadoRepository;
import pe.gob.servir.convocatoria.repository.ConvocatoriaRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.PerfilRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqComunicado;
import pe.gob.servir.convocatoria.request.ReqEstadoComunicado;
import pe.gob.servir.convocatoria.request.dto.ApiUploadFile;
import pe.gob.servir.convocatoria.request.dto.ParametrosDTO;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.ComunicadoService;
import pe.gob.servir.convocatoria.util.DateUtil;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ComunicadoImpl implements ComunicadoService {

	@Autowired
	ConvocatoriaRepository convocatoriaRepository;

	@Autowired
	PerfilRepository perfilRepository;

	@Autowired
	MaestraDetalleRepository maestraDetalleRepository;

	@Value("${ruta.file.server}")
	public String fileServer;

	@Autowired
	MaestraApiClient maestraApiClient;

	@Autowired
	ComunicadoRepository comunicadoRepository;
	

	final String PREFIJO_NAME = "COMUN";
	public static final String DD_MM_YYYY = "dd/MM/yyyy";

	@Override
	@Transactional(transactionManager = "convocatoriaTransactionManager")
	public RespBase<RespComunicado> creaComunicado(ReqBase<ReqComunicado> request, MyJsonWebToken token) {
		if (!Util.isEmpty(request.getPayload().getComunicadoId())) {
			request.getPayload().setComunicadoId(null);
		}

		Comunicado comunicado = new Comunicado();
		if (request.getPayload() == null)
			throw new NotFoundException("el payload no puede ser null");

		Optional<Convocatoria> convocatoria = convocatoriaRepository.findById(request.getPayload().getConvocatoriaId());
		if (!convocatoria.isPresent())
			throw new NotFoundException("no existe la convocatoria con el id: ("
					+ request.getPayload().getConvocatoriaId() + ") en base de datos");

		convocatoria.ifPresent(comunicado::setConvocatoria);

		if (!Util.isEmpty(request.getPayload().getPerfilId())) {
			Optional<Perfil> perfil = perfilRepository.findById(request.getPayload().getPerfilId());
			perfil.ifPresent(comunicado::setPerfil);
		}

		Optional<MaestraDetalle> etapa = maestraDetalleRepository.findById(request.getPayload().getEtapaId());
		if (!etapa.isPresent())
			throw new NotFoundException(
					"no existe la etapa con el id: (" + request.getPayload().getEtapaId() + ") en base de datos");

		etapa.ifPresent(comunicado::setEtapa);

		Optional<MaestraDetalle> estado = maestraDetalleRepository.findById(request.getPayload().getEstadoId());
		if (!estado.isPresent())
			throw new NotFoundException(
					"no existe el estado con el id: (" + request.getPayload().getEstadoId() + ") en base de datos");

		Optional<MaestraDetalle> tipoComunicado = maestraDetalleRepository
				.findById(request.getPayload().getTipoComunicadoId());
		if (!tipoComunicado.isPresent())
			throw new NotFoundException("no existe el tipo comunicado con el id: ("
					+ request.getPayload().getTipoComunicadoId() + ") en base de datos");

		tipoComunicado.ifPresent(comunicado::setTipoComunicado);

		estado.ifPresent(comunicado::setEstado);
		comunicado.setCoordinadorId(request.getPayload().getCoordinadorId());
		comunicado.setGestorId(request.getPayload().getGestorId());
		comunicado.setNombreGestor(Util.isEmptyToString(request.getPayload().getNombreGestor()));
		comunicado.setNombreCoordinador(Util.isEmptyToString(request.getPayload().getNombreCoordinador()));
		comunicado.setComunicadoId(request.getPayload().getComunicadoId());
		comunicado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());

		comunicadoRepository.save(comunicado);

		comunicado = uploadComunicado(comunicado, request.getPayload().getPdfBase64());
		comunicado.setConPdf(Constantes.ACTIVO);
		comunicadoRepository.save(comunicado);
		return new RespBase<RespComunicado>().ok(settComunicadoDto(comunicado));

	}

	/**
	 * SETT VALORES DE COMUNICADO DTO
	 *
	 * @param comunicado
	 * @return
	 */
	private RespComunicado settComunicadoDto(Comunicado comunicado) {
		RespComunicado dto = new RespComunicado();
		dto.setConvocatoriaId(comunicado.getConvocatoria() == null ? null : comunicado.getConvocatoria().getConvocatoriaId());
		dto.setCoordinadorId(comunicado.getCoordinadorId());
		dto.setGestorId(comunicado.getGestorId());
		dto.setComunicadoId(comunicado.getComunicadoId());
		dto.setNombreEtapa(comunicado.getEtapa().getDescripcion());
		dto.setEtapaId(comunicado.getEtapa().getMaeDetalleId());
		dto.setTipoComunicadoNombre(comunicado.getTipoComunicado().getDescripcion());
		dto.setTipoComunicadoId(comunicado.getTipoComunicado().getMaeDetalleId());
		if (comunicado.getPerfil() != null)
			dto.setNombrePerfil(comunicado.getPerfil().getNombrePuesto());
		dto.setNombreGestor(comunicado.getNombreGestor());
		dto.setNombreCoordinador(comunicado.getNombreCoordinador());
		dto.setNombreEstado(comunicado.getEstado().getDescripcion());
		dto.setFechaPublicacion(Util.instantToString(comunicado.getFechaCreacion())); //CORREGIR
		dto.setEstadoId(comunicado.getEstado().getMaeDetalleId());
		dto.setFechaPublicacion(Util.instantToString(comunicado.getFechaCreacion()));
		dto.setEstadoRegistro(comunicado.getEstadoRegistro());
		dto.setUrl(comunicado.getUrl());
		return dto;
	}

	/**
	 * ENVIA UN FILE AL SERVIDOR / COMUNICADO SETT LA URL Y EL NOMBRE AL COMUNICADO
	 *
	 * @param comunicado
	 * @return
	 */
	private Comunicado uploadComunicado(Comunicado comunicado, String pdfBase64) {
		RespBase<ApiUploadFile> requestApiUploadFile = new RespBase<ApiUploadFile>();
		ApiUploadFile uploadFile = new ApiUploadFile();
		uploadFile.setExtension(Constantes.EXTENSION_PDF);
		uploadFile.setFileBase64(pdfBase64);
		String pathTmp = rutaFileServerEntidad();
		uploadFile.setFileName(PREFIJO_NAME + comunicado.getComunicadoId());
		String rutaFileServer = ParametrosUtil.datePathReplaceRepositoryAlfresco(pathTmp);
		rutaFileServer = rutaFileServer.replace(Constantes.PATH_ENTIDAD,
				comunicado.getConvocatoria().getBase().getEntidadId().toString());
		uploadFile.setPath(rutaFileServer);
		requestApiUploadFile.setPayload(uploadFile);
		RespBase<RespUploadFile> upload = maestraApiClient.uploadFile(requestApiUploadFile);
		comunicado.setUrl(upload.getPayload().getPathRelative());
		comunicado.setNombre(comunicado.getTipoComunicado().getDescripcion());
		return comunicado;
	}

	/**
	 * RUTAS_FILE / RUTA_FILE_SERVER_ENTIDAD
	 *
	 * @return
	 */

	String rutaFileServerEntidad() {
		RespBase<RespObtieneLista<ParametrosDTO>> resp = maestraApiClient
				.rutaFileServerEntidad(Constantes.PARAMETRO_FILE, Constantes.PAR_CODIGO_TEXTO);
		for (ParametrosDTO it : resp.getPayload().getItems()) {
			if (it.getCodigoTexto().equalsIgnoreCase(Constantes.PAR_CODIGO_TEXTO)) {
				return it.getValorTexto();
			}
		}
		return null;
	}



	/**
	 * FIND COMUNICADOS
	 *
	 * @param parametroMap
	 * @return
	 */
	@Override
	public RespBase<RespObtieneLista<RespComunicadoDTO>> findComunicado(Map<String, Object> parametroMap) {
		
		int page = 0;
		int size = 0;
		RespObtieneLista<RespComunicadoDTO> respPayload = new RespObtieneLista<>();
		page = (Integer) parametroMap.get("page"); 
		size = (Integer) parametroMap.get("size");
	
		
		 Pageable pageable = PageRequest.of(page, size);
		Page<Comunicado> comunicado =  comunicadoRepository.findAll((root,cq, cb)->{
			 Predicate p = cb.conjunction();
				Join<Comunicado, Convocatoria> convocatoriaJoin = root.join("convocatoria");
				Join<Comunicado, Perfil> perfilJoin = root.join("perfil", JoinType.LEFT);
				Join<Comunicado, MaestraDetalle> etapaJoin = root.join("etapa");
				Join<Comunicado, MaestraDetalle> estadoJoin = root.join("estado");
				Join<Comunicado, MaestraDetalle> tipoComunicadoJoin = root.join("tipoComunicado");

				if (!Util.isEmpty(parametroMap.get("entidad"))) {
					p = cb.and(p,cb.equal(convocatoriaJoin.get("entidadId"), parametroMap.get("entidad")));
				}
				if (!Util.isEmpty(parametroMap.get("convocatoria"))) {
					p = cb.and(p,cb.equal(convocatoriaJoin.get("convocatoriaId"), parametroMap.get("convocatoria")));
				}
				if (!Util.isEmpty(parametroMap.get("perfil"))) {
					p = cb.and(p, cb.equal(perfilJoin.get("perfilId"), parametroMap.get("perfil")));
				}
				if (!Util.isEmpty(parametroMap.get("etapa"))) {
					p = cb.and(p, cb.equal(etapaJoin.get("maeDetalleId"), parametroMap.get("etapa")));
				}

				if (!Util.isEmpty(parametroMap.get("fechaIni")) && !Util.isEmpty(parametroMap.get("fechaFin"))) {
					Date fechaIni = (Date) parametroMap.get("fechaIni");
					Date fechaFin = (Date) parametroMap.get("fechaFin");
					p = cb.and(p, cb.between(cb.function("to_char", String.class, root.get("fechaPublicacion"),
							cb.literal(DD_MM_YYYY)), DateUtil.fmtDt(fechaIni), DateUtil.fmtDt(fechaFin)));
				}

				if (!Util.isEmpty(parametroMap.get("estado"))) {
					p = cb.and(p, cb.equal(estadoJoin.get("maeDetalleId"), parametroMap.get("estado")));
				}
				if (!Util.isEmpty(parametroMap.get("tipoComunicado"))) {
					p = cb.and(p,cb.equal(tipoComunicadoJoin.get("maeDetalleId"), parametroMap.get("tipoComunicado")));
				}

				// Solo activos
				p = cb.and(p, cb.equal(root.get("estadoRegistro"), "1"));

				cq.orderBy(cb.desc(root.get("comunicadoId")));
		        return p;
				
		}, pageable);
		
		respPayload.setCount(comunicado.getContent().size());
        respPayload.setTotal(comunicado.getTotalElements());
		respPayload.setItems(this.setListComunicadoDto(comunicado.getContent()));

		return new RespBase<RespObtieneLista<RespComunicadoDTO>>().ok(respPayload);

	}

	/**
	 * SET DTO COMUNICADO
	 *
	 * @param lstComunicado
	 * @return
	 */
	private List<RespComunicadoDTO> setListComunicadoDto(List<Comunicado> lstComunicado) {
		return lstComunicado.stream()
				.filter(comunicado -> !comunicado.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO)).map(co -> {
					RespComunicadoDTO dto = new RespComunicadoDTO();
					dto.setComunicadoId(co.getComunicadoId());
					dto.setConvocatoriaId(co.getConvocatoria().getConvocatoriaId());
					dto.setCodigoConvocatoria(co.getConvocatoria().getCodigoConvocatoria());
					dto.setPerfilId(co.getPerfil() == null ? null : co.getPerfil().getPerfilId());
					dto.setNombrePerfil(co.getPerfil() == null ? null : co.getPerfil().getNombrePuesto());
					dto.setEtapa(co.getEtapa().getDescripcion());
					dto.setFechaPublicacion(DateUtil.fmtDt(co.getFechaPublicacion()));
					dto.setTipoComunicado(co.getTipoComunicado().getDescripcion());
					dto.setEstado(co.getEstado().getDescripcion());
					dto.setCoordinador(co.getNombreCoordinador());
					dto.setGestor(co.getNombreGestor());
					dto.setEstadoId(co.getEstado().getMaeDetalleId());
					dto.setCodProgEstado(co.getEstado().getCodProg());
					return dto;
				}).collect(Collectors.toList());
	}

	@Override
	public RespBase<Object> eliminarComunicado(MyJsonWebToken token, Long comunicadoId) {

		RespBase<Object> response = new RespBase<>();
		Optional<Comunicado> comunicadoFind = comunicadoRepository.findById(comunicadoId);
		if (comunicadoFind.isPresent()) {
			Comunicado comunicado = comunicadoFind.get();
			comunicado.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
					Instant.now());
			comunicadoRepository.save(comunicado);

			response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se eliminó el comunicado correctamente ");
		} else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el comunicadoId Ingresado");
		}
		return response;

	}

	@Override
	public RespBase<RespComunicado> actualizaComunicado(ReqBase<ReqComunicado> request, MyJsonWebToken token,
			Long comunicadoId) {
		Comunicado comunicado = new Comunicado();

		if (comunicadoId == null)
			throw new NotFoundException("el comunicadoId no puede ser null");

		if (request.getPayload() == null)
			throw new NotFoundException("el payload no puede ser null");

		Optional<Comunicado> comunicadoOptional = comunicadoRepository.findById(comunicadoId);
		if (!comunicadoOptional.isPresent())
			throw new NotFoundException("no existe el comunicado con el id: (" + comunicadoId + ") en base de datos");

		comunicado = comunicadoOptional.get();

		Optional<Convocatoria> convocatoria = convocatoriaRepository.findById(request.getPayload().getConvocatoriaId());
		if (!convocatoria.isPresent())
			throw new NotFoundException("no existe la convocatoria con el id: ("
					+ request.getPayload().getConvocatoriaId() + ") en base de datos");

		convocatoria.ifPresent(comunicado::setConvocatoria);

		if (!Util.isEmpty(request.getPayload().getPerfilId())) {
			Optional<Perfil> perfil = perfilRepository.findById(request.getPayload().getPerfilId());
			if (!perfil.isPresent())
				throw new NotFoundException("no existe el perfil con el id: ("
						+ request.getPayload().getPerfilId() + ") en base de datos");
			
			perfil.ifPresent(comunicado::setPerfil);
		}

		Optional<MaestraDetalle> etapa = maestraDetalleRepository.findById(request.getPayload().getEtapaId());
		if (!etapa.isPresent())
			throw new NotFoundException(
					"no existe la etapa con el id: (" + request.getPayload().getEtapaId() + ") en base de datos");

		etapa.ifPresent(comunicado::setEtapa);

		Optional<MaestraDetalle> estado = maestraDetalleRepository.findById(request.getPayload().getEstadoId());
		if (!estado.isPresent())
			throw new NotFoundException(
					"no existe el estado con el id: (" + request.getPayload().getEstadoId() + ") en base de datos");

		estado.ifPresent(comunicado::setEstado);

		Optional<MaestraDetalle> tipoComunicado = maestraDetalleRepository
				.findById(request.getPayload().getTipoComunicadoId());
		if (!tipoComunicado.isPresent())
			throw new NotFoundException("no existe el tipo comunicado con el id: ("
					+ request.getPayload().getTipoComunicadoId() + ") en base de datos");

		tipoComunicado.ifPresent(comunicado::setTipoComunicado);

		comunicado.setCoordinadorId(request.getPayload().getCoordinadorId());
		comunicado.setGestorId(request.getPayload().getGestorId());
		comunicado.setNombreGestor(Util.isEmptyToString(request.getPayload().getNombreGestor()));
		comunicado.setNombreCoordinador(Util.isEmptyToString(request.getPayload().getNombreCoordinador()));
		// comunicado.setComunicadoId(request.getPayload().getComunicadoId());
		comunicado.setCampoSegUpd(comunicado.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());

		comunicadoRepository.save(comunicado);

		comunicado = uploadComunicado(comunicado, request.getPayload().getPdfBase64());
		comunicado.setConPdf(Constantes.ACTIVO);
		comunicadoRepository.save(comunicado);
		return new RespBase<RespComunicado>().ok(settComunicadoDto(comunicado));
	}

	@Override
	public RespBase<RespComunicado> obtenerComunicado(Long comunicadoId) {

		if (Util.isEmpty(comunicadoId))
			throw new NotFoundException("el comunicadoId no puede ser null");

		Comunicado comunicado = new Comunicado();
		Optional<Comunicado> comunicadoOptional = comunicadoRepository.findById(comunicadoId);
		if (!comunicadoOptional.isPresent()) {
			throw new NotFoundException("no existe el comunicado con el id: (" + comunicadoId + ") en base de datos");
		} else {
			comunicado = comunicadoOptional.get();
		}

		RespComunicado payload = this.obtenerComunicadoDTO(comunicado);

		return new RespBase<RespComunicado>().ok(payload);

	}

	private RespComunicado obtenerComunicadoDTO(Comunicado c) {
		RespComunicado rp = new RespComunicado();
		rp.setComunicadoId(c.getComunicadoId());
		rp.setConvocatoriaId(c.getConvocatoria().getConvocatoriaId());
		rp.setPerfilId(c.getPerfil() == null ? null : c.getPerfil().getPerfilId());
		rp.setNombrePerfil(c.getPerfil() == null ? null : c.getPerfil().getNombrePuesto());
		rp.setEtapaId(c.getEtapa() == null ? null : c.getEtapa().getMaeDetalleId());
		rp.setNombreEtapa(c.getEtapa() == null ? null : c.getEtapa().getDescripcion());
		rp.setEstadoId(c.getEstado() == null ? null : c.getEstado().getMaeDetalleId());
		rp.setNombreEstado(c.getEstado() == null ?  null : c.getEstado().getDescripcion());
		rp.setTipoComunicadoId(c.getTipoComunicado() == null ? null : c.getTipoComunicado().getMaeDetalleId());
		rp.setTipoComunicadoNombre(c.getTipoComunicado() == null ? null : c.getTipoComunicado().getDescripcion());
		rp.setGestorId(c.getGestorId() == null ? null : c.getGestorId());
		rp.setNombreGestor(c.getNombreGestor() == null ? null : c.getNombreGestor());
		rp.setCoordinadorId(c.getCoordinadorId() == null ? null : c.getCoordinadorId());
		rp.setNombreCoordinador(c.getNombreCoordinador() == null ? null : c.getNombreCoordinador());
		rp.setUrl(c.getUrl() == null ? null : c.getUrl());
		rp.setNombreArchivo(c.getNombre() == null ? null : c.getNombre());
		rp.setObservacion(c.getObservacion() == null ? null : c.getObservacion());
		rp.setFechaPublicacion(c.getFechaPublicacion() == null ? null :DateUtil.fmtDt(c.getFechaPublicacion()));
		return rp;

	}

	@Override
	public RespBase<RespComunicado> actualizaEstadoComunicado(ReqBase<ReqEstadoComunicado> request,
			MyJsonWebToken token, Long comunicadoId) {
		
		RespBase<Object> response = new RespBase<>();
		
		Comunicado comunicado = new Comunicado();
		Convocatoria convocatoria = new Convocatoria();
		List<MaestraDetalle>  lstDetEstComunicado = null;
		List<MaestraDetalle>  lstDetEstConvocatoria = null;
		String fechaPublicacion = null;

		if (comunicadoId == null)
			throw new NotFoundException("el comunicadoId no puede ser null");

		if (request.getPayload() == null)
			throw new NotFoundException("el payload no puede ser null");

		Optional<Comunicado> comunicadoOptional = comunicadoRepository.findById(comunicadoId);
		if (!comunicadoOptional.isPresent())
			throw new NotFoundException("no existe el comunicado con el id: (" + comunicadoId + ") en base de datos");

		comunicado = comunicadoOptional.get();
		
		lstDetEstComunicado = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_EST_COMUNICADO);
		Map<Long, String> mapDetalleEstComu = lstDetEstComunicado.stream().filter(Objects::nonNull)
                 .collect(Collectors.toMap( MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));
		
		 if(!Util.isEmpty(request.getPayload().getCoordinadorId()))
			 comunicado.setCoordinadorId(request.getPayload().getCoordinadorId());
		 	 comunicado.setNombreCoordinador(request.getPayload().getNombreCoordinador());	
		 if(!Util.isEmpty(request.getPayload().getGestorId()))
			 comunicado.setGestorId(request.getPayload().getGestorId());
		 	 comunicado.setNombreGestor(request.getPayload().getNombreGestor());
		 if(!Util.isEmpty(request.getPayload().getEstadoId())) {
			 String  codigoEstado =null;
			 codigoEstado = mapDetalleEstComu.get(request.getPayload().getEstadoId());
			 if(codigoEstado.equals(Constantes.COD_EST_COMU_PUBLICADO)) {
				 fechaPublicacion = Util.instantToString(Instant.now());
			 		Date fechaDate = null;
					SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
					fechaDate = DateUtil.parse(formato, fechaPublicacion);
					comunicado.setFechaPublicacion(fechaDate);
			 }
		 }
		 if(!Util.isEmpty(request.getPayload().getObservacion()))
			 comunicado.setObservacion(request.getPayload().getObservacion());

		 
		 Optional<MaestraDetalle> maeDetalleEstado = maestraDetalleRepository.findById(request.getPayload().getEstadoId());
		 if (!maeDetalleEstado.isPresent())
				throw new NotFoundException("no existe la maestra detalle con el id: (" + request.getPayload().getEstadoId() + ") en base de datos");

		comunicado.setEstado(maeDetalleEstado.get());
		comunicado.setCampoSegUpd(comunicado.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
		comunicadoRepository.save(comunicado);
		
		//logica para actualizar contrato
		if(!Util.isEmpty(request.getPayload().getEstadoConvocatoriaId())) {
			lstDetEstConvocatoria = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_EST_CONVOCATORIA);
			Map<Long, String> mapDetalleEstConvo = lstDetEstConvocatoria.stream().filter(Objects::nonNull)
	                 .collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));
			
			Long estadoConvocatoriaId = request.getPayload().getEstadoConvocatoriaId();
			
			if(mapDetalleEstConvo.get(estadoConvocatoriaId).equals(Constantes.COD_ESTADO_DESIERTO)
					|| (mapDetalleEstConvo.get(estadoConvocatoriaId).equals(Constantes.COD_ESTADO_DESIERTO))){
			Optional<MaestraDetalle> maeDetalleEstadoConvo = maestraDetalleRepository.findById(estadoConvocatoriaId);
			if (!maeDetalleEstadoConvo.isPresent())
					throw new NotFoundException("no existe la maestra detalle con el id: (" + estadoConvocatoriaId + ") en base de datos");

			convocatoria = comunicado.getConvocatoria();
			convocatoria.setEstadoConvocatoria(maeDetalleEstadoConvo.get());
			convocatoria.setCampoSegUpd(convocatoria.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
			convocatoriaRepository.save(convocatoria);
			
			}
		}
		return new RespBase<RespComunicado>().ok(obtenerComunicadoDTO(comunicado));
	}

	@Override
	public RespBase<RespObtieneLista<RutaComunicadoDTO>> getRutaComunicado( Long perfilId , Long baseId) {
		List<RutaComunicadoDTO> ls;
		RespObtieneLista<RutaComunicadoDTO> respPayload = new RespObtieneLista<>();
		ls = comunicadoRepository.findComunicados(perfilId, baseId).stream().map(comunicado -> {
			RutaComunicadoDTO dto = new RutaComunicadoDTO();
			dto.setRuta(comunicado.getUrl());
			dto.setFecha(comunicado.getFechaPublicacion());
			return dto;
		}).collect(Collectors.toList());
		/*Base base = new Base();
		base.setBaseId(baseId);
		Optional<Convocatoria> convocatoria = convocatoriaRepository.findByBase(base);
		convocatoria.ifPresent(value -> ls.addAll(comunicadoRepository.findByConvocatoria(value).stream()
				.filter(val -> val.getEstadoRegistro().equalsIgnoreCase(Constantes.ACTIVO))
				.map(comunicado -> {
					RutaComunicadoDTO dto = new RutaComunicadoDTO();
					dto.setRuta(comunicado.getUrl());
					dto.setFecha(comunicado.getFechaPublicacion());
					return dto;
				}).collect(Collectors.toList())));*/

		respPayload.setItems(ls.stream().sorted(Comparator.comparing(RutaComunicadoDTO::getFecha).reversed()).collect(Collectors.toList()));

		return new RespBase<RespObtieneLista<RutaComunicadoDTO>>().ok(respPayload);
	}
}
