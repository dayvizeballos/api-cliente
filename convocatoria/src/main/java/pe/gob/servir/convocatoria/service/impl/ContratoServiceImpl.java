package pe.gob.servir.convocatoria.service.impl;

import java.awt.Desktop;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Node;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.exception.NotFoundException;
import pe.gob.servir.convocatoria.feign.client.SeleccionClient;
import pe.gob.servir.convocatoria.model.*;

import pe.gob.servir.convocatoria.feign.client.MaestraApiClient;
import pe.gob.servir.convocatoria.repository.BaseRepository;
import pe.gob.servir.convocatoria.repository.ContratoRepository;
import pe.gob.servir.convocatoria.repository.ConvocatoriaRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.TipoContratoRepository;
import pe.gob.servir.convocatoria.request.*;
import pe.gob.servir.convocatoria.request.dto.ApiUploadFile;
import pe.gob.servir.convocatoria.request.dto.ContratoDTO;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.request.dto.ParametrosDTO;
import pe.gob.servir.convocatoria.request.dto.TipoContratoDTO;
import pe.gob.servir.convocatoria.response.dto.ReporteConvocatoriaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.ContratoService;
import pe.gob.servir.convocatoria.service.ConvocatoriaService;
import pe.gob.servir.convocatoria.service.GeneraDocumentoService;
import pe.gob.servir.convocatoria.util.DateUtil;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;

@Service
public class ContratoServiceImpl implements ContratoService {

	private static final Logger logger = LoggerFactory.getLogger(ContratoServiceImpl.class);
	
    @Autowired
    ContratoRepository contratoRepository;

    @Autowired
    MaestraDetalleRepository maestraDetalleRepository;
    
    @Autowired
    BaseRepository baseRepository;
    
    @Autowired
    ConvocatoriaRepository convocatoriaRepository;

    @Autowired
    ConvocatoriaService convocatoriaService;

    @Value("${ruta.file.server}")
    public String fileServer;

    @Autowired
    MaestraApiClient maestraApiClient;

    @Autowired
    SeleccionClient seleccionClient;

    @Autowired
    TipoContratoRepository tipoContratoRepository;
    
    @Autowired
    GeneraDocumentoService generaDocumento;
    
    public static final String DD_MM_YYYY = "dd/MM/yyyy";
    
    public static final String BOOKMARK_START_TAG = "bookmarkStart";
    public static final String BOOKMARK_END_TAG = "bookmarkEnd";

    /**
     * REGISTRO DE POSTULANTES EN CONTRATO
     *
     * @param request
     * @param token
     * @return
     */
    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
    public RespBase<Object> registrarContrato(ReqBase<ReqGeneraContrato> request, MyJsonWebToken token) {

        RespBase<Object> response = new RespBase<>();
        List<MaestraDetalle> lsTipoTrabajo = new ArrayList<>();
        List<MaestraDetalle> lsEstadoProceso = new ArrayList<>();
        List<Contrato> ls = new ArrayList<>();

        //Obtener el id del tipo de contrato
        lsTipoTrabajo = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_TIPO_TRABAJO);
        Map<String, Long> mapTipoTrabajo = lsTipoTrabajo.stream().filter(Objects::nonNull)
                .collect(Collectors.toMap(MaestraDetalle::getCodProg, MaestraDetalle::getMaeDetalleId));

        lsEstadoProceso = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_EST_CONTRATO);
        Map<String, Long> mapEstado = lsEstadoProceso.stream().filter(Objects::nonNull)
                .collect(Collectors.toMap(MaestraDetalle::getCodProg, MaestraDetalle::getMaeDetalleId));

        if (!request.getPayload().getPostulantes().isEmpty()) {

            ls = request.getPayload().getPostulantes().stream()
                    .map(dto -> {

                        RespBase<RespToContratoConvenio> py = convocatoriaService.
                                findDatosToContratosToConvenios(dto.getBaseId(), dto.getPostulanteId(), dto.getPerfilId(), dto.getEntidadId());
                        if (Util.isEmpty(py.getPayload())) {
                            throw new NotFoundException("no existe informacion a procesar");
                        }

                        RespToContratoConvenio cont = py.getPayload();
                        Contrato c = null;
                        Base base = null;
                        c = this.setContrato(cont);
                        
                        if(!Util.isEmpty(request.getPayload().getTipoTrabajo())) {
                        	Optional<MaestraDetalle> tipoTrabajo = maestraDetalleRepository.findById(mapTipoTrabajo.get(request.getPayload().getTipoTrabajo()));
                        	tipoTrabajo.ifPresent(c::setTipoTrabajoId);// CONTRATO 1 CONVENIO 2
                        }
                        
                        if (!Util.isEmpty(mapEstado.get(Constantes.COD_ESTADO_PENDIENTE))) {
                            Optional<MaestraDetalle> estado = maestraDetalleRepository.findById(mapEstado.get(Constantes.COD_ESTADO_PENDIENTE));
                            estado.ifPresent(c::setEstado);
                        }
                        if(!Util.isEmpty(request.getPayload().getTipoContrato())) {
                        	Optional<TipoContrato> tipoContrato = tipoContratoRepository.findById(request.getPayload().getTipoContrato());
                        	tipoContrato.ifPresent(c::setTipoContratoId);
                        }
                        if(!Util.isEmpty(cont.getBaseId())) {
                        	Optional<Base> baseF = baseRepository.findById(cont.getBaseId());
                        	if(baseF.isPresent()) {
                        		base = baseF.get();
                        	}else {
                        		 throw new NotFoundException("no existe informacion de la base");
                        	}
                        }
                        
                        if(!Util.isEmpty(base.getRegimenId())) {
                        	Optional<MaestraDetalle> regimen = maestraDetalleRepository.findById(base.getRegimenId());
                        	regimen.ifPresent(c::setRegimenId);
                        }
                        
                        if(!Util.isEmpty(cont.getBaseId())) {
                        	Optional<Convocatoria> convocatoria = convocatoriaRepository.findByBase(base);
                        	convocatoria.ifPresent(c::setConvocatoria);
                        }
                        c.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        return c;
                    }).collect(Collectors.toList());


            contratoRepository.saveAll(ls);

            /**
             *  QUQUE REPORTE ETAPA 7
             */
            if (!ls.isEmpty()){
                if (ls.get(0).getContratoId()!= null){
                   sendQueueEtapa5(ls.get(0).getConvocatoria() , ls.size());
                }
            }


            ls.forEach(contrato -> {
                RequestFlagContrato requestFlagContrato = new RequestFlagContrato();
                requestFlagContrato.setFlagContrato(1);
                requestFlagContrato.setPostulanteSelId(contrato.getPostulanteSelId());

                ReqBase<RequestFlagContrato> payload = new ReqBase<>();
                payload.setPayload(requestFlagContrato);
                RespBase<RequestFlagContrato> resp = seleccionClient
                        .updateFlagContratado(payload);
            });


            response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se registraron los contratos correctamente ");
        } else {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe informacion");
        }

        return response;
    }

    /**
     * QUEEUE REPORTE ETAPA 7
     * @param convocatoria
     * @param size
     */
    private void sendQueueEtapa5(Convocatoria convocatoria , Integer size) {
        try {
            ReporteConvocatoriaDTO dto = new ReporteConvocatoriaDTO();
            dto.setConvocatoriaId(convocatoria.getConvocatoriaId());
            dto.setBaseId(convocatoria.getBase().getBaseId());
            dto.setNroPostContratados(size);
            dto.setEtapaQueue(Constantes.ETAPA_REG_CONTRATO);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }

    }

    /**
     * SET CONTRATO
     *
     * @param
     * @return
     */
    private Contrato setContrato(RespToContratoConvenio cont) {
        Contrato c = new Contrato();
        c.setNroResolucion(cont.getNroResolucion() == null ? null : cont.getNroResolucion().trim());
        c.setOrgUnOrganica(cont.getOrgUnOrganica() == null ? null : cont.getOrgUnOrganica().trim());
        //c.setOrganoTitulo(cont.getNombreOrgano() == null ? null : cont.getNombreOrgano().trim()); REVISAR
        c.setNombreOrgano(cont.getNombreOrgano() == null ? null : cont.getNombreOrgano().trim());
        c.setNombreUniOrganica(cont.getNombreUniOrganica() == null ? null : cont.getNombreUniOrganica());
        c.setNombrePuesto(cont.getNombrePuesto() == null ? null : cont.getNombrePuesto().trim());
        c.setFechaSubscripcion(cont.getFechaSubscripcion() == null ? null : cont.getFechaSubscripcion());
        c.setFechaVinculacion(cont.getFechaVinculacion() == null ? null : cont.getFechaVinculacion());
        c.setEntidadId(cont.getEntidadId() == null ? null : cont.getEntidadId());
        c.setRazonSocial(cont.getRazonSocial() == null ? null : cont.getRazonSocial().trim());
        c.setJornalaboral(cont.getJornalaboral() == null ? null : cont.getJornalaboral().trim());
        c.setLugarPrestacion(cont.getLugarPrestacion() == null ? null : cont.getLugarPrestacion().trim());
        c.setRuc(cont.getRuc() == null ? null : cont.getRuc().trim());
        c.setSede(cont.getSede() == null ? null : cont.getSede().trim());
        c.setSiglas(cont.getSiglas() == null ? null : cont.getSiglas().trim());
        c.setSubNivelcat(cont.getSubNivelcat() == null ? null : cont.getSubNivelcat().trim());
        c.setDireccion(cont.getDireccion() == null ? null : cont.getDireccion());
        c.setRol(cont.getRol() == null ? null : cont.getRol().trim());
        c.setResponsableOrh(cont.getResponsableOrh() == null ? null : cont.getResponsableOrh().trim());
        c.setNroDocResponsable(cont.getNroDocResponsable() == null ? null : cont.getNroDocResponsable().trim());
        c.setTipoDocResponsable(cont.getTipoDocResponsable() == null ? null : cont.getTipoDocResponsable());
        c.setArticulo(cont.getArticulo() == null ? null : cont.getArticulo().trim());
        c.setNroNorma(cont.getNroNorma() == null ? null : cont.getNroNorma());
        c.setNroPosPueMeri(cont.getNroPosPueMeri() == null ? null : cont.getNroPosPueMeri());
        c.setBaseId(cont.getBaseId() == null ? null : cont.getBaseId());
        c.setNombres(cont.getNombres() == null ? null : cont.getNombres().trim());
        c.setNormaAproProy(cont.getNormaAproProy() == null ? null : cont.getNormaAproProy().trim());
        c.setNroConPubMeritos(cont.getNroConPubMeritos() == null ? null : cont.getNroConPubMeritos().trim());
        c.setNroConPub(cont.getNroConPub() == null ? null : cont.getNroConPub().trim());
        c.setNroInforme(cont.getNroInforme() == null ? null : cont.getNroInforme().trim());
        c.setApellidos(cont.getApellidos() == null ? null : cont.getApellidos().trim());
        c.setPostulanteId(cont.getPostulanteId() == null ? null : cont.getPostulanteId());
        c.setDni(cont.getDni() == null ? null : cont.getDni().trim());
        c.setTipoDoc(cont.getTipoDoc() == null ? null : cont.getTipoDoc());
        c.setNacionalidad(cont.getNacionalidad() == null ? null : cont.getNacionalidad().trim());
        c.setNivelCategoria(cont.getNivelCategoria() == null ? null : cont.getNivelCategoria().trim());
        c.setFamilia(cont.getFamilia() == null ? null : cont.getFamilia().trim());
        c.setDomicilio(cont.getDomicilio() == null ? null : cont.getDomicilio().trim());
        c.setFechaNacimiento(cont.getFechaNacimiento() == null ? null : cont.getFechaNacimiento());
        c.setFecha(cont.getFecha() == null ? null : cont.getFecha());//REVISAR
        c.setAreaLabores(cont.getAreaLabores() == null ? null : cont.getAreaLabores().trim());
        c.setCentroEstudios(cont.getCentroEstudios() == null ? null : cont.getCentroEstudios().trim());
        c.setDireccionCentroEstudios(cont.getDireccionCentroEstudios() == null ? null : cont.getDireccionCentroEstudios().trim());
        c.setCiclo(cont.getCiclo() == null ? null : cont.getCiclo().trim());
        c.setPeriodoPrueba(cont.getPeriodoPrueba() == null ? null : cont.getPeriodoPrueba().trim());
        c.setCompeEconomica(cont.getCompeEconomica() == null ? null : cont.getCompeEconomica());
        c.setPuestoAutoRepresentativa(cont.getPuestoAutoRepresentativa() == null ? null : cont.getPuestoAutoRepresentativa().trim());
        //c.setTipoTrabajoId(mapTipoTrabajo.get(request.getPayload().getTipoTrabajo())); // CONTRATO 1 CONVENIO 2
        c.setPerfilId(cont.getPerfilId() == null ? null : cont.getPerfilId());
        c.setPosCuadPuesto(cont.getPosCuadPuesto() == null ? null : cont.getPosCuadPuesto());
        c.setFamiliaId(cont.getFamiliaId() == null ? null : cont.getFamiliaId());
        c.setRolId(cont.getRolId() == null ? null : cont.getRolId());
        c.setNivelCategoriaId(cont.getNivelCategoriaId() == null ? null : cont.getNivelCategoriaId());
        c.setOrganoId(cont.getOrganoId() == null ? null : cont.getOrganoId());
        c.setUniOrganicaId(cont.getUnidadOrganicaId() == null ? null : cont.getUnidadOrganicaId());
        c.setPeriodoConvenio(cont.getPeriodoConvenio() == null ? null : cont.getPeriodoConvenio().trim());
        c.setPostulanteSelId(cont.getPostulanteSelId() == null ? null : cont.getPostulanteSelId());
        c.setBasePerfilId(cont.getBasePerfilId() == null ? null : cont.getBasePerfilId());
        //c.setEstado(estadoProceso);
        return c;
    }

    private Contrato setContratoAct(Contrato c, ReqContrato req) {
    	c.setNroResolucion(req.getNroResolucion() == null ? null : req.getNroResolucion().trim());
    	c.setFechaResolucion(req.getFechaResolucion() == null ? null : req.getFechaResolucion());
    	c.setNroInforme(req.getNroInforme() == null ? null : req.getNroInforme().trim());
    	c.setFechaVinculacion(req.getFechaVinculacion() == null ? null : req.getFechaVinculacion());
    	c.setPeriodoPrueba(req.getPeriodoPrueba() == null ? null : req.getPeriodoPrueba().trim());
    	c.setResolResponOrh(req.getResolResponOrh() == null ? null : req.getResolResponOrh().trim());
        c.setNroNorma(req.getNroNorma() == null ? null : req.getNroNorma());
        return c;
    }

    
    private Contrato setConvenioAct(Contrato c, ReqConvenio req) {
    	c.setDireccionLabores(req.getDireccionLabores() == null ? null : req.getDireccionLabores());
    	c.setFechaIniPractica(req.getFechaIniPractica() == null ? null : req.getFechaIniPractica());
    	c.setFechaFinPractica(req.getFechaFinPractica() == null ? null : req.getFechaFinPractica());
    	c.setHoraIniPractica(req.getFechaIniPractica() == null ? null : req.getHoraIniPractica());
    	c.setHoraFinPractica(req.getHoraFinPractica() == null ? null : req.getHoraFinPractica());
    	
    	c.setPuestoResponsableOrh(req.getPuestoResponsableOrh() == null ? null : req.getPuestoResponsableOrh());
    	c.setResponsableOrh(req.getResponsableOrh() == null ? null: req.getResponsableOrh());
    	c.setTipoDocResponsable(req.getTipoDocResponsable() == null ? null : req.getTipoDocResponsable());
    	c.setNroDocResponsable(req.getNroDocResponsable() == null ? null : req.getNroDocResponsable());
    	c.setPuestoRepreUni(req.getPuestoRepreUni() == null ? null : req.getPuestoRepreUni());
    	c.setNombRepreUni(req.getNombRepreUni() == null ? null : req.getNombRepreUni());
    	c.setTipoDocRepreUni(req.getTipoDocRepreUni() == null ? null : req.getTipoDocRepreUni());
    	c.setNroDocRepreUni(req.getNroDocRepreUni() == null ? null : req.getNroDocRepreUni());
    	c.setDireccionCentroEstudios(req.getDireccionCentroEstudios() == null ? null : req.getDireccionCentroEstudios());
    	c.setRucCentroEstudios(req.getRucCentroEstudios() == null ? null : req.getRucCentroEstudios());
        return c;
    }    
   
    
    /**
     * SETT DTO DE PRUEBA
     *
     * @param
     * @return
     */
    private ContratoDTO setContratoT() {
        //LocalDate date = LocalDate.now();
        Date date = new Date();
        ContratoDTO c = new ContratoDTO();
        c.setNroResolucion("Nro 1XXX");
        c.setOrgUnOrganica("ORH");
        //c.setNumero(); //pendiente
        c.setOrganoTitulo("Direccion General");
        c.setFechaSubscripcion(date);
        c.setFechaVinculacion(date);
        c.setEntidadId(41L);
        c.setRazonSocial("PRUEBA SAC");
        c.setJornalaboral("8 horas");
        c.setLugarPrestacion("Lima");
        c.setRuc("123456789");
        c.setSede("San Borja 123");
        c.setSiglas("SIGLA");
        c.setSubNivelcat("CA2-3");
        c.setDireccion("Av Jose Olaya123");
        c.setRol("Politico estratigo");
        c.setResponsableOrh("Juan Perez");
        c.setNroDocResponsable("12345678");
        c.setArticulo("Articulo");
        c.setNroNorma("Normal 123");
        c.setNroPosPueMeri(5L);
        c.setBaseId(118L);
        c.setNombres("JOSE ROCA");
        c.setNormaAproProy("CARLOS ROSS");
        c.setNroConPubMeritos("1");
        c.setNroConPub("1");
        c.setNroInforme("NRO 1");
        c.setApellidos("GALVEZ ROMERO");
        c.setPostulanteId(118L);
        c.setDni("12345678");
        c.setNacionalidad("Peruano");
        c.setNivelCategoria("Analista");
        c.setFamilia("Gestion Institucional");
        c.setDomicilio("AV peru 123");
        c.setFechaNacimiento(date);
        c.setFecha("");
        c.setAreaLabores("Area 123");
        c.setCentroEstudios("Juan bautista 4020");
        c.setCiclo("ciclo");
        c.setPeriodoPrueba("6 semanas");
        c.setCompeEconomica(15400.12);
        c.setPuestoAutoRepresentativa("Puesto nuevo");
        c.setTipoTrabajoId(187L); // CONTRATO 1 CONVENIO 2
        c.setTipoServicioId(1L);
        c.setTipoTramiteId(2L);
        c.setPerfilId(8L);
        c.setBaseId(108L);
        c.setPosCuadPuesto(10L);

        return c;
    }

    /**
     * ACTUALIZA CONTRATO POR ID / ESTADO A CREADO
     *
     * @param token
     * @param contratoId
     * @return
     */
    
    @Override
    public RespBase<RespToContratoConvenio> actualizaContrato(ReqBase<ReqContrato> request, MyJsonWebToken token,
                                                              Long contratoId) {

        LocalDate date = LocalDate.now();
        if (contratoId == null)
            throw new NotFoundException("el contratoId no puede ser null");

        if (request.getPayload() == null)
            throw new NotFoundException("el payload no puede ser null");

        Optional<Contrato> contratoOpt = contratoRepository.findById(contratoId);
        if (!contratoOpt.isPresent())
            throw new NotFoundException("no existe el contrato con el id: (" + contratoId + ") en base de datos");


        Contrato contrato = contratoOpt.get();
        contrato = this.setContratoAct(contrato, request.getPayload());

        /*if (!Util.isEmpty(request.getPayload().getTipoServicioId())) {
            Optional<MaestraDetalle> tipoServicio = maestraDetalleRepository.findById(request.getPayload().getTipoServicioId());
            tipoServicio.ifPresent(contrato::setTipoServicioId);
        }
        if (!Util.isEmpty(request.getPayload().getTipoTramiteId())) {
            Optional<MaestraDetalle> tipoTramite = maestraDetalleRepository.findById(request.getPayload().getTipoTramiteId());
            tipoTramite.ifPresent(contrato::setTipoTramiteId);
        }
        */
        if (!Util.isEmpty(request.getPayload().getEstadoId())) {
            Optional<MaestraDetalle> estado = maestraDetalleRepository.findById(request.getPayload().getEstadoId());
            estado.ifPresent(contrato::setEstado);
        }

        contrato.setAnio(date.getYear());
        contrato.setNumero(this.obtenerCorrelativoContrato(
        		contrato.getTipoTrabajoId(), date.getYear(), contrato.getEntidadId()));
        contrato.setCampoSegUpd(contrato.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
        contratoRepository.save(contrato);

        RespBase<RespToContratoConvenio> resPayload = new RespBase<>();
        resPayload.setPayload(this.settContratoDto(contrato));
        return resPayload;
    }


    private int obtenerCorrelativoContrato(MaestraDetalle tipoTrabajoId, int anio, Long entidadId) {
        int numero = contratoRepository.findMaxContrato(tipoTrabajoId, anio, entidadId);
        return numero;
    }

    /**
     * OBTENER CONTRATO/CONVENIOS POR ID
     *
     * @param contratoId
     * @return
     */
    @Override
    public RespBase<RespToContratoConvenio> getContratoConvenio(Long contratoId) {
        Optional<Contrato> contratoOptional = contratoRepository.findById(contratoId);
        if (!contratoOptional.isPresent())
            throw new NotFoundException("no existe el contrato con el id: (" + contratoId + ") en base de datos");

        Contrato contrato = contratoOptional.get();
        RespBase<RespToContratoConvenio> resPayload = new RespBase<>();
        resPayload.setPayload(settContratoDto(contrato));
        return resPayload;
    }

    /**
     * FIND CONTRATO/CONVENIOS POR FILTRO
     *
     * @param regimen
     * @param perfil
     * @param fechaIni
     * @param fechaFin
     * @param estado
     * @param page
     * @param size
     * @return
     */
    @Override
    public RespBase<RespObtieneLista<RespContratoConvenio>> findContratoaConveniosFiltro(Long tipoPractica , Long tipo , Long entidad , Long regimen, Long perfil, Date fechaIni,
                                                                                         Date fechaFin, Long estado, int page, int size) {

        RespObtieneLista<RespContratoConvenio> respPayload = new RespObtieneLista<>();
        Pageable pageable = PageRequest.of(page, size);
        Page<Contrato> contrato = contratoRepository.findAll((root, cq, cb) -> {
            Join<Contrato, MaestraDetalle> estadoJ = root.join("estado");
            Join<Contrato, MaestraDetalle> regimenJ = root.join("regimenId");
            Join<Contrato, MaestraDetalle> tipoTrabajoJ = root.join("tipoTrabajoId");

            Predicate p = cb.conjunction();

            if (!Util.isEmpty(entidad)) {
                p = cb.and(p, cb.equal(root.get("entidadId"), entidad));
            }

            if (!Util.isEmpty(tipo)){
                p = cb.and(p, cb.equal(tipoTrabajoJ.get("maeDetalleId"), tipo));
            }

            if (!Util.isEmpty(tipoPractica)){
                p = cb.and(p, cb.equal(root.get("tipoPracticaId"), tipoPractica));
            }

            if (!Util.isEmpty(regimen)) {
                p = cb.and(p, cb.equal(regimenJ.get("maeDetalleId"), regimen));
            }

            if (!Util.isEmpty(perfil)) {
                p = cb.and(p, cb.equal(root.get("perfilId"), perfil));
            }

            if (!Util.isEmpty(estado)) {
                p = cb.and(p, cb.equal(estadoJ.get("maeDetalleId"), estado));
            }

            if (!Util.isEmpty(fechaIni) && !Util.isEmpty(fechaFin)) {
                p = cb.and(p, cb.between(cb.function("to_char", String.class, root.get("fechaSubscripcion"),
                        cb.literal(DD_MM_YYYY)), DateUtil.fmtDt(fechaIni), DateUtil.fmtDt(fechaFin)));
            }
            p = cb.and(p, cb.equal(root.get("estadoRegistro"), Constantes.ACTIVO));
            cq.orderBy(cb.desc(root.get("contratoId")));
            return p;
        }, pageable);
        respPayload.setCount(contrato.getContent().size());
        respPayload.setTotal(contrato.getTotalElements());
        respPayload.setItems(settRespContratoConvenioDto(contrato.getContent()));
        return new RespBase<RespObtieneLista<RespContratoConvenio>>().ok(respPayload);
    }

    /**
     * SETT RESPCONTRATOCONVENIO DTO
     * BANDEJA FILTROS
     *
     * @param ls
     * @return
     */
    private List<RespContratoConvenio> settRespContratoConvenioDto(List<Contrato> ls) {
        return ls.stream()
                .map(contrato -> {
                    RespContratoConvenio dto = new RespContratoConvenio();
                    dto.setCodigoConvocatoria(contrato.getConvocatoria() == null ? null : contrato.getConvocatoria().getCodigoConvocatoria());
                    dto.setApellidos(contrato.getApellidos());
                    dto.setNombres(contrato.getNombres());
                    dto.setRegimen(contrato.getRegimenId() == null ? null : contrato.getRegimenId().getDescripcion());
                    dto.setTipoDoc(contrato.getTipoDoc());
                    dto.setNroDocumento(contrato.getDni()); //???? SE DEBE LLAMAR NRO DOCUMENTO
                    dto.setFechaContrato(contrato.getFechaSubscripcion() == null ? null : String.valueOf(contrato.getFechaSubscripcion()));
                    dto.setEstado(contrato.getEstado() == null ? null : contrato.getEstado().getDescripcion());
                    dto.setIdEstado(contrato.getEstado() == null ? null : contrato.getEstado().getMaeDetalleId());
                    dto.setCodProEstado(contrato.getEstado() == null ? null : contrato.getEstado().getCodProg());
                    dto.setEstadoRegistro(contrato.getEstadoRegistro());
                    dto.setIdContrato(contrato.getContratoId());
                    dto.setPerfil(contrato.getNombrePuesto());
                    dto.setIdPerfil(contrato.getPerfilId());
                    dto.setPostulanteSelId(contrato.getPostulanteSelId());
                    dto.setTipoTrabajo(contrato.getTipoTrabajoId() == null ? null : contrato.getTipoTrabajoId().getMaeDetalleId());
                    dto.setTipoTrabajoDesc(contrato.getTipoTrabajoId() == null ? null : contrato.getTipoTrabajoId().getDescripcion());
                    dto.setArchivoSuscrito(contrato.getArchivoSuscrito() == null ? null : contrato.getArchivoSuscrito());
                    dto.setBaseId(contrato.getBaseId() == null ? null : contrato.getBaseId());
                    return dto;
                }).collect(Collectors.toList());
    }

    /**
     * SETT DTO CONTRATO
     *
     * @param contrato
     * @return
     */
    private RespToContratoConvenio settContratoDto(Contrato contrato) {
        RespToContratoConvenio resp = new RespToContratoConvenio();
        /*CONVOCATORIA*/
        resp.setNumero(contrato.getNumero() == null ? null : contrato.getNumero());
        resp.setAnio(contrato.getAnio());
        resp.setNombrePuesto(contrato.getNombrePuesto());
        resp.setFamilia(contrato.getFamilia());
        resp.setRol(contrato.getRol());
        resp.setNombreOrgano(contrato.getNombreOrgano());
        resp.setNombreUniOrganica(contrato.getNombreUniOrganica());
        resp.setNivelCategoria(contrato.getNivelCategoria());
        resp.setSubNivelcat(contrato.getSubNivelcat());
        resp.setNroConPub(contrato.getNroConPub());
        resp.setTipoTramiteId(contrato.getTipoTramiteId() == null ? null : contrato.getTipoTramiteId().getMaeDetalleId());
        resp.setTipoServicioId(contrato.getTipoServicioId() == null ? null : contrato.getTipoServicioId().getMaeDetalleId());
        resp.setPosCuadPuesto(contrato.getPosCuadPuesto());
        resp.setLugarPrestacion(contrato.getLugarPrestacion());
        resp.setSede(contrato.getSede());
        resp.setCompeEconomica(contrato.getCompeEconomica());
        resp.setJornalaboral(contrato.getJornalaboral());
        resp.setPeriodoPrueba(contrato.getPeriodoPrueba());
        resp.setNroInforme(contrato.getNroInforme());
        resp.setNormaAproProy(contrato.getNormaAproProy());
        resp.setNroResolucion(contrato.getNroResolucion());
        resp.setFechaSubscripcion(contrato.getFechaSubscripcion());
        resp.setFechaResolucion(contrato.getFechaResolucion());
        resp.setFechaVinculacion(contrato.getFechaVinculacion());
        resp.setArchivoSuscrito(contrato.getArchivoSuscrito() == null ? null : contrato.getArchivoSuscrito());
        resp.setNroConPubMeritos(contrato.getConvocatoria() == null ? null :contrato.getConvocatoria().getCodigoConvocatoria());
        resp.setBaseId(contrato.getBaseId());

        /*ENTIDAD*/
        resp.setRazonSocial(contrato.getRazonSocial());
        resp.setRuc(contrato.getRuc());
        resp.setSiglas(contrato.getSiglas());
        resp.setDireccion(contrato.getDireccion());
        resp.setResponsableOrh(contrato.getResponsableOrh());
        resp.setPuestoResponsableOrh(contrato.getPuestoResponsableOrh());
        
        resp.setResolResponOrh(contrato.getResolResponOrh());
        resp.setTipoDocResponsable(contrato.getTipoDocResponsable());
        resp.setNroDocResponsable(contrato.getNroDocResponsable());
        resp.setArticulo(contrato.getArticulo());
        resp.setNroNorma(contrato.getNroNorma());

        /*POSTULANTE*/
        resp.setNombres(contrato.getNombres() == null ? null : contrato.getNombres());
        resp.setApellidos(contrato.getApellidos() == null ? null : contrato.getApellidos());
        resp.setDni(contrato.getDni() == null ? null : contrato.getDni());
        resp.setNacionalidad(contrato.getNacionalidad() == null ? null : contrato.getNacionalidad());
        resp.setDomicilio(contrato.getDomicilio() == null ? null : contrato.getDomicilio());
        resp.setFechaNacimiento(contrato.getFechaNacimiento() == null ? null : contrato.getFechaNacimiento());
        
        /*POSTULANTE CONVENIO*/
        resp.setDireccionLabores(contrato.getDireccionLabores() == null ? null : contrato.getDireccionLabores());
        resp.setFechaIniPractica(contrato.getFechaIniPractica() == null ? null : contrato.getFechaIniPractica());
        resp.setFechaFinPractica(contrato.getFechaFinPractica() == null ? null : contrato.getFechaFinPractica());
        resp.setHoraIniPractica(contrato.getHoraIniPractica() == null ? null : contrato.getHoraIniPractica().trim());
        resp.setHoraFinPractica(contrato.getHoraFinPractica() == null ? null : contrato.getHoraFinPractica().trim());
        resp.setDireccionCentroEstudios(contrato.getDireccionCentroEstudios() == null ? null : contrato.getDireccionCentroEstudios().trim());
        resp.setRucCentroEstudios(contrato.getRucCentroEstudios() == null ? null : contrato.getRucCentroEstudios().trim());
        resp.setCentroEstudios(contrato.getCentroEstudios() == null ? null : contrato.getCentroEstudios());
        resp.setPuestoRepreUni(contrato.getPuestoRepreUni() == null ? null : contrato.getPuestoRepreUni());
        resp.setNombRepreUni(contrato.getNombRepreUni() == null ? null : contrato.getNombRepreUni());
        resp.setTipoDocRepreUni(contrato.getTipoDocRepreUni() == null ? null : contrato.getTipoDocRepreUni());
        resp.setNroDocRepreUni(contrato.getNroDocRepreUni() == null ? null : contrato.getNroDocRepreUni());
        resp.setCiclo(contrato.getCiclo() == null ? null : contrato.getCiclo());
        resp.setTipoPractica(contrato.getTipoPractica() == null ? null : Long.valueOf(contrato.getTipoPractica()));
        
        resp.setNroPosPueMeri(contrato.getNroPosPueMeri() == null ? null : contrato.getNroPosPueMeri());
        resp.setTipoContratoId(contrato.getTipoContratoId().getTipoContratoId());
        resp.setCodigoTipoContrato(contrato.getTipoContratoId().getCodigoPlantilla());
        resp.setRutaPlantilla(contrato.getTipoContratoId().getRuta());
        
        if(!Util.isEmpty(contrato.getConvocatoria().getBase().getTipoId())) {
        	Optional<MaestraDetalle> tipoOpt = maestraDetalleRepository.findById(contrato.getConvocatoria().getBase().getTipoId());
        	if(tipoOpt.isPresent()) {
        		MaestraDetalle maestra = tipoOpt.get();
        		resp.setTipoConcurso(maestra.getDescripcion());
        	}
        }
        
        
        return resp;
    }

    /**
     * DESESTIMAR CONTRATO POR ID / ESTADO A DESESTIMADO
     *
     * @param
     * @param token
     * @param contratoId
     * @return
     */

    @Override
    public RespBase<Object> desestimarContrato(ReqBase<ReqDesestimaContrato> request, MyJsonWebToken token,
                                               Long contratoId) {

        RespBase<Object> response = new RespBase<>();
        String ruta = null;
        String prefijo = null;
        List<MaestraDetalle> lsTipoTrabajo = new ArrayList<>();
        List<MaestraDetalle> lsEstadoProceso = new ArrayList<>();
        Base base = null;

        lsTipoTrabajo = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_TIPO_TRABAJO);
        Map<String, Long> mapTipoTrabajo = lsTipoTrabajo.stream().filter(Objects::nonNull)
                .collect(Collectors.toMap(MaestraDetalle::getCodProg, MaestraDetalle::getMaeDetalleId));

        lsEstadoProceso = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_EST_CONTRATO);
        Map<String, Long> mapEstado = lsEstadoProceso.stream().filter(Objects::nonNull)
                .collect(Collectors.toMap(MaestraDetalle::getCodProg, MaestraDetalle::getMaeDetalleId));

        //ACTUALIZAR CONTRATO A DESESTIMADO
        if (contratoId == null)
            throw new NotFoundException("el contratoId no puede ser null");

        if (request.getPayload() == null)
            throw new NotFoundException("el payload no puede ser null");

        Optional<Contrato> contratoOpt = contratoRepository.findById(contratoId);
        if (!contratoOpt.isPresent())
            throw new NotFoundException("no existe el contrato con el id: (" + contratoId + ") en base de datos");

        Contrato contrato = contratoOpt.get();
        if(contrato.getTipoTrabajoId().getCodProg().equals("1")) {
        	prefijo = Constantes.PRE_ADJ_DESE_CONTRATO;
        }else {
        	prefijo = Constantes.PRE_ADJ_DESE_CONVENIO;
        }
        ruta = this.uploadContrato(contrato, request.getPayload().getPdfBase64(), prefijo);
        if (ruta == null || ruta.isEmpty()) {
            throw new NotFoundException("Error al guardar el archivo adjunto");
        }

        if (!Util.isEmpty(request.getPayload().getEstadoId())) {
            Optional<MaestraDetalle> estado = maestraDetalleRepository.findById(request.getPayload().getEstadoId());
            estado.ifPresent(contrato::setEstado);
        }
        if (!Util.isEmpty(request.getPayload().getCategoriaDesestimientoId())) {
            Optional<MaestraDetalle> categoriaDese = maestraDetalleRepository.findById(request.getPayload().getCategoriaDesestimientoId());
            categoriaDese.ifPresent(contrato::setCategoriaDesestimiento);
        }
        contrato.setMotivoDesestimiento(request.getPayload().getMotivoDesestimiento());
        contrato.setArchivoDesestimiento(ruta);
        contrato.setCampoSegUpd(contrato.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
        contratoRepository.save(contrato);

        //GENERAR NUEVO CONTRATO
        RespBase<RespToContratoConvenio> py = convocatoriaService.
                findDatosToContratosToConvenios(
                        request.getPayload().getPostulante().getBaseId(),
                        request.getPayload().getPostulante().getPostulanteId(),
                        request.getPayload().getPostulante().getPerfilId(),
                        request.getPayload().getPostulante().getEntidadId());
        if (Util.isEmpty(py.getPayload())) {
            throw new NotFoundException("no existe informacion a procesar");
        }

        RespToContratoConvenio cont = py.getPayload();
        Contrato newCon = this.setContrato(cont);
        
        if(!Util.isEmpty(request.getPayload().getTipoTrabajo())) {
        	Optional<MaestraDetalle> tipoTrabajo = maestraDetalleRepository.findById(mapTipoTrabajo.get(request.getPayload().getTipoTrabajo()));
        	tipoTrabajo.ifPresent(newCon::setTipoTrabajoId);// CONTRATO 1 CONVENIO 2
        }
        
        if (!Util.isEmpty(mapEstado.get(Constantes.COD_ESTADO_PENDIENTE))) {
            Optional<MaestraDetalle> estado = maestraDetalleRepository.findById(mapEstado.get(Constantes.COD_ESTADO_PENDIENTE));
            estado.ifPresent(newCon::setEstado);
        }
        if(!Util.isEmpty(cont.getBaseId())) {
        	Optional<Base> baseF = baseRepository.findById(cont.getBaseId());
        	if(baseF.isPresent()) {
        		base = baseF.get();
        	}
        }
        if (base!= null) {
            if (!Util.isEmpty(base.getRegimenId())) {
                Optional<MaestraDetalle> regimen = maestraDetalleRepository.findById(base.getRegimenId());
                regimen.ifPresent(newCon::setRegimenId);
            }
        }
        
        if(!Util.isEmpty(cont.getBaseId())) {
        	Optional<Convocatoria> convocatoria = convocatoriaRepository.findByBase(base);
        	convocatoria.ifPresent(newCon::setConvocatoria);
        }

        newCon.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        contratoRepository.save(newCon);

        response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se registraron los contratos correctamente ");

        return response;
    }

    private String uploadContrato(Contrato contrato, String pdfBase64, String  filename) {
        String url = "";
        RespBase<ApiUploadFile> requestApiUploadFile = new RespBase<ApiUploadFile>();
        ApiUploadFile uploadFile = new ApiUploadFile();
        uploadFile.setExtension(Constantes.EXTENSION_PDF);
        uploadFile.setFileBase64(pdfBase64);
        String pathTmp = rutaFileServerEntidad();
        uploadFile.setFileName(filename + contrato.getContratoId());
        String rutaFileServer = ParametrosUtil.datePathReplaceRepositoryAlfresco(pathTmp);
        rutaFileServer = rutaFileServer.replace(Constantes.PATH_ENTIDAD,
                contrato.getEntidadId().toString());
        uploadFile.setPath(rutaFileServer);
        requestApiUploadFile.setPayload(uploadFile);
        try {
            RespBase<RespUploadFile> upload = maestraApiClient.uploadFile(requestApiUploadFile);
            url = upload.getPayload().getPathRelative();
        } catch (Exception e) {
          return  url;
        }
        
        return url;
    }

    private String rutaFileServerEntidad() {
        RespBase<RespObtieneLista<ParametrosDTO>> resp = maestraApiClient
                .rutaFileServerEntidad(Constantes.PARAMETRO_FILE, Constantes.PAR_CODIGO_TEXTO);
        for (ParametrosDTO it : resp.getPayload().getItems()) {
            if (it.getCodigoTexto().equalsIgnoreCase(Constantes.PAR_CODIGO_TEXTO)) {
                return it.getValorTexto();
            }
        }
        return null;
    }

    /**
     * SUSCRIBIR CONTRATO POR ID / ESTADO A SUSCRITO
     * @param token
     * @param contratoId
     * @return
     */
    
	@Override
	public RespBase<Object> suscribirContrato(ReqBase<ReqSuscribirContrato> request, MyJsonWebToken token,
			Long contratoId) {
		 String ruta = null;
		 String prefijo = null;
		 RespBase<Object> response = new RespBase<>();
		 
		if (contratoId == null)
            throw new NotFoundException("el contratoId no puede ser null");

        if (request.getPayload() == null)
            throw new NotFoundException("el payload no puede ser null");

        Optional<Contrato> contratoOpt = contratoRepository.findById(contratoId);
        if (!contratoOpt.isPresent())
            throw new NotFoundException("no existe el contrato con el id: (" + contratoId + ") en base de datos");

        Contrato contrato = contratoOpt.get();
        if(contrato.getTipoTrabajoId().getCodProg().equals("1")) {
        	prefijo = Constantes.PRE_ADJ_SUSC_CONTRATO;
        }else {
        	prefijo = Constantes.PRE_ADJ_SUSC_CONVENIO;
        }
        	
        ruta = this.uploadContrato(contrato, request.getPayload().getPdfBase64(), prefijo);
        if (ruta.isEmpty() || ruta == null) {
            throw new NotFoundException("Error al guardar el archivo adjunto");
        }
		
        if (!Util.isEmpty(request.getPayload().getEstadoId())) {
            Optional<MaestraDetalle> estadoS = maestraDetalleRepository.findById(request.getPayload().getEstadoId());
            estadoS.ifPresent(contrato::setEstado);
        }
        
        contrato.setFechaSubscripcion(request.getPayload().getFechaSuscripcion());
        contrato.setArchivoSuscrito(ruta);
        contrato.setCampoSegUpd(contrato.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
        contratoRepository.save(contrato);
        
        response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se suscribio correctamente el contrato");
        
		return response;
	}

	@Override
	public RespBase<RespToContratoConvenio> actualizaConvenio(ReqBase<ReqConvenio> request, MyJsonWebToken token,
			Long convenioId) {
		
		 LocalDate date = LocalDate.now();
	        if (convenioId == null)
	            throw new NotFoundException("el contratoId no puede ser null");

	        if (request.getPayload() == null)
	            throw new NotFoundException("el payload no puede ser null");

	        Optional<Contrato> convenioOpt = contratoRepository.findById(convenioId);
	        if (!convenioOpt.isPresent())
	            throw new NotFoundException("no existe el contrato con el id: (" + convenioId + ") en base de datos");


	        Contrato convenio = convenioOpt.get();
	        convenio = this.setConvenioAct(convenio, request.getPayload());
	        
	        if (!Util.isEmpty(request.getPayload().getEstadoId())) {
	            Optional<MaestraDetalle> estado = maestraDetalleRepository.findById(request.getPayload().getEstadoId());
	            estado.ifPresent(convenio::setEstado);
	        }
		
	        convenio.setAnio(date.getYear());
	        convenio.setNumero(this.obtenerCorrelativoContrato(
	        		convenio.getTipoTrabajoId(), date.getYear(), convenio.getEntidadId()));
	        convenio.setCampoSegUpd(convenio.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
	        contratoRepository.save(convenio);

	        RespBase<RespToContratoConvenio> resPayload = new RespBase<>();
	        resPayload.setPayload(this.settContratoDto(convenio));
	        return resPayload;
	}

	@Override
	public byte[] descargaContrato(Long contratoId) {
		
		//String resultado = null;
		String pathFileContrato = null;
		
		 RespBase<RespToContratoConvenio> py = this.getContratoConvenio(contratoId);
         if (Util.isEmpty(py.getPayload())) {
             throw new NotFoundException("no existe informacion a procesar");
         }
        
        String ruta = py.getPayload().getRutaPlantilla();
        //pathFileContrato = getClass().getResource(ruta).getFile(); //desa
        pathFileContrato =  System.getProperty("user.home") + ruta; //servidor
        
        try {
			byte[] mergedOutput = generaDocumento.generarDocumento(pathFileContrato, 
					TemplateEngineKind.Freemarker, this.setMarcadores(py.getPayload()));
			return mergedOutput;
		} catch (IOException | XDocReportException e1) {
			//resultado = "Error";
			 logger.info(e1.getMessage());
		}
		return null;
		
	}
	
	public Map<String,Object> setMarcadores(RespToContratoConvenio r){
		Map<String,Object> dataMap = new HashMap<>();
		
		dataMap.put("ORGANO", r.getNombreOrgano() == null ? "$NOMBRE_ORGANO$" : r.getNombreOrgano());
		dataMap.put("NRORESOLUCION", r.getNroResolucion() == null ? "$NRO_RESOLUCION$" : r.getNroResolucion());
		dataMap.put("NRO_INFORME", r.getNroInforme() == null  ? "$NRO_INFORMFE$" : r.getNroInforme());
		dataMap.put("UNIDAD_ORGANICA", r.getNombreUniOrganica() == null ? "$UNIDAD_ORGANICA$" : r.getNombreUniOrganica());
		dataMap.put("NOMBRE_PUESTO", r.getNombrePuesto() == null ? "$NOMBRE_PUESTO$" : r.getNombrePuesto());
		dataMap.put("FAMILIA", r.getFamilia() == null ? "$FAMILIA$" : r.getFamilia());
		dataMap.put("NIVEL", r.getNivelCategoria() == null ? "NIVEL" : r.getNivelCategoria());
		dataMap.put("SUBNIVEL", r.getSubNivelcat() == null ? "SUBNIVEL" : r.getSubNivelcat());
		dataMap.put("ROL", r.getRol() == null ? "$ROL$" : r.getRol());
		dataMap.put("NROPOSPUEMERI", String.valueOf(r.getNroPosPueMeri()) == null ? "$PUESTO_MERITO$" : String.valueOf(r.getNroPosPueMeri()));
		dataMap.put("NROCONPUBMERITOS", r.getNroConPubMeritos() == null ? "$CODIGO_CONVOCATORIA$": r.getNroConPubMeritos());
		dataMap.put("NOMBRES", r.getNombres()+" "+r.getApellidos());
		dataMap.put("DNI", r.getDni() == null ? "$DNI$" : r.getDni());
		dataMap.put("SIGLA", r.getSiglas() == null ? "$SIGLA$" : r.getSiglas());
		dataMap.put("FECHAVINCULACION", r.getFechaVinculacion() == null ? "$FECHA_VINCULACION$" : DateUtil.fmtDt(r.getFechaVinculacion()));
		dataMap.put("PERIODOPRUEBA", r.getPeriodoPrueba() == null ? "$PERIODO_PRUEBA$" : r.getPeriodoPrueba());
		dataMap.put("TIPO_CONCURSO", r.getTipoConcurso() == null ? "$TIPO_CONCURSO$" : r.getTipoConcurso());
		dataMap.put("RAZONSOCIAL", r.getRazonSocial() == null ? "$RAZON_SOCIAL$" : r.getRazonSocial());
		dataMap.put("RUC",r.getRuc() == null ? "$RUC$" : r.getRuc());
		dataMap.put("DIRECCION", r.getDireccion() == null ? "$DIRECCION$" : r.getDireccion());
		dataMap.put("PUESTOORH", r.getPuestoResponsableOrh() == null ? "$PUESTO_ADMIN$" : r.getPuestoResponsableOrh());
		dataMap.put("RESPONSABLEORH",r.getResponsableOrh() == null ? "$NOMBRE_PUESTO_ADMIN$" : r.getResponsableOrh());
		dataMap.put("NRODOCRESPONSABLE",r.getNroDocResponsable() == null ? "$NRO_DOC_ADMIN$" : r.getNroDocResponsable());
		dataMap.put("CENTROESTUDIOS", r.getCentroEstudios() == null ? "$CENTRO_ESTUDIOS$" : r.getCentroEstudios());
		dataMap.put("RUCCENTROESTUDIOS", r.getRucCentroEstudios() == null ? "$RUC_CENTRO_ESTUDIOS$" : r.getRucCentroEstudios());
		dataMap.put("DIRECCIONCENTROESTUDIOS", r.getDireccionCentroEstudios() == null ? "$DIRECCION_CENTRO_ESTUDIOS$": r.getDireccionCentroEstudios());
		dataMap.put("PUESTOREPREUNI",r.getPuestoRepreUni() == null ? "$NOMBRE_PUESTO_UNI$" : r.getPuestoRepreUni());
		dataMap.put("NOMBREPREUNI",r.getNombRepreUni() == null ? "$NOMBRE_REPRE_UNI$" : r.getNombRepreUni());
		dataMap.put("NRODOCREPREUNI", r.getNroDocRepreUni() == null ? "$NRODOC_REPRE_UNI$" :r.getNroDocRepreUni());
		dataMap.put("NACIONALIDAD" , r.getNacionalidad() == null ? "$NACIONALIDAD$" : r.getNacionalidad());
		dataMap.put("FECHANACIMIENTO", r.getFechaNacimiento() == null ? "$FECHA_NACIMIENTO$" : r.getFechaNacimiento());
		dataMap.put("DOMICILIO", r.getDomicilio() == null ? "$DOMICILIO$" : r.getDomicilio());
		dataMap.put("CICLO" ,r.getCiclo() == null ? "$CICLO$" : r.getCiclo());
		dataMap.put("DIRECCIONLABORES", r.getDireccionLabores() == null ? "$DIRECCIONLABORES$" : r.getDireccionLabores());
		dataMap.put("PERIODOCONVENIO", r.getPeriodoConvenio() == null ? "$PERIODOCONVENIO$" : r.getPeriodoConvenio());
		dataMap.put("FECHAINI", r.getFechaIniPractica() == null ? "$FECHAINI$" : DateUtil.fmtDt(r.getFechaIniPractica()));
		dataMap.put("FECHAFIN", r.getFechaFinPractica() == null ? "$FECHAFIN$" : DateUtil.fmtDt(r.getFechaFinPractica()));
		dataMap.put("HORAINI", r.getHoraIniPractica() == null ? "$HORAFIN$" : r.getHoraIniPractica());
		dataMap.put("HORAFIN", r.getHoraFinPractica() == null ? "$HORAFIN$" : r.getHoraFinPractica());
		dataMap.put("COMPEECONOMICA", r.getCompeEconomica() == null ? "$COMPEECONOMICA$" : r.getCompeEconomica());

		return dataMap;
	}
	
	@Override
	public RespBase<RespTipoContrato> listaTipoContrato(Long regimenId) {
		
		  RespTipoContrato respPayload  = new RespTipoContrato();

	        List<TipoContrato> lstTipoContrato = tipoContratoRepository.findAll((root, cq, cb) -> {
	            Predicate p = cb.conjunction();
	            Join<TipoContrato, MaestraDetalle> regimen = root.join("regimenId");

	            if (!Util.isEmpty(regimenId)) {
	            	p = cb.and(p, cb.equal(regimen.get("maeDetalleId"), regimenId));
	            }
	            return p;
	        });
	        // p = cb.and(p, cb.equal(baseJ.get("regimenId"), regimen));
	        respPayload.setLstTipoContrato(this.setLstTipoContrato(lstTipoContrato));
	       // setConvocatorias(this.setConvocatoriaJob(lstConvocatoria));

	        return new RespBase<RespTipoContrato>().ok(respPayload);
	
	}
	

    private List<TipoContratoDTO> setLstTipoContrato(List<TipoContrato> ls) {
        return ls.stream()
                .filter(tipoContrato -> !tipoContrato.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
                .map(tipoContrato -> {
                	TipoContratoDTO c = new TipoContratoDTO();
                	c.setTipoContratoId(tipoContrato.getTipoContratoId());
                	c.setDescPlantilla(tipoContrato.getDescPlantilla());
                	c.setCodigoPlantilla(tipoContrato.getCodigoPlantilla());
                	c.setRegimenId(tipoContrato.getRegimenId().getMaeDetalleId());
                    return c;
                }).collect(Collectors.toList());
    }

	
}
