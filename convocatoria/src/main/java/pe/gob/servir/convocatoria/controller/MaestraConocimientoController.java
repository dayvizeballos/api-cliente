package pe.gob.servir.convocatoria.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraConocimiento;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespListaMaestraConocimiento;
import pe.gob.servir.convocatoria.response.RespMaestraConocimiento;
import pe.gob.servir.convocatoria.response.RespObtieneConocimiento;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.MaestraConocimientoService;

@RestController
@Tag(name = "Maestra Conocimiento", description = "")
public class MaestraConocimientoController {

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	MaestraConocimientoService maestraConocimientoService;

	@Operation(summary = "Crea una maestra conocimiento", description = "Crea una maestra conocimiento", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType =  MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType =  MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/maestraConocimiento" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespMaestraConocimiento>> registrarMaestraConocimiento(@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqMaestraConocimiento> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespMaestraConocimiento> response = maestraConocimientoService.guardarMaestraConocimiento(request, jwt, null);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra conocimientos", description = Constantes.SUM_OBT_LIST+"maestra conocimientos", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraConocimiento"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespListaMaestraConocimiento>> listarMaestraConocimiento(
			@PathVariable String access,
			@RequestParam(value = "tipoConocimiento", required = false) Long tipoConocimiento,
			@RequestParam(value = "categoriaConocimiento", required = false) Long categoriaConocimiento,
			@RequestParam(value = "descripcion", required = false) String descripcion
			) {
		
				Map<String, Object> parametroMap = new HashMap<>();
				parametroMap.put("tipoConocimiento", tipoConocimiento);
				parametroMap.put("categoriaConocimiento", categoriaConocimiento);
				parametroMap.put("descripcion", descripcion);
				
		RespBase<RespListaMaestraConocimiento> response = maestraConocimientoService.obtenerMaestraConocimiento(parametroMap);
		
		return ResponseEntity.ok(response);		
	}
	
	@Operation(summary = "Actualiza una maestra conocimiento", description = "Actualiza una maestra conocimiento", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/maestraConocimiento/{maeConocimientoId}" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespMaestraConocimiento>> actualizarMaestraConocimiento(@PathVariable String access,
			@PathVariable Long maeConocimientoId, @Valid @RequestBody ReqBase<ReqMaestraConocimiento> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespMaestraConocimiento> response = maestraConocimientoService.guardarMaestraConocimiento(request, jwt, maeConocimientoId);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "get conocimiento ", description = "get conocimiento ", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/maestraConocimiento/{maeConocimientoId}"},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RespObtieneConocimiento>> getConocimiento(
				@PathVariable String access, 
				@PathVariable Long maeConocimientoId) {
		RespBase<RespObtieneConocimiento> response = maestraConocimientoService.obtenerMaestraConocimientoPorId(maeConocimientoId);
		 return ResponseEntity.ok(response);
		 }

	@Operation(summary = "Inactiva un Conocimiento", description = "Inactiva una Conocimiento", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@DeleteMapping(path = { Constantes.BASE_ENDPOINT + "/maestraConocimiento/{maeConocimientoId}" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> eliminarConocimiento(@PathVariable String access,
			@PathVariable Long maeConocimientoId){
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> response = maestraConocimientoService.eliminarConocimiento(jwt, maeConocimientoId);
		return ResponseEntity.ok(response);
	}
	
}
