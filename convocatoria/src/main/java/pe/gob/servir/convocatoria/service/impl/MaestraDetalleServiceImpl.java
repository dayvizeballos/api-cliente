package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.ConfiguracionMaestra;
import pe.gob.servir.convocatoria.model.MaestraCebecera;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.repository.ConfiguracionMaestraRepository;
import pe.gob.servir.convocatoria.repository.EvaluacionRepositoryJdbc;
import pe.gob.servir.convocatoria.repository.MaestraCabeceraRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepositoryJdbc;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraDetalle;
import pe.gob.servir.convocatoria.request.dto.MaestraDetalleDto;
import pe.gob.servir.convocatoria.request.dto.MasterEvaluacionDTO;
import pe.gob.servir.convocatoria.request.dto.ModalidadDTO;
import pe.gob.servir.convocatoria.request.dto.RegimenDTO;
import pe.gob.servir.convocatoria.request.dto.TipoDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespComboCabecera;
import pe.gob.servir.convocatoria.response.RespComboDetalleMaestra;
import pe.gob.servir.convocatoria.response.RespListaMaestraEntidad;
import pe.gob.servir.convocatoria.response.RespMaestraDetalle;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespObtieneMaestraDetalle;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

@Service
public class MaestraDetalleServiceImpl implements MaestraDetalleService{
		
	@Autowired
	private MaestraDetalleRepository maestraDetalleRespository;
	
	@Autowired
	private MaestraCabeceraRepository maestraCabeceraRepository;
	
	@Autowired
	private ConfiguracionMaestraRepository configuracionMaestraRepository;
	
	@Autowired
	private MaestraDetalleRepositoryJdbc maestraDetalleRepositoryJdbc;
	
    @Autowired
    private EvaluacionRepositoryJdbc evaluacionRepositoryJdbc;
	
	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<RespMaestraDetalle> guardarMaestraDetalle(ReqBase<ReqMaestraDetalle> request, MyJsonWebToken token,
			Long maestraDetalleId) {
		
		RespBase<RespMaestraDetalle> response = new RespBase<>();
		
		MaestraDetalle maestraDetalle = null;
		
		String codigoDetalle = null;
		
		if(maestraDetalleId!=null) {
			Optional<MaestraDetalle> maestraDetalleFind = maestraDetalleRespository.findById(maestraDetalleId);
			
			if(maestraDetalleFind.isPresent()) {
				maestraDetalle = maestraDetalleFind.get();
			}else{
				response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
						"No Existe el maestraDetalleId Ingresado");
				return response;
			}
			
			maestraDetalle.setCampoSegUpd(request.getPayload().getEstado(), token.getUsuario().getUsuario(),
					Instant.now());
		}else {
			maestraDetalle = new MaestraDetalle();
			maestraDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now()); 
			
			codigoDetalle = maestraDetalleRepositoryJdbc.findCorrelativoDetalle(request.getPayload().getMaeCabeceraId());
			maestraDetalle.setCodProg(codigoDetalle);
		}
		
		maestraDetalle.setMaeCabeceraId(request.getPayload().getMaeCabeceraId());
		maestraDetalle.setDescripcion(request.getPayload().getNombreCompleto());
		maestraDetalle.setDescripcionCorta(request.getPayload().getNombreCorto());
		maestraDetalle.setSigla(request.getPayload().getSigla());
		maestraDetalle.setReferencia(request.getPayload().getReferencia());

	
		maestraDetalleRespository.save(maestraDetalle);
		RespMaestraDetalle  payload = new RespMaestraDetalle();
		payload.setMaestraDetalle(maestraDetalle);
		
		return new RespBase<RespMaestraDetalle>().ok(payload);
		
	}


	@Override
	public RespBase<RespObtieneMaestraDetalle> obtenerMaestraDetalle(Long maestraCabeceraId,String descripcion,String sigla, String estado , String codCabecera, String codProg) {
		
		MaestraCebecera maeCabecera = new MaestraCebecera();
		List<MaestraDetalle> listaDetalles = null;
		List<MaestraDetalle> lisDetalles = null;

		if (!Util.isEmpty(maestraCabeceraId)){
			Optional<MaestraCebecera> oCabecera = maestraCabeceraRepository.findById(maestraCabeceraId);
			if(oCabecera.isPresent()) {
				maeCabecera = oCabecera.get();
			}
		}else{
			List<MaestraCebecera> lst = maestraCabeceraRepository.findAllcodigoCabecera(codCabecera);
			if (lst != null && !lst.isEmpty())
				maeCabecera = lst.get(0);

		}

		MaestraDetalle maeDetalle =  new MaestraDetalle();

		if (!Util.isEmpty(maestraCabeceraId)){
			maeDetalle.setMaeCabeceraId(maestraCabeceraId);
		}else{
			maeDetalle.setMaeCabeceraId(maeCabecera.getMaeCabeceraId());
		}

		Example<MaestraDetalle> example = Example.of(maeDetalle);
		MaestraDetalle det =  null;
		listaDetalles = new ArrayList<>();
		if(descripcion != null) {
			lisDetalles = maestraDetalleRespository.findAllDescripcion(descripcion);
			for (MaestraDetalle maestraDetalleDescripcion : lisDetalles) {
				det = new MaestraDetalle();
				det.setMaeCabeceraId(maestraDetalleDescripcion.getMaeCabeceraId());
				det.setMaeDetalleId(maestraDetalleDescripcion.getMaeDetalleId());
				det.setDescripcion(maestraDetalleDescripcion.getDescripcion());
				det.setDescripcionCorta(maestraDetalleDescripcion.getDescripcionCorta());
				det.setSigla(maestraDetalleDescripcion.getSigla());
				det.setReferencia(maestraDetalleDescripcion.getReferencia());
				det.setOrden(maestraDetalleDescripcion.getOrden());
				det.setEstadoRegistro(maestraDetalleDescripcion.getEstadoRegistro());
				det.setEstado(maestraDetalleDescripcion.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
				det.setCodProg(maestraDetalleDescripcion.getCodProg());
				listaDetalles.add(det);
			}
		}else if(sigla != null) {
			lisDetalles = maestraDetalleRespository.findAllSigla(sigla);
			List<MaestraDetalle> temp = new ArrayList<>();
			for (MaestraDetalle maestraDetalleSigla : lisDetalles) {
				det = new MaestraDetalle();
				det.setMaeCabeceraId(maestraDetalleSigla.getMaeCabeceraId());
				det.setMaeDetalleId(maestraDetalleSigla.getMaeDetalleId());
				det.setDescripcion(maestraDetalleSigla.getDescripcion());
				det.setDescripcionCorta(maestraDetalleSigla.getDescripcionCorta());
				det.setSigla(maestraDetalleSigla.getSigla());
				det.setReferencia(maestraDetalleSigla.getReferencia());
				det.setOrden(maestraDetalleSigla.getOrden());
				det.setEstadoRegistro(maestraDetalleSigla.getEstadoRegistro());
				det.setEstado(maestraDetalleSigla.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
				det.setCodProg(maestraDetalleSigla.getCodProg());
				temp.add(det);
			}
			if(estado!=null) {
					listaDetalles.addAll(temp.stream().filter(e -> e.getEstadoRegistro().equals(estado)).collect(Collectors.toList()));
			} else {
				listaDetalles.addAll(temp);
			}
		
			
			
		}

		else if(estado != null) {
			lisDetalles = maestraDetalleRespository.findAllEstado(estado, maestraCabeceraId);
			//lisDetalles = maestraDetalleRespository.findDetalleByCodeCabeceraEstado(codCabecera, estado);
			
			for (MaestraDetalle maestraDetalleEstado : lisDetalles) {
				det = new MaestraDetalle();
				det.setMaeCabeceraId(maestraDetalleEstado.getMaeCabeceraId());
				det.setMaeDetalleId(maestraDetalleEstado.getMaeDetalleId());
				det.setDescripcion(maestraDetalleEstado.getDescripcion());
				det.setDescripcionCorta(maestraDetalleEstado.getDescripcionCorta());
				det.setSigla(maestraDetalleEstado.getSigla());
				det.setReferencia(maestraDetalleEstado.getReferencia());
				det.setOrden(maestraDetalleEstado.getOrden());
				det.setEstadoRegistro(maestraDetalleEstado.getEstadoRegistro());
				det.setEstado(maestraDetalleEstado.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
				det.setCodProg(maestraDetalleEstado.getCodProg());
				listaDetalles.add(det);
			}
		}
		else if(codProg != null) {
			lisDetalles = maestraDetalleRespository.findDetalleByCodProg(codCabecera, codProg);
			
			for (MaestraDetalle maestraDetalleEstado : lisDetalles) {
				det = new MaestraDetalle();
				det.setMaeCabeceraId(maestraDetalleEstado.getMaeCabeceraId());
				det.setMaeDetalleId(maestraDetalleEstado.getMaeDetalleId());
				det.setDescripcion(maestraDetalleEstado.getDescripcion());
				det.setDescripcionCorta(maestraDetalleEstado.getDescripcionCorta());
				det.setSigla(maestraDetalleEstado.getSigla());
				det.setReferencia(maestraDetalleEstado.getReferencia());
				det.setOrden(maestraDetalleEstado.getOrden());
				det.setEstadoRegistro(maestraDetalleEstado.getEstadoRegistro());
				det.setEstado(maestraDetalleEstado.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
				det.setCodProg(maestraDetalleEstado.getCodProg());
				listaDetalles.add(det);
			}
		}
		else {
			lisDetalles = maestraDetalleRespository.findAll(example, Sort.by(Sort.Direction.ASC, "orden"));
			for (MaestraDetalle maestraDetalle : lisDetalles) {
				det = new MaestraDetalle();
				det.setMaeCabeceraId(maestraDetalle.getMaeCabeceraId());
				det.setMaeDetalleId(maestraDetalle.getMaeDetalleId());
				det.setDescripcion(maestraDetalle.getDescripcion());
				det.setDescripcionCorta(maestraDetalle.getDescripcionCorta());
				det.setSigla(maestraDetalle.getSigla());
				det.setReferencia(maestraDetalle.getReferencia());
				det.setOrden(maestraDetalle.getOrden());
				det.setEstadoRegistro(maestraDetalle.getEstadoRegistro());
				det.setEstado(maestraDetalle.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
				det.setCodProg(maestraDetalle.getCodProg());
				listaDetalles.add(det);
			}
		}
		RespObtieneMaestraDetalle respPayload =  new RespObtieneMaestraDetalle();
		respPayload.setMaestraCebecera(maeCabecera);
		respPayload.setMaestraDetalles(listaDetalles);
		return new RespBase<RespObtieneMaestraDetalle>().ok(respPayload);
	}

	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<Object> eliminarMaestraDetalle(MyJsonWebToken token, Long maeDetalleId, String estado) {
		RespBase<Object> response = new RespBase<>();
		Optional<MaestraDetalle> detalleFind = maestraDetalleRespository.findById(maeDetalleId);
		if(detalleFind.isPresent()) {
			MaestraDetalle detalle = detalleFind.get();
			detalle.setEstadoRegistro(estado);
			detalle.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
					Instant.now());
			maestraDetalleRespository.save(detalle);
			return  new RespBase<Object>().ok(detalle);
		}else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el maeDetalleId Ingresado");
		}
		
		return response;

	}


	@Override
	public RespBase<RespComboCabecera> comboMaestra() {
		List<MaestraCebecera> lista = maestraCabeceraRepository.findAll();
		RespComboCabecera respPayload =  new RespComboCabecera();
		respPayload.setListaCabecera(lista);
		return new RespBase<RespComboCabecera>().ok(respPayload);
	}


	
	@Override
	public RespBase<RespComboCabecera> obtenerMaestra(Long maestraCabeceraId,Long entidadId) {
		MaestraCebecera maeCabecera = null;
		List<MaestraDetalle> listaDetalles = null;
		RespComboCabecera respPayload = new RespComboCabecera();
		List<MaestraCebecera> listaCabecera = null;
		if(maestraCabeceraId != null) {
			maeCabecera = new MaestraCebecera();
			Optional<MaestraCebecera> oCabecera = maestraCabeceraRepository.findById(maestraCabeceraId);
			if(oCabecera.isPresent()) {
				maeCabecera = oCabecera.get();
			}
			MaestraDetalle maeDetalle =  new MaestraDetalle();
			maeDetalle.setMaeCabeceraId(maestraCabeceraId);		
			Example<MaestraDetalle> example = Example.of(maeDetalle);
			List<MaestraDetalle> listarDetalles = maestraDetalleRespository.findAll(example);
			listaDetalles = new ArrayList<>();
			for (MaestraDetalle detalle : listarDetalles) {
				MaestraDetalle det = new MaestraDetalle();
				ConfiguracionMaestra config = configuracionMaestraRepository.findByDetalleServir(detalle.getMaeDetalleId(), entidadId);
				
				det.setMaeDetalleId(detalle.getMaeDetalleId());
				det.setMaeCabeceraId(detalle.getMaeCabeceraId());
				det.setDescripcion(detalle.getDescripcion());
				det.setDescripcionCorta(detalle.getDescripcionCorta());
				det.setSigla(detalle.getSigla());
				det.setReferencia(detalle.getReferencia());
				det.setOrden(detalle.getOrden());	
				det.setEstadoRegistro(detalle.getEstadoRegistro());
				det.setEstado(detalle.getEstadoRegistro().equals("1")? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
				det.setCodProg(detalle.getCodProg());

				
				if(entidadId != null) {
					if(config != null) {
						det.setConfiguracionId(config.getConfiguracionMaestraId());
						det.setEstadoConfiguracion(config.getEstadoRegistro());
					}
				}
				listaDetalles.add(det);
			}
			maeCabecera.setListMaestraDetalles(listaDetalles);
			listaCabecera = new ArrayList<>();
			listaCabecera.add(maeCabecera);
			respPayload.setListaCabecera(listaCabecera);			
		}else {
			List<MaestraCebecera>listarCabecera = maestraCabeceraRepository.findAll();
			listaCabecera = new ArrayList<>();
			for (MaestraCebecera maestraCabecera : listarCabecera) {				
				maeCabecera = new MaestraCebecera();
				maeCabecera.setMaeCabeceraId(maestraCabecera.getMaeCabeceraId());
				maeCabecera.setCodigoCabecera(maestraCabecera.getCodigoCabecera());
				maeCabecera.setDescripcion(maestraCabecera.getDescripcion());
				maeCabecera.setDescripcionCorta(maestraCabecera.getDescripcionCorta());
				maeCabecera.setSoloServir(maestraCabecera.getSoloServir());
				MaestraDetalle maeDetalle =  new MaestraDetalle();
				maeDetalle.setMaeCabeceraId(maestraCabecera.getMaeCabeceraId());		
				Example<MaestraDetalle> example = Example.of(maeDetalle);
				List<MaestraDetalle> listarDetalles = maestraDetalleRespository.findAll(example);
				listaDetalles = new ArrayList<>();
				for (MaestraDetalle detalle : listarDetalles) {
					MaestraDetalle det =  null;	
					ConfiguracionMaestra config = configuracionMaestraRepository.findByDetalleServir(detalle.getMaeDetalleId(), entidadId);
					if(detalle.getMaeCabeceraId().toString().equals(maestraCabecera.getMaeCabeceraId().toString())) {
						det = new MaestraDetalle();
						det.setMaeDetalleId(detalle.getMaeDetalleId());
						det.setMaeCabeceraId(detalle.getMaeCabeceraId());
						det.setDescripcion(detalle.getDescripcion());
						det.setDescripcionCorta(detalle.getDescripcionCorta());
						det.setSigla(detalle.getSigla());
						det.setReferencia(detalle.getReferencia());
						det.setOrden(detalle.getOrden());	
						det.setEstadoRegistro(detalle.getEstadoRegistro());
						det.setEstado(detalle.getEstadoRegistro().equals("1")? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
						det.setCodProg(detalle.getCodProg());
						
						if(entidadId != null) {
							if(config != null) {
								det.setConfiguracionId(config.getConfiguracionMaestraId());
								det.setEstadoConfiguracion(config.getEstadoRegistro());
							}
						}
						listaDetalles.add(det);
					}					
					maeCabecera.setListMaestraDetalles(listaDetalles);	
				}
				listaCabecera.add(maeCabecera);					
			}
			respPayload.setListaCabecera(listaCabecera);	
			
		}
		
		return new RespBase<RespComboCabecera>().ok(respPayload);
	}


//	@Override
//	public RespBase<RespComboDetalleMaestra> comboMaestraDetalle(Long idCabecera) {
//		RespComboDetalleMaestra resPayload = new RespComboDetalleMaestra();
//		List<MaestraDetalle> listarComboDetalleServir = maestraDetalleRespository.findAllCombos(idCabecera);
//		resPayload.setListaDetalles(listarComboDetalleServir);
//		return new RespBase<RespComboDetalleMaestra>().ok(resPayload);
//	}


	@Override
	public RespBase<RespComboDetalleMaestra> comboMaestraDetalleByEntidad(Long idEntidad) {
		RespComboDetalleMaestra resPayload = new RespComboDetalleMaestra();
		List<MaestraDetalle> listarComboDetalleServir = maestraDetalleRespository.findRegimenByEntidad(idEntidad);
		resPayload.setListaDetalles(listarComboDetalleServir);
		return new RespBase<RespComboDetalleMaestra>().ok(resPayload);
	}


	@Override
	public RespBase<RespListaMaestraEntidad> obtenerMaestraPorEntidad(Long entidad, Long regimen, Long modalidad) {
		
		RespListaMaestraEntidad payload = new RespListaMaestraEntidad(); 
		List<MaestraDetalleDto> lst = null;
		
		if (!Util.isEmpty(entidad) && Util.isEmpty(regimen) && Util.isEmpty(modalidad)) {
			List<MasterEvaluacionDTO> listRegimen = evaluacionRepositoryJdbc.listMasterEvaluacionDtos2(entidad, regimen, modalidad, null);
			lst = listRegimen.stream()
			.map(maestra -> {
				MaestraDetalleDto dto = new MaestraDetalleDto(); 
				Optional<MaestraDetalle> optRegimen = maestraDetalleRespository.findById(maestra.getCodigoNivel1());
				dto.setMaestraDetalleId(maestra.getCodigoNivel1());
				dto.setMaeDetalleId(maestra.getCodigoNivel1());
				dto.setDescripcion(maestra.getDescripcionCorta());
				optRegimen.ifPresent(val -> {
					dto.setCodProg(val.getCodProg());	
				});
				return dto;
			}).collect(Collectors.toList());
			
			payload.setLstMaestraEntidad(lst);
			return new RespBase<RespListaMaestraEntidad>().ok(payload);
		}
		if (!Util.isEmpty(entidad) && !Util.isEmpty(regimen) && Util.isEmpty(modalidad)) {
			List<ModalidadDTO> listModalidad = evaluacionRepositoryJdbc.listMasterEvaluacionDtos3(entidad, regimen, null, null);
			lst = listModalidad.stream()
					.map(maestra ->{
						MaestraDetalleDto dto = new MaestraDetalleDto(); 
						Optional<MaestraDetalle> optModalidad = maestraDetalleRespository.findById(maestra.getCodNivel2());
						dto.setMaestraDetalleId(maestra.getCodNivel2());
						dto.setMaeDetalleId(maestra.getCodNivel2());
						dto.setDescripcion(maestra.getDescripcionCorta());
						optModalidad.ifPresent(val -> {
							dto.setCodProg(val.getCodProg());	
						});						
						return dto;
					}).collect(Collectors.toList());
			payload.setLstMaestraEntidad(lst);
			return new RespBase<RespListaMaestraEntidad>().ok(payload);
		}
		if (!Util.isEmpty(entidad) && !Util.isEmpty(regimen) && !Util.isEmpty(modalidad)) {
			List<TipoDTO> listaTipo = evaluacionRepositoryJdbc.listMasterEvaluacionDtos4(entidad, regimen, modalidad, null);
			lst = listaTipo.stream()
					.map(maestra -> {
						MaestraDetalleDto dto = new MaestraDetalleDto(); 
						Optional<MaestraDetalle> optModalidad = maestraDetalleRespository.findById(maestra.getCodigoNivel3());
						dto.setMaestraDetalleId(maestra.getCodigoNivel3());
						dto.setMaeDetalleId(maestra.getCodigoNivel3());
						dto.setDescripcion(maestra.getDescripcionCorta());
						optModalidad.ifPresent(val -> {
							dto.setCodProg(val.getCodProg());	
						});						
						return dto;
					}).collect(Collectors.toList());
			payload.setLstMaestraEntidad(lst);
			return new RespBase<RespListaMaestraEntidad>().ok(payload);
		}
		
		   return new RespBase<RespListaMaestraEntidad>().ok(payload);
	}


//	@Override
//	public RespBase<RespMaestraDetalleAll> obtenerEtapas() {
//		 
//		RespMaestraDetalleAll payload = new RespMaestraDetalleAll();
//		Long etapaid= new Long(25);
//		payload.setMaestraDetalle(maestraDetalleRespository.findAllCombos(etapaid));
//		 
//		return new RespBase<RespMaestraDetalleAll>().ok(payload);
//	}


//	@Override
//	public RespBase<RespComboDetalleMaestra> comboMaestraDetalleTipoInforme() {
//		RespComboDetalleMaestra resPayload = new RespComboDetalleMaestra();
//		List<MaestraDetalle> listarComboDetalleServir = maestraDetalleRespository.findAllCombos(Constantes.COD_MAE_CABECERA_TIPOINFORME);
//		List<MaestraDetalle> listMaeDetalle = new ArrayList<>();
////		for (MaestraDetalle maestraDetalle : listarComboDetalleServir) {
////			if(!Util.isEmpty( getTipoInformeRequerido().get(maestraDetalle.getReferencia())  ) ) {
////				listMaeDetalle.add(maestraDetalle);
////			}
////		}
//		resPayload.setListaDetalles(listarComboDetalleServir);
//		return new RespBase<RespComboDetalleMaestra>().ok(resPayload);
//	}
//	
//	public Map<String,String> getTipoInformeRequerido(){
//		Map<String,String> map = new HashMap<>();
//		map.put("BONIFICACION", "BONIFICACION");
//		map.put("DECLARACIONDESIERTO", "DECLARACIONDESIERTO");
//		map.put("IMPEDIMENTO", "IMPEDIMENTO");
//		map.put("CRITERIO_DE_ELECCION", "CRITERIO_DE_ELECCION");
//		return map;
//	}

}
