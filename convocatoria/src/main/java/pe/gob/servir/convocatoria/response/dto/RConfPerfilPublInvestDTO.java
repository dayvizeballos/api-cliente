package pe.gob.servir.convocatoria.response.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class RConfPerfilPublInvestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<DetalleInvestigacionPublicacion> lstInvestigacion;
	private List<DetalleInvestigacionPublicacion> lstPublicacion;
	private List<EspecifInvestigacionPublicacion> lstEspecifInvestigacion;
	private List<EspecifInvestigacionPublicacion> lstEspecifPublicacion;
	
	@Data
	@AllArgsConstructor
	public  static class  DetalleInvestigacionPublicacion  implements Serializable{
		
		private static final long serialVersionUID = 1L;
		private Long configId;
		private Long tipoId;
		private String tipoDescripcion;
		private String descripcion;
		private Boolean requisito;
		private Long baseId;
		
		public DetalleInvestigacionPublicacion() {
			
		}
	}
	
	@Data
	@AllArgsConstructor
	public  static class  EspecifInvestigacionPublicacion  implements Serializable{
		
		private static final long serialVersionUID = 1L;
		private Long configId;
		private Long tipoId;
		private String tipoDescripcion;
		private Integer cantidad;
		private Boolean superior;
		private BigDecimal puntaje;
		private Long baseId;
	}
}
