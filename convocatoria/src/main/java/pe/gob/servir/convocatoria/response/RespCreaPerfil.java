package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.Perfil;

@Getter
@Setter
public class RespCreaPerfil {

	
	private Perfil perfil;
}
