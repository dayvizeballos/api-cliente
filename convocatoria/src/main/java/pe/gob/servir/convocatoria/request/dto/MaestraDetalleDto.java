package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Transient;


@Getter
@Setter
public class MaestraDetalleDto {
    private Long maestraDetalleId;
    private Long maeCabeceraId;
    private String descripcion;
    private String descripcionCorta;
    private String sigla;
    private String referencia;
    private Long orden;
    private String codProg;
    private Long maeDetalleId;
}
