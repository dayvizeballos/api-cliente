package pe.gob.servir.convocatoria.model;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import java.util.List;

@Entity
@Table(name = "tbl_informe_detalle", schema = "sch_base")
@Getter
@Setter
public class InformeDetalle  extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tbl_informe_detalle")
    @SequenceGenerator(name = "seq_tbl_informe_detalle", sequenceName = "seq_tbl_informe_detalle", schema = "sch_base", allocationSize = 1)

    @Column(name = "informe_detalle_id")
    private Long informeDetalleId;

    @Column(name = "tipo_info_id")
    private Long tipoInfo;


    @Column(name = "titulo")
    private String titulo;


    @Column(name = "contenido")
    private String contenido;

    @Column(name = "entidad_id")
    private Long entidadId;

}
