package pe.gob.servir.convocatoria.repository;

import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BaseEvaluacionDetalle;

import java.util.Optional;

@Repository
public interface BaseEvaluacionDetalleRepository extends JpaRepository<BaseEvaluacionDetalle, Long> {

    @Query("select eva from BaseEvaluacionDetalle eva where eva.evaluacionEntidad.evaluacionEntidadId=:evaluacionEntidadId " +
            " and eva.baseEvaluacion.baseEvaluacionId=:baseEvaluacionId and eva.estadoRegistro = '1'" )
    Optional<BaseEvaluacionDetalle> findBaseEvaluacionDetalleByEvalEntAndEval(@Param("evaluacionEntidadId")Long evaluacionEntidadId , @Param("baseEvaluacionId")Long baseEvaluacionId);
}
