package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.BonificacionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.InformeDetalleDTO;
import pe.gob.servir.convocatoria.response.RespCriterioEvaluacion;

import java.util.List;

public interface InformeDetalleRepositoryJdbc {
    List<InformeDetalleDTO> listAllInformeDetalleDto(Long entidad , Long tipoInforme , String estado , String titulo , Long idInforme);
    List<BonificacionDTO> listAllBonificacionDto(Long idInformeDetalle);
    List<BonificacionDetalleDTO> listAllBonificacionDetalleDtos(Long idInformeDetalle);
    List<RespCriterioEvaluacion> listInformeDetalle(Long entidad , Long tipoInforme , String estado);
}
