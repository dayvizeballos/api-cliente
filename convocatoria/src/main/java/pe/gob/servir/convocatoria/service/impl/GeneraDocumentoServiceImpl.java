package pe.gob.servir.convocatoria.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

import org.springframework.stereotype.Service;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import pe.gob.servir.convocatoria.service.GeneraDocumentoService;

@Service
public class GeneraDocumentoServiceImpl implements GeneraDocumentoService {

	/**
	 * Toma la ruta de fichero y devuelve un stream abierto para su lectura.
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	@Override
	public InputStream loadDocumentAsStream(String filePath) throws IOException {
		URL url = new File(filePath).toURI().toURL();
		InputStream documentAsStream = url.openStream();
		return documentAsStream;
	}

	/**
	 * Carga en el objeto context el valor de las variables.
	 *
	 * @return
	 * @throws XDocReportException
	 */
	@Override
	public IContext cargarVariables(Map<String, Object> variables, IContext context) throws XDocReportException {
		for (Map.Entry<String, Object> variable : variables.entrySet()) {
			context.put(variable.getKey(), variable.getValue());
		}

		return context;
	}

	@Override
	public void cargarImagenes(IXDocReport report, Map<String, String> variables, IContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public byte[] generarDocumento(String rutaPlantilla, TemplateEngineKind templateEngine,
			Map<String, Object> variablesMap) throws IOException, XDocReportException {

		// Cargar el fichero y configurar el Template Engine
		InputStream inputStream = loadDocumentAsStream(rutaPlantilla);
		IXDocReport xdocReport = XDocReportRegistry.getRegistry().loadReport(inputStream, templateEngine);

		// Se cargan los metadatos
		//xdocReport.setFieldsMetadata(metadatos);

		// Se crea el contexto y se cargan las variables de reemplazo
		IContext context = xdocReport.createContext();
		cargarVariables(variablesMap, context);
		//cargarImagenes(xdocReport, imagenesPathMap, context);

		// Se genera la salida a partir de la plantilla.
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		// Se genera el documento remplazando las variables manteniendo el
		// formato original.
		xdocReport.process(context, out);

		return out.toByteArray();

	}

}
