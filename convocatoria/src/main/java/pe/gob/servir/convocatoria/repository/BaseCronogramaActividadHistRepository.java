package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pe.gob.servir.convocatoria.model.BaseCronogramaActividadHist;
import pe.gob.servir.convocatoria.model.BaseCronogramaHist;

import java.util.List;


public interface BaseCronogramaActividadHistRepository extends JpaRepository<BaseCronogramaActividadHist, Long> {
  List<BaseCronogramaActividadHist> findByBaseCronogramaHistAndEstadoRegistro(BaseCronogramaHist bch , String estado);
}
