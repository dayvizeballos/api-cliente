package pe.gob.servir.convocatoria.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;
import pe.gob.servir.convocatoria.common.EstadoRegistro;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbl_bonificacion", schema = "sch_base")
@Getter
@Setter
public class Bonificacion extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tbl_bonificacion")
    @SequenceGenerator(name = "seq_tbl_bonificacion", sequenceName = "seq_tbl_bonificacion", schema = "sch_base", allocationSize = 1)

    @Column(name = "bonificacion_id")
    private Long bonificacionId;

    @Column(name = "tipo_info_id")
    private Long tipoInfoId;
    
    @Column(name = "tipo_bonificacion")
    private Long tipoBonificacion;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "contenido")
    private String contenido;
    
    @OneToMany(cascade= CascadeType.ALL ,  mappedBy = "bonificacion")
    @Column(nullable = true)
    @JsonManagedReference
    private List<BonificacionDetalle> bonificacionDetalleList;
    
    public Bonificacion(Long BonificacionId){
    	this.bonificacionId = BonificacionId;
    	this.estadoRegistro = EstadoRegistro.ACTIVO.getCodigo();
    }
    
    public Bonificacion(){
    	
    }
}
