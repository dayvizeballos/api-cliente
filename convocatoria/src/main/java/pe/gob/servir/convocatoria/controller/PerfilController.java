package pe.gob.servir.convocatoria.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.lowagie.text.DocumentException;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaFormacionAcademica;
import pe.gob.servir.convocatoria.request.ReqCreaPerfil;
import pe.gob.servir.convocatoria.request.ReqCreaPerfilFuncion;
import pe.gob.servir.convocatoria.request.dto.ObtenerPerfilConfigDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerPerfilDTO;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.response.dto.RespPerfilDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.PerfilService;
import pe.gob.servir.convocatoria.service.ReportePerfilService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;


@RestController
@Tag(name = "Perfil", description = "")
public class PerfilController {
	
	
	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private PerfilService perfilService;
	
	@Autowired
	private ReportePerfilService reporteService;
	
	@Operation(summary = "Crea un perfil", description = "Crea un perfil", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/perfil" }, consumes = { 
				MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespCreaPerfil>> registrarPerfil(@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqCreaPerfil> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespCreaPerfil> response = perfilService.guardarPerfil(request, jwt, null);
		return ResponseEntity.ok(response);
		}
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"perfiles", description = Constantes.SUM_OBT_LIST+"perfiles", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/perfil/filter/{idEntidad}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<ObtenerPerfilDTO>>> obtenerPerfil(
			@PathVariable String access,
			@PathVariable Long idEntidad,
			@RequestParam(value = "codigoPuesto", required = false) String codigoPuesto,
			@RequestParam(value = "nombrePerfil", required = false) String nombrePerfil,
			@RequestParam(value = "regimenId", required = false) Long regimenId,
			@RequestParam(value = "organoId", required = false) Long organoId,
			@RequestParam(value = "unidadOrganicaId", required = false) Long unidadOrganicaId,
			@RequestParam(value = "fechaIni", required = false) String fechaIni,
	        @RequestParam(value = "fechaFin", required = false) String fechaFin,
	        @RequestParam(value = "origenPerfil", required = false) String origenPerfil,
	        @RequestParam(value = "estadoRevision", required = false) Long estadoRevision,
			@RequestParam(value = "estado", defaultValue = "1" , required = false) String estado) {
		RespBase<RespObtieneLista<ObtenerPerfilDTO>> response = perfilService.obtenerPerfil(fechaIni , fechaFin , idEntidad,codigoPuesto, nombrePerfil ,
				regimenId , organoId , unidadOrganicaId , origenPerfil , estadoRevision, estado);
		return ResponseEntity.ok(response);		
	}

	@Operation(summary = "Cambiar estado perfil ", description = "Cambiar estado perfil", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/perfil" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtienePerfil>> cambiarEstadoPerfil(@PathVariable String access,
																			   @RequestParam(value = "perfilId", required = false) Long perfilId,
																			   @RequestParam(value = "estado", required = false) String estado) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespObtienePerfil> response = perfilService.cambiarEstado(perfilId , estado.trim() , jwt);
		return ResponseEntity.ok(response);
	}


	@Operation(summary = "Obtiene un Perfil Funcion", description = "Obtiene un Perfil Funcion", tags = { "" },
			security = { @SecurityRequirement(name = "bearer-key") }
	)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) })
	})
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/perfil/funcion/{perfilId}" },
			produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public ResponseEntity<RespBase<RespCreaPerfilFuncion>> obtienePerfilFuncion(
			@PathVariable String access,
			@PathVariable Long perfilId) {
		RespBase<RespCreaPerfilFuncion> response = perfilService.findPerfilFuncion(perfilId);
		return ResponseEntity.ok(response);
	}


	@Operation(summary = "Crea un perfil funcion", description = "Crea un perfil funcion", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/funcion" }, consumes = { 
				MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespCreaPerfilFuncion>> registrarPerfilFuncion(@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqCreaPerfilFuncion> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespCreaPerfilFuncion> response = perfilService.guardarPerfilFuncion(request, jwt, null);
		return ResponseEntity.ok(response);
		}


	@Operation(summary = "Actualizar un perfil funcion", description = "Actualizar un perfil funcion", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/funcion" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespCreaPerfilFuncion>> actualizarPerfilFuncion(@PathVariable String access,
																				  @Valid @RequestBody ReqBase<ReqCreaPerfilFuncion> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespCreaPerfilFuncion> response = perfilService.actualizarPerfilFuncion(request, jwt, null);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = "Crea una Formacion Academica", description = "Crea una Formacion Academica", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/formacionAcademica" }, consumes = { 
				MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<ReqCreaFormacionAcademica>> registrarFormacionAcademica(@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqCreaFormacionAcademica> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<ReqCreaFormacionAcademica> response = perfilService.guardarFormacionAcademica(request, jwt);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Obtiene una perfil", description = "Obtiene un perfil", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/perfil/{perfilId}" }, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtienePerfil>> obtienePerfilById(
			@PathVariable String access, 
			@PathVariable Long perfilId) {
		RespBase<RespObtienePerfil> response = perfilService.obtienePerfilById(perfilId);
		return ResponseEntity.ok(response);

	}
	
	@Operation(summary = "Actualiza un perfil", description = "Actualiza un perfil", tags = { "" },security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/{perfilId}" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespCreaPerfil>> actualizarPerfil(@PathVariable String access,
			@PathVariable Long perfilId, @Valid @RequestBody ReqBase<ReqCreaPerfil> request){
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespCreaPerfil> response = perfilService.guardarPerfil(request, jwt, perfilId);
		return ResponseEntity.ok(response);	
	
	}
	
	
	@Operation(summary = "Obtiene Formacion Academica", description = "Obtiene Formacion Academica", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/perfil/formacionAcademica/{perfilId}" }, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<ReqCreaFormacionAcademica>> obtieneFormacionAcademicaByPerfil(
			@PathVariable String access, 
			@PathVariable Long perfilId) {
		RespBase<ReqCreaFormacionAcademica> response = perfilService.obtieneFormacionAcademicaById(perfilId);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = Constantes.SUM_OBT_LIST + "configurar perfiles", description = Constantes.SUM_OBT_LIST + "configurar perfiles", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/perfil/buscar"},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RespBase<RespObtieneLista<ObtenerPerfilConfigDTO>>> obtenerPerfiles(
			@PathVariable String access,
			@RequestParam(value = "entidadId", required = true) Long entidadId,
			@RequestParam(value = "nombrePerfil", required = false) String nombrePerfil,
			@RequestParam(value = "regimenId", required = false) Long regimenId,
			@RequestParam(value = "rolId", required = false) Long rolId,
			@RequestParam(value = "estadoRmId", required = false) Long estadoRmId,
			@RequestParam(value = "estadoEcId", required = false) Long estadoEcId) {
		RespBase<RespObtieneLista<ObtenerPerfilConfigDTO>> response = null;
		Map<String, Object> parametroMap = new HashMap<>();
		parametroMap.put("entidadId", entidadId);
		parametroMap.put("regimenId", regimenId);
		parametroMap.put("nombrePerfil", nombrePerfil);
		parametroMap.put("rolId", rolId);
		parametroMap.put("estadoRmId", estadoRmId);
		parametroMap.put("estadoEcId", estadoEcId);

		response = perfilService.buscarPerfilByFilter(parametroMap);

		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = Constantes.SUM_OBT_LIST + "perfiles por convocatoria", description = Constantes.SUM_OBT_LIST + "perfiles por convocatoria", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/perfil/convocatoria/{convocatoriaId}"},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RespBase<RespObtieneLista<RespPerfilDTO>>> obtenerPerfiles(
			@PathVariable String access, @PathVariable Long convocatoriaId) {
		RespBase<RespObtieneLista<RespPerfilDTO>> response = null;
		response = perfilService.listarPerfilByConvocatoria(convocatoriaId);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Generar Pdf Perfil", description = "Generar Pdf Perfil", tags = {""},
			   security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", 
			content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/pdf/{perfilId}" })
	public @ResponseBody ResponseEntity<RespBase<String>> generarPdfPerfil
						   (@PathVariable String access, @PathVariable Long perfilId) throws DocumentException, IOException {
		
		byte[] bytes = reporteService.generatePdfFromHtml(perfilId);
     String encodedString =
             Base64.getEncoder().withoutPadding().encodeToString(bytes);
     RespBase<String> payload = new RespBase<>();
     payload.setPayload(encodedString);
     
		return ResponseEntity.ok(payload);
		 
	}
	
	@Operation(summary = "Cambiar estado revision ", description = "Cambiar estado revision", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/estadorevision" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtienePerfil>> cambiarEstadoRevisionPerfil(@PathVariable String access,
																			   @RequestParam(value = "perfilId", required = false) Long perfilId,
																			   @RequestParam(value = "estadoRevision", required = false) Long estado) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespObtienePerfil> response = perfilService.cambiarEstadoRevision(perfilId , estado , jwt);
		return ResponseEntity.ok(response);
	}
	
}
