package pe.gob.servir.convocatoria.repository;

import java.util.List;

import pe.gob.servir.convocatoria.request.dto.DetalleEvaluacionEntidadDTO;

public interface EvaluacionEntidadRepositoryJdbc {

	List<DetalleEvaluacionEntidadDTO> buscarEEntidad(Long idEntidad, Long idJerarquia);
}
