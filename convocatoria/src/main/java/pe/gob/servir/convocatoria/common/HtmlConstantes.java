package pe.gob.servir.convocatoria.common;

public class HtmlConstantes {
	
	private HtmlConstantes() {
		super();
	}
	
	public static final String CLOSE_BODY_TAG = "</body>";
	public static final String TAG_BR = "<br></br>";
	public static final String OPEN_TR_TAG = "<tr>";
	public static final String CLOSE_TR_TAG = "</tr>";
	public static final String OPEN_TD_TAG = "<td>";
	public static final String CLOSE_TD_TAG = "</td>";
	public static final String OPEN_TH_TAG = "<th>";
	public static final String CLOSE_TH_TAG = "</th>";
	public static final String OPEN_P_STYLE_TAG = "<p style=\" {0} \" >";
	public static final String OPEN_P_TAG = "<p>";
	public static final String CLOSE_P_TAG = "</p>";
	public static final String OPEN_STRONG_TAG = "<strong>";
	public static final String CLOSE_STRONG_TAG = "</strong>";
	public static final String CENTER = "center";
	public static final String DEFAULT_LOGO ="src/main/resources/nologo.png";
	public static final String IMG_SRC = "<img src='{0}'  width=\"200px\" height=\"80px\"></img>";

}
