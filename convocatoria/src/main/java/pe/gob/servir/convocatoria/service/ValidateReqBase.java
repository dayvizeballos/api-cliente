package pe.gob.servir.convocatoria.service;

import java.util.Objects;
import java.util.function.Function;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import pe.gob.servir.convocatoria.request.ReqGuardarInformacionComp;

public class ValidateReqBase  {
	
	private ValidateReqBase() {
		super();
	}
	
	 public static final Function< ReqGuardarInformacionComp , String> validatePayloadInformacionComp = (ReqGuardarInformacionComp r ) -> {
		     String response = StringUtils.EMPTY;
	    	 if (Objects.isNull(r))
	             response = "Informacion complemetaria es nulo";
	             
	    	 if((CollectionUtils.isEmpty(r.getInformes())))
	        	response = "Debe de ingresar informes";
	        
	        return response;
	    };

}
