package pe.gob.servir.convocatoria.service;

import java.io.IOException;

import pe.gob.servir.convocatoria.response.RespBase;

public interface ExcelService {

	RespBase<String> descargarPlantillaRegimen30057(Long idEntidad)  throws IOException ;
	
	RespBase<String> descargarPlantillaOtrosRegimen(Long idEntidad)  throws IOException ;
	
	RespBase<String> descargarPlantillaPracticas(Long idEntidad)  throws IOException ;
}
