package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.CarreraProfesional;

import java.util.Collection;
import java.util.List;

@Repository
public interface CarreraProfesionalRepository extends JpaRepository<CarreraProfesional, Long> {

    List<CarreraProfesional> findByDescripcionInAndEstadoRegistroAndIdDetalle(Collection<String> ages , String estado , Long tipo);


}
