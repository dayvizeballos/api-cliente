package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@Getter
@Setter
public class ReqMaestraDetalleEntidad {

	@NotNull(message = Constantes.CAMPO + " entidadId " + Constantes.ES_OBLIGATORIO)
	private Long entidadId;
	
	@NotNull(message = Constantes.CAMPO + " maeCabeceraEntidadId " + Constantes.ES_OBLIGATORIO)
	private Long maeCabeceraEntidadId;
	
	@NotNull(message = Constantes.CAMPO + " descripcion " + Constantes.ES_OBLIGATORIO)
	@Size(max = 250, message = Constantes.CAMPO + " nombreCompletoEntidad " + Constantes.ES_INVALIDO + ", máximo 250 ")
	private String nombreCompletoEntidad;
	
	@Size(max = 40, message = Constantes.CAMPO + " nombreCortoEntidad " + Constantes.ES_INVALIDO + ", máximo 40 ")
	private String nombreCortoEntidad;
	
	@Size(max = 15, message = Constantes.CAMPO + " siglaEntidad " + Constantes.ES_INVALIDO + ", máximo 15 ")
	private String siglaEntidad;
	
	@Size(max = 100, message = Constantes.CAMPO + " referenciaEntidad " + Constantes.ES_INVALIDO + ", máximo 100 ")
	private String referenciaEntidad;
	
	@NotNull(message = Constantes.CAMPO + " estado " + Constantes.ES_OBLIGATORIO)
	@Pattern(regexp = "1|0", message = Constantes.CAMPO + " estado " + Constantes.ES_INVALIDO + ", valores permitidos 1(ACTIVO), 0(INACTIVO)")
	private String estado;
}
