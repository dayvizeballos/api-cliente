package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.VacanteDTO;

@Getter
@Setter
public class RespListaVacante {

	private Long vacantesDisponibles;
	private List<VacanteDTO> items;
}
