package pe.gob.servir.convocatoria.response;

import lombok.Data;

@Data
public class RespEtapaActualVacanteBase {

	private Long etapaId;
	private String etapa;
	
	private Integer vacante;
	
	private String fechaInicio;
	private String fechaFin;
	
}
