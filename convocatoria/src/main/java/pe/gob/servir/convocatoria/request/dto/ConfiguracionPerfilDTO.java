package pe.gob.servir.convocatoria.request.dto;

import lombok.Data;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPublInvestDTO.DetalleInvestigacionPublicacion;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPublInvestDTO.EspecifInvestigacionPublicacion;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class ConfiguracionPerfilDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = Constantes.CAMPO + " listConfigPerfFormacCarrera " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<ConfigPerfFormacCarrera> listConfigPerfFormacCarrera;

    @NotNull(message = Constantes.CAMPO + " listConfigPerfFormacEspecial " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<ConfigPerfFormacEspecial> listConfigPerfFormacEspecial;

    @NotNull(message = Constantes.CAMPO + " listConfigPerfFormacEspecif " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<ConfigPerfFormacEspecif> listConfigPerfFormacEspecif;

    @NotNull(message = Constantes.CAMPO + " listConfigPerfExpeLaboral " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<ConfigPerfExpeLaboral> listConfigPerfExpeLaboral;

    @NotNull(message = Constantes.CAMPO + " listConfigPerfOtrosRequisitos " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<ConfigPerfOtrosRequisitos> listConfigPerfOtrosRequisitos;

    @NotNull(message = Constantes.CAMPO + " listConfigPerfDeclaraJurada " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<ConfigPerfDeclaraJurada> listConfigPerfDeclaraJurada;

    @NotNull(message = Constantes.CAMPO + " listaConfigPerfPesoSeccion " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<ConfigPerfPesoSeccion> listaConfigPerfPesoSeccion;
    
    @NotNull(message = Constantes.CAMPO + " listaConfigOtrosGrados " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<ConfigOtrosGrados> listaConfigOtrosGrados;
    
    @NotNull(message = Constantes.CAMPO + " listaConfigInvestigacion " + Constantes.ES_OBLIGATORIO)
    @Valid
    private List<DetalleInvestigacionPublicacion> listaConfigInvestigacion;
    
    @NotNull(message = Constantes.CAMPO + " listaConfigPublicacion " + Constantes.ES_OBLIGATORIO)
    @Valid
	private List<DetalleInvestigacionPublicacion> listaConfigPublicacion;
    
    @NotNull(message = Constantes.CAMPO + " listaConfigEspecifInvestigacion" + Constantes.ES_OBLIGATORIO)
    @Valid
	private List<EspecifInvestigacionPublicacion> listaConfigEspecifInvestigacion;
    
    @NotNull(message = Constantes.CAMPO + " listaConfigEspecifPublicacion " + Constantes.ES_OBLIGATORIO)
    @Valid
	private List<EspecifInvestigacionPublicacion> listaConfigEspecifPublicacion;
    
    @NotNull(message = Constantes.CAMPO + " modelo para configuracion de perfiles " + Constantes.ES_OBLIGATORIO)
    @Valid
    private ConfigPerfiles  configPerfiles;

    @Data
    public static class ConfigPerfDeclaraJurada implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfDeclaracionId;
        private Long perfilId;
        private String descripcion;
        private Boolean requisitoMinimo;
        private Long flagDDJJ;
        private Long declaracionId;
        private Long tipoId;
        private Long orden;
        private String estado;
        private String isServir;
        private Long baseId;
    }

    @Data
    public static class ConfigPerfExpeLaboral implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfExpeLaboralId;
        private Long perfilId;
        private Long tipoExperiencia;
        private String nivelAcademico;
        private Long nivelEducativoId;
        private String nivelEducativoDesc;
        private Long situacionAcademicaId;
        private String situacionAacademicaDesc;
        private Long desdeAnios;
        private Long hastaAnios;
        private Boolean requisitoMinimo;
        private Long puntaje;
        private Long baseId;
        private List<ConfigPerfilExperienciaLaboralDetalleDTO> listModeloDetalleExperiencia;
    }

    @Data
    public static class ConfigPerfFormacCarrera implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfFormacCarreraId;
        private Long perfilId;
        private Long nivelEducativoId;
        private String descripcionAcademica;
        private String descripcionNivelEducativo;
        private Boolean requisitoMinimo;
        private Integer puntaje;
        private Long situacionAcademicaId;
        private String descripcionCarrera;
        private String carreraIds;
        private Long baseId;
    }

    @Data
    public static class ConfigPerfFormacEspecial implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfFormacEspecialId;
        private Long perfilId;
        private Long tipoConocimientoId;
        private String descripcionTipo;
        private Long conocimientoId;
        private String descripcionConocimiento;
        private Boolean requisitoMinimo;
        private Integer puntaje;
        private Long baseId;
    }

    @Data
    public static class ConfigPerfFormacEspecif implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfFormacEspecifId;
        private Long perfilId;
        private Long tipoEspecificacion;
        private Long cantidad;
        private Long puntaje;
        private String descripcion;
        private Boolean requisitoMinimo;
        private Long baseId;
    }

    @Data
    public static class ConfigPerfOtrosRequisitos implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfOtrosRequisitosId;
        private Long perfilId;
        private String descripcion;
        private Boolean requisitosAdicionales;
        private Long experienciaDetalleId;
        private Long tipoDatoExperId;
        private Long requisitoId;
        private Long perfilExperienciaId;
        private Long baseId;
    }

    @Data
    public static class ConfigPerfPesoSeccion implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfSeccionId;
        private Long idPerfil;
        private Long tipoSeccion;
        private Long pesoSeccion;
        private String descripcionSeccion;
        private Long baseId;
    }
    
    
    
    @Data
    public static class ConfigOtrosGrados implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfFormacCarreraDetalleId;
        private Long perfilId;
        private Long grado;
        private Long  puntaje;
        private Long baseId;
    }

    @Data
    public static class ConfigPerfiles implements Serializable {
        private static final long serialVersionUID = 1L;
    	private Long configPerfilId;
    	private Long perfilId;
    	private Long tipoPerfil;
    	private String descripcion;
        private Long baseId;
    }

}
