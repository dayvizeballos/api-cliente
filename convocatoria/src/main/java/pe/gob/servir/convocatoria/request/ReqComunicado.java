package pe.gob.servir.convocatoria.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.servir.convocatoria.common.Constantes;

import javax.validation.constraints.NotNull;

@Data
public class ReqComunicado {
    @NotNull(message = Constantes.CAMPO + " convocatoriaId " + Constantes.ES_OBLIGATORIO)
    private Long convocatoriaId;

    private Long perfilId;

    @NotNull(message = Constantes.CAMPO + " etapaId " + Constantes.ES_OBLIGATORIO)
    private Long etapaId;

    private Long estadoId;

    @NotNull(message = Constantes.CAMPO + " tipoComunicadoId " + Constantes.ES_OBLIGATORIO)
    private Long tipoComunicadoId;

    private Long gestorId;

    private Long coordinadorId;

    private String nombreGestor;

    private String nombreCoordinador;

    private String estadoRegistro;

    private MultipartFile[] files;

    private String pdfBase64;

    private Long comunicadoId;

}
