package pe.gob.servir.convocatoria.request.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ParametrosDTO {
    private Long parametroId;
    private String tipoParametro;
    private String codigoTexto;
    private Long codigoNumero;
    private String valorTexto;
    private BigDecimal valorNumero;
    private Date valorFecha;
    private String descripcion;
}
