package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.model.DeclaracionJurada;
import pe.gob.servir.convocatoria.model.RequisitoGeneral;
import pe.gob.servir.convocatoria.request.dto.DeclaracionJuradaDTO;
import pe.gob.servir.convocatoria.request.dto.RequisitoGeneralDTO;
import pe.gob.servir.convocatoria.util.Util;

import java.util.function.Function;

@FunctionalInterface
public interface RequisitoGeneralConvert {
    public void main();
    Function<RequisitoGeneralDTO, RequisitoGeneral> converDtoRequisitoGeneral = (RequisitoGeneralDTO p) -> {
        RequisitoGeneral requisitoGeneral = new RequisitoGeneral();
       // requisitoGeneral.setRequisitoId(p.getRequisitoId());
        //requisitoGeneral.setCuestionario(p.getCuestionario());
        //requisitoGeneral.setDescripcion(p.getDescripcion());
        //requisitoGeneral.setInformeDetalleId(p.getInformeDetalleId());
        return requisitoGeneral;
    };

    Function<DeclaracionJuradaDTO, DeclaracionJurada> converDtoDeclaracionJurada = (DeclaracionJuradaDTO p) -> {
        DeclaracionJurada declaracionJurada = new DeclaracionJurada();
        declaracionJurada.setDeclaracionId(p.getDeclaracionId());
        declaracionJurada.setIsServir(p.getIsServir());
        declaracionJurada.setOrden(p.getOrden());
        declaracionJurada.setTipoId(p.getTipoId());
        return declaracionJurada;
    };

}
