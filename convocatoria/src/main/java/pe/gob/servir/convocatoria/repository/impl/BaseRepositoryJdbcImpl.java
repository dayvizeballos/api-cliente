package pe.gob.servir.convocatoria.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.repository.BaseRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.*;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BaseRepositoryJdbcImpl  implements BaseRepositoryJdbc {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BaseRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_FIND_BASE_CONDICION = " SELECT tb.base_id as baseId ";

    private static final String SQL_FIND_BASE_ENTIDADES_IDS = "select distinct entidad_id from sch_base.tbl_base tb where tb.estado_registro = '1' and entidad_id is not null ";

    private static final String SQL_FIND_COD_CONVOCATORIA = //"select case when  max(tb.correlativo) is null then 1 else max(tb.correlativo) + 1 end as \n" +
    		"select case when  max(tb.correlativo) is null then lpad(1::text,5,'0') else lpad((max(tb.correlativo) + 1)::text,5,'0') end as \n" +
    		//"select case when  max(tb.correlativo) is null then '0000000001' else max(tb.correlativo) end as \n" +
            "correlativo  from sch_base.tbl_base tb where tb.anio = :anio and regimen_id = :regimen and tb.entidad_id = :entidad and estado_registro = '1' ";

    private static final String SQL_FIND_BASE_CORREOS = "select row_number() OVER (order by b.fecha_creacion ) as rnum , b.entidad_id as entidadId,  b.base_id as baseId , b.etapa_id as etapaId , b.codigo_convocatoria as codigoConvocatoria  , " +
            " TO_CHAR(b.fecha_creacion :: DATE, 'dd/mm/yyyy') as fechaCreacion  , TO_CHAR(b.fecha_publicacion :: DATE, 'dd/mm/yyyy') as fechaPublicacion,  \n" +
            " b.nombre_gestor as gestor , b.nombre_coordinador as coordinador, b.regimen_id  as regimenId, \n" +
            " (select tm.descripcion from sch_convocatoria.tbl_mae_detalle tm where tm.mae_detalle_id = b.regimen_id) as regimen,  \n" +
            " (select c.nro_vacantes from sch_base.tbl_base_datos_concurso c where c.base_id = b.base_id)  as vacantes,\n" +
            " (select d.nombre from sch_base.tbl_base_datos_concurso d where d.base_id = b.base_id) as denominacion \n" +
            " from sch_base.tbl_base b where b.entidad_id = :entidadId  and b.estado_registro = '1' ";

    private static final String SQL_FIND_BASE_IS_OBSERVADO = " select count(1) from sch_base.tbl_base tb where tb.base_id = :idBase and condicion_etapa1 = '1'  \n" +
            " and condicion_etapa2 = '1'  and condicion_etapa3 = '1'  and condicion_etapa4 = '1'  and condicion_etapa5 = '1' and condicion_etapa6 = '1' ";

    private static final String SQL_FIND_MAESTRA_DETALLE = " select tmd.mae_detalle_id  as maestraDetalleId , tmd.descripcion as condicionNombre from sch_convocatoria.tbl_mae_detalle tmd  join sch_convocatoria.tbl_mae_cabecera tmc \n" +
            " on tmd.mae_cabecera_id  = tmc.mae_cabecera_id where tmd.cod_prog = :codpMaDetalle \n" +
            " and tmc.mae_cabecera_id  = (select tmc2.mae_cabecera_id  from sch_convocatoria.tbl_mae_cabecera tmc2 where tmc2.codigo_cabecera = :codCabMaeCabe) ";

    private static final  String SQL_FIND_DATOS_CONCURSO = "select estado_registro as estado , base_id as baseId ,  tipo_practica_id as tipoPracticaId , dt.dato_concurso_id as datoConcursoId, dt.nombre as nombre  , dt.objetivo as objetivo, dt.organo_responsable_id as organoResponsableId, \n" +
            "  dt.organo_encargado_id as organoEncargadoId, dt.unidad_organica_id as unidadOrganicaId , dt.correo as correo, dt.nro_vacantes as nroVacantes , dt.informe_detalle_id  as informeDetalleId, dt.informe_especifica_id  as informeEspecificaId, dt.telefono as telefono, dt.anexo as anexo\n" +
            " from sch_base.tbl_base_datos_concurso dt where dt.base_id = :base and dt.estado_registro = '1' " ;


    private static final String SQL_FIND_BASES_FILTER = "select distinct b.base_id as baseId, tc.codigo_convocatoria as codigoConvocatoria, TO_CHAR(b.fecha_creacion,'dd/mm/yyyy') as fechaConvocatoria, \r\n"
            + "	  sch_convocatoria.fnc_get_desc_mae_detalle(b.regimen_id) as regimenLaboral, d.nro_vacantes as nroVacante, \r\n"
            + "	   b.nombre_coordinador as coordinador, b.nombre_gestor as gestor, b.etapa_id as etapaId,\r\n"
            + "	   sch_convocatoria.fnc_get_desc_mae_detalle(b.etapa_id) as etapa,\r\n"
            + "	   sch_convocatoria.fnc_get_cod_mae_detalle(b.etapa_id) as codigoEtapa, b.estado_registro as estado,\r\n "
            + "	   sch_convocatoria.fnc_get_cod_mae_detalle(b.regimen_id) as codigoRegimen,\r\n"
            + "	   sch_convocatoria.fnc_get_cod_mae_detalle(b.modalidad_id) as codigoModalidad,\r\n"
            + "	   sch_convocatoria.fnc_get_cod_mae_detalle(b.tipo_id) as codigoTipo, \r\n"
            + "	   TO_CHAR(b.fecha_publicacion,'dd/mm/yyyy') as fechaPublicacion \r\n"
            + "from sch_base.tbl_base b inner join sch_base.tbl_base_datos_concurso d on d.base_id = b.base_id \r\n"
            + "left join sch_base.tbl_base_perfil p on p.base_id = b.base_id \r\n "
            + "left join sch_convocatoria.tbl_perfil pe on pe.perfil_id = p.perfil_id \r\n "
            + "left join sch_convocatoria.tbl_convocatoria tc on tc.base_id = b.base_id \r\n "
            + "where b.estado_registro = '1' and d.estado_registro = '1' ";


    @Override
    public List<DatosConcursoDTO> listDatosConcursoDtos(Long base , Long datoConcursoId) {
        List<DatosConcursoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_DATOS_CONCURSO);
            if (!Util.isEmpty(datoConcursoId)) sql.append(" and dt.dato_concurso_id = :datoConcursoId ");

            if (!Util.isEmpty(base)) objectParam.put("base", base);
            if (!Util.isEmpty(datoConcursoId)) objectParam.put("datoConcursoId", datoConcursoId);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(DatosConcursoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }



    @Override
    public List<ObtenerBaseDTO> buscarBaseByFilter(Map<String, Object> parametroMap) {
        List<ObtenerBaseDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASES_FILTER);
            if (!Util.isEmpty(parametroMap.get("entidadId"))) sql.append(" and b.entidad_id =:entidadId ");
            if (!Util.isEmpty(parametroMap.get("codigoConvocatoria"))) sql.append(" and upper(b.codigo_convocatoria) like upper('%'||:codigoConvocatoria||'%') ");
            if (!Util.isEmpty(parametroMap.get("fechaIni")) && !Util.isEmpty(parametroMap.get("fechaFin"))) sql.append(" and to_char(b.fecha_creacion , 'yyyy-mm-dd')  >= :fechaIni and  to_char(b.fecha_creacion , 'yyyy-mm-dd')  <= :fechaFin ");
            if (!Util.isEmpty(parametroMap.get("etapaId"))) sql.append(" and b.etapa_id =:etapaId ");
            if (!Util.isEmpty(parametroMap.get("regimenId"))) sql.append(" and b.regimen_id =:regimenId ");
            if (!Util.isEmpty(parametroMap.get("modalidadId"))) sql.append(" and b.modalidad_id =:modalidadId ");
            if (!Util.isEmpty(parametroMap.get("tipoId"))) sql.append(" and b.tipo_id =:tipoId ");
            if (!Util.isEmpty(parametroMap.get("practicaId"))) sql.append(" and b.practica_id =:practicaId ");
            if (!Util.isEmpty(parametroMap.get("condicionId"))) sql.append(" and p.condicion_trabajo_id =:condicionId ");
            if (!Util.isEmpty(parametroMap.get("nombrePerfil"))) sql.append(" and upper(pe.nombre_puesto) like upper('%'||:nombrePerfil||'%') ");
            if (!Util.isEmpty(parametroMap.get("tipoRol"))) {
            	if(parametroMap.get("tipoRol").equals("1")) sql.append(" and b.gestor_id =:personaId ");
            	if(parametroMap.get("tipoRol").equals("2")) sql.append(" and b.coordinador_id =:personaId ");
            }
            
            if (!Util.isEmpty(parametroMap.get("entidadId"))) objectParam.put("entidadId",parametroMap.get("entidadId"));
            if (!Util.isEmpty(parametroMap.get("codigoConvocatoria"))) objectParam.put("codigoConvocatoria",parametroMap.get("codigoConvocatoria"));
            if (!Util.isEmpty(parametroMap.get("fechaIni")) && !Util.isEmpty(parametroMap.get("fechaFin"))) objectParam.put("fechaIni",parametroMap.get("fechaIni")) ; objectParam.put("fechaFin",parametroMap.get("fechaFin"));
            if (!Util.isEmpty(parametroMap.get("etapaId"))) objectParam.put("etapaId",parametroMap.get("etapaId"));
            if (!Util.isEmpty(parametroMap.get("regimenId"))) objectParam.put("regimenId",parametroMap.get("regimenId"));
            if (!Util.isEmpty(parametroMap.get("modalidadId"))) objectParam.put("modalidadId",parametroMap.get("modalidadId"));
            if (!Util.isEmpty(parametroMap.get("tipoId"))) objectParam.put("tipoId",parametroMap.get("tipoId"));
            if (!Util.isEmpty(parametroMap.get("practicaId"))) objectParam.put("practicaId",parametroMap.get("practicaId"));
            if (!Util.isEmpty(parametroMap.get("condicionId"))) objectParam.put("condicionId",parametroMap.get("condicionId"));
            if (!Util.isEmpty(parametroMap.get("nombrePerfil"))) objectParam.put("nombrePerfil",parametroMap.get("nombrePerfil"));
            if (!Util.isEmpty(parametroMap.get("tipoRol"))) {
            	if (!Util.isEmpty(parametroMap.get("personaId"))) objectParam.put("personaId",parametroMap.get("personaId"));
            }
            sql.append("order by b.base_id desc");
            
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ObtenerBaseDTO.class));
            if(list!=null && !list.isEmpty()) {
            	int correlativo = 0;
                for(ObtenerBaseDTO d : list) {
                	correlativo +=1;
                	d.setCorrelativo(correlativo);
                }
            }
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }

    }


    private static final String SQL_FIND_DATO_BASE = "SELECT bs.base_id, bs.codigo_convocatoria, bs.rol_id, bs.etapa_id, bs.regimen_id, \n"
            + " bs.modalidad_id, bs.tipo_id, bs.practica_id, bs.gestor_id, bs.condicion_etapa1, bs.condicion_etapa2, bs.condicion_etapa3, \n"
            + " bs.condicion_etapa4, bs.condicion_etapa5, bs.condicion_etapa6, bs.comentario_etapa1,bs.comentario_etapa2, bs.comentario_etapa3, \n"
            + " bs.comentario_etapa4, bs.comentario_etapa5, bs.comentario_etapa6, bs.fecha_publicacion, bs.entidad_id, bs.comentario,bs.estado_registro \n"
            + " FROM sch_base.tbl_base bs " + " WHERE bs.estado_registro = '1' ";

    @Override
    public BaseDTO getBase(Long idBase, Long entidadId) {
        List<BaseDTO> base = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_DATO_BASE);
            if (!Util.isEmpty(idBase))sql.append(" AND bs.base_id = :idBase ");
            if (!Util.isEmpty(entidadId))sql.append(" AND bs.entidad_id = :codigoEntidad ");

            if (!Util.isEmpty(idBase))objectParam.put("idBase", idBase);
            if (!Util.isEmpty(entidadId))objectParam.put("codigoEntidad", entidadId);
            base = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam,
                    BeanPropertyRowMapper.newInstance(BaseDTO.class));
            return base.get(0);
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return null;
        }
    }

    @Override
    public boolean updateBase(Base base, Long etapa) {
        StringBuilder sql = new StringBuilder();
        int valor = 0;
        try {
				sql.append("UPDATE sch_base.tbl_base SET condicion_etapa");
	            sql.append(etapa);
	            sql.append(" = :condicion");
	            sql.append(" , fecha_modificacion = CURRENT_TIMESTAMP ");
	            if (!Util.isEmpty(base.getComentarioEtapa1())) {
	                sql.append(" ,  comentario_etapa");
	                sql.append(etapa);
	                sql.append(" = :comentario ");
	            }
             
            sql.append(" , etapa_id = :etapaId ");
            sql.append(" , usuario_modificacion = :user ");
            sql.append(" , coordinador_id = :coordinadorId ");
            sql.append(" , nombre_coordinador = :nombreCoordinador ");
            sql.append(" where base_id = :baseId ");

            Map<String, Object> objectParam = new HashMap<>();
            if (!Util.isEmpty(base.getCondicionEtapa1())) objectParam.put("condicion", base.getCondicionEtapa1());
            if (!Util.isEmpty(base.getComentarioEtapa1())) objectParam.put("comentario", base.getComentarioEtapa1());
            if (!Util.isEmpty(base.getEtapaId())) objectParam.put("etapaId", base.getEtapaId());
            if (!Util.isEmpty(base.getUsuarioModificacion())) objectParam.put("user", base.getUsuarioModificacion());
            if (!Util.isEmpty(base.getCoordinadorId())) objectParam.put("coordinadorId", base.getCoordinadorId());
            if (!Util.isEmpty(base.getNombreCoordinador())) objectParam.put("nombreCoordinador", base.getNombreCoordinador());
            if (!Util.isEmpty(base.getBaseId())) objectParam.put("baseId", base.getBaseId());
            System.out.print(sql.toString());
            valor = this.namedParameterJdbcTemplate.update(sql.toString(), objectParam);
        }catch (Exception e){
            LogManager.getLogger(this.getClass()).error(e);
            return false;
        }

        return valor > 0;
    }

    @Override
    public  List<MaestraDetalleDto>  maestraDetalle(String codpMaDetalle, String codCabMaeCabe) {
        List<MaestraDetalleDto> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_MAESTRA_DETALLE);
            if (!Util.isEmpty(codpMaDetalle)) objectParam.put("codpMaDetalle", codpMaDetalle);
            if (!Util.isEmpty(codCabMaeCabe)) objectParam.put("codCabMaeCabe", codCabMaeCabe);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(MaestraDetalleDto.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<CondicionDto> getCambioCondicion(Long baseId, Long etapa) {
        List<CondicionDto> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_CONDICION);
            sql.append(" tb.condicion_etapa");
            sql.append(etapa);
            sql.append(" as condicion , ");
            sql.append(" tb.comentario_etapa");
            sql.append(etapa);
            sql.append(" as comentario ");
            sql.append(" FROM sch_base.tbl_base tb where tb.base_id = :baseId and tb.estado_registro = '1'");
            if (!Util.isEmpty(baseId)) objectParam.put("baseId", baseId);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(CondicionDto.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public Long isObservado(Long idBase) {
        SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("idBase", idBase);
        return namedParameterJdbcTemplate.queryForObject(
                SQL_FIND_BASE_IS_OBSERVADO, namedParameters, Long.class);

    }

    @Override
    public List<Long> idEntidades() {
        List<Long> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(SQL_FIND_BASE_ENTIDADES_IDS);
            list = jdbcTemplate.queryForList(sql.toString(), Long.class);
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<CorreoDTO> listDetalleCorreo(Long entidadId , Long estadoProceso , String fechaHoy) {
        List<CorreoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BASE_CORREOS);
            if (!Util.isEmpty(estadoProceso))sql.append(" and b.etapa_id = :estadoProceso ");
            if (!Util.isEmpty(fechaHoy)) sql.append(" and TO_CHAR(b.fecha_publicacion :: DATE, 'dd/mm/yyyy') =  :fechaHoy ");

            sql.append(" order by rnum , b.fecha_creacion asc ");

            if (!Util.isEmpty(entidadId)) objectParam.put("entidadId", entidadId);
            if (!Util.isEmpty(estadoProceso)) objectParam.put("estadoProceso", estadoProceso);
            if (!Util.isEmpty(fechaHoy)) objectParam.put("fechaHoy", fechaHoy);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(CorreoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public String codigoConvocatoria(BaseDTO baseDTO) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("anio", baseDTO.getAnio())
                .addValue("regimen",baseDTO.getRegimenId())
                .addValue("entidad", baseDTO.getEntidadId());
        return namedParameterJdbcTemplate.queryForObject(
                SQL_FIND_COD_CONVOCATORIA, namedParameters, String.class);
    }

}
