package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.BasePerfilDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespBasePerfil;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface BasePerfilService {
	RespBase<RespBasePerfil> creaBasePerfil(ReqBase<BasePerfilDTO>request,MyJsonWebToken token); 
	RespBase<RespBasePerfil> obtenerBasePerfil(Long basePerfilId );
	RespBase<RespBasePerfil> actualizarBasePerfil(ReqBase<BasePerfilDTO>request,MyJsonWebToken token,Long basePerfilId);
}
