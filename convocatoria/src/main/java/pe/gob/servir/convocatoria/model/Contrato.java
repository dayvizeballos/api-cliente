package pe.gob.servir.convocatoria.model;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_contrato", schema = "sch_convocatoria")
@Getter
@Setter
public class Contrato extends AuditEntity implements AuditableEntity{
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_contrato")
    @SequenceGenerator(name = "seq_contrato", sequenceName = "seq_contrato", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "contrato_id")
    private Long contratoId;

    @Column(name = "nro_resolucion")
    private String nroResolucion;

    @Column(name = "organo_unidad_organica")
    private String  orgUnOrganica;

    @Column(name = "numero")
    private Integer numero;

    @Column(name = "anio")
    private Integer anio;

    @Column(name = "organo_titulo")
    private String organoTitulo;

    @Column(name = "tipo_doc")
    private Integer tipoDoc;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_suscripcion")
    private Date fechaSubscripcion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_resolucion")
    private Date fechaResolucion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_vinculacion")
    private Date fechaVinculacion;

    @Column(name = "entidad_id")
    private Long entidadId;

    @Column(name = "razon_social")
    private String razonSocial;

    @Column(name = "jornada_laboral")
    private String  jornalaboral;

    @Column(name = "lugar_prestacion")
    private String  lugarPrestacion;

    @Column(name = "ruc")
    private String ruc;

    @Column(name = "sede")
    private String sede;

    @Column(name = "siglas")
    private String siglas;

    @Column(name = "sub_nivel_categoria")
    private String subNivelcat;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "rol")
    private String rol;

    @Column(name = "responsable_orh")
    private String responsableOrh;

    @Column(name = "puesto_responsable_orh")
    private String puestoResponsableOrh;
    
    @Column(name = "tipo_doc_responsable")
    private Long tipoDocResponsable;
    
    @Column(name = "nro_doc_responsable")
    private String nroDocResponsable;

    @Column(name = "articulo")
    private String articulo;

    @Column(name = "nro_norma")
    private String nroNorma;

    @Column(name = "nro_posi_pue_meritos")
    private Long nroPosPueMeri;

    @Column(name = "base_perfil_id")
    private Long  basePerfilId;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "norma_apro_proy")
    private String normaAproProy;

    @Column(name = "nro_con_pub_meritos") // viene a ser codigo convocatoria?
    private String nroConPubMeritos;

    @Column(name = "nro_concurso_publico")
    private String nroConPub;

    @Column(name = "nro_informe")
    private String nroInforme;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "postulante_id")
    private Long postulanteId;

    @Column(name = "dni")
    private String dni;

    @Column(name = "nacionalidad")
    private String nacionalidad;

    @Column(name = "nivel_categoria")
    private String nivelCategoria;

    @Column(name = "familia")
    private String familia;

    @Column(name = "domicilio")
    private String domicilio;

    @Column(name = "fecha_nacimiento")
    private Date fechaNacimiento;

    @Column(name = "fecha")
    private String fecha;

    @Column(name = "area_labores")
    private String areaLabores;

    @Column(name = "centro_estudios")
    private String centroEstudios;
    
    @Column(name = "direccion_centro_estudios")
    private String direccionCentroEstudios;

    @Column(name = "direccion_labores")
    private String  direccionLabores;
    
    @Column(name = "ciclo")
    private String  ciclo;

    @Column(name = "periodo_prueba")
    private String  periodoPrueba;

    @Column(name = "compe_economica")
    private Double  compeEconomica;

    @Column(name = "puesto_auto_representativa")
    private String  puestoAutoRepresentativa;

    @Column(name = "nombre_puesto")
    private String  nombrePuesto;

    @Column(name = "nombre_organo")
    private String  nombreOrgano;

    @Column(name = "nombre_uni_organica")
    private String  nombreUniOrganica;

    @JoinColumn(name = "tipo_trabajo_id")
    @ManyToOne(optional = false)
    private MaestraDetalle  tipoTrabajoId;

    /*@Column(name = "tipo_servicio_id")
    private Long  tipoServicioId;
    */
    @JoinColumn(name = "tipo_servicio_id", nullable = true)
    @ManyToOne(optional = true)
    @JsonBackReference
    private MaestraDetalle tipoServicioId;

    @JoinColumn(name = "convocatoria_id")
    @ManyToOne(optional = false)
    private Convocatoria convocatoria;

   /* @Column(name = "tipo_tramite_id")
    private Long  tipoTramiteId;
    */
    @JoinColumn(name = "tipo_tramite_id", nullable = true)
    @ManyToOne(optional = true)
    @JsonBackReference
    private MaestraDetalle tipoTramiteId;
    
    @Column(name = "perfil_id")
    private Long  perfilId;

    @Column(name = "base_id")
    private Long  baseId;

    @Column(name = "posi_cuadro_puesto")
    private Long  posCuadPuesto;

    @Column(name = "familia_id")
    private Long  familiaId;

    @Column(name = "rol_id")
    private Long  rolId;

    @Column(name = "nivel_categoria_id")
    private Long  nivelCategoriaId;

    @Column(name = "organo_id")
    private Long  organoId;

    @Column(name = "unidad_organica_id")
    private Long  uniOrganicaId;

    @Column(name = "periodo_convenio")
    private String periodoConvenio;

    @Column(name = "nomb_repre_uni")
    private String nombRepreUni;

    @Column(name = "nro_doc_repre_uni")
    private String nroDocRepreUni;
    
    @JoinColumn(name = "estado_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private MaestraDetalle estado;

    @Column(name = "resol_respon_orh")
    private String resolResponOrh;
    
    @Column(name = "motivo_desestimiento")
    private String motivoDesestimiento;
    
    @Column(name = "archivo_desestimiento")
    private String archivoDesestimiento;
    
    @JoinColumn(name = "categoria_desestimiento_id", nullable = true)
    @ManyToOne(optional = true)
    @JsonBackReference
    private MaestraDetalle categoriaDesestimiento;

    @Column(name = "postulante_sel_id")
    private Long  postulanteSelId;
    
    @Column(name = "tipo_practica")
    private String tipoPractica;
    
    @Column(name = "tipo_practica_id")
    private Long tipoPracticaId;

    @JoinColumn(name = "regimen_id")
    @ManyToOne(optional = false)
    private MaestraDetalle  regimenId;
    
    @Column(name = "archivo_suscrito")
    private String archivoSuscrito;
    
    @Column(name = "fecha_ini_practica")
    private Date fechaIniPractica;
    
    @Column(name = "fecha_fin_practica")
    private Date fechaFinPractica;

    @Column(name = "puesto_repre_uni")
    private String puestoRepreUni;
    
    @Column(name = "tipo_doc_uni")
    private Long tipoDocRepreUni;
    
    @Column(name = "hora_ini_practica")
    private String horaIniPractica;
    
    @Column(name = "hora_fin_practica")
    private String horaFinPractica;
    
    @Column(name = "ruc_centro_estudios")
    private String rucCentroEstudios;
    
	@JoinColumn(name = "tipo_contrato_id")
	@ManyToOne(optional = false)
	private TipoContrato tipoContratoId;
}

