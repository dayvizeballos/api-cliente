package pe.gob.servir.convocatoria.repository;

import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import pe.gob.servir.convocatoria.model.Comunicado;
import pe.gob.servir.convocatoria.model.Convocatoria;

import java.util.List;

public interface ComunicadoRepository extends JpaRepository<Comunicado, Long> , JpaSpecificationExecutor<Comunicado>{

    @Query("select c "
            + "from Comunicado c where c.perfil.perfilId = :perfil and c.convocatoria.base.baseId = :base and c.estadoRegistro = '1' and c.fechaPublicacion is not null ")
    List<Comunicado> findComunicados(@Param("perfil") Long perfil, @Param("base")Long base);

    List<Comunicado> findByConvocatoria(Convocatoria convocatoria);
}
