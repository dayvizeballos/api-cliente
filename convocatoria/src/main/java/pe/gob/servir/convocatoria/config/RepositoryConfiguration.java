package pe.gob.servir.convocatoria.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		entityManagerFactoryRef = "convocatoriaEntityManagerFactory", 
		transactionManagerRef = "convocatoriaTransactionManager", 
		basePackages = { "pe.gob.servir.convocatoria.repository" })
public class RepositoryConfiguration {

	@Primary
	@Bean(name = "convocatoriaDataSourceProperties")
	@ConfigurationProperties("convocatoria.datasource")
	public DataSourceProperties dataSourceProperties() {
		return new DataSourceProperties();
	}

	@Primary
	@Bean(name = "convocatoriaDataSource")
	@ConfigurationProperties("convocatoria.datasource.configuration")
	public DataSource dataSource(
			@Qualifier("convocatoriaDataSourceProperties") DataSourceProperties convocatoriaDataSourceProperties) {
		return convocatoriaDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Primary
	@Bean(name = "convocatoriaEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("convocatoriaDataSource") DataSource convocatoriaDataSource) {

		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		properties.put("hibernate.session_factory.session_scoped_interceptor",
				"pe.gob.servir.convocatoria.audit.AuditEntityInterceptor");
		properties.put("hibernate.proc.param_null_passing", "true");
		properties.put("hibernate.show_sql", "true");
		properties.put("tomee.jpa.factory.lazy", "true");


		return builder.dataSource(convocatoriaDataSource).packages("pe.gob.servir.convocatoria.model")
				.persistenceUnit("convocatoria").properties(properties).build();
	}

	@Primary
	@Bean(name = "convocatoriaTransactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("convocatoriaEntityManagerFactory") EntityManagerFactory convocatoriaEntityManagerFactory) {
		return new JpaTransactionManager(convocatoriaEntityManagerFactory);
	}
}
