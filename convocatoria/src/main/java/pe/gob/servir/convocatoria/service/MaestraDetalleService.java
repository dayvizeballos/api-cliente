package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraDetalle;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespComboCabecera;
import pe.gob.servir.convocatoria.response.RespComboDetalleMaestra;
import pe.gob.servir.convocatoria.response.RespListaMaestraEntidad;
import pe.gob.servir.convocatoria.response.RespMaestraDetalle;
//import pe.gob.servir.convocatoria.response.RespMaestraDetalleAll;
import pe.gob.servir.convocatoria.response.RespObtieneMaestraDetalle;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface MaestraDetalleService {

	RespBase<RespMaestraDetalle> guardarMaestraDetalle(ReqBase<ReqMaestraDetalle> request, MyJsonWebToken token, 
			Long maestraDetalleId);
	
	RespBase<RespObtieneMaestraDetalle> obtenerMaestraDetalle(Long maestraCabeceraId,String descripcion,String sigla, String estado , String codCabecera, String codProg);

	RespBase<Object> eliminarMaestraDetalle(MyJsonWebToken token, Long maeDetalleId,String estado);
	
	RespBase<RespComboCabecera> comboMaestra();
	
	RespBase<RespComboCabecera> obtenerMaestra(Long maestraCabeceraId,Long entidadId);
	
//	RespBase<RespComboDetalleMaestra> comboMaestraDetalle(Long idCabecera);
	
	RespBase<RespComboDetalleMaestra> comboMaestraDetalleByEntidad(Long idEntidad);
	
//	RespBase<RespMaestraDetalleAll> obtenerEtapas();
//	
//	RespBase<RespComboDetalleMaestra> comboMaestraDetalleTipoInforme();
	
	RespBase<RespListaMaestraEntidad> obtenerMaestraPorEntidad(Long entidad, Long regimen, Long modalidad);
	
}
