package pe.gob.servir.convocatoria.request;

import lombok.Data;

@Data
public class RequestFlagContrato {
    Long postulanteSelId;
    Integer flagContrato;
    String descripcion;
}
