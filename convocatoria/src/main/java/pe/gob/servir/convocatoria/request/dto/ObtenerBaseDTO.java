package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerBaseDTO {
	
	private Integer correlativo;
	private Long baseId;
	private String codigoConvocatoria;
	private String fechaConvocatoria;
	private String regimenLaboral;
	private Integer nroVacante;
	private String coordinador;
	private String gestor;
	private String etapa;
	private Long etapaId;
	private String codigoEtapa;
	private String estado;
	private String codigoRegimen;
	private String codigoModalidad;
	private String codigoTipo;
	private String fechaPublicacion;
	

}
