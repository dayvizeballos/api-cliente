package pe.gob.servir.convocatoria.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespEtapasEvaluacion implements Serializable {

	private static final long serialVersionUID = 1L;

	private String periodoInicio;
	private String periodoFin;
	private List<DetalleEtapaEvaluacion> listDetalleEtapaEvaluacion;

	@Getter
	@Setter
	public static class DetalleEtapaEvaluacion implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private Integer cronogramaId;
		
		private Long tipoEvaluacion;
		private String descripcion;
		
		private String fechaInicio;
		private String fechaFin;
				
		private boolean flagcompletado;

		public DetalleEtapaEvaluacion() {
		}

	}
}
