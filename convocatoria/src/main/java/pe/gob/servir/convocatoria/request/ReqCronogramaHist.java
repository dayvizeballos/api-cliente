package pe.gob.servir.convocatoria.request;

import lombok.Data;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.Convocatoria;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

import javax.persistence.*;
import java.util.Date;

@Data
public class ReqCronogramaHist {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_cronograma_hist")
    @SequenceGenerator(name = "seq_base_cronograma_hist", sequenceName = "seq_base_cronograma_hist", schema = "sch_base", allocationSize = 1)
    @Column(name = "base_cronograma_hist_id")
    private Integer basecronogramaId;

    @JoinColumn (name = "etapa_id")
    @ManyToOne(optional = false)
    private MaestraDetalle etapa;

    @JoinColumn(name = "base_id")
    @ManyToOne(optional = false)
    private Base base;

    @JoinColumn(name = "convocatoria_id")
    @ManyToOne(optional = false)
    private Convocatoria convocatoria;

    @Column(name = "descripcion_cronograma")
    private String descripcion;

    @Column(name = "responsable")
    private String responsable;

    @Column(name = "periodo_ini")
    private Date periodoini;

    @Column(name = "periodo_fin")
    private Date periodofin;

    @Column(name = "hora_ini")
    private String horaIni;

    @Column(name = "hora_fin")
    private String horaFin;
}
