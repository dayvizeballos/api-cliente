package pe.gob.servir.convocatoria.request;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class ReqCodePdf {
	private String code;
}
