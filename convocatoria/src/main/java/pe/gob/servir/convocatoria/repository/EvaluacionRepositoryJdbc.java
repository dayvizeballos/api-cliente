package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.request.dto.*;

import java.util.List;

public interface EvaluacionRepositoryJdbc {
    public List<RegimenDTO> listallRegimenLaboralDtos (Long regimenLaboral , String estado);
    public List<ModalidadDTO> listallModalidadDtos (Long modalidad , String estado , Long codigoNivel2);
    public List<ModalidadDTO> listallModalidadDtos2 (Long modalidad , String estado);
    public List<TipoDTO> listallTipoDtos (Long tipoAcceso , Long codigoNivel2 , String estado , Long codigoNivel3);
    public List<TipoDTO> listallTipoDtos2 (Long tipo  , String estado);
    public List<EvaluacionDTO> listallTiEvaluacionDtos (Long jerarquia , String estado );
    public List<EvaluacionDTO> listallTiEvaluacionDtosServir (Long jerarquia , String estado );
    public List<EvaluacionDTO> listallTiEvaluacionEntidadDtos (Long jerarquia , String estado , Long entidad);
    public List<MasterEvaluacionDTO> listMasterEvaluacionDtos (Long entidad);
    public List<MasterEvaluacionDTO> listMasterEvaluacionDtos2 (Long entidad , Long codNivel1 , Long codNivel2 , Long codNivel3);
    public List<MasterEvaluacionDTO> listMasterEvaluacionDtos2Servir (Long entidad , Long codNivel1 , Long codNivel2 , Long codNivel3);
    public List<ModalidadDTO> listMasterEvaluacionDtos3 (Long entidad , Long codNivel1 ,  Long codNivel2 , Long codNivel3);
    public List<ModalidadDTO> listMasterEvaluacionDtos3Servir(Long entidad , Long codNivel1 ,  Long codNivel2 , Long codNivel3);
    public List<TipoDTO> listMasterEvaluacionDtos4 (Long entidad , Long codNivel1 , Long codNivel2 , Long codNivel3);
    public List<TipoDTO> listMasterEvaluacionDtos4Servir (Long entidad , Long codNivel1 , Long codNivel2 , Long codNivel3);



}
