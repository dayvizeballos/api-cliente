package pe.gob.servir.convocatoria.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarBonificacion;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerBonificacionDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespBonificacionDTO;
import pe.gob.servir.convocatoria.response.RespObtieneBonificacion;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.BonificacionService;


@RestController
@Tag(name = "Bonificacion", description = "")
public class BonificacionController {

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private BonificacionService bonificacionService;
	
	@Operation(summary = Constantes.SUM_OBT_LIST + "bonificaciones", description = Constantes.SUM_OBT_LIST + "bonificaciones", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/bonificacion/filter"},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RespBase<RespObtieneLista<ObtenerBonificacionDTO>>> obtenerBonificaciones(
			@PathVariable String access,
			@RequestParam(value = "tipoBonificacion", required = false) Long tipoBonificacion,
			@RequestParam(value = "titulo", required = false) String titulo,
			@RequestParam(value = "estado",  defaultValue = "1", required = false) String estado) {
		RespBase<RespObtieneLista<ObtenerBonificacionDTO>> response = null;
		Map<String, Object> parametroMap = new HashMap<>();
		parametroMap.put("tipoBonificacion", tipoBonificacion);
		parametroMap.put("titulo", titulo);
		parametroMap.put("estado", estado);

		response = bonificacionService.buscarBonificacionByFilter(parametroMap);

		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "get bonificacion ", description = "get bonificacion ", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/bonificacion/{bonificacionId}"},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RespObtieneBonificacion>> getBonificacion(
				@PathVariable String access, 
				@PathVariable Long bonificacionId) {
		RespBase<RespObtieneBonificacion> response = bonificacionService.obtenerBonificacion(bonificacionId);
		 return ResponseEntity.ok(response);
		 }
	
	@Operation(summary = "Guardar bonificacion ", description = "Guardar una bonificacion ", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PostMapping(path = {Constantes.BASE_ENDPOINT + "/bonificacion"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<BonificacionDTO>> guardarBonificacion(@PathVariable String access, @Valid
    @RequestBody ReqBase<ReqGuardarBonificacion> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<BonificacionDTO> response = bonificacionService.guardarBonificacion(request, jwt);
        return ResponseEntity.ok(response);

    }
    
	@Operation(summary = "Actualizar bonificacion ", description = "Actualizar bonificacion ", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})	
	@PutMapping(path = {Constantes.BASE_ENDPOINT + "/bonificacion/{bonificacionId}"},
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RespBase<BonificacionDTO>> actualizarBonificacion(@PathVariable String access,
			@PathVariable Long bonificacionId, @Valid @RequestBody ReqBase<ReqGuardarBonificacion> request
	) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<BonificacionDTO> response = bonificacionService.actualizarBonificacion(request, jwt, bonificacionId);
		return ResponseEntity.ok(response);

	}
	
	@Operation(summary = "Inactiva una bonificacion", description = "Inactiva una bonificacion", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@DeleteMapping(path = { Constantes.BASE_ENDPOINT + "/bonificacion/{bonificacionId}" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> eliminarSede(@PathVariable String access,
			@PathVariable Long bonificacionId){
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> response = bonificacionService.eliminarBonificacion(jwt, bonificacionId);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "get bonificacion por base", description = "get bonificacion por base", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/bonificacion/base/{baseId}"},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RespObtieneLista<RespBonificacionDTO>>> getBonificacionBase(
				@PathVariable String access, 
				@PathVariable Long baseId) {
		 RespBase<RespObtieneLista<RespBonificacionDTO>> response = bonificacionService.obtenerBonificacionPorBase(baseId);
		 return ResponseEntity.ok(response);
	}
}
