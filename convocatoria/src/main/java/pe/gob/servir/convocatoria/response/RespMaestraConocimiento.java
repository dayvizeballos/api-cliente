package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.MaestraConocimientoDTO;

@Getter
@Setter
public class RespMaestraConocimiento {

	private MaestraConocimientoDTO maestraConocimiento;
}
