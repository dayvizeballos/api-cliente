package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_base_horario", schema = "sch_base")
@Getter
@Setter
public class BaseHorario extends AuditEntity implements AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_horario")
    @SequenceGenerator(name = "seq_base_horario", sequenceName = "seq_base_horario", schema = "sch_base", allocationSize = 1)
	@Column(name = "base_horario_id")
	private Long baseHorarioId;
	
	@Column(name = "hora_ini")
	private String horaIni;
	
	@Column(name = "hora_fin")
	private String horaFin;
	
	@Column(name = "frecuencia_id")
	private Long frecuenciaId;
	
	
    @JoinColumn(name="base_perfil_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BasePerfil basePerfil;
	
}
