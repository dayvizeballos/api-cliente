package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tbl_base", schema = "sch_base")
@Getter
@Setter
public class Base  extends AuditEntity implements AuditableEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base")
    @SequenceGenerator(name = "seq_base", sequenceName = "seq_base", schema = "sch_base", allocationSize = 1)
	@Column(name = "base_id")
	private Long baseId;
	
	@Column(name = "entidad_id")
	private Long entidadId;
	
	@Column(name = "codigo_convocatoria")
	private String codigoConvocatoria;
	
	@Column(name = "rol_id")
	private Long rolId;
	
	@Column(name = "etapa_id")
	private Long etapaId;
	
	@Column(name = "regimen_id")
	private Long regimenId;
	
	@Column(name = "modalidad_id")
	private Long modalidadId;
	
	@Column(name = "tipo_id")
	private Long tipoId;
	
	@Column(name = "practica_id")
	private Long practicaId;
	
	@Column(name = "gestor_id")
	private Long gestorId;

	@Column(name = "coordinador_id")
	private Long coordinadorId;

	@Column(name = "nombre_gestor")
	private String nombreGestor;

	@Column(name = "nombre_coordinador")
	private String nombreCoordinador;

	@Column(name = "condicion_etapa1")
	private String condicionEtapa1;
	@Column(name = "condicion_etapa2")
	private String condicionEtapa2;
	@Column(name = "condicion_etapa3")
	private String condicionEtapa3;
	@Column(name = "condicion_etapa4")
	private String condicionEtapa4;
	@Column(name = "condicion_etapa5")
	private String condicionEtapa5;
	@Column(name = "condicion_etapa6")
	private String condicionEtapa6;
	
	@Column(name = "comentario_etapa1")
	private String comentarioEtapa1;
	@Column(name = "comentario_etapa2")
	private String comentarioEtapa2;
	@Column(name = "comentario_etapa3")
	private String comentarioEtapa3;
	@Column(name = "comentario_etapa4")
	private String comentarioEtapa4;
	@Column(name = "comentario_etapa5")
	private String comentarioEtapa5;
	@Column(name = "comentario_etapa6")
	private String comentarioEtapa6;

	@Column(name = "observacion_etapa6")
	private String observacionEtapa6;

	@Column(name = "user_id")
	private Long userId;
	
	@Column(name = "fecha_publicacion")
	private Date fechaPublicacion;
	
	@Column(name = "url")
	private String url;
	
	@Column(name = "correlativo")
	private Integer correlativo;
	
	@Column(name = "anio")
	private String anio;
	
	@Column(name = "fecha_reclutamiento")
	private Date fechaReclutamiento;
	
	@Column(name = "fecha_cierre")
	private Date fechaCierre;
	
    @OneToMany(cascade= CascadeType.ALL , fetch = FetchType.LAZY , mappedBy = "base")
    @Column(nullable = true)
    @JsonManagedReference
    private List<DatosConcurso> datosConcursoList;

	@OneToMany(cascade= CascadeType.ALL ,  fetch = FetchType.LAZY , mappedBy = "base")
	@Column(nullable = true)
	@JsonManagedReference
	private List<DeclaracionJurada> declaracionJuradaList;

	@Column(name = "siglas")
	private String siglas;
	
	@Column(name = "distrito_entidad")
	private String distritoEntidad;
	
	@Column(name = "url_logo_entidad")
	private String urlLogoEntidad;
	
	@Column(name = "nombre_entidad")
	private String nombreEntidad;
	
	@Column(name = "url_web_entidad")
	private String urlWebEntidad;
	
	public Base() {
	}

	public Base(Long baseId) {
		this.baseId = baseId;
	}
}
