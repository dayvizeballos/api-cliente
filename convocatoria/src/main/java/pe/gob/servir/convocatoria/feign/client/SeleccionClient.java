package pe.gob.servir.convocatoria.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pe.gob.servir.convocatoria.model.Postulante;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.RequestFlagContrato;
import pe.gob.servir.convocatoria.response.RespBase;

import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespPerfilVacante;



@FeignClient(name = "seleccionApi", url = "${seleccion.private.base.url}")
public interface SeleccionClient {

    @GetMapping(value = "/v1/postulante/{postulante_sel_id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    RespBase<Postulante> obtienePostulante(@PathVariable("postulante_sel_id") Long postulanteId);
    
    @GetMapping(value = "/v1/convocatoria/perfilVacante/{baseId}" , produces = MediaType.APPLICATION_JSON_VALUE)
    RespBase<RespObtieneLista<RespPerfilVacante>> obtenerPerfilVacante (@PathVariable("baseId") Long baseId);

    @PutMapping(value = "/v1/convocatoria/flagContrato" , produces = MediaType.APPLICATION_JSON_VALUE)
    RespBase<RequestFlagContrato> updateFlagContratado (@RequestBody ReqBase<RequestFlagContrato> request);
}
