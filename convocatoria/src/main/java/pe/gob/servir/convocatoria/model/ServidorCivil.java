package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_perfil_servidor_civil", schema = "sch_convocatoria")
@NamedQuery(name="ServidorCivil.findAll", query="SELECT t FROM ServidorCivil t")
@Getter
@Setter
public class ServidorCivil extends AuditEntity implements AuditableEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil_servidor_civil")
	@SequenceGenerator(name = "seq_perfil_servidor_civil", sequenceName = "seq_perfil_servidor_civil", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "perfil_servidor_civil_id")
	private Long perfilServidorCivilId;

	@Column(name = "tipo_relacion")
	private String tipoRelacion;
	
	@Column(name = "servidor_civil_id")
	private Long servidorCiviId;

}
