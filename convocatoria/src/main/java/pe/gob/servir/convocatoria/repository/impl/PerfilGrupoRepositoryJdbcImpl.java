package pe.gob.servir.convocatoria.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.PerfilGrupo;
import pe.gob.servir.convocatoria.repository.PerfilGrupoRepositoryJdbc;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PerfilGrupoRepositoryJdbcImpl implements PerfilGrupoRepositoryJdbc {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static  final  String SQL_FIND_ALL_PERFIL_GRUPO = "select  perfil_grupo_id  as id , tipo , codigo  , descripcion  ,  orden , perfil_grupo_padre_id as padreId , estado_registro as estado \n" +
            " from sch_convocatoria.tbl_perfil_grupo where perfil_grupo_padre_id is null and estado_registro  = '1'" ;

    private static  final  String SQL_FIND_ALL_PERFIL_GRUPO2 = "select  perfil_grupo_id  as id , tipo , codigo  , descripcion  ,  orden , perfil_grupo_padre_id as padreId , estado_registro as estado \n" +
            " from sch_convocatoria.tbl_perfil_grupo where perfil_grupo_padre_id = ? and estado_registro  = '1'" ;


    @Override
    public List<PerfilGrupo> findAllPerfilGrupo(Long id) {

        List<PerfilGrupo> list = new ArrayList<>();
        try {

            if (Util.isEmpty(id)){
                list = this.jdbcTemplate.query(SQL_FIND_ALL_PERFIL_GRUPO, BeanPropertyRowMapper.newInstance(PerfilGrupo.class));

            }else{
                list = this.jdbcTemplate.query(SQL_FIND_ALL_PERFIL_GRUPO2, BeanPropertyRowMapper.newInstance(PerfilGrupo.class),new Object[]{id});

            }

        }catch (Exception e){
            LogManager.getLogger(this.getClass()).error(e);
            return list;

        }
        return list;
    }
}
