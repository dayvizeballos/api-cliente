package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;
import java.io.Serializable;

@Data
public class RespObtenerPuntajesEvaluacionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long evaluacionId;
    private Long jerarquiaId;
    private Long tipoEvaluacionId;
    private Integer orden;
    private Integer peso;
    private Integer puntajeMinimo;
    private Integer puntajeMaximo;
    private String detalleEvaluacion;
    private String estado;
    private Long entidadId;
    private Long evaluacionEntidad;
    private Long evaluacionOrigenId;

}
