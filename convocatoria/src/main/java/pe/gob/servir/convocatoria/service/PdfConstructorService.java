package pe.gob.servir.convocatoria.service;

import java.io.IOException;
import java.util.Map;

import com.lowagie.text.DocumentException;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCrearCriterioEvaluacioPdf;
import pe.gob.servir.convocatoria.request.ReqOrquestadorPdf;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespPdfInforme;

public interface PdfConstructorService {
	
	byte[] getPdfFromInformeDetalle(Long idInformeDetalle) throws DocumentException, IOException;
	byte[] createCriterioEvaluacionPdf(ReqCrearCriterioEvaluacioPdf request) throws DocumentException, IOException, IllegalAccessException;
	byte[] getPdfBonificacion(Long idInformeDetalle,Map<Long, String> mapDetalle ) throws DocumentException, IOException, IllegalAccessException;
	RespBase<RespPdfInforme> seleccionarPdfByTipoInforme(ReqBase<ReqOrquestadorPdf> request) throws DocumentException, IOException, IllegalAccessException;

	
}
