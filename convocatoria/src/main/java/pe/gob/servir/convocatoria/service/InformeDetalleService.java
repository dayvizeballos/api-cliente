package pe.gob.servir.convocatoria.service;


import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.InformeDetalleDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCriterioEvaluacion;
import pe.gob.servir.convocatoria.response.RespInformeDetalle;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;


public interface InformeDetalleService {

    RespBase<RespObtieneLista<InformeDetalleDTO>>listAllInformeDetalleDto(Long entidad , Long tipoInforme , String estado , String titulo , Long idInforme);
    RespBase<RespObtieneLista<BonificacionDTO>> listAllBonificacionDto(Long idInformeDetalle);
    RespBase<InformeDetalleDTO> cambiarEstado(Long idInformeDetalle , String estado , MyJsonWebToken token);
    RespBase<RespInformeDetalle> guardarInformeDetalle(ReqBase<InformeDetalleDTO> request, MyJsonWebToken token);
    RespBase<RespInformeDetalle> actualizarInformeDetalle(ReqBase<InformeDetalleDTO> request, MyJsonWebToken token);
    RespBase<BonificacionDTO> actualizarBonificacion(ReqBase<BonificacionDTO> request, MyJsonWebToken token);
    RespBase<RespObtieneLista<RespCriterioEvaluacion>>listarDocumentosInforme(Long entidadId, Long tipoInfoId);
    RespBase<RespObtieneLista<RespCriterioEvaluacion>>listarInformeDetalle(Long entidadId, Long tipoInfoId);
}
