package pe.gob.servir.convocatoria.response;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class RespSaveUpdateInfCompl {
	
	private String resultado;

	
}
