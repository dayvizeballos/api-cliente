package pe.gob.servir.convocatoria.response;

import lombok.Data;

@Data
public class RespDifusionExperiencia {
    private String anioExperTotal;
    private String anioExpSecPu;
    private String nivelMiniPues;
}
