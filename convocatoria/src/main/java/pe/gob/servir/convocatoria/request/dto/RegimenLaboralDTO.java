package pe.gob.servir.convocatoria.request.dto;

import java.util.List;

import javax.persistence.Transient;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

@JGlobalMap
@Getter
@Setter
public class RegimenLaboralDTO {
	
	private Long maeCabeceraId;
	private String descripcionCorta;
	private String estadoRegistro;
	private String estado;
	
	@Transient
	private List<MaestraDetalle> listaDetalleModalidad;
	
	
	

}
