package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaFormacionAcademica;
import pe.gob.servir.convocatoria.request.ReqCreaPerfil;
import pe.gob.servir.convocatoria.request.ReqCreaPerfilFuncion;
import pe.gob.servir.convocatoria.request.dto.ObtenerPerfilConfigDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerPerfilDTO;
import pe.gob.servir.convocatoria.request.dto.VacanteDTO;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.response.dto.RespPerfilDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

import java.util.Map;

public interface PerfilService {
		
	RespBase<RespCreaPerfil> guardarPerfil(ReqBase<ReqCreaPerfil> request, MyJsonWebToken token, Long perfilId);
	
	RespBase<RespObtieneLista<ObtenerPerfilDTO>> obtenerPerfil(String fechaIni , String fechaFin , Long entidad ,
															   String codigoPuesto , String nombrePerfil , Long regimenId , Long organoId , Long unidadOrganicaId ,
															   String origenPerfil , Long estadoRevision , String estado);

	RespBase<RespCreaPerfilFuncion> guardarPerfilFuncion(ReqBase<ReqCreaPerfilFuncion> request, MyJsonWebToken token, Long perfilFuncionId);
	RespBase<RespCreaPerfilFuncion> actualizarPerfilFuncion(ReqBase<ReqCreaPerfilFuncion> request, MyJsonWebToken token, Long perfilFuncionId);

	RespBase<RespCreaPerfilFuncion> findPerfilFuncion( Long perfilId);
	
	RespBase<ReqCreaFormacionAcademica> guardarFormacionAcademica(ReqBase<ReqCreaFormacionAcademica> request, MyJsonWebToken token);

	RespBase<RespObtienePerfil> obtienePerfilById(Long perfilId);
	RespBase<RespObtienePerfil> cambiarEstado(Long perfilId , String estado , MyJsonWebToken token);
	
	
	RespBase<ReqCreaFormacionAcademica> obtieneFormacionAcademicaById(Long perfilId);

	RespBase<RespListaVacante> listarVacantes(Long baseId);
	
	RespBase<VacanteDTO> cambiarEstadoVacante(Long basePerfilId, String estado, MyJsonWebToken token) ;
	
	RespBase<RespObtieneLista<ObtenerPerfilConfigDTO>> buscarPerfilByFilter(Map<String, Object> parametroMap);
	
	RespBase<RespObtieneLista<RespPerfilDTO>> listarPerfilByConvocatoria(Long convocatoriaId);
	
	RespBase<RespObtienePerfil> cambiarEstadoRevision(Long perfilId , Long estado , MyJsonWebToken token);


}
