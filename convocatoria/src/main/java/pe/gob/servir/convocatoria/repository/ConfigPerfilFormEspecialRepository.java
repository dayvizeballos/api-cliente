package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.gob.servir.convocatoria.model.TblConfPerfFormacEspecial;

public interface ConfigPerfilFormEspecialRepository extends JpaRepository<TblConfPerfFormacEspecial, Long>{

}
