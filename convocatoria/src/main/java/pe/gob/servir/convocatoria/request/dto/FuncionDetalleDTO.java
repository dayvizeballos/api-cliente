package pe.gob.servir.convocatoria.request.dto;


import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class FuncionDetalleDTO {

	private Long funcionDetalleId;
	
	@NotNull(message = Constantes.CAMPO + " descripcion " + Constantes.ES_OBLIGATORIO)
	private String descripcion;
	
	@NotNull(message = Constantes.CAMPO + " orden " + Constantes.ES_OBLIGATORIO)
	private Integer orden;

	private String estado;
}
