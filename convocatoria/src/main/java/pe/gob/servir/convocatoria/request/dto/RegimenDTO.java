package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JGlobalMap
@Getter
@Setter
public class RegimenDTO {
    private Long maeCabeceraId;
    private Long maeDetalleId;
    private String descripcionCorta;
    private String estadoRegistro;
    private String estado;
    private Long codigoNivel1;
    private Long codigoNivel2;
    private Long codigoNivel3;
    private Long jerqrquia;
    private  List<ModalidadDTO> listaDetalleModalidad;

}
