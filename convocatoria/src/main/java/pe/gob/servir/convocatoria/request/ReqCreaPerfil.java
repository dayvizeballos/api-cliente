package pe.gob.servir.convocatoria.request;


import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.PerfilDTO;

@Getter
@Setter
public class ReqCreaPerfil {

	private PerfilDTO perfil;
	
}
