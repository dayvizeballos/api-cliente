package pe.gob.servir.convocatoria.request;

import lombok.Data;

@Data
public class ReqCargaMasivaPerfil {
    private String codRegimen;
    private String versionExcel;
    private Long idEntidad;
}
