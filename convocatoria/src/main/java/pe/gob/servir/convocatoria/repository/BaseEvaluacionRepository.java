package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BaseEvaluacion;
import pe.gob.servir.convocatoria.model.BaseEvaluacionDetalle;

import java.util.List;
import java.util.Optional;

@Repository
public interface BaseEvaluacionRepository extends JpaRepository<BaseEvaluacion, Long>{
	@Query("select eva from BaseEvaluacion eva where base.baseId=:baseId")
	 Optional<BaseEvaluacion> findBaseEvaluacion(@Param("baseId")Long baseId);

	@Query("select eva.base from BaseEvaluacion eva where eva.base.baseId=:baseId")
	Optional<Base> findBaseEvaluacionBase(@Param("baseId")Long baseId);

	@Query("select eva.baseEvaluacionDetalleList from BaseEvaluacion eva where eva.baseEvaluacionId=:baseEvaluacionId")
	 List<BaseEvaluacionDetalle> listEvalDetalleByEval(@Param("baseId") Long baseEvaluacionId);
	
	@Query("select eva.baseEvaluacionDetalleList from BaseEvaluacion eva where eva.base=:base")
	 List<BaseEvaluacionDetalle> listEvalDetalleByBase(@Param("base") Base base);
}
