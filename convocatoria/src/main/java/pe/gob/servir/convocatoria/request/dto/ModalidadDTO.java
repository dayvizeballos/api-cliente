package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JGlobalMap
@Getter
@Setter
public class ModalidadDTO {
    private Long maeDetalleId;
    private Long maeCabeceraId;
    private String descripcion;
    private String descripcionCorta;
    private String estadoRegistro;
    private String estado;
    private Long codNivel1;
    private Long codNivel2;
    private Long codNivel3;
    private Long jerarquia;
    List<TipoDTO>listaTipo;
}
