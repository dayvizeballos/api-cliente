package pe.gob.servir.convocatoria.request.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;


@JGlobalMap
@Getter
@Setter
public class PerfilFuncionDTO {
	
	
	private Long perfilFuncionId;
	
	@NotNull(message = Constantes.CAMPO + " perfilId " + Constantes.ES_OBLIGATORIO)
	private Long perfilId;
	
	@NotNull(message = Constantes.CAMPO + " regimenLaboralId " + Constantes.ES_OBLIGATORIO)
	private String codigoRegimen;
	
	private String condicionAtipica;
	
	private Long periocidadCondicionAtipicaId;
	
	private String sustentoCondicionAtipica;
		
	private String coordinacionInterna;
	
	private String coordinacionExterna;

	private String estado;
	
	private List<FuncionDetalleDTO> lstFuncionDetalle;
	
	private String lstServidorCivil;

}
