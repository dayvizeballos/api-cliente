package pe.gob.servir.convocatoria.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.CarreraProfesional;
import pe.gob.servir.convocatoria.repository.CarreraProfesionalRepository;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.service.CapacitacionService;

@Service
public class CapacitacionServiceImpl implements CapacitacionService {

	@Autowired
	CarreraProfesionalRepository carreraProfesionalnRepository;


	@Override
	public RespBase<RespObtieneLista<CarreraProfesional>> obtenerCarreras(Long situacionId) {
		RespObtieneLista<CarreraProfesional> respPayload = new RespObtieneLista<>();
		CarreraProfesional carreraProfesionalFilter = new CarreraProfesional();
		carreraProfesionalFilter.setIdDetalle(situacionId);
		carreraProfesionalFilter.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<CarreraProfesional> example = Example.of(carreraProfesionalFilter);
		List<CarreraProfesional> ltaCarreraFilter = carreraProfesionalnRepository.findAll(example);
		respPayload.setCount(ltaCarreraFilter.size());
		respPayload.setItems(ltaCarreraFilter);
		return new RespBase<RespObtieneLista<CarreraProfesional>>().ok(respPayload);
	}
	
	@Override
	public RespBase<RespObtieneLista<CarreraProfesional>> listarCarreras() {
		RespObtieneLista<CarreraProfesional> respPayload = new RespObtieneLista<>();
		CarreraProfesional carreraProfesionalFilter = new CarreraProfesional();
		carreraProfesionalFilter.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<CarreraProfesional> example = Example.of(carreraProfesionalFilter);
		List<CarreraProfesional> ltaCarreraFilter = carreraProfesionalnRepository.findAll(example);
		respPayload.setCount(ltaCarreraFilter.size());
		respPayload.setItems(ltaCarreraFilter);
		return new RespBase<RespObtieneLista<CarreraProfesional>>().ok(respPayload);
	}

}
