package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.MaestraConocimientoDTO;

@Getter
@Setter
public class RespListaMaestraConocimiento {
	
	private List<MaestraConocimientoDTO> lstMaestraConocimiento;

}
