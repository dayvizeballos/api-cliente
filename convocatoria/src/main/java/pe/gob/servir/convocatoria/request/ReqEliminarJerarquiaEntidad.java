package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@Getter
@Setter
public class ReqEliminarJerarquiaEntidad {

	@NotNull(message = Constantes.CAMPO + " entidadId " + Constantes.ES_OBLIGATORIO)
	private Long entidadId;	
	
	@NotNull(message = Constantes.CAMPO + " jerarquiaId " + Constantes.ES_OBLIGATORIO)
	private Long jerarquiaId;
}
