package pe.gob.servir.convocatoria.controller;

import com.lowagie.text.DocumentException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaBaseCronograma;
import pe.gob.servir.convocatoria.request.ReqEliminarActividad;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCreaBaseCronograma;
import pe.gob.servir.convocatoria.response.RespEtapaActualVacanteBase;
import pe.gob.servir.convocatoria.response.dto.RespPeriodoCronogramaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.BaseCronogramaService;
import pe.gob.servir.convocatoria.service.ReportePerfilService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;


@RestController
@Tag(name = "BaseCronograma", description = "")
public class BaseCronogramaController {
	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private BaseCronogramaService baseCronogramaService;

	
	@Autowired
	private ReportePerfilService reporteService;
	

	/*
	@Autowired
	private ReporteBase baseService;
	*/

	
	@Operation(summary = "Crea base cronograma", description = "Crea base cronograma", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/basecronogramas" }, consumes = {
				MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	
	public ResponseEntity<RespBase<RespCreaBaseCronograma>> registrarBaseCronograma(@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqCreaBaseCronograma> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespCreaBaseCronograma> response = baseCronogramaService.guardarBaseCronograma(request, jwt, null);
			 
		return ResponseEntity.ok(response);
		}


	@Operation(summary = "Actualiza base cronograma", description = "Actualiza base cronograma", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/basecronogramas" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })

	//@AuditableEndpoint(endpointName = "PRUEBA") /*PARA QUE SEA UDITABLE EL ENDPOINT*/
	public ResponseEntity<RespBase<RespCreaBaseCronograma>> updateBaseCronograma(@PathVariable String access,
																					@Valid @RequestBody ReqBase<ReqCreaBaseCronograma> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespCreaBaseCronograma> response = baseCronogramaService.updateBaseCronograma(request, jwt, null);

		return ResponseEntity.ok(response);
	}

	@Operation(summary = "Obtener basecronograma por baseId", description = "Obtener basecronograma por baseId history = true trae los historicos", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/basecronogramas/{baseId}"},
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespCreaBaseCronograma>> findAllBaseCronograma(
			@PathVariable String access,
			@PathVariable Long baseId,
			@RequestParam(defaultValue = "0") int history)
			 {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespCreaBaseCronograma> response = baseCronogramaService.findAllCronogramas(baseId , history);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Elimina una etapa de base cronograma", description = "Elimina una etapa de base cronograma", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/basecronogramas/actividad" }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<ReqEliminarActividad>> eliminarBaseCronograma(
			@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqEliminarActividad> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<ReqEliminarActividad> response = baseCronogramaService.eliminarBaseCronograma(request, jwt);
		return ResponseEntity.ok(response);
	}




//	@Operation(summary = "Actualiza Base Cronograma", description = "Actualiza Base Cronograma", tags = { "" },
//			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
//	@ApiResponses(value = {
//			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
//			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
//			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
//	@PutMapping(path = { Constantes.BASE_ENDPOINT+"/basecronograma/{basecronogramaId}"},
//				produces = { MediaType.APPLICATION_JSON_VALUE })
//	public ResponseEntity<RespBase<RespActualizaBaseCronograma>> actualizarBaseCronograma(
//			@PathVariable String access,
//			@PathVariable Integer basecronogramaId,
//			@Valid @RequestBody ReqBase<ReqActualizaBaseCronograma> request) {
//		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
//		RespBase<RespActualizaBaseCronograma> response = baseCronogramaService.actualizarBaseCronograma(request, jwt, basecronogramaId);
//		return ResponseEntity.ok(response);
//
//	}
	



	
//	@Operation(summary = Constantes.SUM_OBT_LIST+" maestra detalles by cabecera", description = Constantes.SUM_OBT_LIST+" maestra detalles by cabecera", tags = { "" },
//			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
//	@ApiResponses(value = { 
//			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
//			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
//			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
//	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/basecronograma/etapas"}, 
//				produces = { MediaType.APPLICATION_JSON_VALUE })
//	public ResponseEntity<RespBase<RespMaestraDetalleAll>> comboEtapas(
//			@PathVariable String access) {
//		RespBase<RespMaestraDetalleAll> response = maestraDetalleService.obtenerEtapas();
//		return ResponseEntity.ok(response);		
//	}
	


	
	@Operation(summary = "Generar Pdf Perfil", description = "Generar Pdf Perfil", tags = {""},
			   security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", 
			content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/basecronograma/perfiles/pdf/{perfilId}" })
	public @ResponseBody ResponseEntity<RespBase<String>> generarPdfPerfil
						   (@PathVariable String access, @PathVariable Long perfilId) throws DocumentException, IOException {
		
		byte[] bytes = reporteService.generatePdfFromHtml(perfilId);
        String encodedString =
                Base64.getEncoder().withoutPadding().encodeToString(bytes);
        RespBase<String> payload = new RespBase<>();
        payload.setPayload(encodedString);
        
		return ResponseEntity.ok(payload);
		 
	}

	/*
	@Operation(summary = "Generar Pdf Base", description = "Generar Pdf Base", tags = {""},
			   security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", 
			content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/basecronograma/perfiles/pdfbase/{perfilId}" })
	public @ResponseBody ResponseEntity<RespBase<String>> generarPdfBase
						   (@PathVariable String access,@PathVariable Long perfilId) throws DocumentException, IOException {
		byte[] bytes = baseService.generatePdfFromHtml(perfilId);
		String encodedString =
                Base64.getEncoder().withoutPadding().encodeToString(bytes);
        RespBase<String> payload = new RespBase<>();
        payload.setPayload(encodedString);
        
        return ResponseEntity.ok(payload);
	}
 */

	@Operation(summary = "Obtener cronograma por etapa y baseId", description = "Obtener cronograma por etapa y baseId", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/basecronogramas/etapa/{etapaId}/{baseId}"},
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespPeriodoCronogramaDTO>> obtenerCronogramaEtapaBase(
			@PathVariable String access, @PathVariable Long baseId,	@PathVariable Long etapaId)
	{
		RespBase<RespPeriodoCronogramaDTO> response = baseCronogramaService.obtenerCronogramaEtapaBase(etapaId, baseId);
		return ResponseEntity.ok(response);
	}
	
	
	@Operation(summary = "Obtener etapa actual - vacante de convocatoria", description = "Obtener etapa actual - vacante de convocatoria", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/basecronogramas/etapaActual/{baseId}"},
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespEtapaActualVacanteBase>> obtenerEtapaActualBase(
			@PathVariable String access, @PathVariable Long baseId)
	{
		RespBase<RespEtapaActualVacanteBase> response = baseCronogramaService.obtenerEtapaActualBase(baseId);
		return ResponseEntity.ok(response);
	}
}
