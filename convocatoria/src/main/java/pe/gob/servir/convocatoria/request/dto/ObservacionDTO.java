package pe.gob.servir.convocatoria.request.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.Movimiento;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ObservacionDTO {

    /*valores de entrada*/
    private String observacion;
    @NotNull(message = "el campo etapa no puede ser null")
    private Long etapa;


    /*valores de salida*/
    private Long baseObsId;
    private Long movimientoId;

}
