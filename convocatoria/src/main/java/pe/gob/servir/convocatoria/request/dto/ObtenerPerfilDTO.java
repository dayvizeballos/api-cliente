package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerPerfilDTO {
	private Long perfilId;
	private String nombrePuesto;
	private Long entidadId;	
	private Long regimenLaboralId;
	private String regimenLaboral;	
	private String puestoCodigo;
	private Long organoId;
	private String siglaOrgano;	
	private Long unidadOrganicaId;	
	private String unidadOrganica;	
	private Integer nroPosicionesPuesto;
	private String estadoId;
	private String estado;
	private String codProg;
	private String origenPerfil;
	private Long estadoRevision;
	private boolean revisionHabilitado;
	private String estadoFunc;
	private String estadoForm;
	private String estadoExp;
}
