package pe.gob.servir.convocatoria.request;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.EvaluacionEntidadDTO;

public class ReqActualizaEvaluacionEntidad {

	@NotNull(message = Constantes.CAMPO + " listaEvaluacion " + Constantes.ES_OBLIGATORIO)
	@Valid
	private List<EvaluacionEntidadDTO> listaEvaluacionEntidad;
}
