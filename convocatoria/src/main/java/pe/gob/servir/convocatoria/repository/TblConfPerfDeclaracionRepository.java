package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.TblConfPerfDeclaracion;

@Repository
public interface TblConfPerfDeclaracionRepository extends JpaRepository<TblConfPerfDeclaracion, Long> {

}