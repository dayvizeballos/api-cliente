package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;
import pe.gob.servir.convocatoria.model.TblConfPerfFormacEspecif;

@Data
public class RConfPerfilFormEspecificDTO {
	private Long especificacionId;
	private Long perfilId;
	private Long tipoEspecificacion;
	private String descripcion;
	private Long cantidad;
	private Long puntaje;
	private Boolean requisitoMinimo;
	
	public RConfPerfilFormEspecificDTO (TblConfPerfFormacEspecif oConfig) {
		this.especificacionId = oConfig.getConfPerfFormacEspecifId();
		this.perfilId = oConfig.getPerfil().getPerfilId();
		this.tipoEspecificacion = oConfig.getTipoEspecificacion();
		this.descripcion = oConfig.getDescripcion();
		this.cantidad = oConfig.getCantidad();
		this.puntaje = oConfig.getPuntaje();
		this.requisitoMinimo=oConfig.getRequisitoMinimo();
	}
	
	public RConfPerfilFormEspecificDTO() {
		
	}
	
	public RConfPerfilFormEspecificDTO(Long tipoEspecificacion, String descripcion, Long perfilId) {
		this.tipoEspecificacion=tipoEspecificacion;
		this.descripcion=descripcion;
		this.perfilId=perfilId;
		
	}

}
