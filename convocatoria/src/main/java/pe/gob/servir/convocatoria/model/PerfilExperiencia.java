package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbl_perfil_experiencia", schema = "sch_convocatoria")
@Getter
@Setter
public class PerfilExperiencia  extends  AuditEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_experiencia_perfil")
    @SequenceGenerator(name = "seq_experiencia_perfil", sequenceName = "seq_experiencia_perfil", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "experiencia_perfil_id")
    private Long id;

    @Column(name = "perfil_id")
    private Long perfilId;

    @Column(name = "anio_experiencia_total")
    private Long  anioExpTotal;

    @Column(name = "mes_experiencia_total")
    private Long  mesExpTotal;

    @Column(name = "anio_exper_requ_puesto")
    private Long  anioExReqPuesto;

    @Column(name = " mes_exper_requ_puesto")
    private Long  mesExReqPuesto;

    @Column(name = " anio_exper_sector_publico")
    private Long  anioExpSecPub;

    @Column(name = " mes_exper_sector_publico")
    private Long  mesExpSecPub;

    @Column(name = " nivel_minimo_puesto_id")
    private Long  nivelMinPueId;

    @Column(name = "aspectos_complementarios ")
    private String aspectos;


    @OneToMany(cascade= CascadeType.ALL , mappedBy = "perfilExperiencia")
    private List<PerfilExperienciaDetalle> perfilExperienciaDetalleList1;


    @OneToMany(cascade= CascadeType.ALL , mappedBy = "perfilExperiencia")
    @Column(nullable = true)
    @JsonManagedReference
    private List<PerfilExperienciaDetalle> perfilExperienciaDetalleList2;


}
