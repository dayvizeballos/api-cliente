package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnidadOrganica extends AuditEntity{

	private Long organigramaId;

	private Long entidadId;

	private Long areaId;	
	
	private Long sedeId;
	
	private String puesto;
	
	private Integer orden;
	
	private Long nivel;
	
	private String desNivel;
	
	private Long tipoOrganoUoId;
	
	private String descripcionTipoOrg;
	
	private Long nivelGobiernoId;
	
	private String unidadOrganica;
	
	private String descripcionCorta;
	
	private String sigla;
	
	private String estadoRegistro;
	
	private String estado; 
	
	private Long personaResponsableId;
	
	private String nombres;
	
	private String apellidoPaterno;
	
	private String apellidoMaterno;
	
	private Long telefonoId;
	
	private String telefono;
	
	private Long correoId;	
	
	private String correo; 
	
	private Integer tipoDocumento;
	
	private String destipoDocumento;
	
	private String nroDocumento;

	private Long padreOrganigramaId;	
	
	private String descripOrganoPadre;
	
	private Long paisId;
	
	private String nombrePais;

}
