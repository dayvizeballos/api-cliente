package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;

@Getter
@Setter
public class RespObtieneBonificacion {

	private BonificacionDTO bonificacion;
}
