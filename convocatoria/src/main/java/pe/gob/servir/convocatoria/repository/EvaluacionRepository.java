package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Evaluacion;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

@Repository
public interface EvaluacionRepository extends JpaRepository<Evaluacion, Long>{

	@Query("select eva from Evaluacion eva\r\n" + 
			"INNER JOIN MaestraDetalle tipoeva on tipoeva.maeDetalleId = eva.tipoEvaluacionId\r\n"+
			 "where 1 = 1 and eva.tipoEvaluacionId=:idTipoEvaluacion and eva.jerarquiaId=:jerarquiaId and eva.estadoRegistro='1'")
	public Evaluacion findEvaluacion(@Param("idTipoEvaluacion")Long idTipoEvaluacion,@Param("jerarquiaId")Long jerarquiaId);
	
	@Query("select tipoeva from Evaluacion eva\r\n" + 
			"INNER JOIN MaestraDetalle tipoeva on tipoeva.maeDetalleId = eva.tipoEvaluacionId\r\n"+
			 "where 1 = 1 and eva.tipoEvaluacionId=:idTipoEvaluacion and eva.jerarquiaId=:jerarquiaId and eva.estadoRegistro='1'")
	public MaestraDetalle findMaestraDetalle(@Param("idTipoEvaluacion")Long idTipoEvaluacion,@Param("jerarquiaId")Long jerarquiaId);
}
