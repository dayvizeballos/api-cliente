package pe.gob.servir.convocatoria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespComboCabecera;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;

@RestController
@Tag(name = "Maestra Cabecera", description = "")
public class MaestraCabeceraController {
		
	@Autowired
	MaestraDetalleService maestraDetalleService;
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra cabecera", description = Constantes.SUM_OBT_LIST+"maestra cabecera", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraCabecera/combo"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespComboCabecera>> listarMaestraCabecera(
			@PathVariable String access) {
		RespBase<RespComboCabecera> response = maestraDetalleService.comboMaestra();
		
		return ResponseEntity.ok(response);		
	}
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra cabecera y sus detalles", description = Constantes.SUM_OBT_LIST+"maestra cabecera y sus detalles", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraCabecera"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespComboCabecera>> listarMaestras(
			@PathVariable String access,
			@RequestParam(value = "idCabecera", required = false) Long idCabecera,
			@RequestParam(value = "idEntidad", required = false) Long idEntidad) {
		RespBase<RespComboCabecera> response = maestraDetalleService.obtenerMaestra(idCabecera,idEntidad);
		
		return ResponseEntity.ok(response);		
	}

}
