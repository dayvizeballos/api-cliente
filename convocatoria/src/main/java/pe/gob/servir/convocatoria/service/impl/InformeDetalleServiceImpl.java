package pe.gob.servir.convocatoria.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.model.Bonificacion;
import pe.gob.servir.convocatoria.model.BonificacionDetalle;
import pe.gob.servir.convocatoria.model.InformeDetalle;
import pe.gob.servir.convocatoria.repository.BonificacionRepository;
import pe.gob.servir.convocatoria.repository.InformeDetalleRepository;
import pe.gob.servir.convocatoria.repository.InformeDetalleRepositoryJdbc;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.BonificacionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.InformeDetalleDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCriterioEvaluacion;
import pe.gob.servir.convocatoria.response.RespInformeDetalle;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.InformeDetalleFunction;
import pe.gob.servir.convocatoria.service.InformeDetalleService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class InformeDetalleServiceImpl implements InformeDetalleService {

    private static final Logger logger = LoggerFactory.getLogger(InformeDetalleServiceImpl.class);

    @Autowired
    InformeDetalleRepositoryJdbc informeDetalleRepositoryJdbc;

    @Autowired
    InformeDetalleRepository informeDetalleRepository;

    @Autowired
    BonificacionRepository bonificacionRepository;
    
    @Override
    public RespBase<RespObtieneLista<InformeDetalleDTO>> listAllInformeDetalleDto(Long entidad, Long tipoInforme, String estado, String titulo , Long idInforme) {
        RespBase<RespObtieneLista<InformeDetalleDTO>> response = new RespBase<>();
        if (Util.isEmpty(entidad)){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "entidad es obligatorio");
            return response;
        }
        
        List<InformeDetalleDTO> lst = informeDetalleRepositoryJdbc.listAllInformeDetalleDto(entidad , tipoInforme , estado , titulo , idInforme);
        RespObtieneLista respObtieneLista = new RespObtieneLista();
        respObtieneLista.setCount(lst.size());
        respObtieneLista.setItems(lst);

        return new RespBase<RespObtieneLista<InformeDetalleDTO>>().ok(respObtieneLista);
    }

    @Override
    public RespBase<RespObtieneLista<BonificacionDTO>> listAllBonificacionDto(Long idInformeDetalle) {
        RespBase<RespObtieneLista<BonificacionDTO>> response = new RespBase<>();
        if (Util.isEmpty(idInformeDetalle)){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "idInformeDetalle es obligatorio");
            return response;
        }

        List<BonificacionDTO> lst = informeDetalleRepositoryJdbc.listAllBonificacionDto(idInformeDetalle);
        List<BonificacionDTO>  lstOut  = lst.stream()
                .map(bonificacionDTO -> {
                    bonificacionDTO.setBonificacionDetalleDTOList(settValoresBonificacionDetalle(bonificacionDTO));
                    return bonificacionDTO;
                }).collect(Collectors.toList());


        RespObtieneLista respObtieneLista = new RespObtieneLista();
        respObtieneLista.setCount(lstOut.size());
        respObtieneLista.setItems(lstOut);
        return new RespBase<RespObtieneLista<BonificacionDTO>>().ok(respObtieneLista);
    }

    /**
     * list bonificacion detalle
     * @param bonificacionDTO
     * @return
     */
    private List<BonificacionDetalleDTO> settValoresBonificacionDetalle(BonificacionDTO bonificacionDTO){
        return informeDetalleRepositoryJdbc.listAllBonificacionDetalleDtos(bonificacionDTO.getBonificacionId());
    }


    @Override
    public RespBase<InformeDetalleDTO> cambiarEstado(Long idInformeDetalle, String estado, MyJsonWebToken token) {
        InformeDetalleDTO payload = new InformeDetalleDTO();
        RespBase<InformeDetalleDTO> response = new RespBase<>();

        if (Util.isEmpty(idInformeDetalle)){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "idInformeDetalle es obligatorio");
            return response;
        }

        if (Util.isEmpty(estado)){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "estado es obligatorio");
            return response;
        }

        Optional<InformeDetalle> informeDetalleptional = informeDetalleRepository.findById(idInformeDetalle);
        if (informeDetalleptional.isPresent()){
            InformeDetalle informeDetalle = informeDetalleptional.get();
            informeDetalle.setEstadoRegistro(estado);
            informeDetalle.setCampoSegUpd(estado , token.getUsuario().getUsuario() , Instant.now());
            informeDetalleRepository.save(informeDetalle);
            payload.setEstado(informeDetalle.getEstadoRegistro());
        }else{
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "no se encontro Informe detalle con el id: "+idInformeDetalle);
            return response;
        }

        return new RespBase<InformeDetalleDTO>().ok(payload);
    }

    @Transactional(transactionManager = "convocatoriaTransactionManager")
    @Override
	public RespBase<RespInformeDetalle> guardarInformeDetalle(ReqBase<InformeDetalleDTO> request,
                                                              MyJsonWebToken token) {
		InformeDetalle informeDetalle = new InformeDetalle();
		InformeDetalleDTO informeDetalleDTO =  request.getPayload();
		informeDetalle = InformeDetalleFunction.convertirInformeDetalle.apply(informeDetalleDTO);
		informeDetalle.setCampoSegIns(token.getUsuario().getUsuario() , Instant.now());

		informeDetalleRepository.save(informeDetalle);

		RespInformeDetalle payload = new RespInformeDetalle();

		InformeDetalleDTO informeDTO = null;
		List<BonificacionDTO> lstbonificacionDTO = new ArrayList<>();
		List<BonificacionDetalleDTO> lstbonificacionDetalleDTO = new ArrayList<>();
		informeDTO = InformeDetalleFunction.convertirDTOInformeDetalle.apply(informeDetalle);

		payload.setInformeDetalle(informeDTO);
		return new RespBase<RespInformeDetalle>().ok(payload);
	}

    @Override
    public RespBase<RespInformeDetalle> actualizarInformeDetalle(ReqBase<InformeDetalleDTO> request, MyJsonWebToken token) {
        RespBase<RespInformeDetalle> response = new RespBase<>();


        if ( Util.isEmpty(request.getPayload().getInformeDetalleId())){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "el id de informe detalle es obligatorio");
            return response;
        }

        Optional<InformeDetalle> informeDetalleOptional  = informeDetalleRepository.findById(request.getPayload().getInformeDetalleId());
        if (!informeDetalleOptional.isPresent()){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "informe detalle no existe con el id: "+request.getPayload().getInformeDetalleId());
            return response;
        }

        InformeDetalle informeDetalle = InformeDetalleFunction.convertirInformeDetalle.apply(request.getPayload());
        informeDetalle.setCampoSegUpd(Constantes.ACTIVO , token.getUsuario().getUsuario() , Instant.now());

        List<Bonificacion> bonificacionList = new ArrayList<>();

        informeDetalleRepository.save(informeDetalle);

        InformeDetalleDTO resPayload = new InformeDetalleDTO();
        List<InformeDetalleDTO> ls =  informeDetalleRepositoryJdbc.listAllInformeDetalleDto(informeDetalle.getEntidadId(), null, Constantes.ACTIVO, null, informeDetalle.getInformeDetalleId());
        if (!ls.isEmpty()) resPayload = ls.get(0);

        RespInformeDetalle respInformeDetalle = new RespInformeDetalle();
        respInformeDetalle.setInformeDetalle(resPayload);
        return new RespBase<RespInformeDetalle>().ok(respInformeDetalle);
    }

    /**
     * sett campos para bonificacion detalle
     *
     * @param it
     * @param bonificacion
     * @param token
     * @return
     */
    private List<BonificacionDetalle> settValoresBonificacionDetalle(BonificacionDTO it, Bonificacion bonificacion, MyJsonWebToken token) {
        List<BonificacionDetalle> ls = new ArrayList<>();

        for (BonificacionDetalleDTO de : it.getBonificacionDetalleDTOList()) {
            BonificacionDetalle bonificacionDetalle = InformeDetalleFunction.convertirBonificacionDetalle.apply(de);
            bonificacionDetalle.setBonificacion(bonificacion);

            if (de.getBonificacionDetalleId() == null) {
                bonificacionDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
            } else {
                bonificacionDetalle.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
            }
            bonificacionDetalle.setEstadoRegistro(de.getEstadoRegistro().trim());
            ls.add(bonificacionDetalle);
        }
        return ls;
    }

    @Override
    public RespBase<BonificacionDTO> actualizarBonificacion(ReqBase<BonificacionDTO> request, MyJsonWebToken token) {
        return null;
    }

	@Override
	public RespBase<RespObtieneLista<RespCriterioEvaluacion>> listarDocumentosInforme(Long entidadId, Long tipoInfoId) {
		
		RespBase<RespObtieneLista<RespCriterioEvaluacion>> response = new RespBase<>();
		RespObtieneLista<RespCriterioEvaluacion> respObtieneLista = new RespObtieneLista<>();
		try {
			List<RespCriterioEvaluacion> lst = informeDetalleRepositoryJdbc.listInformeDetalle(entidadId, tipoInfoId,Constantes.ACTIVO);
			if(Objects.nonNull(lst) && !lst.isEmpty()) {
				respObtieneLista.setCount(lst.size());
				respObtieneLista.setItems(lst);
			}else {
				//response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No se encontró registros");
				respObtieneLista.setItems(new ArrayList<RespCriterioEvaluacion>());
				response.setPayload(respObtieneLista);
				return response;
			}
		} catch (Exception e) {
			//response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "Ocurrio algún error");
			respObtieneLista.setItems(new ArrayList<RespCriterioEvaluacion>());
			response.setPayload(respObtieneLista);
			return response;
		}

		return new RespBase<RespObtieneLista<RespCriterioEvaluacion>>().ok(respObtieneLista);  
	}

	@Override
	public RespBase<RespObtieneLista<RespCriterioEvaluacion>> listarInformeDetalle(Long entidadId, Long tipoInfoId) {
		RespBase<RespObtieneLista<RespCriterioEvaluacion>> response = new RespBase<>();
		RespObtieneLista<RespCriterioEvaluacion> respObtieneLista = new RespObtieneLista<>();
		try {
			List<RespCriterioEvaluacion> listResp = new ArrayList<>();
			RespCriterioEvaluacion resp = null;
			 
			List<InformeDetalle> list = informeDetalleRepository.findAllByTipoInfoId(tipoInfoId,entidadId);
			if(Objects.nonNull(list) && !list.isEmpty()) {
				for (InformeDetalle item : list) {
					resp = new RespCriterioEvaluacion();
					resp.setInformeDetalleId(item.getInformeDetalleId());
					resp.setTipoInfoId(item.getTipoInfo());
					resp.setTitulo(item.getTitulo());
					resp.setEntidadId(item.getEntidadId());
					resp.setEstadoRegistro(item.getEstadoRegistro());
					listResp.add(resp);
				}
				
				respObtieneLista.setCount(listResp.size());
				respObtieneLista.setItems(listResp);
			}else {
				List<Bonificacion> listBoni = bonificacionRepository.findByIdTipoInfo(tipoInfoId);
				if (Objects.nonNull(listBoni) && !listBoni.isEmpty()) {
					for (Bonificacion item : listBoni) {
						resp = new RespCriterioEvaluacion();
						resp.setInformeDetalleId(item.getBonificacionId());
						resp.setTipoInfoId(item.getTipoInfoId());
						resp.setTitulo(item.getTitulo());
						/** resp.setEntidadId(item.getEntidadId()); */
						resp.setEstadoRegistro(item.getEstadoRegistro());
						listResp.add(resp);
					}
					respObtieneLista.setCount(listResp.size());
					respObtieneLista.setItems(listResp);
				} else {
					//response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No se encontró registros");
					respObtieneLista.setItems(new ArrayList<RespCriterioEvaluacion>());
					response.setPayload(respObtieneLista);
					return response;
				}
			}
		} catch (Exception e) {
			//response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "Ocurrio algún error");
			respObtieneLista.setItems(new ArrayList<RespCriterioEvaluacion>());
			response.setPayload(respObtieneLista);
			return response;
		}

		return new RespBase<RespObtieneLista<RespCriterioEvaluacion>>().ok(respObtieneLista);  
	}
}
