package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.MaestraCebecera;

@Getter
@Setter
public class RespComboCabecera {
	private List<MaestraCebecera> listaCabecera;

}
