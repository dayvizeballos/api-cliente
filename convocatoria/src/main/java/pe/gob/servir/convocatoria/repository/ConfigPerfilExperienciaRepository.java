package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.gob.servir.convocatoria.model.ConfigPerfilExperienciaLaboral;
public interface ConfigPerfilExperienciaRepository extends JpaRepository<ConfigPerfilExperienciaLaboral, Long>{

}
