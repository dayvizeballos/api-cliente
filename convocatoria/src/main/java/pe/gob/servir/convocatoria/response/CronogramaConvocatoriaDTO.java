package pe.gob.servir.convocatoria.response;

import java.util.Date;

import lombok.Data;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.model.BaseCronograma;
import pe.gob.servir.convocatoria.util.ParametrosUtil;

@Data
public class CronogramaConvocatoriaDTO {
	private Integer basecronogramaId;
    private Long etapaId;
    private String desEtapa;
    private String descripcion;
    private String responsable;
    private String periodoInicio;
    private String periodoFin;
    
    public CronogramaConvocatoriaDTO(BaseCronograma item) {
    	this.basecronogramaId = item.getBasecronogramaId();
    	this.etapaId = item.getEtapaId().getMaeDetalleId();
    	this.desEtapa = item.getEtapaId().getDescripcion();
    	this.responsable = item.getResponsable();
    	this.periodoInicio = ParametrosUtil.convertirDateToString(item.getPeriodoini(),
				Constantes.FORMATO_FECHA_DD_MM_YYYY_HORA);
    	this.periodoFin = ParametrosUtil.convertirDateToString(item.getPeriodofin(),
				Constantes.FORMATO_FECHA_DD_MM_YYYY_HORA);
    }
}
