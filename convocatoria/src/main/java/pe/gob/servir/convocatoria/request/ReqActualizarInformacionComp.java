package pe.gob.servir.convocatoria.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@Getter
@Setter
public class ReqActualizarInformacionComp extends ReqGuardarInformacionComp {
	
	@NotNull(message = Constantes.CAMPO + " listIdBaseCompl " + Constantes.ES_OBLIGATORIO)
	private List<Long> listIdBaseCompl;
	
	@NotNull(message = Constantes.CAMPO + " listIdBaseComplDetalle " + Constantes.ES_OBLIGATORIO)
	private List<Long> listIdBaseComplDetalle;

}
