package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;
import pe.gob.servir.convocatoria.model.TblConfPerfSeccion;

@Data
public class RConfPerfilPesoDTO {
	
	private Long confPerfSeccionId;
	private Long tipoSeccion;
	private Long pesoSeccion;
	private String descripcionSeccion;
	
	public RConfPerfilPesoDTO (TblConfPerfSeccion peso) {
		this.confPerfSeccionId = peso.getConfPerfSeccionId();
		this.tipoSeccion = peso.getTipoSeccion();
		this.pesoSeccion = peso.getPesoSeccion();
		this.descripcionSeccion = peso.getDescripcionSeccion();
	}
	
	public RConfPerfilPesoDTO () {
		
	}
}
