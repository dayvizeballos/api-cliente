package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerPerfilConfigDTO {

	private Long perfilId;
	private String nombrePerfil;
	private Long estadoRmId;
	private String estadoRm;
	private String descEstadoRm;
	private Long estadoEcId;
	private String estadoEc;
	private String descEstadoEc;
	private Long entidadId;	
	private Long experienciaId;
	private Long baseId;
	private String codigoConvocatoria;
	private Long idRol;
	private String rol;
	private Long regimenId;
	private String regimen;
}
