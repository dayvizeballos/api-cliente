package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbl_perfil_formacion_academica", schema = "sch_convocatoria")
@Getter
@Setter
public class FormacionAcademica extends AuditEntity implements AuditableEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil_formacion_academica")
	@SequenceGenerator(name = "seq_perfil_formacion_academica", sequenceName = "seq_perfil_formacion_academica", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "perfil_formacion_academica_id")
	private Long formacionAcademicaId;


	@Transient
//	@Column(name = "perfil_id")
	private Long perfilId;

	@Column(name = "nivel_educativo_id")
	private Long nivelEducativoId;
	
	@Column(name = "estado_nivel_educativo_id")
	private Long estadoNivelEducativoId;
	
	@Column(name = "situacion_academica_id")
	private Long situacionAcademicaId;
	
	@Column(name = "estado_situacion_academica_id")
	private Long estadoSituacionAcademicaId;
	
	@Column(name = "nombre_grado")
	private String nombreGrado;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "fomarcionAcademica")
	@Column(nullable = true)
	@JsonManagedReference
	private List<CarreraFormacionAcademica> carreraFormacionAcademicas;

	@JoinColumn(name = "perfil_id")
	@ManyToOne(optional = false)
	@JsonBackReference
	private Perfil perfil;

	public FormacionAcademica() {

	}

}
