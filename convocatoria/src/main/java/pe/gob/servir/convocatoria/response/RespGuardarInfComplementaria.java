package pe.gob.servir.convocatoria.response;

import java.util.List;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.InformacionComplDTO;

@JGlobalMap
@Getter
@Setter
public class RespGuardarInfComplementaria {
	private String observaciones;
	private List<InformacionComplDTO> informes;
}
