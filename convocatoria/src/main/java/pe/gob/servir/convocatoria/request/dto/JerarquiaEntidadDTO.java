package pe.gob.servir.convocatoria.request.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class JerarquiaEntidadDTO {
	
	@NotNull(message = "Campo codigoNivel1 es obligatorio")
	@Valid
	private Long codigoNivel1;
	
	@NotNull(message = "Campo codigoNivel2 es obligatorio")
	@Valid
	private Long codigoNivel2;
	
	@NotNull(message = "Campo codigoNivel3 es obligatorio")
	@Valid
	private Long codigoNivel3;


}
