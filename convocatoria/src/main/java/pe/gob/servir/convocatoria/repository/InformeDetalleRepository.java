package pe.gob.servir.convocatoria.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import feign.Param;
import pe.gob.servir.convocatoria.model.InformeDetalle;

public interface InformeDetalleRepository extends JpaRepository<InformeDetalle, Long> {
	
	@Query("SELECT info FROM InformeDetalle info WHERE info.informeDetalleId=:informeDetalleId and info.estadoRegistro = '1' ")
	public Optional<InformeDetalle> findAllByIdAndEstadoRegistro(@Param("informeDetalleId")Long informeDetalleId);
	
	@Query("SELECT info FROM InformeDetalle info WHERE info.tipoInfo=:tipoInfoId and info.entidadId=:idEntidad and info.estadoRegistro = '1' ")
	public List<InformeDetalle> findAllByTipoInfoId(@Param("tipoInfoId")Long tipoInfoId, @Param("idEntidad")Long idEntidad);
}
