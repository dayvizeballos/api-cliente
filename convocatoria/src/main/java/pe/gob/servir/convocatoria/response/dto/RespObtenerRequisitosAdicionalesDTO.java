package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RespObtenerRequisitosAdicionalesDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long perfilId;
    private List<RequisitoAdicionalDTO> listaRequisitosAdicionales;

    @Data
    public static class RequisitoAdicionalDTO implements Serializable {
        private static final long serialVersionUID = 1L;
        private Long confPerfOtrosRequisitosId;
        private Long experienciaDetalleId;
        private Long perfilExperienciaId;
        private String descripcion;
        private Boolean requisitoMinimo;
        private Long tipoDatoExperId;
        private Long requisitoId;
        private Long baseId;
    }

}
