package pe.gob.servir.convocatoria.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lowagie.text.DocumentException;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.CssStyle;
import pe.gob.servir.convocatoria.common.HtmlConstantes;
import pe.gob.servir.convocatoria.common.VariablesSistema;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.model.Bonificacion;
import pe.gob.servir.convocatoria.model.BonificacionDetalle;
import pe.gob.servir.convocatoria.model.InformeDetalle;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.repository.BonificacionDetalleRepository;
import pe.gob.servir.convocatoria.repository.InformeDetalleRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCrearCriterioEvaluacioPdf;
import pe.gob.servir.convocatoria.request.ReqOrquestadorPdf;
import pe.gob.servir.convocatoria.request.dto.BonificacionPdfDTO;
import pe.gob.servir.convocatoria.request.dto.DetalleEvaluacionEntidadDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespEntidad;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespPdfInforme;
import pe.gob.servir.convocatoria.service.PdfConstructorService;
import pe.gob.servir.convocatoria.util.HtmlGeneradorUtil;
import pe.gob.servir.convocatoria.util.PDFGeneradorUtil;
import pe.gob.servir.convocatoria.util.Util;

@Service
public class PdfConstructorServiceImpl implements PdfConstructorService {

	private final InformeDetalleRepository informeDetalleRepository;
	private final BonificacionDetalleRepository bonificacionDetalleRepository;
	private final MaestraDetalleRepository maestraDetalleRepository;
	private final EntidadClient entidadRepository;
	private final VariablesSistema variablesSistema;
	private final EvaluacionServiceImpl evaluacionServiceImpl;
	
	@Autowired
	public PdfConstructorServiceImpl(InformeDetalleRepository informeDetalleRepository,
			BonificacionDetalleRepository bonificacionDetalleRepository,
			MaestraDetalleRepository maestraDetalleRepository,
			EntidadClient entidadRepository,
			VariablesSistema variablesSistema,
			EvaluacionServiceImpl evaluacionServiceImpl) {
		this.informeDetalleRepository = informeDetalleRepository;
		this.bonificacionDetalleRepository = bonificacionDetalleRepository;
		this.maestraDetalleRepository = maestraDetalleRepository;
		this.entidadRepository = entidadRepository;
		this.variablesSistema = variablesSistema;
		this.evaluacionServiceImpl = evaluacionServiceImpl;
	}

	@Override
	public byte[] getPdfFromInformeDetalle(Long idInformeDetalle) throws DocumentException, IOException {
		Optional<InformeDetalle> informeDetalle = informeDetalleRepository
				.findAllByIdAndEstadoRegistro(idInformeDetalle);
		String imgLogo = HtmlConstantes.IMG_SRC;
		StringBuilder sb = new StringBuilder(PDFGeneradorUtil.getHtmlTemplate());
		if (informeDetalle.isPresent()) {
			String logo = obtenerLogoPorEntidadId(informeDetalle.get().getEntidadId());
			imgLogo = StringUtils.replace(imgLogo, Constantes.PARAM_INDEX_0, logo);
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, imgLogo);
			
			String tituloDocumento = HtmlGeneradorUtil.buildTitlePdf(informeDetalle.get().getTitulo());
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, tituloDocumento);
			
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG,informeDetalle.get().getContenido());
			
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlConstantes.TAG_BR);
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlGeneradorUtil.alineacionDerecha(Util.fechaImpresion()));
		
		}
		else
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG,StringUtils.EMPTY);
		return PDFGeneradorUtil.getPdfByte(sb);
	}

	@Override
	public byte[] createCriterioEvaluacionPdf(ReqCrearCriterioEvaluacioPdf request)
			throws DocumentException, IOException, IllegalAccessException {
		Optional<InformeDetalle> informeDetalle = informeDetalleRepository
				.findAllByIdAndEstadoRegistro(request.getIdInformeDetalle());
		StringBuilder sb = new StringBuilder(PDFGeneradorUtil.getHtmlTemplate());
		String imgLogo = HtmlConstantes.IMG_SRC;
		if (informeDetalle.isPresent()) {
			String logo = obtenerLogoPorEntidadId(informeDetalle.get().getEntidadId());
			imgLogo = StringUtils.replace(imgLogo, Constantes.PARAM_INDEX_0, logo);
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, imgLogo);
			
			String tituloDocumento = HtmlGeneradorUtil.buildTitlePdf(informeDetalle.get().getTitulo());
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, tituloDocumento);
			
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, informeDetalle.get().getContenido());
 
			informeDetalle.get().getEntidadId();
			
			String theadersHtml = StringUtils.EMPTY;
			String tbodyHtml = StringUtils.EMPTY;
			String tfootHtml = StringUtils.EMPTY;
			
			RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> evaluaciones = evaluacionServiceImpl
					.findEvaluacionEntidadById(request.getBaseId(), informeDetalle.get().getEntidadId());
			if(evaluaciones.getStatus().getSuccess().booleanValue()) {
				evaluaciones.getPayload().getItems();
				if (!CollectionUtils.isEmpty(evaluaciones.getPayload().getItems())) {
					theadersHtml = HtmlGeneradorUtil.buildTableHeader(evaluaciones.getPayload().getItems().get(0));
					tbodyHtml = HtmlGeneradorUtil.buildTableBody(evaluaciones.getPayload().getItems());
					tfootHtml = HtmlGeneradorUtil.buildTableFooter(StringUtils.EMPTY);
				}
			}
			
			String tableHtml = HtmlGeneradorUtil.buildTableHtmlCode(theadersHtml, tbodyHtml, tfootHtml);
			
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, tableHtml);
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlConstantes.TAG_BR);
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlGeneradorUtil.alineacionDerecha(Util.fechaImpresion()));
		
		}
		
		HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlConstantes.TAG_BR);


		return PDFGeneradorUtil.getPdfByte(sb);
	}

	@Override
	public byte[] getPdfBonificacion(Long idInformeDetalle,Map<Long, String> mapDetalle ) throws DocumentException, IOException, IllegalAccessException {
		String logo = HtmlConstantes.DEFAULT_LOGO;
		Optional<InformeDetalle> informeDetalle = informeDetalleRepository
				.findAllByIdAndEstadoRegistro(idInformeDetalle);
		if(informeDetalle.isPresent())
			logo = obtenerLogoPorEntidadId(informeDetalle.get().getEntidadId());
		
		List<BonificacionDetalle> listDetBonificacion = bonificacionDetalleRepository.findAllByBonificacionId(idInformeDetalle);
		
		List<BonificacionPdfDTO> listDescripcionPdf = new ArrayList<>();
		
		StringBuilder sb = new StringBuilder(PDFGeneradorUtil.getHtmlTemplate());
		
		String imgLogo = HtmlConstantes.IMG_SRC;
		imgLogo = StringUtils.replace(imgLogo, Constantes.PARAM_INDEX_0, logo);
		HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, imgLogo);


		if(!listDetBonificacion.isEmpty()) {
			listDetBonificacion.stream().forEach(c-> {
				BonificacionPdfDTO dto = new BonificacionPdfDTO();
				dto.setAplicaSobre(mapDetalle.get(c.getAplicaId()));
				dto.setDescripcion(c.getDescripcion());
				dto.setNivel(StringUtils.defaultString(mapDetalle.get(c.getNivelId())));
				dto.setPorcentajeBonificacion(c.getPorcentajeBono().toString());
				listDescripcionPdf.add(dto);
			});
			
			listDescripcionPdf.sort(Comparator.comparing(BonificacionPdfDTO::getNivel));
			
			CssStyle cssStyle = new CssStyle();
			cssStyle.setTextAlign(HtmlConstantes.CENTER);
			
			Bonificacion bonificacion = listDetBonificacion.get(0).getBonificacion();
			
			String tituloDocumento = HtmlGeneradorUtil.buildTitlePdf("DE LAS BONIFICACIONES");
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, tituloDocumento);
			
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlGeneradorUtil.buildParagraph(bonificacion.getTitulo()));
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlGeneradorUtil.buildParagraph(bonificacion.getContenido()));
			
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlConstantes.TAG_BR);

			String theadersHtml = HtmlGeneradorUtil.buildTableHeader(listDescripcionPdf.get(0));
			String tbodyHtml = HtmlGeneradorUtil.buildTableBodyWithCss(listDescripcionPdf,cssStyle);
			String tableHtml = HtmlGeneradorUtil.buildTableHtmlCode(theadersHtml, tbodyHtml, null);
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, tableHtml);
			
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlConstantes.TAG_BR);
			HtmlGeneradorUtil.addCodeBeforeHtmlTag(sb, HtmlConstantes.CLOSE_BODY_TAG, HtmlGeneradorUtil.alineacionDerecha(Util.fechaImpresion()));
		}
		
		return PDFGeneradorUtil.getPdfByte(sb);
	}

	@Override
	public RespBase<RespPdfInforme> seleccionarPdfByTipoInforme(ReqBase<ReqOrquestadorPdf> request)
			throws DocumentException, IOException, IllegalAccessException {
		
		byte[] pdfByte = null;
		 
		List<MaestraDetalle> listaMaeDetalle = maestraDetalleRepository.findAll();
		Map<Long, String> mapDetalle = listaMaeDetalle.stream().filter(Objects::nonNull)
				.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));

		if(Objects.nonNull(mapDetalle)) {
			switch (mapDetalle.get(request.getPayload().getIdTipoInformacion())) {
			case Constantes.COD_TI_BONIFICACION:
				//bonificaciones
				pdfByte = getPdfBonificacion(request.getPayload().getIdInforme(),mapDetalle);
				break;
			case Constantes.COD_TI_CRITERIO_EVALUACION:
				//evaluacion
				ReqCrearCriterioEvaluacioPdf req =  new ReqCrearCriterioEvaluacioPdf();
				req.setIdInformeDetalle(request.getPayload().getIdInforme());
				req.setBaseId(request.getPayload().getBaseId());
				pdfByte = createCriterioEvaluacionPdf(req);
				break;
			default:
				//otros informes
				pdfByte = getPdfFromInformeDetalle(request.getPayload().getIdInforme());
				break;
			}
		}
		
		RespPdfInforme payload = new RespPdfInforme();
		payload.setPdfByte(pdfByte);
		
		return new RespBase<RespPdfInforme>().ok(payload);
	}
	
	private String obtenerLogoPorEntidadId(Long entidadId) {
		
		 RespBase<RespEntidad> response = entidadRepository.obtieneEntidad(entidadId);
		 String logo="src/main/resources/nologo.png";
		 if(response.getPayload().getEntidad().get(0).getLogo()!=null)
				logo = variablesSistema.fileServer + response.getPayload().getEntidad().get(0).getLogo();
		 return logo;
	}

}
