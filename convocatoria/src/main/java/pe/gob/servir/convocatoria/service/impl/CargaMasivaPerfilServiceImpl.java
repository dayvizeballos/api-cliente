package pe.gob.servir.convocatoria.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.loader.ConstantesExcel;

import pe.gob.servir.convocatoria.request.ReqCargaMasivaPerfil;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCargaMasiva;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.CargaMasivaPerfilService;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
public class CargaMasivaPerfilServiceImpl implements CargaMasivaPerfilService {

    @Autowired
    LoaderMasiva30057 loaderMasiva30057;
    
    @Autowired
    LoaderMasiva1401 loaderMasiva1401;
    
    @Autowired
    MaestraDetalleService maestraDetalleService;



    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
    public RespBase<RespCargaMasiva> uploadExcel(String model, MultipartFile[] files, MyJsonWebToken token) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ReqCargaMasivaPerfil modelDTO = mapper.readValue(model, ReqCargaMasivaPerfil.class);
        RespCargaMasiva payload = new RespCargaMasiva();


        switch (modelDTO.getCodRegimen().trim()){
        case Constantes.REGIMEN_LABORAL_30057:
        	
        	Arrays.stream(files).forEach(file -> {
        		try {
		                HashMap<String, Object> rpta = new HashMap<>();
		                modelDTO.setVersionExcel(ConstantesExcel.EXCEL_VERSION_XLSM);
		                if(modelDTO.getCodRegimen().equalsIgnoreCase(Constantes.REGIMEN_LABORAL_30057)) {
		                	rpta = loaderMasiva30057.cargarArchivoExcel30057(modelDTO, file.getInputStream(), token);
		                }	             
		                payload.setRegimen(modelDTO.getCodRegimen());
		                payload.setErrors((List<String>) rpta.get("lstErrores"));
		                payload.setCorrectos((Integer) rpta.get("correctos"));
		                payload.setIncorrectos((Integer) rpta.get("incorrectos"));
		                payload.setTotal((Integer) rpta.get("total"));
		            } catch (IOException ex) {
		                log.error(ex.getMessage());
		            }
        		});
        	
            break;
        case Constantes.REGIMEN_LABORAL_1401:

        	Arrays.stream(files).forEach(file -> {
        		try {
		                HashMap<String, Object> rpta = new HashMap<>();
		                modelDTO.setVersionExcel(ConstantesExcel.EXCEL_VERSION_XLSM);
		                rpta = loaderMasiva1401.cargarArchivoExcel1401(modelDTO, file.getInputStream(), token);
		                payload.setErrors((List<String>) rpta.get("lstErrores"));
		                payload.setCorrectos((Integer) rpta.get("correctos"));
		                payload.setIncorrectos((Integer) rpta.get("incorrectos"));
		                payload.setTotal((Integer) rpta.get("total"));
		            } catch (IOException ex) {
		                log.error(ex.getMessage());
		            }
        		});
        	
            break;
        default:
            break;
        }


        return new RespBase<RespCargaMasiva>().ok(payload);
    }
}
