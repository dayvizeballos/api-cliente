package pe.gob.servir.convocatoria.request;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.JerarquiaEntidadDTO;

@Getter
@Setter
public class ReqAsignaJerarquia {

	
	private Long entidadId;
	
	private List<JerarquiaEntidadDTO> lstJerarquia;
}
