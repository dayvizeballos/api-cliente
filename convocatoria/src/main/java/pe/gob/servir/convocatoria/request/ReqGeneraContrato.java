package pe.gob.servir.convocatoria.request;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.PostulanteDTO;

@Getter
@Setter
public class ReqGeneraContrato {
	
	private String tipoTrabajo;
	private Long tipoContrato;
	private List<PostulanteDTO> postulantes;

}
