package pe.gob.servir.convocatoria.response;

import lombok.Data;

@Data
public class RespConvocatoriaJob {
    private Long convocatoriaId;
    private Long baseId;

}
