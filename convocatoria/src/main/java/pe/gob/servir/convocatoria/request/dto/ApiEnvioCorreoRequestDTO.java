package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiEnvioCorreoRequestDTO {
    String body;
    String enviarTo;
    String asunto;
    String cc;
    String cco;

}
