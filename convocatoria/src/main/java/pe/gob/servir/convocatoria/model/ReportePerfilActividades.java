package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportePerfilActividades {

 
	private Integer id;
	private String descripcion;
	
 
}
