package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.ConfiguracionMaestra;
import pe.gob.servir.convocatoria.model.MaestraDetalleEntidad;

@Getter
@Setter
public class RespMaestraDetalleEntidad {

	private MaestraDetalleEntidad maestraDetalleEntidad;
	
	private ConfiguracionMaestra configMaestra;
}
