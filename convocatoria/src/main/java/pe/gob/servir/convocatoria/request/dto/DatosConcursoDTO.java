package pe.gob.servir.convocatoria.request.dto;


import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

import javax.validation.constraints.NotNull;

@JGlobalMap
@Getter
@Setter
public class DatosConcursoDTO {

    private Long datoConcursoId;
    private String nombre;

    private String objetivo;

    private Long organoResponsableId;

    private Long unidadOrganicaId;

    private Long organoEncargadoId;

    private String correo;

    private Integer nroVacantes;

    private Integer nroVacantesPerfil;

    private Long informeDetalleId;

    private Long tipoPracticaId;

    private String estado;
    private Long baseId;

    /*datos para Base*/
    @NotNull(message = Constantes.CAMPO + " baseRegimenId " + Constantes.ES_OBLIGATORIO)
    private Long baseRegimenId;

    @NotNull(message = Constantes.CAMPO + " baseModalidadId " + Constantes.ES_OBLIGATORIO)
    private Long baseModalidadId;

    @NotNull(message = Constantes.CAMPO + " baseTipoId " + Constantes.ES_OBLIGATORIO)
    private Long baseTipoId;

    private Long baseRolID;
    private Long baseGestorId;
    private Long baseCoordinadorId;

    @NotNull(message = Constantes.CAMPO + " baseTipoId " + Constantes.ES_OBLIGATORIO)
    private Long baseEntidadId;

    private String nombreGestor;
    private String nombreCoordinador;

    private Long informeEspecificaId;

    private String telefono;

    private String anexo;


}
