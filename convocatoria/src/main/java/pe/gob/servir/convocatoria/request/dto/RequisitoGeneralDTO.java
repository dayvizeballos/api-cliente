package pe.gob.servir.convocatoria.request.dto;


import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@JGlobalMap
@Getter
@Setter
public class RequisitoGeneralDTO {
    //private Long requisitoId;
    //private String descripcion;
    //private String cuestionario;
   // private Long informeDetalleId;
    @NotNull(message = "el campo baseId no puede ser null")
    private Long baseId;
    //private String estado;
    private List<DeclaracionJuradaDTO> declaracionJuradaDTOList;
}
