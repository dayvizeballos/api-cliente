package pe.gob.servir.convocatoria.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.InformacionComplDTO;

@JGlobalMap
@Getter
@Setter
public class ReqGuardarInformacionComp {
	
	@NotNull(message = Constantes.CAMPO + " baseId " + Constantes.ES_OBLIGATORIO)
	private Long baseId;
	@NotNull(message = Constantes.CAMPO + " criteriosEval " + Constantes.ES_OBLIGATORIO)
	private String observaciones;
	@NotNull(message = Constantes.CAMPO + " informes " + Constantes.ES_OBLIGATORIO)
	private List<InformacionComplDTO> informes;

}
