package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.ConfiguracionMaestra;


@Getter
@Setter
public class RespAsignarMaeDetalleEntidad {

	private ConfiguracionMaestra configuracionMaestra;
}
