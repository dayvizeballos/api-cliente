package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.request.dto.MaestraDetalleDto;

public interface MaestraDetalleRepositoryJdbc {

	 String findCorrelativoDetalle(Long cabeceraId);
	 Long findIdMaestraDetalle(String codigoCabecera, String codProg);
	 MaestraDetalleDto findMaestraDetalle (Long id);
}
