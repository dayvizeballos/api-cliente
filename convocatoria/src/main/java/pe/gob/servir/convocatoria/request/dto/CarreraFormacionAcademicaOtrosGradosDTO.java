package pe.gob.servir.convocatoria.request.dto;

import lombok.Data;

@Data
public class CarreraFormacionAcademicaOtrosGradosDTO {
    private Long confPerfFormacCarreraDetalleId;
    private Long perfilId;
    private Long grado;
    private Long  puntaje;
    private Long  baseId;


}
