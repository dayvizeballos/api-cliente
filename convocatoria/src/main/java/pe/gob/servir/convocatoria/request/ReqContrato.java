package pe.gob.servir.convocatoria.request;

import java.util.Date;

import lombok.Data;

@Data
public class ReqContrato {
	
	private String nroResolucion;
	private Date fechaResolucion;
	private String nroInforme;
	private Date fechaVinculacion;
	
    private String  periodoPrueba;
	private String resolResponOrh;
    private String nroNorma;
    private Long estadoId;
    
	/*private String  lugarPrestacion;
	private String sede;
	private String articulo;
    private String  nombrePuesto;
    private String  nombreUniOrganica;
    private String  nombreOrgano; // se usa too en convenio
    private String  orgUnOrganica;
    private String numero;
    private Date fechaSubscripcion;
      
    private String razonSocial;
    private String  jornalaboral;
    
    private String ruc;  
    private String siglas;
    private String subNivelcat;
    private String direccion;
    private String rol;
    private String responsableOrh;
    private String puestoResponsableOrh;
    private String nroDocResponsable;
    private Long tipoDocResponsable;
    
    private Long nroPosPueMeri;
    private String nombres;
    private String normaAproProy;
    private String nroConPubMeritos;
    private String nroConPub;
    
    private String apellidos;
    private Long postulanteId;
    private String dni;
    private Integer tipoDoc;
    private String nacionalidad;
    private String nivelCategoria;
    private String familia;
    private String domicilio;
    private Date fechaNacimiento;
    private String fecha;
    private String areaLabores;
    private String centroEstudios;
    private String direccionCentroEstudios;
    private String  direccionLabores;
    private String  ciclo;

    private Double  compeEconomica;
    private String  puestoAutoRepresentativa;
    private Long  tipoTrabajoId;
    private Long  tipoServicioId;
    private Long  tipoTramiteId;
    private Long  posCuadPuesto;
    private String periodoConvenio;
    
    
    private String tipoPractica;
    private String nombRepreUni;
    private String nroDocRepreUni;
	*/
}
