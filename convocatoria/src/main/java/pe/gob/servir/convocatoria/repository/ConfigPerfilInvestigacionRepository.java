package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.gob.servir.convocatoria.model.TblConfigPerfilInvestigacion;

public interface ConfigPerfilInvestigacionRepository extends JpaRepository<TblConfigPerfilInvestigacion, Long>{

}
