package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "tbl_base_movi", schema = "sch_base")
@Getter
@Setter
public class Movimiento extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tbl_base_movi")
    @SequenceGenerator(name = "seq_tbl_base_movi", sequenceName = "seq_tbl_base_movi", schema = "sch_base", allocationSize = 1)
    @Column(name = "base_movi_id")
    private Long movimientoId;

    @Column(name = "base_id")
    private Long baseId;

    @Column(name = "rol_id")
    private Long rolId;

    @Column(name = "entidad_id")
    private Long entidadId;

    @Column(name = "estado_old_id")
    private Long estadoOldId;

    @Column(name = "estado_new_id")
    private Long estadoNewId;

    @OneToMany(cascade= CascadeType.ALL , mappedBy = "movimiento")
    @Column(nullable = true)
    @JsonManagedReference
    private List<Observacion> observacionList;

}
