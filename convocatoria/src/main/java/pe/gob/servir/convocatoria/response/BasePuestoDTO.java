package pe.gob.servir.convocatoria.response;

import java.util.Date;

import lombok.Data;
import pe.gob.servir.convocatoria.model.Base;

@Data
public class BasePuestoDTO {
	
	private Long baseId;	
	private Long entidadId;	
	private String codigoConvocatoria;
	private Long rolId;
	private Long etapaId;
	private Long regimenId;
	private Long modalidadId;
	private Long tipoId;
	private Long practicaId;
	private Long gestorId;
	private Long coordinadorId;
	private String nombreGestor;
	private String nombreCoordinador;
	private String condicionEtapa1;
	private String condicionEtapa2;
	private String condicionEtapa3;
	private String condicionEtapa4;
	private String condicionEtapa5;
	private String condicionEtapa6;
	private String comentarioEtapa1;
	private String comentarioEtapa2;
	private String comentarioEtapa3;
	private String comentarioEtapa4;
	private String comentarioEtapa5;
	private String comentarioEtapa6;
	private String observacionEtapa6;
	private Date fechaPublicacion;
	private String url;
	private Integer correlativo;
	private String anio;
	private Date fechaReclutamiento;
	private Date fechaCierre;
	private String siglas;	
	private String distritoEntidad;
	private String nombreEntidad;
	private String urlWebEntidad;
	
	public BasePuestoDTO(Base base) {
		this.baseId = base.getBaseId();
		this.entidadId = base.getEntidadId();
		this.codigoConvocatoria = base.getCodigoConvocatoria();
		this.rolId = base.getRolId();
		this.etapaId = base.getEtapaId();
		this.regimenId = base.getRegimenId();
		this.modalidadId = base.getModalidadId();
		this.tipoId = base.getTipoId();
		this.practicaId = base.getPracticaId();
		this.gestorId = base.getGestorId();
		this.coordinadorId = base.getCoordinadorId();
		this.nombreGestor = base.getNombreGestor();
		this.nombreCoordinador = base.getNombreCoordinador();
		this.condicionEtapa1 = base.getCondicionEtapa1();
		this.condicionEtapa2 = base.getCondicionEtapa2();
		this.condicionEtapa3 = base.getCondicionEtapa3();
		this.condicionEtapa4 = base.getCondicionEtapa4();
		this.condicionEtapa5 = base.getCondicionEtapa5();
		this.condicionEtapa6 = base.getCondicionEtapa6();
		this.comentarioEtapa1 = base.getComentarioEtapa1();
		this.comentarioEtapa2 = base.getComentarioEtapa2();
		this.comentarioEtapa3 = base.getComentarioEtapa3();
		this.comentarioEtapa4 = base.getComentarioEtapa4();
		this.comentarioEtapa5 = base.getComentarioEtapa5();
		this.comentarioEtapa6 = base.getComentarioEtapa6();
		this.observacionEtapa6 = base.getObservacionEtapa6();
		this.fechaPublicacion = base.getFechaPublicacion();
		this.url = base.getUrl();
		this.correlativo = base.getCorrelativo();
		this.anio = base.getAnio();
		this.fechaReclutamiento = base.getFechaReclutamiento();
		this.fechaCierre = base.getFechaCierre();
		this.siglas = base.getSiglas();
		this.distritoEntidad = base.getDistritoEntidad();
		this.nombreEntidad = base.getNombreEntidad();
		this.urlWebEntidad =  base.getUrlWebEntidad();		
	}
}
