package pe.gob.servir.convocatoria.response;

import lombok.Data;

@Data
public class RespContratoConvenio {
    private Long idContrato;
    private String codigoConvocatoria;
    private String regimen;
    private String nombres;
    private String apellidos;
    private String perfil;
    private Integer tipoDoc;
    private String nroDocumento;
    private String fechaContrato;
    private String estado;
    private Long idEstado;
    private String codProEstado;
    private String estadoRegistro;
    private Long idPerfil;
    private Long postulanteSelId;
    private Long tipoTrabajo;
    private String tipoTrabajoDesc;
    private String archivoSuscrito;
    private Long baseId;


}
