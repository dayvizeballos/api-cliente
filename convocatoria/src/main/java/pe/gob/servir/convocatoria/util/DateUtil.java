package pe.gob.servir.convocatoria.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public static String fmtDt(Date date) {
        if (date == null)
            return "";
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    public static String fmtDt2(Date date , String format) {
        if (date == null)
            return "";
        return new SimpleDateFormat(format).format(date);
    }


    public static Date parse(SimpleDateFormat sdf,String date) {
        try {
            return sdf.parse(date);
        } catch (ParseException e) {

        }
        return null;
    }

   public static Date lessOrMoreDay(Date date , int cant) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, cant);
        return c.getTime();
    }

    public static int diffDias(Date dateFront , Date dateBack){
        Instant instantFromDate = dateFront.toInstant();
        Instant instantToDate = dateBack.toInstant();
        Long resp = ChronoUnit.DAYS.between(instantFromDate , instantToDate);
        return resp.intValue();
    }
}
