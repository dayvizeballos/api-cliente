package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportePerfilExDetalle {
	private String descripcion;
}
