package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;
import pe.gob.servir.convocatoria.model.TblConfPerfFormacEspecial;

@Data
public class RConfPerfilFormEspecialDTO {
	private Long especializacionId;
	private Long tipoConocimientoId;
	private String descTipo;
	private Long conocimientoId;
	private String descConocimiento;
	private Boolean flagReqMin;
	private Integer puntaje;
	
	public RConfPerfilFormEspecialDTO (TblConfPerfFormacEspecial oConfig) {
		this.especializacionId = oConfig.getConfPerfFormacEspecialId();
		this.tipoConocimientoId = oConfig.getTipoConocimientoId();
		this.descTipo = oConfig.getDescripcionTipo();
		this.conocimientoId = oConfig.getConocimientoId();
		this.descConocimiento = oConfig.getDescripcionConocimiento();
		this.flagReqMin = oConfig.getRequisitoMinimo();
		this.puntaje = oConfig.getPuntaje();
	}
	
	public RConfPerfilFormEspecialDTO() {
		
	}
}
