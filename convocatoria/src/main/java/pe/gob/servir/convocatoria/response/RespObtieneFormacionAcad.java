package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.ConocimientoDTO;
import pe.gob.servir.convocatoria.request.dto.FormacionAcademicaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilDTO;

@Getter
@Setter
public class RespObtieneFormacionAcad {

	private PerfilDTO perfil;
	
	private List<FormacionAcademicaDTO> lstFormacionAcademica;
	
	private List<ConocimientoDTO> lstConocimiento;
}
