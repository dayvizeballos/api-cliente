package pe.gob.servir.convocatoria.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

@Repository
public interface MaestraDetalleRepository extends JpaRepository<MaestraDetalle, Long>{

	@Query("SELECT maeDet FROM MaestraDetalle maeDet WHERE maeDet.descripcion LIKE %:descripcion% ORDER BY maeDet.maeDetalleId DESC")
	public List<MaestraDetalle> findAllDescripcion(@Param("descripcion")String descripcion);
	
	@Query("SELECT maeDet FROM MaestraDetalle maeDet WHERE maeDet.sigla LIKE %:sigla% ORDER BY maeDet.maeDetalleId DESC")
	public List<MaestraDetalle> findAllSigla(@Param("sigla")String sigla);

	@Query("SELECT maeDet.descripcion FROM MaestraDetalle maeDet WHERE maeDet.maeDetalleId =:maeDetalleId ORDER BY maeDet.maeDetalleId DESC")
	public List<MaestraDetalle> findMaestraDetalle(@Param("maeDetalleId")Long maeDetalleId);


	@Query("SELECT maeDet FROM MaestraDetalle maeDet WHERE maeDet.codProg=:codProg  ORDER BY maeDet.maeDetalleId DESC")
	public List<MaestraDetalle> findAllCodProgr(@Param("codProg")String codProg);

	@Query("SELECT maeDet FROM MaestraDetalle maeDet WHERE maeDet.estadoRegistro=:estado AND maeDet.maeCabeceraId=:idCabecera ORDER BY maeDet.maeDetalleId DESC")
	public List<MaestraDetalle> findAllEstado(@Param("estado")String estado,@Param("idCabecera")Long idCabecera);
	
//	@Query("SELECT maeDet FROM MaestraDetalle maeDet WHERE maeDet.maeCabeceraId=:idCabecera AND maeDet.estadoRegistro='1'")
//	public List<MaestraDetalle> findAllCombos(@Param("idCabecera")Long idCabecera);
	
	@Query("SELECT DISTINCT regimen\r\n" + 
			"FROM Jerarquia je\r\n" + 
			"INNER JOIN MaestraDetalle regimen on regimen.maeDetalleId = je.codigoNivel1\r\n" + 
			"WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 = :idCabecera AND je.estadoRegistro = '1' ORDER BY regimen.maeDetalleId DESC")
    public List<MaestraDetalle> findRegimenByid(@Param("idCabecera")Long idCabecera);
	
	@Query("SELECT DISTINCT regimen\r\n" + 
			"FROM Jerarquia je\r\n" + 
			"INNER JOIN MaestraDetalle regimen on regimen.maeDetalleId = je.codigoNivel2\r\n" + 
			"WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 = :idCabecera AND je.codigoNivel2=:idModalidad AND je.estadoRegistro = '1'")
    public MaestraDetalle findModalidadByid(@Param("idCabecera")Long idCabecera,@Param("idModalidad")Long idModalidad);
	
	@Query("SELECT DISTINCT regimen\r\n" + 
			"FROM Jerarquia je\r\n" + 
			"INNER JOIN MaestraDetalle regimen on regimen.maeDetalleId = je.codigoNivel3\r\n" + 
			"WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 = :idCabecera AND je.codigoNivel2=:idModalidad AND je.codigoNivel3=:idTipo AND je.estadoRegistro = '1'")
    public Optional<MaestraDetalle> findTipoByid(@Param("idCabecera")Long idCabecera,@Param("idModalidad")Long idModalidad,@Param("idTipo")Long idTipo);
	
	@Query("select mae_det from ConfiguracionMaestra conf \r\n" + 
			"inner join MaestraCebecera mae_cab on mae_cab.maeCabeceraId = conf.maeCabeceraId \r\n" + 
			"inner join MaestraDetalle mae_det on mae_det.maeDetalleId = conf.maeDetalleId \r\n" + 
			"where 1 = 1 and conf.maeCabeceraId = 1 and conf.estadoRegistro='1' and \r\n" + 
			"conf.entidadId=:idEntidad")
	public List<MaestraDetalle> findRegimenByEntidad(@Param("idEntidad")Long idEntidad);
	
	@Query("select mae_det from MaestraCebecera mae_cab \r\n" + 
			"inner join MaestraDetalle mae_det on mae_det.maeCabeceraId = mae_cab.maeCabeceraId "+
			"and mae_cab.codigoCabecera=:codCabecera \r\n" + 
			"where 1 = 1 and mae_det.estadoRegistro='1' and mae_cab.estadoRegistro='1'")
	public List<MaestraDetalle> findDetalleByCodeCabecera(@Param("codCabecera")String codCabecera);
	
	
	@Query("select mae_det from MaestraCebecera mae_cab \r\n" + 
			"inner join MaestraDetalle mae_det on mae_det.maeCabeceraId = mae_cab.maeCabeceraId "+
			"and mae_cab.codigoCabecera=:codCabecera \r\n" + 
			"where 1 = 1 and mae_cab.estadoRegistro='1' and mae_det.estadoRegistro= :estado ")
	public List<MaestraDetalle> findDetalleByCodeCabeceraEstado(@Param("codCabecera")String codCabecera, @Param("estado")String estado);
	
	@Query("select maeDet from MaestraCebecera maeCab \r\n" + 
			"inner join MaestraDetalle maeDet on maeDet.maeCabeceraId = maeCab.maeCabeceraId "+
			"and maeCab.codigoCabecera=:codCabecera \r\n" + 
			"where 1 = 1 and maeCab.estadoRegistro='1' and maeDet.estadoRegistro= '1' and maeDet.codProg= :codProg ")
	public List<MaestraDetalle> findDetalleByCodProg(@Param("codCabecera")String codCabecera, @Param("codProg")String codProg);
	
	
	

}
