package pe.gob.servir.convocatoria.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Entity
@Table(name = "tbl_conf_perf_expe_laboral_detalle", schema = "sch_convocatoria")
@Setter
@Getter
@ToString
@NamedQuery(name="ConfigPerfilExperienciaLaboralDetalle.findAll", query="SELECT t FROM ConfigPerfilExperienciaLaboralDetalle t")
public class ConfigPerfilExperienciaLaboralDetalle  extends AuditEntity implements Serializable {

	    private static final long serialVersionUID = 1L;
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_expe_laboral_detalle")
	    @SequenceGenerator(name = "seq_conf_perf_expe_laboral_detalle", sequenceName = "seq_conf_perf_expe_laboral_detalle", schema = "sch_convocatoria", allocationSize = 1)
	    
	    @Column(name = "conf_perf_expe_laboral_detalle_id")
	    private Long confPerfExpeLaboralDetalleId;
	    
	    @Column(name = "conf_perf_expe_laboral_id")
	    private Long confPerfExpeLaboralId;
	    	    
	    @Column(name = "desde_anios")
	    private Long desdeAnios;
	    
	    @Column(name = "hasta_anios")
	    private Long hastaAnios;
	   
	    @Column(name = "requisito_minimo")
	    private Boolean  requisitoMinimo;
	    
	    @Column(name = "requisito_minimo_desc")
	    private String requisitoMinimoDesc;
	    
	    @Column(name = "puntaje")
	    private Long  puntaje;

	    @Column(name = "tipo_modelo")
	    private Long  tipoModelo;
	    
	    @Column(name = "tipo_experiencia")
	    private Long tipoExperiencia;
	    public ConfigPerfilExperienciaLaboralDetalle() {
	    }
	}