package pe.gob.servir.convocatoria.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.response.RespBase;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParametrosUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(ParametrosUtil.class);
	
	public static <T> RespBase<T> setearResponse(RespBase<T> response, Boolean status, String mensaje) {
		response.getStatus().setSuccess(status);
		response.getStatus().getError().getMessages().add(mensaje);		
		return response;
	}

	public static String onlyName(String nameFile){
		int index = nameFile.lastIndexOf('.');
		return nameFile.substring(0, index);
	}
	
	public static String datePathReplaceRepositoryAlfresco(String pathAlfresco){
		Date fecha = new Date();
        //SimpleDateFormat formatDia = new SimpleDateFormat("dd");
        SimpleDateFormat formatMes = new SimpleDateFormat("MMMM");
        SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
        
        //String dia = formatDia.format(fecha);
        String mes = formatMes.format(fecha).toUpperCase();
        String anio = formatAnio.format(fecha);
        
        pathAlfresco = pathAlfresco.replace("{anio}", anio);
        pathAlfresco = pathAlfresco.replace("{mes}", mes);
       
        return pathAlfresco;
	}
	
	public static String convertirDateToString(Date date, String formato) {
		try {
			if(date != null) {
				SimpleDateFormat formateador = new SimpleDateFormat(formato);
				String fechaFormateada = formateador.format(date);
				return fechaFormateada;
			}else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Date getFechaActualToDate() {
		String formato = Constantes.FORMATO_FECHA_DD_MM_YYYY;
		SimpleDateFormat dateFormat = new SimpleDateFormat(formato); // "yyyy-MM-dd hh:mm:ss.SSS");
		Date parsedDate = new Date();
		try {
			parsedDate = dateFormat.parse(getFechaActual());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return parsedDate;
	}
	
	public static String getFechaActual() {
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat(Constantes.FORMATO_FECHA_DD_MM_YYYY);
		return formateador.format(ahora);
	}

	public static Date getOnlyDate(Date fecha) {
		
			String formato = Constantes.FORMATO_FECHA_DD_MM_YYYY;
			SimpleDateFormat dateFormat = new SimpleDateFormat(formato); 
			Date parsedDate = new Date();
			try {
				parsedDate = dateFormat.parse(dateFormat.format(fecha));
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			return parsedDate;
		
	}
	
	public static boolean validarMaxNroCaracteres(String cadena, int longMax) {
		return cadena.length() <= longMax;
	}
	
	public static boolean validarCantidadCarecteres(String valor, int longitudMinima) {
		try {
			boolean resp = (!StringUtils.isBlank(valor) && valor.length() >= longitudMinima);
			return resp;
		} catch (Exception e) {
		}
		return false;
	} 	
	
}
