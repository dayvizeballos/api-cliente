package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.FormacionAcademica;

import java.util.List;

@Repository
public interface FormacionAcademicaRepository extends JpaRepository<FormacionAcademica, Long>{

    @Query("SELECT FO FROM FormacionAcademica FO  WHERE FO.perfil.perfilId = :perfilId")
    List<FormacionAcademica> findListByPerfilId(@Param("perfilId")Long perfilId);

}
