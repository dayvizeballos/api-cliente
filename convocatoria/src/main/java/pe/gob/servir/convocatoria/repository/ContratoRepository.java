package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Contrato;
import pe.gob.servir.convocatoria.model.MaestraDetalle;

@Repository
public interface ContratoRepository extends JpaRepository<Contrato,Long> , JpaSpecificationExecutor<Contrato> {

	@Query("select case when max(c.numero) is null then 1 else (max(c.numero) + 1) end "
			+ "from Contrato c where c.tipoTrabajoId = :tipoTrabajoId and c.anio = :anio and c.entidadId = :entidadId "
			+ "and c.estadoRegistro = '1' ")
	public int findMaxContrato(@Param("tipoTrabajoId")MaestraDetalle tipoTrabajoId, @Param("anio")int anio, @Param("entidadId")Long entidadId);

}
