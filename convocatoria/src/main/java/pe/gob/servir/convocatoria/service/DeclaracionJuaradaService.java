package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.dto.RespObtenerDDJJByPerfilIdDTO;

public interface DeclaracionJuaradaService {

    RespBase<RespObtieneLista> listarDeclaraJuaradaEspecifica (Long baseId);

    RespBase<RespObtenerDDJJByPerfilIdDTO> listarDeclaraJuradaByPerfilId (Long idPerfil, Long baseId);

}
