package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReporteDatosConcurso {
	
	private String nombre;;
	private String objetivo;
	private Integer nroVacantes;
	private String organo;
	private String unidadOrganica;
	private String organoSeleccion;
	private String baseLegal;
	private String correo;
	private String baseLegalEspecifico;
	private String telefonoContacto;

}
