package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class CarreraFormacionAcademicaDTO {

	private Long carreraFormacionAcademicaId;
	
	private Long carreraId;
	
	private String estado;
	
	private String descripcion;
	
	//private Long maedetalleId;


	@Override
	public String toString() {
		return "CarreraFormacionAcademicaDTO{" +
				"carreraFormacionAcademicaId=" + carreraFormacionAcademicaId +
				", carreraId=" + carreraId +
				", estado='" + estado + '\'' +
				", descripcion='" + descripcion + '\'' +
				'}';
	}
}
