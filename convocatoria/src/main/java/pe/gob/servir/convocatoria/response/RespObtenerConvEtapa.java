package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Data;
import pe.gob.servir.convocatoria.request.dto.CorreoDTO;

@Data
public class RespObtenerConvEtapa {

	private List<CorreoDTO> convocatorias;
}
