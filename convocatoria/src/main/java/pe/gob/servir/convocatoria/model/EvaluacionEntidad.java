package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_evaluacion_entidad", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
public class EvaluacionEntidad extends AuditEntity implements AuditableEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_evaluacion_entidad")
	@SequenceGenerator(name = "seq_evaluacion_entidad", sequenceName = "seq_evaluacion_entidad", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "evaluacion_entidad_id")
	private Long evaluacionEntidadId;
	
	@Column(name = "entidad_id")
	private Long entidadId;
	
	@Column(name = "jerarquia_id")
	private Long jerarquiaId;
	
	@Column(name = "tipo_evaluacion_id")
	private Long tipoEvaluacionId;
	
	@Column(name = "orden")
	private Integer orden;
	
	@Column(name = "peso")
	private Integer peso;
	
	@Column(name = "puntaje_minimo")
	private Integer puntajeMinimo;
	
	@Column(name = "puntaje_maximo")
	private Integer puntajeMaximo;
	
	@Column(name = "ind_valida_servir")
	private String indValidaServir;
	
	@Column(name = "evaluacion_origen_id")
	private Long evaluacionOrigenId;
	
}
