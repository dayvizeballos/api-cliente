package pe.gob.servir.convocatoria.model;

import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_perfil_funcion", schema = "sch_convocatoria")
@NamedQuery(name="PerfilFuncion.findAll", query="SELECT t FROM PerfilFuncion t")
@Getter
@Setter
public class PerfilFuncion extends AuditEntity implements AuditableEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil_funcion")
	@SequenceGenerator(name = "seq_perfil_funcion", sequenceName = "seq_perfil_funcion", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "perfil_funcion_id")
	private Long perfilFuncionId;
	
	@Column(name = "perfil_id")
	private Long perfilId;
	
	@Column(name = "condicion_atipica")
	private String condicionAticipica;
	
	@Column(name = "periocidad_condicion_atipica_id")
	private Long periocidadCondicionAtipicaId;
	
	@Column(name = "sustento_condicion_atipica")
	private String sustentoCondicionAtipica;
	
	@Column(name = "coordinacion_interna")
	private String coordinacionInterna;
	
	@Column(name = "coordinacion_externa")
	private String coordinacionExterna;	
	
	//@Transient
	@OneToMany(cascade= CascadeType.ALL , fetch = FetchType.LAZY , mappedBy="perfilFuncion")
	private List<FuncionDetalle> funcionDetalles;

	@Column(name = "lstservidorcivil")
	private String servidorCiviles;
}
