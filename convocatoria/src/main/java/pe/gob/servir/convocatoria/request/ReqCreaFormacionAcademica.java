package pe.gob.servir.convocatoria.request;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.ConocimientoDTO;
import pe.gob.servir.convocatoria.request.dto.FormacionAcademicaDTO;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class ReqCreaFormacionAcademica {

	@NotNull(message = Constantes.CAMPO + " perfilId " + Constantes.ES_OBLIGATORIO)
	private Long perfilId; 
	
	private Long regimenLaboralId;
	
	private String indColegiatura;
	
	private String indHabilitacionProf;

	private String observaciones;
	
	private List<FormacionAcademicaDTO> lstFormacionAcademica;
	
	private List<ConocimientoDTO> lstConocimientos;


	@Override
	public String toString() {
		return "ReqCreaFormacionAcademica{" +
				"perfilId=" + perfilId +
				", regimenLaboralId=" + regimenLaboralId +
				", indColegiatura='" + indColegiatura + '\'' +
				", indHabilitacionProf='" + indHabilitacionProf + '\'' +
				", observaciones='" + observaciones + '\'' +
				'}';
	}
}
