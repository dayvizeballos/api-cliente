package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BaseHorario;
import pe.gob.servir.convocatoria.model.BasePerfil;
import pe.gob.servir.convocatoria.queue.producer.ReporteConvocatoriaProducer;
import pe.gob.servir.convocatoria.repository.BasePerfilRepository;
import pe.gob.servir.convocatoria.repository.BasePerfilRepositoryJdbc;
import pe.gob.servir.convocatoria.repository.BaseRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.BaseHorarioDTO;
import pe.gob.servir.convocatoria.request.dto.BasePerfilDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespBasePerfil;
import pe.gob.servir.convocatoria.response.dto.ReporteConvocatoriaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.BaseConverIF;
import pe.gob.servir.convocatoria.service.BasePerfilService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

@Service
@Slf4j
public class BasePerfilServiceImpl  implements BasePerfilService {

	@Autowired
	private BasePerfilRepository basePerfilRepository;

	@Autowired
	private BaseRepository baseRepository;
	
	@Autowired
	private BasePerfilRepositoryJdbc basePerfilRepositoryJdbc;

	@Autowired
	private ReporteConvocatoriaProducer reporteConvocatoriaProducer;
	
	@Override
	public RespBase<RespBasePerfil> creaBasePerfil(ReqBase<BasePerfilDTO> request, MyJsonWebToken token) {
		BasePerfil basePerfil = new BasePerfil();
		BasePerfilDTO basePerfilDTO = new BasePerfilDTO();
		RespBase<RespBasePerfil> response = new RespBase<>();
		List<BaseHorario> lstBaseHorario=  new ArrayList<>();
		List<BaseHorarioDTO> lstBaseHorarioDTO = null;
		Optional<Base> baseExiste = baseRepository.findByidBase(request.getPayload().getBaseId());

        if (!baseExiste.isPresent()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Base con el id " + request.getPayload().getBaseId() + " no existe");
            return response;
        }
        
        Base base = new Base();
        base.setBaseId(request.getPayload().getBaseId());
        
		basePerfil = BaseConverIF.convertDtoBasePerfil.apply(request.getPayload());
		basePerfil.setCampoSegIns(token.getUsuario().getUsuario() , Instant.now());
		basePerfil.setBase(base);
		
		if(request.getPayload().getBaseHorarioDTOList() != null && !request.getPayload().getBaseHorarioDTOList().isEmpty()) {
		
			for(BaseHorarioDTO b : request.getPayload().getBaseHorarioDTOList()) {
				BaseHorario baseHorario = BaseConverIF.convertDtoBaseHorario.apply(b);
				baseHorario.setBasePerfil(basePerfil);
				baseHorario.setCampoSegIns(token.getUsuario().getUsuario() , Instant.now());
				lstBaseHorario.add(baseHorario);
			}
		}
		basePerfil.setBaseHorarioList(lstBaseHorario);
		basePerfilRepository.save(basePerfil);

		/**
		 * QUEUE REPORTE ETAPA 3
		 */
		if (basePerfil.getBasePerfilId()!= null)
		 sendQueueEtapa3(basePerfil);

		RespBasePerfil payload = new RespBasePerfil();
		basePerfilDTO = basePerfilRepositoryJdbc.getBasePerfil(basePerfil.getBasePerfilId());
		lstBaseHorarioDTO = basePerfilRepositoryJdbc.getBaseHorarioList(basePerfil.getBasePerfilId());
		basePerfilDTO.setBaseHorarioDTOList(lstBaseHorarioDTO);
		payload.setBasePerfil(basePerfilDTO);
		
		return new RespBase<RespBasePerfil>().ok(payload);
	}

	/**
	 * QUEUE REPORTE ETAPA 3
	 * @param basePerfil
	 */
	private void sendQueueEtapa3(BasePerfil basePerfil) {
		try {
			ReporteConvocatoriaDTO dto = new ReporteConvocatoriaDTO();
			dto.setBaseId(basePerfil.getBase().getBaseId());
			dto.setDepartamentoId(basePerfil.getDepaId());
			dto.setDescDepartamento(basePerfil.getDescDepa());
			dto.setEtapaQueue(Constantes.ETAPA_BASE_PERFIL);
			reporteConvocatoriaProducer.sendMessageBase(dto);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	@Override
	public RespBase<RespBasePerfil> obtenerBasePerfil(Long basePerfilId) {
		
		 RespBase<RespBasePerfil> response = new RespBase<>();
		 List<BaseHorarioDTO> lstBaseHorario = null;
	        if (Util.isEmpty(basePerfilId)) {
	            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
	                    "el id de base perfiles obligatorio");
	            return response;
	        }

	        Optional<BasePerfil> basePerfilOptional = basePerfilRepository.findByIdBasePerfil(basePerfilId);
	        if (!basePerfilOptional.isPresent()) {
	            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
	                    "Base Perfil no existe con el id: " + basePerfilId);
	            return response;
	        }
	        
	        RespBasePerfil payload = new RespBasePerfil();
	        BasePerfilDTO basePerfil = basePerfilRepositoryJdbc.getBasePerfil(basePerfilId);
	        lstBaseHorario = basePerfilRepositoryJdbc.getBaseHorarioList(basePerfilId);
	        basePerfil.setBaseHorarioDTOList(lstBaseHorario);
	        payload.setBasePerfil(basePerfil);	       
	        return new RespBase<RespBasePerfil>().ok(payload);
		
	}

	@Override
	public RespBase<RespBasePerfil> actualizarBasePerfil(ReqBase<BasePerfilDTO>request,MyJsonWebToken token, Long basePerfilId) {

		RespBase<RespBasePerfil> response = new RespBase<>();
		BasePerfil basePerfil = null;
		BasePerfilDTO basePerfilDTO = new BasePerfilDTO();
		List<BaseHorarioDTO> lstBaseHorarioDTO = null;
		List<BaseHorario> lstBaseHorario = new ArrayList<>();
		if (Util.isEmpty(basePerfilId)) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
					"el id de base perfiles obligatorio");
			return response;
		}

		Optional<BasePerfil> basePerfilFind = basePerfilRepository.findByIdBasePerfil(basePerfilId);
		if (!basePerfilFind.isPresent()) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
					"Base Perfil no existe con el id: " + basePerfilId);
			return response;
		}

		Optional<Base> baseFind = baseRepository.findByidBase(request.getPayload().getBaseId());
		if (!baseFind.isPresent()) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
					"Base con el id " + request.getPayload().getBaseId() + " no existe");
			return response;
		}

		Base base = new Base();
		base.setBaseId(request.getPayload().getBaseId());
		basePerfil = BaseConverIF.convertDtoBasePerfil.apply(request.getPayload());
		basePerfil.setCampoSegUpd((Strings.isEmpty(request.getPayload().getEstado()) ? EstadoRegistro.ACTIVO.getCodigo() : request.getPayload().getEstado()),
				token.getUsuario().getUsuario(), Instant.now());
		basePerfil.setBasePerfilId(basePerfilId);
		basePerfil.setBase(base);

		if (request.getPayload().getBaseHorarioDTOList() != null && !request.getPayload().getBaseHorarioDTOList().isEmpty()) {

			for (BaseHorarioDTO b : request.getPayload().getBaseHorarioDTOList()) {
				BaseHorario baseHorario = BaseConverIF.convertDtoBaseHorario.apply(b);
				baseHorario.setBasePerfil(basePerfil);
				if (baseHorario.getBaseHorarioId() == null) {
					baseHorario.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				} else {
					baseHorario.setCampoSegUpd(b.getEstado(), token.getUsuario().getUsuario(), Instant.now());
				}
				lstBaseHorario.add(baseHorario);
			}
		}

		basePerfil.setBaseHorarioList(lstBaseHorario);
		basePerfilRepository.save(basePerfil);

		RespBasePerfil payload = new RespBasePerfil();
		basePerfilDTO = basePerfilRepositoryJdbc.getBasePerfil(basePerfilId);
		lstBaseHorarioDTO = basePerfilRepositoryJdbc.getBaseHorarioList(basePerfilId);
		basePerfilDTO.setBaseHorarioDTOList(lstBaseHorarioDTO);
		payload.setBasePerfil(basePerfilDTO);

		return new RespBase<RespBasePerfil>().ok(payload);
	}
	
}
