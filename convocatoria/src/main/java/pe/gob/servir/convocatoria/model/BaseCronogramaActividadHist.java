package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_crono_actividad_hist", schema = "sch_base")
@Getter
@Setter
public class BaseCronogramaActividadHist extends AuditEntity implements AuditableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_cronograma_act_hist")
    @SequenceGenerator(name = "seq_base_cronograma_act_hist", sequenceName = "seq_base_cronograma_act_hist", schema = "sch_base", allocationSize = 1)
    @Column(name = "crono_actividad_id_hist")
    private Long actividadId;

    @JoinColumn(name = "cronograma_hist_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BaseCronogramaHist baseCronogramaHist;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_ini")
    private Date fechaIni;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_fin")
    private Date fechaFin;

    @Column(name = "hora_ini")
    private String horaIni;

    @Column(name = "hora_fin")
    private String horaFin;

    @Column(name = "responsable")
    private String respomsable;

    @Column(name = "descripcion")
    private String descripcion;

    @JoinColumn(name = "tipo_actividad")
    @ManyToOne()
    private MaestraDetalle tipoActividad;

}
