package pe.gob.servir.convocatoria.request;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.JerarquiaDTO;

@Getter
@Setter
public class ReqCreaJerarquia {

	@NotNull(message = Constantes.CAMPO + " jerarquia " + Constantes.ES_OBLIGATORIO)
	@Valid
	private JerarquiaDTO jerarquia;
}
