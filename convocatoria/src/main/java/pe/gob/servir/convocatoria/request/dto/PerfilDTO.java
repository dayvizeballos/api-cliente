package pe.gob.servir.convocatoria.request.dto;


import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@Getter
@Setter
public class PerfilDTO {

	private Long perfilId;
	
	@NotNull(message = Constantes.CAMPO + " entidadId " + Constantes.ES_OBLIGATORIO)
	private Long entidadId;
	
	@NotNull(message = Constantes.CAMPO + " regimenLaboralId " + Constantes.ES_OBLIGATORIO)
	private Long regimenLaboralId;

	private String codigoRegimen;

	private Long organoId;
	private String  nombreOrgano;
	private String siglaOrgano;
	private Long unidadOrganicaId;
	private String nombreUnidadOrganica;
	private String unidadFuncional;
	private String nivelOrganizacional;
	private Long servidorCivilId;
	private Long familiaPuestoId;
	private Long rolId;
	private Long nivelCategoriaId;
	private Long puestoTipoId;
	private String subNivelsubCategoria;
	private String dependenciaFuncional;
	private String dependenciaJerarquica;
	private Long servidorCivilReporteId;
	private Integer nroPosicionesCargo;
	private String puestoCodigo;
	private String nombrePuesto;
	private String misionPuesto;
	private Integer nroPosicionesPuesto;
	private String codigoPosicion;
	private String puestosCargo;
	private Long tipoPracticaId;
	private Long condicionPracticaId;
	private String indColegiatura;
	private String indHabilitacionProf;
	private String puestoEstructural;
	private String observaciones;

}
