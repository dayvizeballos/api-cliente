package pe.gob.servir.convocatoria.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Picture {
    private String name;
    private String type;
    private String source;
    private long size;
}
