package pe.gob.servir.convocatoria.exception;

public class PdfException extends FileException {
    private static final String DESCRIPTION = "";

    public PdfException(String detail) {
        super(DESCRIPTION + "" + detail);
    }

}
