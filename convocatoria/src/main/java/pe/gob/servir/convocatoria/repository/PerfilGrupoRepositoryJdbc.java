package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.model.PerfilGrupo;

import java.util.List;

public interface PerfilGrupoRepositoryJdbc {
    List<PerfilGrupo> findAllPerfilGrupo(Long codigo);

}
