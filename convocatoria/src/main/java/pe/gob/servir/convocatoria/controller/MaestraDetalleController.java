package pe.gob.servir.convocatoria.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.VariablesSistema;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraDetalle;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespComboDetalleMaestra;
import pe.gob.servir.convocatoria.response.RespMaestraDetalle;
import pe.gob.servir.convocatoria.response.RespObtieneMaestraDetalle;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;


@RestController
@Tag(name = "Maestra Detalle", description = "")
public class MaestraDetalleController {

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	MaestraDetalleService maestraDetalleService;
	
	@Autowired
	VariablesSistema variablesSistema;
	
	@Operation(summary = "Crea una maestra detalle", description = "Crea una maestra detalle", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType =  MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType =  MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
			}) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/maestraDetalle" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespMaestraDetalle>> registrarMaestraDetalle(@PathVariable String access,
			@Valid @RequestBody ReqBase<ReqMaestraDetalle> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespMaestraDetalle> response = maestraDetalleService.guardarMaestraDetalle(request, jwt, null);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra detalles", description = Constantes.SUM_OBT_LIST+"maestra detalles", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraDetalle"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneMaestraDetalle>> listarMaestraDetalles(
			@PathVariable String access,
			@RequestParam(value = "idCabecera", required = false) Long idCabecera,
			@RequestParam(value = "texto", required = false) String texto,
			@RequestParam(value = "sigla", required = false) String sigla,
			@RequestParam(value = "estado", required = false) String estado,
			@RequestParam(value = "codcabecera", required = false) String codcabecera,
			@RequestParam(value = "codProg", required = false) String codProg
			) {
		RespBase<RespObtieneMaestraDetalle> response = maestraDetalleService.obtenerMaestraDetalle(idCabecera,texto,sigla,estado , codcabecera,codProg);
		
		return ResponseEntity.ok(response);		
	}
		
	@Operation(summary = "Inactiva una maestra detalle", description = "Inactiva una maestra detalle", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}) })
	@DeleteMapping(path = { Constantes.BASE_ENDPOINT + "/maestraDetalle/{maeDetalleId}" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> eliminarMaestraDetalle(@PathVariable String access,
			@PathVariable Long maeDetalleId,
			@RequestParam(value = "estado", required = true) String estado){
	
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> response = maestraDetalleService.eliminarMaestraDetalle(jwt, maeDetalleId, estado);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Actualiza una maestra detalle", description = "Actualiza una maestra detalle", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/maestraDetalle/{maeDetalleId}" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespMaestraDetalle>> actualizarMaestraDetalle(@PathVariable String access,
			@PathVariable Long maeDetalleId, @Valid @RequestBody ReqBase<ReqMaestraDetalle> request){
		
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<RespMaestraDetalle> response = maestraDetalleService.guardarMaestraDetalle(request, jwt, maeDetalleId);
		return ResponseEntity.ok(response);
	}
	
//	
//	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra detalles by Regimen Laboral", description = Constantes.SUM_OBT_LIST+"maestra detalles by Regimen Laboral", tags = { "" },
//			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
//	@ApiResponses(value = { 
//			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
//			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
//			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
//	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraDetalle/combo/regimen"}, 
//				produces = { MediaType.APPLICATION_JSON_VALUE })
//	public ResponseEntity<RespBase<RespComboDetalleMaestra>> comboMaestraDetallesByRegimen(
//			@PathVariable String access) {
//		RespBase<RespComboDetalleMaestra> response = maestraDetalleService.comboMaestraDetalle(variablesSistema.CABECERA_REGIMEN_LABORAL);
//		
//		return ResponseEntity.ok(response);		
//	}
//	
//	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra detalles by Modalidad", description = Constantes.SUM_OBT_LIST+"maestra detalles by Modalidad", tags = { "" },
//			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
//	@ApiResponses(value = { 
//			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
//			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
//			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
//	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraDetalle/combo/modalidad"}, 
//				produces = { MediaType.APPLICATION_JSON_VALUE })
//	public ResponseEntity<RespBase<RespComboDetalleMaestra>> comboMaestraDetallesByModalidad(
//			@PathVariable String access) {
//		RespBase<RespComboDetalleMaestra> response = maestraDetalleService.comboMaestraDetalle(variablesSistema.CABECERA_MODALIDAD);
//		
//		return ResponseEntity.ok(response);		
//	}
//	
//	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra detalles by Tipo", description = Constantes.SUM_OBT_LIST+"maestra detalles by Tipo", tags = { "" },
//			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
//	@ApiResponses(value = { 
//			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
//			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
//			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
//	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraDetalle/combo/tipo"}, 
//				produces = { MediaType.APPLICATION_JSON_VALUE })
//	public ResponseEntity<RespBase<RespComboDetalleMaestra>> comboMaestraDetallesByTipo(
//			@PathVariable String access) {
//		RespBase<RespComboDetalleMaestra> response = maestraDetalleService.comboMaestraDetalle(variablesSistema.CABECERA_TIPO);
//		
//		return ResponseEntity.ok(response);		
//	}
//	
//	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra detalles by Evaluaciones", description = Constantes.SUM_OBT_LIST+"maestra detalles by Evaluaciones", tags = { "" },
//			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
//	@ApiResponses(value = { 
//			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
//			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
//			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
//	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraDetalle/combo/evaluaciones"}, 
//				produces = { MediaType.APPLICATION_JSON_VALUE })
//	public ResponseEntity<RespBase<RespComboDetalleMaestra>> comboMaestraDetallesByEvaluaciones(
//			@PathVariable String access) {
//		RespBase<RespComboDetalleMaestra> response = maestraDetalleService.comboMaestraDetalle(variablesSistema.CABECERA_EVALUACIONES);
//		
//		return ResponseEntity.ok(response);		
//	}
	
	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra detalles regimen by entidad", description = Constantes.SUM_OBT_LIST+"maestra detalles regimen by entidad", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraDetalle/regimen/entidad/{idEntidad}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespComboDetalleMaestra>> comboMaestraDetallesRegimenByEntidad(
			@PathVariable String access,
			@PathVariable Long idEntidad) {
		RespBase<RespComboDetalleMaestra> response = maestraDetalleService.comboMaestraDetalleByEntidad(idEntidad);
		
		return ResponseEntity.ok(response);		
	}

//	@Operation(summary = Constantes.SUM_OBT_LIST+"maestra detalles by tipo informe", description = Constantes.SUM_OBT_LIST+"maestra detalles by tipo informe", tags = { "" },
//			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
//	@ApiResponses(value = { 
//			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
//			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
//			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
//					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
//	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/maestraDetalle/combo/tipoInforme"}, 
//				produces = { MediaType.APPLICATION_JSON_VALUE })
//	public ResponseEntity<RespBase<RespComboDetalleMaestra>> comboMaestraDetallesByTipoInforme(
//			@PathVariable String access) {
//		RespBase<RespComboDetalleMaestra> response = maestraDetalleService.comboMaestraDetalleTipoInforme();
//		
//		return ResponseEntity.ok(response);		
//	}
	
}
