package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_base_evaluacion_detalle", schema = "sch_base")
@Getter
@Setter
public class BaseEvaluacionDetalle extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_eval_detalle")
    @SequenceGenerator(name = "seq_base_eval_detalle", sequenceName = "seq_base_eval_detalle", schema = "sch_base", allocationSize = 1)
    @Column(name = "evaluacion_detalle_id")
    private Long evaluacionDetalleId;

    @JoinColumn(name = "evaluacion_entidad_id")
    @ManyToOne(optional = false)
    private EvaluacionEntidad evaluacionEntidad; //1

    @JoinColumn(name = "informe_detalle_id")
    @ManyToOne(optional = false)
    private InformeDetalle informeDetalle; //2


    @JoinColumn(name="evaluacion_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private BaseEvaluacion baseEvaluacion; //1

    @Column(name = "peso")
    private Long peso;

    @Column(name = "puntaje_minimo")
    private Long puntajeMinimo;

    @Column(name = "puntaje_maximo")
    private Long puntajeMaximo;
}
