package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.EvaluacionEntidad;

@Getter
@Setter
public class RespActualizaEvaluacionEntidad {
	
	private List<EvaluacionEntidad> listaEvaluacionEntidad;

}
