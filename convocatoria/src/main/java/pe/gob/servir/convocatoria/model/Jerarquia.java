package pe.gob.servir.convocatoria.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_jerarquia", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
public class Jerarquia  extends AuditEntity implements AuditableEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_jerarquia")
	@SequenceGenerator(name = "seq_jerarquia", sequenceName = "seq_jerarquia", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "jerarquia_id")
	private Long jerarquiaId;
	
	@Column(name = "codigo_nivel1")
	private Long codigoNivel1;
	
	@Column(name = "codigo_nivel2")
	private Long codigoNivel2;
	
	@Column(name = "codigo_nivel3")
	private Long codigoNivel3;
	
	@Column(name = "codigo_nivel4")
	private Long codigoNivel4;
	
	@Transient
	private List<MaestraDetalle> listaMoDetalles; 
	
	@Transient
	private List<Evaluacion> listaEvaluacion;
	
}
