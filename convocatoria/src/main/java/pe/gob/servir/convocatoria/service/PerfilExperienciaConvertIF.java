package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.model.PerfilExperiencia;
import pe.gob.servir.convocatoria.model.PerfilExperienciaDetalle;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDetalleDTO;


import java.util.function.Function;

@FunctionalInterface
public interface PerfilExperienciaConvertIF {
    public String hellow ();

     Function<PerfilExperienciaDTO, PerfilExperiencia> convertPerfilExperiencia = (PerfilExperienciaDTO p) -> {
        PerfilExperiencia perfilExperiencia = new PerfilExperiencia();
        perfilExperiencia.setPerfilId(p.getPerfilId());
        perfilExperiencia.setId(p.getId() == null ? null : p.getId());
        perfilExperiencia.setAnioExpTotal(p.getAnioExpTotal() == null ? null : p.getAnioExpTotal());
        perfilExperiencia.setMesExpTotal(p.getMesExpTotal()== null ? null : p.getMesExpTotal());
        perfilExperiencia.setAnioExReqPuesto(p.getAnioExReqPuesto()== null ? null : p.getAnioExReqPuesto());
        perfilExperiencia.setMesExReqPuesto(p.getMesExReqPuesto()== null ? null : p.getMesExReqPuesto());
        perfilExperiencia.setAnioExpSecPub(p.getAnioExpSecPub()== null ? null : p.getAnioExpSecPub());
        perfilExperiencia.setMesExpSecPub(p.getMesExpSecPub()== null ? null : p.getMesExpSecPub());
        perfilExperiencia.setNivelMinPueId(p.getNivelMinPueId()== null ? null : p.getNivelMinPueId());
        perfilExperiencia.setAspectos(p.getAspectos()== null ? null : p.getAspectos().toUpperCase().trim());
        return perfilExperiencia;
    };

     Function<PerfilExperienciaDetalleDTO, PerfilExperienciaDetalle> convertPerfilExperienciaDetalle = (PerfilExperienciaDetalleDTO p) -> {
        PerfilExperienciaDetalle perfilExperienciaDetalle = new PerfilExperienciaDetalle();
        perfilExperienciaDetalle.setId(p.getId() == null ? null : p.getId());
        perfilExperienciaDetalle.setRequisitoId(p.getRequisitoId() == null ? null : p.getRequisitoId());
        perfilExperienciaDetalle.setDescripcion(p.getDescripcion() == null ? null : p.getDescripcion().toUpperCase().trim());
        perfilExperienciaDetalle.setTiDaExId(p.getTiDaExId() == null ? null :  p.getTiDaExId());
        return perfilExperienciaDetalle;
    };

    Function<PerfilExperiencia , PerfilExperienciaDTO> convertPerfilExperiencia2 = (PerfilExperiencia p) -> {
        PerfilExperienciaDTO perfilExperiencia = new PerfilExperienciaDTO();
        perfilExperiencia.setId(p.getId());
        perfilExperiencia.setPerfilId(p.getPerfilId());
        perfilExperiencia.setAnioExpTotal(p.getAnioExpTotal() == null ? null : p.getAnioExpTotal());
        perfilExperiencia.setMesExpTotal(p.getMesExpTotal()== null ? null : p.getMesExpTotal());
        perfilExperiencia.setAnioExReqPuesto(p.getAnioExReqPuesto()== null ? null : p.getAnioExReqPuesto());
        perfilExperiencia.setMesExReqPuesto(p.getMesExReqPuesto()== null ? null : p.getMesExReqPuesto());
        perfilExperiencia.setAnioExpSecPub(p.getAnioExpSecPub()== null ? null : p.getAnioExpSecPub());
        perfilExperiencia.setMesExpSecPub(p.getMesExpSecPub()== null ? null : p.getMesExpSecPub());
        perfilExperiencia.setNivelMinPueId(p.getNivelMinPueId()== null ? null : p.getNivelMinPueId());
        perfilExperiencia.setAspectos(p.getAspectos()== null ? null : p.getAspectos().toUpperCase().trim());
        return perfilExperiencia;
    };

    Function<PerfilExperienciaDetalle , PerfilExperienciaDetalleDTO> convertPerfilExperienciaDetalle2 = (PerfilExperienciaDetalle p) -> {
        PerfilExperienciaDetalleDTO perfilExperienciaDetalle = new PerfilExperienciaDetalleDTO();
        perfilExperienciaDetalle.setId(p.getId());
        perfilExperienciaDetalle.setTiDaExId(p.getTiDaExId() == null ? null : p.getTiDaExId());
        perfilExperienciaDetalle.setRequisitoId(p.getRequisitoId() == null ? null :p.getRequisitoId());
        perfilExperienciaDetalle.setDescripcion(p.getDescripcion() == null ? null : p.getDescripcion().toUpperCase().trim());
        return perfilExperienciaDetalle;
    };

}
