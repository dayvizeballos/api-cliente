package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_perfil_conocimiento", schema = "sch_convocatoria")
@NamedQuery(name="Conocimiento.findAll", query="SELECT t FROM Conocimiento t")
@Getter
@Setter
public class Conocimiento extends AuditEntity implements AuditableEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil_conocimiento")
	@SequenceGenerator(name = "seq_perfil_conocimiento", sequenceName = "seq_perfil_conocimiento", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "perfil_conocimiento_id")
	private Long perfilConocimientoId;

	/*
	@Column(name = "tipo_conocimiento_id")
	private Long tipoConocimientoId;

	 */

	/*
	@Column(name = "conocimiento_id")
	private Long conocimientoId;

	 */

	/*
	@Column(name = "descripcion_conocimiento")
	private String descripcionConocimiento;

	 */

	@Column(name = "horas")
	private Integer horas;

	@Column(name = "regimemlaboralid")
	private Long regimemlaboralid;

	@Column(name = "nivel_dominio_id")
	private Long nivelDominioId;

	@Transient
	@Column(name = "observaciones")
	private String observaciones;

	/*
	@Column(name = "origen")
	private String origen;

	 */


	@JoinColumn(name="mae_cono_id")
	@ManyToOne(optional = false)
	private MaeConocimiento maeConocimiento;

	@JoinColumn(name = "perfil_id")
	@ManyToOne(optional = false)
	@JsonBackReference
	private Perfil perfil;

}
