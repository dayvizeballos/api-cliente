package pe.gob.servir.convocatoria.util;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import pe.gob.servir.convocatoria.loader.ConstantesExcel;

public class Util {

    public static boolean isEmpty(Object obj) {
        return (obj == null) || (obj.toString().trim().length() == 0);
    }

    public static String isEmptyToString(Object obj) {
        if ((obj == null) || (obj.toString().trim().length() == 0)) {
            return null;
        } else {
            return obj.toString().toUpperCase().trim();
        }
    }


    public static boolean isNumeric(String cadena){
    	try {
    		Integer.parseInt(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}
    }
    public static Object nullToEmptyString(Object obj) {
    	return Objects.isNull(obj) ? StringUtils.EMPTY : obj;
    }
    
    public static String fechaImpresion() {
    	return "Fecha de Impresión: "+ new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    } 
    
    public static String Capitalizador(String str) {
    	StringBuffer strbf = new StringBuffer();
    	Matcher match = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(str);
        while(match.find()) 
        {
           match.appendReplacement(strbf, match.group(1).toUpperCase() + match.group(2).toLowerCase());
        }
        return match.appendTail(strbf).toString();
    }
    
    public static String fechaPublicacion(Date date) {
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
    	cal.setTime(date);
    	int year = cal.get(Calendar.YEAR);
    	int month = cal.get(Calendar.MONTH)+1;
    	int day = cal.get(Calendar.DAY_OF_MONTH);
    	
    	LocalDate localDate=LocalDate.of(year,month,day);
    	 Locale spanishLocale=new Locale("es","PE");
    	 
    	  return localDate.format(DateTimeFormatter.ofPattern(", dd - MMMM - yyyy",spanishLocale)).replaceAll("-", "de")+".";
    	 
    }

    public static String instantToString (Instant instant) {
        Date myDate = Date.from(instant);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = formatter.format(myDate);
        return formattedDate;
    }
    
    public static String completarCaracteresIzq(int numero, String caracter, String len) {
        Formatter obj = new Formatter();
        String valor = "";
        try {
            valor = String.valueOf(obj.format("%" + caracter + len + "d", numero));
            return valor;
        } catch (Exception e) {
            return valor;
        } finally {
            obj.close();
        }
    }
    
    public static String abrirDocFromPCBasico(String rutaDoc) {
		String retVal = "OK";

		try {
			File f = new File(rutaDoc);
			if (f.exists()) {
				if (Desktop.isDesktopSupported()) {
					try {
						Desktop.getDesktop().open(f);
						retVal = "OK";
					} catch (IOException e) {
						// Util.Logger(strNombreClase, "abrirDocFromPCBasico", "IOException:" + e.toString());
						retVal = "Archivo No Soportado";
					} catch (Exception e) {
						// Util.Logger(strNombreClase, "abrirDocFromPCBasico", "Exception:" + e.toString());
					}
				} else {
					retVal = "Opcion no Soportado";
					// Util.LoggerInfo(strNombreClase, "abrirDocFromPCBasico", retVal);
				}
			} else {
				retVal = "El archivo " + rutaDoc + " No Existe";
				//Util.LoggerInfo(strNombreClase, "abrirDocFromPCBasico", retVal);
			}
		} catch (Exception e) {
			//Util.Logger(strNombreClase, "abrirDocFromPCBasico", "Exception2:" + e.toString());
			retVal = "ERROR";
		}
		return retVal;
	}
    
    public static String cantidadConLetra(String s) {
        StringBuilder result = new StringBuilder();
        BigDecimal totalBigDecimal = new BigDecimal(s).setScale(2, BigDecimal.ROUND_DOWN);
        long parteEntera = totalBigDecimal.toBigInteger().longValue();
        int triUnidades      = (int)((parteEntera % 1000));
        int triMiles         = (int)((parteEntera / 1000) % 1000);
        int triMillones      = (int)((parteEntera / 1000000) % 1000);
        int triMilMillones   = (int)((parteEntera / 1000000000) % 1000);
 
        if (parteEntera == 0) {
            result.append("Cero ");
            return result.toString();
        }
 
        if (triMilMillones > 0) result.append(triTexto(triMilMillones).toString() + "Mil ");
        if (triMillones > 0)    result.append(triTexto(triMillones).toString());
 
        if (triMilMillones == 0 && triMillones == 1) result.append("Millón ");
        else if (triMilMillones > 0 || triMillones > 0) result.append("Millones ");
 
        if (triMiles > 0)       result.append(triTexto(triMiles).toString() + "Mil ");
        if (triUnidades > 0)    result.append(triTexto(triUnidades).toString());
 
        return result.toString();
    }
    
    private static StringBuilder triTexto(int n) {
        StringBuilder result = new StringBuilder();
        int centenas = n / 100;
        int decenas  = (n % 100) / 10;
        int unidades = (n % 10);
 
        switch (centenas) {
            case 0: break;
            case 1:
                if (decenas == 0 && unidades == 0) {
                    result.append("Cien ");
                    return result;
                }
                else result.append("Ciento ");
                break;
            case 2: result.append("Doscientos "); break;
            case 3: result.append("Trescientos "); break;
            case 4: result.append("Cuatrocientos "); break;
            case 5: result.append("Quinientos "); break;
            case 6: result.append("Seiscientos "); break;
            case 7: result.append("Setecientos "); break;
            case 8: result.append("Ochocientos "); break;
            case 9: result.append("Novecientos "); break;
        }
 
        switch (decenas) {
            case 0: break;
            case 1:
                if (unidades == 0) { result.append("Diez "); return result; }
                else if (unidades == 1) { result.append("Once "); return result; }
                else if (unidades == 2) { result.append("Doce "); return result; }
                else if (unidades == 3) { result.append("Trece "); return result; }
                else if (unidades == 4) { result.append("Catorce "); return result; }
                else if (unidades == 5) { result.append("Quince "); return result; }
                else result.append("Dieci");
                break;
            case 2:
                if (unidades == 0) { result.append("Veinte "); return result; }
                else result.append("Veinti");
                break;
            case 3: result.append("Treinta "); break;
            case 4: result.append("Cuarenta "); break;
            case 5: result.append("Cincuenta "); break;
            case 6: result.append("Sesenta "); break;
            case 7: result.append("Setenta "); break;
            case 8: result.append("Ochenta "); break;
            case 9: result.append("Noventa "); break;
        }
 
        if (decenas > 2 && unidades > 0)
            result.append("y ");
 
        switch (unidades) {
            case 0: break;
            case 1: result.append("Un "); break;
            case 2: result.append("Dos "); break;
            case 3: result.append("Tres "); break;
            case 4: result.append("Cuatro "); break;
            case 5: result.append("Cinco "); break;
            case 6: result.append("Seis "); break;
            case 7: result.append("Siete "); break;
            case 8: result.append("Ocho "); break;
            case 9: result.append("Nueve "); break;
        }
 
        return result;
    }
    
    public static String formatoFechaString(Date fecha,String formato){
    	String fechaConFormato="";
    	try{
           	 SimpleDateFormat sdf = new SimpleDateFormat(formato);  
           	 fechaConFormato = sdf.format(fecha);
    	}catch(Exception ex){
    		return null;
    	}
    	 return fechaConFormato;
    }
    
    public static boolean isString(Object str) {
        if (str.equals(str.toString())) {
            return true;
        } else {
            return false;
        }
 
    }

	public static boolean isDouble(String cadena) {
		boolean resultado;
		try {
			Double.parseDouble(cadena);
			resultado = true;
		} catch (NumberFormatException excepcion) {
			resultado = false;
		}
		return resultado;
	}
	
	public static boolean isAnioMes(String cadena,String tipo) {
		boolean resultado;
		int cantidad = 0;
		try {
			double d = Double.parseDouble(cadena);
			cantidad = (int) d;
			resultado = true;
			if (tipo.equals(ConstantesExcel.CAMPO_MES)) {
				if (!(cantidad <= ConstantesExcel.MAX_MES && cantidad >= ConstantesExcel.MINIMO_AÑO_MES)) {
					resultado = false;
				}
			}
			if (tipo.equals(ConstantesExcel.CAMPO_ANIO)) {
				if (!(cantidad <= ConstantesExcel.MAX_AÑO && cantidad >= ConstantesExcel.MINIMO_AÑO_MES)) {
					resultado = false;
				}
			}
			
		} catch (NumberFormatException excepcion) {
			resultado = false;
		}
		return resultado;
	}
	
	public static List<String> obtenerString(String cadena){
		List<String> items = Arrays.asList(cadena.split(","));
		return items;
	}
	
	public static boolean validarCaracteres(String expReg, String valor) {
		Pattern patron = Pattern.compile(expReg);
		Matcher encajador = patron.matcher(valor);
		return encajador.matches();
	}
	
    public static <T> List<T> convertArrayToList(T[] array) {

        List<T> outputList = new ArrayList<T>();

        outputList = Arrays.asList(array);
        return outputList;
    }
}
