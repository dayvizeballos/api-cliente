package pe.gob.servir.convocatoria.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.PerfilExperienciaService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@Tag(name = "PerfilExperiencia", description = "")
public class PerfilExperienciaController {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private PerfilExperienciaService perfilExperienciaService;


    @Operation(summary = "Crea una perfilExperiencias", description = "Crea una perfilExperiencia", tags = { "" },
            security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
    @PostMapping(path = { Constantes.BASE_ENDPOINT+"/perfilExperiencias" },
            consumes = {MediaType.APPLICATION_JSON_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE })

    public ResponseEntity<RespBase<PerfilExperienciaDTO>> perfilExperiencias (@PathVariable String access, @Valid
    @RequestBody ReqBase<PerfilExperienciaDTO> request){
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");

        RespBase<PerfilExperienciaDTO> response = perfilExperienciaService.crearPerfilExperiencia(request,jwt);
        return ResponseEntity.ok(response);
    }


    @Operation(summary = "Actualizar perfilExperiencias", description = "Actualizar una perfilExperiencia", tags = { "" },
            security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
    @PutMapping(path = { Constantes.BASE_ENDPOINT+"/perfilExperiencias" },
            consumes = {MediaType.APPLICATION_JSON_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE })

    public ResponseEntity<RespBase<PerfilExperienciaDTO>> actualizarExperiencias (@PathVariable String access, @Valid
    @RequestBody ReqBase<PerfilExperienciaDTO> request){
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");

        RespBase<PerfilExperienciaDTO> response = perfilExperienciaService.actualizarPerfilExperiencia(request,jwt);
        return ResponseEntity.ok(response);

    }

    @Operation(summary = "Obtiene Perfil Experiencia", description = "Obtiene Perfil Experiencia", tags = { "" },
            security = { @SecurityRequirement(name = "bearer-key") }
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "operación exitosa"),
            @ApiResponse(responseCode = "500", description = "error interno", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) })
    })
    @GetMapping(path = { Constantes.BASE_ENDPOINT+"/perfilExperiencias/{perfilId}" },
            produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<RespBase<PerfilExperienciaDTO>> obtienePerfilExperiencia(
            @PathVariable String access,
            @PathVariable Long perfilId) {
        RespBase<PerfilExperienciaDTO> response = perfilExperienciaService.findPerfilExpereincia(perfilId);
        return ResponseEntity.ok(response);
    }

}
