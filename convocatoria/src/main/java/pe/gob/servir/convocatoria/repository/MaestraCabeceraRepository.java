package pe.gob.servir.convocatoria.repository;

import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import pe.gob.servir.convocatoria.model.MaestraCebecera;


import java.util.List;

public interface MaestraCabeceraRepository extends JpaRepository<MaestraCebecera, Long> {

    @Query("SELECT maeDet FROM MaestraCebecera maeDet WHERE maeDet.codigoCabecera = :codigoCabecera ORDER BY maeDet.maeCabeceraId DESC")
    public List<MaestraCebecera> findAllcodigoCabecera(@Param("codigoCabecera")String codigoCabecera);

}
