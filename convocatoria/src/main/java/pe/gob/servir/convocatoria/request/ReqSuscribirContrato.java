package pe.gob.servir.convocatoria.request;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;
import pe.gob.servir.convocatoria.common.Constantes;

@Data
public class ReqSuscribirContrato {

	@NotNull(message = Constantes.CAMPO + " estadoId " + Constantes.ES_OBLIGATORIO)
	private Long estadoId;
	@NotNull(message = Constantes.CAMPO + " pdfBase64 " + Constantes.ES_OBLIGATORIO)
	private String pdfBase64;
	@NotNull(message = Constantes.CAMPO + " fechaSuscripcion " + Constantes.ES_OBLIGATORIO)
	private Date fechaSuscripcion;
}
