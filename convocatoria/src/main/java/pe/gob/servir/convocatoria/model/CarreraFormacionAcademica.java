package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_perfil_carrera_form_academ", schema = "sch_convocatoria")
@Getter
@Setter
public class CarreraFormacionAcademica extends AuditEntity implements AuditableEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil_carrera_form_academ")
	@SequenceGenerator(name = "seq_perfil_carrera_form_academ", sequenceName = "seq_perfil_carrera_form_academ", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "perfil_carrera_form_academ_id")
	private Long carreraAcademicaId;
	

	@JoinColumn(name="perfil_formacion_academica_id")
	@ManyToOne(optional = false)
	@JsonBackReference
	private FormacionAcademica fomarcionAcademica;

	/*
	@Column(name = "carrera_id")
	private Long carreraId;
	*/

	@Transient
	private String descripcion;
	
	@Transient
	private Long maeDetalleId;


	@JoinColumn(name="carrera_id")
	@ManyToOne(optional = false)
	private CarreraProfesional carreraProfesional;

	public CarreraFormacionAcademica() {

	}
}
