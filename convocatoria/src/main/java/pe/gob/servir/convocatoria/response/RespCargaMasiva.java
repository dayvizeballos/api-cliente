package pe.gob.servir.convocatoria.response;

import lombok.Data;

import java.util.List;

@Data
public class RespCargaMasiva {
    private String regimen;
    private Long idRegimen;
    private Integer correctos;
    private Integer incorrectos;
    private Integer total;
    private List<String> errors;
}
