package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Data;
import pe.gob.servir.convocatoria.request.dto.MaestraDetalleDto;

@Data
public class RespListaMaestraEntidad {

	private List<MaestraDetalleDto> lstMaestraEntidad;
}
