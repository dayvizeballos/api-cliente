package pe.gob.servir.convocatoria.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BasePerfil;

@Repository
public interface BasePerfilRepository extends JpaRepository<BasePerfil, Long >{

	@Query("SELECT  b.basePerfilId FROM  BasePerfil b WHERE b.basePerfilId = :basePerfilId ")
    public Optional<BasePerfil> findByIdBasePerfil(@Param("basePerfilId")Long basePerfilId);
	
	@Query("SELECT bp  FROM  BasePerfil bp WHERE bp.base = :base ")
	public List<BasePerfil> findbyBaseId(@Param("baseId")Base base);

	@Query("SELECT bp  FROM  BasePerfil bp WHERE bp.base.baseId = :base and bp.perfilId= :perfilId and bp.estadoRegistro = '1'")
	 List<BasePerfil> findbyBaseIdAndPerfil(@Param("base")Long base , @Param("perfilId")Long perfilId);	
}
