package pe.gob.servir.convocatoria.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Entity
@Table(name = "tbl_conf_perf_expe_laboral", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
@NamedQuery(name="ConfigPerfilExperienciaLaboral.findAll", query="SELECT t FROM ConfigPerfilExperienciaLaboral t")
public class ConfigPerfilExperienciaLaboral  extends AuditEntity implements Serializable {

	    private static final long serialVersionUID = 1L;
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_expe_laboral")
	    @SequenceGenerator(name = "seq_conf_perf_expe_laboral", sequenceName = "seq_conf_perf_expe_laboral", schema = "sch_convocatoria", allocationSize = 1)
	    
	    @Column(name = "conf_perf_expe_laboral_id")
	    private Long confPerfExpeLaboralId;
	    
		@JoinColumn(name = "perfil_id")
		@ManyToOne(optional = false)
		@JsonBackReference
		private Perfil perfil;

		@Column(name = "perfil_id" , insertable = false , updatable = false)
		private Long perfilId;
	    
	    @Column(name = "tipo_experiencia")
	    private Long tipoExperiencia;

	    @Column(name = "nivel_academico")
	    private String nivelAcademico;

	    @Column(name = "desde_anios")
	    private Long desdeAnios;
	    
	    @Column(name = "hasta_anios")
	    private Long hastaAnios;
	   
	    @Column(name = "requisito_minimo")
	    private Boolean  requisitoMinimo;
	    
	    @Column(name = "puntaje")
	    private Long  puntaje;	  

	    @Column(name = "nivel_educativo_id")
	    private Long  nivelEducativoId;
	    
	    @Column(name = "situacion_academica_id")
	    private Long  situacionAcademicaId;

	    @Column(name = "nivel_educativo_desc")
	    private String nivelEducativoDesc;
	    
	    @Column(name = "sit_academica_desc")
	    private String sitAcademiDesc;
	    
	    @Column(name = "base_id")
	    private Long baseId;
	    
	    
	    

	    public ConfigPerfilExperienciaLaboral() {
	    }
	}