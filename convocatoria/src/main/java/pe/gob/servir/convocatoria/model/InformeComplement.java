package pe.gob.servir.convocatoria.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.audit.AuditableEntity;


/**
 * The persistent class for the tbl_base_inf_complement database table.
 * 
 */
@Entity
@Table(name="tbl_base_inf_complement",schema = "sch_base")
@NamedQuery(name="InformeComplement.findAll", query="SELECT t FROM InformeComplement t")
@Getter
@Setter
@ToString
public class InformeComplement extends AuditEntity implements AuditableEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tbl_base_inf_complement")
	@SequenceGenerator(name = "seq_tbl_base_inf_complement", sequenceName = "seq_tbl_base_inf_complement", schema = "sch_base", allocationSize = 1)
	@Column(name="base_comple_id")
	private Long baseCompleId;

	@Column(name="base_id")
	private Long baseId;

	@Column(name="bonificacion_id")
	private Long bonificacionId;

    @Column(name = "tipo_informe_id")
    private Long tipoInfoId;

	//bi-directional many-to-one association to ComplDetalle
	@OneToMany(mappedBy="informeComplement")
	private List<ComplDetalle> complDetalles;

	public InformeComplement() {
		super();
	}

}