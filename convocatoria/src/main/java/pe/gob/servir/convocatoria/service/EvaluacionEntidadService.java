package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.request.ReqActualizaEvaluacionEntidad;
import pe.gob.servir.convocatoria.request.ReqAsignaJerarquia;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqEliminarJerarquiaEntidad;
import pe.gob.servir.convocatoria.response.RespActualizaEvaluacionEntidad;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface EvaluacionEntidadService {
	
	RespBase<RespActualizaEvaluacionEntidad> actualizarEvaluacionEntidad(ReqBase<ReqActualizaEvaluacionEntidad> request,
			MyJsonWebToken token, Long jerarquiaId);

	
	RespBase<Object> asignarEvaluacionEntidad(ReqBase<ReqAsignaJerarquia> request, MyJsonWebToken token);
	
	RespBase<Object> eliminarJerarquiaEntidad(ReqBase<ReqEliminarJerarquiaEntidad> request, MyJsonWebToken token);
}
