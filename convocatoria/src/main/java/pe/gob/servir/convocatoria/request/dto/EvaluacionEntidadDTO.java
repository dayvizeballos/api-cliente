package pe.gob.servir.convocatoria.request.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@Getter
@Setter
public class EvaluacionEntidadDTO {

	
	private Long evaluacionEntidadId;
	
	@NotNull(message = Constantes.CAMPO + " entidadId " + Constantes.ES_OBLIGATORIO)
	private Long entidadId;
	
	@NotNull(message = Constantes.CAMPO + " jerarquiaId " + Constantes.ES_OBLIGATORIO)
	private Long jerarquiaId;
	
	@NotNull(message = Constantes.CAMPO + " tipoEvaluacionId " + Constantes.ES_OBLIGATORIO)
	private Long tipoEvaluacionId;
	
	@NotNull(message = Constantes.CAMPO + " orden " + Constantes.ES_OBLIGATORIO)
	private Integer orden;
	
	@NotNull(message = Constantes.CAMPO + " peso " + Constantes.ES_OBLIGATORIO)
	private Integer peso;
	
	@NotNull(message = Constantes.CAMPO + " puntajeMinimo " + Constantes.ES_OBLIGATORIO)
	private Integer puntajeMinimo;
	
	@NotNull(message = Constantes.CAMPO + " puntajeMaximo " + Constantes.ES_OBLIGATORIO)
	private Integer puntajeMaximo;
	
	private String estado;
	
	@NotNull(message = Constantes.CAMPO + " ind_solo_servir " + Constantes.ES_OBLIGATORIO)
	@Pattern(regexp = "1|0", message = Constantes.CAMPO + " ind_solo_servir " + Constantes.ES_INVALIDO + ", valores permitidos 1(SERVIR), 0(ENTIDAD)")
	private String ind_solo_servir;
}
