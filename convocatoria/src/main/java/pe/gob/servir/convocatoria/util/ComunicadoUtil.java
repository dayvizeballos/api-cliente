package pe.gob.servir.convocatoria.util;

import pe.gob.servir.convocatoria.common.Constantes;

public class ComunicadoUtil {

   public static String obtenerURL(Long entidadId, String path, String nombre) {
        String rutaFileServer;
        rutaFileServer = ParametrosUtil.datePathReplaceRepositoryAlfresco(path);
        rutaFileServer = rutaFileServer.replace(Constantes.PATH_ENTIDAD, entidadId.toString());
        path = rutaFileServer + "/" + nombre + Constantes.EXTENSION_PDF;
        return path;
    }

   public static String obtenerNombreArchivo(String path) {
        String fileName;
        int lastSeparatorIndex = path.lastIndexOf("/");
        if (lastSeparatorIndex == -1) {
            fileName = path;
        } else {
            fileName = path.substring(lastSeparatorIndex + 1);
        }
        fileName = fileName.substring(0, fileName.lastIndexOf('.'));
        return fileName;

    }
}
