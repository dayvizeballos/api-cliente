package pe.gob.servir.convocatoria.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import pe.gob.servir.convocatoria.audit.AuditEndpointInterceptor;
import pe.gob.servir.convocatoria.interceptor.AuthenticationInterceptor;

@Configuration
public class InterceptorConfiguration implements WebMvcConfigurer {

	@Autowired
	AuthenticationInterceptor authenticationInterceptor;
	@Autowired
	AuditEndpointInterceptor auditEndpointInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(authenticationInterceptor);
		//registry.addInterceptor(auditEndpointInterceptor); /*INICIO: SE COMENTA POR AHORA NO ESTA DEFINIDO AUDITORIA ENDPOINT PARA API CONVOCATORIA*/
	}
}
