package pe.gob.servir.convocatoria.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Entity
@Table(name = "tbl_conf_perf_formac_carrera_detalle", schema = "sch_convocatoria")
@Setter
@Getter
@ToString
@NamedQuery(name="ConfigPerfilFormacionCarreraDetalle.findAll", query="SELECT t FROM ConfigPerfilFormacionCarreraDetalle t")
public class ConfigPerfilFormacionCarreraDetalle  extends AuditEntity implements Serializable {

	    private static final long serialVersionUID = 1L;
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_formac_carrera_detalle")
	    @SequenceGenerator(name = "seq_conf_perf_formac_carrera_detalle", sequenceName = "seq_conf_perf_formac_carrera_detalle", schema = "sch_convocatoria", allocationSize = 1)
	    
	    @Column(name = "conf_perf_formac_carrera_detalle_id")
	    private Long confPerfFormacCarreraDetalleId;
	 
	    @Column(name = "perfil_Id")
	    private Long perfilId;
	    
	    @Column(name = "grado")
	    private Long grado;
	   
	    @Column(name = "puntaje")
	    private Long  puntaje;
	    
	    @Column(name = "base_id")
	    private Long  baseId;
	    
	    public ConfigPerfilFormacionCarreraDetalle() {
	    }
	}