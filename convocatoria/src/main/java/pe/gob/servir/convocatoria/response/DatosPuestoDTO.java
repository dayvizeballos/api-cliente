package pe.gob.servir.convocatoria.response;


import java.util.Objects;

import lombok.Data;
import pe.gob.servir.convocatoria.model.BasePerfil;


@Data
public class DatosPuestoDTO {
    	
	private Long basePerfilId;	
	private Long perfilId;	
	private String jornadaLaboral;	
	private Integer vacante;	
	private Double remuneracion;	
	private String indContratoIndeterminado;	
	private Integer tiempoContrato;	
	private Long condicionTrabajoId;	
	private Long sedeId;	
	private String condicionEsencial;	
	private String observaciones;	
	private String sedeDireccion;	
	private Long modalidadContratoId;	
	private BasePuestoDTO base;	  
    private Integer depaId;    
    private String descDepa;    
    private Integer provId;    
    private String descProv;    
    private Integer distId;    
    private String descDist;
    
    public DatosPuestoDTO(BasePerfil basePerfil){
    	this.basePerfilId = basePerfil.getBasePerfilId();
    	this.perfilId = basePerfil.getPerfilId();
    	this.jornadaLaboral = basePerfil.getJornadaLaboral();
    	this.vacante = basePerfil.getVacante();
    	this.remuneracion = basePerfil.getRemuneracion();
    	this.indContratoIndeterminado = basePerfil.getIndContratoIndeterminado();
    	this.tiempoContrato = basePerfil.getTiempoContrato();
    	this.condicionEsencial = basePerfil.getCondicionEsencial();
    	this.observaciones = basePerfil.getObservaciones();
    	this.sedeDireccion = basePerfil.getSedeDireccion();
    	this.modalidadContratoId = basePerfil.getModalidadContratoId();
    	this.depaId = basePerfil.getDepaId();
    	this.descDepa = basePerfil.getDescDepa();
    	this.provId = basePerfil.getProvId();
    	this.descProv = basePerfil.getDescProv();
    	this.distId = basePerfil.getDistId();
    	this.descDist = basePerfil.getDescDist();
    	this.base = Objects.nonNull(basePerfil.getBase()) ? new BasePuestoDTO(basePerfil.getBase()) : null;
    }    
    
}
