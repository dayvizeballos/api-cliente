package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApiParametro {
	
	private Integer parametroId;
	private String tipoParametro;
	private Integer codigoNumero;
	private String codigoTexto;
	private String valorTexto;
	private Integer orden;

}
