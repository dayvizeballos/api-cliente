package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class PerfilExperienciaDetalleDTO {

    private  Long id;
    private Long  tiDaExId;
    private Long  requisitoId;
    private String descripcion;
    private String estado;
    private String orden;
    private Long perfilExperienciaId;


}
