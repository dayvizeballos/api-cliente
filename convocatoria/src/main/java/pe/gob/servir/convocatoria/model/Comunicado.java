package pe.gob.servir.convocatoria.model;


import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import java.util.Date;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "tbl_comunicado", schema = "sch_convocatoria")
@Getter
@Setter
public class Comunicado extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_comunicado")
    @SequenceGenerator(name = "seq_comunicado", sequenceName = "seq_comunicado", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "comunicado_id")
    private Long comunicadoId;

    @JoinColumn(name = "perfil_id", nullable = true)
    @ManyToOne(optional = true)
    private Perfil perfil;

    @JoinColumn(name = "etapa_id")
    @ManyToOne(optional = false)
    private MaestraDetalle etapa;

    @JoinColumn(name = "estado_id")
    @ManyToOne(optional = false)
    private MaestraDetalle estado;

    @JoinColumn(name = "tipo_comunicado_id")
    @ManyToOne(optional = false)
    private MaestraDetalle tipoComunicado;

    @JoinColumn(name = "convocatoria_id")
    @ManyToOne(optional = false)
    private Convocatoria convocatoria;

    @Column(name = "gestor_id")
    private Long gestorId;

    @Column(name = "coordinador_id")
    private Long coordinadorId;

    @Column(name = "nombre_gestor")
    private String nombreGestor;

    @Column(name = "nombre_coordinador")
    private String nombreCoordinador;

    @Column(name = "url")
    private String url;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "con_pdf")
    private String conPdf;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_publicacion")
    private Date fechaPublicacion;
    
    @Column(name = "observacion")
    private String observacion;

}
