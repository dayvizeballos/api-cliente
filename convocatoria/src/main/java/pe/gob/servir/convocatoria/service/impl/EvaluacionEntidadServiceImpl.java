package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.Evaluacion;
import pe.gob.servir.convocatoria.model.EvaluacionEntidad;
import pe.gob.servir.convocatoria.model.Jerarquia;
import pe.gob.servir.convocatoria.repository.EvaluacionEntidadRepository;
import pe.gob.servir.convocatoria.repository.JerarquiaRepository;
import pe.gob.servir.convocatoria.request.ReqActualizaEvaluacionEntidad;
import pe.gob.servir.convocatoria.request.ReqAsignaJerarquia;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqEliminarJerarquiaEntidad;
import pe.gob.servir.convocatoria.request.dto.JerarquiaEntidadDTO;
import pe.gob.servir.convocatoria.response.RespActualizaEvaluacionEntidad;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.EvaluacionEntidadService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;

@Service
public class EvaluacionEntidadServiceImpl implements EvaluacionEntidadService {

	/**@Autowired
	private ConfiguracionMaestraRepository configMaestraRepository;*/
	
	@Autowired
	private JerarquiaRepository jerarquiaRepository;
	
	/**@Autowired
	private EvaluacionRepository evaluacionRepository;*/
	
	@Autowired
	private EvaluacionEntidadRepository evaluacionEntidadRepository;
	
	
	@Override
	public RespBase<RespActualizaEvaluacionEntidad> actualizarEvaluacionEntidad(
			ReqBase<ReqActualizaEvaluacionEntidad> request, MyJsonWebToken token, Long jerarquiaId) {
		return null;
	}

	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<Object> asignarEvaluacionEntidad(ReqBase<ReqAsignaJerarquia> request, MyJsonWebToken token) {
		
		RespBase<Object> response = new RespBase<>();
		EvaluacionEntidad evaluacionEntidad = null;
 		if(request.getPayload().getEntidadId() != null) {
			if(request.getPayload().getLstJerarquia() != null && !request.getPayload().getLstJerarquia().isEmpty()) {
				
				for(JerarquiaEntidadDTO jerarquia : request.getPayload().getLstJerarquia()) {
										
					List<Evaluacion> lstEvaluacion = evaluacionEntidadRepository.findEvaluacion(jerarquia.getCodigoNivel1(),
							jerarquia.getCodigoNivel2(), jerarquia.getCodigoNivel3());
					
					if(lstEvaluacion!= null && !lstEvaluacion.isEmpty()) {
						
						for(Evaluacion evaluacion : lstEvaluacion) {
							
							Optional<EvaluacionEntidad> buscaEvaluacion = evaluacionEntidadRepository
									.findEvaluacionByIdOrigen(request.getPayload().getEntidadId() , evaluacion.getEvaluacionId());
							
							if (!buscaEvaluacion.isPresent()) {
								evaluacionEntidad = new EvaluacionEntidad();
								evaluacionEntidad.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
								evaluacionEntidad.setEntidadId(request.getPayload().getEntidadId());
								evaluacionEntidad.setJerarquiaId(evaluacion.getJerarquiaId());
								evaluacionEntidad.setTipoEvaluacionId(evaluacion.getTipoEvaluacionId());
								evaluacionEntidad.setOrden(evaluacion.getOrden());
								evaluacionEntidad.setPeso(evaluacion.getPeso());
								evaluacionEntidad.setPuntajeMinimo(evaluacion.getPuntajeMinimo());
								evaluacionEntidad.setPuntajeMaximo(evaluacion.getPuntajeMaximo());
								evaluacionEntidad.setEvaluacionOrigenId(evaluacion.getEvaluacionId());
								evaluacionEntidad.setIndValidaServir("1");
								evaluacionEntidadRepository.save(evaluacionEntidad);
								
							}else {
								response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "Existe una jerarquia asociada a la entidad");
								return response;
							}
						}
						
					}
					
				}
				
				response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se asignó correctamente las evaluaciones. ");
				
			}else {
				response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No existe jerarquias asociadas");
			}
		}else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, Constantes.MENSAJE_ENTIDAD_VACIO);
		}
		
		return response;
	}

	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<Object> eliminarJerarquiaEntidad(ReqBase<ReqEliminarJerarquiaEntidad> request,
			MyJsonWebToken token) {
		
		RespBase<Object> response = new RespBase<>();
		
		Optional<Jerarquia> jerarquiaFind = jerarquiaRepository.findById(request.getPayload().getJerarquiaId());
		if(jerarquiaFind.isPresent()) {
			EvaluacionEntidad filterEvaluacion = new EvaluacionEntidad();
			filterEvaluacion.setJerarquiaId(request.getPayload().getEntidadId());
			filterEvaluacion.setJerarquiaId(request.getPayload().getJerarquiaId());
			filterEvaluacion.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
			
			Example<EvaluacionEntidad> exampleEvaluacion = Example.of(filterEvaluacion);
			List<EvaluacionEntidad> lstEvaluacion = evaluacionEntidadRepository.findAll(exampleEvaluacion);
			if (lstEvaluacion != null && Boolean.FALSE.equals(lstEvaluacion.isEmpty())) {
				for(EvaluacionEntidad evaluacion : lstEvaluacion) {
					evaluacion.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),Instant.now());
					evaluacionEntidadRepository.save(evaluacion);
				}
			}
			response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se eliminó la jerarquia correctamente ");
		}else{
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el jeraquiaId Ingresado");
		}
		
		return response;
	}

}
