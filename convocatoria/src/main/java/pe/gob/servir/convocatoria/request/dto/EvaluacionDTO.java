package pe.gob.servir.convocatoria.request.dto;

import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class EvaluacionDTO {

	private Long evaluacionId;
	
	private Long jerarquiaId;
	
	@NotNull(message = Constantes.CAMPO + " tipoEvaluacionId " + Constantes.ES_OBLIGATORIO)
	private Long tipoEvaluacionId;
	
	@NotNull(message = Constantes.CAMPO + " orden " + Constantes.ES_OBLIGATORIO)
	private Integer orden;
	
	@NotNull(message = Constantes.CAMPO + " peso " + Constantes.ES_OBLIGATORIO)
	private Integer peso;
	
	@NotNull(message = Constantes.CAMPO + " puntajeMinimo " + Constantes.ES_OBLIGATORIO)
	private Integer puntajeMinimo;
	
	@NotNull(message = Constantes.CAMPO + " puntajeMaximo " + Constantes.ES_OBLIGATORIO)
	private Integer puntajeMaximo;

	private String detalleEvaluacion;

	private String estado;

	/*
	 valores propios de evaluacion entidad
	 reusar DTO
	 */
	private Long entidadId;
	private Long evaluacionEntidad;
	private Long evaluacionOrigenId;
}
