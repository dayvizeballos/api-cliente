package pe.gob.servir.convocatoria.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.feign.client.MaestraApiClient;
import pe.gob.servir.convocatoria.loader.ConstantesExcel;
import pe.gob.servir.convocatoria.loader.Validacion30057;
import pe.gob.servir.convocatoria.model.CargaMasiva;
import pe.gob.servir.convocatoria.model.CarreraFormacionAcademica;
import pe.gob.servir.convocatoria.model.CarreraProfesional;
import pe.gob.servir.convocatoria.model.Conocimiento;
import pe.gob.servir.convocatoria.model.FormacionAcademica;
import pe.gob.servir.convocatoria.model.FuncionDetalle;
import pe.gob.servir.convocatoria.model.MaeConocimiento;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.Organigrama;
import pe.gob.servir.convocatoria.model.Perfil;
import pe.gob.servir.convocatoria.model.PerfilExperiencia;
import pe.gob.servir.convocatoria.model.PerfilExperienciaDetalle;
import pe.gob.servir.convocatoria.model.PerfilFuncion;
import pe.gob.servir.convocatoria.model.PerfilGrupo;
import pe.gob.servir.convocatoria.model.UnidadOrganica;
import pe.gob.servir.convocatoria.repository.CargaMasivaRepositoryRepository;
import pe.gob.servir.convocatoria.repository.CarreraProfesionalRepository;
import pe.gob.servir.convocatoria.repository.FormacionAcademicaRepository;
import pe.gob.servir.convocatoria.repository.FuncionDetalleRepository;
import pe.gob.servir.convocatoria.repository.MaeConocimientoRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.PerfilExperienciaRepository;
import pe.gob.servir.convocatoria.repository.PerfilFuncionRepository;
import pe.gob.servir.convocatoria.repository.PerfilGrupoRepository;
import pe.gob.servir.convocatoria.repository.PerfilRepository;
import pe.gob.servir.convocatoria.repository.PerfilRepositoryJdbc;
import pe.gob.servir.convocatoria.request.ReqCargaMasivaPerfil;
import pe.gob.servir.convocatoria.request.dto.ParametrosDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.CapacitacionService;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;
import pe.gob.servir.convocatoria.service.PerfilGrupoService;
import pe.gob.servir.convocatoria.util.Util;

@Slf4j
@Service
public class LoaderMasiva1401 {

    private HSSFWorkbook hssfLibroExcel;
    private XSSFWorkbook xssfLibroExcel;


    private HSSFFormulaEvaluator hssFormulaEvaluator;
    private XSSFFormulaEvaluator xssFormulaEvaluator;

    List<String> lstErrores;

    Map<String, Object> objectMapOrganos;

    Map<String, Object> objectMapUnidadOrganica;

    Map<String, Object> objectMapGrupoSerCivil;

    Map<String, Object> objectMapNivelCategoria;

    Map<String, Object> objectMapPuestoTipo;

    Map<String, Object> objectMapTipoPeriodicidad;

    Map<String, Object> objectMapGrupoSerCoordina;

    Map<String, Object> objectMapNivelEducativo;

    Map<String, Object> objectMapCarreras;

    Map<String, Object> objectMapSituAcademica;

    Map<String, Object> objectMapTipoRequisitoAd;

    Map<String, Object> objectMapEstadoNivelEducativo;

    Map<String, Object> objectMapGrados;
    
    Map<String, Object> objectMapTipoPractica;
    
    Map<String, Object>  objectMapTipoPracticaCondicion;

    @Autowired
    Validacion30057 validacion;

    @Autowired
    CargaMasivaRepositoryRepository cargaMasivaRepositoryRepository;

    @Autowired
    PerfilFuncionRepository perfilFuncionRepository;

    @Autowired
    PerfilExperienciaRepository perfilExperienciaRepository;

    @Autowired
    PerfilRepository perfilRepository;

    @Autowired
    EntidadClient entidadClient;

    @Autowired
    PerfilGrupoService perfilGrupoService;

    @Autowired
    PerfilGrupoRepository perfilGrupoRepository;

    @Autowired
    MaestraApiClient maestraClient;

    @Autowired
    MaestraDetalleService maestraDetalleService;

    @Autowired
    MaestraDetalleRepository maestraDetalleRepository;

    @Autowired
    FuncionDetalleRepository funcionDetalleRepository;

    @Autowired
    CapacitacionService capacitacionService;

    @Autowired
    CarreraProfesionalRepository carreraProfesionalRepository;

    @Autowired
    MaeConocimientoRepository maeConocimientoRepository;

    @Autowired
    FormacionAcademicaRepository formacionAcademicaRepository;
    
	@Autowired
	PerfilRepositoryJdbc perfilRepositoryJdbc;
    
    private void guardarBD(Perfil perfil, List<PerfilExperiencia> ls , PerfilFuncion perfilFuncion) {
        try {
            perfilRepository.save(perfil);

            for (PerfilExperiencia it : ls) {
                it.setPerfilId(perfil.getPerfilId());
            }

            perfilExperienciaRepository.saveAll(ls);

            perfilFuncion.setPerfilId(perfil.getPerfilId());

            perfilFuncionRepository.save(perfilFuncion);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        

    }
    private static final String[] ACTIVIDAD = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16" };
    
    /**
    *
    * @param hssfFilaExcel
    * @param xssfFilaExcel
    * @param fila
    * @param ok
    * @param objectMap
    * @param token
    * @return
    * @throws Exception
    */
   private Map<String, Object> settTabPerfilFuncion( HSSFRow hssfFilaExcel,
                                               XSSFRow xssfFilaExcel, int fila, boolean ok, Map<String, Object> objectMap , MyJsonWebToken token) throws Exception {

       PerfilFuncion pFuncion = new PerfilFuncion();
       pFuncion.setFuncionDetalles(new ArrayList<>());

       Map<String, String> valores;
       CargaMasiva cargaMasiva;
       Map<String, Object> perfilFuncionMap = new HashMap<>();
       //7 AL 22: FUNCIONES
       List<FuncionDetalle> lstFuncionDetalle = new ArrayList<>();
       List<String> lstErrorFunciones =  new ArrayList<>();
       int reg = 0;
       for (int i = 1; i < ACTIVIDAD.length + 1; i++) {
           String campo = ConstantesExcel.ACTIVIDAD + ACTIVIDAD[i - 1];
           cargaMasiva = (CargaMasiva) objectMap.get(campo);
           if (cargaMasiva != null) {
               FuncionDetalle detalle = new FuncionDetalle();

               String funcion = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva,
                       this.hssFormulaEvaluator, this.xssFormulaEvaluator);
               String rpta = validarErrorSubstring(funcion);

               if (rpta == null) {
                   this.lstErrores.add(funcion);
                   lstErrorFunciones.add(funcion);
                   ok = false;
               }else {
                   if (!Util.isEmpty(funcion)) {
                       reg++;
                       detalle.setDescripcion(funcion);
                       detalle.setOrden(reg);
                       lstFuncionDetalle.add(detalle);
                   }
               }

           }
       }
       
       pFuncion.setFuncionDetalles(new ArrayList<>());
       if (!lstFuncionDetalle.isEmpty()) {
           for (FuncionDetalle it : lstFuncionDetalle) {
               it.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
               it.setPerfilFuncion(pFuncion);
           }
       }
       
       pFuncion.setFuncionDetalles(lstFuncionDetalle);
       pFuncion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
       
       perfilFuncionMap.put("pFuncion", pFuncion);
       perfilFuncionMap.put("valida", ok);
       return perfilFuncionMap;
   }
    
    /**
    *
    * @param cargaMasiva
    * @param perfil
    * @param hssfFilaExcel
    * @param xssfFilaExcel
    * @param fila
    * @param ok
    * @param objectMap
    * @return
    * @throws Exception
    */
    
    
   private Map<String, Object> settTabPerfil(CargaMasiva cargaMasiva, Perfil perfil,
                                             HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila,
                                             boolean ok, Map<String, Object> objectMap) throws Exception {
  
       Map<String, Object> perfilMap = new HashMap<>();
       // 1: ORGANO
       cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ORGANO);

       Map<String, String> valores;
       if (cargaMasiva != null) {

           String organo = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
           String rpta = validarErrorSubstring(organo);

           if (rpta == null) {
               this.lstErrores.add(organo);
               ok = false;
           }

           if (ok)
               if (!Util.isEmpty(organo)) {
                   if (cargaMasiva.getCompuesto().equalsIgnoreCase("1")) {
                       valores = splitCeldaCompuestaOrgano(organo, objectMapOrganos);
                       perfil.setOrganoId(Long.valueOf(valores.get("id")));
                       perfil.setNombreOrgano(valores.get("nombre"));
                       perfil.setSiglaOrgano(valores.get("sigla"));
                   }
               }
       }


       // 2: UNIDAD_ORGANICA
       cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.UNIDAD_ORGANICA);

       if (cargaMasiva != null) {
           String unidadOrganica = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
           String rpta = validarErrorSubstring(unidadOrganica);

           if (rpta == null) {
               this.lstErrores.add(unidadOrganica);
               ok = false;

           }
           if (ok)
               if (!Util.isEmpty(unidadOrganica)) {
                   if (cargaMasiva.getCompuesto().equalsIgnoreCase("1")) {
                       valores = splitCeldaCompuestaUnidadOrganica(unidadOrganica, objectMapUnidadOrganica);
                       perfil.setUnidadOrganicaId(Long.valueOf(valores.get("id")));
                       perfil.setUnidadOrganica(valores.get("nombre"));
                   }
               }
       }


       //3: NOMBRE_PUESTO
       cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NOMBRE_PUESTO);
       if (cargaMasiva != null) {
           String nombrePuesto = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
           String rpta = validarErrorSubstring(nombrePuesto);

           if (rpta == null) {
               this.lstErrores.add(nombrePuesto);
               ok = false;

           }
           if (ok)
               if (!Util.isEmpty(nombrePuesto)) {
                   perfil.setNombrePuesto(nombrePuesto);
               }
       }
     

       //4: DEPENDENCIA_JERARQUICA_LINEAL
       cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.DEPENDENCIA_JERARQUICA_LINEAL);
       if (cargaMasiva != null) {
           String depJerLineal = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
           String rpta = validarErrorSubstring(depJerLineal);

           if (rpta == null) {
               this.lstErrores.add(depJerLineal);
               ok = false;

           }

           if (ok)
               if (!Util.isEmpty(depJerLineal)) {
                   perfil.setDependenciaJerarquica(depJerLineal);
               }
       }

     
       //5: UNIDAD_FUNCIONAL
       cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.UNIDAD_FUNCIONAL);
       if (cargaMasiva != null) {
           String unidadFuncional = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
           String rpta = validarErrorSubstring(unidadFuncional);

           if (rpta == null) {
               this.lstErrores.add(unidadFuncional);
               ok = false;
           }

           if (ok)
               if (!Util.isEmpty(unidadFuncional)) {
                   perfil.setUnidadFuncional(unidadFuncional);
               }

       }
       
       //6: TIPO_DE_PRACTICAS
       cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.TIPO_DE_PRACTICAS);
       if (cargaMasiva != null) {
           String tipoPractica = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
           String rpta = validarErrorSubstring(tipoPractica);

           if (rpta == null) {
               this.lstErrores.add(tipoPractica);
               ok = false;
           }

           if (ok)
        	   if (!Util.isEmpty(tipoPractica)) {
                   if (cargaMasiva.getCompuesto().equalsIgnoreCase("1")) {
                       valores = splitCeldaTipoPractica(tipoPractica, objectMapTipoPractica);
                       perfil.setTipoPracticaId(Long.valueOf(valores.get("id")).longValue());
                       valores = splitCeldaTipoPracticaCondicion(valores.get("codProgCondicion"), objectMapTipoPracticaCondicion);
                       perfil.setCondicionPracticaId(Long.valueOf(valores.get("id")).longValue());

                   }
               }

       }

       perfilMap.put("valida", ok);
       perfilMap.put("perfil", perfil);
       //return perfil;
       return perfilMap;
   }
	
	
    public HashMap<String, Object> cargarArchivoExcel1401(ReqCargaMasivaPerfil model, InputStream archivoImportado,  MyJsonWebToken token) throws IOException {

        log.info("Inicio - cargarArchivoExcelRegimen");

        if (model.getVersionExcel().equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_XLSM)) {
            this.xssfLibroExcel = new XSSFWorkbook(archivoImportado);
            xssFormulaEvaluator = xssfLibroExcel.getCreationHelper().createFormulaEvaluator();
            this.xssfLibroExcel.setMissingCellPolicy(Row.RETURN_NULL_AND_BLANK);
            archivoImportado.close();
        }


        HashMap<String, Object> hashExcel = new HashMap<String, Object>();

        //Contenedor Errores
        lstErrores = new ArrayList<>();

        //Registros ingresados a BD correctamente
        int correctos = 0;

        //Para validar repitencias ********************************0:Contenedor de repitencias
        List<String> lstRepitencias = new ArrayList<>();

        String strRepitencia = "";

        //Insumo para contar los correctos/incorrectos/total
        int filasReales = 0;


        CargaMasiva cargaMasiva = new CargaMasiva();

        List<Organigrama> listarOrganos = entidadClient.obtieneOrgano(model.getIdEntidad(), null).getPayload().getListaOrganigrama();
        objectMapOrganos = converListToMapOrgano(listarOrganos);


        List<UnidadOrganica> listarUnidadOrganica = entidadClient.obtieneUnidadOrganica(model.getIdEntidad()).getPayload().getListaUnidadOrganica();
        objectMapUnidadOrganica = converListToMapUnidadOrganica(listarUnidadOrganica);


        //Se usa para encontrar Grupo de servidores reporta , Grupo de servidores civiles, Familia de Puestos y Rol esa data esta en la misma tabla se buscara por descripcion
        List<PerfilGrupo> listarGrupoDeServidoresCiviles = perfilGrupoService.listAllPerfilGrupo().getPayload().getItems();
        objectMapGrupoSerCivil = converListToMapServCivil(listarGrupoDeServidoresCiviles);

        List<ParametrosDTO> listarNivel = maestraClient.rutaFileServerEntidad(Constantes.NIVEL_ORGANO, "").getPayload().getItems();
        objectMapNivelCategoria = converListToNivelCategoria(listarNivel);

        List<MaestraDetalle> listarPuestoTipoNivelEducativo = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null, Constantes.TBL_PER_NIV_EDU, null).getPayload().getMaestraDetalles();
        objectMapNivelEducativo = converListToPuestoTipo(listarPuestoTipoNivelEducativo);
        
        List<MaestraDetalle> listarEstadoEducativo = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                Constantes.TBL_PER_EST_NIV_EDU, null).getPayload().getMaestraDetalles();
        objectMapEstadoNivelEducativo = converListToPuestoTipo(listarEstadoEducativo);
        
        List<MaestraDetalle> listarSituacionAcademica = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                Constantes.TBL_PER_SIT_ACA, null).getPayload().getMaestraDetalles();
        objectMapSituAcademica = converListToPuestoTipo(listarSituacionAcademica);
        
        List<MaestraDetalle> listarGrados = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                Constantes.TBL_PER_EST_GRA, null).getPayload().getMaestraDetalles();
        objectMapGrados = converListToPuestoTipo(listarGrados);
        
        List<MaestraDetalle> listarTipoPractica = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                Constantes.TBL_PER_CON_PRA, null).getPayload().getMaestraDetalles();
        objectMapTipoPractica = converListToPuestoTipo(listarTipoPractica);
        
        List<MaestraDetalle> listarTipoPracticaCondicion = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                Constantes.TBL_PER_TIP_PRA, null).getPayload().getMaestraDetalles();
        objectMapTipoPracticaCondicion = converListToCondicion(listarTipoPracticaCondicion);
        
        List<MaestraDetalle> lstEstado  = maestraDetalleRepository.
				findDetalleByCodProg(Constantes.TIP_COND_PUESTO, Constantes.COD_POR_REVISAR);
        
        try {

            List<CargaMasiva> cargaMasivas = cargaMasivaRepositoryRepository.findByRegimen(ConstantesExcel.REGIMEN_1401);
            Map<String, Object> objectMap = converListToMap(cargaMasivas);
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NOMBRE_PLANTILLA_1401);
            int posicionHoja = 0;

            if (cargaMasiva != null) {
                if (cargaMasiva.getPosicion() != null) {
                    posicionHoja = cargaMasiva.getPosicion();
                } else {
                    lstErrores.add("Asigne valor en la Base de Datos la posicion de la hoja excel de la plantilla ");
                }
            }

            if (lstErrores.size() > 0) {
                hashExcel.put("lstErrores", lstErrores);
                return hashExcel;
            }

            XSSFSheet hojaXssfExcelLastRowNum = null;
            hojaXssfExcelLastRowNum = this.xssfLibroExcel.getSheetAt(posicionHoja);

            int lastRowNum = hojaXssfExcelLastRowNum.getLastRowNum() + 1;

            Iterator<Row> iteradorFila = this.getIteradorFilasExcel(model.getVersionExcel(), posicionHoja);

            int fila = 0;

            HSSFRow hssfFilaExcel = null;
            XSSFRow xssfFilaExcel = null;

            while (iteradorFila.hasNext()) {
                //Si se tiene la hoja de acuerdo lo conversado el iteradorFila valida automatico el final ya no es necesario lastRowNum
                if (lastRowNum == fila) {
                    log.info("Se termino la iteracion" + "----->>>" + fila + "------>>>" + lastRowNum);
                    break;
                }

                boolean ok = true;
                boolean save = true;
                Map<String, Object> perfilMap = new HashMap<>();
                Map<String, Object> perfilFuncionMap = new HashMap<>();
                Map<String, Object> perfilExperienciaMap = new HashMap<>();
                fila++;

                if (model.getVersionExcel().equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2003)) {
                    hssfFilaExcel = (HSSFRow) iteradorFila.next();
                } else if (model.getVersionExcel().equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2007)) {
                    xssfFilaExcel = (XSSFRow) iteradorFila.next();
                } else if (model.getVersionExcel().equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_XLSM)) {
                    xssfFilaExcel = (XSSFRow) iteradorFila.next();
                }


                //Valida que empiece a recorrer desde la 3 fila donde empiesa la data
                if (fila < 3) {
                    continue;
                }


                //TRANSACCION CASCADA ALL
                
               Perfil perfil = new Perfil();
               Map<String, Object> mapPerfil = settTabPerfil(cargaMasiva, perfil , hssfFilaExcel, xssfFilaExcel, fila, ok, objectMap);
               perfil = (Perfil) mapPerfil.get("perfil");
               boolean rptaPerfil = (Boolean)mapPerfil.get("valida");
               //TRANSACCION CASCADA ALL
               //Perfil perfil = new Perfil();
              // perfil =  settTabPerfil(cargaMasiva, perfil , hssfFilaExcel, xssfFilaExcel, fila, ok, objectMap);
               Map<String, Object>   mapPerfilForma = settTabFormacionAcademicaPerfil (cargaMasiva, perfil, hssfFilaExcel, xssfFilaExcel, fila, ok, objectMap, token);
               perfil = (Perfil) mapPerfilForma.get("perfil");
               boolean rptaPerfilForma = (Boolean)mapPerfilForma.get("valida");
               
               Long regimenId =Long.parseLong(model.getCodRegimen()); 
               perfil.setRegimenLaboralId(regimenId);
               perfil.setEntidadId(model.getIdEntidad());
               perfil.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());

               Map<String, Object> mapPerfilExp = settTabPerfilExp(perfil , cargaMasiva, hssfFilaExcel, xssfFilaExcel, fila, true, objectMap , token ); 
               List<PerfilExperiencia> ls = (List<PerfilExperiencia>)mapPerfilExp.get("pExperiencia");
               boolean rptaPerfilExpc = (Boolean)mapPerfilExp.get("valida");
               
               
               Map<String, Object> mapPerfilFuncion = settTabPerfilFuncion(hssfFilaExcel, xssfFilaExcel, fila, true, objectMap, token);
               PerfilFuncion perfilFuncion = (PerfilFuncion) mapPerfilFuncion.get("pFuncion");
               boolean rptaPerfilFunc = (Boolean)mapPerfilFuncion.get("valida");
               
               if(rptaPerfil && rptaPerfilForma && rptaPerfilExpc && rptaPerfilFunc)
               {
               	String estadoPerfil= lstEstado.get(0).getCodProg();
               	perfil.setIndOrigenPerfil(Constantes.MASIVO);
               	perfil.setIndEstadoRevision(Long.valueOf(estadoPerfil));
               	perfil.setEstadoFunc(Constantes.ACTIVO);
               	perfil.setEstadoForm(Constantes.ACTIVO);
               	perfil.setEstadoExp(Constantes.ACTIVO);
               	perfil.setPuestoCodigo(obtenerCodigoPuesto(model.getCodRegimen(), model.getIdEntidad()));
               	guardarBD(perfil, ls ,perfilFuncion );
               	correctos++;
               }
                //FIN


                filasReales++;
                hashExcel.put("correctos", correctos);
            } //ITERACION: FINAL


        } catch (Exception ex) {
            log.error("Error: " + ex.getMessage());
            lstErrores.add("Error al leer datos del excel: " + ex.getMessage());
        } finally {
            hashExcel.put("lstErrores", lstErrores);
            hashExcel.put("incorrectos", filasReales - correctos);
            hashExcel.put("total", correctos + (filasReales - correctos));

        }

        return hashExcel;
    }
    
    
    private Iterator<Row> getIteradorFilasExcel(String versionExcel, int posicionHoja) throws Exception {
        Iterator<Row> iteradorFila = null;
        HSSFSheet hojaHssfExcel = null;
        XSSFSheet hojaXssfExcel = null;

        if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2003)) {
            hojaHssfExcel = this.hssfLibroExcel.getSheetAt(posicionHoja);
            iteradorFila = hojaHssfExcel.rowIterator();
        } else if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2007)) {
            hojaXssfExcel = this.xssfLibroExcel.getSheetAt(posicionHoja);
            iteradorFila = hojaXssfExcel.rowIterator();
        } else if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_XLSM)) {
            hojaXssfExcel = this.xssfLibroExcel.getSheetAt(posicionHoja);
            iteradorFila = hojaXssfExcel.rowIterator();
        }

        return iteradorFila;
    }
    
    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMap(List<CargaMasiva> ls) {
        return ls.stream()
                .collect(Collectors.toMap(CargaMasiva::getNombreColumna, Function.identity()));
    }
    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMapOrgano(List<Organigrama> ls) {
        return ls.stream()
                .collect(Collectors.toMap(Organigrama::getDescripcion, Function.identity()));
    }
    
    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMapUnidadOrganica(List<UnidadOrganica> ls) {
        return ls.stream()
                .collect(Collectors.toMap(UnidadOrganica::getUnidadOrganica, Function.identity()));
    }
    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMapServCivil(List<PerfilGrupo> ls) {
        return ls.stream()
                .collect(Collectors.toMap(PerfilGrupo::getDescripcion, Function.identity()));
    }
    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToNivelCategoria(List<ParametrosDTO> ls) {
        return ls.stream()
                .collect(Collectors.toMap(ParametrosDTO::getDescripcion, Function.identity()));
    }
    /**
     * Valida sub string si es ERROR
     *
     * @param valor
     * @return
     */
    private String validarErrorSubstring(String valor) {
        if (!Util.isEmpty(valor)) {
            if (valor.length() > 4) {
                if (valor.substring(0, 5).equalsIgnoreCase(ConstantesExcel.ERROR)) {
                    return null;
                }
            }
        }
        return valor;
    }
    
    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestaOrgano(String valor, Map<String, Object> mapInput) {

        Organigrama organo = (Organigrama) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("nombre", organo.getDescripcion() == null ? null : organo.getDescripcion().toUpperCase());
        map.put("id", String.valueOf(organo.getOrganigramaId()));
        map.put("sigla", String.valueOf(organo.getSigla()));
        return map;
    }
    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestaUnidadOrganica(String valor, Map<String, Object> mapInput) {

        UnidadOrganica unidadOrganica = (UnidadOrganica) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("nombre", unidadOrganica.getUnidadOrganica() == null ? null : unidadOrganica.getUnidadOrganica().toUpperCase());
        map.put("id", String.valueOf(unidadOrganica.getOrganigramaId()));
        return map;
    }
    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestoSerCivil(String valor, Map<String, Object> mapInput) {

        PerfilGrupo perfilGrupo = (PerfilGrupo) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(perfilGrupo.getId()));
        return map;
    }
    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestoNivelCategoria(String valor, Map<String, Object> mapInput) {
        ParametrosDTO parametrosDTO = (ParametrosDTO) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(parametrosDTO.getParametroId()));
        return map;
    }
    /**
     * Codigo de Puesto
     *
     * @param valor
     * @param svcivil
     * @param familia
     * @param rol
     * @param objectMapGrupoSerCivil
     * @return
     */
    private String codigoPuesto(String valor, String svcivil, String familia, String rol, Map<String, Object> objectMapGrupoSerCivil) {
        PerfilGrupo perfilGrupoScivil = (PerfilGrupo) objectMapGrupoSerCivil.get(svcivil);
        PerfilGrupo perfilFamilia = (PerfilGrupo) objectMapGrupoSerCivil.get(familia);
        PerfilGrupo perfilRol = (PerfilGrupo) objectMapGrupoSerCivil.get(rol);

        return perfilGrupoScivil.getCodigo() + "_" + perfilFamilia.getCodigo() + "_" + perfilRol.getCodigo() + "_" + valor;

    }
    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestoPuestoTipo(String valor, Map<String, Object> mapInput) {
        MaestraDetalle maestraDetalle = (MaestraDetalle) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(maestraDetalle.getMaeDetalleId()));
        return map;
    }
    
    /**
     * Save
     *
     * @param cargaMasiva
     * @param hssfFilaExcel
     * @param xssfFilaExcel
     * @param fila
     * @param ok
     * @param objectMap
     * @return
     */
    private Map<String, Object> settTabFormacionAcademicaPerfil (CargaMasiva cargaMasiva, Perfil perfil,
                                                 HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila,
                                                 boolean ok, Map<String, Object> objectMap, MyJsonWebToken token) throws Exception {

        FormacionAcademica formacionAcademicaPrimaria = new FormacionAcademica();
        FormacionAcademica formacionAcademicaSecundaria = new FormacionAcademica();

        FormacionAcademica formacionAcademicaTecnicaBasicaEgresado = new FormacionAcademica();
        FormacionAcademica formacionAcademicaTecnicaBasicaTitulado = new FormacionAcademica();

        FormacionAcademica formacionAcademicaTecnicaSuperiorEgresado = new FormacionAcademica();
        FormacionAcademica formacionAcademicaTecnicaSuperiorTitulado = new FormacionAcademica();

        FormacionAcademica formacionAcademicaUniversitariaEgresado = new FormacionAcademica();
        FormacionAcademica formacionAcademicaUniversitariaBachiller = new FormacionAcademica();
        FormacionAcademica formacionAcademicaUniversitariaTitulado = new FormacionAcademica();
        FormacionAcademica formacionAcademicaUniversitariaMaestria = new FormacionAcademica();
        FormacionAcademica formacionAcademicaUniversitariaDoctorado = new FormacionAcademica();


        List<FormacionAcademica> lstFormAcademica = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaTecnicaBasicaEgresadoList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaTecnicaBasicaTituladoList = new ArrayList<>();

        List<CarreraFormacionAcademica> carreraFormacionAcademicaTecnicaSuperiorEgresadoList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaTecnicaSuperiorTituladoList = new ArrayList<>();


        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaEgresadoList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaBachillerList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaTituladoList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaMaestriaList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaDoctoradoList = new ArrayList<>();


        List<Conocimiento> conocimientoList = new ArrayList<>();
        Map<String, String> valores;
        Map<String, Object> perfilFormaMap = new HashMap<>();
        String nivelEducativoPrimaria = "";
        String nivelEducativoSecundaria = "";

        //23: PRIMARIA (NIVEL EDUCATIVO ID)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.PRIMARIA); //(NIVEL EDUCATIVO ID)
        if (cargaMasiva != null) {
             nivelEducativoPrimaria = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(nivelEducativoPrimaria);

            if (rpta == null) {
                this.lstErrores.add(nivelEducativoPrimaria);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(nivelEducativoPrimaria)) { //ESTE VALOR SI SE PASA EN UNA CONSTANTE IGUAL A BD/ O MEJOR COD PROGRAMADOR
                    valores = splitCeldaCompuestoPuestoTipo("PRIMARIA", objectMapNivelEducativo);
                    formacionAcademicaPrimaria = new FormacionAcademica();
                    formacionAcademicaPrimaria = settValorersDefault(perfil, token);
                    formacionAcademicaPrimaria.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }


        }

        //24: ESTADO_NIVEL_PRIMARIA (ESTADO NIVEL EDUCATIVO ID)
        
        //if (ok) {
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_NIVEL_PRIMARIA); //(ESTADO NIVEL EDUCATIVO ID)
            if (cargaMasiva != null) {
                String estadoNivelEducativoPrimaria = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator,
                		nivelEducativoPrimaria);
                
                String rpta = validarErrorSubstring(estadoNivelEducativoPrimaria);


                if (rpta == null) {
                    this.lstErrores.add(estadoNivelEducativoPrimaria);
                    ok = false;
                }

                if (ok)
                    if (!Util.isEmpty(estadoNivelEducativoPrimaria)) {
                        valores = splitCeldaCompuestoPuestoTipo(estadoNivelEducativoPrimaria, objectMapEstadoNivelEducativo);
                        formacionAcademicaPrimaria.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));

                    }

            }
        //}


        // 1: SE CARGA EL PRIMER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaPrimaria)) {
        	lstFormAcademica.add(formacionAcademicaPrimaria);
        }    
        
        //25: SECUNDARIA (NIVEL EDUCATIVO ID)
        
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SECUNDARIA); //(NIVEL EDUCATIVO ID)

        if (cargaMasiva != null) {
             nivelEducativoSecundaria = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(nivelEducativoSecundaria);

            if (rpta == null) {
                this.lstErrores.add(nivelEducativoSecundaria);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(nivelEducativoSecundaria)) {
                    valores = splitCeldaCompuestoPuestoTipo("SECUNDARIA", objectMapNivelEducativo);
                    formacionAcademicaSecundaria = new FormacionAcademica();
                    formacionAcademicaSecundaria = settValorersDefault(perfil, token);
                    formacionAcademicaSecundaria.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }

        }


        //26: ESTADO_NIVEL_SECUNDARIA (ESTADO NIVEL EDUCATIVO ID)
       // ok = true;
        //if (ok) {
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_NIVEL_SECUNDARIA); //(ESTADO NIVEL EDUCATIVO ID)
            if (cargaMasiva != null) {
                String estadoNivelEducativoSecundaria = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator,
                		nivelEducativoSecundaria);
                String rpta = validarErrorSubstring(estadoNivelEducativoSecundaria);


                if (rpta == null) {
                    this.lstErrores.add(estadoNivelEducativoSecundaria);
                    ok = false;
                }

                if (ok)
                    if (!Util.isEmpty(estadoNivelEducativoSecundaria)) {
                        valores = splitCeldaCompuestoPuestoTipo(estadoNivelEducativoSecundaria, objectMapEstadoNivelEducativo);
                        formacionAcademicaSecundaria.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));

                    }

            }
        //}


        // 2: SE CARGA EL SEGUNDO OBJETO A LA LISTA lstFormAcademica
         if(!Objects.isNull(formacionAcademicaSecundaria)) {
        	lstFormAcademica.add(formacionAcademicaSecundaria);
        }
       

        //27: TECNICA_BASICA (NIVEL EDUCATIVO ID)
        
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.TECNICA_BASICA);
        String tecnicaBasica = null;
        if (cargaMasiva != null) {
            tecnicaBasica = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(tecnicaBasica);

            if (rpta == null) {
                this.lstErrores.add(tecnicaBasica);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(tecnicaBasica)) {
                    valores = splitCeldaCompuestoPuestoTipo("TÉCNICA BÁSICA (1 o 2 años)", objectMapNivelEducativo); //CONSTANTE
                    formacionAcademicaTecnicaBasicaEgresado = new FormacionAcademica();
                    formacionAcademicaTecnicaBasicaEgresado = settValorersDefault(perfil, token);
                    formacionAcademicaTecnicaBasicaEgresado.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }

        }


        //28: ESTADO_TECNICA_BASICA (ESTADO NIVEL EDUCATIVO ID)
        
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_TECNICA_BASICA); //(ESTADO NIVEL EDUCATIVO ID)
                if (cargaMasiva != null) {
                    String estadoTecnicaBasica = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator, tecnicaBasica);
                    

                    String rpta = validarErrorSubstring(estadoTecnicaBasica);

                    if (rpta == null) {
                        this.lstErrores.add(estadoTecnicaBasica);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(estadoTecnicaBasica)) {
                            valores = splitCeldaCompuestoPuestoTipo(estadoTecnicaBasica, objectMapEstadoNivelEducativo);
                            formacionAcademicaTecnicaBasicaEgresado.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //29: SITUACION_TECNICA_BASICA_EGRESADO (SITUACION ACADEMICA ID)
        
        String situacionTecBasiEgresado = "";
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TECNICA_BASICA_EGRESADO); //(SITUACION ACADEMICA ID)
                if (cargaMasiva != null) {
                     situacionTecBasiEgresado = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situacionTecBasiEgresado);


                    if (rpta == null) {
                        this.lstErrores.add(situacionTecBasiEgresado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situacionTecBasiEgresado)) {
                            valores = splitCeldaCompuestoPuestoTipo("EGRESADO(A)", objectMapSituAcademica);
                            formacionAcademicaTecnicaBasicaEgresado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //30:ESTUDIOS_REQUERIDOS_TBASICA_EGRE
        
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ESTUDIOS_REQUERIDOS_TBASICA_EGRE);
                if (cargaMasiva != null) {
                    String estudiosRequeridosTecBasiEgresado = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator, situacionTecBasiEgresado);

                    String rpta = validarErrorSubstring(estudiosRequeridosTecBasiEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(estudiosRequeridosTecBasiEgresado);
                        ok = false;
                    }

                    List<String> lstCarreras = listCarreras(rpta);
                    List<CarreraProfesional> carreraProfesionalList;
                    if (lstCarreras != null && !lstCarreras.isEmpty()) {
                        carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 56L);
                        formacionAcademicaTecnicaBasicaEgresado.setCarreraFormacionAcademicas(new ArrayList<>());

                        if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                            for (CarreraProfesional it : carreraProfesionalList) {
                                CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                                carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                carreraFormacionAcademica.setCarreraProfesional(it);
                                carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaTecnicaBasicaEgresado);
                                carreraFormacionAcademicaTecnicaBasicaEgresadoList.add(carreraFormacionAcademica);
                            }
                        }
                        formacionAcademicaTecnicaBasicaEgresado.setCarreraFormacionAcademicas(carreraFormacionAcademicaTecnicaBasicaEgresadoList);

                    }

                }
            }
        }


        //3: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaTecnicaBasicaEgresado)) {
        	lstFormAcademica.add(formacionAcademicaTecnicaBasicaEgresado);	
        }
        

        //31:GRAD_EDUC_TEC_BASICA_TITU_LICENCIATURA (SITUACION ACADEMICA ID)
       // ok = true;
        String gradoEduTecBasTituLicenciatura = "";
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TEC_BASICA_TITU_LICENCIATURA); //(SITUACION ACADEMICA ID)
                if (cargaMasiva != null) {
                     gradoEduTecBasTituLicenciatura = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(gradoEduTecBasTituLicenciatura);


                    if (rpta == null) {
                        this.lstErrores.add(gradoEduTecBasTituLicenciatura);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(gradoEduTecBasTituLicenciatura)) {
                            valores = splitCeldaCompuestoPuestoTipo("TÍTULO O LICENCIATURA", objectMapSituAcademica);
                            formacionAcademicaTecnicaBasicaTitulado = new FormacionAcademica();
                            formacionAcademicaTecnicaBasicaTitulado = convertFormacionAcademica(formacionAcademicaTecnicaBasicaEgresado);
                            formacionAcademicaTecnicaBasicaTitulado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaTecnicaBasicaTitulado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));

                        }

                }
            }
        }


        //32:ESTU_REQ_TEC_BASICA_TITU
        
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ESTU_REQ_TEC_BASICA_TITU);
                String estudiosRequeridosTecSupTitulado = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva,  this.hssFormulaEvaluator, this.xssFormulaEvaluator, gradoEduTecBasTituLicenciatura);

                String rpta = validarErrorSubstring(estudiosRequeridosTecSupTitulado);

                if (rpta == null) {
                    this.lstErrores.add(estudiosRequeridosTecSupTitulado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 58L);
                    formacionAcademicaTecnicaBasicaTitulado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaTecnicaBasicaTitulado);
                            carreraFormacionAcademicaTecnicaBasicaTituladoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaTecnicaBasicaTitulado.setCarreraFormacionAcademicas(carreraFormacionAcademicaTecnicaBasicaTituladoList);

                }
            }
        }

        //4: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaTecnicaBasicaTitulado)) {
        	lstFormAcademica.add(formacionAcademicaTecnicaBasicaTitulado);	
        }
        


        //33:TECNICA_SUPERIOR(NIVEL EDUCATIVO)
        
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.TECNICA_SUPERIOR);
        String tecnicaSuperior = null;
        if (cargaMasiva != null) {
            tecnicaSuperior = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(tecnicaSuperior);

            if (rpta == null) {
                this.lstErrores.add(tecnicaSuperior);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(tecnicaSuperior)) {
                    valores = splitCeldaCompuestoPuestoTipo("TÉCNICA SUPERIOR (3 o 4 años)", objectMapNivelEducativo);
                    formacionAcademicaTecnicaSuperiorEgresado = new FormacionAcademica();
                    formacionAcademicaTecnicaSuperiorEgresado = settValorersDefault(perfil , token);
                    formacionAcademicaTecnicaSuperiorEgresado.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }

        }


        //34: SITUACION_TECNICA_SUPERIOR  (ESTADO NIVEL EDUCATIVC ID)
        
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaSuperior)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_TECNICA_SUPERIOR); // (ESTADO NIVEL EDUCATIVC ID)
                if (cargaMasiva != null) {
                    String estadoTecnicaSuperior = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator, tecnicaSuperior);

                    String rpta = validarErrorSubstring(estadoTecnicaSuperior);

                    if (rpta == null) {
                        this.lstErrores.add(estadoTecnicaSuperior);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(estadoTecnicaSuperior)) {
                            valores = splitCeldaCompuestoPuestoTipo(estadoTecnicaSuperior, objectMapEstadoNivelEducativo);
                            formacionAcademicaTecnicaSuperiorEgresado.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //35:GRAD_EDUC_TEC_SUP_EGRESADO (SITUACION CADEMICA ID)
        
        String situacionTecnicaSuperiorEgresado = "";
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaSuperior)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TEC_SUP_EGRESADO); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                     situacionTecnicaSuperiorEgresado = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situacionTecnicaSuperiorEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(situacionTecnicaSuperiorEgresado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situacionTecnicaSuperiorEgresado)) {
                            valores = splitCeldaCompuestoPuestoTipo("EGRESADO(A)", objectMapSituAcademica);
                            formacionAcademicaTecnicaSuperiorEgresado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //36: ESTU_REQ_TEC_SUP_EGRESADO
        
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaSuperior)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ESTU_REQ_TEC_SUP_EGRESADO);

                if (cargaMasiva != null) {
                    String estuReqTecnSupeEgresado = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator,situacionTecnicaSuperiorEgresado);

                    String rpta = validarErrorSubstring(estuReqTecnSupeEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(estuReqTecnSupeEgresado);
                        ok = false;
                    }

                    List<String> lstCarreras = listCarreras(rpta);
                    List<CarreraProfesional> carreraProfesionalList;
                    if (lstCarreras != null && !lstCarreras.isEmpty()) {
                        carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 56L);
                        formacionAcademicaTecnicaSuperiorEgresado.setCarreraFormacionAcademicas(new ArrayList<>());

                        if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                            for (CarreraProfesional it : carreraProfesionalList) {
                                CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                                carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                carreraFormacionAcademica.setCarreraProfesional(it);
                                carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaTecnicaSuperiorEgresado);
                                carreraFormacionAcademicaTecnicaSuperiorEgresadoList.add(carreraFormacionAcademica);
                            }
                        }
                        formacionAcademicaTecnicaSuperiorEgresado.setCarreraFormacionAcademicas(carreraFormacionAcademicaTecnicaSuperiorEgresadoList);

                    }

                }
            }
        }


        //5: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaTecnicaSuperiorEgresado)) {	
        	 lstFormAcademica.add(formacionAcademicaTecnicaSuperiorEgresado);
        }


        //37: GRAD_EDUC_TEC_SUP_TITU_LIC (situacion acdemica id)
        
        String situTecSupTitulado="";
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TEC_SUP_TITU_LIC);
                if (cargaMasiva != null) {
                     situTecSupTitulado = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situTecSupTitulado);


                    if (rpta == null) {
                        this.lstErrores.add(situTecSupTitulado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situTecSupTitulado)) {
                            valores = splitCeldaCompuestoPuestoTipo("TÍTULO O LICENCIATURA", objectMapSituAcademica);
                            formacionAcademicaTecnicaSuperiorTitulado = new FormacionAcademica();
                            formacionAcademicaTecnicaSuperiorTitulado = convertFormacionAcademica(formacionAcademicaTecnicaSuperiorEgresado);
                            formacionAcademicaTecnicaSuperiorTitulado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));

                            formacionAcademicaTecnicaSuperiorTitulado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());


                        }

                }
            }
        }


        //38:EST_REQ_TEC_SUP_TITU
        
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaSuperior)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_TEC_SUP_TITU);
                String estudiosRequeridosTecSupTitulado = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator, situTecSupTitulado);

                String rpta = validarErrorSubstring(estudiosRequeridosTecSupTitulado);

                if (rpta == null) {
                    this.lstErrores.add(estudiosRequeridosTecSupTitulado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 58L);
                    formacionAcademicaTecnicaSuperiorTitulado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaTecnicaSuperiorTitulado);
                            carreraFormacionAcademicaTecnicaSuperiorTituladoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaTecnicaSuperiorTitulado.setCarreraFormacionAcademicas(carreraFormacionAcademicaTecnicaSuperiorTituladoList);

                }
            }
        }

        //6: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaTecnicaSuperiorTitulado)) {
        	lstFormAcademica.add(formacionAcademicaTecnicaSuperiorTitulado);	
        }
        


        //39: UNIVERSITARIA (NIVEL EDUCATIVO)
        
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.UNIVERSITARIA);
        String universitaria = null;
        if (cargaMasiva != null) {
            universitaria = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(universitaria);

            if (rpta == null) {
                this.lstErrores.add(universitaria);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(universitaria)) {
                    valores = splitCeldaCompuestoPuestoTipo("UNIVERSITARIA", objectMapNivelEducativo);
                    formacionAcademicaUniversitariaEgresado =  new FormacionAcademica();
                    formacionAcademicaUniversitariaEgresado = settValorersDefault(perfil, token);
                    formacionAcademicaUniversitariaEgresado.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }

        }


        //40: SITUACION_UNIVERSITARIA (estado_nivel_educativo_id )
        
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_UNIVERSITARIA); // estado_nivel_educativo_id
                if (cargaMasiva != null) {
                    String estadoSituAcaUniverEgresado = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator
                    		,universitaria);

                    String rpta = validarErrorSubstring(estadoSituAcaUniverEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(estadoSituAcaUniverEgresado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(estadoSituAcaUniverEgresado)) {
                            valores = splitCeldaCompuestoPuestoTipo(estadoSituAcaUniverEgresado, objectMapEstadoNivelEducativo);
                            formacionAcademicaUniversitariaEgresado.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }

        //41: GRAD_EDUC_SUPE_EGRESADO ((SITUACION CADEMICA ID))
        
        String situAcaUniverEgresado ="";
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUPE_EGRESADO); // ((SITUACION CADEMICA ID))
                if (cargaMasiva != null) {
                     situAcaUniverEgresado = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcaUniverEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(situAcaUniverEgresado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcaUniverEgresado)) {
                            valores = splitCeldaCompuestoPuestoTipo("EGRESADO(A)", objectMapSituAcademica);
                            formacionAcademicaUniversitariaEgresado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }

        //42: EST_REQ_EDU_SUPERIOR
        
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_EDU_SUPERIOR);
                String estuRequeUnivEgresado = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator
                		,situAcaUniverEgresado);

                String rpta = validarErrorSubstring(estuRequeUnivEgresado);

                if (rpta == null) {
                    this.lstErrores.add(estuRequeUnivEgresado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 58L);
                    formacionAcademicaUniversitariaEgresado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaEgresado);
                            carreraFormacionAcademicaUniversitariaEgresadoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaEgresado.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaEgresadoList);

                }
            }
        }

        //7: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaUniversitariaEgresado)) {
        	lstFormAcademica.add(formacionAcademicaUniversitariaEgresado);
        }
        
        //43: GRAD_EDUC_SUP_BACHILLER ( SITUACION ACADEMICA)
        
        //cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_BACHILLER);
        String situAcadeBachiller = "";
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_BACHILLER); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                     situAcadeBachiller = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcadeBachiller);

                    if (rpta == null) {
                        this.lstErrores.add(situAcadeBachiller);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcadeBachiller)) {
                            valores = splitCeldaCompuestoPuestoTipo("BACHILLER", objectMapSituAcademica);
                            formacionAcademicaUniversitariaBachiller = new FormacionAcademica();
                            formacionAcademicaUniversitariaBachiller = convertFormacionAcademica(formacionAcademicaUniversitariaEgresado);
                            formacionAcademicaUniversitariaBachiller.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaBachiller.setSituacionAcademicaId(Long.valueOf(valores.get("id")));


                        }

                }
            }
        }

        // 44: EST_REQ_SUP_BACHILLER
        
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_SUP_BACHILLER);
                String estuReqBachiller = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator
                		,situAcadeBachiller);

                String rpta = validarErrorSubstring(estuReqBachiller);

                if (rpta == null) {
                    this.lstErrores.add(estuReqBachiller);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 57L);
                    formacionAcademicaUniversitariaBachiller.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaBachiller);
                            carreraFormacionAcademicaUniversitariaBachillerList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaBachiller.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaBachillerList);

                }
            }
        }

        //8: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaUniversitariaBachiller)) {
        	lstFormAcademica.add(formacionAcademicaUniversitariaBachiller);
        }
        


        // 45: GRAD_EDUC_SUP_TIT_LICENCIATURA (SITUACION CADEMICA ID)
       // ok = true;
        //cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_TIT_LICENCIATURA);
        String situAcadeUniTitulado="";
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_TIT_LICENCIATURA); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                     situAcadeUniTitulado = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcadeUniTitulado);

                    if (rpta == null) {
                        this.lstErrores.add(situAcadeUniTitulado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcadeUniTitulado)) {
                            valores = splitCeldaCompuestoPuestoTipo("TÍTULO O LICENCIATURA", objectMapSituAcademica);
                            formacionAcademicaUniversitariaMaestria = new FormacionAcademica();
                            formacionAcademicaUniversitariaTitulado = convertFormacionAcademica(formacionAcademicaUniversitariaEgresado);
                            formacionAcademicaUniversitariaMaestria.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaTitulado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));


                        }

                }
            }
        }

        //46: EST_REQ_SUP_TITU_LICENCIATURA
       // ok = true;
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_SUP_TITU_LICENCIATURA);
                String estuReqTitulado = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator
                		,situAcadeUniTitulado);

                String rpta = validarErrorSubstring(estuReqTitulado);

                if (rpta == null) {
                    this.lstErrores.add(estuReqTitulado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 58L);
                    formacionAcademicaUniversitariaTitulado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaTitulado);
                            carreraFormacionAcademicaUniversitariaTituladoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaTitulado.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaTituladoList);

                }
            }
        }

        //9: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaUniversitariaTitulado)) {
        	lstFormAcademica.add(formacionAcademicaUniversitariaTitulado);
        }
        

        //47: GRAD_EDUC_SUP_MAESTRIA  (SITUACION CADEMICA ID)
       
        String situAcadeUniMaestria = "";
        //cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_MAESTRIA); //(SITUACION CADEMICA ID)
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_MAESTRIA); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                     situAcadeUniMaestria = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcadeUniMaestria);

                    if (rpta == null) {
                        this.lstErrores.add(situAcadeUniMaestria);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcadeUniMaestria)) {
                            valores = splitCeldaCompuestoPuestoTipo("MAESTRÍA", objectMapSituAcademica);
                            formacionAcademicaUniversitariaMaestria = new FormacionAcademica();
                            formacionAcademicaUniversitariaMaestria = convertFormacionAcademica(formacionAcademicaUniversitariaEgresado);
                            formacionAcademicaUniversitariaMaestria.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaMaestria.setSituacionAcademicaId(Long.valueOf(valores.get("id")));


                        }

                }
            }
        }

        //48: EST_REQ_MAESTRIA
        
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_MAESTRIA);
                String estuReqUniMAestria = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator
                		,situAcadeUniMaestria);

                String rpta = validarErrorSubstring(estuReqUniMAestria);

                if (rpta == null) {
                    this.lstErrores.add(estuReqUniMAestria);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 59L);
                    formacionAcademicaUniversitariaMaestria.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaMaestria);
                            carreraFormacionAcademicaUniversitariaMaestriaList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaMaestria.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaMaestriaList);

                }
            }
        }


        //49: GRAD_EDUC_SUP_DOCTORADO
       // ok = true;
        //cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_DOCTORADO); //(SITUACION CADEMICA ID)
        String situAcadeUniDoctorado="";
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_DOCTORADO); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                     situAcadeUniDoctorado = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcadeUniDoctorado);

                    if (rpta == null) {
                        this.lstErrores.add(situAcadeUniDoctorado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcadeUniDoctorado)) {
                            valores = splitCeldaCompuestoPuestoTipo("DOCTORADO", objectMapSituAcademica);
                            formacionAcademicaUniversitariaDoctorado =  new FormacionAcademica();
                            formacionAcademicaUniversitariaDoctorado = convertFormacionAcademica(formacionAcademicaUniversitariaEgresado);
                            formacionAcademicaUniversitariaDoctorado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaDoctorado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));


                        }

                }
            }
        }

        //50: EST_REQ_DOCTORADO
        
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_DOCTORADO);
                String estuReqUniDoctorado = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator
                		,situAcadeUniDoctorado);

                String rpta = validarErrorSubstring(estuReqUniDoctorado);

                if (rpta == null) {
                    this.lstErrores.add(estuReqUniDoctorado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 60L);
                    formacionAcademicaUniversitariaDoctorado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaDoctorado);
                            carreraFormacionAcademicaUniversitariaDoctoradoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaDoctorado.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaDoctoradoList);

                }
            }
        }



        // 51: MAESTRIA_SITUACION
       // ok = true;
        String maestriaSituacion = "";
        if(!Objects.isNull(formacionAcademicaUniversitariaMaestria)) {
        	if (formacionAcademicaUniversitariaMaestria.getSituacionAcademicaId() != null) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.MAESTRIA_SITUACION);
                if (cargaMasiva != null) {
                     maestriaSituacion = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(maestriaSituacion);

                    if (rpta == null) {
                        this.lstErrores.add(maestriaSituacion);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(maestriaSituacion)) {
                            valores = splitCeldaCompuestoPuestoTipo("GRADO", objectMapGrados);
                            formacionAcademicaUniversitariaMaestria.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaMaestria.setEstadoSituacionAcademicaId(Long.valueOf(valores.get("id")));
                        }

                }
            }	
        }


        //52: MAESTRIA_DESCRIPCION
        
        if(!Objects.isNull(formacionAcademicaUniversitariaMaestria)) {
            if (formacionAcademicaUniversitariaMaestria.getSituacionAcademicaId() != null) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.MAESTRIA_DESCRIPCION);
                if (cargaMasiva != null) {
                    String maestriaDescri = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator
                    		,maestriaSituacion);

                    String rpta = validarErrorSubstring(maestriaDescri);

                    if (rpta == null) {
                        this.lstErrores.add(maestriaDescri);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(maestriaDescri)) {
                            formacionAcademicaUniversitariaMaestria.setNombreGrado(maestriaDescri);
                        }

                }
            }
        }


        //10: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        if(!Objects.isNull(formacionAcademicaUniversitariaMaestria)) {
        	lstFormAcademica.add(formacionAcademicaUniversitariaMaestria);	
        }
        

        // 53: DOCTORADO_SITUACION
        
        String doctoradoSituacion="";
        if(!Objects.isNull(formacionAcademicaUniversitariaDoctorado)) {
            if (formacionAcademicaUniversitariaDoctorado.getSituacionAcademicaId() != null) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.DOCTORADO_SITUACION);
                if (cargaMasiva != null) {
                     doctoradoSituacion = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(doctoradoSituacion);

                    if (rpta == null) {
                        this.lstErrores.add(doctoradoSituacion);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(doctoradoSituacion)) {
                            valores = splitCeldaCompuestoPuestoTipo("EGRESADO", objectMapGrados);
                            formacionAcademicaUniversitariaDoctorado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaDoctorado.setEstadoSituacionAcademicaId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //54: DESCRIPCION_DOCTORADO
        
        if(!Objects.isNull(formacionAcademicaUniversitariaDoctorado)) {
        	if (formacionAcademicaUniversitariaDoctorado.getSituacionAcademicaId() != null) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.DESCRIPCION_DOCTORADO);
                if (cargaMasiva != null) {
                    String doctoradoDescri = validacion.obtenerValorCeldaDependiente(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator
                    		,doctoradoSituacion);

                    String rpta = validarErrorSubstring(doctoradoDescri);

                    if (rpta == null) {
                        this.lstErrores.add(doctoradoDescri);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(doctoradoDescri)) {
                            formacionAcademicaUniversitariaDoctorado.setNombreGrado(doctoradoDescri);
                        }

                }
            }
        }
        //11: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaUniversitariaDoctorado);

        // formacionAcademicaRepository.saveAll(lstFormAcademica);
        
        //55:CONOCIMIENTOS_TECNICOS
        
        List<String> listConcocimientos = new ArrayList<>();
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.CONOCIMIENTOS_BASICOS);
        if (cargaMasiva != null) {
            String conociTecnicos = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(conociTecnicos);

            if (rpta == null) {
                this.lstErrores.add(conociTecnicos);
                ok = false;
            }

            listConcocimientos = listCarreras(rpta);
            List<MaeConocimiento> maeConocimientoList = maeConocimientoRepository.findByDescripcionInAndEstadoRegistro(listConcocimientos, Constantes.ACTIVO);
            if (maeConocimientoList != null && !maeConocimientoList.isEmpty()) {
                for (MaeConocimiento it : maeConocimientoList) {
                    Conocimiento conocimiento = new Conocimiento();
                    conocimiento.setMaeConocimiento(it);
                    conocimiento.setPerfil(perfil);
                    conocimiento.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    conocimientoList.add(conocimiento);
                }
            }
        }

        for (FormacionAcademica it : lstFormAcademica){
            it.setPerfil(perfil);
            it.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        }

        perfil.setFormacionAcademicaList(lstFormAcademica);
        perfil.setConocimientoList(conocimientoList);
        
        perfilFormaMap.put("valida", ok);
        perfilFormaMap.put("perfil", perfil);
        //return perfil;
        return perfilFormaMap;

    }
    
    private Map<String, Object> settTabPerfilExp(Perfil perfil , CargaMasiva cargaMasiva, HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila,
            boolean ok, Map<String, Object> objectMap , MyJsonWebToken token) throws Exception {
			PerfilExperiencia perfilExperiencia = new PerfilExperiencia();
			perfilExperiencia.setPerfilExperienciaDetalleList1(new ArrayList<>());
			perfilExperiencia.setPerfilExperienciaDetalleList2(new ArrayList<>());
			Map<String, Object> perfilExpMap =  new HashMap<>();
			
			// 56: HABILIDADES O COMPETENCIAS
			cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.HABILIDADES_COMPETENCIAS);
			
			if (cargaMasiva != null) {
			
				String habilidades = validacion.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
				String rpta = validarErrorSubstring(habilidades);
				
				if (rpta == null) {
					this.lstErrores.add(habilidades);
					ok = false;
				}
				
				if (ok)
					if (!Util.isEmpty(habilidades)) {
						perfilExperiencia.setPerfilExperienciaDetalleList1(splitCeldaHabilidadesCompetencia(habilidades,perfilExperiencia , token));
					}
			}
	
			
			List<PerfilExperiencia> ls = new ArrayList<>();
			perfilExperiencia.setPerfilId(perfil.getPerfilId());
			perfilExperiencia.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			ls.add(perfilExperiencia);
			
			perfilExpMap.put("pExperiencia", ls);
			perfilExpMap.put("valida", ok);
			return perfilExpMap;
			//return ls;
			
			}


    private FormacionAcademica settValorersDefault(Perfil perfil, MyJsonWebToken token) {
        FormacionAcademica formacionAcademica = new FormacionAcademica();
        formacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        formacionAcademica.setPerfil(perfil);
        return formacionAcademica;
    }
    /**
     * @param valor
     * @return
     */
    private List<String> listCarreras(String valor) {
        if (!Util.isEmpty(valor)) {
            String[] str = valor.trim().split(",");
            String[] array = Arrays.stream(str).map(String::trim).toArray(String[]::new);
            return Util.convertArrayToList(array);
        }
        return null;
    }
    
    private FormacionAcademica convertFormacionAcademica(FormacionAcademica formacionAcademicaTecnicaBasica) {
        FormacionAcademica formacionAcademica = new FormacionAcademica();
        formacionAcademica.setNivelEducativoId(formacionAcademicaTecnicaBasica.getNivelEducativoId());
        formacionAcademica.setEstadoNivelEducativoId(formacionAcademicaTecnicaBasica.getEstadoNivelEducativoId());

        return formacionAcademica;
    }

    private Map<String, Object> converListToPuestoTipo(List<MaestraDetalle> ls) {
        return ls.stream()
                .collect(Collectors.toMap(MaestraDetalle::getDescripcion, Function.identity()));
    }
    private Map<String, Object> converListToCondicion(List<MaestraDetalle> ls) {
        return ls.stream()
                .collect(Collectors.toMap(MaestraDetalle::getCodProg, Function.identity()));
    }
    
    private List<PerfilExperienciaDetalle> splitCeldaHabilidadesCompetencia(String valor,PerfilExperiencia perfilExp , MyJsonWebToken token) {
    	List<PerfilExperienciaDetalle> listaPerfilExperienciaDetalle = new  ArrayList<>();
        List<String> listaHabilidades = Arrays.asList(valor.split(",", -1));
        for (int i = 0; i < listaHabilidades.size(); i++) {
        	PerfilExperienciaDetalle perfilExperienciaDetalle= new PerfilExperienciaDetalle();
			String habilidades = listaHabilidades.get(i);
			perfilExperienciaDetalle.setDescripcion(habilidades);
			perfilExperienciaDetalle.setOrden(Integer.valueOf(i).longValue());
			perfilExperienciaDetalle.setTiDaExId(Constantes.HABILIDADES);
			perfilExperienciaDetalle.setPerfilExperiencia(perfilExp);
            perfilExperienciaDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			listaPerfilExperienciaDetalle.add(perfilExperienciaDetalle);
		}

        return listaPerfilExperienciaDetalle;
    }
    
    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaTipoPractica(String valor, Map<String, Object> mapInput) {
    	MaestraDetalle maestraDetalle = (MaestraDetalle) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        String condicion = "";
        map.put("id", String.valueOf(maestraDetalle.getMaeDetalleId()));
        map.put("codProg", String.valueOf(maestraDetalle.getCodProg()));
        if (Constantes.ESTUDIANTE.equals(String.valueOf(maestraDetalle.getCodProg()))) {
        	condicion = Constantes.PRACTICANTE_PRE_PROFESIONAL;
		}else if (Constantes.EGRESADO.equals(String.valueOf(maestraDetalle.getCodProg()))){
        	condicion = Constantes.PRACTICANTE_PROFESIONAL;
		}

        map.put("codProgCondicion", condicion);

        return map;
    }
    
    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaTipoPracticaCondicion(String valor, Map<String, Object> mapInput) {
    	MaestraDetalle maestraDetalle = (MaestraDetalle) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(maestraDetalle.getMaeDetalleId()));
        return map;
    }
    
	public String obtenerCodigoPuesto(String codigoRegimen, Long entidadId) {

		String correlativo;
		String codigoPuesto = "";
		correlativo = perfilRepositoryJdbc.findCorrelativoCodigoPuesto(Long.parseLong(codigoRegimen), entidadId);
		if (Constantes.REGIMEN_LABORAL_1401.equals(codigoRegimen)) {
			codigoPuesto += Constantes.COD_REG_LABORAL_1401 + Constantes.GUION;
		} 
		return codigoPuesto + correlativo;
	}
}

