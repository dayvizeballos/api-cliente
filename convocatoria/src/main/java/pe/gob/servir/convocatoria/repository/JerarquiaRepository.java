package pe.gob.servir.convocatoria.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Jerarquia;

@Repository
public interface JerarquiaRepository extends JpaRepository<Jerarquia,Long> {
	@Query("SELECT je FROM Jerarquia je WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 =:idCabecera AND je.codigoNivel2=:idModalidad AND je.estadoRegistro = '1'")
	public List<Jerarquia> findAllModalidad(@Param("idCabecera")Long idCabecera, @Param("idModalidad")Long idModalidad);
	
	@Query("SELECT DISTINCT je.codigoNivel2 FROM Jerarquia je WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 =:idCabecera AND je.estadoRegistro = '1'")
	public List<Long> findModalidad(@Param("idCabecera")Long idCabecera);
	
//	@Query("SELECT je FROM Jerarquia je WHERE 1 = 1 \r\n" + 
//			"AND je.codigoNivel1 =:idCabecera AND je.codigoNivel2 =:idModalidad AND je.estadoRegistro = '1'")
//	public List<Jerarquia> findModalidadById(@Param("idCabecera")Long idCabecera,@Param("idModalidad")Long idModalidad);
	
	@Query("SELECT DISTINCT je.codigoNivel2 FROM Jerarquia je WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 =:idCabecera AND je.codigoNivel2 =:idModalidad AND je.estadoRegistro = '1'")
	public List<Long> findModalidadById(@Param("idCabecera")Long idCabecera,@Param("idModalidad")Long idModalidad);
	
	@Query("SELECT DISTINCT je.codigoNivel3 FROM Jerarquia je WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 =:idCabecera AND je.codigoNivel2 =:idModalidad AND je.codigoNivel3 =:idTipo AND je.estadoRegistro = '1'")
	public Optional<Long> findTipoById(@Param("idCabecera")Long idCabecera,@Param("idModalidad")Long idModalidad,@Param("idTipo")Long idTipo);
	
	@Query("SELECT je FROM Jerarquia je WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 =:idCabecera AND je.estadoRegistro =:idEstado")
	public List<Jerarquia> findEstadoById(@Param("idCabecera")Long idCabecera,@Param("idEstado")String idEstado);	
	
	@Query("SELECT DISTINCT je.codigoNivel1 FROM Jerarquia je  WHERE je.estadoRegistro = '1'")
	public List<Long> findAllJerarquia();
	
	@Query("SELECT DISTINCT je FROM Jerarquia je where je.jerarquiaId=:idJerarquia ")
	public List<Jerarquia> findAllJerarquias(@Param("idJerarquia")Long idJerarquia);
	
	@Query("SELECT DISTINCT je.estadoRegistro FROM Jerarquia je WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 =:idCabecera AND je.codigoNivel2=:idModalidad AND je.estadoRegistro = '1'")
	public List<String> findEstado(@Param("idCabecera")Long idCabecera, @Param("idModalidad")Long idModalidad);
	
	@Query("SELECT je FROM Jerarquia je WHERE 1 = 1 \r\n" + 
			"AND je.codigoNivel1 =:regimenId AND je.codigoNivel2 =:modalidadId AND je.codigoNivel3 =:tipoId AND je.estadoRegistro = '1' ")
	public List<Jerarquia> getJerarquia(@Param("regimenId")Long regimenId, @Param("modalidadId")Long modalidadId, @Param("tipoId")Long tipoId);
	
}
