package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RespObtieneLista<T> {

	private Integer count;
	private Long total;
	private List<T> items;


	public RespObtieneLista(List<T> items) {
		this.items = items;
		this.count = items.size();
	}
	
	public RespObtieneLista() {
		
	}
}
