package pe.gob.servir.convocatoria.service;

import java.io.IOException;
import java.util.List;

import com.lowagie.text.DocumentException;

import pe.gob.servir.convocatoria.model.BonificacionDetalle;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.request.dto.BonificacionPdfDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespUploadFile;

public interface ReporteBase {
	
	/*String parseThymeleafTemplate(Long id);
	
	byte[] generatePdfFromHtml(Long id) throws DocumentException, IOException;
	
	List<BonificacionPdfDTO> findBonificaciones(List<BonificacionDetalle> listDetBonificacion,List<MaestraDetalle> listaMaeDetalle);
	*/
	byte[] generatePdfFromHtmlBase(Long id) throws DocumentException, IOException;
	
	byte[] generatePdfFromHtmlBaseConvocatoria(Long id) throws DocumentException, IOException;
	
	String parseThymeleafTemplateBase(Long id);
	
	String parseThymeleafTemplateBaseConvocatoria(Long id);
	
	//byte[] generatePdfFromHtmlBaseInforme(Long id) throws DocumentException, IOException;
	
}
