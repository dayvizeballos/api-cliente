package pe.gob.servir.convocatoria.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.model.PerfilGrupo;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.service.PerfilGrupoService;


@RestController
@Tag(name = "Perfilgrupo", description = "")
public class PerfilGrupoController {

    @Autowired
    private PerfilGrupoService perfilGrupoService;

    @Operation(summary = Constantes.SUM_OBT_LIST+"perfilGrupo", description = Constantes.SUM_OBT_LIST+"perfilGrupo", tags = { "" },
            security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})

    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })

    @GetMapping(path = { Constantes.BASE_ENDPOINT+"/perfilgrupo"},
            produces = { MediaType.APPLICATION_JSON_VALUE })

    public ResponseEntity<RespBase<RespObtieneLista<PerfilGrupo>>> perfilGrupos(@PathVariable String access,
                                                                                @RequestParam(value = "id", required = false) Long id){

        RespBase<RespObtieneLista<PerfilGrupo>> response = perfilGrupoService.findAllPerfilGrupo(id);
        return ResponseEntity.ok(response);
    }
    
    @Operation(summary = Constantes.SUM_OBT_LIST+" perfilGrupo", description = Constantes.SUM_OBT_LIST+"perfilGrupo", tags = { "" },
            security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})

    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })

    @GetMapping(path = { Constantes.BASE_ENDPOINT+"/perfilgrupo/listar"},
            produces = { MediaType.APPLICATION_JSON_VALUE })

    public ResponseEntity<RespBase<RespObtieneLista<PerfilGrupo>>> listaPerfilGrupos(@PathVariable String access){

        RespBase<RespObtieneLista<PerfilGrupo>> response = perfilGrupoService.listAllPerfilGrupo();
        return ResponseEntity.ok(response);
    }
}
