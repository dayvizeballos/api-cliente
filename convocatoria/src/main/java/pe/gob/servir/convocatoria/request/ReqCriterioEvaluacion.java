package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@Getter
@Setter
public class ReqCriterioEvaluacion {

	@NotNull(message = Constantes.CAMPO + " entidadId " + Constantes.ES_OBLIGATORIO)
	private Long entidadId;
	
	@NotNull(message = Constantes.CAMPO + " tipoInfoId " + Constantes.ES_OBLIGATORIO)
	private Long tipoInfoId;
	
	private Long informeDetalleId;
	
	@NotNull(message = Constantes.CAMPO + " estadoRegistro " + Constantes.ES_OBLIGATORIO)
	private boolean estadoRegistro;
	
}
