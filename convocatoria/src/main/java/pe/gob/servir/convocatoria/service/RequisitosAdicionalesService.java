package pe.gob.servir.convocatoria.service;

import java.util.List;

import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespConfigOtrosRequisitos;
import pe.gob.servir.convocatoria.response.dto.RespObtenerRequisitosAdicionalesDTO;

public interface RequisitosAdicionalesService {

    RespBase<RespObtenerRequisitosAdicionalesDTO> listarRequisitosAdicionalesByPerfilId (Long perfilId, Long baseId);
    RespBase<List<RespConfigOtrosRequisitos>> listarConfiOtrosRequisitosByPerfil(Long perfilId) ;
}
