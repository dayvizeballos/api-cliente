package pe.gob.servir.convocatoria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.BonificacionDetalle;

@Repository
public interface BonificacionDetalleRepository extends JpaRepository<BonificacionDetalle,Long>{

	@Query("SELECT det FROM BonificacionDetalle det WHERE 1 = 1 \r\n" + 
			"AND det.bonificacion.bonificacionId =:idInformeDetalle AND det.estadoRegistro = '1'")
	public List<BonificacionDetalle> findAllByBonificacionId(@Param("idInformeDetalle")Long idInformeDetalle);
	
	
}
