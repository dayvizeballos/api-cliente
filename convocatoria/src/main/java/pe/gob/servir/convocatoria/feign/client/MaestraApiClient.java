package pe.gob.servir.convocatoria.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pe.gob.servir.convocatoria.common.ConstantesApiMaestra;
import pe.gob.servir.convocatoria.model.Pais;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.ApiEnvioCorreoRequestDTO;
import pe.gob.servir.convocatoria.request.dto.ApiUploadFile;
import pe.gob.servir.convocatoria.request.dto.ParametrosDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespUploadFile;


@FeignClient(name = "maestraApi", url = "${maestra.private.base.url}")
public interface MaestraApiClient {

    @RequestMapping(method = RequestMethod.GET, value = "/v1/tiposparametro/{tipoParametro}/parametros",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    RespBase<RespObtieneLista<ParametrosDTO>> rutaFileServerEntidad(
            @PathVariable String tipoParametro,
            @RequestParam String parametros);


    @RequestMapping(method = RequestMethod.POST, value = ConstantesApiMaestra.ENDPOINT_ENVIAR_CORREO, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    RespBase<Object> enviarCorreoNotificacion(ReqBase<ApiEnvioCorreoRequestDTO> request);

    @RequestMapping(method = RequestMethod.POST, value = ConstantesApiMaestra.ENDPOINT_SUBIR_ARCHIVO, produces = {
            MediaType.APPLICATION_JSON_VALUE})
    RespBase<RespUploadFile> uploadFile(RespBase<ApiUploadFile> request);

    @RequestMapping(method = RequestMethod.GET, value = "/v1/pais/query",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    RespBase<RespObtieneLista<Pais>> getPais(
            @RequestParam Integer paisId , @RequestParam String nombrePais);
}
