package pe.gob.servir.convocatoria.response.dto;

import java.util.List;

import lombok.Data;
import pe.gob.servir.convocatoria.model.Perfil;

@Data
public class RespPerfilDTO {
	
	private Long perfilId;
	private String desPerfil;
	
	public RespPerfilDTO(Long perfilId, List<Perfil> lstPerfiles) {
		this.perfilId = perfilId;
		this.desPerfil = obtenerDescripcion(perfilId, lstPerfiles);
	}
	
	public String obtenerDescripcion(Long perfilId, List<Perfil> lista ) {
		for(Perfil tmp: lista) {
			if(tmp.getPerfilId().equals(perfilId)) {
				return tmp.getNombrePuesto();
			}
		}
		return "";
	}
}
