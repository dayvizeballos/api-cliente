package pe.gob.servir.convocatoria.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.*;
import pe.gob.servir.convocatoria.repository.BasePerfilRepository;
import pe.gob.servir.convocatoria.repository.DeclaracionJuradaRepository;
import pe.gob.servir.convocatoria.repository.TblConfPerfDeclaracionRepository;
import pe.gob.servir.convocatoria.request.dto.DeclaracionJuradaDTO;
import pe.gob.servir.convocatoria.request.dto.RequisitoGeneralDTO;
import pe.gob.servir.convocatoria.response.DeclaracionJuradaEspecificaDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.dto.RespObtenerDDJJByPerfilIdDTO;
import pe.gob.servir.convocatoria.service.DeclaracionJuaradaService;
import pe.gob.servir.convocatoria.service.RequisitoGeneralService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class DeclaracionJuaradaServiceImpl implements DeclaracionJuaradaService {

    @Autowired
    private DeclaracionJuradaRepository declaracionJuradaRepository;

    @Autowired
    private BasePerfilRepository basePerfilRepository;

    @Autowired
    private RequisitoGeneralService requisitoGeneralService;

    @Autowired
    private TblConfPerfDeclaracionRepository tblConfPerfDeclaracionRepository;

    @Override
    public RespBase<RespObtieneLista> listarDeclaraJuaradaEspecifica(Long baseId) {

        RespObtieneLista response = new RespObtieneLista();

        DeclaracionJurada filtro = new DeclaracionJurada();
        filtro.setBase(new Base(baseId));
        filtro.setIsServir(Constantes.ISSERVIR_DECLARA_JURADA_ESPECIF);

        Example<DeclaracionJurada> example = Example.of(filtro);

        List<DeclaracionJurada> listarDeclaraciones = this.declaracionJuradaRepository.findAll(example);

        List<DeclaracionJuradaEspecificaDTO> items = new ArrayList<>();

        listarDeclaraciones.forEach( item -> { items.add(new DeclaracionJuradaEspecificaDTO(item)); } );

        response.setItems(items);
        response.setCount(items.size());

        return new RespBase<RespObtieneLista>().ok(response);
    }

    @Override
    public RespBase<RespObtenerDDJJByPerfilIdDTO> listarDeclaraJuradaByPerfilId(Long idPerfil, Long baseId) {

        RespBase<RespObtenerDDJJByPerfilIdDTO> response = new RespBase<>();

        RespObtenerDDJJByPerfilIdDTO dto;

        List<RespObtenerDDJJByPerfilIdDTO.DeclaraJuradaByPerfilIdDTO> listDeclaracionJurada;

        RespBase<RequisitoGeneralDTO> listaReqGen = this.requisitoGeneralService.getRequisitoGeneral(baseId);
        boolean validar = Objects.nonNull(listaReqGen) && listaReqGen.getStatus().getSuccess() && !listaReqGen.getPayload().getDeclaracionJuradaDTOList().isEmpty();
        if (validar) {
            dto = new RespObtenerDDJJByPerfilIdDTO();
            listDeclaracionJurada = new ArrayList<>();

            for (DeclaracionJuradaDTO requisGenDto : listaReqGen.getPayload().getDeclaracionJuradaDTOList()) {
                List<TblConfPerfDeclaracion> tblConfPerfDecla = this.obtenerListTblConfPerfDeclara(idPerfil, baseId, requisGenDto);
                RespObtenerDDJJByPerfilIdDTO.DeclaraJuradaByPerfilIdDTO declaJurDto = new RespObtenerDDJJByPerfilIdDTO.DeclaraJuradaByPerfilIdDTO();

                if (!tblConfPerfDecla.isEmpty()) {
                    TblConfPerfDeclaracion tblConfPDecla = tblConfPerfDecla.get(0);
                    declaJurDto.setConfPerfDeclaracionId(tblConfPDecla.getConfPerfDeclaracionId());
                    declaJurDto.setRequisitoMinimo(tblConfPDecla.getRequisitoMinimo());
                    declaJurDto.setFlagDDJJ(tblConfPDecla.getFlagDj());
                }
                declaJurDto.setDeclaracionId(requisGenDto.getDeclaracionId());
                declaJurDto.setTipoId(requisGenDto.getTipoId());
                declaJurDto.setOrden(requisGenDto.getOrden());
                declaJurDto.setDescripcion(requisGenDto.getDescripcion());
                declaJurDto.setEstado(requisGenDto.getEstado());
                declaJurDto.setIsServir(requisGenDto.getIsServir());
                declaJurDto.setBaseId(baseId);
                listDeclaracionJurada.add(declaJurDto);
            }
            dto.setIdPerfil(idPerfil);
            dto.setListDeclaracionJurada(listDeclaracionJurada);

        } else {
            return ParametrosUtil.setearResponse(response, Boolean.FALSE, "No hay data.");
        }


        return new RespBase<RespObtenerDDJJByPerfilIdDTO>().ok(dto);
    }

    private List<TblConfPerfDeclaracion> obtenerListTblConfPerfDeclara(Long idPerfil, Long baseId, DeclaracionJuradaDTO declaJur) {
        TblConfPerfDeclaracion filtro = new TblConfPerfDeclaracion();
        filtro.setPerfil(new Perfil(idPerfil));
        filtro.setBaseId(baseId);
        filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
        filtro.setDeclaracionId(declaJur.getDeclaracionId());
        filtro.setTipoId(declaJur.getTipoId());
        filtro.setDescripcion(declaJur.getDescripcion());
        Example<TblConfPerfDeclaracion> example = Example.of(filtro);

        return this.tblConfPerfDeclaracionRepository.findAll(example);
    }

}
