package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.annotation.ColumnaTablaHtml;

@JGlobalMap
@Getter
@Setter
public class DetalleEvaluacionEntidadDTO {

	private Long evaluacionEntidadId;
	@ColumnaTablaHtml(orden = 1,nombre="EVALUACIONES")
	private String descripcion;
	@ColumnaTablaHtml(orden = 2,nombre="PORCENTAJE",concatValue="%")
	private Integer peso;
	@ColumnaTablaHtml(orden = 3,nombre="PUNT. MIN.")
	private Integer puntajeMin;
	@ColumnaTablaHtml(orden = 4,nombre="PUNT. MAX.")
	private Integer puntajeMax;
	
}
