package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorreoDTO {
	private Long baseId;
    private String codigoConvocatoria;
    private String fechaCreacion;
    private String fechaPublicacion;
    private String gestor;
    private String coordinador;
    private String regimen;
    private Long vacantes;
    private String denominacion;
    private Long entidadId;
    private String rnum;
    private Long regimenId;
    private Long etapaId;
    private String url;

    private Long convocatoriaId;
    private String correlativo;
    private String nombreEntidad;
    private String etapa;
    private String etapaProxima;
    private String fechaEtapaProxima;
}

