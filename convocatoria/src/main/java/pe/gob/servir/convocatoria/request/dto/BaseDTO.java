package pe.gob.servir.convocatoria.request.dto;


import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;


@JGlobalMap
@Getter
@Setter
public class BaseDTO {
    private Long baseId;
    private Long entidadId;
    private String codigoConvocatoria;
    private Long rolId;
    private Long etapaId;
    private Long regimenId;
    private Long modalidadId;
    private Long tipoId;
    private Long practicaId;
    private Long gestorId;
    private Long especialistaId;
    private String estadoRegistro;
    private String condicionEtapa1;
    private String condicionEtapa2;
    private String condicionEtapa3;
    private String condicionEtapa4;
    private String condicionEtapa5;
    private String condicionEtapa6;
    private String comentarioEtapa1;
    private String comentarioEtapa2;
    private String comentarioEtapa3;
    private String comentarioEtapa4;
    private String comentarioEtapa5;
    private String comentarioEtapa6;
    private String anio;
    private Integer correlativo;
}
