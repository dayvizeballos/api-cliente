package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.UnidadOrganica;

@Getter
@Setter
public class RespUnidadOrganica {
	
	private List<UnidadOrganica> listaUnidadOrganica;

}
