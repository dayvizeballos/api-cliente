package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.TblConfPerfExpeLaboral;

@Repository
public interface TblConfPerfExpeLaboralRepository extends JpaRepository<TblConfPerfExpeLaboral, Long> {

}