package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_carrera_profesional", schema = "sch_convocatoria")
@Getter
@Setter
public class CarreraProfesional extends AuditEntity implements AuditableEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_carrera_profesional")
	@SequenceGenerator(name = "seq_carrera_profesional", sequenceName = "seq_carrera_profesional", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@Column(name = "mae_detalle_id")
	private Long idDetalle;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "orden")
	private Integer orden;

	/*
	@OneToMany(cascade= CascadeType.ALL , mappedBy = "carreraProfesional")
	@Column(nullable = true)
	@JsonManagedReference
	private List<CarreraFormacionAcademica> carreraFormacionAcademicaList;

	 */
	
	public CarreraProfesional() {

	}
}
