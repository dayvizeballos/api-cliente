package pe.gob.servir.convocatoria.request.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class BaseHorarioDTO {
	
	private Long baseHorarioId;
	@Size(max = 5, message = Constantes.CAMPO + " horaIni " + Constantes.ES_INVALIDO + ", máximo 5 ")
	private String horaIni;
	@Size(max = 5, message = Constantes.CAMPO + " horaIni " + Constantes.ES_INVALIDO + ", máximo 5 ")
	private String horaFin;
	@NotNull(message = Constantes.CAMPO + " frecuenciaId " + Constantes.ES_OBLIGATORIO)	
	private Long frecuenciaId;
	private String estado;
	

}
