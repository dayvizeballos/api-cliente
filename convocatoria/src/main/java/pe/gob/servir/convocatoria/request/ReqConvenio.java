package pe.gob.servir.convocatoria.request;

import java.util.Date;

import javax.persistence.Column;

import lombok.Data;

@Data
public class ReqConvenio {
	
	private String  direccionLabores;
    private Date fechaIniPractica;
    private Date fechaFinPractica;
    private String horaIniPractica;
    private String horaFinPractica;
    
    private String puestoResponsableOrh;
    private String responsableOrh;
    private Long tipoDocResponsable;
    private String nroDocResponsable;

    private String puestoRepreUni;
    private String nombRepreUni;
    private Long tipoDocRepreUni;
    private String nroDocRepreUni;
    
    private String direccionCentroEstudios;
    private String rucCentroEstudios;
    
    private Long estadoId;
  

}
