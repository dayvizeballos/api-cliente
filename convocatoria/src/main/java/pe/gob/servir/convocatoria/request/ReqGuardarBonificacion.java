package pe.gob.servir.convocatoria.request;

import javax.validation.Valid;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;

@Getter
@Setter
public class ReqGuardarBonificacion {
	
	@Valid
	BonificacionDTO bonificacionDTO;

}