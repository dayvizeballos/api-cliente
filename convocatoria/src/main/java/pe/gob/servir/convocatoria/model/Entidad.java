package pe.gob.servir.convocatoria.model;
 

import lombok.Getter;
import lombok.Setter; 

@Getter
@Setter
public class Entidad extends AuditEntity{
	
	
	private Long entidadId;
	private Long sectorId;
	private String sector;
	private Long nivelGobiernoId;
	private String nivelGobierno;
	private String descripcionEntidad;
	private String sigla;
	private Long personaId;
	private String razonSocial;
	private String nombreComercial;
	private Long tipoDocumento;
	private String numeroDocumento;
	private Long direccionId;
	private String direccion;
	private Long correoId;
	private String correo;
	private Long telefonoId;
	private String telefono;
	private String anexo;
	private String logo;
	private String urlWeb;
	private Long distritoId;
	private String distrito;
	private Long provinciaId;
	private String provincia;
	private Long departamentoId;
	private String departamento;
	private String flag;
	private Long direccionFiscalId;
	private String direccionFiscal;
	
 
	
	
 
	
}