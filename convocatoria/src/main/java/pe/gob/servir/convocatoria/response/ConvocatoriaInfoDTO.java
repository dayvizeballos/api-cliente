package pe.gob.servir.convocatoria.response;

import java.util.Date;
import java.util.List;

import lombok.Data;
import pe.gob.servir.convocatoria.model.Convocatoria;

@Data
public class ConvocatoriaInfoDTO {
	
	private Long baseId;
	private String codigoConvocatoria;
	private Long idEtapa;
	private String desEtapa;
	private Long idEstado;
	private String desEstado;
	private String fechaInicioConvocatoria;
	private String fechaFinConvocatoria;
	private List<CronogramaConvocatoriaDTO> listaCronograma;
	
	public ConvocatoriaInfoDTO(Convocatoria item) {
		this.baseId = item.getBase().getBaseId();
		this.codigoConvocatoria = item.getCodigoConvocatoria();
		this.idEtapa = item.getEtapa()!= null ?item.getEtapa().getMaeDetalleId() : null;
		this.desEtapa = item.getEtapa()!= null ?item.getEtapa().getDescripcion() : null;
		this.idEstado = (item.getEstadoConvocatoria() != null)?item.getEstadoConvocatoria().getMaeDetalleId() : null;
		this.desEstado = (item.getEstadoConvocatoria() != null)? item.getEstadoConvocatoria().getDescripcion() : null;
		
	}
}
