package pe.gob.servir.convocatoria.loader;

public class ConstantesExcel {
    public static final String EXCEL_VERSION_2003 = ".xls";
    public static final String EXCEL_VERSION_2007 = ".xlsx";
    public static final String EXCEL_VERSION_XLSM = ".xlsm";

    public static final String NOMBRE_PLANTILLA_30057 = "PLANTILLA_30057";
    public static final String REGIMEN_30057 = "30057";
    public static final String NOMBRE_PLANTILLA_1401 = "PLANTILLA_1401";
    public static final String REGIMEN_1401 = "1401";

    /**
     * DATOS DEL EXCEL TBL_PERFIL_FUNCION
     */
    public static final String FUNCION = "FUNCION";
    public static final String ACTIVIDAD = "ACTIVIDAD";
    public static final String CONDICION_ATIPICA = "CONDICION_ATIPICA";
    public static final String PERIODICIDAD_CONDICION_ATIPICA = "PERIODICIDAD_CONDICION_ATIPICA";
    public static final String SUSTENTO_CONDICION_ATIPICA = "SUSTENTO_CONDICION_ATIPICA";
    public static final String COORDINACION_INTERNA = "COORDINACION_INTERNA";
    public static final String COORDINACION_EXTERNA = "COORDINACION_EXTERNA";
    public static final String FUNCIONARIO_PUBLICO = "FUNCIONARIO_PUBLICO";
    public static final String DIRECTIVO_PUBLICO = "DIRECTIVO_PUBLICO";
    public static final String SERVIDORES_CARRERA = "SERVIDORES_CARRERA";
    public static final String ACTIVIDADES_COMPLEMENTARIAS = "ACTIVIDADES_COMPLEMENTARIAS";

    public static final String FUNCION1 = "FUNCION1";
    public static final String FUNCION2 = "FUNCION2";
    public static final String FUNCION3 = "FUNCION3";
    public static final String FUNCION4 = "FUNCION4";
    public static final String FUNCION5 = "FUNCION5";


    public static final String SC_DP = "DP";
    public static final String SC_CA = "CA";
    public static final String SC_CO = "CO";
    public static final String TBL_PER_PER_APL = "TBL_PER_PER_APL";
    /**
     * DATOS DEL EXCEL TAB PERFIL
     */
    public static final String ORGANO = "ORGANO";
    public static final String UNIDAD_ORGANICA = "UNIDAD_ORGANICA";
    public static final String NIVEL_ORGANIZACIONAL = "NIVEL_ORGANIZACIONAL";
    public static final String GRUPO_SERVIDORES_CIVILES = "GRUPO_SERVIDORES_CIVILES";
    public static final String FAMILIA_PUESTOS = "FAMILIA_PUESTOS";
    public static final String ROL = "ROL";
    public static final String NIVEL_CATEGORIA = "NIVEL_CATEGORIA";
    public static final String SUB_NIVEL_SUB_CATEGORIA = "SUB_NIVEL_SUB_CATEGORIA";
    public static final String NOMBRE_PUESTO = "NOMBRE_PUESTO";
    public static final String NRO_POSICIONES = "NRO_POSICIONES";
    public static final String DEPENDENCIA_JERARQUICA_LINEAL = "DEPENDENCIA_JERARQUICA_LINEAL";
    public static final String DEPENDENCIA_FUNCIONAL = "DEPENDENCIA_FUNCIONAL";
    public static final String UNIDAD_FUNCIONAL = "UNIDAD_FUNCIONAL";
    public static final String PUESTO_TIPO = "PUESTO_TIPO";
    public static final String GRUPO_SERVIDORES_REPORTA = "GRUPO_SERVIDORES_REPORTA";
    public static final String NRO_POSICIONES_CARGO = "NRO_POSICIONES_CARGO";
    public static final String MISION_PUESTO = "MISION_PUESTO";
    public static final String CODIGO_POSICION = "CODIGO_POSICION";
    public static final String CODIGO_PUESTO = "CODIGO_PUESTO";
    public static final String NIVEL_EDUCATIVO = "NIVEL_EDUCATIVO";
    public static final String TIPO_DE_PRACTICAS = "TIPO_DE_PRACTICAS";

    /**
     * DATOS DEL EXCEL TAB FORMACION ACADEMICA
     */
    public static final String PRIMARIA = "PRIMARIA";
    public static final String SITUACION_NIVEL_PRIMARIA = "SITUACION_NIVEL_PRIMARIA";
    public static final String SECUNDARIA = "SECUNDARIA";
    public static final String SITUACION_NIVEL_SECUNDARIA = "SITUACION_NIVEL_SECUNDARIA";
    public static final String TECNICA_BASICA = "TECNICA_BASICA";
    public static final String SITUACION_TECNICA_BASICA = "SITUACION_TECNICA_BASICA";
    public static final String GRAD_EDUC_TECNICA_BASICA_EGRESADO = "SGRAD_EDUC_TECNICA_BASICA_EGRESADO";
    public static final String ESTUDIOS_REQUERIDOS_TBASICA_EGRE = "ESTUDIOS_REQUERIDOS_TBASICA_EGRE";
    public static final String GRAD_EDUC_TEC_BASICA_TITU_LICENCIATURA = "GRAD_EDUC_TEC_BASICA_TITU_LICENCIATURA";
    public static final String ESTU_REQ_TEC_BASICA_TITU = "ESTU_REQ_TEC_BASICA_TITU";
    public static final String TECNICA_SUPERIOR = "TECNICA_SUPERIOR";
    public static final String SITUACION_TECNICA_SUPERIOR = "SITUACION_TECNICA_SUPERIOR";
    public static final String GRAD_EDUC_TEC_SUP_EGRESADO = "GRAD_EDUC_TEC_SUP_EGRESADO";
    public static final String ESTU_REQ_TEC_SUP_EGRESADO = "ESTU_REQ_TEC_SUP_EGRESADO";
    public static final String GRAD_EDUC_TEC_SUP_TITU_LIC = "GRAD_EDUC_TEC_SUP_TITU_LIC";
    public static final String EST_REQ_TEC_SUP_TITU = "EST_REQ_TEC_SUP_TITU";
    public static final String UNIVERSITARIA = "UNIVERSITARIA";
    public static final String SITUACION_UNIVERSITARIA = "SITUACION_UNIVERSITARIA";
    public static final String GRAD_EDUC_SUPE_EGRESADO = "GRAD_EDUC_SUPE_EGRESADO";
    public static final String EST_REQ_EDU_SUPERIOR = "EST_REQ_EDU_SUPERIOR";
    public static final String GRAD_EDUC_SUP_BACHILLER = "GRAD_EDUC_SUP_BACHILLER";
    public static final String EST_REQ_SUP_BACHILLER = "EST_REQ_SUP_BACHILLER";
    public static final String GRAD_EDUC_SUP_TIT_LICENCIATURA = "GRAD_EDUC_SUP_TIT_LICENCIATURA";
    public static final String EST_REQ_SUP_TITU_LICENCIATURA = "EST_REQ_SUP_TITU_LICENCIATURA";
    public static final String GRAD_EDUC_SUP_MAESTRIA = "GRAD_EDUC_SUP_MAESTRIA";
    public static final String EST_REQ_MAESTRIA = "EST_REQ_MAESTRIA";
    public static final String GRAD_EDUC_SUP_DOCTORADO = "GRAD_EDUC_SUP_DOCTORADO";
    public static final String EST_REQ_DOCTORADO = "EST_REQ_DOCTORADO";
    public static final String MAESTRIA_SITUACION = "MAESTRIA_SITUACION";
    public static final String MAESTRIA_DESCRIPCION = "MAESTRIA_DESCRIPCION";
    public static final String DOCTORADO_SITUACION = "DOCTORADO_SITUACION";
    public static final String DESCRIPCION_DOCTORADO = "DESCRIPCION_DOCTORADO";
    public static final String COLEGIATURA = "COLEGIATURA";
    public static final String HABILITACION_PROFESIONAL = "HABILITACION_PROFESIONAL";
    public static final String CONOCIMIENTOS_TECNICOS = "CONOCIMIENTOS_TECNICOS";
    public static final String CONOCIMIENTOS_BASICOS = "CONOCIMIENTOS_BASICOS";
    public static final String CURSOS = "CURSOS";
    public static final String HORAS_CURSOS = "HORAS_CURSOS";
    public static final String PROGRAMAS = "PROGRAMAS";
    public static final String HORAS_PROGRAMAS = "HORAS_PROGRAMAS";
    public static final String CONOCIMIENTOS_OFIMATICA = "CONOCIMIENTOS_OFIMATICA";
    public static final String HORAS_CONO_OFIMATICA = "HORAS_CONO_OFIMATICA";
    public static final String IDIOMAS = "IDIOMAS";
    public static final String NIVEL_IDIOMA = "NIVEL_IDIOMA";
    public static final String OBSERVACION = "OBSERVACION";


    /**
     * DATOS EXCEL SITUACION ACADEMICA
     */
    
    public static final String AÑOS_EXPERIENCIA_GENERAL="ANIOS_EXP_GENERAL";
    public static final String MESES_EXPERIENCIA_GENERAL="MESES_EXP_GENERAL";
    public static final String AÑOS_EXPERIENCIA_ESPECÍFICA="ANIOS_EXP_ESPECIFICA";
    public static final String MESES_EXPERIENCIA_ESPECÍFICA="MESES_EXP_ESPECIFICA";
    public static final String AÑOS_EXPERIENCIA_SECTOR_PÚBLICO="ANIOS_EXP_SECT_PUBLICO";
    public static final String MESES_EXPERIENCIA_SECTOR_PÚBLICO="ANIOS_EXP_SECT_PUBLICO";
    public static final String NIVEL_MÍNIMO_PUESTO="NIVEL_MINIMO_PUESTO";
    public static final String OTROS_ASPECTOS_COMPLEMENTARIOS="OTROS_ASPECTOS_COMPL";
    public static final String HABILIDADES_COMPETENCIAS = "HABILIDADES_COMPETENCIAS";
    public static final String TIPO_REQUISITOS_ADICIONALES="TIPO_REQUISITOS_ADICIONALES";
    public static final String DESCRIPCION_REQUISITOS="DESCRIPCION_REQUISITOS";
    public static final String OBSERVACIONES_PLANTILLA="OBSERVACIONES_PLANTILLA";
    public static final String OBS_REQUISITOS = "El campo no tiene la misma cantidad de valor de "+TIPO_REQUISITOS_ADICIONALES;
    public static final String CAMPO_ANIO= "ANIO";
    public static final String CAMPO_MES= "MES";
    public static final int MAX_MES= 11;
    public static final int MAX_AÑO= 12;
    public static final int MINIMO_AÑO_MES= 0;



    public static final String EGRESADO = "EGRESADO(A)";
    public static final String BACHILLER = "BACHILLER";
    public static final String TITULO_LICENCIATURA = "TÍTULO O LICENCIATURA";
    public static final String MAESTRIA = "MAESTRÍA";
    public static final String DOCTORADO = "DOCTORADO";
    public static final String NO_APLICA = "NO APLICA";

    public static final String MARCA = "X";
    public static final String OK = "OK";
    public static final String OBS_CAMPO_NULO = "El campo es obligatorio, no puede ser nulo o vacio";
    public static final String OBS_CAMPO_TEXTO = "El campo es de tipo texto";
    public static final String OBS_CAMPO_NUMERICO = "El campo es de tipo numerico";
    public static final String OBS_CAMPO_LONGITUD_TEXTO = "El campo excede su longitud, Valor Max. ";
    public static final String OBS_CAMPO_MARCA = "El campo permite valor X o vacio";
    public static final String CAMPO_TEXTO = "TEXTO";
    public static final String CAMPO_NUMERICO= "NUMBER";
    public static final String OBLIGATORIO = "1";
    public static final String NO_OBLIGATORIO = "0";
    public static final String ERROR = "ERROR";
    public static final String OBS_NUMERICO_COMA = "El campo solo permite numeros o el caracter ',' ";
    public static final String OBS_TAMANO_CAMPO_COMA = "El campo no tiene la misma cantidad de valor(es) de ";
    
    public static final String COD_REGIMEN = "TBL_REGIMEN";
    public static final String DEPENDIENTE = "2";
    public static final String OBS_CAMPO_NULO_DEPENDIENTE = "El campo es obligatorio si seleccionó la columna ";
    public static final String OBS_CAMPO_MARCA_DEPENDIENTE = "Debe ingresar el valor X en el campo ";
    public static final String OBS_CAMPO_SITUACION_DEPENDIENTE = "Debe seleccionar un valor en el campo ";

}
