package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.request.dto.ApiParametro;

@Getter
@Setter
@ToString
public class RespObtieneListaParametro {
	
	private Integer count;
	private List<ApiParametro> items;

}
