package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Organigrama {
	
	private Long organigramaId;

	private Long entidadId;

	private Long areaId;
	
	private Long paisId;
	
	private Long sedeId;

	private Long padreOrganigramaId;

	private String puesto;

	private Integer orden;

	private Long nivel;

	private Long tipoOrganoUoId;

	private Long naturalezaOrgano;

	private Long nivelGobiernoId;

	private String descripcion;

	private String descripcionCorta;

	private String sigla;

	private Long personaResponsableId;

	private Long telefonoId;

	private Long correoId;

	private String descripcionTipoOrg;
	private String estado;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String telefono;
	private String correo;
	private Integer tipoDocumento;
	private String nroDocumento;
	private String descripOrganoPadre;
	
	private String nombrePais;
	private String desNivel;
	private String desNaturaleza;
	private String estadoRegistro;
	private String destipoDocumento;

}
