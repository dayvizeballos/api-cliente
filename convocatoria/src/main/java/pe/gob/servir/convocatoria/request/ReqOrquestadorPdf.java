package pe.gob.servir.convocatoria.request;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class ReqOrquestadorPdf {
	@NotNull(message = Constantes.CAMPO + " idInforme " + Constantes.ES_OBLIGATORIO)
	@Valid
	private Long idInforme;
	@NotNull(message = Constantes.CAMPO + " idTipoInformacion " + Constantes.ES_OBLIGATORIO)
	@Valid
	private Long idTipoInformacion;
	@NotNull(message = Constantes.CAMPO + " baseId " + Constantes.ES_OBLIGATORIO)
	@Valid
	private Long baseId;

}
