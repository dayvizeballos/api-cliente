package pe.gob.servir.convocatoria.response.dto;


import lombok.Data;

import java.util.Date;

@Data
public class ReporteConvocatoriaDTO {
    private Long reporteConvocatoriaId;
    private Long entidadId;
    private String nombreEntidad;
    private Long baseId;
    private Long estadoBaseId;
    private String estadoBase;
    private String fechaCreacionBase;
    private Long convocatoriaId;
    private Long gestorId;
    private String nombreGestor;
    private Long coordinadorId;
    private String nombreCoordinador;
    private Long estado_convocatoriaId;
    private String estadoConvocatoria;
    private Long etapaConvocatoriaId;
    private String etapaConvocatoria;
    private Long regimenId;
    private String regimen;
    private Long modalidadId;
    private String modalidad;
    private Long tipoId;
    private String tipo;
    private String denominacionPuesto;
    private Integer nroVacantes;
    private String tipoPractica;
    private Long nroPostPorConvo;
    private Integer nroPostContratados;
    private String fechaPublicacion;
    private String codigoConvocatoria;
    private Date periodoIniEtapa;
    private Date periodoFinEtapa;
    private Integer departamentoId;
    private String descDepartamento;
    private String estadoRegistro;
    private String usuarioCreacion;
    private String fechaCreacion;
    private String usuarioModificacion;
    private String fechaModificacion;
    private Integer mes;
    private Integer anio;
    private String codEstadoBase;
    private String codEstadoConvocatoria;
    private String codEtapaConvocatoria;
    private String etapaQueue;
}
