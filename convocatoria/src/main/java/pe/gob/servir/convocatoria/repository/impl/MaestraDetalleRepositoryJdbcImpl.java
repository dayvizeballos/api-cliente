package pe.gob.servir.convocatoria.repository.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import pe.gob.servir.convocatoria.repository.MaestraDetalleRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.MaestraDetalleDto;
import pe.gob.servir.convocatoria.util.Util;

@Repository
public class MaestraDetalleRepositoryJdbcImpl implements MaestraDetalleRepositoryJdbc{

	private static final String SQL_CORRELATIVO_COD_DETALLE = "select COUNT(1) + 1 from sch_convocatoria.tbl_mae_detalle where mae_cabecera_id = :cabeceraId ";

	private static final String FIND_MAESTRA_DETALLE = "select mae_detalle_id as maestraDetalleId, mae_cabecera_id as maeCabeceraId , descripcion as descripcion, descripcion_corta as descripcionCorta ,\n" +
			"cod_prog as codProg, abreviatura as sigla from sch_convocatoria.tbl_mae_detalle tmd \n" +
			"where tmd.mae_detalle_id = :id";
	
    private static final String SQL_GET_ID_MAE_DETALLE = "select sch_convocatoria.fnc_get_id_mae_detalle(:codigoCabecera,:codProg)";
	/*private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;
    
    @Autowired
    public MaestraDetalleRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }
    */
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    
	@Override
	public String findCorrelativoDetalle(Long cabeceraId) {
		
		Map<String, Object> objectParam = new HashMap<>();
		if (!Util.isEmpty(cabeceraId)) objectParam.put("cabeceraId",cabeceraId);
		/**if (!Util.isEmpty(entidadId)) objectParam.put("entidadId",entidadId);
		String correlativo = this.namedParameterJdbcTemplate.queryForObject(SQL_CORRELATIVO_COD_DETALLE, objectParam, String.class);*/
		return this.namedParameterJdbcTemplate.queryForObject(SQL_CORRELATIVO_COD_DETALLE, objectParam, String.class);
		
	}

	@Override
	public Long findIdMaestraDetalle(String codigoCabecera, String codProg) {
		
		Long id = null;
		
		try {
			Map<String, Object> objectParam = new HashMap<>();
			if (!Util.isEmpty(codigoCabecera)) objectParam.put("codigoCabecera",codigoCabecera);
			if (!Util.isEmpty(codProg)) objectParam.put("codProg",codProg);
			id = this.namedParameterJdbcTemplate.queryForObject(SQL_GET_ID_MAE_DETALLE, objectParam, Long.class);
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
		}
		return id;
	}

	@Override
	public MaestraDetalleDto findMaestraDetalle(Long id) {
		List<MaestraDetalleDto> list = new ArrayList<>();
		try {
			StringBuilder sql = new StringBuilder();
			Map<String, Object> objectParam = new HashMap<>();
			sql.append(FIND_MAESTRA_DETALLE);
			if (!Util.isEmpty(id)) objectParam.put("id", id);

			list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(MaestraDetalleDto.class));
			if (!list.isEmpty()) return list.get(0);
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
			return new MaestraDetalleDto();
		}
		return null;
	}
}
