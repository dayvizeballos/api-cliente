package pe.gob.servir.convocatoria.request.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConvocatoriaDTO {

    private Long convocatoriaId;

    private String codigoConvocatoria;

    private Long baseId;

    private Long etapaId;

    private Long estadoConvocatoriaId;

    private String url;

    private Long correlativo;

    private Long anio;

    /*SE PUEDE AGRUPAR UN TIPO EN MAESTRA DETALLE  CABECERA: TIPO_JOB :DETALLE: 1:ind_archivo, 2:ind_notificacion*/

    private String indiArchivo;

    private String indiNotificacion;
}
