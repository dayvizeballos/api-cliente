package pe.gob.servir.convocatoria.repository;


import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import pe.gob.servir.convocatoria.model.BaseCronograma;
import pe.gob.servir.convocatoria.model.BaseCronogramaActividad;

import java.util.List;

public interface BaseCronogramaRepository extends JpaRepository<BaseCronograma, Integer> , JpaSpecificationExecutor<BaseCronograma> {

    @Query("select b from BaseCronograma b where baseId=:baseId order by etapaId asc, periodoini asc")
    public List<BaseCronograma> findAllCronogramas(@Param("baseId") Integer baseId);

    @Query("select to_char(MAX(b.periodoini), 'yyyy-mm-dd') from BaseCronograma b where baseId=:baseId and etapaId=:etapaId")
    public String findMaxFecha(@Param("baseId") Integer baseId, @Param("etapaId") Integer etapaId);

    @Query("select c from BaseCronograma c where baseId.baseId=:baseId order by c.etapaId.maeDetalleId asc")
    List<BaseCronograma> listBaseCronograma(@Param("baseId") Long baseId);

    @Query("select c.baseCronogramaActividadList from BaseCronograma c where c.basecronogramaId =:id ")
    List<BaseCronogramaActividad> listActividades(@Param("id") Integer id);
}
