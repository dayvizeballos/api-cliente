package pe.gob.servir.convocatoria.request.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BaseCronogramaDTO {

    private Long cronogramaId;
    private Long baseId;
    private Long etapaId;
    private String descripcion;
    private String responsable;
    private String codProg;
    private Date periodoIni;
    private Date periodoFin;
    private String estadoRegistro;
    private List<ActividadDTO> actividadDTOList;
}
