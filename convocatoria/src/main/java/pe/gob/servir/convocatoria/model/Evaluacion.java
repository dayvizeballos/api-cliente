package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_evaluacion", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
public class Evaluacion extends AuditEntity implements AuditableEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_evaluacion")
	@SequenceGenerator(name = "seq_evaluacion", sequenceName = "seq_evaluacion", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "evaluacion_id")
	private Long evaluacionId;
	
	@Column(name = "jerarquia_id")
	private Long jerarquiaId;
	
	@Column(name = "tipo_evaluacion_id")
	private Long tipoEvaluacionId;
	
	@Column(name = "orden")
	private Integer orden;
	
	@Column(name = "peso")
	private Integer peso;
	
	@Column(name = "puntaje_minimo")
	private Integer puntajeMinimo;
	
	@Column(name = "puntaje_maximo")
	private Integer puntajeMaximo;
	
	@Transient
	private String detalleEvaluacion;
	
	@Transient
	private String estado;
}
