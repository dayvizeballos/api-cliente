package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Data;
import pe.gob.servir.convocatoria.request.dto.BaseCronogramaDTO;


@Data
public class RespCreaBaseCronograma {

	private List<BaseCronogramaDTO> baseCronogramaDTOList;
}
