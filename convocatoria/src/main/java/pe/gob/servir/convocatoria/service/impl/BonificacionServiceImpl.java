package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.Bonificacion;
import pe.gob.servir.convocatoria.model.BonificacionDetalle;
import pe.gob.servir.convocatoria.model.InformeComplement;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.repository.BaseInformeComplRepository;
import pe.gob.servir.convocatoria.repository.BonificacionDetalleRepository;
import pe.gob.servir.convocatoria.repository.BonificacionRepository;
import pe.gob.servir.convocatoria.repository.BonificacionRepositoryJdbc;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepositoryJdbc;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarBonificacion;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.BonificacionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerBonificacionDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespBonificacionDTO;
import pe.gob.servir.convocatoria.response.RespBonificacionDetalleDTO;
import pe.gob.servir.convocatoria.response.RespObtieneBonificacion;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.BaseConverIF;
import pe.gob.servir.convocatoria.service.BonificacionService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;


@Service
public class BonificacionServiceImpl implements BonificacionService {

	
	@Autowired
	BonificacionRepository bonificacionRepository;
	
	@Autowired
	BonificacionDetalleRepository bonificacionDetalleRepository;
	
	@Autowired
	BonificacionRepositoryJdbc bonificacionRepositoryJdbc;
	
	@Autowired
	MaestraDetalleRepositoryJdbc maestraDetalleRepositoryJdbc;
		
	@Autowired
	BaseInformeComplRepository baseInformeComplRepository;
	
	@Autowired
	MaestraDetalleRepository maestraDetalleRepository;
		
    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
    public RespBase<BonificacionDTO> guardarBonificacion(ReqBase<ReqGuardarBonificacion> request, MyJsonWebToken token) {

        RespBase<BonificacionDTO> response = new RespBase<>();
        BonificacionDTO payload = new BonificacionDTO();
        try {
            if (request.getPayload() == null) {
                response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                        "Bonificacion es nulo");
                return response;
            }
                        
            request.getPayload().getBonificacionDTO().setUsuario(token.getUsuario().getUsuario());
            Bonificacion bonificacion = BaseConverIF.converReqGuardarBonificacion.apply(request.getPayload());
            bonificacion.setTipoInfoId(maestraDetalleRepositoryJdbc.findIdMaestraDetalle(Constantes.COD_TABLA_TIPO_INFORME, Constantes.COD_TI_BONIFICACION));
            bonificacion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
            bonificacion = bonificacionRepository.save(bonificacion);

            payload = BaseConverIF.converBonificacionToDto.apply(bonificacion);
        } catch (Exception e) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Error al guardar la bonificación");
            return response;
        }

        return new RespBase<BonificacionDTO>().ok(payload);
    }

	@Override
	public RespBase<RespObtieneLista<ObtenerBonificacionDTO>> buscarBonificacionByFilter(
			Map<String, Object> parametroMap) {

		List<ObtenerBonificacionDTO> listaBonificacionDTO = bonificacionRepositoryJdbc.buscarBonificacionByFilter(parametroMap);
        RespObtieneLista<ObtenerBonificacionDTO> respPayload = new RespObtieneLista<>();
        respPayload.setCount(listaBonificacionDTO.size());
        respPayload.setItems(listaBonificacionDTO);
        return new RespBase<RespObtieneLista<ObtenerBonificacionDTO>>().ok(respPayload);
	}

	@Override
	public RespBase<RespObtieneBonificacion> obtenerBonificacion(Long bonificacionId) {
		RespBase<RespObtieneBonificacion> response = new RespBase<>();
		 List<BonificacionDetalleDTO> lstBonificacionDetalle = null;
	        if (Util.isEmpty(bonificacionId)) {
	            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
	                    "el id de bonificacion es obligatorio");
	            return response;
	        }

	        Optional<Bonificacion> bonificacionOptional = bonificacionRepository.findByIdBonificacion(bonificacionId);
	        if (!bonificacionOptional.isPresent()) {
	            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
	                    "Bonificación no existe con el id: " + bonificacionId);
	            return response;
	        }
	        
	        RespObtieneBonificacion payload = new RespObtieneBonificacion();
	        BonificacionDTO bonificacion = bonificacionRepositoryJdbc.getBonificacion(bonificacionId);
	        lstBonificacionDetalle = bonificacionRepositoryJdbc.getBonificacionDetalleList(bonificacionId);
	        bonificacion.setBonificacionDetalleDTOList(lstBonificacionDetalle);
	        payload.setBonificacion(bonificacion);      
	        return new RespBase<RespObtieneBonificacion>().ok(payload);
	
	}
	
    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
	public RespBase<BonificacionDTO> actualizarBonificacion(@Valid ReqBase<ReqGuardarBonificacion> request,
			MyJsonWebToken token, Long bonificacionId) {
		RespBase<BonificacionDTO> response = new RespBase<>();
        BonificacionDTO payload = new BonificacionDTO();
        try {
            if (request.getPayload() == null) {
                response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                        "Bonificacion es nulo");
                return response;
            }
            
            Optional<Bonificacion> bonificacionOptional = bonificacionRepository.findByIdBonificacion(bonificacionId);
	        if (!bonificacionOptional.isPresent()) {
	            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
	                    "Bonificación no existe con el id: " + bonificacionId);
	            return response;
	        }else {
	        		        
	        	BonificacionDetalle bonificacionDetalle = null;
	            Bonificacion bonificacion = bonificacionOptional.get();
	            BonificacionDTO bonificacionDTO = request.getPayload().getBonificacionDTO();
	            
	            bonificacion.setTipoInfoId(bonificacionDTO.getTipoInfoId());
	        	bonificacion.setTipoBonificacion(bonificacionDTO.getTipoBonificacion());    	
	        	bonificacion.setTitulo(bonificacionDTO.getTitulo());
	        	bonificacion.setContenido(bonificacionDTO.getContenido());
	            bonificacion.setCampoSegUpd(request.getPayload().getBonificacionDTO().getEstadoRegistro(),token.getUsuario().getUsuario(), Instant.now());

	            for (int i = 0; i < request.getPayload().getBonificacionDTO().getBonificacionDetalleDTOList().size() ; i++) {
	            	
	        		if(bonificacionDTO.getBonificacionDetalleDTOList().get(i).getBonificacionDetalleId()==null) {//ES NUEVO

	        			bonificacionDetalle = BaseConverIF.converReqNuevoDetalleBonificacion.apply(bonificacionDTO.getBonificacionDetalleDTOList().get(i));
	        			bonificacionDetalle.setBonificacion(bonificacion);
	        			bonificacionDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());	
	        			
	        			bonificacion.getBonificacionDetalleList().add(bonificacionDetalle);

	        		}else {//buscamos la bonificacion para actualizar los campos de usuario y fechacreacion para asignarlo
	        			 
		            	for (int k = 0; k < bonificacion.getBonificacionDetalleList().size(); k++) {
	        				if(bonificacionDTO.getBonificacionDetalleDTOList().get(i).getBonificacionDetalleId().equals(bonificacion.getBonificacionDetalleList().get(k).getBonificacionDetalleId())) {
	        					bonificacion.getBonificacionDetalleList().get(k).setDescripcion(bonificacionDTO.getBonificacionDetalleDTOList().get(i).getDescripcion());
	        					bonificacion.getBonificacionDetalleList().get(k).setNivelId(bonificacionDTO.getBonificacionDetalleDTOList().get(i).getNivelId());	        		
	        					bonificacion.getBonificacionDetalleList().get(k).setAplicaId(bonificacionDTO.getBonificacionDetalleDTOList().get(i).getAplicaId());
	        					bonificacion.getBonificacionDetalleList().get(k).setPorcentajeBono(bonificacionDTO.getBonificacionDetalleDTOList().get(i).getPorcentajeBono());
	        					bonificacion.getBonificacionDetalleList().get(k).setCampoSegUpd(bonificacionDTO.getBonificacionDetalleDTOList().get(i).getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());	    	        			
	        					break;
	        				}  					
						}    	            	  				
	        		
	        		}	        		
	    		}
	            
	        	bonificacion = bonificacionRepository.save(bonificacion);
	
	            payload = BaseConverIF.converBonificacionToDto.apply(bonificacion);
	            
	        }
	        
        } catch (Exception e) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "Error al guardar la bonificación");
            return response;
        }

        return new RespBase<BonificacionDTO>().ok(payload);
	}

	@Override
	@Transactional(transactionManager = "convocatoriaTransactionManager")
	public RespBase<Object> eliminarBonificacion(MyJsonWebToken token, Long bonificacionId) {
		
		RespBase<Object> response = new RespBase<>();
		Optional<Bonificacion> bonificacionFind =  bonificacionRepository.findById(bonificacionId);
		if (bonificacionFind.isPresent()){
			Bonificacion bonificacion = bonificacionFind.get();
			
			BonificacionDetalle bonifiacionDetalle = new BonificacionDetalle();
			bonifiacionDetalle.setBonificacion(bonificacion);
			Example<BonificacionDetalle> example = Example.of(bonifiacionDetalle);
			List<BonificacionDetalle> ltaBonificacionFilter = bonificacionDetalleRepository.findAll(example);
			
			if(ltaBonificacionFilter != null && !ltaBonificacionFilter.isEmpty()) {
				for(BonificacionDetalle p : ltaBonificacionFilter) {
					p.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
					Instant.now());
					bonificacionDetalleRepository.save(p);
				}
			}
			
			bonificacion.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
					Instant.now());
			bonificacionRepository.save(bonificacion);
			
			response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se eliminó la bonificacion correctamente ");
		}else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el bonificacionId Ingresado");
		}
		return response;
		
	}

	@Override
	public RespBase<RespObtieneLista<RespBonificacionDTO>> obtenerBonificacionPorBase(Long baseId) {
		List<RespBonificacionDTO> listaBonificacionDTO = new ArrayList<>();
		InformeComplement comple = new InformeComplement();
		comple.setBaseId(baseId);
		comple.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<InformeComplement> filtroComple = Example.of(comple);
		List<InformeComplement> lista = baseInformeComplRepository.findAll(filtroComple,Sort.by("bonificacionId").ascending());
		for (InformeComplement informeComplement : lista) {
			if(Objects.nonNull(informeComplement.getBonificacionId())){
				Optional<Bonificacion> bonificacionOptional = bonificacionRepository.findById(informeComplement.getBonificacionId());
				if(bonificacionOptional.isPresent()) {
					Bonificacion boni = bonificacionOptional.get();
					RespBonificacionDTO bonificacionDTO = new RespBonificacionDTO(boni);
					BonificacionDetalle detBonificacion = new BonificacionDetalle();
					detBonificacion.setBonificacion(new Bonificacion(boni.getBonificacionId()));
					detBonificacion.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
					Example<BonificacionDetalle> filtroDetBonficacion = Example.of(detBonificacion);
					List<BonificacionDetalle> ltaDetBonificacion = bonificacionDetalleRepository.findAll(filtroDetBonficacion,Sort.by("nivelId").ascending());
					List<RespBonificacionDetalleDTO> listaDetBoni = new ArrayList<>();
					for (BonificacionDetalle itemDetalle : ltaDetBonificacion) {
						RespBonificacionDetalleDTO detalle = new RespBonificacionDetalleDTO(itemDetalle);
						if(Objects.nonNull(itemDetalle.getNivelId())) {
							Optional<MaestraDetalle> nivel =  maestraDetalleRepository.findById(itemDetalle.getNivelId());
							if(nivel.isPresent()) {
								detalle.setDesNivel(nivel.get().getDescripcion());
							}
						}					
						listaDetBoni.add(detalle);
					}	
					bonificacionDTO.setLtaDetalleBonificacion(listaDetBoni);
					listaBonificacionDTO.add(bonificacionDTO);
				}
			}
		}
		return new RespBase<RespBonificacionDTO>().okLista(listaBonificacionDTO);
	}
}
