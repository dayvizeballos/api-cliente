package pe.gob.servir.convocatoria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BasePerfil;
import pe.gob.servir.convocatoria.model.CarreraProfesional;
import pe.gob.servir.convocatoria.model.Conocimiento;
import pe.gob.servir.convocatoria.model.FormacionAcademica;
import pe.gob.servir.convocatoria.model.FuncionDetalle;
import pe.gob.servir.convocatoria.model.Perfil;
import pe.gob.servir.convocatoria.model.PerfilExperiencia;
import pe.gob.servir.convocatoria.model.PerfilExperienciaDetalle;
import pe.gob.servir.convocatoria.model.PerfilFuncion; 

@Repository
public interface ReportePerfilRepository  extends JpaRepository<Perfil, Long>{
	
/*
	 @Query("select p from Perfil p where perfilId=:perfilId")
	public Perfil findEvaluacion(@Param("perfilId")Long perfilId); 
	 
	 @Query(" select fd from PerfilFuncion f\r\n" + 
	 		" inner join FuncionDetalle fd\r\n" + 
	 		" on f.perfilFuncionId = fd.perfilFuncion.perfilFuncionId where f.perfilId =:perfilId")
	 public List<FuncionDetalle> findActividades(@Param("perfilId")Long perfilId); 
	 
	 @Query(" select f from PerfilFuncion f\r\n" + 
		 		" where f.perfilId =:perfilId")
		 public PerfilFuncion findCondiciones(@Param("perfilId")Long perfilId); 

	 @Query( "select fa "
	 		+ "from FormacionAcademica fa "
	 		+ "inner join CarreraFormacionAcademica cfa "
	 		+ "on fa.formacionAcademicaId = cfa.fomarcionAcademica.formacionAcademicaId "
	 		+ "inner join CarreraProfesional cf "
	 		+ "on cfa.carreraId = cf.id "
	 		+ "where fa.perfilId=:perfilId" )
	 public List<FormacionAcademica> findFormacionAcademica(@Param("perfilId")Long perfilId);



	 @Query( "select cf "
		 		+ "from FormacionAcademica fa "
		 		+ "inner join CarreraFormacionAcademica cfa "
		 		+ "on fa.formacionAcademicaId = cfa.fomarcionAcademica.formacionAcademicaId "
		 		+ "inner join CarreraProfesional cf "
		 		+ "on cfa.carreraId = cf.id "
		 		+ "where fa.perfilId=:perfilId" )
		 public List<CarreraProfesional> findFormacionAcademicaCarrera(@Param("perfilId")Long perfilId);


	 
	 @Query("select c from Conocimiento c where c.perfilId=:perfilId")
	 public List<Conocimiento> findConocimiento(@Param("perfilId")Long perfilId);
	 
	 @Query("select pes from PerfilExperiencia pes where pes.id= "
	 		+ " (select min(id) from PerfilExperiencia pe where pe.perfilId=:perfilId)"  )
	 public PerfilExperiencia  findExperiencia(@Param("perfilId")Long perfilId);
	 
	 @Query("select ped from PerfilExperienciaDetalle ped "
	 		+ "inner join PerfilExperiencia pe on "
	 		+ "ped.perfilExperiencia.id = pe.id  where pe.perfilId=:perfilId")
	 public List<PerfilExperienciaDetalle> findExpDetalle(@Param("perfilId")Long perfilId);
	 
	 //--------------- GENERAL-------------------

	 @Query("select b.baseId from BasePerfil bp inner join Perfil p on bp.perfilId=p.perfilId "
	 		+ "inner join Base b on b.baseId=bp.base.baseId where p.perfilId=:perfilId")
	 public Integer findBaseId(@Param("perfilId")Long perfilId);


	 
	 @Query("select b from Base b where  b.baseId=:baseId")
		 public Base findBase(@Param("baseId")Long baseId);
	 
	 @Query("select b from BasePerfil b where  b.perfilId=:perfilId")
	 public BasePerfil findBasePerfil(@Param("perfilId")Long perfilId);
*/

}
