package pe.gob.servir.convocatoria.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.model.Perfil;
import pe.gob.servir.convocatoria.model.PerfilExperiencia;
import pe.gob.servir.convocatoria.model.PerfilExperienciaDetalle;
import pe.gob.servir.convocatoria.repository.PerfilExperienciaRepository;
import pe.gob.servir.convocatoria.repository.PerfilExperienciaRepositoryJdbc;
import pe.gob.servir.convocatoria.repository.PerfilRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDetalleDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.PerfilExperienciaConvertIF;
import pe.gob.servir.convocatoria.service.PerfilExperienciaService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class PerfilExperienciaServiceImpl implements PerfilExperienciaService {

    @Autowired
    PerfilExperienciaRepository perfilExperienciaRepository;

    @Autowired
    PerfilExperienciaRepositoryJdbc perfilExperienciaRepositoryJdbc;
    	
    @Autowired
    PerfilRepository perfilRepository;
    
    @Override
    public RespBase<PerfilExperienciaDTO> crearPerfilExperiencia(ReqBase<PerfilExperienciaDTO> request, MyJsonWebToken token) {
        PerfilExperiencia perfilExperiencia = new PerfilExperiencia();
        if(request.getPayload() != null){
            PerfilExperienciaDTO perfilExperienciaDTO = request.getPayload();
            List<PerfilExperienciaDetalle> experienciaDetalleList = new ArrayList<>();
            List<PerfilExperienciaDetalle> experienciaDetalleList2 = new ArrayList<>();
             perfilExperiencia = PerfilExperienciaConvertIF.convertPerfilExperiencia.apply(perfilExperienciaDTO);
             perfilExperiencia.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());


            if(request.getPayload().getDetalle() != null && !request.getPayload().getDetalle().isEmpty()){
              for (PerfilExperienciaDetalleDTO it : perfilExperienciaDTO.getDetalle()){
                  PerfilExperienciaDetalle detalle =  PerfilExperienciaConvertIF.convertPerfilExperienciaDetalle.apply(it);
                  detalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                  detalle.setTiDaExId(Constantes.HABILIDADES);
                  detalle.setPerfilExperiencia(perfilExperiencia);
                  experienciaDetalleList.add(detalle);
              }
            }


            if(request.getPayload().getDetalle2() != null && !request.getPayload().getDetalle2().isEmpty()){
                for (PerfilExperienciaDetalleDTO it : perfilExperienciaDTO.getDetalle2()){
                    PerfilExperienciaDetalle detalle =  PerfilExperienciaConvertIF.convertPerfilExperienciaDetalle.apply(it);
                    detalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    detalle.setTiDaExId(Constantes.REQUISITOS);
                    detalle.setPerfilExperiencia(perfilExperiencia);
                    experienciaDetalleList2.add(detalle);
                }
            }

            perfilExperiencia.setPerfilExperienciaDetalleList1(new ArrayList<>());
            perfilExperiencia.setPerfilExperienciaDetalleList2(new ArrayList<>());

            perfilExperiencia.setPerfilExperienciaDetalleList1(experienciaDetalleList);
            perfilExperiencia.setPerfilExperienciaDetalleList2(experienciaDetalleList2);

        }

        perfilExperienciaRepository.save(perfilExperiencia);
        
        PerfilExperienciaDTO  payload = new PerfilExperienciaDTO();
        payload = PerfilExperienciaConvertIF.convertPerfilExperiencia2.apply(perfilExperiencia);

        List<PerfilExperienciaDetalleDTO> experienciaDetalleDTOList = new ArrayList<>();
        List<PerfilExperienciaDetalleDTO> experienciaDetalleDTOList2 = new ArrayList<>();

        if(perfilExperiencia.getPerfilExperienciaDetalleList1() != null && !perfilExperiencia.getPerfilExperienciaDetalleList1().isEmpty()){
            for (PerfilExperienciaDetalle it : perfilExperiencia.getPerfilExperienciaDetalleList1()){
                PerfilExperienciaDetalleDTO detalle =  PerfilExperienciaConvertIF.convertPerfilExperienciaDetalle2.apply(it);
                experienciaDetalleDTOList.add(detalle);
            }
        }

        if(perfilExperiencia.getPerfilExperienciaDetalleList2() != null && !perfilExperiencia.getPerfilExperienciaDetalleList2().isEmpty()){
            for (PerfilExperienciaDetalle it : perfilExperiencia.getPerfilExperienciaDetalleList2()){
                PerfilExperienciaDetalleDTO detalle =  PerfilExperienciaConvertIF.convertPerfilExperienciaDetalle2.apply(it);
                experienciaDetalleDTOList2.add(detalle);
            }
        }
        
        Optional<Perfil> perfilOptional = perfilRepository.findById(request.getPayload().getPerfilId());
		if (perfilOptional.isPresent()) {
			Perfil perfil = perfilOptional.get();
			perfil.setEstadoExp(Constantes.ACTIVO);
			perfilRepository.save(perfil);
		}

        payload.setDetalle(new ArrayList<>());
        payload.setDetalle(experienciaDetalleDTOList);
        payload.setDetalle2(experienciaDetalleDTOList2);
        return new RespBase<PerfilExperienciaDTO>().ok(payload);

    }

    @Override
    public RespBase<PerfilExperienciaDTO> actualizarPerfilExperiencia(ReqBase<PerfilExperienciaDTO> request, MyJsonWebToken token) {

        RespBase<PerfilExperienciaDTO> response = new RespBase<>();


        if ( Util.isEmpty(request.getPayload().getId())){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "el id de perfil experiencia es null");
            return response;
        }

       Optional<PerfilExperiencia> perfilExperiencia  = perfilExperienciaRepository.findById(request.getPayload().getId());
        if (!perfilExperiencia.isPresent()){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "perfil experiencia no existe con el id: "+request.getPayload().getId());
            return response;
        }

        PerfilExperiencia perfilExperienciaUp = PerfilExperienciaConvertIF.convertPerfilExperiencia.apply(request.getPayload());
        perfilExperienciaUp.setCampoSegUpd(Constantes.ACTIVO , token.getUsuario().getUsuario() , Instant.now());
        perfilExperienciaUp.setPerfilExperienciaDetalleList1(new ArrayList<>());
        perfilExperienciaUp.setPerfilExperienciaDetalleList2(new ArrayList<>());

        List<PerfilExperienciaDetalle> detalleList = new ArrayList<>();
        List<PerfilExperienciaDetalle> detalleList2 = new ArrayList<>();


        if (request.getPayload().getDetalle()!= null && !request.getPayload().getDetalle().isEmpty()){
            for (PerfilExperienciaDetalleDTO it : request.getPayload().getDetalle()){
                PerfilExperienciaDetalle experienciaDetalle = PerfilExperienciaConvertIF.convertPerfilExperienciaDetalle.apply(it);
                experienciaDetalle.setPerfilExperiencia(perfilExperienciaUp);
                if (experienciaDetalle.getId() == null){
                    experienciaDetalle.setCampoSegIns(token.getUsuario().getUsuario() , Instant.now());
                }else{
                    experienciaDetalle.setCampoSegUpd(Constantes.ACTIVO , token.getUsuario().getUsuario() , Instant.now());
                }
                experienciaDetalle.setEstadoRegistro(it.getEstado().trim());
                detalleList.add(experienciaDetalle);
            }
        }

        if (request.getPayload().getDetalle2()!= null && !request.getPayload().getDetalle2().isEmpty()){
            for (PerfilExperienciaDetalleDTO it : request.getPayload().getDetalle2()){
                PerfilExperienciaDetalle experienciaDetalle = PerfilExperienciaConvertIF.convertPerfilExperienciaDetalle.apply(it);
                experienciaDetalle.setPerfilExperiencia(perfilExperienciaUp);
                if (experienciaDetalle.getId() == null){
                    experienciaDetalle.setCampoSegIns(token.getUsuario().getUsuario() , Instant.now());
                }else{
                    experienciaDetalle.setCampoSegUpd(Constantes.ACTIVO , token.getUsuario().getUsuario() , Instant.now());
                }
                experienciaDetalle.setEstadoRegistro(it.getEstado().trim());
                detalleList2.add(experienciaDetalle);
            }
        }

        detalleList.addAll(detalleList2);

        perfilExperienciaUp.setPerfilExperienciaDetalleList1(detalleList);
        perfilExperienciaUp.setPerfilExperienciaDetalleList2(null);

        perfilExperienciaRepository.save(perfilExperienciaUp);

        PerfilExperienciaDTO resPayload = new PerfilExperienciaDTO();
        List<PerfilExperienciaDetalleDTO> detalles = new ArrayList<>();
        List<PerfilExperienciaDetalleDTO> detalles2 = new ArrayList<>();

        List<PerfilExperienciaDTO> ls =  perfilExperienciaRepositoryJdbc.listPerfilExperiencia(perfilExperienciaUp.getPerfilId());

        if (!ls.isEmpty()) {
            resPayload = ls.get(0);
            detalles =  perfilExperienciaRepositoryJdbc.listPerfilExperienciaDetalle(resPayload.getId() , Constantes.HABILIDADES);
            detalles2 =  perfilExperienciaRepositoryJdbc.listPerfilExperienciaDetalle(resPayload.getId() , Constantes.REQUISITOS);
            resPayload.setDetalle(detalles);
            resPayload.setDetalle2(detalles2);
        }
        
        Optional<Perfil> perfilOptional = perfilRepository.findById(request.getPayload().getPerfilId());
		if (perfilOptional.isPresent()) {
			Perfil perfil = perfilOptional.get();
			perfil.setEstadoExp(Constantes.ACTIVO);
			perfilRepository.save(perfil);
		}

        return new RespBase<PerfilExperienciaDTO>().ok(resPayload);
    }

    @Override
    public RespBase<PerfilExperienciaDTO> findPerfilExpereincia(Long perfilId) {
        RespBase<PerfilExperienciaDTO> response = new RespBase<>();
        if (Util.isEmpty(perfilId)){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "perfilId es null o vacio");
            return response;
        }

        List<PerfilExperienciaDTO> listPerfilExperienciaDTOList = new ArrayList<>();
        PerfilExperienciaDTO perfilExperienciaDTO = new PerfilExperienciaDTO();

        listPerfilExperienciaDTOList = perfilExperienciaRepositoryJdbc.listPerfilExperiencia(perfilId);

        if (listPerfilExperienciaDTOList == null ||  listPerfilExperienciaDTOList.isEmpty()){
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "no existe Perfil Experiencia Funcion con el perfil: "+perfilId);
            return response;
        }

        perfilExperienciaDTO = listPerfilExperienciaDTOList.get(0);

        perfilExperienciaDTO.setDetalle(perfilExperienciaRepositoryJdbc.listPerfilExperienciaDetalle(perfilExperienciaDTO.getId() , Constantes.HABILIDADES));
        perfilExperienciaDTO.setDetalle2(perfilExperienciaRepositoryJdbc.listPerfilExperienciaDetalle(perfilExperienciaDTO.getId() , Constantes.REQUISITOS));

        return new RespBase<PerfilExperienciaDTO>().ok(perfilExperienciaDTO);
    }
}
