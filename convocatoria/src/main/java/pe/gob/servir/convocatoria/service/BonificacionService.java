package pe.gob.servir.convocatoria.service;

import java.util.Map;

import javax.validation.Valid;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarBonificacion;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerBonificacionDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespBonificacionDTO;
import pe.gob.servir.convocatoria.response.RespObtieneBonificacion;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface BonificacionService {
	
    public RespBase<BonificacionDTO> guardarBonificacion(ReqBase<ReqGuardarBonificacion> request, MyJsonWebToken token) ;

	public RespBase<BonificacionDTO> actualizarBonificacion(@Valid ReqBase<ReqGuardarBonificacion> request,
			MyJsonWebToken token, Long bonificacionId);
			
    public RespBase<RespObtieneLista<ObtenerBonificacionDTO>> buscarBonificacionByFilter(Map<String, Object> parametroMap);
    RespBase<RespObtieneBonificacion> obtenerBonificacion(Long bonificacionId );
    RespBase<Object> eliminarBonificacion(MyJsonWebToken token, Long bonificacionId);

	public RespBase<RespObtieneLista<RespBonificacionDTO>> obtenerBonificacionPorBase(Long baseId);
}
