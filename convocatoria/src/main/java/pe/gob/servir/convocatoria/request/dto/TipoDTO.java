package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JGlobalMap
@Getter
@Setter
public class TipoDTO {
    private Long maeDetalleId;
    private Long maeCabeceraId;
    private String descripcion;
    private String descripcionCorta;
    private String estadoRegistro;
    private String estado;
    private Long jerarquia;
    private Long codigoNivel1;
    private Long codigoNivel2;
    private Long codigoNivel3;
    List<EvaluacionDTO> listaEvaluacion;
}
