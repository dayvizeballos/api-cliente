package pe.gob.servir.convocatoria.service;

import java.util.Map;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraConocimiento;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespListaMaestraConocimiento;
import pe.gob.servir.convocatoria.response.RespMaestraConocimiento;
import pe.gob.servir.convocatoria.response.RespObtieneConocimiento;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface MaestraConocimientoService {

	RespBase<RespMaestraConocimiento> guardarMaestraConocimiento(ReqBase<ReqMaestraConocimiento> request, MyJsonWebToken token, 
			Long maeConocimientoId);

	RespBase<RespListaMaestraConocimiento> obtenerMaestraConocimiento(Map<String, Object> parametroMap);
	
	RespBase<RespObtieneConocimiento> obtenerMaestraConocimientoPorId(Long maeConocimientoId);
	
	 RespBase<Object> eliminarConocimiento(MyJsonWebToken token, Long maeConocimientoId);
	
}
