package pe.gob.servir.convocatoria.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.BasePerfil;

@Getter
@Setter
@SuppressWarnings("serial")
public class RespPerfilVacante implements Serializable {
    
	private Long convocatoriaSelId;
	private Long idbase;
	private Long idperfil;
	private String nombrePuesto;
	private Long vacantes;
    private Long identidad;

}
