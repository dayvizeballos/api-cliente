package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportePerfilConocimiento {
	private String descripcion;
	private String niveldominio;
	private Integer horas;
	private String tipo;
}
