package pe.gob.servir.convocatoria.queue.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.var;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.servir.convocatoria.response.dto.ReporteConvocatoriaDTO;


@Service
public class ReporteConvocatoriaProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    public void sendMessageBase(ReporteConvocatoriaDTO p) throws JsonProcessingException {
        var json = objectMapper.writeValueAsString(p);
        rabbitTemplate.convertAndSend("x.reporte", "reporte", json);

    }
}
