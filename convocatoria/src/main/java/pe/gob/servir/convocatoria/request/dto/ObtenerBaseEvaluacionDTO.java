package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class ObtenerBaseEvaluacionDTO {
	private Long informeDetalleId;
	private String observacion;
}
