package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostulanteDTO {
	
	private Long baseId;
	private Long postulanteId;
	private Long perfilId;
	private Long entidadId;

}
