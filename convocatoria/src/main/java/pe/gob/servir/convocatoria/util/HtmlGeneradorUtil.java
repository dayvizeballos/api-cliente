package pe.gob.servir.convocatoria.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import pe.gob.servir.convocatoria.annotation.AplicarCss;
import pe.gob.servir.convocatoria.annotation.ColumnaTablaHtml;
import pe.gob.servir.convocatoria.annotation.StyleCss;
import pe.gob.servir.convocatoria.annotation.TituloHtml;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.CssStyle;
import pe.gob.servir.convocatoria.common.HtmlConstantes;

public class HtmlGeneradorUtil {
	
	private HtmlGeneradorUtil() {
		super();
	}
	
	public static String buildParagraph(String texto) {
		return HtmlConstantes.OPEN_P_TAG + texto + HtmlConstantes.CLOSE_P_TAG;
	}
	
	public static String buildTitlePdf(Object obj) {
		StringBuilder tituloPdf = new StringBuilder();
		Class<?> clazz = obj.getClass();
		if(clazz.isPrimitive() || obj instanceof String) {
			tituloPdf.append("<p style=\"text-align: center;\"><strong>"+obj+"</strong></p>");
		}else {
		    for (Field field : clazz.getDeclaredFields()) {
		        field.setAccessible(true);
		        if (field.isAnnotationPresent(TituloHtml.class)) {
		        	String openParrafo = HtmlConstantes.OPEN_P_STYLE_TAG.replace(Constantes.PARAM_INDEX_0, field.getAnnotation(TituloHtml.class).style());
		        	boolean isStrong = field.getAnnotation(TituloHtml.class).strong();
		        	String valor = field.getAnnotation(TituloHtml.class).nombre();
		        	tituloPdf.append(openParrafo);
		        	if(isStrong) {
		        		tituloPdf.append(HtmlConstantes.OPEN_STRONG_TAG);
		        		tituloPdf.append(valor);
		        		tituloPdf.append(HtmlConstantes.OPEN_STRONG_TAG);
		        	}else {
		        		tituloPdf.append(valor);
		        	}
		        	tituloPdf.append(HtmlConstantes.CLOSE_P_TAG);
		        }
		    }	
		}
		
		return tituloPdf.toString();
	}
	
	public static String alineacionDerecha(Object des) {
		StringBuilder descripcion = new StringBuilder();
		Class<?> clazz = des.getClass();
		if(clazz.isPrimitive() || des instanceof String) {
			descripcion.append("<p style=\"text-align: right;\">"+des+"</p>");
		}
		
		return descripcion.toString();
	}
	
	
	public static String buildTableFooter(Object obj) throws IllegalAccessException {
		StringBuilder tfooterHtml = new StringBuilder();
	    Map<Integer, Object> jsonElementsMap = beanProperties(obj);
	    Map<Integer, Object> map = new TreeMap<>(jsonElementsMap);
		
	    tfooterHtml.append(HtmlConstantes.OPEN_TR_TAG);
		for (Map.Entry<Integer, Object> entry : map.entrySet()) {
			tfooterHtml.append(HtmlConstantes.OPEN_TD_TAG + entry.getValue() + HtmlConstantes.CLOSE_TD_TAG);
	     }
		tfooterHtml.append(HtmlConstantes.CLOSE_TR_TAG);
		
		return tfooterHtml.toString();
	}
	
	
	public static String buildTableHeader(Object obj) throws IllegalAccessException {
		StringBuilder theadersHtml = new StringBuilder();
		Class<?> clazz = obj.getClass();
	    Map<Integer, Object> jsonElementsMap = new HashMap<>();
	    for (Field field : clazz.getDeclaredFields()) {
	        field.setAccessible(true);
	        if (field.isAnnotationPresent(ColumnaTablaHtml.class)) {
	        	
	        	String ignoreWhenValue = field.getAnnotation(ColumnaTablaHtml.class).ignoreWhen();
	        	int order = field.getAnnotation(ColumnaTablaHtml.class).orden();
	        	String nombreColumna = field.getAnnotation(ColumnaTablaHtml.class).nombre();
	        	boolean validIgnore = field.getAnnotation(ColumnaTablaHtml.class).validIgnore();
	        	
	        	if(!(validIgnore && StringUtils.compare((field.get(obj)+StringUtils.EMPTY), ignoreWhenValue)==0)) {
	        		jsonElementsMap.put(order, nombreColumna);
	        	}
	        }
	    }	
	    Map<Integer, Object> map = new TreeMap<>(jsonElementsMap);
	    
		theadersHtml.append(HtmlConstantes.OPEN_TR_TAG);
		for (Map.Entry<Integer, Object> entry : map.entrySet()) {
			theadersHtml.append(HtmlConstantes.OPEN_TH_TAG + entry.getValue() + HtmlConstantes.CLOSE_TH_TAG);
	     }
		theadersHtml.append(HtmlConstantes.CLOSE_TR_TAG);
		
		return theadersHtml.toString();
	}
	
	public static String buildTableBody(List<?> lista) throws  IllegalAccessException {
		StringBuilder tBodyHtml = new StringBuilder();
		for (Object object : lista) {
			tBodyHtml.append(HtmlConstantes.OPEN_TR_TAG);
			Map<Integer, Object> map = beanProperties(object);

			for (Map.Entry<Integer, Object> entry : map.entrySet()) {
				tBodyHtml.append(HtmlConstantes.OPEN_TD_TAG + entry.getValue() + HtmlConstantes.CLOSE_TD_TAG);
		     }
			tBodyHtml.append(HtmlConstantes.CLOSE_TR_TAG);
			
		}
		return tBodyHtml.toString();
	}
	
	public static Map<Integer, Object> beanProperties(Object bean) throws  IllegalAccessException {
		
		Class<?> clazz = bean.getClass();
	    Map<Integer, Object> jsonElementsMap = new HashMap<>();
	    for (Field field : clazz.getDeclaredFields()) {
	        field.setAccessible(true);
	        if (field.isAnnotationPresent(ColumnaTablaHtml.class)) {
	        	String ignoreWhenValue = field.getAnnotation(ColumnaTablaHtml.class).ignoreWhen();
	        	int order = field.getAnnotation(ColumnaTablaHtml.class).orden();
	        	String concatValue = field.getAnnotation(ColumnaTablaHtml.class).concatValue();
	        	boolean validIgnore = field.getAnnotation(ColumnaTablaHtml.class).validIgnore();
	        	if(!(validIgnore && StringUtils.compare((field.get(bean)+StringUtils.EMPTY), ignoreWhenValue)==0)) {
	        		jsonElementsMap.put(order, (field.get(bean)+concatValue));
	        	}
	        }
	    }	
	    return new TreeMap<>(jsonElementsMap);
	}
	
	public static String buildTableHtmlCode(String theadersHtml,String tbodyHtml,String tfootHtml) {
		
		StringBuilder sb = new StringBuilder(StringUtils.EMPTY);
		sb.append("<table class=\"minimalistBlack\">");
		
		if(Objects.nonNull(theadersHtml) && StringUtils.compare(StringUtils.EMPTY, theadersHtml)!=0) {
			sb.append("<thead>");
			sb.append(theadersHtml);
			sb.append("</thead>");
		}
		
		sb.append("<tbody>");
		sb.append(tbodyHtml);
		sb.append("</tbody>");
		
		if(Objects.nonNull(tfootHtml) && StringUtils.compare(StringUtils.EMPTY, tfootHtml)!=0) {
			sb.append("<tfoot>");
			sb.append(tfootHtml);
			sb.append("</tfoot>");
		}
		
		sb.append("</table>");
		
		return sb.toString();
		
		
	}
	
	public static StringBuilder addCodeBeforeHtmlTag(StringBuilder sb, String tag, String codeHtml) {
		int indexHtmlTag = sb.indexOf(tag);
		sb.insert(indexHtmlTag, codeHtml);
		return sb;
	}
	
	public static String buildCssStyle(CssStyle object) throws  IllegalAccessException {
		
		Class<?> clazz = object.getClass();
	    StringBuilder styleAttribute = new StringBuilder(" style=\"\" ");
	    for (Field field : clazz.getDeclaredFields()) {
	        field.setAccessible(true);
	        if (field.isAnnotationPresent(StyleCss.class)) {
	        	String styleName = field.getAnnotation(StyleCss.class).styleName();
	        	int indexHtmlTag = styleAttribute.lastIndexOf("\"");
	        	if(Objects.nonNull(field.get(object)))
	        		styleAttribute.insert(indexHtmlTag, (styleName + ":" + field.get(object)+";"));
	        }
	    }
	    
		return styleAttribute.toString();	
		
	}
	
	public static String buildTableBodyWithCss(List<?> lista, CssStyle css) throws  IllegalAccessException {
		StringBuilder tBodyHtml = new StringBuilder();
		for (Object object : lista) {
			tBodyHtml.append(HtmlConstantes.OPEN_TR_TAG);
			
			Class<?> clazz = object.getClass();
		    Map<Integer, Field> jsonElementsMap = new HashMap<>();
		    for (Field field : clazz.getDeclaredFields()) {
		        field.setAccessible(true);
		        if (field.isAnnotationPresent(ColumnaTablaHtml.class)) {
		        	int order = field.getAnnotation(ColumnaTablaHtml.class).orden();
		        	jsonElementsMap.put(order, field);
		        }
		    }	
		    tBodyHtml = generateTbodyHtml(tBodyHtml, clazz, css, object, jsonElementsMap).append(HtmlConstantes.CLOSE_TR_TAG);
		}
		return tBodyHtml.toString();
	}
	
	private static StringBuilder generateTbodyHtml(StringBuilder tBodyHtml, Class<?> clazz,
			CssStyle css, Object object, Map<Integer, Field> jsonElementsMap)
			throws  IllegalAccessException {
		Map<Integer, Field> mapaOrdenadoAsc = new TreeMap<>(jsonElementsMap);
		for (Map.Entry<Integer, Field> entry : mapaOrdenadoAsc.entrySet()) {
			Field field = entry.getValue();
			
			String ignoreWhenValue = field.getAnnotation(ColumnaTablaHtml.class).ignoreWhen();
        	String concatValue = field.getAnnotation(ColumnaTablaHtml.class).concatValue();
        	boolean validIgnore = field.getAnnotation(ColumnaTablaHtml.class).validIgnore();
        	Object valueField = field.get(object);
        	
        	if(!(validIgnore && StringUtils.compare((field.get(object)+StringUtils.EMPTY), ignoreWhenValue)==0)) {
        		valueField = valueField+concatValue;
        		if(field.isAnnotationPresent(AplicarCss.class) || clazz.isAnnotationPresent(AplicarCss.class)) {
	        		StringBuilder tag = new StringBuilder(HtmlConstantes.OPEN_TD_TAG);
	        		tBodyHtml.append(addCodeBeforeHtmlTag(tag, ">", buildCssStyle(css)).toString() + valueField + HtmlConstantes.CLOSE_TD_TAG);
	        	}else {
	        		tBodyHtml.append(HtmlConstantes.OPEN_TD_TAG + valueField + HtmlConstantes.CLOSE_TD_TAG);
	        	}
        	}
	     }
		return tBodyHtml;
	}

}
