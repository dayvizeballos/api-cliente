package pe.gob.servir.convocatoria.response;

import lombok.Data;

import java.util.List;

@Data
public class RespDifusionConvocatoria {
    private Long idBase;
    private Long idPerfil;
    private String nombrePuesto;
    private Long idRegimen;
    private String regimen;
    private Integer idEntidad;
    private String entidad;
    private Double remuneracion;
    private String prestacionServicio;
    private Long vacantes;
    private String aniosExpTotal;
    private String fechaHasta;
    private String fechaCierre;
    private Long idSede;
    private String sede;
    private String correo;
    private String direccion;
    private String telefono;
    private String anexo;
    private String contacto;
    private Long idEstado;
    private String estado;
    private String codProEstado;
    private Long codEstado;
    private Long idDepartamento;
    private String departamento;
    private Long idProvincia;
    private String provincia;
    private Long idDistrito;
    private String distrito;
    private String misionPuesto;
    private Long basePerfilId;
    private String logoEntidad;
    private String portadaEntidad;
    private String urlPdfBase;
    private String codProgRegimen;
    private Long dirPublId;
    private String dirPublNombre;
    private String codigoConvocatoria;

    private List<RespDifusionExperiencia> experienciaList;
    private List<RespDifusionConocimientos> conocimientoList;
    private List<RespCondicionAtipica> condicionAtipicasList;
    private List<RespDifusionFormacionAcademica> formacionAcademicaList;

    private List<Long> carreras;
    private List<String> funcionesPuestoList; //to actividades practicante
    private List<String> habilidadesList;
    private List<String> requiAdicionalesList;
}
