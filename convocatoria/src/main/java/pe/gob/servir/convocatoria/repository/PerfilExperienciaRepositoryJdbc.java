package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDetalleDTO;

import java.util.List;

public interface PerfilExperienciaRepositoryJdbc {
    public List<PerfilExperienciaDTO> listPerfilExperiencia (Long perfilId);
    public List<PerfilExperienciaDetalleDTO> listPerfilExperienciaDetalle (Long perfiExperiencia , Long tipo);
}
