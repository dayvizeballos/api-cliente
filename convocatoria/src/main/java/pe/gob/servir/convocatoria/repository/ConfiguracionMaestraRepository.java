package pe.gob.servir.convocatoria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.ConfiguracionMaestra;

@Repository
public interface ConfiguracionMaestraRepository extends JpaRepository<ConfiguracionMaestra, Long>{
	
	@Query("SELECT distinct config.maeCabeceraId FROM ConfiguracionMaestra config WHERE config.entidadId=:idEntidad")
	public List<Long> findAllByEntidad(@Param("idEntidad")Long idEntidad);
	
	@Query("SELECT config FROM ConfiguracionMaestra config WHERE config.maeDetalleId=:idDetalle AND config.entidadId=:idEntidad ORDER BY config.maeDetalleId DESC")
	public ConfiguracionMaestra findByDetalleServir(@Param("idDetalle")Long idDetalle, @Param("idEntidad")Long idEntidad);
	
	@Query("SELECT config FROM ConfiguracionMaestra config WHERE config.maeDetalleEntidadId=:idDetalleEntidad AND config.entidadId=:idEntidad ORDER BY config.maeDetalleEntidadId DESC")
	public ConfiguracionMaestra findByDetalleEntidad(@Param("idDetalleEntidad")Long idDetalleEntidad,@Param("idEntidad")Long idEntidad);	
	
}
