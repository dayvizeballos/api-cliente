package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_perfil_funcion_detalle", schema = "sch_convocatoria")
@Getter
@Setter
public class FuncionDetalle extends AuditEntity implements AuditableEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil_funcion_detalle")
	@SequenceGenerator(name = "seq_perfil_funcion_detalle", sequenceName = "seq_perfil_funcion_detalle", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "perfil_funcion_detalle_id")
	private Long funcionDetalleId;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "orden")
	private Integer orden;

	@JoinColumn(name="perfil_funcion_id")
	@ManyToOne(optional = false)
	@JsonBackReference
	private PerfilFuncion perfilFuncion;



}
