package pe.gob.servir.convocatoria.response;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class RespCriterioEvaluacion {

	private Long informeDetalleId;
	private Long tipoInfoId;
	private String nombretipoInfo;
	private String titulo;
	private Long entidadId;
	private String estadoRegistro;
	private String codProg;
}
