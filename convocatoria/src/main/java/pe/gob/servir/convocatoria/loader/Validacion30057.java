package pe.gob.servir.convocatoria.loader;


import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.springframework.stereotype.Component;
import pe.gob.servir.convocatoria.model.CargaMasiva;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

@Component
public class Validacion30057 {


	public  String obtenerValorCelda(HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila, CargaMasiva carga ,  HSSFFormulaEvaluator hssFormulaEvaluator ,
									 XSSFFormulaEvaluator xssFormulaEvaluator) throws Exception {

		String valor = "";
		String salida = "";
		
		salida = getValor(hssfFilaExcel, xssfFilaExcel, hssFormulaEvaluator, xssFormulaEvaluator, carga);
		valor = validaCampo(salida, carga, fila) ;
		if(!valor.equalsIgnoreCase(ConstantesExcel.OK)) {
			salida = valor;
		}

		return salida;
	}

	public String validaCampo(String valor,CargaMasiva carga, int fila) {
		String salida = "";

		if (carga.getValida().equals(ConstantesExcel.OBLIGATORIO) && Util.isEmpty(valor)) {
			salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
					+ ConstantesExcel.OBS_CAMPO_NULO;
			return salida;
		}

		if (!Util.isEmpty(valor)) {
			switch (carga.getTipoColummna()) {
				case ConstantesExcel.CAMPO_NUMERICO: {
					if (!Util.isDouble(valor)) {
						salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
								+ ConstantesExcel.OBS_CAMPO_NUMERICO;

						return salida;
					}
					break;
				}
				case ConstantesExcel.CAMPO_TEXTO: {
					if (!Util.isString(valor)) {
						salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
								+ ConstantesExcel.OBS_CAMPO_TEXTO;
						return salida;
					}
					break;

				}
				case ConstantesExcel.CAMPO_ANIO:{
					if (!Util.isAnioMes(valor,ConstantesExcel.CAMPO_ANIO)) {
						salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
								+ ConstantesExcel.OBS_CAMPO_NUMERICO + " Del "+ConstantesExcel.MINIMO_AÑO_MES+" a "+ConstantesExcel.MAX_AÑO;
						
						return salida;
					}
					break;
				}
				case ConstantesExcel.CAMPO_MES:{
					if (!Util.isAnioMes(valor, ConstantesExcel.CAMPO_MES)) {
						salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
								+ ConstantesExcel.OBS_CAMPO_NUMERICO + " Del "+ConstantesExcel.MINIMO_AÑO_MES+" a "+ConstantesExcel.MAX_MES;
						
						return salida;
					}
					break;
				}

			}

		}

		if (!Util.isEmpty(valor) && !Util.isEmpty(carga.getLongitud())) {
			if(!ParametrosUtil.validarMaxNroCaracteres(valor, carga.getLongitud())) {
				salida = "ERROR:Fila " + fila + ";columna "
						+ carga.getNombreColumna() + ";" + ConstantesExcel.OBS_CAMPO_LONGITUD_TEXTO + carga.getLongitud().toString();
				return salida;
			}
		}

		if(!Util.isEmpty(valor) && !Util.isEmpty(carga.getPatron())) {
			if(carga.getPatron().equalsIgnoreCase(ConstantesExcel.MARCA)) {
				if(!valor.equalsIgnoreCase(carga.getPatron())) {
					salida = "ERROR:Fila " + fila + ";columna "
							+ carga.getNombreColumna() + ";" + ConstantesExcel.OBS_CAMPO_MARCA;

					return salida;
				}
			}
		}

		salida = ConstantesExcel.OK;

		return salida;

	}

	public boolean validaUltimaFila(XSSFRow xssfFilaExcel,XSSFFormulaEvaluator xssFormulaEvaluator) {
		Cell celda = null;
		CellValue cellValue = null;
		int tipo = Cell.CELL_TYPE_BLANK;
		boolean ultimo = false;

		if (xssfFilaExcel != null) {
			celda = xssfFilaExcel.getCell(0);
			cellValue = xssFormulaEvaluator.evaluate(celda);
		}

		if (cellValue != null) {
			tipo = cellValue.getCellType();
		}

		if(tipo == Cell.CELL_TYPE_BLANK) {
			ultimo = true;
		}

		return ultimo;
	}

	public boolean validaMarca(String pattern, String valor) {
		return pattern.equalsIgnoreCase(valor);
	}
	

	public String validaCampoComas(String valor,int tamaño, CargaMasiva carga, int fila) {
		String salida = "";
		String campo = valor.trim();
		String regex = "^[0-9,]+$";
		
		if (carga.getValida().equals(ConstantesExcel.OBLIGATORIO) && Util.isEmpty(valor)) {
			salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
					+ ConstantesExcel.OBS_CAMPO_NULO;
			return salida;
		}
		
	   if(!Util.isEmpty(carga.getPatron())) {
		   if(!Util.validarCaracteres(regex, campo)) {
				salida = "ERROR:Fila " + fila + ";columna "
						+ carga.getNombreColumna() + ";" + ConstantesExcel.OBS_NUMERICO_COMA;
				return salida;
			}
	   }	
		
		
		campo = campo.substring(campo.length() - 1, campo.length()).equals(",")
				? campo.substring(0, campo.length() - 1)
				: campo;
		List<String> lst = Util.obtenerString(campo);
		if(lst.size() != tamaño) {
			salida = "ERROR:Fila " + fila + ";columna "
					+ carga.getNombreColumna() + ";" + ConstantesExcel.OBS_TAMANO_CAMPO_COMA + obtenerCampoAsociado(carga.getNombreColumna());
			return salida;
		}
		
		salida = ConstantesExcel.OK;
		
		return salida;
	}

	public String validarRequisitoDescripcion(String requisito,String descripcion,CargaMasiva carga, int fila) {
        List<String> requisitos = Arrays.asList(requisito.split(",", -1));
        List<String> descripciones = Arrays.asList(descripcion.split(",", -1));
        String salida="";

        if (requisitos.size() == descripciones.size() ) {
        	salida = descripcion;
		}else {
			salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
					+ ConstantesExcel.OBS_REQUISITOS;
		}
		

		return salida;
	}
	
	public String obtenerValorCeldaDependiente(HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila, CargaMasiva carga ,  HSSFFormulaEvaluator hssFormulaEvaluator ,
			 XSSFFormulaEvaluator xssFormulaEvaluator, String seleccion) throws Exception {
		
		String salida = "";
		if(carga.getNombreColumna().equalsIgnoreCase(ConstantesExcel.MAESTRIA_DESCRIPCION) ||
				carga.getNombreColumna().equalsIgnoreCase(ConstantesExcel.DESCRIPCION_DOCTORADO)) {
			
		}else {
			if(!seleccion.equalsIgnoreCase(ConstantesExcel.MARCA)) {
				seleccion = "";
			}
		}
		
		salida = getValor(hssfFilaExcel, xssfFilaExcel, hssFormulaEvaluator, xssFormulaEvaluator, carga);
		
		if(!Util.isEmpty(seleccion) && Util.isEmpty(salida) ) {
			salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
					+ ConstantesExcel.OBS_CAMPO_NULO_DEPENDIENTE + obtenerCampoAsociado(carga.getNombreColumna());
			return salida;
		}
		
		if(carga.getNombreColumna().equalsIgnoreCase(ConstantesExcel.MAESTRIA_DESCRIPCION) ||
				carga.getNombreColumna().equalsIgnoreCase(ConstantesExcel.DESCRIPCION_DOCTORADO)) {
			if(Util.isEmpty(seleccion) && !Util.isEmpty(salida) ) {
				salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
						+ ConstantesExcel.OBS_CAMPO_SITUACION_DEPENDIENTE + obtenerCampoAsociado(carga.getNombreColumna());
				
				return salida;
			}
		}
		
		if(Util.isEmpty(seleccion) && !Util.isEmpty(salida) ) {
			salida = "ERROR:Fila " + fila + ";columna " + carga.getNombreColumna() + ";"
					+ ConstantesExcel.OBS_CAMPO_MARCA_DEPENDIENTE + obtenerCampoAsociado(carga.getNombreColumna());
			
			return salida;
		}
		return salida;
		
	}
	
	public String obtenerValorCeldaComas(HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila, CargaMasiva carga ,  HSSFFormulaEvaluator hssFormulaEvaluator ,
			 XSSFFormulaEvaluator xssFormulaEvaluator, int tamaño) {
		
		String salida = "";
		String valida = "";
		
		salida = getValor(hssfFilaExcel, xssfFilaExcel, hssFormulaEvaluator, xssFormulaEvaluator, carga);
	
		valida = validaCampoComas(salida, tamaño, carga, fila);
		if(!valida.equalsIgnoreCase(ConstantesExcel.OK)) {
			salida = valida;
		}
		
		return salida;
	}
	
	public String getValor(HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, HSSFFormulaEvaluator hssFormulaEvaluator ,
			 XSSFFormulaEvaluator xssFormulaEvaluator, CargaMasiva carga) {
		Cell celda = null;
		CellValue cellValue = null;
		String salida = "";
		int columna = carga.getPosicion();
		int tipo = Cell.CELL_TYPE_BLANK;
		boolean obs = true;

		if (hssfFilaExcel != null) {
			celda = hssfFilaExcel.getCell(columna);
			cellValue = hssFormulaEvaluator.evaluate(celda);
		} else if (xssfFilaExcel != null) {
			celda = xssfFilaExcel.getCell(columna);
			cellValue = xssFormulaEvaluator.evaluate(celda);
		}

		if (cellValue != null) {
			tipo = cellValue.getCellType();
		}

		switch (tipo) {
			case Cell.CELL_TYPE_BLANK: {
				salida = "";
				break;
			}
			case Cell.CELL_TYPE_BOOLEAN: {
				salida = String.valueOf(cellValue.getBooleanValue());
				break;
			}
			case Cell.CELL_TYPE_NUMERIC: {
				Double value = cellValue.getNumberValue();
				salida = String.valueOf(value.intValue());
				break;

			}
			case Cell.CELL_TYPE_STRING: {
				salida = cellValue.getStringValue().toString();
				break;
			}
		}
		return salida;
	}
	
	public String obtenerCampoAsociado(String campo) {
		switch (campo) {
			case ConstantesExcel.SITUACION_NIVEL_PRIMARIA: {
				campo = ConstantesExcel.PRIMARIA;
				break;
			}
			case ConstantesExcel.SITUACION_NIVEL_SECUNDARIA: {
				campo = ConstantesExcel.SECUNDARIA;
				break;
			}
			case ConstantesExcel.SITUACION_TECNICA_BASICA: {
				campo = ConstantesExcel.TECNICA_BASICA;
				break;
			}
			case ConstantesExcel.ESTUDIOS_REQUERIDOS_TBASICA_EGRE: {
				campo = ConstantesExcel.GRAD_EDUC_TECNICA_BASICA_EGRESADO;
				break;
			}
			case ConstantesExcel.ESTU_REQ_TEC_BASICA_TITU: {
				campo = ConstantesExcel.GRAD_EDUC_TEC_BASICA_TITU_LICENCIATURA;
				break;
			}
			case ConstantesExcel.SITUACION_TECNICA_SUPERIOR: {
				campo = ConstantesExcel.TECNICA_SUPERIOR;
				break;
			}
			case ConstantesExcel.ESTU_REQ_TEC_SUP_EGRESADO: {
				campo = ConstantesExcel.GRAD_EDUC_TEC_SUP_EGRESADO;
				break;
			}
			case ConstantesExcel.EST_REQ_TEC_SUP_TITU: {
				campo = ConstantesExcel.GRAD_EDUC_TEC_SUP_TITU_LIC;
				break;
			}
			case ConstantesExcel.SITUACION_UNIVERSITARIA: {
				campo = ConstantesExcel.UNIVERSITARIA;
				break;
			}
			case ConstantesExcel.EST_REQ_EDU_SUPERIOR: {
				campo = ConstantesExcel.GRAD_EDUC_SUPE_EGRESADO;
				break;
			}
			case ConstantesExcel.EST_REQ_SUP_BACHILLER: {
				campo = ConstantesExcel.GRAD_EDUC_SUP_BACHILLER;
				break;
			}
			case ConstantesExcel.EST_REQ_SUP_TITU_LICENCIATURA: {
				campo = ConstantesExcel.GRAD_EDUC_SUP_TIT_LICENCIATURA;
				break;
			}	
			case ConstantesExcel.EST_REQ_MAESTRIA: {
				campo = ConstantesExcel.GRAD_EDUC_SUP_MAESTRIA;
				break;
			}	
			case ConstantesExcel.EST_REQ_DOCTORADO: {
				campo = ConstantesExcel.GRAD_EDUC_SUP_DOCTORADO;
				break;
			}
			case ConstantesExcel.MAESTRIA_DESCRIPCION: {
				campo = ConstantesExcel.MAESTRIA_SITUACION;
				break;
			}
			case ConstantesExcel.DESCRIPCION_DOCTORADO: {
				campo = ConstantesExcel.DOCTORADO_SITUACION;
				break;
			}
			case ConstantesExcel.HORAS_CURSOS: {
				campo = ConstantesExcel.CURSOS;
				break;
			}
			case ConstantesExcel.HORAS_PROGRAMAS: {
				campo = ConstantesExcel.PROGRAMAS;
				break;
			}
			case ConstantesExcel.HORAS_CONO_OFIMATICA: {
				campo = ConstantesExcel.CONOCIMIENTOS_OFIMATICA;
				break;
			}
			case ConstantesExcel.NIVEL_IDIOMA: {
				campo = ConstantesExcel.IDIOMAS;
				break;
			}				
	        default:
	            break;
		}
		return campo;
	}
	
	public String getNivelDominio (String nivelDominioId) {
		
		 String valor="";
		
		 switch(nivelDominioId) {
		 case "BASICO":
			 valor="1";
			 break;
		 case "INTERMEDIO" :
			 valor="2";
			 break;
		 case "AVANZADO":
			 valor="3";
			 break;
		default:
			valor="No Aplica";
			 break;
			 }
		 
		 return valor;
	}
	

}
