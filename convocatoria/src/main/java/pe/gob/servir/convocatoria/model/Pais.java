package pe.gob.servir.convocatoria.model;

import lombok.Data;

@Data
public class Pais {
    private Integer paisId;
    private String nombrePais;
    private String nacionalidad;
    private String codSunat;
    private String estadoRegistro;
}
