package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "tbl_mae_detalle_entidad", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
public class MaestraDetalleEntidad extends AuditEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mae_detalle_entidad")
	@SequenceGenerator(name = "seq_mae_detalle_entidad", sequenceName = "seq_mae_detalle_entidad", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "mae_detalle_entidad_id")
	private Long maeDetalleEntidadId;

	@Column(name = "entidad_id")
	private Long entidadId;
	
	@Column(name = "mae_cabecera_id")
	private Long maeCabeceraId;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "descripcion_corta")
	private String descripcionCorta;
	
	@Column(name = "abreviatura")
	private String sigla;
	
	@Column(name = "referencia")
	private String referencia;
	
	@Column(name = "orden")
	private Long orden;
	
	@Transient
	private String estado;
	
	@Transient
	private Long configuracionId;
	
	@Transient 
	String estadoConfiguracion;
	
}
