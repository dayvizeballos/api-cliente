package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.gob.servir.convocatoria.model.CargaMasiva;

import java.util.List;

public interface CargaMasivaRepositoryRepository extends JpaRepository<CargaMasiva, Long>{
   List< CargaMasiva> findByRegimen(String regimen );

}
