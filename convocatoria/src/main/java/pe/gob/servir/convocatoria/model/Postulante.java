package pe.gob.servir.convocatoria.model;

import lombok.Data;

import java.util.Date;

@Data
public class Postulante {
    private Long idPostulante;
    private String nombre;
    private String apellidos;
    private String nroDocumentos;

    /*rch*/
    private String idPais;
    private Date fechaNacimiento;
    private Integer tipoDoc;
    private String domicilio;
    private Long postulanteSelId;

}
