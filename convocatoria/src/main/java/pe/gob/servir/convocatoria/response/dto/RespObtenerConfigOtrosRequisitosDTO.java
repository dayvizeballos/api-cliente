package pe.gob.servir.convocatoria.response.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RespObtenerConfigOtrosRequisitosDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long confPerfOtrosRequisitosId;
    private Long perfilId;
    private String  descripcion;
    private boolean  requisitosAdicionales;
    private Long  experienciaDetalleId;
    private Long  tipoDatosExperId;
    private Long  requisitoId;
    private Long  perfilExperienciaId;
}
