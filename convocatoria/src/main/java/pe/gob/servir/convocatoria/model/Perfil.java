package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;
import pe.gob.servir.convocatoria.common.Constantes;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tbl_perfil", schema = "sch_convocatoria")
@Getter
@Setter
public class Perfil extends AuditEntity implements AuditableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil")
    @SequenceGenerator(name = "seq_perfil", sequenceName = "seq_perfil", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "perfil_id")
    private Long perfilId;

    @Column(name = "entidad_id")
    private Long entidadId;

    @Column(name = "regimen_laboral_id")
    private Long regimenLaboralId;

    @Column(name = "organo_id")
    private Long organoId;

    @Column(name = "unidad_organica_id")
    private Long unidadOrganicaId;

    @Column(name = "unidad_funcional_id")
    private Long unidadFuncionalId;

    @Column(name = "unidad_funcional")
    private String unidadFuncional;

    @Column(name = "nivel_organizacional_id")
    private Long nivelOrganizacionalId;

    @Column(name = "nivel_organizacional")
    private String nivelOrganizacional;

    @Column(name = "servidor_civil_id")
    private Long servidorCivilId;

    @Column(name = "familia_puesto_id")
    private Long familiaPuestoId;

    @Column(name = "rol_id")
    private Long rolId;

    @Column(name = "nivel_categoria_id")
    private Long nivelCategoriaId;

    @Column(name = "puesto_tipo_id")
    private Long puestoTipoId;

    @Column(name = "subnivel_subcategoria")
    private String subNivelsubCategoria;

    @Column(name = "dependencia_funcional")
    private String dependenciaFuncional;

    @Column(name = "dependencia_jerarquica")
    private String dependenciaJerarquica;

    @Column(name = "servidor_civil_reporte_id")
    private Long servidorCivilReporteId;

    @Column(name = "nro_posiciones_a_cargo")
    private Integer nroPosicionesCargo;

    @Column(name = "puesto_codigo")
    private String puestoCodigo;

    @Column(name = "nombre_puesto")
    private String nombrePuesto;

    @Column(name = "mision_puesto")
    private String misionPuesto;

    @Column(name = "nro_posiciones_puesto")
    private Integer nroPosicionesPuesto;

    @Column(name = "puestos_a_cargo")
    private String puestosCargo;

    @Column(name = "tipo_practica_id")
    private Long tipoPracticaId;

    @Column(name = "condicion_practica_id")
    private Long condicionPracticaId;

    @Column(name = "ind_colegiatura")
    private String indColegiatura;

    @Column(name = "ind_habilitacion_prof")
    private String indHabilitacionProf;

    @Column(name = "nombre_organo")
    private String nombreOrgano;

    @Column(name = "unidad_organica")
    private String unidadOrganica;

    @Column(name = "codigo_posicion")
    private String codigoPosicion;

    @Column(name = "sigla_organo")
    private String siglaOrgano;

    @Column(name = "puesto_estructural")
    private String puestoEstructural;

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "estado_rm")
    private String estadoRm;

    @Column(name = "estado_ec")
    private String estadoEc;
    
    @Column(name = "ind_origen_perfil")
    private String indOrigenPerfil;
    
    @Column(name = "ind_estado_revision")
    private Long indEstadoRevision;
    
    @Column(name = "estado_func")
    private String estadoFunc = Constantes.INACTIVO;
    
    @Column(name = "estado_forma")
    private String estadoForm = Constantes.INACTIVO;
    
    @Column(name = "estado_exp")
    private String estadoExp = Constantes.INACTIVO;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "perfil")
    @Column(nullable = true)
    @JsonManagedReference
    private List<Conocimiento> conocimientoList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "perfil")
    @Column(nullable = true)
    @JsonManagedReference
    private List<FormacionAcademica> formacionAcademicaList = new ArrayList<>();

    public Perfil() {
    }
    
    public Perfil(Long perfilId) {
    	this.perfilId = perfilId;
    }
}
