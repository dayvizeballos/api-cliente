package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@Getter
@Setter
public class ReqActualizarBaseEvaluacion {

	@NotNull(message = Constantes.CAMPO + " baseId " + Constantes.ES_OBLIGATORIO)
	private Long baseId;
	
	@NotNull(message = Constantes.CAMPO + " informeDetalleId " + Constantes.ES_OBLIGATORIO)
	private Long informeDetalleId;
	
	private String observacion;
	
	@NotNull(message = Constantes.CAMPO + " estadoRegistro " + Constantes.ES_OBLIGATORIO)
	private boolean estadoRegistro;
	
	@NotNull(message = Constantes.CAMPO + " estadoCondicion " + Constantes.ES_OBLIGATORIO)
	private boolean estadoCondicion;
	
	private String comentarioCondicion;
	
}
