package pe.gob.servir.convocatoria.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cronograma {
	private Integer basecronogramaId;
	private Integer etapaId;
	private String descripcionetapa;
	private String descripcion;
	private String responsable;
	private Date periodoini;
	private Date periodofin;
	private String horaIni;
	private String horaFin;
	private String codProg;
}
