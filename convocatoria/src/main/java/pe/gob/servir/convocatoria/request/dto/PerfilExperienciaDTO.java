package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;


import java.util.List;

@JGlobalMap
@Getter
@Setter
public class PerfilExperienciaDTO {

    private Long  id;

    private Long  anioExpTotal;

    private Long  perfilId;


    private Long  mesExpTotal;


    private Long  anioExReqPuesto;


    private Long  mesExReqPuesto;


    private Long  anioExpSecPub;


    private Long  mesExpSecPub;


    private Long  nivelMinPueId;


    private String aspectos;

    private String estado;


    private List<PerfilExperienciaDetalleDTO> detalle;
    private List<PerfilExperienciaDetalleDTO> detalle2;


}
