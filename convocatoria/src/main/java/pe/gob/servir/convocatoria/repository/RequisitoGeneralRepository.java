package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.RequisitoGeneral;

import java.util.Optional;

public interface RequisitoGeneralRepository  extends JpaRepository<RequisitoGeneral , Long> {
    @Query("SELECT  b.requisitoId FROM  RequisitoGeneral b WHERE b.requisitoId = :requisitoId ")
    public Optional<RequisitoGeneral> findByidReq(@Param("requisitoId")Long requisitoId);
    
    @Query("SELECT r FROM RequisitoGeneral r WHERE r.base = :base")
    public RequisitoGeneral findbyBase(@Param("base")Base base);

}
