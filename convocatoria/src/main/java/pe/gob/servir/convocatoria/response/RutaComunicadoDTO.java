package pe.gob.servir.convocatoria.response;

import lombok.Data;

import java.util.Date;

@Data
public class RutaComunicadoDTO {
    String ruta;
    Date fecha;
}
