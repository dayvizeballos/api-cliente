package pe.gob.servir.convocatoria.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.repository.RequisitoGeneralRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.DeclaracionJuradaDTO;
import pe.gob.servir.convocatoria.request.dto.RequisitoGeneralDTO;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RequisitoGeneralRepositoryJdbcImpl implements RequisitoGeneralRepositoryJdbc {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RequisitoGeneralRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

   private static final String SQL_FIND_ALL_REQUISITO = "select p.req_general_id as requisitoId , p.base_id as baseId ,\n" +
           " p.cuestionario_decl_jur as cuestionario  , p.descripcion_req_gen as descripcion , p.estado_registro as estado , p.informe_detalle_id as informeDetalleId\n" +
           " from sch_base.tbl_base_req_general P where p.base_id = :baseId and p.estado_registro = '1' ";

    private static final String SQL_FIND_ALL_DECLARACION_JURADA = "select p.base_id as idBase , p.isservir as isServir , p.estado_registro as estado , p.tipo_declaracion_id as tipoId , p.decla_jurada_id as declaracionId , p.orden as orden, \n" +
            "case when p.isservir = '1' then (select tmd.descripcion \n" +
            "from sch_convocatoria.tbl_mae_detalle tmd where tmd.mae_detalle_id = p.tipo_declaracion_id) \n" +
            "else (select tmde.descripcion from sch_convocatoria.tbl_mae_detalle_entidad tmde where tmde.mae_detalle_entidad_id = p.tipo_declaracion_id)  end as descripcion\n" +
            "from  sch_base.tbl_base_decla_jurada p where p.estado_registro = '1' and p.base_id = :idBase order by p.orden asc";

    @Override
    public List<RequisitoGeneralDTO> lisGeneralDtoList(Long baseId , Long requisitoId) {
        List<RequisitoGeneralDTO> list = new ArrayList<RequisitoGeneralDTO>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<String, Object>();
            sql.append(SQL_FIND_ALL_REQUISITO);

            if (!Util.isEmpty(requisitoId))sql.append(" and p.req_general_id  = :requisitoId ");
            sql.append(" order by p.req_general_id desc ");

            if (!Util.isEmpty(baseId)) objectParam.put("baseId", baseId);
            if (!Util.isEmpty(requisitoId)) objectParam.put("requisitoId", requisitoId);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(RequisitoGeneralDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<DeclaracionJuradaDTO> lisDeclaracionJuradaDtos(Long idBase) {
        List<DeclaracionJuradaDTO> list = new ArrayList<DeclaracionJuradaDTO>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<String, Object>();
            sql.append(SQL_FIND_ALL_DECLARACION_JURADA);
            if (!Util.isEmpty(idBase)) objectParam.put("idBase", idBase);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(DeclaracionJuradaDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }
}
