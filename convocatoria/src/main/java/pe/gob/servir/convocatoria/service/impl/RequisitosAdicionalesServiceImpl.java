package pe.gob.servir.convocatoria.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.EvaluacionEntidad;
import pe.gob.servir.convocatoria.model.Perfil;
import pe.gob.servir.convocatoria.model.TblConfPerfOtrosRequisitos;
import pe.gob.servir.convocatoria.repository.TblConfPerfOtrosRequisitosRepository;
import pe.gob.servir.convocatoria.request.dto.ConfiguracionPerfilDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilExperienciaDetalleDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespConfigOtrosRequisitos;
import pe.gob.servir.convocatoria.response.dto.RespObtenerConfigOtrosRequisitosDTO;
import pe.gob.servir.convocatoria.response.dto.RespObtenerRequisitosAdicionalesDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.PerfilExperienciaService;
import pe.gob.servir.convocatoria.service.RequisitosAdicionalesService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class RequisitosAdicionalesServiceImpl implements RequisitosAdicionalesService {

	@Autowired
	private PerfilExperienciaService perfilExperienciaService;

	@Autowired
	private TblConfPerfOtrosRequisitosRepository tblConfPerfOtrosRequisitosRepository;

	@Override
	public RespBase<RespObtenerRequisitosAdicionalesDTO> listarRequisitosAdicionalesByPerfilId(Long perfilId, Long baseId) {

		RespObtenerRequisitosAdicionalesDTO dto;
		RespBase<RespObtenerRequisitosAdicionalesDTO> response = new RespBase<>();
		RespBase<PerfilExperienciaDTO> perfilExp = perfilExperienciaService.findPerfilExpereincia(perfilId);
		boolean validar = Objects.nonNull(perfilExp) && perfilExp.getStatus().getSuccess();
		if (validar) {
			List<RespObtenerRequisitosAdicionalesDTO.RequisitoAdicionalDTO> listaReqAdic = new ArrayList<>();
			dto = new RespObtenerRequisitosAdicionalesDTO();
			for (PerfilExperienciaDetalleDTO detalle2: perfilExp.getPayload().getDetalle2()) {

				TblConfPerfOtrosRequisitos filtro = new TblConfPerfOtrosRequisitos();
				filtro.setPerfil(new Perfil(perfilId));
				filtro.setBaseId(baseId);
				filtro.setExperienciaDetalleId(detalle2.getId());
				filtro.setTipoDatoExperId(detalle2.getTiDaExId());
				filtro.setRequisitoId(detalle2.getRequisitoId());
				filtro.setDescripcion(detalle2.getDescripcion());
				filtro.setPerfilExperienciaId(detalle2.getPerfilExperienciaId());
				filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
				Example<TblConfPerfOtrosRequisitos> example = Example.of(filtro);

				List<TblConfPerfOtrosRequisitos> confPerfOtroReq = this.tblConfPerfOtrosRequisitosRepository.findAll(example);
				RespObtenerRequisitosAdicionalesDTO.RequisitoAdicionalDTO reqAdicDto = new RespObtenerRequisitosAdicionalesDTO.RequisitoAdicionalDTO();
				if (!confPerfOtroReq.isEmpty()) {
					TblConfPerfOtrosRequisitos tblConfPerfOtrReq = confPerfOtroReq.get(0);
					reqAdicDto.setConfPerfOtrosRequisitosId(tblConfPerfOtrReq.getConfPerfOtrosRequisitosId());
					reqAdicDto.setRequisitoMinimo(tblConfPerfOtrReq.getRequisitosAdicionales());
					reqAdicDto.setExperienciaDetalleId(detalle2.getId());
					reqAdicDto.setPerfilExperienciaId(detalle2.getPerfilExperienciaId());
					reqAdicDto.setDescripcion(detalle2.getDescripcion());
					reqAdicDto.setTipoDatoExperId(detalle2.getTiDaExId());
					reqAdicDto.setRequisitoId(detalle2.getRequisitoId());
					reqAdicDto.setBaseId(baseId);
					listaReqAdic.add(reqAdicDto);
				}				
			}
			dto.setPerfilId(perfilId);
			dto.setListaRequisitosAdicionales(listaReqAdic);

		} else {
			return ParametrosUtil.setearResponse(response, Boolean.FALSE, "Id perfil no existe.");
		}

		return new RespBase<RespObtenerRequisitosAdicionalesDTO>().ok(dto);
	}

	@Override
	public RespBase<List<RespConfigOtrosRequisitos>> listarConfiOtrosRequisitosByPerfil(Long perfilId) {
		Perfil perfil = new Perfil(perfilId);

		TblConfPerfOtrosRequisitos filterEvaluacion = new TblConfPerfOtrosRequisitos();
		List<RespConfigOtrosRequisitos> lstconfig = new ArrayList<>();
		filterEvaluacion.setPerfil(perfil);
		filterEvaluacion.setPerfil(perfil);
		Example<TblConfPerfOtrosRequisitos> example = Example.of(filterEvaluacion);
		List<TblConfPerfOtrosRequisitos> listconfPerfOtroReq = this.tblConfPerfOtrosRequisitosRepository
				.findAll(example);
		for (TblConfPerfOtrosRequisitos tblConfPerfOtrosRequisitos : listconfPerfOtroReq) {
			RespConfigOtrosRequisitos obje = new RespConfigOtrosRequisitos();
			obje.setDescripcion(tblConfPerfOtrosRequisitos.getDescripcion());
			obje.setConfPerfOtrosRequisitosId(tblConfPerfOtrosRequisitos.getConfPerfOtrosRequisitosId());
			obje.setPerfilExperienciaId(tblConfPerfOtrosRequisitos.getPerfilExperienciaId());
			obje.setRequisitosAdicionales(tblConfPerfOtrosRequisitos.getRequisitosAdicionales());

			lstconfig.add(obje);
		}
		return new RespBase<List<RespConfigOtrosRequisitos>>().ok(lstconfig);
	}

}
