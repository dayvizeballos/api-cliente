package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_bonificacion_detalle", schema = "sch_base")
@Getter
@Setter
public class BonificacionDetalle  extends AuditEntity implements AuditableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_bonificacion_detalle")
    @SequenceGenerator(name = "seq_bonificacion_detalle", sequenceName = "seq_bonificacion_detalle", schema = "sch_base", allocationSize = 1)

    @Column(name = "bonificacion_detalle_id")
    private Long bonificacionDetalleId;
    
    @Column(name = "descripcion")
    private String descripcion;
    
    @Column(name = "nivel_id")
    private Long nivelId;
    
    @Column(name = "aplica_id")
    private Long aplicaId;
    
    @Column(name = "porcentaje_bono")
    private Long porcentajeBono;
    
    @JoinColumn(name="bonificacion_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Bonificacion bonificacion;


}
