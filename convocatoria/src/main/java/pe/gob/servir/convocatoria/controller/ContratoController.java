package pe.gob.servir.convocatoria.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqComunicado;
import pe.gob.servir.convocatoria.request.ReqContrato;
import pe.gob.servir.convocatoria.request.ReqConvenio;
import pe.gob.servir.convocatoria.request.ReqDesestimaContrato;
import pe.gob.servir.convocatoria.request.ReqGeneraContrato;
import pe.gob.servir.convocatoria.request.ReqSuscribirContrato;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.ContratoService;
import pe.gob.servir.convocatoria.service.ConvocatoriaService;

import java.util.Base64;
import java.util.Date;

@RestController
@Tag(name = "Contrato", description = "")
public class ContratoController {
	
	@Autowired
    private HttpServletRequest httpServletRequest;

	@Autowired
	ContratoService contratoService;
	
	
    @Operation(summary = "Crea Contrato / Convenio", description = "Crea Contrato / Convenio", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PostMapping(path = {Constantes.BASE_ENDPOINT + "/contrato"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RespBase<Object>> registrarContrato(@PathVariable String access, 
			@Valid @RequestBody ReqBase<ReqGeneraContrato> request){
        	MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        	RespBase<Object> response = contratoService.registrarContrato(request, jwt);
        return ResponseEntity.ok(response);

    }
    
    @Operation(summary = "Actualizar un contrato", description = "Actualizar un contrato ", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/contrato/{contratoId}"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespToContratoConvenio>> actualizarContrato(@PathVariable String access,
    		@PathVariable Long contratoId, @Valid @RequestBody ReqBase<ReqContrato> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<RespToContratoConvenio> resp = contratoService.actualizaContrato(request, jwt, contratoId);
        return ResponseEntity.ok(resp);
 
    }

    @Operation(summary = "Actualizar un convenio", description = "Actualizar un convenio ", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/convenio/{convenioId}"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespToContratoConvenio>> actualizaConvenio(@PathVariable String access,
    		@PathVariable Long convenioId, @Valid @RequestBody ReqBase<ReqConvenio> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<RespToContratoConvenio> resp = contratoService.actualizaConvenio(request, jwt, convenioId);
        return ResponseEntity.ok(resp);
 
    }    
    
    
    @Operation(summary = Constantes.SUM_OBT_LIST + "get contrato/convenio", description = Constantes.SUM_OBT_LIST + "por id contrato", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/contratoconvenios"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespToContratoConvenio>> findContratoConvenio(
            @PathVariable String access,
            @RequestParam(required = false) Long contratoId) {
        RespBase<RespToContratoConvenio>response =  contratoService.getContratoConvenio(contratoId);
        return ResponseEntity.ok(response);
    }

    @Operation(summary = Constantes.SUM_OBT_LIST + "get contrato/convenio bandeja", description = Constantes.SUM_OBT_LIST + "bandeja por filtros", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/contratoconvenios/filtros/{entidad}/{tipo}"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespObtieneLista<RespContratoConvenio>>> findContratoConvenioFiltros(
            @PathVariable String access,
            @PathVariable Long tipo,
            @PathVariable Long entidad,
            @RequestParam(required = false) Long regimen,
            @RequestParam(required = false) Long tipoPractica,
            @RequestParam(required = false) Long perfil,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaIni,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaFin,
            @RequestParam(required = false) Long estado,
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "10") int size

            ) {
        RespBase<RespObtieneLista<RespContratoConvenio>> response = contratoService.findContratoaConveniosFiltro(tipoPractica , tipo ,entidad ,regimen , perfil ,
                fechaIni , fechaFin , estado , page , size);
        return ResponseEntity.ok(response);
    }


    @Operation(summary = "Desestimar Contrato", description = "Desestimar Contrato ", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/contrato/desestimar/{contratoId}"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<Object>> desestimarContrato(@PathVariable String access,
    		@PathVariable Long contratoId, @Valid @RequestBody ReqBase<ReqDesestimaContrato> request) {

    	MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
    	RespBase<Object> resp = contratoService.desestimarContrato(request, jwt, contratoId);

        return ResponseEntity.ok(resp);

    }


    @Operation(summary = "Suscribir Contrato", description = "Suscribir Contrato ", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/contrato/suscribir/{contratoId}"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<Object>> suscribirContrato(@PathVariable String access,
    		@PathVariable Long contratoId, @Valid @RequestBody ReqBase<ReqSuscribirContrato> request) {

    	MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
    	RespBase<Object> resp = contratoService.suscribirContrato(request, jwt, contratoId);

        return ResponseEntity.ok(resp);

    }  
    
    @Operation(summary = "Descargar Contrato", description = "Descargar Contrato ", tags = {""},
    		security = { @SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/contrato/download/{contratoId}" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> descargaContrato(@PathVariable String access, @PathVariable Long contratoId) {
    	try {
    		byte[] resultado = contratoService.descargaContrato(contratoId);
    		//String resultado = contratoService.descargaContrato2(contratoId);
        	RespBase<Object> response = new RespBase<>();
    		response.getStatus().setSuccess(Boolean.TRUE);
    		response.setPayload(Base64.getEncoder().encodeToString(resultado));
    		return ResponseEntity.ok(response);
		} catch (Exception e) {
			RespBase<Object> response = new RespBase<>();
			response.getStatus().setSuccess(Boolean.FALSE);
			response.getStatus().getError().getMessages().add(e.getMessage());
			HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
			return ResponseEntity.status(status).body(response);
		}
    	
	}
    
    
    @Operation(summary = Constantes.SUM_OBT_LIST + "Tipos de Contrato", description = Constantes.SUM_OBT_LIST + "Tipos de Contrato", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/contrato/tipo"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespTipoContrato>> findTipoContrato(
            @PathVariable String access,
            @RequestParam(required = false) Long regimen) {
        RespBase<RespTipoContrato> response = contratoService.listaTipoContrato(regimen);
        return ResponseEntity.ok(response);
    }
    
}