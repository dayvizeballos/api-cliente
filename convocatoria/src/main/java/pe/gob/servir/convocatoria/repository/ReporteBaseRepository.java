package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Base;

@Repository
public interface ReporteBaseRepository  extends JpaRepository<Base, Long>{

	 @Query("select b from Base b where  b.baseId=:baseId")
	 public Base findBase(@Param("baseId")Long baseId);
}
