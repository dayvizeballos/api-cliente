package pe.gob.servir.convocatoria.queue.consumer;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

import pe.gob.servir.convocatoria.common.RoutingKey;

@Component
public class RabbitConsumer {

//	private static final Logger logger = LoggerFactory.getLogger(RabbitConsumer.class);
//
//	private static final String QUEUE_NAME = "appuno.queue";
//
//	@RabbitListener(queues = QUEUE_NAME, ackMode = "MANUAL")
//	public void listener(Message message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag,
//			@Header(AmqpHeaders.RECEIVED_ROUTING_KEY) String routingKey) throws IOException {
//		try {
//			String body = new String(message.getBody(), "UTF-8");
//			if (RoutingKey.PRINT.getKey().equals(routingKey)) {
//				logger.info("message.body={} de queue appuno.queue", body);
//				channel.basicAck(deliveryTag, false);
//
//			} else {
//				logger.warn("routingKey={} no implementado", routingKey);
//				channel.basicNack(deliveryTag, false, false);
//			}
//		} catch (Exception e) {
//			channel.basicNack(deliveryTag, false, false);
//		}
//
//	}
}
