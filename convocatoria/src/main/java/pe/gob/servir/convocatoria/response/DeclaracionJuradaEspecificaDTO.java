package pe.gob.servir.convocatoria.response;

import lombok.Data;
import pe.gob.servir.convocatoria.model.DeclaracionJurada;

import java.io.Serializable;

@Data
public class DeclaracionJuradaEspecificaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long declaraJuradaId;
    private Long tipoDeclaracionId;
    private String descripcion;
    private Long baseId;
    private String isServir;

    public DeclaracionJuradaEspecificaDTO(DeclaracionJurada declara) {
        this.declaraJuradaId = declara.getDeclaracionId();
        this.tipoDeclaracionId = declara.getTipoId();
        this.descripcion = declara.getDescripcion();
        this.baseId = declara.getBase().getBaseId();
        this.isServir = declara.getIsServir();
    }
}
