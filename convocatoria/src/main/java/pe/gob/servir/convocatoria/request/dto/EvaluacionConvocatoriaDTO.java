package pe.gob.servir.convocatoria.request.dto;

import java.util.Objects;

import com.google.common.primitives.Longs;

import lombok.Data;
import pe.gob.servir.convocatoria.model.BaseCronogramaActividad;

@Data
public class EvaluacionConvocatoriaDTO {
	
	private Long id;
	private String descripcion;
	
	public EvaluacionConvocatoriaDTO(BaseCronogramaActividad item){
		this.id = Objects.nonNull(item.getTipoActividad())? Longs.tryParse(item.getTipoActividad().getCodProg()) : null;
		this.descripcion = item.getDescripcion();
	}
	
	public EvaluacionConvocatoriaDTO(DetalleEvaluacionEntidadDTO item,Long id){
		this.id = id;
		this.descripcion = item.getDescripcion();
	}
}
