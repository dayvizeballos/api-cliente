package pe.gob.servir.convocatoria.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

public interface GeneraDocumentoService {
	
	public InputStream loadDocumentAsStream(String filePath) throws IOException;
	public IContext cargarVariables(Map<String, Object> variables, IContext context) throws XDocReportException;
	public void cargarImagenes(IXDocReport report, Map<String, String> variables, IContext context);
	public byte[] generarDocumento(String rutaPlantilla, TemplateEngineKind templateEngine,Map<String, Object> variablesMap)
					throws IOException, XDocReportException; 

}
