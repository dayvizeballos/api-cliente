package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class MovimientoDTO {

    /*valores de entrada*/
    private Long movimientoId;

    @NotNull(message = "el campo baseId no puede ser null")
    private Long baseId;

    private Long rolId;

    private Long entidadId;

    @NotNull(message = "el campo estadoOldId no puede ser null")
    private Long estadoOldId;

    @NotNull(message = "el campo estadoNewId no puede ser null")
    private Long estadoNewId;

    private Long coordinadorId;

    private String nombreCoordinador;

    private List<ObservacionDTO> observacionDTOList;


    /*valores de salida*/
    private String userCreacion;
    private String fechaCreacion;

}
