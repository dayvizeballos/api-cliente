package pe.gob.servir.convocatoria.service;

import java.io.IOException;

import com.lowagie.text.DocumentException;

import pe.gob.servir.convocatoria.model.ReportePerfil;

public interface ReportePerfilService {

	
	ReportePerfil findReportePerfil(Long id);
	
	String parseThymeleafTemplate(Long id);
	
	byte[] generatePdfFromHtml(Long id) throws DocumentException, IOException;
	
 
}
