package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JGlobalMap
@Getter
@Setter
public class FormacionAcademicaDTO {
	
	/*@NotNull(message = Constantes.CAMPO + " perfilId " + Constantes.ES_OBLIGATORIO)
	private Long perfilId;
	*/
	
	private Long formacionAcademicaId;
	
	private Long nivelEducativoId;
	
	private Long estadoNivelEducativoId;
	
	private Long situacionAcademicaId;
	
	private Long estadoSituacionAcademicaId;
	
	private String nombreGrado;
	
	private String estado;
	private String nivelEducativoDesc;
	private String sitAcademiDesc;
	
	private List<CarreraFormacionAcademicaDTO> lstCarreraFormaAcademica;
	private List<CarreraFormacionAcademicaOtrosGradosDTO> lstOtrosGrados;
	

	@Override
	public String toString() {
		return "FormacionAcademicaDTO{" +
				"formacionAcademicaId=" + formacionAcademicaId +
				", nivelEducativoId=" + nivelEducativoId +
				", estadoNivelEducativoId=" + estadoNivelEducativoId +
				", situacionAcademicaId=" + situacionAcademicaId +
				", estadoSituacionAcademicaId=" + estadoSituacionAcademicaId +
				", nombreGrado='" + nombreGrado + '\'' +
				", estado='" + estado + '\'' +
				'}';
	}
}
