package pe.gob.servir.convocatoria.request.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class JerarquiaDTO {


	@NotNull(message = "Campo codigoNivel1 es obligatorio")
	@Valid
	private Long codigoNivel1;
	
	@NotNull(message = "Campo codigoNivel2 es obligatorio")
	@Valid
	private Long codigoNivel2;
	
	@NotNull(message = "Campo codigoNivel3 es obligatorio")
	@Valid
	private Long codigoNivel3;

	@NotNull(message = Constantes.CAMPO + " listaEvaluacion " + Constantes.ES_OBLIGATORIO)
	@Valid
	private List<EvaluacionDTO> listaEvaluacion;
}
