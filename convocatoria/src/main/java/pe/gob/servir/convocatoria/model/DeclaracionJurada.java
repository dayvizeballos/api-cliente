package pe.gob.servir.convocatoria.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_base_decla_jurada", schema = "sch_base")
@Getter
@Setter
public class DeclaracionJurada extends AuditEntity implements AuditableEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tbl_base_decla_jurada")
    @SequenceGenerator(name = "seq_tbl_base_decla_jurada", sequenceName = "seq_tbl_base_decla_jurada", schema = "sch_base", allocationSize = 1)
    @Column(name = "decla_jurada_id")
    private Long declaracionId;

    @Column(name = "tipo_declaracion_id")
    private Long tipoId;

    @Column(name = "orden")
    private Long orden;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "isservir")
    private String isServir;

    @JoinColumn(name="base_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Base base;
}
