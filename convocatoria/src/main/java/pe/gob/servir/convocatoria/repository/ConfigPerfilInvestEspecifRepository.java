package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.gob.servir.convocatoria.model.TblConfigPerfilInvestEspecif;

public interface ConfigPerfilInvestEspecifRepository extends JpaRepository<TblConfigPerfilInvestEspecif, Long> {

}
