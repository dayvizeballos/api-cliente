package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tbl_perfil_grupo", schema = "sch_convocatoria")
@Getter
@Setter
public class PerfilGrupo extends AuditEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil_grupo")
    @SequenceGenerator(name = "seq_perfil_grupo", sequenceName = "seq_perfil_grupo", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "perfil_grupo_id")
    private Long id;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "orden")
    private Long orden;

    @Column(name = "perfil_grupo_padre_id")
    private Long padreId;

    @Column(name = "estado_registro")
    private String estado;

    public PerfilGrupo() {
    }
}
