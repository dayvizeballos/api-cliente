package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.InformeDetalleDTO;

@Getter
@Setter
public class RespInformeDetalle {
	
	private InformeDetalleDTO informeDetalle;

}
