package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Primary;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@JGlobalMap
@Getter
@Setter
public class CondicionDto {

    @NotNull(message = "el campo etapa no puede ser null")
    private Long etapa;

    @NotNull(message = "el campo baseId no puede ser null")
    private Long baseId;

    private String comentario;

    @NotNull(message = "el campo condicion no puede ser null")
    @NotEmpty(message = "el campo condicion no puede estar vacio")
    private String condicion;

    private String etapaNombre;
    private Long etapaId;

    @Primary
    public void setComentario(String comentario) {
        try {
            this.comentario = comentario.trim().toUpperCase();
        } catch (NullPointerException e) {

        }

    }
}
