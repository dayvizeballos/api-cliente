package pe.gob.servir.convocatoria.request.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerBonificacionDTO {

	
	private Integer correlativo;
	private Long bonificacionId;
	private Long tipoBonificacion;
	private String titulo;
	private String nombreBonificacion;
	private String estado;
	
}
