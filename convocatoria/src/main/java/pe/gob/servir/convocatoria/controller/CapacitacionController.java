package pe.gob.servir.convocatoria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.model.CarreraProfesional;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.service.CapacitacionService;

@RestController
@Tag(name = "Capacitacion", description = "")
public class CapacitacionController {

	@Autowired
	CapacitacionService capacitacionService;

	@Operation(summary = "Obtiene lista de carreras por Situacion", description = "Obtiene lista de carreras por Situacion", tags = {
			"" }, security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/capacitacion/carrera/{situacionId}" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<CarreraProfesional>>> obtenerCarrerasPorSituacion(
			@PathVariable String access, @PathVariable Long situacionId) {
		RespBase<RespObtieneLista<CarreraProfesional>> response = capacitacionService.obtenerCarreras(situacionId);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = "Obtiene lista de todas las carreras activas", description = "Obtiene lista de todas las carreras activas", tags = {
	"" }, security = { @SecurityRequirement(name = "bearer-key") })
@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "operación exitosa"),
	@ApiResponse(responseCode = "500", description = "error interno", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
@GetMapping(path = { Constantes.BASE_ENDPOINT + "/capacitacion/carrera" }, produces = {
	MediaType.APPLICATION_JSON_VALUE })
public ResponseEntity<RespBase<RespObtieneLista<CarreraProfesional>>> obtenerCarreras(@PathVariable String access) {
RespBase<RespObtieneLista<CarreraProfesional>> response = capacitacionService.listarCarreras();
return ResponseEntity.ok(response);
}

}
