package pe.gob.servir.convocatoria.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Evaluacion;
import pe.gob.servir.convocatoria.model.EvaluacionEntidad;

@Repository
public interface EvaluacionEntidadRepository extends JpaRepository<EvaluacionEntidad, Long>{

	@Query("select ev from Jerarquia je \r\n" + 
			"inner join Evaluacion ev on ev.jerarquiaId = je.jerarquiaId  \r\n" + 
			"where 1 = 1 and ev.estadoRegistro='1' and  je.estadoRegistro='1' and  \r\n" + 
			"je.codigoNivel1=:idRegimen and je.codigoNivel2=:idModalidad and je.codigoNivel3=:idTipo")
	public List<Evaluacion> findEvaluacion(@Param("idRegimen")Long idRegimen, @Param("idModalidad")Long idModalidad, @Param("idTipo")Long idTipo);
	
	@Query("select  distinct ev from EvaluacionEntidad ev \r\n"+
			"where 1 = 1 and ev.estadoRegistro='1' and ev.entidadId=:idEntidad and \r\n" +
			"ev.evaluacionOrigenId = :idEvaluacionOrigen")
	public Optional<EvaluacionEntidad> findEvaluacionByIdOrigen(@Param("idEntidad")Long idEntidad, @Param("idEvaluacionOrigen")Long idEvaluacionOrigen);
}
