package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;

@JGlobalMap
@Getter
@Setter
public class DeclaracionJuradaDTO {
    private Long declaracionId;
    private Long tipoId;
    private Long orden;
    private String descripcion;
    private String estado;
    private String isServir;
    private Long idBase;
}
