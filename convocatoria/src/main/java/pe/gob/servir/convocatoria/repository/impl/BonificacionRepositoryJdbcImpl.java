package pe.gob.servir.convocatoria.repository.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import pe.gob.servir.convocatoria.repository.BonificacionRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.BonificacionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.ObtenerBonificacionDTO;
import pe.gob.servir.convocatoria.util.Util;

@Repository
public class BonificacionRepositoryJdbcImpl implements BonificacionRepositoryJdbc{

	@Autowired
    JdbcTemplate jdbcTemplate;
    
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
    private static final  String SQL_FIND_BONI_FILTER ="select b.bonificacion_id as bonificacionId, b.tipo_bonificacion as tipoBonificacion,\r\n"
    		+ "	sch_convocatoria.fnc_get_desc_mae_detalle(b.tipo_bonificacion) as nombreBonificacion, \r\n"
    		+ "	b.titulo as titulo, b.estado_registro as estado\r\n"
    		+ "from sch_base.tbl_bonificacion b where 1 = 1";
    
    private static final  String SQL_GET_BONIFICACION ="select b.bonificacion_id  as bonificacionId,  b.tipo_bonificacion as tipoBonificacion,\r\n"
    		+ "	sch_convocatoria.fnc_get_desc_mae_detalle(b.tipo_bonificacion) as bonificacionNombre,b.contenido as contenido,\r\n"
    		+ "	b.titulo as titulo, b.estado_registro as estadoRegistro\r\n"
    		+ "from sch_base.tbl_bonificacion b where b.bonificacion_id= :bonificacionId ";
    
    private static final  String SQL_GET_lST_BONIFICACION_DETALLE ="select bd.bonificacion_detalle_id as bonificacionDetalleId, bd.descripcion as descripcion,\r\n"
    		+ "	bd.nivel_id as nivelId, bd.aplica_id as aplicaId, bd.porcentaje_bono as porcentajeBono,\r\n"
    		+ "	sch_convocatoria.fnc_get_desc_mae_detalle(bd.nivel_id) as nombreNivel,\r\n"
    		+ "	sch_convocatoria.fnc_get_desc_mae_detalle(bd.aplica_id) as nombreAplica,\r\n"
    		+ "	bd.estado_registro as estadoRegistro\r\n"
    		+ "from sch_base.tbl_bonificacion_detalle bd where bd.bonificacion_id = :bonificacionId" ;
    
	@Override
	public List<ObtenerBonificacionDTO> buscarBonificacionByFilter(Map<String, Object> parametroMap) {
		List<ObtenerBonificacionDTO> listaBonificacion = new ArrayList<>();
		
		try {
	         int correlativo = 0;
			 StringBuilder sql = new StringBuilder();
	         Map<String, Object> objectParam = new HashMap<>();
	         sql.append(SQL_FIND_BONI_FILTER);
	         if (!Util.isEmpty(parametroMap.get("tipoBonificacion"))) sql.append(" and b.tipo_bonificacion =:tipoBonificacion ");
	         if (!Util.isEmpty(parametroMap.get("titulo"))) sql.append(" and upper(b.titulo) like upper('%'||:titulo||'%') ");
	         if (!Util.isEmpty(parametroMap.get("estado"))) sql.append(" and b.estado_registro =:estado");
	         sql.append(" order by b.bonificacion_id desc ");
	         
	         if (!Util.isEmpty(parametroMap.get("tipoBonificacion"))) objectParam.put("tipoBonificacion",parametroMap.get("tipoBonificacion"));
	         if (!Util.isEmpty(parametroMap.get("titulo"))) objectParam.put("titulo",parametroMap.get("titulo"));
	         if (!Util.isEmpty(parametroMap.get("estado"))) objectParam.put("estado",parametroMap.get("estado"));
	         
	         listaBonificacion = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ObtenerBonificacionDTO.class));
	         for(ObtenerBonificacionDTO p : listaBonificacion) {
	        	 correlativo +=1;
	        	 p.setCorrelativo(correlativo);
	         }
			return listaBonificacion;
			
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
			return listaBonificacion;
		}
		
	}
	
	@Override
	public BonificacionDTO getBonificacion(Long bonificacionId) {
		BonificacionDTO bonificacion = new BonificacionDTO();
		try {
			 StringBuilder sql = new StringBuilder();
			 Map<String, Object> objectParam = new HashMap<>();
			 sql.append(SQL_GET_BONIFICACION);
			 if (!Util.isEmpty(bonificacionId)) objectParam.put("bonificacionId", bonificacionId);
			 bonificacion =  this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(BonificacionDTO.class)).get(0);
			 return bonificacion;
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
			return bonificacion;
		}
	}

	@Override
	public List<BonificacionDetalleDTO> getBonificacionDetalleList(Long bonificacionId) {
		List<BonificacionDetalleDTO> lstBonificacionDetalle = new ArrayList<>();
		try {
			 StringBuilder sql = new StringBuilder();
			 Map<String, Object> objectParam = new HashMap<>();
			 sql.append(SQL_GET_lST_BONIFICACION_DETALLE);
			 if (!Util.isEmpty(bonificacionId)) objectParam.put("bonificacionId", bonificacionId);
			 lstBonificacionDetalle =  this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(BonificacionDetalleDTO.class));
			 return lstBonificacionDetalle;
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
			return lstBonificacionDetalle;
		}
	}

}
