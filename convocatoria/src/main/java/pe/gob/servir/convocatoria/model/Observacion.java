package pe.gob.servir.convocatoria.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_base_obs", schema = "sch_base")
@Getter
@Setter
public class Observacion   extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tbl_base_obs")
    @SequenceGenerator(name = "seq_tbl_base_obs", sequenceName = "seq_tbl_base_obs", schema = "sch_base", allocationSize = 1)
    @Column(name = "base_obs_id")
    private Long baseObsId;

    @JoinColumn(name="base_movi_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Movimiento movimiento;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "etapa")
    private Long etapa;

}
