package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.Conocimiento;
import pe.gob.servir.convocatoria.model.Perfil;


import java.util.List;



@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long> {
    @Query("SELECT P.conocimientoList FROM Perfil P WHERE P.perfilId = :id")
    List<Conocimiento> findC(@Param("id") Long id);

    List<Perfil> findByEntidadIdAndRegimenLaboralId( Long entidadId , Long regimenLaboralId);


}
