package pe.gob.servir.convocatoria.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespEntidad;
import pe.gob.servir.convocatoria.response.RespOrganigrama;
import pe.gob.servir.convocatoria.response.RespUnidadOrganica;

@FeignClient(name = "entidadApi", url = "${entidad.private.base.url}")
public interface EntidadClient {
	
	
	@GetMapping(value = "/v1/entidad/{entidadId}" , produces = MediaType.APPLICATION_JSON_VALUE)
	RespBase<RespEntidad> obtieneEntidad(@PathVariable("entidadId") Long entidadId);
	
	@GetMapping(value = "/v1/unidadorganica/{entidadId}" , produces = MediaType.APPLICATION_JSON_VALUE)
	RespBase<RespUnidadOrganica> obtieneUnidadOrganica(@PathVariable("entidadId") Long entidadId);
	
	@GetMapping(value = "/v1/organigrama/filter" , produces = MediaType.APPLICATION_JSON_VALUE)
	RespBase<RespOrganigrama> obtieneOrgano(
			@RequestParam(value = "entidadId") Long entidadId,
			@RequestParam(value = "tipo") Long tipo);
}
