package pe.gob.servir.convocatoria.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lowagie.text.DocumentException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqOrquestadorPdf;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespPdfInforme;
import pe.gob.servir.convocatoria.service.PdfConstructorService;

@RestController
@Tag(name = "Generar PDF", description = "")
public class PDFController {

	private final PdfConstructorService pdfConstructorService;

	@Autowired
	public PDFController(PdfConstructorService pdfConstructorService) {
		this.pdfConstructorService = pdfConstructorService;
	}

	@Operation(summary = "Genera Documento PDF", description = "Genera un documento PDF dependiendo del tipo de informe solicitado", tags = {
			"" }, security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/pdf/generar/informe" }, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<RespBase<RespPdfInforme>> orquestadoPdf(@PathVariable String access,
			@RequestBody ReqBase<ReqOrquestadorPdf> request)
			throws DocumentException, IOException, IllegalAccessException {
		RespBase<RespPdfInforme> response = pdfConstructorService.seleccionarPdfByTipoInforme(request);
		return ResponseEntity.ok(response);
	}

}
