package pe.gob.servir.convocatoria.request.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

import javax.validation.constraints.NotNull;

@JGlobalMap
@Getter
@Setter
public class ConocimientoDTO {

	@NotNull(message = Constantes.CAMPO + " perfilId " + Constantes.ES_OBLIGATORIO)
	private Long perfilId;
	
	@NotNull(message = Constantes.CAMPO + " regimenLaboralId " + Constantes.ES_OBLIGATORIO)
	private Long regimenLaboralId;
	
	private Long perfilConocimientoId;

	private String descrConocimiento;
	private String descriTipoConocimiento;

	
	private Integer horas;
	
	private Long nivelDominioId;

	//private String cod_cabecera;
	
	private String observaciones;

	
	private String estado;

	private Long maeConocimientoId;
	private Long tipoConocimientoId;


	@Override
	public String toString() {
		return "ConocimientoDTO{" +
				"perfilId=" + perfilId +
				", regimenLaboralId=" + regimenLaboralId +
				", perfilConocimientoId=" + perfilConocimientoId +
				", horas=" + horas +
				", nivelDominioId=" + nivelDominioId +
				", observaciones='" + observaciones + '\'' +
				", estado='" + estado + '\'' +
				", maeConocimientoId=" + maeConocimientoId +
				'}';
	}
}
