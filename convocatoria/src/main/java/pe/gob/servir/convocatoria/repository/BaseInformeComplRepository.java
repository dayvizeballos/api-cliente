package pe.gob.servir.convocatoria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import feign.Param;
import pe.gob.servir.convocatoria.model.InformeComplement;

public interface BaseInformeComplRepository extends JpaRepository<InformeComplement, Long>{
	@Query("select eva from InformeComplement eva where baseId=:baseId and estadoRegistro = '1' ")
	public List<InformeComplement> findByBaseId(@Param("baseId")Long baseId);
}
