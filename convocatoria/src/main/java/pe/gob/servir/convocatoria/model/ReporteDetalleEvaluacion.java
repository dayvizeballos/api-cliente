package pe.gob.servir.convocatoria.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReporteDetalleEvaluacion {

	private Long evaluacionEntidadId;
	private String descripcion;
	private String informe;
	private Integer peso;
	private Integer puntajeMin;
	private Integer puntajeMax;
	private String aplica;
	private String titulo;
	private String criterio;
	private String contenido;
}
