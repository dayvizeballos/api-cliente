package pe.gob.servir.convocatoria.util;

public class StringUtil {

    private StringUtil() {
        throw new IllegalStateException("Utility class");
    }

    
    public static String devolverCadena(String input) {
        String output = "";
        output = input.replace("&aacute;", "á").
                replace("&eacute;", "é").
                replace("&iacute;", "í").
                replace("&oacute;", "ó").
                replace("&uacute;", "ú").
                replace("&Aacute;", "Á").
                replace("&Eacute;", "É").
                replace("&Iacute;", "Í").
                replace("&Oacute;", "Ó").
                replace("&Uacute;", "Ú").
                replace("&ntilde;", "ñ").
                replace("&Ntilde;", "Ñ").
                replace("&nbsp;", "\t").
                replace("&deg;", "°").
                replace("&ndash;", "-").
                replace("&ldquo;", "\"").
                replace("&rdquo;",  "\"").
                replace("&deg;", "°").
                replace("&ordm;", "°");
        
        return output;
    }
    
    public static String remplazarTag(String input) {
    	String output = "";
    		output = input.replace("<br>", "<br></br>");
    	return output;
    }

    /**
     * HTML BASE PARA ENVIO DE CORREO CONVOCATORIA
     * @return
     */
    public static String pantillaCorreo() {
        String html = "<body style=\"width: 90%;\" >\n" +
                "    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"\n" +
                "        style=\"font-family:Arial,Helvetica,sans-serif;font-size:12px;color:rgb(51,51,51)\" width=\"900\">\n" +
                "        <tbody>\n" +
                "            <tr style=\"background-color:#A60400\">\n" +
                "                <td height=\"40\" style=\"padding:7px 7px;\" valign=\"middle\">\n" +
                "                    <img align=\"left\" border=\"0\" class=\"CToWUd\"\n" +
                "                        src=\"https://www.servir.gob.pe/wp-content/themes/Shigo%20Servir%20Theme%20v1/sjci/img/logo-color.png\" /><br /><br />\n" +
                "                </td>\n" +
                "            </tr>\n" +
                "\n" +
                "        </tbody>\n" +
                "    </table>\n" +
                "    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"\n" +
                "        style=\"height: 5px; background-color: #000;color:white;font-family:Arial,Helvetica,sans-serif;font-size:12px\"\n" +
                "        width=\"900\">\n" +
                "        <tbody>\n" +
                "\n" +
                "            <tr>\n" +
                "                <td style=\"text-align:right\">Sistema de Notificación Convocatoria</td>\n" +
                "            </tr>\n" +
                "        </tbody>\n" +
                "    </table>\n" +
                "    <br />\n" +
                "    <table align=\"center\" width=\"800\">\n" +
                "        <tr>\n" +
                "            <td align=\"center\">\n" +
                "                <table align=\"center\">\n" +
                "                    <tr>\n" +
                "                        <th style=\"text-align:right\">&nbsp;</th>\n" +
                "                        <td style=\"text-align:left\">\n" +
                "                            NOMBRE ENTIDAD <br /><br />\n" +
                "                        </td>\n" +
                "                        <th style=\"text-align:right\">&nbsp;</th>\n" +
                "                        <td style=\"text-align:left\">\n" +
                "                            <NOMBRE> <br /><br />\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                    <tr>\n" +
                "                        <th style=\"text-align:right\">&nbsp;</th>\n" +
                "                        <td style=\"text-align:left\">\n" +
                "                            FECHA PUBLICACIÓN <br /><br />\n" +
                "                        </td>\n" +
                "                        <th style=\"text-align:right\">&nbsp;</th>\n" +
                "                        <td style=\"text-align:left\">\n" +
                "                            <FECHA> <br /><br />\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                    <tr>\n" +
                "                        <th style=\"text-align:right\">&nbsp;</th>\n" +
                "                        <td style=\"text-align:left\">\n" +
                "                            BASES PUBLICADAS <br /><br />\n" +
                "                        </td>\n" +
                "                        <th style=\"text-align:right\">&nbsp;</th>\n" +
                "                        <td style=\"text-align:left\">\n" +
                "                            <CANTIDAD> <br /><br />\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                    <br />\n" +
                "\n" +
                "                </table>\n" +
                "\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <br />\n" +
                "    </table>\n" +
                "\n" +
                "    <table align=\"center\"  width=\"700\" summary=\"Bases a Publicar\" style=\"border-collapse: collapse;width: 800px; text-align: center;\">\n" +
                "        <thead>\n" +
                "          <tr style=\"text-align: center;\">\n" +
                "            <th scope=\"col\">#</th>\n" +
                "            <th scope=\"col\">Codigo</th>\n" +
                "            <th scope=\"col\">Denominación</th>\n" +
                "            <th scope=\"col\">Fecha Creación</th>\n" +
                "            <th scope=\"col\">Fecha Publicación</th>\n" +
                "            <th scope=\"col\">Gestor</th>\n" +
                "            <th scope=\"col\">Coordinador</th>\n" +
                "            <th scope=\"col\">Regimen</th>\n" +
                "            <th scope=\"col\">Vacantes</th>\n" +
                "            <th scope=\"col\">link</th>\n" +
                "          </tr>\n" +
                "        </thead>\n" +
                "        <tbody>\n" +
                "        <CUERPO>\n" +
                "        </tbody>\n" +
                "       \n" +
                "      </table>\n" +
                "\n" +
                "    <br />\n" +
                "    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"\n" +
                "        style=\"border:1px solid rgb(221,221,221);font-family:Arial,Helvetica,sans-serif;font-size:12px;color:rgb(51,51,51)\"\n" +
                "        width=\"800\">\n" +
                "        <tbody>\n" +
                "            <tr>\n" +
                "                <td bgcolor=\"#B80400\" width=\"100%\">\n" +
                "                    <p\n" +
                "                        style=\"margin:0px;text-align:center;color:rgb(255,255,255);padding:5px;line-height:14px;font-family:Arial,Helvetica,sans-serif\">\n" +
                "                        Autoridad Nacional del Servicio Civil - SERVIR<br /><br />Pje. Francisco de Zela 150 piso 10,\n" +
                "                        Jesús María <br /><br />Lima - Perú</p>\n" +
                "                </td>\n" +
                "            </tr>\n" +
                "        </tbody>\n" +
                "    </table>\n" +
                "    <p>&nbsp;</p>\n" +
                "</body>";

        return html;
    }
}
