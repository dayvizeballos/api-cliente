package pe.gob.servir.convocatoria.loader;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.GrupoServidor;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.feign.client.MaestraApiClient;
import pe.gob.servir.convocatoria.model.*;
import pe.gob.servir.convocatoria.repository.*;
import pe.gob.servir.convocatoria.request.ReqCargaMasivaPerfil;
import pe.gob.servir.convocatoria.request.dto.ParametrosDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.CapacitacionService;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;
import pe.gob.servir.convocatoria.service.PerfilGrupoService;
import pe.gob.servir.convocatoria.util.Util;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Slf4j
public class LoaderMasiva30057  {

    private HSSFWorkbook hssfLibroExcel;
    private XSSFWorkbook xssfLibroExcel;


    private HSSFFormulaEvaluator hssFormulaEvaluator;
    private XSSFFormulaEvaluator xssFormulaEvaluator;

    List<String> lstErrores;

    Map<String, Object> objectMapOrganos;

    Map<String, Object> objectMapUnidadOrganica;

    Map<String, Object> objectMapGrupoSerCivil;

    Map<String, Object> objectMapNivelCategoria;

    Map<String, Object> objectMapPuestoTipo;

    Map<String, Object> objectMapTipoPeriodicidad;

    Map<String, Object> objectMapGrupoSerCoordina;

    Map<String, Object> objectMapNivelEducativo;

    Map<String, Object> objectMapCarreras;

    Map<String, Object> objectMapSituAcademica;

    Map<String, Object> objectMapNivelMinimoPuesto;

    Map<String, Object> objectMapTipoRequisitoAd;

    Map<String, Object> objectMapEstadoNivelEducativo;

    Map<String, Object> objectMapGrados;


    @Autowired
    Validacion30057 validacion30057;

    @Autowired
    CargaMasivaRepositoryRepository cargaMasivaRepositoryRepository;

    @Autowired
    PerfilFuncionRepository perfilFuncionRepository;

    @Autowired
    PerfilExperienciaRepository perfilExperienciaRepository;

    @Autowired
    PerfilRepository perfilRepository;

    @Autowired
    EntidadClient entidadClient;

    @Autowired
    PerfilGrupoService perfilGrupoService;

    @Autowired
    PerfilGrupoRepository perfilGrupoRepository;

    @Autowired
    MaestraApiClient maestraClient;

    @Autowired
    MaestraDetalleService maestraDetalleService;

    @Autowired
    MaestraDetalleRepository maestraDetalleRepository;

    @Autowired
    FuncionDetalleRepository funcionDetalleRepository;

    @Autowired
    CapacitacionService capacitacionService;

    @Autowired
    CarreraProfesionalRepository carreraProfesionalRepository;

    @Autowired
    MaeConocimientoRepository maeConocimientoRepository;

    @Autowired
    FormacionAcademicaRepository formacionAcademicaRepository;



    private static final String[] FUNCIONES = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16" };

    /**
     * @param hssfFilaExcel
     * @param xssfFilaExcel
     * @param fila
     * @return
     */
    private PerfilFuncion settTabPerfilFuncion( HSSFRow hssfFilaExcel,
                                                      XSSFRow xssfFilaExcel, int fila, boolean ok, Map<String, Object> objectMap , MyJsonWebToken token) throws Exception {

        PerfilFuncion pFuncion = new PerfilFuncion();
        pFuncion.setFuncionDetalles(new ArrayList<>());

        //FuncionDetalle pFuncionDet =  new FuncionDetalle();
        Map<String, String> valores;
        CargaMasiva cargaMasiva;
        // FUNCIONES
        List<FuncionDetalle> lstFuncionDetalle = new ArrayList<>();
        List<String> lstErrorFunciones =  new ArrayList<>();
        int reg = 0;
        for (int i = 1; i < FUNCIONES.length + 1; i++) {
            String campo = ConstantesExcel.FUNCION + FUNCIONES[i - 1];
            cargaMasiva = (CargaMasiva) objectMap.get(campo);
            if (cargaMasiva != null) {
                FuncionDetalle detalle = new FuncionDetalle();

                String funcion = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva,
                        this.hssFormulaEvaluator, this.xssFormulaEvaluator);
                String rpta = validarErrorSubstring(funcion);

                if (rpta == null) {
                    this.lstErrores.add(funcion);
                    lstErrorFunciones.add(funcion);
                    ok = false;
                }else {
                    if (!Util.isEmpty(funcion)) {
                        reg++;
                        detalle.setDescripcion(funcion);
                        detalle.setOrden(reg);
                        lstFuncionDetalle.add(detalle);
                    }
                }

            }
        }



        // 2: CONDICION ATIPICA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.CONDICION_ATIPICA);
        if (cargaMasiva != null) {
            String condicion = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva,
                    this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(condicion);

            if (rpta == null) {
                this.lstErrores.add(condicion);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(condicion)) {
                    pFuncion.setCondicionAticipica(condicion);
                }

        }

        // 3: PERIODICIDAD CONDICION
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.PERIODICIDAD_CONDICION_ATIPICA);
        if (cargaMasiva != null) {
            String periodicidad = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva,
                    this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(periodicidad);

            if (rpta == null) {
                this.lstErrores.add(periodicidad);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(periodicidad)) {
                    Long id = obtenerId(periodicidad, objectMapTipoPeriodicidad);
                    pFuncion.setPeriocidadCondicionAtipicaId((id));
                }

        }

        // 4: SUSTENTO PERIOCIDAD
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SUSTENTO_CONDICION_ATIPICA);
        if (cargaMasiva != null) {
            String sustento = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva,
                    this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(sustento);

            if (rpta == null) {
                this.lstErrores.add(sustento);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(sustento)) {
                    pFuncion.setSustentoCondicionAtipica(sustento);
                }

        }

        // 5: COORDINACION INTERNA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.COORDINACION_INTERNA);
        if (cargaMasiva != null) {
            String cIntena = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva,
                    this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(cIntena);

            if (rpta == null) {
                this.lstErrores.add(cIntena);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(cIntena)) {
                    pFuncion.setCoordinacionInterna(cIntena);
                }

        }

        // 7: SERVIDORES CIVILES:FUNCIONARIO_PUBLICO
        List<Long> lstServidor = new ArrayList<>();
        List<String> lstErrorServidor = new ArrayList<>();
        GrupoServidor.stream().forEach(m -> {
            CargaMasiva carga = new CargaMasiva();
            Map<String, String> valor;
            carga = (CargaMasiva) objectMap.get(m.getDescripcion());
            if (carga != null) {
                try {
                    String servidor = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, carga,
                            this.hssFormulaEvaluator, this.xssFormulaEvaluator);
                    String rpta = validarErrorSubstring(servidor);

                    if (rpta == null) {
                        this.lstErrores.add(servidor);
                        lstErrorServidor.add(servidor);
                    } else {
                        if (!Util.isEmpty(servidor)) {
                            if(m.getDescripcion().equals(carga.getNombreColumna())) {
                                valor = splitCeldaCompuestoSerCivil(m.getCodigo(), objectMapGrupoSerCoordina);
                                lstServidor.add(Long.valueOf(valor.get("id")));
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //if (Util.isEmpty(lstErrorServidor) && lstErrorServidor.size() == 0) {
        if (CollectionUtils.isEmpty(lstErrorServidor)) {
            if (CollectionUtils.isNotEmpty(lstServidor)) {
                String result = lstServidor.stream().map(i -> i.toString()).collect(Collectors.joining(","));
                pFuncion.setServidorCiviles(result);
            }
        }else {
            ok = false;
        }


        // 8: COORDINACION EXTERNA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.COORDINACION_EXTERNA);
        if (cargaMasiva != null) {
            String cExterna = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva,
                    this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(cExterna);

            if (rpta == null) {
                this.lstErrores.add(cExterna);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(cExterna)) {
                    pFuncion.setCoordinacionExterna(cExterna);
                }

        }
        pFuncion.setFuncionDetalles(new ArrayList<>());
        if (!lstFuncionDetalle.isEmpty()) {
            for (FuncionDetalle it : lstFuncionDetalle) {
                it.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
            }
        }

        pFuncion.setFuncionDetalles(lstFuncionDetalle);
        //pFuncion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        return pFuncion;
    }

    /**
     * OBTENER EL ID DE LA DESCRIPCION
     *
     * @param valor
     * @return
     */
    private Long obtenerId(String valor, Map<String, Object> mapInput) {
        MaestraDetalle maestraDetalle = (MaestraDetalle) mapInput.get(valor);
        return maestraDetalle.getMaeDetalleId();
    }


    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMapServCoordina(List<PerfilGrupo> ls) {
        return ls.stream()
                .collect(Collectors.toMap(PerfilGrupo::getCodigo, Function.identity()));
    }

    public List<PerfilGrupo> listarFamiliasRol(Long id) {
        return perfilGrupoService.findAllPerfilGrupo(id).getPayload().getItems();
    }

    public HashMap<String, Object> cargarArchivoExcel30057(ReqCargaMasivaPerfil model, InputStream archivoImportado,  MyJsonWebToken token) throws IOException {

        log.info("Inicio - cargarArchivoExcelRegimen");

        if (model.getVersionExcel().equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_XLSM)) {
            this.xssfLibroExcel = new XSSFWorkbook(archivoImportado);
            xssFormulaEvaluator = xssfLibroExcel.getCreationHelper().createFormulaEvaluator();
            this.xssfLibroExcel.setMissingCellPolicy(Row.RETURN_NULL_AND_BLANK);
            archivoImportado.close();
        }


        HashMap<String, Object> hashExcel = new HashMap<String, Object>();

        //Contenedor Errores
        lstErrores = new ArrayList<>();

        //Registros ingresados a BD correctamente
        int correctos = 0;

        //Para validar repitencias ********************************0:Contenedor de repitencias
        List<String> lstRepitencias = new ArrayList<>();

        String strRepitencia = "";

        //Insumo para contar los correctos/incorrectos/total
        int filasReales = 0;


        CargaMasiva cargaMasiva = new CargaMasiva();

        List<Organigrama> listarOrganos = entidadClient.obtieneOrgano(model.getIdEntidad(), null).getPayload().getListaOrganigrama();
        objectMapOrganos = converListToMapOrgano(listarOrganos);


        List<UnidadOrganica> listarUnidadOrganica = entidadClient.obtieneUnidadOrganica(model.getIdEntidad()).getPayload().getListaUnidadOrganica();
        objectMapUnidadOrganica = converListToMapUnidadOrganica(listarUnidadOrganica);


        //Se usa para encontrar Grupo de servidores reporta , Grupo de servidores civiles, Familia de Puestos y Rol esa data esta en la misma tabla se buscara por descripcion
        List<PerfilGrupo> listarGrupoDeServidoresCiviles = perfilGrupoService.listAllPerfilGrupo().getPayload().getItems();
        objectMapGrupoSerCivil = converListToMapServCivil(listarGrupoDeServidoresCiviles);

        List<ParametrosDTO> listarNivel = maestraClient.rutaFileServerEntidad(pe.gob.servir.convocatoria.common.Constantes.NIVEL_ORGANO, "").getPayload().getItems();
        objectMapNivelCategoria = converListToNivelCategoria(listarNivel);


        List<MaestraDetalle> listarPuestoTipo = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null, pe.gob.servir.convocatoria.common.Constantes.TBL_PER_PTO, null).getPayload().getMaestraDetalles();
        objectMapPuestoTipo = converListToPuestoTipo(listarPuestoTipo);

        List<MaestraDetalle> listarPeriocidad = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null, ConstantesExcel.TBL_PER_PER_APL, null).getPayload().getMaestraDetalles();
        objectMapTipoPeriodicidad = converListToPuestoTipo(listarPeriocidad);


        objectMapGrupoSerCoordina = converListToMapServCoordina(listarGrupoDeServidoresCiviles);

        List<MaestraDetalle> regimen = maestraDetalleRepository
                .findDetalleByCodProg(ConstantesExcel.COD_REGIMEN, model.getCodRegimen());

        objectMapGrupoSerCoordina = converListToMapServCivil(listarGrupoDeServidoresCiviles);

        List<MaestraDetalle> listarPuestoTipoNivelEducativo = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null, pe.gob.servir.convocatoria.common.Constantes.TBL_PER_NIV_EDU, null).getPayload().getMaestraDetalles();
        objectMapNivelEducativo = converListToPuestoTipo(listarPuestoTipoNivelEducativo);

        List<MaestraDetalle> listarSituacionAcademica = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                pe.gob.servir.convocatoria.common.Constantes.TBL_PER_SIT_ACA, null).getPayload().getMaestraDetalles();
        objectMapSituAcademica = converListToPuestoTipo(listarSituacionAcademica);


        List<CarreraProfesional> listarCarreras = capacitacionService.listarCarreras().getPayload().getItems();
        objectMapCarreras = converListToPuestoTipo(listarSituacionAcademica);

        List<MaestraDetalle> listarEstadoEducativo = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                pe.gob.servir.convocatoria.common.Constantes.TBL_PER_EST_NIV_EDU, null).getPayload().getMaestraDetalles();
        objectMapEstadoNivelEducativo = converListToPuestoTipo(listarEstadoEducativo);

        List<MaestraDetalle> listarGrados = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                pe.gob.servir.convocatoria.common.Constantes.TBL_PER_EST_GRA, null).getPayload().getMaestraDetalles();
        objectMapGrados = converListToPuestoTipo(listarGrados);

        List<MaestraDetalle> listarNivelMinimoPuesto = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                pe.gob.servir.convocatoria.common.Constantes.TBL_PER_NIV_MIN_PTO, null).getPayload().getMaestraDetalles();
        objectMapNivelMinimoPuesto = converListToPuestoTipo(listarNivelMinimoPuesto);

        List<MaestraDetalle> listarTipoRequisitoAd = maestraDetalleService.obtenerMaestraDetalle(null, null, null, null,
                pe.gob.servir.convocatoria.common.Constantes.TBL_PER_TIP_REQ, null).getPayload().getMaestraDetalles();
        objectMapTipoRequisitoAd = converListToPuestoTipo(listarTipoRequisitoAd);


        try {

            List<CargaMasiva> cargaMasivas = cargaMasivaRepositoryRepository.findByRegimen(ConstantesExcel.REGIMEN_30057);
            Map<String, Object> objectMap = converListToMap(cargaMasivas);
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NOMBRE_PLANTILLA_30057);
            int posicionHoja = 0;

            if (cargaMasiva != null) {
                if (cargaMasiva.getPosicion() != null) {
                    posicionHoja = cargaMasiva.getPosicion();
                } else {
                    lstErrores.add("Asigne valor en la Base de Datos la posicion de la hoja excel de la plantilla ");
                }
            }

            if (lstErrores.size() > 0) {
                hashExcel.put("lstErrores", lstErrores);
                return hashExcel;
            }

            XSSFSheet hojaXssfExcelLastRowNum = null;
            hojaXssfExcelLastRowNum = this.xssfLibroExcel.getSheetAt(posicionHoja);

            int lastRowNum = hojaXssfExcelLastRowNum.getLastRowNum() + 1;

            Iterator<Row> iteradorFila = this.getIteradorFilasExcel(model.getVersionExcel(), posicionHoja);

            int fila = 0;

            HSSFRow hssfFilaExcel = null;
            XSSFRow xssfFilaExcel = null;

            while (iteradorFila.hasNext()) {
                //Si se tiene la hoja de acuerdo lo conversado el iteradorFila valida automatico el final ya no es necesario lastRowNum
                if (lastRowNum == fila) {
                    log.info("Se termino la iteracion" + "----->>>" + fila + "------>>>" + lastRowNum);
                    break;
                }

                boolean ok = true;
                boolean save = true;
                Map<String, Object> perfilMap = new HashMap<>();
                Map<String, Object> perfilFuncionMap = new HashMap<>();
                Map<String, Object> perfilExperienciaMap = new HashMap<>();
                fila++;

                if (model.getVersionExcel().equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2003)) {
                    hssfFilaExcel = (HSSFRow) iteradorFila.next();
                } else if (model.getVersionExcel().equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2007)) {
                    xssfFilaExcel = (XSSFRow) iteradorFila.next();
                } else if (model.getVersionExcel().equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_XLSM)) {
                    xssfFilaExcel = (XSSFRow) iteradorFila.next();
                }


                //Valida que empiece a recorrer desde la 3 fila donde empiesa la data
                if (fila < 3) {
                    continue;
                }


                //TRANSACCION CASCADA
                Perfil perfil = new Perfil();
                perfil =  settTabPerfil(cargaMasiva, perfil , hssfFilaExcel, xssfFilaExcel, fila, ok, objectMap);
                perfil =  saveFormacionAcademicaPerfil (cargaMasiva, perfil, hssfFilaExcel, xssfFilaExcel, fila, ok, objectMap, token);
                Long regimenId = 1L;
                perfil.setRegimenLaboralId(regimenId);
                perfil.setEntidadId(model.getIdEntidad());
                perfil.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());




                List<PerfilExperiencia> ls = settTabPerfilExp(perfil , cargaMasiva, hssfFilaExcel, xssfFilaExcel, fila, true, objectMap , token );

                PerfilFuncion perfilFuncion  = settTabPerfilFuncion(hssfFilaExcel, xssfFilaExcel, fila, true, objectMap , token );

                guardarBD(perfil, ls ,perfilFuncion );

               // perfil = perfilRepository.save(perfil);

                System.out.println(perfil);

                //FIN PRUEBA


                //perfilMap = saveTabPerfil(cargaMasiva, hssfFilaExcel, xssfFilaExcel, fila, ok, objectMap);
                if(!Util.isEmpty(perfilMap) ) {

                    //Long regimenId = regimen.get(0).getMaeDetalleId();
                    Perfil perfilDTO = (Perfil) perfilMap.get("perfil");
                    Boolean rpta1 = (Boolean) perfilMap.get("ok");
                    //perfilRepository.save(perfilDTO);

					//perfilFuncionMap =  saveTabPerfilFuncion(hssfFilaExcel, xssfFilaExcel, fila, rpta1, objectMap);
                    PerfilFuncion pFuncionDTO = (PerfilFuncion)perfilFuncionMap.get("perfilFuncion");
                    List<FuncionDetalle> lstFunDet = (List<FuncionDetalle>) perfilFuncionMap.get("perfilFuncionDetalle");
                    Boolean rpta2 = (Boolean) perfilFuncionMap.get("ok");


                   // perfilExperienciaMap = saveTabPerfilExp(cargaMasiva, hssfFilaExcel, xssfFilaExcel, fila, rpta2, objectMap);
                    PerfilExperiencia perfilExperienciaDTO = (PerfilExperiencia)perfilExperienciaMap.get("perfilExperiencia");
                    List<PerfilExperienciaDetalle> detalle1 = perfilExperienciaDTO.getPerfilExperienciaDetalleList1();
                    List<PerfilExperienciaDetalle> detalle2 = perfilExperienciaDTO.getPerfilExperienciaDetalleList2();

                    Boolean rpta3= (Boolean) perfilExperienciaMap.get("ok");
                    ok = rpta3;


                 //   ok = rpta2;
                    if(ok) {
                        correctos++;
                        //PERFIL
                        perfilDTO.setRegimenLaboralId(regimenId);
                        perfilDTO.setEntidadId(model.getIdEntidad());
                        perfilDTO.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        Perfil p = perfilRepository.save(perfilDTO);

                        //PERFIL FUNCION
                        pFuncionDTO.setPerfilId(p.getPerfilId());
                        pFuncionDTO.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        PerfilFuncion pe = perfilFuncionRepository.save(pFuncionDTO);

                        //PERFIL FUNCION DETALLE
                        lstFunDet.stream()
                                .filter(Objects::nonNull)
                                .forEach(d -> {
                                    d.setPerfilFuncion(pe);
                                    d.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                });
                        funcionDetalleRepository.saveAll(lstFunDet);
                        //FORMACION ACADEMICA

                        //EXPERIENCIA
                        perfilExperienciaDTO.setPerfilId(p.getPerfilId());
                        perfilExperienciaDTO.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());

                        detalle1.stream()
                                .filter(Objects::nonNull)
                                .forEach(d -> {
                                    d.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                });
                        detalle2.stream()
                                .filter(Objects::nonNull)
                                .forEach(d -> {
                                    d.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                });
                        PerfilExperiencia pex = perfilExperienciaRepository.save(perfilExperienciaDTO);

                    }

                }


                System.out.println("se valida el ok del map");

                filasReales++;
                hashExcel.put("correctos", correctos);
            } //ITERACION: FINAL


        } catch (Exception ex) {
            log.error("Error: " + ex.getMessage());
            lstErrores.add("Error al leer datos del excel: " + ex.getMessage());
        } finally {
            hashExcel.put("lstErrores", lstErrores);
            hashExcel.put("incorrectos", filasReales - correctos);
            hashExcel.put("total", correctos + (filasReales - correctos));

        }

        return hashExcel;
    }

    /**
     * GUARDA TODA LA INFO DEL EXCEL @TRANSACCIONAL
     * @param perfil
     * @param ls
     */
    private void guardarBD(Perfil perfil, List<PerfilExperiencia> ls , PerfilFuncion perfilFuncion) {

        perfilRepository.save(perfil);

        for (PerfilExperiencia it : ls) {
            it.setPerfilId(perfil.getPerfilId());
        }

        perfilExperienciaRepository.saveAll(ls);

        perfilFuncion.setPerfilId(perfil.getPerfilId());

        perfilFuncionRepository.save(perfilFuncion);




    }

    /**
     * @param cargaMasiva
     * @param hssfFilaExcel
     * @param xssfFilaExcel
     * @param fila
     * @return
     */
    private Perfil settTabPerfil(CargaMasiva cargaMasiva, Perfil perfil,
                                              HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila,
                                              boolean ok, Map<String, Object> objectMap) throws Exception {

        String grupSerCivilInsumo = "";
        String familiaInsumo = "";
        String rolInsumo = "";

        // 1: ORGANO
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ORGANO);

        Map<String, String> valores;
        if (cargaMasiva != null) {

            String organo = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(organo);

            if (rpta == null) {
                this.lstErrores.add(organo);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(organo)) {
                    if (cargaMasiva.getCompuesto().equalsIgnoreCase("1")) {
                        valores = splitCeldaCompuestaOrgano(organo, objectMapOrganos);
                        perfil.setOrganoId(Long.valueOf(valores.get("id")));
                        perfil.setNombreOrgano(valores.get("nombre"));
                    }
                }
        }


        // 2: UNIDAD_ORGANICA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.UNIDAD_ORGANICA);

        if (cargaMasiva != null) {
            String unidadOrganica = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(unidadOrganica);

            if (rpta == null) {
                this.lstErrores.add(unidadOrganica);
                ok = false;

            }
            if (ok)
                if (!Util.isEmpty(unidadOrganica)) {
                    if (cargaMasiva.getCompuesto().equalsIgnoreCase("1")) {
                        valores = splitCeldaCompuestaUnidadOrganica(unidadOrganica, objectMapUnidadOrganica);
                        perfil.setUnidadOrganicaId(Long.valueOf(valores.get("id")));
                        perfil.setUnidadOrganica(valores.get("nombre"));
                    }
                }
        }


        // 3: NIVEL_ORGANIZACIONAL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NIVEL_ORGANIZACIONAL);
        String nivelOrganizacional = "";


        if (cargaMasiva != null) {
            nivelOrganizacional = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(nivelOrganizacional);

            if (rpta == null) {
                this.lstErrores.add(nivelOrganizacional);
                ok = false;

            }
            if (ok)
                if (!Util.isEmpty(nivelOrganizacional)) {
                    perfil.setNivelOrganizacional(nivelOrganizacional);
                }
        }

        // 4: GRUPO_SERVIDORES_CIVILES
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRUPO_SERVIDORES_CIVILES);
        if (cargaMasiva != null) {
            String servCivil = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(servCivil);

            if (rpta == null) {
                this.lstErrores.add(servCivil);
                ok = false;

            }
            if (ok)
                if (!Util.isEmpty(servCivil)) {
                    valores = splitCeldaCompuestoSerCivil(servCivil, objectMapGrupoSerCivil);
                    perfil.setServidorCivilId(Long.valueOf(valores.get("id")));
                    grupSerCivilInsumo = servCivil;


                }
        }

        //5: FAMILIA_PUESTOS
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.FAMILIA_PUESTOS);
        if (cargaMasiva != null) {
            String familiaPuesto = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(familiaPuesto);

            if (rpta == null) {
                this.lstErrores.add(familiaPuesto);
                ok = false;

            }
            if (ok)
                if (!Util.isEmpty(familiaPuesto)) {
                    valores = splitCeldaCompuestoSerCivil(familiaPuesto, objectMapGrupoSerCivil);
                    perfil.setFamiliaPuestoId(Long.valueOf(valores.get("id")));
                    familiaInsumo = familiaPuesto;
                }
        }

        //6: ROL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ROL);
        if (cargaMasiva != null) {
            String rol = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(rol);

            if (rpta == null) {
                this.lstErrores.add(rol);
                ok = false;

            }
            if (ok)
                if (!Util.isEmpty(rol)) {
                    valores = splitCeldaCompuestoSerCivil(rol, objectMapGrupoSerCivil);
                    perfil.setRolId(Long.valueOf(valores.get("id")));
                    rolInsumo = rol;
                }

        }

        //7: NIVEL_CATEGORIA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NIVEL_CATEGORIA);

        if (cargaMasiva != null) {
            String nivelCategoria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(nivelCategoria);

            if (rpta == null) {
                this.lstErrores.add(nivelCategoria);
                ok = false;

            }
            if (ok)
                if (!Util.isEmpty(nivelCategoria)) {
                    valores = splitCeldaCompuestoNivelCategoria(nivelCategoria, objectMapNivelCategoria);
                    perfil.setNivelCategoriaId(Long.valueOf(valores.get("id")));
                }
        }

        //8: SUB_NIVEL_SUB_CATEGORIA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SUB_NIVEL_SUB_CATEGORIA);
        if (cargaMasiva != null) {
            String subNivelCategoria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(subNivelCategoria);

            if (rpta == null) {
                this.lstErrores.add(subNivelCategoria);
                ok = false;

            }
            if (ok)
                if (!Util.isEmpty(subNivelCategoria)) {
                    perfil.setSubNivelsubCategoria(subNivelCategoria);
                }
        }


        //9: NOMBRE_PUESTO
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NOMBRE_PUESTO);
        if (cargaMasiva != null) {
            String nombrePuesto = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(nombrePuesto);

            if (rpta == null) {
                this.lstErrores.add(nombrePuesto);
                ok = false;

            }
            if (ok)
                if (!Util.isEmpty(nombrePuesto)) {
                    perfil.setNombrePuesto(nombrePuesto);
                }
        }

        //10: CODIGO_PUESTO
        if (perfil.getServidorCivilId() != null) {
            if (perfil.getFamiliaPuestoId() != null) {
                if (perfil.getRolId() != null) {
                    cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.CODIGO_PUESTO);
                    if (cargaMasiva != null) {
                        String codigoPuesto = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
                        String rpta = validarErrorSubstring(codigoPuesto);

                        if (rpta == null) {
                            this.lstErrores.add(codigoPuesto);
                            ok = false;

                        }
                        if (ok)
                            if (!Util.isEmpty(codigoPuesto)) {
                                String cod = codigoPuesto(codigoPuesto, grupSerCivilInsumo, familiaInsumo, rolInsumo, objectMapGrupoSerCivil);
                                perfil.setPuestoCodigo(cod);
                            }


                    }
                }
            }
        }


        //11: NRO_POSICIONES
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NRO_POSICIONES);
        if (cargaMasiva != null) {
            String nroPosiciones = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(nroPosiciones);

            if (rpta == null) {
                this.lstErrores.add(nroPosiciones);
                ok = false;

            }

            if (ok)
                if (!Util.isEmpty(nroPosiciones)) {
                    perfil.setNroPosicionesPuesto(Double.valueOf(nroPosiciones).intValue());
                }

        }


        //12: CODIGO_POSICION
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.CODIGO_POSICION);
        if (cargaMasiva != null) {
            String codPosicion = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(codPosicion);

            if (rpta == null) {
                this.lstErrores.add(codPosicion);
                ok = false;

            }

            if (ok)
                if (!Util.isEmpty(codPosicion)) {
                    perfil.setCodigoPosicion(codPosicion);
                }

        }


        //13: DEPENDENCIA_JERARQUICA_LINEAL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.DEPENDENCIA_JERARQUICA_LINEAL);
        if (cargaMasiva != null) {
            String depJerLineal = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(depJerLineal);

            if (rpta == null) {
                this.lstErrores.add(depJerLineal);
                ok = false;

            }

            if (ok)
                if (!Util.isEmpty(depJerLineal)) {
                    perfil.setDependenciaJerarquica(depJerLineal);
                }
        }

        //14: DEPENDENCIA_FUNCIONAL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.DEPENDENCIA_FUNCIONAL);
        if (cargaMasiva != null) {
            String depJerFuncional = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(depJerFuncional);

            if (rpta == null) {
                this.lstErrores.add(depJerFuncional);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(depJerFuncional)) {
                    perfil.setDependenciaFuncional(depJerFuncional);
                }

        }


        //15: UNIDAD_FUNCIONAL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.UNIDAD_FUNCIONAL);
        if (cargaMasiva != null) {
            String unidadFuncional = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(unidadFuncional);

            if (rpta == null) {
                this.lstErrores.add(unidadFuncional);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(unidadFuncional)) {
                    perfil.setUnidadFuncional(unidadFuncional);
                }

        }


        //16: PUESTO_TIPO
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.PUESTO_TIPO);
        if (cargaMasiva != null) {
            String puestoTipo = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(puestoTipo);

            if (rpta == null) {
                this.lstErrores.add(puestoTipo);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(puestoTipo)) {
                    valores = splitCeldaCompuestoPuestoTipo(puestoTipo, objectMapPuestoTipo);
                    perfil.setPuestoTipoId(Long.valueOf(valores.get("id")));
                }

        }


        //17: GRUPO_SERVIDORES_REPORTA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRUPO_SERVIDORES_REPORTA);
        if (cargaMasiva != null) {
            String grupoSerCivReporta = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(grupoSerCivReporta);

            if (rpta == null) {
                this.lstErrores.add(grupoSerCivReporta);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(grupoSerCivReporta)) {
                    valores = splitCeldaCompuestoSerCivil(grupoSerCivReporta, objectMapGrupoSerCivil);
                    perfil.setServidorCivilReporteId(Long.valueOf(valores.get("id")));
                }
        }

        //18: NRO_POSICIONES_CARGO
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NRO_POSICIONES_CARGO);
        if (cargaMasiva != null) {
            String nroPosicionesCargo = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(nroPosicionesCargo);

            if (rpta == null) {
                this.lstErrores.add(nroPosicionesCargo);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(nroPosicionesCargo)) {
                    perfil.setNroPosicionesCargo(Double.valueOf(nroPosicionesCargo).intValue());
                }

        }

        //19: MISION_PUESTO
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.MISION_PUESTO);
        if (cargaMasiva != null) {
            String misionPuesto = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(misionPuesto);

            if (rpta == null) {
                this.lstErrores.add(misionPuesto);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(misionPuesto)) {
                    perfil.setMisionPuesto(misionPuesto);
                }

        }

        return perfil;
    }


    /**
     * Valida Version formato xls
     *
     * @param archivoImportado
     * @throws IOException
     */
    public void setExcelVersion2003(InputStream archivoImportado) throws IOException {
        this.hssfLibroExcel = new HSSFWorkbook(archivoImportado);
        hssFormulaEvaluator = (HSSFFormulaEvaluator) hssfLibroExcel.getCreationHelper().createFormulaEvaluator();
        this.hssfLibroExcel.setMissingCellPolicy(Row.RETURN_NULL_AND_BLANK);
        archivoImportado.close();
    }

    /**
     * Valida Version formato xlsx y xlsm
     *
     * @param archivoImportado
     * @throws IOException
     */
    public void setExcelVersion2007(InputStream archivoImportado) throws IOException {
        this.xssfLibroExcel = new XSSFWorkbook(archivoImportado);
        xssFormulaEvaluator = xssfLibroExcel.getCreationHelper().createFormulaEvaluator();
        this.xssfLibroExcel.setMissingCellPolicy(Row.RETURN_NULL_AND_BLANK);
        archivoImportado.close();
    }


    /**
     * Itera las filas Excel
     *
     * @param versionExcel
     * @param posicionHoja
     * @return
     * @throws Exception
     */
    private Iterator<Row> getIteradorFilasExcel(String versionExcel, int posicionHoja) throws Exception {
        Iterator<Row> iteradorFila = null;
        HSSFSheet hojaHssfExcel = null;
        XSSFSheet hojaXssfExcel = null;

        if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2003)) {
            hojaHssfExcel = this.hssfLibroExcel.getSheetAt(posicionHoja);
            iteradorFila = hojaHssfExcel.rowIterator();
        } else if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2007)) {
            hojaXssfExcel = this.xssfLibroExcel.getSheetAt(posicionHoja);
            iteradorFila = hojaXssfExcel.rowIterator();
        } else if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_XLSM)) {
            hojaXssfExcel = this.xssfLibroExcel.getSheetAt(posicionHoja);
            iteradorFila = hojaXssfExcel.rowIterator();
        }

        return iteradorFila;
    }

    /**
     * Valida existencia de la hoja
     *
     * @param versionExcel
     * @param posicionHoja
     * @return
     * @throws Exception
     */
    private Properties validarExistenciaHoja(String versionExcel, int posicionHoja) throws Exception {
        int numeroHojas = 0;
        List<String> nombreHojas = new ArrayList<String>();
        Properties resultado = new Properties();

        if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2003)) {
            numeroHojas = hssfLibroExcel.getNumberOfSheets();
            for (int i = 0; i < numeroHojas; i++) {
                nombreHojas.add(hssfLibroExcel.getSheetName(i));
            }
        } else if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_2007)) {
            numeroHojas = xssfLibroExcel.getNumberOfSheets();
            for (int i = 0; i < numeroHojas; i++) {
                nombreHojas.add(xssfLibroExcel.getSheetName(i));
            }
        } else if (versionExcel.equalsIgnoreCase(ConstantesExcel.EXCEL_VERSION_XLSM)) {
            for (int i = 0; i < numeroHojas; i++) {
                nombreHojas.add(xssfLibroExcel.getSheetName(i));
            }
        }

        resultado.put("numeroHojas", numeroHojas);

        if (nombreHojas.size() > 0) {
            resultado.put("nombreHojas", nombreHojas.toString());
        }

        return resultado;
    }

    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestaOrgano(String valor, Map<String, Object> mapInput) {

        Organigrama organo = (Organigrama) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("nombre", organo.getDescripcion() == null ? null : organo.getDescripcion().toUpperCase());
        map.put("id", String.valueOf(organo.getOrganigramaId()));
        return map;
    }


    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestaUnidadOrganica(String valor, Map<String, Object> mapInput) {

        UnidadOrganica unidadOrganica = (UnidadOrganica) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("nombre", unidadOrganica.getUnidadOrganica() == null ? null : unidadOrganica.getUnidadOrganica().toUpperCase());
        map.put("id", String.valueOf(unidadOrganica.getOrganigramaId()));
        return map;
    }

    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestoSerCivil(String valor, Map<String, Object> mapInput) {

        PerfilGrupo perfilGrupo = (PerfilGrupo) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(perfilGrupo.getId()));
        return map;
    }

    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestoNivelCategoria(String valor, Map<String, Object> mapInput) {
        ParametrosDTO parametrosDTO = (ParametrosDTO) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(parametrosDTO.getParametroId()));
        return map;
    }

    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestoPuestoTipo(String valor, Map<String, Object> mapInput) {
        MaestraDetalle maestraDetalle = (MaestraDetalle) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(maestraDetalle.getMaeDetalleId()));
        return map;
    }

    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaCompuestoCarreraProfesional(String valor, Map<String, Object> mapInput) {
        CarreraProfesional carreraProfesional = (CarreraProfesional) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(carreraProfesional.getId()));
        return map;
    }

    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @return
     */
    private List<PerfilExperienciaDetalle> splitCeldaRequisitoAd(String requisito,String descripcion, Map<String, Object> mapInput, PerfilExperiencia perfilExperiencia , MyJsonWebToken token) {
    	List<String> listaRequisitoAd = Arrays.asList(requisito.split(",", -1));
    	List<String> listaDescripcion = Arrays.asList(descripcion.split(",", -1));
    	List<PerfilExperienciaDetalle> listaPerfilExperienciaDetalle = new  ArrayList<>();

    	for (int i = 0; i < listaRequisitoAd.size(); i++) {
    		String req = listaRequisitoAd.get(i);
    		MaestraDetalle maestraDetalle = (MaestraDetalle) mapInput.get(req.trim());
        	PerfilExperienciaDetalle perfilExperienciaDetalle= new PerfilExperienciaDetalle();
        	perfilExperienciaDetalle.setDescripcion(listaDescripcion.get(i));
			perfilExperienciaDetalle.setOrden(Integer.valueOf(i).longValue());
			perfilExperienciaDetalle.setRequisitoId(maestraDetalle.getMaeDetalleId());
			perfilExperienciaDetalle.setTiDaExId(Constantes.REQUISITOS);
			perfilExperienciaDetalle.setPerfilExperiencia(perfilExperiencia);
            perfilExperienciaDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			listaPerfilExperienciaDetalle.add(perfilExperienciaDetalle);


		}
        return listaPerfilExperienciaDetalle;
    }

    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private List<PerfilExperienciaDetalle> splitCeldaHabilidadesCompetencia(String valor,PerfilExperiencia perfilExp , MyJsonWebToken token) {
    	List<PerfilExperienciaDetalle> listaPerfilExperienciaDetalle = new  ArrayList<>();
        List<String> listaHabilidades = Arrays.asList(valor.split(",", -1));
        for (int i = 0; i < listaHabilidades.size(); i++) {
        	PerfilExperienciaDetalle perfilExperienciaDetalle= new PerfilExperienciaDetalle();
			String habilidades = listaHabilidades.get(i);
			perfilExperienciaDetalle.setDescripcion(habilidades);
			perfilExperienciaDetalle.setOrden(Integer.valueOf(i).longValue());
			perfilExperienciaDetalle.setTiDaExId(Constantes.HABILIDADES);
			perfilExperienciaDetalle.setPerfilExperiencia(perfilExp);
            perfilExperienciaDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			listaPerfilExperienciaDetalle.add(perfilExperienciaDetalle);
		}

        return listaPerfilExperienciaDetalle;
    }

    /**
     * Valida cuando el valor es compuesto id/nombre
     *
     * @param valor
     * @return
     */
    private Map<String, String> splitCeldaNivelMinimoPuesto(String valor, Map<String, Object> mapInput) {
    	MaestraDetalle maestraDetalle = (MaestraDetalle) mapInput.get(valor);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(maestraDetalle.getMaeDetalleId()));
        return map;
    }

    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMap(List<CargaMasiva> ls) {
        return ls.stream()
                .collect(Collectors.toMap(CargaMasiva::getNombreColumna, Function.identity()));
    }

    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMapOrgano(List<Organigrama> ls) {
        return ls.stream()
                .collect(Collectors.toMap(Organigrama::getDescripcion, Function.identity()));
    }

    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMapUnidadOrganica(List<UnidadOrganica> ls) {
        return ls.stream()
                .collect(Collectors.toMap(UnidadOrganica::getUnidadOrganica, Function.identity()));
    }

    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToMapServCivil(List<PerfilGrupo> ls) {
        return ls.stream()
                .collect(Collectors.toMap(PerfilGrupo::getDescripcion, Function.identity()));
    }

    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToNivelCategoria(List<ParametrosDTO> ls) {
        return ls.stream()
                .collect(Collectors.toMap(ParametrosDTO::getDescripcion, Function.identity()));
    }

    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToPuestoTipo(List<MaestraDetalle> ls) {
        return ls.stream()
                .collect(Collectors.toMap(MaestraDetalle::getDescripcion, Function.identity()));
    }

    /**
     * Convert List to Map
     *
     * @param ls
     * @return
     */
    private Map<String, Object> converListToCarreras(List<CarreraProfesional> ls) {
        return ls.stream()
                .collect(Collectors.toMap(CarreraProfesional::getDescripcion, Function.identity()));
    }

    /**
     * Valida sub string si es ERROR
     *
     * @param valor
     * @return
     */
    private String validarErrorSubstring(String valor) {
        if (!Util.isEmpty(valor)) {
            if (valor.length() > 4) {
                if (valor.substring(0, 5).equalsIgnoreCase(ConstantesExcel.ERROR)) {
                    return null;
                }
            }
        }
        return valor;
    }

    /**
     * Codigo de Puesto
     *
     * @param valor
     * @param svcivil
     * @param familia
     * @param rol
     * @param objectMapGrupoSerCivil
     * @return
     */
    private String codigoPuesto(String valor, String svcivil, String familia, String rol, Map<String, Object> objectMapGrupoSerCivil) {
        PerfilGrupo perfilGrupoScivil = (PerfilGrupo) objectMapGrupoSerCivil.get(svcivil);
        PerfilGrupo perfilFamilia = (PerfilGrupo) objectMapGrupoSerCivil.get(familia);
        PerfilGrupo perfilRol = (PerfilGrupo) objectMapGrupoSerCivil.get(rol);

        return perfilGrupoScivil.getCodigo() + "_" + perfilFamilia.getCodigo() + "_" + perfilRol.getCodigo() + "_" + valor;

    }


    /**
     * Save
     *
     * @param cargaMasiva
     * @param hssfFilaExcel
     * @param xssfFilaExcel
     * @param fila
     * @param ok
     * @param objectMap
     * @return
     */
    private Perfil saveFormacionAcademicaPerfil (CargaMasiva cargaMasiva, Perfil perfil,
                                                             HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila,
                                                             boolean ok, Map<String, Object> objectMap, MyJsonWebToken token) throws Exception {

        FormacionAcademica formacionAcademicaPrimaria = new FormacionAcademica();
        FormacionAcademica formacionAcademicaSecundaria = new FormacionAcademica();

        FormacionAcademica formacionAcademicaTecnicaBasicaEgresado = new FormacionAcademica();
        FormacionAcademica formacionAcademicaTecnicaBasicaTitulado = new FormacionAcademica();

        FormacionAcademica formacionAcademicaTecnicaSuperiorEgresado = new FormacionAcademica();
        FormacionAcademica formacionAcademicaTecnicaSuperiorTitulado = new FormacionAcademica();

        FormacionAcademica formacionAcademicaUniversitariaEgresado = new FormacionAcademica();
        FormacionAcademica formacionAcademicaUniversitariaBachiller = new FormacionAcademica();
        FormacionAcademica formacionAcademicaUniversitariaTitulado = new FormacionAcademica();
        FormacionAcademica formacionAcademicaUniversitariaMaestria = new FormacionAcademica();
        FormacionAcademica formacionAcademicaUniversitariaDoctorado = new FormacionAcademica();


        List<FormacionAcademica> lstFormAcademica = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaTecnicaBasicaEgresadoList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaTecnicaBasicaTituladoList = new ArrayList<>();

        List<CarreraFormacionAcademica> carreraFormacionAcademicaTecnicaSuperiorEgresadoList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaTecnicaSuperiorTituladoList = new ArrayList<>();


        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaEgresadoList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaBachillerList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaTituladoList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaMaestriaList = new ArrayList<>();
        List<CarreraFormacionAcademica> carreraFormacionAcademicaUniversitariaDoctoradoList = new ArrayList<>();


        List<Conocimiento> conocimientoList = new ArrayList<>();
        Map<String, String> valores;


        //45: PRIMARIA (NIVEL EDUCATIVO ID)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.PRIMARIA); //(NIVEL EDUCATIVO ID)
        if (cargaMasiva != null) {
            String nivelEducativoPrimaria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(nivelEducativoPrimaria);

            if (rpta == null) {
                this.lstErrores.add(nivelEducativoPrimaria);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(nivelEducativoPrimaria)) { //ESTE VALOR SI SE PASA EN UNA CONSTANTE IGUAL A BD/ O MEJOR COD PROGRAMADOR
                    valores = splitCeldaCompuestoPuestoTipo("PRIMARIA", objectMapNivelEducativo);
                    formacionAcademicaPrimaria = settValorersDefault(perfil, token);
                    formacionAcademicaPrimaria.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }


        }

        //46: ESTADO_NIVEL_PRIMARIA (ESTADO NIVEL EDUCATIVO ID)
        if (ok) {
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_NIVEL_PRIMARIA); //(ESTADO NIVEL EDUCATIVO ID)
            if (cargaMasiva != null) {
                String estadoNivelEducativoPrimaria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estadoNivelEducativoPrimaria);


                if (rpta == null) {
                    this.lstErrores.add(estadoNivelEducativoPrimaria);
                    ok = false;
                }

                if (ok)
                    if (!Util.isEmpty(estadoNivelEducativoPrimaria)) {
                        valores = splitCeldaCompuestoPuestoTipo(estadoNivelEducativoPrimaria, objectMapEstadoNivelEducativo);
                        formacionAcademicaPrimaria.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));

                    }

            }
        }


        // 1: SE CARGA EL PRIMER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaPrimaria);

        //47: SECUNDARIA (NIVEL EDUCATIVO ID)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SECUNDARIA); //(NIVEL EDUCATIVO ID)

        if (cargaMasiva != null) {
            String nivelEducativoSecundaria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(nivelEducativoSecundaria);

            if (rpta == null) {
                this.lstErrores.add(nivelEducativoSecundaria);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(nivelEducativoSecundaria)) {
                    valores = splitCeldaCompuestoPuestoTipo("SECUNDARIA", objectMapNivelEducativo);
                    formacionAcademicaSecundaria = settValorersDefault(perfil, token);
                    formacionAcademicaSecundaria.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }

        }


        //48: ESTADO_NIVEL_SECUNDARIA (ESTADO NIVEL EDUCATIVO ID)
        if (ok) {
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_NIVEL_SECUNDARIA); //(ESTADO NIVEL EDUCATIVO ID)
            if (cargaMasiva != null) {
                String estadoNivelEducativoSecundaria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estadoNivelEducativoSecundaria);


                if (rpta == null) {
                    this.lstErrores.add(estadoNivelEducativoSecundaria);
                    ok = false;
                }

                if (ok)
                    if (!Util.isEmpty(estadoNivelEducativoSecundaria)) {
                        valores = splitCeldaCompuestoPuestoTipo(estadoNivelEducativoSecundaria, objectMapEstadoNivelEducativo);
                        formacionAcademicaSecundaria.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));

                    }

            }
        }


        // 2: SE CARGA EL SEGUNDO OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaSecundaria);


        //49: TECNICA_BASICA (NIVEL EDUCATIVO ID)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.TECNICA_BASICA);
        String tecnicaBasica = null;
        if (cargaMasiva != null) {
            tecnicaBasica = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(tecnicaBasica);

            if (rpta == null) {
                this.lstErrores.add(tecnicaBasica);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(tecnicaBasica)) {
                    valores = splitCeldaCompuestoPuestoTipo("TÉCNICA BÁSICA (1 o 2 años)", objectMapNivelEducativo); //CONSTANTE
                    formacionAcademicaTecnicaBasicaEgresado = settValorersDefault(perfil, token);
                    formacionAcademicaTecnicaBasicaEgresado.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }

        }


        //50: ESTADO_TECNICA_BASICA (ESTADO NIVEL EDUCATIVO ID)
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_TECNICA_BASICA); //(ESTADO NIVEL EDUCATIVO ID)
                if (cargaMasiva != null) {
                    String estadoTecnicaBasica = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(estadoTecnicaBasica);

                    if (rpta == null) {
                        this.lstErrores.add(estadoTecnicaBasica);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(estadoTecnicaBasica)) {
                            valores = splitCeldaCompuestoPuestoTipo(estadoTecnicaBasica, objectMapEstadoNivelEducativo);
                            formacionAcademicaTecnicaBasicaEgresado.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //51: SITUACION_TECNICA_BASICA_EGRESADO (SITUACION ACADEMICA ID)
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TECNICA_BASICA_EGRESADO); //(SITUACION ACADEMICA ID)
                if (cargaMasiva != null) {
                    String situacionTecBasiEgresado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situacionTecBasiEgresado);


                    if (rpta == null) {
                        this.lstErrores.add(situacionTecBasiEgresado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situacionTecBasiEgresado)) {
                            valores = splitCeldaCompuestoPuestoTipo("EGRESADO(A)", objectMapSituAcademica);
                            formacionAcademicaTecnicaBasicaEgresado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //52:ESTUDIOS_REQUERIDOS_TBASICA_EGRE
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ESTUDIOS_REQUERIDOS_TBASICA_EGRE);
                if (cargaMasiva != null) {
                    String estudiosRequeridosTecBasiEgresado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(estudiosRequeridosTecBasiEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(estudiosRequeridosTecBasiEgresado);
                        ok = false;
                    }

                    List<String> lstCarreras = listCarreras(rpta);
                    List<CarreraProfesional> carreraProfesionalList;
                    if (lstCarreras != null && !lstCarreras.isEmpty()) {
                        carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 56L);
                        formacionAcademicaTecnicaBasicaEgresado.setCarreraFormacionAcademicas(new ArrayList<>());

                        if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                            for (CarreraProfesional it : carreraProfesionalList) {
                                CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                                carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                carreraFormacionAcademica.setCarreraProfesional(it);
                                carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaTecnicaBasicaEgresado);
                                carreraFormacionAcademicaTecnicaBasicaEgresadoList.add(carreraFormacionAcademica);
                            }
                        }
                        formacionAcademicaTecnicaBasicaEgresado.setCarreraFormacionAcademicas(carreraFormacionAcademicaTecnicaBasicaEgresadoList);

                    }

                }
            }
        }


        //3: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaTecnicaBasicaEgresado);


        //53:GRAD_EDUC_TEC_BASICA_TITU_LICENCIATURA (SITUACION ACADEMICA ID)
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TEC_BASICA_TITU_LICENCIATURA); //(SITUACION ACADEMICA ID)
                if (cargaMasiva != null) {
                    String gradoEduTecBasTituLicenciatura = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(gradoEduTecBasTituLicenciatura);


                    if (rpta == null) {
                        this.lstErrores.add(gradoEduTecBasTituLicenciatura);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(gradoEduTecBasTituLicenciatura)) {
                            valores = splitCeldaCompuestoPuestoTipo("TÍTULO O LICENCIATURA", objectMapSituAcademica);

                            formacionAcademicaTecnicaBasicaTitulado = convertFormacionAcademica(formacionAcademicaTecnicaBasicaEgresado);
                            formacionAcademicaTecnicaBasicaTitulado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaTecnicaBasicaTitulado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));

                        }

                }
            }
        }


        //54:ESTU_REQ_TEC_BASICA_TITU
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ESTU_REQ_TEC_BASICA_TITU);
                String estudiosRequeridosTecSupTitulado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estudiosRequeridosTecSupTitulado);

                if (rpta == null) {
                    this.lstErrores.add(estudiosRequeridosTecSupTitulado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 58L);
                    formacionAcademicaTecnicaBasicaTitulado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaTecnicaBasicaTitulado);
                            carreraFormacionAcademicaTecnicaBasicaTituladoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaTecnicaBasicaTitulado.setCarreraFormacionAcademicas(carreraFormacionAcademicaTecnicaBasicaTituladoList);

                }
            }
        }

        //4: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaTecnicaBasicaTitulado);


        //55:TECNICA_SUPERIOR(NIVEL EDUCATIVO)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.TECNICA_SUPERIOR);
        String tecnicaSuperior = null;
        if (cargaMasiva != null) {
            tecnicaSuperior = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(tecnicaSuperior);

            if (rpta == null) {
                this.lstErrores.add(tecnicaSuperior);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(tecnicaSuperior)) {
                    valores = splitCeldaCompuestoPuestoTipo("TÉCNICA SUPERIOR (3 o 4 años)", objectMapNivelEducativo);
                    formacionAcademicaTecnicaSuperiorEgresado = settValorersDefault(perfil , token);
                    formacionAcademicaTecnicaSuperiorEgresado.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }

        }


        //56: SITUACION_TECNICA_SUPERIOR  (ESTADO NIVEL EDUCATIVC ID)
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaSuperior)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_TECNICA_SUPERIOR); // (ESTADO NIVEL EDUCATIVC ID)
                if (cargaMasiva != null) {
                    String estadoTecnicaSuperior = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(estadoTecnicaSuperior);

                    if (rpta == null) {
                        this.lstErrores.add(estadoTecnicaSuperior);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(estadoTecnicaSuperior)) {
                            valores = splitCeldaCompuestoPuestoTipo(estadoTecnicaSuperior, objectMapEstadoNivelEducativo);
                            formacionAcademicaTecnicaSuperiorEgresado.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //57:GRAD_EDUC_TEC_SUP_EGRESADO (SITUACION CADEMICA ID)
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaSuperior)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TEC_SUP_EGRESADO); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                    String situacionTecnicaSuperiorEgresado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situacionTecnicaSuperiorEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(situacionTecnicaSuperiorEgresado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situacionTecnicaSuperiorEgresado)) {
                            valores = splitCeldaCompuestoPuestoTipo("EGRESADO(A)", objectMapSituAcademica);
                            formacionAcademicaTecnicaSuperiorEgresado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }


        //58: ESTU_REQ_TEC_SUP_EGRESADO
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaSuperior)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.ESTU_REQ_TEC_SUP_EGRESADO);

                if (cargaMasiva != null) {
                    String estuReqTecnSupeEgresado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(estuReqTecnSupeEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(estuReqTecnSupeEgresado);
                        ok = false;
                    }

                    List<String> lstCarreras = listCarreras(rpta);
                    List<CarreraProfesional> carreraProfesionalList;
                    if (lstCarreras != null && !lstCarreras.isEmpty()) {
                        carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 56L);
                        formacionAcademicaTecnicaSuperiorEgresado.setCarreraFormacionAcademicas(new ArrayList<>());

                        if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                            for (CarreraProfesional it : carreraProfesionalList) {
                                CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                                carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                carreraFormacionAcademica.setCarreraProfesional(it);
                                carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaTecnicaSuperiorEgresado);
                                carreraFormacionAcademicaTecnicaSuperiorEgresadoList.add(carreraFormacionAcademica);
                            }
                        }
                        formacionAcademicaTecnicaSuperiorEgresado.setCarreraFormacionAcademicas(carreraFormacionAcademicaTecnicaSuperiorEgresadoList);

                    }

                }
            }
        }


        //5: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaTecnicaSuperiorEgresado);


        //59: GRAD_EDUC_TEC_SUP_TITU_LIC (situacion acdemica id)
        if (!Util.isEmpty(tecnicaSuperior)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TEC_SUP_TITU_LIC);
                if (cargaMasiva != null) {
                    String situTecSupTitulado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situTecSupTitulado);


                    if (rpta == null) {
                        this.lstErrores.add(situTecSupTitulado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situTecSupTitulado)) {
                            valores = splitCeldaCompuestoPuestoTipo("TÍTULO O LICENCIATURA", objectMapSituAcademica);
                            formacionAcademicaTecnicaSuperiorTitulado = convertFormacionAcademica(formacionAcademicaTecnicaSuperiorEgresado);
                            formacionAcademicaTecnicaSuperiorTitulado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));

                            formacionAcademicaTecnicaSuperiorTitulado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());


                        }

                }
            }
        }


        //60:EST_REQ_TEC_SUP_TITU
        if (!Util.isEmpty(tecnicaBasica)) {
            if ("X".equalsIgnoreCase(tecnicaBasica)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_TEC_SUP_TITU);
                String estudiosRequeridosTecSupTitulado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estudiosRequeridosTecSupTitulado);

                if (rpta == null) {
                    this.lstErrores.add(estudiosRequeridosTecSupTitulado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 58L);
                    formacionAcademicaTecnicaSuperiorTitulado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaTecnicaSuperiorTitulado);
                            carreraFormacionAcademicaTecnicaSuperiorTituladoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaTecnicaSuperiorTitulado.setCarreraFormacionAcademicas(carreraFormacionAcademicaTecnicaSuperiorTituladoList);

                }
            }
        }

        //6: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaTecnicaSuperiorTitulado);


        //61: UNIVERSITARIA (NIVEL EDUCATIVO)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.UNIVERSITARIA);
        String universitaria = null;
        if (cargaMasiva != null) {
            universitaria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(universitaria);

            if (rpta == null) {
                this.lstErrores.add(universitaria);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(universitaria)) {
                    valores = splitCeldaCompuestoPuestoTipo("UNIVERSITARIA", objectMapNivelEducativo);
                    formacionAcademicaUniversitariaEgresado = settValorersDefault(perfil, token);
                    formacionAcademicaUniversitariaEgresado.setNivelEducativoId(Long.valueOf(valores.get("id")));

                }

        }


        //62: SITUACION_UNIVERSITARIA (estado_nivel_educativo_id )
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.SITUACION_UNIVERSITARIA); // estado_nivel_educativo_id
                if (cargaMasiva != null) {
                    String estadoSituAcaUniverEgresado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(estadoSituAcaUniverEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(estadoSituAcaUniverEgresado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(estadoSituAcaUniverEgresado)) {
                            valores = splitCeldaCompuestoPuestoTipo(estadoSituAcaUniverEgresado, objectMapEstadoNivelEducativo);
                            formacionAcademicaUniversitariaEgresado.setEstadoNivelEducativoId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }

        //63: GRAD_EDUC_SUPE_EGRESADO ((SITUACION CADEMICA ID))
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUPE_EGRESADO); // ((SITUACION CADEMICA ID))
                if (cargaMasiva != null) {
                    String situAcaUniverEgresado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcaUniverEgresado);

                    if (rpta == null) {
                        this.lstErrores.add(situAcaUniverEgresado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcaUniverEgresado)) {
                            valores = splitCeldaCompuestoPuestoTipo("EGRESADO(A)", objectMapSituAcademica);
                            formacionAcademicaUniversitariaEgresado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));
                        }

                }
            }
        }

        //64: EST_REQ_EDU_SUPERIOR
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_EDU_SUPERIOR);
                String estuRequeUnivEgresado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estuRequeUnivEgresado);

                if (rpta == null) {
                    this.lstErrores.add(estuRequeUnivEgresado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 58L);
                    formacionAcademicaUniversitariaEgresado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaEgresado);
                            carreraFormacionAcademicaUniversitariaEgresadoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaEgresado.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaEgresadoList);

                }
            }
        }

        //7: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaUniversitariaEgresado);

        //65: GRAD_EDUC_SUP_BACHILLER ( SITUACION ACADEMICA)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_BACHILLER);
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_TEC_SUP_EGRESADO); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                    String situAcadeBachiller = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcadeBachiller);

                    if (rpta == null) {
                        this.lstErrores.add(situAcadeBachiller);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcadeBachiller)) {
                            valores = splitCeldaCompuestoPuestoTipo("BACHILLER", objectMapSituAcademica);
                            formacionAcademicaUniversitariaBachiller = convertFormacionAcademica(formacionAcademicaUniversitariaEgresado);
                            formacionAcademicaUniversitariaBachiller.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaBachiller.setSituacionAcademicaId(Long.valueOf(valores.get("id")));


                        }

                }
            }
        }

        // 66: EST_REQ_SUP_BACHILLER
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_SUP_BACHILLER);
                String estuReqBachiller = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estuReqBachiller);

                if (rpta == null) {
                    this.lstErrores.add(estuReqBachiller);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 57L);
                    formacionAcademicaUniversitariaBachiller.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaBachiller);
                            carreraFormacionAcademicaUniversitariaBachillerList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaBachiller.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaBachillerList);

                }
            }
        }

        //8: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaUniversitariaBachiller);


        // 67: GRAD_EDUC_SUP_TIT_LICENCIATURA (SITUACION CADEMICA ID)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_TIT_LICENCIATURA);
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_TIT_LICENCIATURA); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                    String situAcadeUniTitulado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcadeUniTitulado);

                    if (rpta == null) {
                        this.lstErrores.add(situAcadeUniTitulado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcadeUniTitulado)) {
                            valores = splitCeldaCompuestoPuestoTipo("TÍTULO O LICENCIATURA", objectMapSituAcademica);
                            formacionAcademicaUniversitariaTitulado = convertFormacionAcademica(formacionAcademicaUniversitariaEgresado);
                            formacionAcademicaUniversitariaMaestria.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaTitulado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));


                        }

                }
            }
        }

        //68: EST_REQ_SUP_TITU_LICENCIATURA
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_SUP_TITU_LICENCIATURA);
                String estuReqTitulado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estuReqTitulado);

                if (rpta == null) {
                    this.lstErrores.add(estuReqTitulado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 58L);
                    formacionAcademicaUniversitariaTitulado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaTitulado);
                            carreraFormacionAcademicaUniversitariaTituladoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaTitulado.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaTituladoList);

                }
            }
        }

        //9: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaUniversitariaTitulado);

        //69: GRAD_EDUC_SUP_MAESTRIA  (SITUACION CADEMICA ID)
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_MAESTRIA); //(SITUACION CADEMICA ID)
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_TIT_LICENCIATURA); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                    String situAcadeUniMaestria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcadeUniMaestria);

                    if (rpta == null) {
                        this.lstErrores.add(situAcadeUniMaestria);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcadeUniMaestria)) {
                            valores = splitCeldaCompuestoPuestoTipo("MAESTRÍA", objectMapSituAcademica);
                            formacionAcademicaUniversitariaMaestria = convertFormacionAcademica(formacionAcademicaUniversitariaEgresado);
                            formacionAcademicaUniversitariaMaestria.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaMaestria.setSituacionAcademicaId(Long.valueOf(valores.get("id")));


                        }

                }
            }
        }

        //70: EST_REQ_MAESTRIA
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_MAESTRIA);
                String estuReqUniMAestria = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estuReqUniMAestria);

                if (rpta == null) {
                    this.lstErrores.add(estuReqUniMAestria);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 59L);
                    formacionAcademicaUniversitariaMaestria.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaMaestria);
                            carreraFormacionAcademicaUniversitariaMaestriaList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaMaestria.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaMaestriaList);

                }
            }
        }


        //71: GRAD_EDUC_SUP_DOCTORADO
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_DOCTORADO); //(SITUACION CADEMICA ID)
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.GRAD_EDUC_SUP_TIT_LICENCIATURA); //SITUACION CADEMICA ID
                if (cargaMasiva != null) {
                    String situAcadeUniDoctorado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                    String rpta = validarErrorSubstring(situAcadeUniDoctorado);

                    if (rpta == null) {
                        this.lstErrores.add(situAcadeUniDoctorado);
                        ok = false;
                    }

                    if (ok)
                        if (!Util.isEmpty(situAcadeUniDoctorado)) {
                            valores = splitCeldaCompuestoPuestoTipo("DOCTORADO", objectMapSituAcademica);
                            formacionAcademicaUniversitariaDoctorado = convertFormacionAcademica(formacionAcademicaUniversitariaEgresado);
                            formacionAcademicaUniversitariaDoctorado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            formacionAcademicaUniversitariaDoctorado.setSituacionAcademicaId(Long.valueOf(valores.get("id")));


                        }

                }
            }
        }

        //72: EST_REQ_DOCTORADO
        if (!Util.isEmpty(universitaria)) {
            if ("X".equalsIgnoreCase(universitaria)) {
                cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.EST_REQ_DOCTORADO);
                String estuReqUniDoctorado = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(estuReqUniDoctorado);

                if (rpta == null) {
                    this.lstErrores.add(estuReqUniDoctorado);
                    ok = false;
                }

                List<String> lstCarreras = listCarreras(rpta);
                List<CarreraProfesional> carreraProfesionalList;
                if (lstCarreras != null && !lstCarreras.isEmpty()) {
                    carreraProfesionalList = carreraProfesionalRepository.findByDescripcionInAndEstadoRegistroAndIdDetalle(lstCarreras, Constantes.ACTIVO , 60L);
                    formacionAcademicaUniversitariaDoctorado.setCarreraFormacionAcademicas(new ArrayList<>());

                    if (carreraProfesionalList != null && !carreraProfesionalList.isEmpty()) {
                        for (CarreraProfesional it : carreraProfesionalList) {
                            CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
                            carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                            carreraFormacionAcademica.setCarreraProfesional(it);
                            carreraFormacionAcademica.setFomarcionAcademica(formacionAcademicaUniversitariaDoctorado);
                            carreraFormacionAcademicaUniversitariaDoctoradoList.add(carreraFormacionAcademica);
                        }
                    }
                    formacionAcademicaUniversitariaDoctorado.setCarreraFormacionAcademicas(carreraFormacionAcademicaUniversitariaDoctoradoList);

                }
            }
        }



        // 73: MAESTRIA_SITUACION
        if (formacionAcademicaUniversitariaMaestria.getSituacionAcademicaId() != null) {
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.MAESTRIA_SITUACION);
            if (cargaMasiva != null) {
                String maestriaSituacion = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(maestriaSituacion);

                if (rpta == null) {
                    this.lstErrores.add(maestriaSituacion);
                    ok = false;
                }

                if (ok)
                    if (!Util.isEmpty(maestriaSituacion)) {
                        valores = splitCeldaCompuestoPuestoTipo("GRADO", objectMapGrados);
                        formacionAcademicaUniversitariaMaestria.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        formacionAcademicaUniversitariaMaestria.setEstadoSituacionAcademicaId(Long.valueOf(valores.get("id")));
                    }

            }
        }


        //74: MAESTRIA_DESCRIPCION
        if (formacionAcademicaUniversitariaMaestria.getSituacionAcademicaId() != null) {
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.MAESTRIA_DESCRIPCION);
            if (cargaMasiva != null) {
                String maestriaDescri = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(maestriaDescri);

                if (rpta == null) {
                    this.lstErrores.add(maestriaDescri);
                    ok = false;
                }

                if (ok)
                    if (!Util.isEmpty(maestriaDescri)) {
                        formacionAcademicaUniversitariaMaestria.setNombreGrado(maestriaDescri);
                    }

            }
        }

        //10: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaUniversitariaMaestria);

        // 75: DOCTORADO_SITUACION
        if (formacionAcademicaUniversitariaDoctorado.getSituacionAcademicaId() != null) {
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.DOCTORADO_SITUACION);
            if (cargaMasiva != null) {
                String doctoradoSituacion = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(doctoradoSituacion);

                if (rpta == null) {
                    this.lstErrores.add(doctoradoSituacion);
                    ok = false;
                }

                if (ok)
                    if (!Util.isEmpty(doctoradoSituacion)) {
                        valores = splitCeldaCompuestoPuestoTipo("EGRESADO", objectMapGrados);
                        formacionAcademicaUniversitariaDoctorado.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        formacionAcademicaUniversitariaDoctorado.setEstadoSituacionAcademicaId(Long.valueOf(valores.get("id")));
                    }

            }
        }


        //76: DESCRIPCION_DOCTORADO
        if (formacionAcademicaUniversitariaDoctorado.getSituacionAcademicaId() != null) {
            cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.DESCRIPCION_DOCTORADO);
            if (cargaMasiva != null) {
                String doctoradoDescri = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

                String rpta = validarErrorSubstring(doctoradoDescri);

                if (rpta == null) {
                    this.lstErrores.add(doctoradoDescri);
                    ok = false;
                }

                if (ok)
                    if (!Util.isEmpty(doctoradoDescri)) {
                        formacionAcademicaUniversitariaDoctorado.setNombreGrado(doctoradoDescri);
                    }

            }
        }

        //11: SE CARGA EL TERCER OBJETO A LA LISTA lstFormAcademica
        lstFormAcademica.add(formacionAcademicaUniversitariaDoctorado);

       // formacionAcademicaRepository.saveAll(lstFormAcademica);




        //77:COLEGIATURA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.COLEGIATURA);
        if (cargaMasiva != null) {
            String colegiatura = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(colegiatura);

            if (rpta == null) {
                this.lstErrores.add(colegiatura);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(colegiatura)) {
                   if (colegiatura.equalsIgnoreCase("INCOMPLETA")){
                       perfil.setIndColegiatura("0");
                   }

                    if (colegiatura.equalsIgnoreCase("COMPLETA")){
                        perfil.setIndColegiatura("1");
                    }

                }

        }


        //78: HABILITACION_PROFESIONAL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.HABILITACION_PROFESIONAL);
        if (cargaMasiva != null) {
            String habilitacionProfe = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(habilitacionProfe);

            if (rpta == null) {
                this.lstErrores.add(habilitacionProfe);
                ok = false;
            }

            if (ok)
                if (!Util.isEmpty(habilitacionProfe)) {
                    if (habilitacionProfe.equalsIgnoreCase("INCOMPLETA")){
                        perfil.setIndHabilitacionProf("0");
                    }

                    if (habilitacionProfe.equalsIgnoreCase("COMPLETA")){
                        perfil.setIndHabilitacionProf("1");
                    }
                }

        }

        //79:CONOCIMIENTOS_TECNICOS
        List<String> listConcocimientos = new ArrayList<>();
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.CONOCIMIENTOS_TECNICOS);
        if (cargaMasiva != null) {
            String conociTecnicos = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(conociTecnicos);

            if (rpta == null) {
                this.lstErrores.add(conociTecnicos);
                ok = false;
            }

           listConcocimientos = listCarreras(rpta);
            List<MaeConocimiento> maeConocimientoList = maeConocimientoRepository.findByDescripcionInAndEstadoRegistro(listConcocimientos, Constantes.ACTIVO);
            if (maeConocimientoList != null && !maeConocimientoList.isEmpty()) {
                for (MaeConocimiento it : maeConocimientoList) {
                    Conocimiento conocimiento = new Conocimiento();
                    conocimiento.setMaeConocimiento(it);
                    conocimiento.setPerfil(perfil);
                    conocimiento.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    conocimientoList.add(conocimiento);
                }
            }


        }




        //80: CURSOS
        listConcocimientos = new ArrayList<>();
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.CURSOS);
        if (cargaMasiva != null) {
            String cursos = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(cursos);

            if (rpta == null) {
                this.lstErrores.add(cursos);
                ok = false;
            }

            listConcocimientos = listCarreras(rpta);

        }

        //81: HORAS_CURSOS
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.HORAS_CURSOS);

        if (cargaMasiva != null) {
            String horaCursos = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(horaCursos);

            if (rpta == null) {
                this.lstErrores.add(horaCursos);
                ok = false;
            }


            List<String> horas = listCarreras(rpta);
            if (horas != null && !horas.isEmpty()) {
                if (listConcocimientos != null && !listConcocimientos.isEmpty()) {
                    if (listConcocimientos.size() == horas.size()) {
                        int cont = 0;
                        List<MaeConocimiento> maeConocimientoList = maeConocimientoRepository.findByDescripcionInAndEstadoRegistro(listConcocimientos, Constantes.ACTIVO);
                        if (maeConocimientoList != null && !maeConocimientoList.isEmpty()) {
                            for (MaeConocimiento it : maeConocimientoList) {
                                Conocimiento conocimiento = new Conocimiento();
                                conocimiento.setMaeConocimiento(it);
                                conocimiento.setHoras(Double.valueOf(horas.get(cont)).intValue());
                                conocimiento.setPerfil(perfil);
                                conocimiento.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                conocimientoList.add(conocimiento);
                                cont++;
                            }
                        }
                    }
                }
            }

        }


        //82: PROGRAMAS
        listConcocimientos = new ArrayList<>();
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.PROGRAMAS);
        if (cargaMasiva != null) {
            String programas = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(programas);

            if (rpta == null) {
                this.lstErrores.add(programas);
                ok = false;
            }

            listConcocimientos = listCarreras(rpta);


        }

        //83: HORAS_PROGRAMAS
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.HORAS_PROGRAMAS);

        if (cargaMasiva != null) {
            String horasProgramadas = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(horasProgramadas);

            if (rpta == null) {
                this.lstErrores.add(horasProgramadas);
                ok = false;
            }

            List<String> horas = listCarreras(rpta);
            if (horas != null && !horas.isEmpty()) {
                if (listConcocimientos != null && !listConcocimientos.isEmpty()) {
                    if (listConcocimientos.size() == horas.size()) {
                        int cont = 0;
                        List<MaeConocimiento> maeConocimientoList = maeConocimientoRepository.findByDescripcionInAndEstadoRegistro(listConcocimientos, Constantes.ACTIVO);
                        if (maeConocimientoList != null && !maeConocimientoList.isEmpty()) {
                            for (MaeConocimiento it : maeConocimientoList) {
                                Conocimiento conocimiento = new Conocimiento();
                                conocimiento.setMaeConocimiento(it);
                                conocimiento.setHoras(Double.valueOf(horas.get(cont)).intValue());
                                conocimiento.setPerfil(perfil);
                                conocimiento.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                conocimientoList.add(conocimiento);
                                cont++;
                            }
                        }
                    }
                }
            }


        }


        //84: CONOCIMIENTOS_OFIMATICA
        listConcocimientos = new ArrayList<>();
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.CONOCIMIENTOS_OFIMATICA);
        if (cargaMasiva != null) {
            String conociOfimatica = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(conociOfimatica);

            if (rpta == null) {
                this.lstErrores.add(conociOfimatica);
                ok = false;
            }

            listConcocimientos = listCarreras(rpta);


        }

        //85: HORAS_CONO_OFIMATICA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.HORAS_CONO_OFIMATICA);

        if (cargaMasiva != null) {
            String horasConoOfimatica = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(horasConoOfimatica);

            if (rpta == null) {
                this.lstErrores.add(horasConoOfimatica);
                ok = false;
            }

            List<String> horas = listCarreras(rpta);
            if (horas != null && !horas.isEmpty()) {
                if (listConcocimientos != null && !listConcocimientos.isEmpty()) {
                    if (listConcocimientos.size() == horas.size()) {
                        int cont = 0;
                        List<MaeConocimiento> maeConocimientoList = maeConocimientoRepository.findByDescripcionInAndEstadoRegistro(listConcocimientos, Constantes.ACTIVO);
                        if (maeConocimientoList != null && !maeConocimientoList.isEmpty()) {
                            for (MaeConocimiento it : maeConocimientoList) {
                                Conocimiento conocimiento = new Conocimiento();
                                conocimiento.setMaeConocimiento(it);
                                conocimiento.setHoras(Double.valueOf(horas.get(cont)).intValue());
                                conocimiento.setPerfil(perfil);
                                conocimiento.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                conocimientoList.add(conocimiento);
                                cont++;
                            }
                        }
                    }
                }
            }


        }

        //86:IDIOMAS
        listConcocimientos = new ArrayList<>();
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.IDIOMAS);
        if (cargaMasiva != null) {
            String idiomas = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(idiomas);

            if (rpta == null) {
                this.lstErrores.add(idiomas);
                ok = false;
            }

            listConcocimientos = listCarreras(rpta);



        }

        //87: HORAS_IDIOMAS
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NIVEL_IDIOMA);

        if (cargaMasiva != null) {
            String horasIdiomas = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(horasIdiomas);

            if (rpta == null) {
                this.lstErrores.add(horasIdiomas);
                ok = false;
            }

                if (listConcocimientos != null && !listConcocimientos.isEmpty()) {
                        List<MaeConocimiento> maeConocimientoList = maeConocimientoRepository.findByDescripcionInAndEstadoRegistro(listConcocimientos, Constantes.ACTIVO);
                        if (maeConocimientoList != null && !maeConocimientoList.isEmpty()) {
                            for (MaeConocimiento it : maeConocimientoList) {
                                Conocimiento conocimiento = new Conocimiento();
                                conocimiento.setMaeConocimiento(it); //1: basico 2://intermedio 33://avanzado
                                if (horasIdiomas.equalsIgnoreCase("BASICO")) {
                                    conocimiento.setNivelDominioId(1l);
                                }
                                if (horasIdiomas.equalsIgnoreCase("INTERMEDIO")) {
                                    conocimiento.setNivelDominioId(1l);
                                }
                                if (horasIdiomas.equalsIgnoreCase("AVANZADO")) {
                                    conocimiento.setNivelDominioId(1l);
                                }
                                conocimiento.setPerfil(perfil);
                                conocimiento.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                                conocimientoList.add(conocimiento);

                            }
                        }

                }


        }


        //88: OBSERVACION
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.OBSERVACION);
        if (cargaMasiva != null) {
            String observacion = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);

            String rpta = validarErrorSubstring(observacion);

            if (rpta == null) {
                this.lstErrores.add(observacion);
                ok = false;
            }

            perfil.setObservaciones(observacion);
        }


       for (FormacionAcademica it : lstFormAcademica){
           it.setPerfil(perfil);
           it.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
       }

        perfil.setFormacionAcademicaList(lstFormAcademica);
        perfil.setConocimientoList(conocimientoList);




        return perfil;
    }


    private FormacionAcademica convertFormacionAcademica(FormacionAcademica formacionAcademicaTecnicaBasica) {
        FormacionAcademica formacionAcademica = new FormacionAcademica();
        formacionAcademica.setNivelEducativoId(formacionAcademicaTecnicaBasica.getNivelEducativoId());
        formacionAcademica.setEstadoNivelEducativoId(formacionAcademicaTecnicaBasica.getEstadoNivelEducativoId());

        return formacionAcademica;
    }

    @Transactional(transactionManager = "convocatoriaTransactionManager")
    private boolean guardarBD( Map<String, Object> perfilMap, Map<String, Object> perfilFuncionMap, Long regimenId, Long entidadId,
    		MyJsonWebToken token) {

    	 boolean rpta = true;

		if (!Util.isEmpty(perfilMap)) {
			Perfil perfilDTO = (Perfil) perfilMap.get("perfil");
			boolean rpta1 = (Boolean) perfilMap.get("ok");

			PerfilFuncion pFuncionDTO = (PerfilFuncion) perfilFuncionMap.get("perfilFuncion");
			List<FuncionDetalle> lstFunDet = (List<FuncionDetalle>) perfilFuncionMap.get("perfilFuncionDetalle");
			boolean rpta2 = (Boolean) perfilFuncionMap.get("ok");

			try {
				// PERFIL
				perfilDTO.setRegimenLaboralId(regimenId);
				perfilDTO.setEntidadId(entidadId);
				perfilDTO.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				Perfil p = perfilRepository.save(perfilDTO);

				// PERFIL FUNCION
				pFuncionDTO.setPerfilId(p.getPerfilId());
				pFuncionDTO.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				PerfilFuncion pe = perfilFuncionRepository.save(pFuncionDTO);

				// PERFIL FUNCION DETALLE
				lstFunDet.stream().filter(Objects::nonNull).forEach(d -> {
					//d.setPerfilFuncion(pe);
					d.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				});
				funcionDetalleRepository.saveAll(lstFunDet);
				// FORMACION ACADEMICA
				// EXPERIENCIA

			} catch (Exception e) {
				e.printStackTrace();
				rpta = false;
			}

		}

    	return rpta;
    }

    private List<PerfilExperiencia> settTabPerfilExp(Perfil perfil , CargaMasiva cargaMasiva, HSSFRow hssfFilaExcel, XSSFRow xssfFilaExcel, int fila,
                                                 boolean ok, Map<String, Object> objectMap , MyJsonWebToken token) throws Exception {
        PerfilExperiencia perfilExperiencia = new PerfilExperiencia();
        perfilExperiencia.setPerfilExperienciaDetalleList1(new ArrayList<>());
        perfilExperiencia.setPerfilExperienciaDetalleList2(new ArrayList<>());

        // 1: AÑOS DE EXPERIENCIA GENERAL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.AÑOS_EXPERIENCIA_GENERAL);

        Map<String, String> valores;
        if (cargaMasiva != null) {

            String años_eg = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(años_eg);

            if (rpta == null) {
                this.lstErrores.add(años_eg);
                ok = false;
            }

            if (ok)
	            if (!Util.isEmpty(años_eg)) {
	            	perfilExperiencia.setAnioExpTotal(Double.valueOf(años_eg).longValue());
	            }
        }

        // 2: MESES DE EXPERIENCIA GENERAL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.MESES_EXPERIENCIA_GENERAL);


        if (cargaMasiva != null) {

            String meses_eg = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(meses_eg);

            if (rpta == null) {
                this.lstErrores.add(meses_eg);
                ok = false;
            }

            if (ok)
	            if (!Util.isEmpty(meses_eg)) {
	            	perfilExperiencia.setMesExpTotal(Double.valueOf(meses_eg).longValue());
	            }
        }

        // 3: AÑOS DE EXPERIENCIA ESPECÍFICA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.AÑOS_EXPERIENCIA_ESPECÍFICA);


        if (cargaMasiva != null) {

            String años_ee = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(años_ee);

            if (rpta == null) {
                this.lstErrores.add(años_ee);
                ok = false;
            }

            if (ok)
	            if (!Util.isEmpty(años_ee)) {
	            	perfilExperiencia.setAnioExReqPuesto(Double.valueOf(años_ee).longValue());
	            }
        }

        // 4: MESES DE EXPERIENCIA ESPECÍFICA
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.MESES_EXPERIENCIA_ESPECÍFICA);


        if (cargaMasiva != null) {

            String meses_ee = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(meses_ee);

            if (rpta == null) {
                this.lstErrores.add(meses_ee);
                ok = false;
            }

            if (ok)
	            if (!Util.isEmpty(meses_ee)) {
	            	perfilExperiencia.setMesExReqPuesto(Double.valueOf(meses_ee).longValue());
	            }
        }

        // 5: AÑOS DE EXPERIENCIA SECTOR PÚBLICO
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.AÑOS_EXPERIENCIA_SECTOR_PÚBLICO);

        if (cargaMasiva != null) {

            String años_esp = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(años_esp);

            if (rpta == null) {
                this.lstErrores.add(años_esp);
                ok = false;
            }

            if (ok)
	            if (!Util.isEmpty(años_esp)) {
	            	perfilExperiencia.setAnioExpSecPub(Double.valueOf(años_esp).longValue());
	            }
        }

        // 6: MESES DE EXPERIENCIA GENERAL
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.MESES_EXPERIENCIA_SECTOR_PÚBLICO);


        if (cargaMasiva != null) {

            String meses_esp = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(meses_esp);

            if (rpta == null) {
                this.lstErrores.add(meses_esp);
                ok = false;
            }

            if (ok)
	            if (!Util.isEmpty(meses_esp)) {
	            	perfilExperiencia.setMesExpSecPub(Double.valueOf(meses_esp).longValue());
	            }
        }

     // 7: NIVEL MÍNIMO DEL PUESTO
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.NIVEL_MÍNIMO_PUESTO);


        if (cargaMasiva != null) {

            String nivel_minimo = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(nivel_minimo);

            if (rpta == null) {
                this.lstErrores.add(nivel_minimo);
                ok = false;
            }

            if (ok)
            	   if (!Util.isEmpty(nivel_minimo)) {
                       if (cargaMasiva.getCompuesto().equalsIgnoreCase("1")) {
                           valores = splitCeldaNivelMinimoPuesto(nivel_minimo, objectMapNivelMinimoPuesto);
                           perfilExperiencia.setNivelMinPueId(Long.valueOf(valores.get("id")).longValue());
                       }
                   }

        }

		// 8: OTROS ASPECTOS COMPLEMENTARIOS
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.OTROS_ASPECTOS_COMPLEMENTARIOS);

        if (cargaMasiva != null) {

            String aspectos = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(aspectos);

            if (rpta == null) {
                this.lstErrores.add(aspectos);
                ok = false;
            }

            if (ok)
	            if (!Util.isEmpty(aspectos)) {
	            	perfilExperiencia.setAspectos(aspectos);
	            }
        }

		// 9: HABILIDADES O COMPETENCIAS
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.HABILIDADES_COMPETENCIAS);

        if (cargaMasiva != null) {

            String habilidades = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(habilidades);

            if (rpta == null) {
                this.lstErrores.add(habilidades);
                ok = false;
            }

            if (ok)
	            if (!Util.isEmpty(habilidades)) {
	                perfilExperiencia.setPerfilExperienciaDetalleList1(splitCeldaHabilidadesCompetencia(habilidades,perfilExperiencia , token));
	            }
        }

        // 10: TIPO DE REQUISITOS ADICIONALES
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.TIPO_REQUISITOS_ADICIONALES);
        String requisitos="";

        if (cargaMasiva != null) {

            requisitos = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(requisitos);

            if (rpta == null) {
                this.lstErrores.add(requisitos);
                ok = false;
                requisitos="";
            }

        }

        // 11: DESCRIPCIÓN DE REQUISITOS ADICIONALES
        cargaMasiva = (CargaMasiva) objectMap.get(ConstantesExcel.DESCRIPCION_REQUISITOS);


        if (cargaMasiva != null) {

            String descripcionRequisito = validacion30057.obtenerValorCelda(hssfFilaExcel, xssfFilaExcel, fila, cargaMasiva, this.hssFormulaEvaluator, this.xssFormulaEvaluator);
            String rpta = validarErrorSubstring(descripcionRequisito);
             if (rpta == null) {
                this.lstErrores.add(descripcionRequisito);
                ok = false;
                descripcionRequisito = "";
            }

            descripcionRequisito = validacion30057.validarRequisitoDescripcion(requisitos, descripcionRequisito, cargaMasiva, fila);
            String rpta2 = validarErrorSubstring(descripcionRequisito);
            if (rpta2 == null) {
               this.lstErrores.add(descripcionRequisito);
               ok = false;
               descripcionRequisito = "";
           }

            if (ok)
            	   if (!Util.isEmpty(descripcionRequisito)) {
            		   perfilExperiencia.setPerfilExperienciaDetalleList2(splitCeldaRequisitoAd(requisitos, descripcionRequisito, objectMapTipoRequisitoAd,perfilExperiencia , token));
                   }


        }


        List<PerfilExperiencia> ls = new ArrayList<>();
        perfilExperiencia.setPerfilId(perfil.getPerfilId());
        perfilExperiencia.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        ls.add(perfilExperiencia);
    	return ls;

    }

    /**
     * @param valor
     * @return
     */
    private List<String> listCarreras(String valor) {
        if (!Util.isEmpty(valor)) {
            String[] str = valor.trim().split(",");
            String[] array = Arrays.stream(str).map(String::trim).toArray(String[]::new);
            return Util.convertArrayToList(array);
        }
        return null;
    }

    /**
     * @param valor
     * @return
     */
    private List<String> listHoras(String valor) {
        if (!Util.isEmpty(valor)) {
            String[] str = valor.trim().split(",");
            String[] array = Arrays.stream(str)
                    .map(String::trim)
                    .toArray(String[]::new);
            return Util.convertArrayToList(array);
        }
        return null;
    }

    /**
     * VALORES POR DEFECTO
     *
     * @param perfil
     * @return
     */
    private FormacionAcademica settValorersDefault(Perfil perfil, MyJsonWebToken token) {
        FormacionAcademica formacionAcademica = new FormacionAcademica();
        formacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        formacionAcademica.setPerfil(perfil);
        return formacionAcademica;
    }


}
