package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.request.dto.FuncionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilFuncionDTO;

import java.util.List;

public interface PerfilFuncionRepositoryJdbc {
    List<PerfilFuncionDTO> listPerfilFuncion (Long perfilId);
    List<FuncionDetalleDTO> listPerfilFuncionDetalle (Long perfilFuncion);


}
