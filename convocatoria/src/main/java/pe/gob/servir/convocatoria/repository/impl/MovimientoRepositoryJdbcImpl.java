package pe.gob.servir.convocatoria.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.servir.convocatoria.repository.MovimientoRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.MovimientoDTO;
import pe.gob.servir.convocatoria.request.dto.ObservacionDTO;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MovimientoRepositoryJdbcImpl implements MovimientoRepositoryJdbc {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public MovimientoRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_FIND_MOVIMIENTOS =  "select base_movi_id as movimientoId , usuario_creacion as userCreacion , fecha_creacion as fechaCreacion , \n" +
            "base_id as baseId , rol_id as rolId  , entidad_id as entidadId , estado_old_id as estadoOldId , estado_new_id as estadoNewId\n" +
            "from sch_base.tbl_base_movi tbm  where tbm.base_id = :baseId  order by fecha_creacion desc " ;

    private static final String SQL_FIND_OBSERVACIONES =  "select base_obs_id as baseObsId ,  etapa as etapa , base_movi_id as movimientoId ,  observacion as observacion from sch_base.tbl_base_obs tbo where tbo.base_movi_id  = :baseMovId" ;


    @Override
    public List<MovimientoDTO> listMovimientos(Long baseId, Long movId) {
        List<MovimientoDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_MOVIMIENTOS);

            if (!Util.isEmpty(movId)) sql.append(" and tbm.base_movi_id  = :movId ");

            if (!Util.isEmpty(baseId)) objectParam.put("baseId", baseId);
            if (!Util.isEmpty(movId)) objectParam.put("movId", movId);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(MovimientoDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<ObservacionDTO> listObservacionDtos(Long movId) {
        List<ObservacionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_OBSERVACIONES);
            if (!Util.isEmpty(movId)) objectParam.put("baseMovId", movId);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(ObservacionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }
}
