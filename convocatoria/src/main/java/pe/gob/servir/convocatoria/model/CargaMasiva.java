package pe.gob.servir.convocatoria.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tbl_carga_masiva_perfil", schema = "sch_convocatoria")
@Getter
@Setter
public class CargaMasiva {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_carga_masiva_perfil")
    @SequenceGenerator(name = "seq_carga_masiva_perfil", sequenceName = "seq_carga_masiva_perfil", schema = "sch_convocatoria", allocationSize = 1)
    @Column(name = "carga_masiva_perfil_id")
    private Long id;

    @Column(name = "posicion_columna_excel")
    private Integer posicion;

    @Column(name = "nom_columna_excel")
    private String nombreColumna;

    @Column(name = "numero_decimales")
    private Long numeroDecimales;

    @Column(name = "tipo_columna")
    private String tipoColummna;

    @Column(name = "fl_valida")
    private String valida;

    @Column(name = "patron")
    private String patron;

    @Column(name = "regimen")
    private String regimen;

    @Column(name = "compuesto")
    private String compuesto;
    
    @Column(name = "longitud")
    private Integer longitud;

}

