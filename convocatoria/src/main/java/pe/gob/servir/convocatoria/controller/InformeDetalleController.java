package pe.gob.servir.convocatoria.controller;

import com.lowagie.text.DocumentException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.InformeDetalleDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCriterioEvaluacion;
import pe.gob.servir.convocatoria.response.RespInformeDetalle;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.InformeDetalleService;
import pe.gob.servir.convocatoria.service.PdfConstructorService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;

@RestController
@Tag(name = "InformeDetalle", description = "")
public class InformeDetalleController {

    private static final String INFORMEDETALLE = "informedetalle";

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private InformeDetalleService informeDetalleService;

    private final PdfConstructorService pdfConstructorService;

    @Autowired
    public InformeDetalleController(PdfConstructorService pdfConstructorService) {
        this.pdfConstructorService = pdfConstructorService;
    }

    @Operation(summary = "list Informe Detalle", description = " Informe detalle plantillas", tags = {""},
            security = {@SecurityRequirement(name = "bearer-key")}
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "operación exitosa"),
            @ApiResponse(responseCode = "500", description = "error interno", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})
    })
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/informedetalle/{entidadId}"},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<RespBase<RespObtieneLista<InformeDetalleDTO>>> listInformeDetalle(
            @PathVariable String access,
            @PathVariable Long entidadId,
            @RequestParam(value = "tipoInfo", required = false) Long tipoInfo,
            @RequestParam(value = "titulo", required = false) String titulo,
            @RequestParam(value = "estado", defaultValue = "1", required = false) String estado,
            @RequestParam(value = "idInforme", required = false) Long idInforme
    ) {
        RespBase<RespObtieneLista<InformeDetalleDTO>> response = informeDetalleService.listAllInformeDetalleDto(entidadId, tipoInfo, estado, titulo, idInforme);
        return ResponseEntity.ok(response);
    }

    @Operation(summary = "list Bonificacion ", description = "List Bonificacion por Informe Detalle", tags = {""},
            security = {@SecurityRequirement(name = "bearer-key")}
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "operación exitosa"),
            @ApiResponse(responseCode = "500", description = "error interno", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})
    })
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/informedetalle/bonificacion"},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<RespBase<RespObtieneLista<BonificacionDTO>>> listBonificacion(
            @PathVariable String access,
            @RequestParam(value = "idInformeDetalle") Long idInformeDetalle

    ) {
        RespBase<RespObtieneLista<BonificacionDTO>> response = informeDetalleService.listAllBonificacionDto(idInformeDetalle);
        return ResponseEntity.ok(response);
    }


    @Operation(summary = "Cambiar estado Informe Detalle ", description = "Cambiar estado Informe Detalle", tags = {""}, security = {
            @SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
            }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
            })})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/informedetalle/cambioestado"}, consumes = {
            MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<InformeDetalleDTO>> cambiarEstadoInformeDetalle(@PathVariable String access,
                                                                                   @RequestParam(value = "idInformeDetalle") Long idInformeDetalle,
                                                                                   @RequestParam(value = "estado") String estado) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<InformeDetalleDTO> response = informeDetalleService.cambiarEstado(idInformeDetalle, estado.trim(), jwt);
        return ResponseEntity.ok(response);
    }


    @Operation(summary = "Crea un Informe Detalle", description = "Crea un Informe Detalle", tags = {""}, security = {
            @SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
            }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
            })})
    @PostMapping(path = {Constantes.BASE_ENDPOINT + "/informedetalle"}, consumes = {
            MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespInformeDetalle>> registrarInformeDetalle(@PathVariable String access,
                                                                                @Valid @RequestBody ReqBase<InformeDetalleDTO> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<RespInformeDetalle> response = informeDetalleService.guardarInformeDetalle(request, jwt);
        return ResponseEntity.ok(response);
    }

    @Operation(summary = "Actualiza un Informe Detalle", description = "Actualiza un Informe Detalle", tags = {""}, security = {
            @SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
            }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))
            })})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/informedetalle"}, consumes = {
            MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespInformeDetalle>> actualizarInformeDetalle(@PathVariable String access,
                                                                                 @Valid @RequestBody ReqBase<InformeDetalleDTO> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<RespInformeDetalle> response = informeDetalleService.actualizarInformeDetalle(request, jwt);
        return ResponseEntity.ok(response);
    }


    @Operation(summary = "Informe detalle pdf", description = " Informe detalle pdf", tags = {""},
            security = {@SecurityRequirement(name = "bearer-key")})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "operación exitosa"),
            @ApiResponse(responseCode = "500", description = "error interno",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/"+INFORMEDETALLE +"/pdf/{id}"})
    public ResponseEntity<RespBase<String>> generarInformePdf
            (@PathVariable String access, @PathVariable Long id) throws DocumentException, IOException {

        byte[] bytes = pdfConstructorService.getPdfFromInformeDetalle(id);

        String encodedString =
                Base64.getEncoder().withoutPadding().encodeToString(bytes);
        RespBase<String> payload = new RespBase<>();
        payload.setPayload(encodedString);

        return ResponseEntity.ok(payload);

    }
    
    @Operation(summary = Constantes.SUM_OBT_LIST+"documentos e informes", description = Constantes.SUM_OBT_LIST+"documentos e informe - Paso 4", 
			tags = { "" },security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/"+INFORMEDETALLE +"/documentoInforme/{idEntidad}/{tipoInfoId}"},
			produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<RespCriterioEvaluacion>>> listarDocumentosInforme(
			@PathVariable String access,
			@PathVariable Long idEntidad,
			@PathVariable Long tipoInfoId){
		RespBase<RespObtieneLista<RespCriterioEvaluacion>> response = informeDetalleService.listarDocumentosInforme(idEntidad , tipoInfoId );
		return ResponseEntity.ok(response);
	
	}
    
    @Operation(summary = Constantes.SUM_OBT_LIST+"documentos e informe detalle", description = Constantes.SUM_OBT_LIST+"documentos e informe detalle", 
			tags = { "" },security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/"+INFORMEDETALLE +"/criteriosEvaluacion/{idEntidad}/{tipoInfoId}"}, 
			produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<RespCriterioEvaluacion>>> listarDocumentosInformeDetalle(
			@PathVariable String access,
			@PathVariable Long idEntidad,
			@PathVariable Long tipoInfoId){
		RespBase<RespObtieneLista<RespCriterioEvaluacion>> response = informeDetalleService.listarInformeDetalle(idEntidad, tipoInfoId);
		return ResponseEntity.ok(response);
	
	}
}
