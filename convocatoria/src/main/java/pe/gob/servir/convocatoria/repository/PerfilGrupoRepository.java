package pe.gob.servir.convocatoria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pe.gob.servir.convocatoria.model.PerfilGrupo;

public interface PerfilGrupoRepository extends JpaRepository<PerfilGrupo, Long>{

	 @Query(" select f from PerfilGrupo f where f.estado ='1'")
	 public List<PerfilGrupo> listarPerfilesActivos();


}
