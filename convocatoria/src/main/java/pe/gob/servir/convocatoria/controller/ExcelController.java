package pe.gob.servir.convocatoria.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.service.ExcelService;

@RestController
@Tag(name = "Excel", description = "")
public class ExcelController {

	@Autowired
	private ExcelService excelService;
	
	@Operation(summary = "Descarga Plantilla Regimen 30057", description = "Descarga Plantilla Perfil Regimen 30057", tags = {""},
			   security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", 
			content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/regimen/30057/{idEntidad}/download" })
	public @ResponseBody ResponseEntity<RespBase<String>> downloadPerfil30057
						   (@PathVariable String access,@PathVariable Long idEntidad) throws IOException {
		 RespBase<String> response = excelService.descargarPlantillaRegimen30057(idEntidad);
		return ResponseEntity.ok(response);
		 
	}
	
	@Operation(summary = "Descarga Plantilla Perfil Otros Regimen", description = "Descarga Plantilla Otros Regimen", tags = {""},
			   security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", 
			content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/regimen/otros/{idEntidad}/download" })
	public @ResponseBody ResponseEntity<RespBase<String>> downloadPerfilOtros
						   (@PathVariable String access,@PathVariable Long idEntidad) throws IOException {
		 RespBase<String> response = excelService.descargarPlantillaOtrosRegimen(idEntidad);
		return ResponseEntity.ok(response);
		 
	}
	
	@Operation(summary = "Descarga Plantilla Practicas", description = "Descarga Plantilla Perfil Practicas", tags = {""},
			   security = { @SecurityRequirement(name = "bearer-key") })
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "operación exitosa"),
			@ApiResponse(responseCode = "500", description = "error interno", 
			content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT + "/perfil/regimen/practicas/{idEntidad}/download" })
	public @ResponseBody ResponseEntity<RespBase<String>> downloadPracticas
						   (@PathVariable String access,@PathVariable Long idEntidad) throws IOException {
		 RespBase<String> response = excelService.descargarPlantillaPracticas(idEntidad);
		return ResponseEntity.ok(response);
		 
	}
	
}
