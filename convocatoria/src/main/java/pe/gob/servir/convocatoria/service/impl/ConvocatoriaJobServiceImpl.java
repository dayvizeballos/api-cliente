package pe.gob.servir.convocatoria.service.impl;


import java.time.Instant;
import java.time.LocalDate;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.VariablesSistema;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.Convocatoria;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.Movimiento;
import pe.gob.servir.convocatoria.repository.BaseRepository;
import pe.gob.servir.convocatoria.repository.ConvocatoriaRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.MovimientoRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGeneraToken;
import pe.gob.servir.convocatoria.request.dto.ApiUploadFile;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespEntidad;
import pe.gob.servir.convocatoria.service.ConvocatoriaJobService;
import pe.gob.servir.convocatoria.service.ReporteBase;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

@Service
public class ConvocatoriaJobServiceImpl implements ConvocatoriaJobService{

	private static final Logger logger = LoggerFactory.getLogger(ConvocatoriaJobServiceImpl.class);
	
	@Autowired
	ConvocatoriaRepository convocatoriaRepository;
	
	@Autowired
	BaseRepository baseRepository;
	
	@Autowired
	MaestraDetalleRepository maestraDetalleRepository;
	
	@Autowired
	MovimientoRepository movimientoRepository;
	
	@Autowired
	EntidadClient entidadClient;
	
	@Autowired
	ReporteBase reporteBaseService;
	
	@Autowired
	VariablesSistema variableSistema;
	
	@Override
	public void procesaConvocatoria(Long convocatoriaId) {
		Convocatoria convocatoria = new Convocatoria();
		int resActBase = 0;
		int resActConv = 0;
		int resRegMov = 0; 
		//OBTENER CONVOCATORIA
		try {
			Optional<Convocatoria> findConvocatoria= convocatoriaRepository.findById(convocatoriaId);
			if(findConvocatoria.isPresent()) {
				convocatoria = findConvocatoria.get();
			}

			resActConv = this.actualizaConvocatoria(convocatoria);
			if(resActConv == 1) {
				resActBase = this.actualizaBase(convocatoria.getBase().getBaseId());
				if(resActBase == 1) {
					resRegMov = this.registraMovimiento(convocatoria.getBase());
				}
			}		
					
		} catch (Exception e) {
			logger.info(e.getMessage());
		}	
			
	}
	
	
	public Long findCorrelativo(Convocatoria c, Long anio) {
		Long correlativo;
		
		try {
			correlativo = convocatoriaRepository.findMaxConvocatoria(c.getRegimen(), anio, c.getEntidadId());
		} catch (Exception e) {
			correlativo = null;
			logger.info(e.getMessage());
		}
		
		return correlativo;
	}
	
	public String findCodigoConvocatoria(Convocatoria c, Long correlativo) {
		String codigo = null;
		//String entidad = null;
		String sCorrelativo = null;
		LocalDate date = LocalDate.now();
		try {
			// OBTENER ENTIDAD
			sCorrelativo = Util.completarCaracteresIzq(correlativo.intValue(), "0", "5");
			
			codigo = c.getRegimen().getSigla() + " N° " + sCorrelativo + "-" + String.valueOf(date.getYear()) + "-" + c.getSiglaEntidad() ;
			
		} catch (Exception e) {
			codigo = null;
			logger.info(e.getMessage());
		}

		return codigo;
	}
	
	public int actualizaBase(Long baseId) {
		
		try {
			
			List<MaestraDetalle> lstEtapaBase  = maestraDetalleRepository.findDetalleByCodProg(Constantes.COD_TABLA_SITUACION_ETAPA, Constantes.COD_PRO_MA_DET_PUBLICADA);
			
			Long idPublicado = lstEtapaBase.get(0).getMaeDetalleId();
			
			Optional<Base>  baseFind = baseRepository.findById(baseId);
			Base base = new Base();
			if(baseFind.isPresent()) {
				base = baseFind.get();
				base.setEtapaId(idPublicado);
				base.setCampoSegUpd(base.getEstadoRegistro(), Constantes.USER_JOB,Instant.now());
				baseRepository.save(base);
				
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			return 0;
		}
		return 1;
	}
	
	public int registraMovimiento(Base base) {
		
		try {
			
			List<MaestraDetalle> lstEstado  = maestraDetalleRepository.
					findDetalleByCodProg(Constantes.COD_TABLA_SITUACION_ETAPA, Constantes.COD_PRO_MA_DET_PUBLICADA);
			Long etapaIdNew = lstEstado.get(0).getMaeDetalleId();
			
			Movimiento mov = new Movimiento();
			mov.setBaseId(base.getBaseId());
			mov.setEntidadId(base.getEntidadId());
			mov.setEstadoOldId(base.getEtapaId());
			mov.setEstadoNewId(etapaIdNew);
			mov.setCampoSegIns(Constantes.USER_JOB, Instant.now());
			movimientoRepository.save(mov);
			
		} catch (Exception e) {
			logger.info(e.getMessage());
			return 0;
		}
		
		return 1;
	}
	
	public int actualizaConvocatoria(Convocatoria convocatoria) {
		
		LocalDate date = LocalDate.now();
		Long correlativo;
		Long anio;
		String codigo = null;
		try {
			anio = Long.valueOf(date.getYear());
			correlativo = findCorrelativo(convocatoria, anio);
			codigo = this.findCodigoConvocatoria(convocatoria, correlativo);
			
			convocatoria.setAnio(anio);
			convocatoria.setCorrelativo(correlativo);
			convocatoria.setCodigoConvocatoria(codigo);
			convocatoria.setCampoSegUpd(convocatoria.getEstadoRegistro(),Constantes.USER_JOB,  Instant.now());
			convocatoriaRepository.save(convocatoria);
		} catch (Exception e) {
			logger.info(e.getMessage());
			return 0;
		}
		
		return 1;
	}
	
	
	 public String generateToken() {
	        try {
	            ReqBase<ReqGeneraToken> api = new ReqBase<>();
	            ReqGeneraToken reqGeneraToken = new ReqGeneraToken();
	            reqGeneraToken.setGrantType(variableSistema.jobGrant);
	            api.setPayload(reqGeneraToken);
	            RestTemplate restTemplate = new RestTemplate();

	            String fooResourceUrl = variableSistema.jobOauthurl;
	            HttpHeaders headers = new HttpHeaders();
	            headers.setBasicAuth(variableSistema.jobBasicAuth);
	            HttpEntity<ReqBase<ReqGeneraToken>> request = new HttpEntity<>(api, headers);
	            ResponseEntity<RespBase> response = restTemplate.exchange(fooResourceUrl, HttpMethod.POST, request, RespBase.class);
	            if (response.getBody().getPayload() != null) {
	                Map<String, Object> oauth = (Map<String, Object>) response.getBody().getPayload();
	                if (oauth.containsKey(Constantes.ACCESS_TOKEN_KEY)) {
	                    return (String) oauth.get(Constantes.ACCESS_TOKEN_KEY);
	                }
	            }
	        } catch (Exception e) {
	            logger.info(e.getMessage());
	            return null;
	        }
	        return null;
	    }
	 
	 public String obtenerParamaetro(String tipoParametro, String codigoTexto) {

			String parametro = null;

			try {

				RestTemplate restTemplate = new RestTemplate();
				String fooResourceUrl = variableSistema.jobApiMaestraParametro;
				HttpHeaders headers = new HttpHeaders();
				headers.setBearerAuth(generateToken());

				fooResourceUrl = fooResourceUrl.replace(Constantes.PATH_PARAMETRO, tipoParametro);
				HttpEntity<String> request = new HttpEntity<String>(headers);
				ResponseEntity<RespBase> response = restTemplate.exchange(fooResourceUrl, HttpMethod.GET, request, RespBase.class);

				HashMap<String, Object> payload = (HashMap<String, Object>) response.getBody().getPayload();
				if (payload.containsKey("items")) {
					List<Map<String, Object>> listBeforeGroup = (List<Map<String, Object>>) payload.get("items");
					for (Map<String, Object> entry : listBeforeGroup) {
						if (entry.get("codigoTexto").equals(codigoTexto)) {
							parametro = (String) entry.get("valorTexto");
							break;
						}
					}
				}
				logger.info("Descripcion Parametro " + parametro);
			} catch (Exception e) {
				logger.info(e.getMessage());
				parametro = "";
			}


			return parametro;
		}
	 
	 
	 public String encodePDF(Long baseId) {
		 
		 String encodedString = null;
		 try {
			 byte[] bytes = reporteBaseService.generatePdfFromHtmlBase(baseId);
			 encodedString = Base64.getEncoder().withoutPadding().encodeToString(bytes);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		 return encodedString;
	 }

	 public String subirArchivo(Convocatoria convocatoria) {
		 
		 String url = "";
		 String encode = "";
		 String pathTmp = "";
		 String rutaFileServer = "";
		 
		 try {
				logger.info("Inicio subirArchivo()");
				RestTemplate restTemplate = new RestTemplate();
				String fooResourceUrl = variableSistema.jobApiMaestraUpload;
				HttpHeaders headers = new HttpHeaders();
				headers.setBearerAuth(this.generateToken());

				RespBase<ApiUploadFile> requestApiUploadFile = new RespBase<ApiUploadFile>();
				encode = this.encodePDF(convocatoria.getBase().getBaseId());

				ApiUploadFile uploadFile = new ApiUploadFile();
				pathTmp = this.obtenerParamaetro(Constantes.PARAMETRO_FILE, Constantes.PAR_CODIGO_TEXTO); //Definir path para convocatoria
				rutaFileServer = ParametrosUtil.datePathReplaceRepositoryAlfresco(pathTmp);
				rutaFileServer = rutaFileServer.replace(Constantes.PATH_CONVOCATORIA, convocatoria.getConvocatoriaId().toString());
 

				uploadFile.setExtension(Constantes.EXTENSION_PDF);
				uploadFile.setFileBase64(encode);
				uploadFile.setFileName((convocatoria.getRegimen().getSigla() + convocatoria.getCorrelativo()).toLowerCase()); //otbener el correlativo y regimen
				uploadFile.setPath(rutaFileServer);
				requestApiUploadFile.setPayload(uploadFile);

				try {
					HttpEntity<RespBase<ApiUploadFile>> request = new HttpEntity<>(requestApiUploadFile, headers);
					ResponseEntity<RespBase> response = restTemplate.exchange(fooResourceUrl, HttpMethod.POST, request,
							RespBase.class);
					logger.info("Se subio correctamente el archivo");

					HashMap<String, Object> payload = (HashMap<String, Object>) response.getBody().getPayload();
					if (payload.containsKey("pathRelative")) {
						url = (String) payload.get("pathRelative");
					}
					logger.info("Obtener Ruta generaArchivo " + pathTmp);
				} catch (Exception e) {
					logger.info(e.getMessage());
				}
				logger.info("Fin subirArchivo()");

			} catch (Exception e) {
				logger.info(e.getMessage());
			}
		 
		 return url;
	 }

	@Override
	@Transactional(transactionManager = "convocatoriaTransactionManager")
	public void generaArchivo(Long convocatoriaId, Long baseId) {
		
		LocalDate date = LocalDate.now();
		Convocatoria convocatoria = new Convocatoria();
		String rutaArchivo = null;
		
		try {
			Optional<Convocatoria> findConvocatoria= convocatoriaRepository.findById(convocatoriaId);
			if(findConvocatoria.isPresent()) {
				convocatoria = findConvocatoria.get();
				
				rutaArchivo = subirArchivo(convocatoria); 
				if(rutaArchivo == null || rutaArchivo.isEmpty()) {					
					throw new Exception("Error en la subida de archivo: " + convocatoriaId);
				}
				List<MaestraDetalle> lstEstadoConvocatoria  = maestraDetalleRepository.findDetalleByCodProg(Constantes.COD_TABLA_EST_CONVOCATORIA, Constantes.COD_ESTADO_EN_PROCESO);
				MaestraDetalle estado = lstEstadoConvocatoria.get(0);

				convocatoria.setEstadoConvocatoria(estado);
				convocatoria.setUrl(rutaArchivo);
				convocatoria.setIndiArchivo("1");
				convocatoria.setCampoSegUpd(convocatoria.getEstadoRegistro(), Constantes.USER_JOB,  Instant.now());
				convocatoriaRepository.save(convocatoria);
			}else {
				throw new Exception("Error no existe la convocatoria: " + convocatoriaId);
			}
			
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
	}
		
}
