package pe.gob.servir.convocatoria.service;

import java.util.Map;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqComunicado;
import pe.gob.servir.convocatoria.request.ReqEstadoComunicado;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface ComunicadoService {
    RespBase<RespComunicado> creaComunicado(ReqBase<ReqComunicado> request, MyJsonWebToken token);

    RespBase<RespObtieneLista<RespComunicadoDTO>> findComunicado(Map<String, Object> parametroMap);
    
    RespBase<Object> eliminarComunicado(MyJsonWebToken token, Long comunicadoId);
    
    RespBase<RespComunicado> actualizaComunicado(ReqBase<ReqComunicado> request, MyJsonWebToken token, Long comunicadoId);
    
    RespBase<RespComunicado> obtenerComunicado(Long comunicadoId);
    
    RespBase<RespComunicado> actualizaEstadoComunicado(ReqBase<ReqEstadoComunicado> request, MyJsonWebToken token, Long comunicadoId);

    RespBase<RespObtieneLista<RutaComunicadoDTO>> getRutaComunicado( Long perfilId , Long baseId);
}
