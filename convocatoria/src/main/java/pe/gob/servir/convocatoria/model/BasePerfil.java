package pe.gob.servir.convocatoria.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_base_perfil", schema = "sch_base")
@Getter
@Setter
public class BasePerfil extends AuditEntity implements AuditableEntity {


	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_perfil")
    @SequenceGenerator(name = "seq_base_perfil", sequenceName = "seq_base_perfil", schema = "sch_base", allocationSize = 1)
	@Column(name = "base_perfil_id")
	private Long basePerfilId;
	
	@Column(name = "perfil_id")
	private Long perfilId;
	
	@Column(name = "jornada_trabajo")
	private String jornadaLaboral;
	
	@Column(name = "nro_vacante")
	private Integer vacante;
	
	@Column(name = "remuneracion")
	private Double remuneracion;
	
	@Column(name = "ind_cont_indeterminado")
	private String indContratoIndeterminado;
	
	@Column(name = "duracion_contrato")
	private Integer tiempoContrato;
	
	@Column(name = "condicion_trabajo_id")
	private Long condicionTrabajoId;
	
	@Column(name = "sede_id")
	private Long sedeId;
	
	@Column(name = "condiciones_esenciales")
	private String condicionEsencial;
	
	@Column(name = "observaciones")
	private String observaciones;
	
	@Column(name = "sede_direccion")
	private String sedeDireccion;
	
	@Column(name = "modalidad_contrato_id")
	private Long modalidadContratoId;
	
	@JoinColumn(name="base_id")
	@ManyToOne(optional = false)
	@JsonBackReference
	private Base base;
	
    @OneToMany(cascade= CascadeType.ALL ,  mappedBy = "basePerfil")
    @Column(nullable = true)
    @JsonManagedReference
    private List<BaseHorario> baseHorarioList;
    
    @Column(name = "depa_Id")
    private Integer depaId;
    
    @Column(name = "descDepa")
    private String descDepa;
    
    @Column(name = "prov_Id")
    private Integer provId;
    
    @Column(name = "descProv")
    private String descProv;
    
    @Column(name = "dist_Id")
    private Integer distId;
    
    @Column(name = "descDist")
    private String descDist;
    
}
