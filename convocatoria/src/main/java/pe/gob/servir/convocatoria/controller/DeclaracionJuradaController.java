package pe.gob.servir.convocatoria.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.response.DeclaracionJuradaEspecificaDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.dto.RespObtenerDDJJByPerfilIdDTO;
import pe.gob.servir.convocatoria.service.DeclaracionJuaradaService;

@RestController
@Tag(name = "Declaracion Juarada", description = "")
public class DeclaracionJuradaController {

    @Autowired
    private DeclaracionJuaradaService declaracionJuaradaService;

    @Operation(summary = Constantes.LISTA+" la declaracion jurada especifica por baseId", description = Constantes.LISTA+" la declaracion jurada especifica por baseId", tags = { "" }, security = {
            @SecurityRequirement(name = Constantes.BEARER_JWT) })
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
    @GetMapping(path = { Constantes.BASE_ENDPOINT + "/declaracion/{baseId}" },produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<RespBase<RespObtieneLista>> listarDeclaracionJuaradaEspecifica(@PathVariable String access,
                                                                          @PathVariable Long baseId) {
        RespBase<RespObtieneLista> response = declaracionJuaradaService.listarDeclaraJuaradaEspecifica(baseId);
        return ResponseEntity.ok(response);
    }

    @Operation(summary = Constantes.LISTA+" los items de DDJJ por idPerfil", description = Constantes.LISTA+" los items de DDJJ por idPerfil", tags = { "" }, security = {
            @SecurityRequirement(name = Constantes.BEARER_JWT) })
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
    @GetMapping(path = { Constantes.BASE_ENDPOINT + "/declaracionJurada/{idPerfil}/{baseId}" },produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<RespBase<RespObtenerDDJJByPerfilIdDTO>> listarDeclaracionJuaradaByPerfilId(@PathVariable String access,
                                                                                                     @PathVariable Long idPerfil,
                                                                                                     @PathVariable Long baseId) {
        RespBase<RespObtenerDDJJByPerfilIdDTO> response = declaracionJuaradaService.listarDeclaraJuradaByPerfilId(idPerfil, baseId);
        return ResponseEntity.ok(response);
    }

}
