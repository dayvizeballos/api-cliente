package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;

import lombok.Data;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.dto.PostulanteDTO;

@Data
public class ReqDesestimaContrato {
	
	@NotNull(message = Constantes.CAMPO + " categoriaDesestimientoId " + Constantes.ES_OBLIGATORIO)
	private Long categoriaDesestimientoId;
	
	@NotNull(message = Constantes.CAMPO + " motivoDesestimiento " + Constantes.ES_OBLIGATORIO)
	private String motivoDesestimiento;
	
	@NotNull(message = Constantes.CAMPO + " estadoId " + Constantes.ES_OBLIGATORIO)
	private Long estadoId;
	
	@NotNull(message = Constantes.CAMPO + " pdfBase64 " + Constantes.ES_OBLIGATORIO)
	private String pdfBase64;
	
	@NotNull(message = Constantes.CAMPO + " tipoTrabajo " + Constantes.ES_OBLIGATORIO)
	private String tipoTrabajo;
	
	@NotNull(message = Constantes.CAMPO + " postulante " + Constantes.ES_OBLIGATORIO)
	private PostulanteDTO postulante;

}
