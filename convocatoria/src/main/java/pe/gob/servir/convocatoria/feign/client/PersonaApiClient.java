package pe.gob.servir.convocatoria.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pe.gob.servir.convocatoria.response.RespBase;

@FeignClient(name = "personaApi", url = "${persona.private.base.url}")
public interface PersonaApiClient {

	// @formatter:off
	@RequestMapping(method = RequestMethod.GET, value = "/v1/personas/documentoQuery", 
			produces = {MediaType.APPLICATION_JSON_VALUE })
	RespBase<Object> obtienePersonaPorDocumento(
			@RequestParam(value = "tipoDocumento") Integer tipoDocumento,
			@RequestParam(value = "numeroDocumento") String numeroDocumento);
	// @formatter:on
}
