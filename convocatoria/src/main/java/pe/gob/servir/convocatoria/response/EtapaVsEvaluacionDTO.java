package pe.gob.servir.convocatoria.response;

import java.io.Serializable;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class EtapaVsEvaluacionDTO implements Serializable {
	
	private String etapa;
	private String periodo;
	private String nota;
	private Integer califica;

}
