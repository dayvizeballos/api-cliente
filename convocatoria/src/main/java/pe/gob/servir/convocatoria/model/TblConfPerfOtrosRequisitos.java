package pe.gob.servir.convocatoria.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "tbl_conf_perf_otros_requisitos", schema = "sch_convocatoria")
@NamedQuery(name="TblConfPerfOtrosRequisitos.findAll", query="SELECT t FROM TblConfPerfOtrosRequisitos t")
public class TblConfPerfOtrosRequisitos extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conf_perf_otros_requisitos_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_otros_requisitos")
    @SequenceGenerator(name = "seq_conf_perf_otros_requisitos", sequenceName = "seq_conf_perf_otros_requisitos", schema = "sch_convocatoria", allocationSize = 1)
    private Long confPerfOtrosRequisitosId;

    @ManyToOne
    @JoinColumn(name = "perfil_id", nullable = false)
    private Perfil perfil;

    @Column(name = "descripcion", nullable = false)
    private String descripcion;

    @Column(name = "requisitos_adicionales")
    private Boolean requisitosAdicionales;

    @Column(name = "experiencia_detalle_id")
    private Long experienciaDetalleId;

    @Column(name = "tipo_dato_exper_id")
    private Long tipoDatoExperId;

    @Column(name = "requisito_id")
    private Long requisitoId;

    @Column(name = "perfil_experiencia_id")
    private Long perfilExperienciaId;

    @Column(name = "base_id")
    private Long baseId;


    public TblConfPerfOtrosRequisitos() {
        super();
    }
}
