package pe.gob.servir.convocatoria.repository;


import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.request.dto.*;

import java.util.List;
import java.util.Map;


public interface BaseRepositoryJdbc {
    List<DatosConcursoDTO> listDatosConcursoDtos(Long base , Long datoConcursoId);
    List<ObtenerBaseDTO> buscarBaseByFilter(Map<String, Object> parametroMap);
    BaseDTO getBase (Long idBase, Long entidadId);
    boolean updateBase(Base base , Long etapa);
    List<MaestraDetalleDto> maestraDetalle (String codpMaDetalle , String codCabMaeCabe);
    List<CondicionDto> getCambioCondicion(Long baseId , Long etapa);
    Long isObservado(Long idBase);
    List<Long> idEntidades();
    List<CorreoDTO> listDetalleCorreo(Long entidadId , Long estadoProceso , String fechaHoy);
    String codigoConvocatoria(BaseDTO baseDTO);


}
