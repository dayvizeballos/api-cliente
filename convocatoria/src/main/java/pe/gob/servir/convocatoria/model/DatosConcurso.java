package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

@Entity
@Table(name = "tbl_base_datos_concurso", schema = "sch_base")
@Getter
@Setter
public class DatosConcurso  extends AuditEntity implements AuditableEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_datos_consurso")
    @SequenceGenerator(name = "seq_base_datos_consurso", sequenceName = "seq_base_datos_consurso", schema = "sch_base", allocationSize = 1)
	@Column(name = "dato_concurso_id")
	private Long datoConcursoId;

	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "objetivo")
	private String objetivo;
	
	@Column(name = "organo_responsable_id")
	private Long organoResponsableId;
	
	@Column(name = "unidad_organica_id")
	private Long unidadOrganicaId;
	
	@Column(name = "organo_encargado_id")
	private Long organoEncargadoId;
	
	@Column(name = "correo")
	private String correo;
	
	@Column(name = "nro_vacantes")
	private Integer nroVacantes;
	
	@Column(name = "informe_detalle_id")
	private Long informeDetalleId;

	@Column(name = "tipo_practica_id")
	private Long tipoPracticaId;

	@Column(name = "informe_especifica_id")
	private Long informeEspecificaId;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "anexo")
	private String anexo;
	
    @JoinColumn(name="base_id")
    @ManyToOne(optional = false)
    @JsonBackReference
    private Base base;
    
	@Column(name = "nombre_organo")
	private String nombreOrgano;
	
	@Column(name = "nombre_unidad_organica")
	private String nombreUnidadOrganica;
	

}
