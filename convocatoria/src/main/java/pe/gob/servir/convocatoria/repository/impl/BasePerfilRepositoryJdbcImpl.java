package pe.gob.servir.convocatoria.repository.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import pe.gob.servir.convocatoria.repository.BasePerfilRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.BaseHorarioDTO;
import pe.gob.servir.convocatoria.request.dto.BasePerfilDTO;
import pe.gob.servir.convocatoria.util.Util;

@Repository
public class BasePerfilRepositoryJdbcImpl implements BasePerfilRepositoryJdbc{

	@Autowired
    JdbcTemplate jdbcTemplate;
    
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    
    private static final  String SQL_GET_BASE_PERFIL ="select p.base_perfil_id as basePerfilId, p.base_id as baseId, pe.nombre_puesto as nombrePerfil, p.perfil_id as perfilId,\r\n"
    		+ "	p.jornada_trabajo as jornadaLaboral, p.nro_vacante as vacante, p.remuneracion as remuneracion, \r\n"
    		+ "	p.ind_cont_indeterminado as indContratoIndeterminado, p.duracion_contrato as tiempoContrato,\r\n"
    		+ "	p.condicion_trabajo_id as condicionTrabajoId, p.sede_id as sedeId, p.sede_direccion as sedeDireccion,\r\n"
    		+ "	p.condiciones_esenciales as condicionEsencial, p.observaciones as observaciones, p.modalidad_contrato_id as modalidadContratoId,\r\n"
    		+ " p.estado_registro as estado, coalesce(p.depa_Id,0) as depaId, p.descDepa as descDepa, \r\n"
    		+ " coalesce(p.prov_Id,0) as provId, p.descProv as descProv, coalesce(p.dist_Id,0) as distId, p.descDist as descDist \r\n"
    		+ "	from sch_base.tbl_base_perfil p\r\n"
    		+ " inner join sch_convocatoria.tbl_perfil pe on pe.perfil_id = p.perfil_id\r\n"
    		+ "where p.base_perfil_id = :basePerfilId";
    
    private static final String SQL_GET_BASE_HORARIO ="select h.base_horario_id as baseHorarioId, h.hora_ini as horaIni, h.hora_fin as horaFin, \r\n"
    		+ "	 h.frecuencia_id as frecuenciaId,\r\n"
    		+ "	 h.estado_registro as estado\r\n"
    		+ "from sch_base.tbl_base_horario h\r\n"
    		+ "where h.base_perfil_id = :basePerfilId ";
    
	@Override
	public BasePerfilDTO getBasePerfil(Long basePerfilId) {
		BasePerfilDTO basePerfil = new BasePerfilDTO();
		try {
			 StringBuilder sql = new StringBuilder();
			 Map<String, Object> objectParam = new HashMap<>();
			 sql.append(SQL_GET_BASE_PERFIL);
			 if (!Util.isEmpty(basePerfilId)) objectParam.put("basePerfilId", basePerfilId);
			 basePerfil =  this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(BasePerfilDTO.class)).get(0);
			 return basePerfil;
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
			return basePerfil;
		}
		
	}

	@Override
	public List<BaseHorarioDTO> getBaseHorarioList(Long basePerfilId) {
		List<BaseHorarioDTO> baseHorarioList = new ArrayList<>();
		try {
			 StringBuilder sql = new StringBuilder();
			 Map<String, Object> objectParam = new HashMap<>();
			 sql.append(SQL_GET_BASE_HORARIO);
			 if (!Util.isEmpty(basePerfilId)) objectParam.put("basePerfilId", basePerfilId);
			 baseHorarioList = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(BaseHorarioDTO.class));
			return baseHorarioList;
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
			return baseHorarioList;
		}
		
	}

}
