package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespPdfInforme {
	
	private byte[] pdfByte;

}
