package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.model.*;
import pe.gob.servir.convocatoria.request.dto.*;

import java.util.function.Function;

@FunctionalInterface
public interface PerfilFunction {

	public String main ();
	
	Function<FormacionAcademica, FormacionAcademicaDTO> convertirFormacionAcademica = (FormacionAcademica p) -> {
		FormacionAcademicaDTO formacionDTO = new FormacionAcademicaDTO();
		formacionDTO.setFormacionAcademicaId(p.getFormacionAcademicaId());
		formacionDTO.setNivelEducativoId(p.getNivelEducativoId());
		formacionDTO.setEstadoNivelEducativoId(p.getEstadoNivelEducativoId()== null ? null : p.getEstadoNivelEducativoId());
		formacionDTO.setSituacionAcademicaId(p.getSituacionAcademicaId()== null ? null : p.getSituacionAcademicaId());
		formacionDTO.setEstadoSituacionAcademicaId(p.getEstadoSituacionAcademicaId()== null ? null : p.getEstadoSituacionAcademicaId());
		formacionDTO.setNombreGrado(p.getNombreGrado()== null ? null : p.getNombreGrado().trim());
		formacionDTO.setEstado(p.getEstadoRegistro() == null ? null : p.getEstadoRegistro().trim());
		return formacionDTO;
	};
	
	
	Function<CarreraFormacionAcademica, CarreraFormacionAcademicaDTO> convertirCarreraFormacion = (CarreraFormacionAcademica p) -> {
		CarreraFormacionAcademicaDTO carreraDTO = new CarreraFormacionAcademicaDTO();
		carreraDTO.setCarreraFormacionAcademicaId(p.getCarreraAcademicaId());
		carreraDTO.setEstado(p.getEstadoRegistro() == null ? null : p.getEstadoRegistro().trim() );
		carreraDTO.setDescripcion(p.getDescripcion() == null ? null : p.getDescripcion().trim());
		return carreraDTO;
	};
	
	Function<Conocimiento, ConocimientoDTO> convertirConocimiento = (Conocimiento p) ->{
		ConocimientoDTO conocimientoDTO = new ConocimientoDTO();
		conocimientoDTO.setPerfilId(p.getPerfil().getPerfilId());
		conocimientoDTO.setRegimenLaboralId(p.getPerfil().getRegimenLaboralId());
		conocimientoDTO.setPerfilConocimientoId(p.getPerfilConocimientoId());
		conocimientoDTO.setHoras(p.getHoras() == null ? null : p.getHoras());
		conocimientoDTO.setNivelDominioId(p.getNivelDominioId()== null ? null : p.getNivelDominioId());
		conocimientoDTO.setDescrConocimiento(p.getMaeConocimiento()== null ? null : p.getMaeConocimiento().getDescripcion());
		conocimientoDTO.setEstado(p.getEstadoRegistro() == null ? null : p.getEstadoRegistro().trim());
		conocimientoDTO.setMaeConocimientoId(p.getMaeConocimiento() == null ? null : p.getMaeConocimiento().getMaeConocimentoId());
		conocimientoDTO.setTipoConocimientoId(p.getMaeConocimiento() == null ? null : p.getMaeConocimiento().getTipoConocimientoId());
		return conocimientoDTO;
	};
	
	Function<FuncionDetalle, FuncionDetalleDTO> convertirFuncionDetalle = (FuncionDetalle p) -> {
		FuncionDetalleDTO funcionDTO = new FuncionDetalleDTO();
		funcionDTO.setFuncionDetalleId(p.getFuncionDetalleId());
		funcionDTO.setDescripcion(p.getDescripcion() == null ? null : p.getDescripcion().trim());
		funcionDTO.setOrden(p.getOrden() == null ? null : p.getOrden());
		funcionDTO.setEstado(p.getEstadoRegistro() == null ? null : p.getEstadoRegistro());
		return funcionDTO;
	};

	Function<FuncionDetalleDTO ,FuncionDetalle > convertirDtoFuncionDetalle = (FuncionDetalleDTO p) -> {
		FuncionDetalle funcionDetalle = new FuncionDetalle();
		funcionDetalle.setFuncionDetalleId(p.getFuncionDetalleId() == null ? null : p.getFuncionDetalleId());
		funcionDetalle.setDescripcion(p.getDescripcion() == null ? null : p.getDescripcion().trim());
		funcionDetalle.setOrden(p.getOrden() == null ? null : p.getOrden());

		return funcionDetalle;
	};
	
	Function<ServidorCivil, ServidorCivilDTO> convertirServidorCivil = (ServidorCivil p) ->{
		ServidorCivilDTO servidorDTO = new ServidorCivilDTO();
		servidorDTO.setPerfilServidorId(p.getPerfilServidorCivilId() ==  null ? null :p.getPerfilServidorCivilId());
		servidorDTO.setServidorCivilId(p.getServidorCiviId());

		return servidorDTO;
	};

	Function<ServidorCivilDTO , ServidorCivil > convertirDtoServidorCivil = (ServidorCivilDTO p) ->{
		ServidorCivil servidor = new ServidorCivil();
		servidor.setPerfilServidorCivilId (p.getPerfilServidorId());
		servidor.setServidorCiviId(p.getServidorCivilId() == null ? null :p.getServidorCivilId());

		return servidor;
	};
	
	Function<PerfilFuncion, PerfilFuncionDTO> convertirPerfilFuncion = (PerfilFuncion p) -> {
		PerfilFuncionDTO perfilFuncionDTO = new PerfilFuncionDTO();
		perfilFuncionDTO.setPerfilFuncionId(p.getPerfilFuncionId());
		perfilFuncionDTO.setPerfilId(p.getPerfilId());
		perfilFuncionDTO.setCondicionAtipica(p.getCondicionAticipica() == null ? null : p.getCondicionAticipica().trim());
		perfilFuncionDTO.setPeriocidadCondicionAtipicaId(p.getPeriocidadCondicionAtipicaId() == null ? null : p.getPeriocidadCondicionAtipicaId());
		perfilFuncionDTO.setSustentoCondicionAtipica(p.getSustentoCondicionAtipica()== null ? null :p.getSustentoCondicionAtipica().trim());
		perfilFuncionDTO.setCoordinacionInterna(p.getCoordinacionInterna() == null ? null : p.getCoordinacionInterna().trim());
		perfilFuncionDTO.setCoordinacionExterna(p.getCoordinacionExterna() == null ? null : p.getCoordinacionExterna().trim());
		perfilFuncionDTO.setLstServidorCivil(p.getServidorCiviles() == null ? null : p.getServidorCiviles().trim());
		return perfilFuncionDTO;
	};
	
	Function<Perfil, PerfilDTO> convertirPerfil = (Perfil p) ->{
		PerfilDTO perfilDTO = new PerfilDTO();
		perfilDTO.setPerfilId(p.getPerfilId());
		perfilDTO.setEntidadId(p.getEntidadId());
		perfilDTO.setRegimenLaboralId(p.getRegimenLaboralId());
		perfilDTO.setOrganoId(p.getOrganoId() == null ? null : p.getOrganoId());
		perfilDTO.setUnidadOrganicaId(p.getUnidadOrganicaId() == null ? null : p.getUnidadOrganicaId());
		perfilDTO.setUnidadFuncional(p.getUnidadFuncional() == null ? null : p.getUnidadFuncional().trim());
		perfilDTO.setNivelOrganizacional(p.getNivelOrganizacional() == null ? null : p.getNivelOrganizacional().trim());
		perfilDTO.setServidorCivilId(p.getServidorCivilId() == null ? null : p.getServidorCivilId());
		perfilDTO.setFamiliaPuestoId(p.getFamiliaPuestoId() == null ? null : p.getFamiliaPuestoId());
		perfilDTO.setRolId(p.getRolId() == null ? null : p.getRolId());
		perfilDTO.setNivelCategoriaId(p.getNivelCategoriaId() == null ? null : p.getNivelCategoriaId());
		perfilDTO.setPuestoTipoId(p.getPuestoTipoId() == null ? null : p.getPuestoTipoId());
		perfilDTO.setSubNivelsubCategoria(p.getSubNivelsubCategoria() == null ? null : p.getSubNivelsubCategoria().trim());
		perfilDTO.setDependenciaFuncional(p.getDependenciaFuncional() == null ? null : p.getDependenciaFuncional().trim());
		perfilDTO.setDependenciaJerarquica(p.getDependenciaJerarquica() == null ? null : p.getDependenciaJerarquica().trim());
		perfilDTO.setServidorCivilReporteId(p.getServidorCivilReporteId() == null ? null : p.getServidorCivilReporteId());
		perfilDTO.setNroPosicionesCargo(p.getNroPosicionesCargo() == null ? null : p.getNroPosicionesCargo());
		perfilDTO.setPuestoCodigo(p.getPuestoCodigo() == null ?  null : p.getPuestoCodigo().trim());
		perfilDTO.setNombrePuesto(p.getNombrePuesto() == null ? null : p.getNombrePuesto().trim());
		perfilDTO.setMisionPuesto(p.getMisionPuesto() == null ? null : p.getMisionPuesto().trim());
		perfilDTO.setNroPosicionesPuesto(p.getNroPosicionesPuesto() == null ? null : p.getNroPosicionesPuesto());
		perfilDTO.setPuestosCargo(p.getPuestosCargo() == null ? null : p.getPuestosCargo().trim());
		perfilDTO.setTipoPracticaId(p.getTipoPracticaId() == null ? null : p.getTipoPracticaId());
		perfilDTO.setCondicionPracticaId(p.getCondicionPracticaId() == null ? null : p.getCondicionPracticaId());
		perfilDTO.setIndColegiatura(p.getIndColegiatura() == null ? null : p.getIndColegiatura().trim());
		perfilDTO.setIndHabilitacionProf(p.getIndHabilitacionProf() == null ? null : p.getIndHabilitacionProf().trim());
		perfilDTO.setNombreOrgano(p.getNombreOrgano() == null ? null : p.getNombreOrgano().trim());
		perfilDTO.setNombreUnidadOrganica(p.getUnidadOrganica() == null ? null : p.getUnidadOrganica().trim());
		perfilDTO.setCodigoPosicion(p.getCodigoPosicion() == null ? null : p.getCodigoPosicion());
		perfilDTO.setSiglaOrgano(p.getSiglaOrgano() ==  null ? null : p.getSiglaOrgano().trim());
		perfilDTO.setPuestoEstructural(p.getPuestoEstructural() == null ? null : p.getPuestoEstructural().trim());
		perfilDTO.setObservaciones(p.getObservaciones() == null ? null : p.getObservaciones().trim());
		return perfilDTO;
	};

	Function<PerfilFuncionDTO, PerfilFuncion> convertirDtoPerfilFuncion = (PerfilFuncionDTO p) -> {
		PerfilFuncion perfilFuncion = new PerfilFuncion();
		perfilFuncion.setPerfilFuncionId(p.getPerfilFuncionId());
		perfilFuncion.setPerfilId(p.getPerfilId());
		perfilFuncion.setCondicionAticipica(p.getCondicionAtipica() == null ? null : p.getCondicionAtipica().trim());
		perfilFuncion.setPeriocidadCondicionAtipicaId(p.getPeriocidadCondicionAtipicaId() == null ? null : p.getPeriocidadCondicionAtipicaId());
		perfilFuncion.setSustentoCondicionAtipica(p.getSustentoCondicionAtipica()== null ? null :p.getSustentoCondicionAtipica().trim());
		perfilFuncion.setCoordinacionInterna(p.getCoordinacionInterna() == null ? null : p.getCoordinacionInterna().trim());
		perfilFuncion.setCoordinacionExterna(p.getCoordinacionExterna() == null ? null : p.getCoordinacionExterna().trim());
		perfilFuncion.setServidorCiviles(p.getLstServidorCivil() == null ? null : p.getLstServidorCivil().trim());
		return perfilFuncion;

	};
	Function<ConfigPerfilExperienciaLaboralDetalle, ConfigPerfilExperienciaLaboralDetalleDTO> convertirDtoDetalleExperiencia = (ConfigPerfilExperienciaLaboralDetalle p) -> {
		ConfigPerfilExperienciaLaboralDetalleDTO config = new ConfigPerfilExperienciaLaboralDetalleDTO();
		config.setConfPerfExpeLaboralDetalleId(p.getConfPerfExpeLaboralDetalleId());
		config.setConfPerfExpeLaboralId(p.getConfPerfExpeLaboralId());
		config.setDesdeAnios(p.getDesdeAnios() == null ? null : p.getDesdeAnios());
		config.setHastaAnios(p.getHastaAnios() == null ? null : p.getHastaAnios());
		config.setRequisitoMinimo(p.getRequisitoMinimo() == null ? null : p.getRequisitoMinimo());
		config.setTipoExperiencia(p.getTipoExperiencia() == null ? null : p.getTipoExperiencia());
		config.setTipoModelo(p.getTipoModelo() == null ? null : p.getTipoModelo());
		config.setPuntaje(p.getPuntaje()== null ? null : p.getPuntaje());
		return config;

	};
	
	Function<ConfigPerfilFormacionCarreraDetalle, CarreraFormacionAcademicaOtrosGradosDTO> convertirDtoFormacionAcademicaDetalle= (ConfigPerfilFormacionCarreraDetalle p) -> {
		CarreraFormacionAcademicaOtrosGradosDTO config = new CarreraFormacionAcademicaOtrosGradosDTO();
		config.setConfPerfFormacCarreraDetalleId(p.getConfPerfFormacCarreraDetalleId());
		config.setPerfilId(p.getPerfilId());
		config.setGrado(p.getGrado()== null ? null : p.getGrado());
		config.setPuntaje(p.getPuntaje()== null ? null : p.getPuntaje());
		config.setBaseId(p.getBaseId()== null ? null : p.getBaseId());
		return config;
	};
	
	
}
