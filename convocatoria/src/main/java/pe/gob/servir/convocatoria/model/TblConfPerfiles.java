package pe.gob.servir.convocatoria.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tbl_conf_perfiles", schema = "sch_convocatoria")
@NamedQuery(name="TblConfPerfiles.findAll", query="SELECT t FROM TblConfPerfiles t")
public class TblConfPerfiles extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conf_perfiles_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perfiles")
    @SequenceGenerator(name = "seq_conf_perfiles", sequenceName = "seq_conf_perfiles", schema = "sch_convocatoria", allocationSize = 1)
    private Long confPerfilesId;

    @ManyToOne
    @JoinColumn(name = "perfil_id", nullable = false)
    private Perfil perfil;

    @Column(name = "tipo_perfil", nullable = false)
    private Long tipoPerfil;

    @Column(name = "base_id")
    private Long baseId;

    public TblConfPerfiles() { super(); }
}
