package pe.gob.servir.convocatoria.service.impl;

import com.sun.org.apache.bcel.internal.generic.I2F;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.exception.NotFoundException;
import pe.gob.servir.convocatoria.model.*;
import pe.gob.servir.convocatoria.repository.*;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaFormacionAcademica;
import pe.gob.servir.convocatoria.request.ReqCreaPerfil;
import pe.gob.servir.convocatoria.request.ReqCreaPerfilFuncion;
import pe.gob.servir.convocatoria.request.dto.*;
import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.response.dto.RespPerfilDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.PerfilExperienciaService;
import pe.gob.servir.convocatoria.service.PerfilFunction;
import pe.gob.servir.convocatoria.service.PerfilService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PerfilServiceImpl implements PerfilService {

	public static final Logger logger = Logger.getLogger(PerfilServiceImpl.class);

	@Autowired
	private PerfilRepository perfilRepository;

	@Autowired
	private PerfilRepositoryJdbc perfilRepositoryJdbc;

	@Autowired
	private PerfilFuncionRepository perfilFuncionRepository;

	@Autowired
	private FuncionDetalleRepository funcionDetalleRepository;

	@Autowired
	private FormacionAcademicaRepository formacionAcademicaRepository;

	@Autowired
	private CarreraFormacionAcademicaRepository carreraFormacionRepository;

	@Autowired
	private ConocimientoRepository conocimientoRepository;

	@Autowired
	private ConfiguracionMaestraRepository configuracionMaestraRepository;

	@Autowired
	MaestraCabeceraRepository maestraCabeceraRepository;

	@Autowired
	MaestraDetalleEntidadRepository maestraDetalleEntidadRepository;

	@Autowired
	PerfilFuncionRepositoryJdbc perfilFuncionRepositoryJdbc;

	@Autowired
	CarreraProfesionalRepository carreraProfesionalRepositiry;

	@Autowired
	BasePerfilRepository basePerfilRepository;

	@Autowired
	private MaestraDetalleRepository maestraDetalleRespository;

	@Autowired
	MaeConocimientoRepository maeConocimientoRepository;

	@Autowired
	CarreraProfesionalRepository carreraProfesionalRepository;

	@Autowired
	FormacionCarreraDetalleRepository formacionCarreraDetalleRepository;

	@Autowired
	ConvocatoriaRepository convocatoriaRepository;
	
	@Autowired
    PerfilExperienciaService perfilExperienciaService;

	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<RespCreaPerfil> guardarPerfil(ReqBase<ReqCreaPerfil> request, MyJsonWebToken token, Long perfilId) {

		RespBase<RespCreaPerfil> response = new RespBase<>();
		Perfil perfil = null;
		if (perfilId != null) {
			Optional<Perfil> perfilFind = perfilRepository.findById(perfilId);

			if (perfilFind.isPresent()) {
				perfil = perfilFind.get();
				perfil.setCampoSegUpd(perfil.getEstadoRegistro(), token.getUsuario().getUsuario(), Instant.now());
				perfil.setPuestoCodigo(request.getPayload().getPerfil().getPuestoCodigo());
			} else {
				response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el perfilId Ingresado");
				return response;
			}

		} else {
			perfil = new Perfil();
			perfil.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			if (Objects.equals(Constantes.REGIMEN_LABORAL_30057, request.getPayload().getPerfil().getCodigoRegimen())) {
				perfil.setPuestoCodigo(request.getPayload().getPerfil().getPuestoCodigo());
			} else {
				perfil.setPuestoCodigo(obtenerCodigoPuesto(request.getPayload().getPerfil().getRegimenLaboralId(),
						request.getPayload().getPerfil().getCodigoRegimen(),
						request.getPayload().getPerfil().getEntidadId()));
			}
			perfil.setEstadoRm(Constantes.ESTADO_REQ_MINIMO_PENDIENTE);
			perfil.setEstadoEc(Constantes.ESTADO_EVA_CURRICULAR_PENDIENTE);
		}

		perfil.setEntidadId(request.getPayload().getPerfil().getEntidadId());
		perfil.setRegimenLaboralId(request.getPayload().getPerfil().getRegimenLaboralId());
		perfil.setNombrePuesto(request.getPayload().getPerfil().getNombrePuesto());
		perfil.setMisionPuesto(request.getPayload().getPerfil().getMisionPuesto());
		perfil.setOrganoId(request.getPayload().getPerfil().getOrganoId());
		perfil.setNombreOrgano(request.getPayload().getPerfil().getNombreOrgano());
		perfil.setSiglaOrgano(request.getPayload().getPerfil().getSiglaOrgano());
		perfil.setUnidadOrganicaId(request.getPayload().getPerfil().getUnidadOrganicaId());
		perfil.setUnidadOrganica(request.getPayload().getPerfil().getNombreUnidadOrganica());
		perfil.setNroPosicionesCargo(request.getPayload().getPerfil().getNroPosicionesCargo());
		perfil.setIndOrigenPerfil(Constantes.INDIVIDUAL);
		perfil.setIndEstadoRevision(Constantes.POR_REVISAR);
		perfil.setEstadoFunc(Constantes.INACTIVO);
		perfil.setEstadoForm(Constantes.INACTIVO);
		perfil.setEstadoExp(Constantes.INACTIVO);
		if (!Util.isEmpty(request.getPayload().getPerfil().getRegimenLaboralId())) {
			if (!Util.isEmpty(request.getPayload().getPerfil().getEntidadId())) {

				if (Objects.equals(Constantes.REGIMEN_LABORAL_30057,
						request.getPayload().getPerfil().getCodigoRegimen())) {
					perfil.setRolId(request.getPayload().getPerfil().getRolId());
					perfil.setUnidadFuncional(request.getPayload().getPerfil().getUnidadFuncional());
					perfil.setNivelOrganizacional(request.getPayload().getPerfil().getNivelOrganizacional());// validar
					// perfil.setNivelOrganizacionalId(request.getPayload().getPerfil().getnive());
					// //validar
					perfil.setServidorCivilId(request.getPayload().getPerfil().getServidorCivilId());
					perfil.setFamiliaPuestoId(request.getPayload().getPerfil().getFamiliaPuestoId());
					perfil.setNivelCategoriaId(request.getPayload().getPerfil().getNivelCategoriaId());
					perfil.setPuestoTipoId(request.getPayload().getPerfil().getPuestoTipoId());
					perfil.setSubNivelsubCategoria(request.getPayload().getPerfil().getSubNivelsubCategoria());
					perfil.setDependenciaJerarquica(request.getPayload().getPerfil().getDependenciaJerarquica());
					perfil.setDependenciaFuncional(request.getPayload().getPerfil().getDependenciaFuncional());
					perfil.setServidorCivilReporteId(request.getPayload().getPerfil().getServidorCivilReporteId());
					perfil.setNroPosicionesPuesto(request.getPayload().getPerfil().getNroPosicionesPuesto());
					perfil.setCodigoPosicion(request.getPayload().getPerfil().getCodigoPosicion());

				} else if (Objects.equals(Constantes.REGIMEN_LABORAL_276,
						request.getPayload().getPerfil().getCodigoRegimen())) {
					// determinar que campos van
					perfil.setRolId(request.getPayload().getPerfil().getRolId());
					perfil.setUnidadFuncional(request.getPayload().getPerfil().getUnidadFuncional());
					perfil.setNivelOrganizacional(request.getPayload().getPerfil().getNivelOrganizacional());// validar
					// perfil.setNivelOrganizacionalId(request.getPayload().getPerfil().getnive());
					// //validar
					perfil.setServidorCivilId(request.getPayload().getPerfil().getServidorCivilId());
					perfil.setFamiliaPuestoId(request.getPayload().getPerfil().getFamiliaPuestoId());
					perfil.setNivelCategoriaId(request.getPayload().getPerfil().getNivelCategoriaId());
					perfil.setPuestoTipoId(request.getPayload().getPerfil().getPuestoTipoId());
					perfil.setSubNivelsubCategoria(request.getPayload().getPerfil().getSubNivelsubCategoria());
					perfil.setDependenciaJerarquica(request.getPayload().getPerfil().getDependenciaJerarquica());
					perfil.setDependenciaFuncional(request.getPayload().getPerfil().getDependenciaFuncional());
					perfil.setServidorCivilReporteId(request.getPayload().getPerfil().getServidorCivilReporteId());
					perfil.setPuestoEstructural(request.getPayload().getPerfil().getPuestoEstructural());
					perfil.setNroPosicionesPuesto(request.getPayload().getPerfil().getNroPosicionesPuesto());
					perfil.setCodigoPosicion(request.getPayload().getPerfil().getCodigoPosicion());

				} else if (Objects.equals(Constantes.REGIMEN_LABORAL_728,
						request.getPayload().getPerfil().getCodigoRegimen())) {
					// determinar que campos van
					perfil.setRolId(request.getPayload().getPerfil().getRolId());
					perfil.setUnidadFuncional(request.getPayload().getPerfil().getUnidadFuncional());
					perfil.setNivelOrganizacional(request.getPayload().getPerfil().getNivelOrganizacional());// validar
					// perfil.setNivelOrganizacionalId(request.getPayload().getPerfil().getnive());
					// //validar
					perfil.setServidorCivilId(request.getPayload().getPerfil().getServidorCivilId());
					perfil.setFamiliaPuestoId(request.getPayload().getPerfil().getFamiliaPuestoId());
					perfil.setNivelCategoriaId(request.getPayload().getPerfil().getNivelCategoriaId());
					perfil.setPuestoTipoId(request.getPayload().getPerfil().getPuestoTipoId());
					perfil.setSubNivelsubCategoria(request.getPayload().getPerfil().getSubNivelsubCategoria());
					perfil.setDependenciaJerarquica(request.getPayload().getPerfil().getDependenciaJerarquica());
					perfil.setDependenciaFuncional(request.getPayload().getPerfil().getDependenciaFuncional());
					perfil.setServidorCivilReporteId(request.getPayload().getPerfil().getServidorCivilReporteId());
					perfil.setPuestoEstructural(request.getPayload().getPerfil().getPuestoEstructural());
					perfil.setNroPosicionesPuesto(request.getPayload().getPerfil().getNroPosicionesPuesto());
					perfil.setCodigoPosicion(request.getPayload().getPerfil().getCodigoPosicion());

				} else if (Objects.equals(Constantes.REGIMEN_LABORAL_1401,
						request.getPayload().getPerfil().getCodigoRegimen())) {
					perfil.setUnidadFuncional(request.getPayload().getPerfil().getUnidadFuncional());
					perfil.setDependenciaJerarquica(request.getPayload().getPerfil().getDependenciaJerarquica());
					perfil.setTipoPracticaId(request.getPayload().getPerfil().getTipoPracticaId());
					perfil.setCondicionPracticaId(request.getPayload().getPerfil().getCondicionPracticaId());
				} else {
					perfil.setPuestoEstructural(request.getPayload().getPerfil().getPuestoEstructural());
					perfil.setDependenciaJerarquica(request.getPayload().getPerfil().getDependenciaJerarquica());
					perfil.setDependenciaFuncional(request.getPayload().getPerfil().getDependenciaFuncional());
					perfil.setPuestosCargo(request.getPayload().getPerfil().getPuestosCargo());

				}

			}
		}

		perfilRepository.save(perfil);

		RespCreaPerfil payload = new RespCreaPerfil();
		payload.setPerfil(perfil);

		return new RespBase<RespCreaPerfil>().ok(payload);
	}

	@Override
	public RespBase<RespObtieneLista<ObtenerPerfilDTO>> obtenerPerfil(String fechaIni, String fechaFin, Long entidad,
			String codigoPuesto, String nombrePerfil, Long regimenId, Long organoId, Long unidadOrganicaId,String origenPerfil,
			Long estadoRevision, String estado) {

		List<ObtenerPerfilDTO> listarPerfilDTO = perfilRepositoryJdbc.findAllPerfil(fechaIni, fechaFin, entidad,
				codigoPuesto, nombrePerfil, regimenId, organoId, unidadOrganicaId ,  origenPerfil ,  estadoRevision, estado);
		
		for(ObtenerPerfilDTO perfil : listarPerfilDTO) {			
			perfil.setRevisionHabilitado(perfil.getEstadoFunc().equals(Constantes.ACTIVO) && 
					perfil.getEstadoForm().equals(Constantes.ACTIVO) &&
					perfil.getEstadoExp().equals(Constantes.ACTIVO));
		}
		
		RespObtieneLista<ObtenerPerfilDTO> respPayload = new RespObtieneLista<>();

		respPayload.setCount(listarPerfilDTO.size());
		respPayload.setItems(listarPerfilDTO);
		return new RespBase<RespObtieneLista<ObtenerPerfilDTO>>().ok(respPayload);

	}

	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<RespCreaPerfilFuncion> guardarPerfilFuncion(ReqBase<ReqCreaPerfilFuncion> request,
			MyJsonWebToken token, Long perfilFuncionId) {

		RespBase<RespCreaPerfilFuncion> response = new RespBase<>();
		PerfilFuncion perfilFuncion = null;
		if (perfilFuncionId != null) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el perfilFuncionId Ingresado");
			return response;
		} else {
			perfilFuncion = new PerfilFuncion();
			/* para evitar la transaccion en cascada */
			perfilFuncion.setFuncionDetalles(null);
			/**/
			perfilFuncion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			perfilFuncion.setServidorCiviles(request.getPayload().getPerfilFuncion().getLstServidorCivil());
		}
		
		Optional<Perfil> perfilOptional = perfilRepository.findById(request.getPayload().getPerfilFuncion().getPerfilId());
		if (perfilOptional.isPresent()) {
			Perfil perfil = perfilOptional.get();
			perfil.setEstadoFunc(Constantes.ACTIVO);
			perfilRepository.save(perfil);
		}

		List<FuncionDetalle> lstFuncionDetalle = new ArrayList<>();

		perfilFuncion.setPerfilId(request.getPayload().getPerfilFuncion().getPerfilId());

		if (Objects.equals(Constantes.REGIMEN_LABORAL_30057,
				request.getPayload().getPerfilFuncion().getCodigoRegimen())) {
			perfilFuncion.setCondicionAticipica(request.getPayload().getPerfilFuncion().getCondicionAtipica());
			perfilFuncion.setPeriocidadCondicionAtipicaId(
					request.getPayload().getPerfilFuncion().getPeriocidadCondicionAtipicaId());
			perfilFuncion
					.setSustentoCondicionAtipica(request.getPayload().getPerfilFuncion().getSustentoCondicionAtipica());
			perfilFuncion.setCoordinacionInterna(request.getPayload().getPerfilFuncion().getCoordinacionInterna());
			perfilFuncion.setCoordinacionExterna(request.getPayload().getPerfilFuncion().getCoordinacionExterna());
			perfilFuncionRepository.save(perfilFuncion);

		} else if (Objects.equals(Constantes.REGIMEN_LABORAL_1401,
				request.getPayload().getPerfilFuncion().getCodigoRegimen())) {
			perfilFuncionRepository.save(perfilFuncion);
		} else {
			perfilFuncion.setCoordinacionInterna(request.getPayload().getPerfilFuncion().getCoordinacionInterna());
			perfilFuncion.setCoordinacionExterna(request.getPayload().getPerfilFuncion().getCoordinacionExterna());
			perfilFuncionRepository.save(perfilFuncion);
		}

		if (request.getPayload().getPerfilFuncion().getLstFuncionDetalle() != null) {
			for (FuncionDetalleDTO funcionDetalleDTO : request.getPayload().getPerfilFuncion().getLstFuncionDetalle()) {
				FuncionDetalle funcionDetalle = new FuncionDetalle();
				funcionDetalle.setPerfilFuncion(perfilFuncion);
				funcionDetalle.setDescripcion(funcionDetalleDTO.getDescripcion());
				funcionDetalle.setOrden(funcionDetalleDTO.getOrden());
				funcionDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				funcionDetalleRepository.save(funcionDetalle);
				lstFuncionDetalle.add(funcionDetalle);
			}
		}

		PerfilFuncionDTO funcionDTO = null;
		funcionDTO = PerfilFunction.convertirPerfilFuncion.apply(perfilFuncion);

		List<FuncionDetalleDTO> lstFuncionDetalleDTO = new ArrayList<>();

		if (lstFuncionDetalle != null && !lstFuncionDetalle.isEmpty()) {
			for (FuncionDetalle f : lstFuncionDetalle) {
				FuncionDetalleDTO funcion = PerfilFunction.convertirFuncionDetalle.apply(f);
				lstFuncionDetalleDTO.add(funcion);
			}
			funcionDTO.setLstFuncionDetalle(lstFuncionDetalleDTO);
		}

		RespCreaPerfilFuncion resPayload = new RespCreaPerfilFuncion();

		resPayload.setPerfilFuncion(funcionDTO);
		return new RespBase<RespCreaPerfilFuncion>().ok(resPayload);
	}

	@Override
	public RespBase<RespCreaPerfilFuncion> actualizarPerfilFuncion(ReqBase<ReqCreaPerfilFuncion> request,
			MyJsonWebToken token, Long perfilFuncionId) {

		RespBase<RespCreaPerfilFuncion> response = new RespBase<>();
		if (request.getPayload().getPerfilFuncion().getPerfilFuncionId() == null) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "el id ingresado es null");
			return response;
		}

		Optional<PerfilFuncion> perfilFuncionBD = perfilFuncionRepository
				.findById(request.getPayload().getPerfilFuncion().getPerfilFuncionId());
		if (!perfilFuncionBD.isPresent()) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, " perfil funcion no existe con el id: "
					+ request.getPayload().getPerfilFuncion().getPerfilFuncionId());
			return response;
		}

		Optional<Perfil> perfilOptional = perfilRepository.findById(request.getPayload().getPerfilFuncion().getPerfilId());
		if (perfilOptional.isPresent()) {
			Perfil perfil = perfilOptional.get();
			perfil.setEstadoFunc(Constantes.ACTIVO);
			perfilRepository.save(perfil);
		}
		
		PerfilFuncion perfilFuncion = PerfilFunction.convertirDtoPerfilFuncion
				.apply(request.getPayload().getPerfilFuncion());
		perfilFuncion.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
		perfilFuncion.setFuncionDetalles(new ArrayList<>());

		List<FuncionDetalle> detalleList = new ArrayList<>();

		if (request.getPayload().getPerfilFuncion().getLstFuncionDetalle() != null
				&& !request.getPayload().getPerfilFuncion().getLstFuncionDetalle().isEmpty()) {
			for (FuncionDetalleDTO it : request.getPayload().getPerfilFuncion().getLstFuncionDetalle()) {
				FuncionDetalle funcionDetalle = PerfilFunction.convertirDtoFuncionDetalle.apply(it);
				funcionDetalle.setPerfilFuncion(perfilFuncion);
				if (funcionDetalle.getFuncionDetalleId() == null) {
					funcionDetalle.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
				} else {
					funcionDetalle.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
				}
				funcionDetalle.setEstadoRegistro(it.getEstado());
				detalleList.add(funcionDetalle);
			}
		}

		perfilFuncion.setFuncionDetalles(detalleList);

		perfilFuncionRepository.save(perfilFuncion);

		RespCreaPerfilFuncion resPayload = new RespCreaPerfilFuncion();
		List<PerfilFuncionDTO> listPerfilFuncion = new ArrayList<>();
		PerfilFuncionDTO perfilFuncionDTO = new PerfilFuncionDTO();
		listPerfilFuncion = perfilFuncionRepositoryJdbc.listPerfilFuncion(perfilFuncion.getPerfilId());

		perfilFuncionDTO = listPerfilFuncion.get(0);

		perfilFuncionDTO.setLstFuncionDetalle(
				perfilFuncionRepositoryJdbc.listPerfilFuncionDetalle(perfilFuncion.getPerfilFuncionId()));
		resPayload.setPerfilFuncion(perfilFuncionDTO);
		return new RespBase<RespCreaPerfilFuncion>().ok(resPayload);

	}

	@Override
	public RespBase<RespCreaPerfilFuncion> findPerfilFuncion(Long perfilId) {

		RespBase<RespCreaPerfilFuncion> response = new RespBase<>();
		if (Util.isEmpty(perfilId)) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "perfilId es null o vacio");
			return response;
		}

		List<PerfilFuncionDTO> listPerfilFuncion = new ArrayList<>();
		PerfilFuncionDTO perfilFuncionDTO = new PerfilFuncionDTO();

		listPerfilFuncion = perfilFuncionRepositoryJdbc.listPerfilFuncion(perfilId);

		if (listPerfilFuncion == null || listPerfilFuncion.isEmpty()) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
					"no existe Perfil Funcion con el perfil: " + perfilId);
			return response;
		}

		perfilFuncionDTO = listPerfilFuncion.get(0);

		perfilFuncionDTO.setLstFuncionDetalle(
				perfilFuncionRepositoryJdbc.listPerfilFuncionDetalle(perfilFuncionDTO.getPerfilFuncionId()));

		RespCreaPerfilFuncion respPayload = new RespCreaPerfilFuncion();
		respPayload.setPerfilFuncion(perfilFuncionDTO);
		return new RespBase<RespCreaPerfilFuncion>().ok(respPayload);
	}

	public String obtenerCodigoPuesto(Long regimenId, String codigoRegimen, Long entidadId) {

		String correlativo;
		String codigoPuesto = "";
		correlativo = perfilRepositoryJdbc.findCorrelativoCodigoPuesto(regimenId, entidadId);

		if (Constantes.REGIMEN_LABORAL_1057.equals(codigoRegimen)) {
			codigoPuesto += Constantes.COD_REG_LABORAL_1057 + Constantes.GUION;
		} else if (Constantes.REGIMEN_LABORAL_1401.equals(codigoRegimen)) {
			codigoPuesto += Constantes.COD_REG_LABORAL_1401 + Constantes.GUION;
		} else if (Constantes.REGIMEN_LABORAL_276.equals(codigoRegimen)) {
			codigoPuesto += Constantes.COD_REG_LABORAL_276 + Constantes.GUION;
		} else if (Constantes.REGIMEN_LABORAL_728.equals(codigoRegimen)) {
			codigoPuesto += Constantes.COD_REG_LABORAL_728 + Constantes.GUION;
		}
		return codigoPuesto + correlativo;
	}

	/**
	 * REGISTRO FORMACION ACADEMICA POST / PUT
	 * 
	 * @param request
	 * @param token
	 * @return
	 */
	@Override
	public RespBase<ReqCreaFormacionAcademica> guardarFormacionAcademica(ReqBase<ReqCreaFormacionAcademica> request,
			MyJsonWebToken token) {
		RespBase<ReqCreaFormacionAcademica> response = new RespBase<>();
		ReqCreaFormacionAcademica payload = new ReqCreaFormacionAcademica();
		Optional<Perfil> perfilOptional = perfilRepository.findById(request.getPayload().getPerfilId());
		if (!perfilOptional.isPresent()) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el perfil");
			return response;
		}

		Perfil perfil = perfilOptional.get();
		perfil.setIndColegiatura(request.getPayload().getIndColegiatura());
		perfil.setIndHabilitacionProf(request.getPayload().getIndHabilitacionProf());
		perfil.setObservaciones(request.getPayload().getObservaciones());
		perfil.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), token.getUsuario().getUsuario(), Instant.now());
		perfil.setFormacionAcademicaList(new ArrayList<>());
		perfil.setConocimientoList(new ArrayList<>());
		perfil.setEstadoForm(Constantes.ACTIVO);

		if (request.getPayload().getLstFormacionAcademica() != null
				&& !request.getPayload().getLstFormacionAcademica().isEmpty())
			perfil.setFormacionAcademicaList(settListFormacionAcademica(request, perfil, token));
		if (request.getPayload().getLstConocimientos() != null && !request.getPayload().getLstConocimientos().isEmpty())
			perfil.setConocimientoList(settListConocimiento(request, perfil, token));

		perfilRepository.save(perfil);

		List<FormacionAcademicaDTO> formacionAcademicaDTOList = formacionAcademicaRepository
				.findListByPerfilId(perfil.getPerfilId()).stream()
				.filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
				.map(formacionAcademica -> {
					FormacionAcademicaDTO formacionAcademicaDTO = PerfilFunction.convertirFormacionAcademica
							.apply(formacionAcademica);
					formacionAcademicaDTO.setLstCarreraFormaAcademica(
							settListCarreraFormacionAcademicaDTOS(carreraFormacionRepository
									.findListByFormacionAca(formacionAcademica.getFormacionAcademicaId())));
					return formacionAcademicaDTO;
				}).collect(Collectors.toList());

		List<ConocimientoDTO> conocimientoDTOList = conocimientoRepository.findListByPerfil(perfil.getPerfilId())
				.stream().filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
				.map(conocimiento -> {
					ConocimientoDTO conocimientoDTO = PerfilFunction.convertirConocimiento.apply(conocimiento);
					return conocimientoDTO;
				}).collect(Collectors.toList());

		payload.setPerfilId(perfil.getPerfilId());
		payload.setObservaciones(perfil.getObservaciones());
		payload.setRegimenLaboralId(perfil.getRegimenLaboralId());
		payload.setIndColegiatura(perfil.getIndColegiatura());
		payload.setIndHabilitacionProf(perfil.getIndHabilitacionProf());
		payload.setLstFormacionAcademica(formacionAcademicaDTOList);
		payload.setLstConocimientos(conocimientoDTOList);
		payload.setObservaciones(perfil.getObservaciones());
		return new RespBase<ReqCreaFormacionAcademica>().ok(payload);
	}

	/**
	 * OBTENER FORMACION ACADEMICA POR PERFIL
	 * 
	 * @param perfilId
	 * @return
	 */
	@Override
	public RespBase<ReqCreaFormacionAcademica> obtieneFormacionAcademicaById(Long perfilId) {
		RespBase<ReqCreaFormacionAcademica> response = new RespBase<>();
		ReqCreaFormacionAcademica payload = new ReqCreaFormacionAcademica();

		Optional<Perfil> perfilOptional = perfilRepository.findById(perfilId);
		if (!perfilOptional.isPresent()) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el perfil");
			return response;
		}

		List<FormacionAcademicaDTO> formacionAcademicaDTOList = formacionAcademicaRepository
				.findListByPerfilId(perfilId).stream()
				.filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
				.map(formacionAcademica -> {
					FormacionAcademicaDTO formacionAcademicaDTO = PerfilFunction.convertirFormacionAcademica
							.apply(formacionAcademica);

					formacionAcademicaDTO.setLstCarreraFormaAcademica(
							settListCarreraFormacionAcademicaDTOS(carreraFormacionRepository
									.findListByFormacionAca(formacionAcademica.getFormacionAcademicaId())));

					if (formacionAcademicaDTO.getNivelEducativoId() != null) {
						Optional<MaestraDetalle> ma = maestraDetalleRespository
								.findById(formacionAcademicaDTO.getNivelEducativoId());
						ma.ifPresent(p -> formacionAcademicaDTO.setNivelEducativoDesc(p.getDescripcion()));
					}

					if (formacionAcademicaDTO.getSituacionAcademicaId() != null) {
						Optional<MaestraDetalle> sit = maestraDetalleRespository
								.findById(formacionAcademicaDTO.getSituacionAcademicaId());
						sit.ifPresent(b -> formacionAcademicaDTO.setSitAcademiDesc(b.getDescripcion()));
					}
					return formacionAcademicaDTO;
				}).collect(Collectors.toList());

		List<ConocimientoDTO> conocimientoDTOList = conocimientoRepository.findListByPerfil(perfilId).stream()
				.filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO)).map(conocimiento -> {
					ConocimientoDTO conocimientoDTO = PerfilFunction.convertirConocimiento.apply(conocimiento);
					if (conocimientoDTO.getTipoConocimientoId() != null) {
						Optional<MaestraDetalle> ma = maestraDetalleRespository
								.findById(conocimientoDTO.getTipoConocimientoId());
						ma.ifPresent(maestraDetalle -> conocimientoDTO
								.setDescriTipoConocimiento(maestraDetalle.getDescripcion()));
					}
					return conocimientoDTO;
				}).collect(Collectors.toList());

		payload.setPerfilId(perfilOptional.get().getPerfilId());
		payload.setRegimenLaboralId(perfilOptional.get().getRegimenLaboralId());
		payload.setIndColegiatura(perfilOptional.get().getIndColegiatura());
		payload.setIndHabilitacionProf(perfilOptional.get().getIndHabilitacionProf());
		payload.setLstFormacionAcademica(formacionAcademicaDTOList);
		payload.setLstConocimientos(conocimientoDTOList);
		payload.setObservaciones(perfilOptional.get().getObservaciones());
		return new RespBase<ReqCreaFormacionAcademica>().ok(payload);
	}

	/**
	 * SETT LIST DE CARRERA FORMACION ACADEMICA DTO
	 * 
	 * @param ls
	 * @return
	 */
	public List<CarreraFormacionAcademicaDTO> settListCarreraFormacionAcademicaDTOS(
			List<CarreraFormacionAcademica> ls) {
		return ls.stream().filter(val -> !val.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
				.map(carreraFormacionAcademica -> {
					CarreraFormacionAcademicaDTO carreraFormacionAcademicaDTO = PerfilFunction.convertirCarreraFormacion
							.apply(carreraFormacionAcademica);
					carreraFormacionAcademicaDTO.setCarreraId(carreraFormacionRepository
							.idCarreraByCarrFormAcId(carreraFormacionAcademica.getCarreraAcademicaId()));
					if (carreraFormacionAcademicaDTO.getCarreraId() != null) {
						Optional<CarreraProfesional> ca = carreraProfesionalRepository
								.findById(carreraFormacionAcademicaDTO.getCarreraId());
						ca.ifPresent(carr -> carreraFormacionAcademicaDTO.setDescripcion(carr.getDescripcion()));
					}

					return carreraFormacionAcademicaDTO;
				}).collect(Collectors.toList());
	}

	/**
	 * SETT LIST DE CONOCIMENTOS
	 * 
	 * @param request
	 * @param perfil
	 * @param token
	 * @return
	 */
	private List<Conocimiento> settListConocimiento(ReqBase<ReqCreaFormacionAcademica> request, Perfil perfil,
			MyJsonWebToken token) {
		return request.getPayload().getLstConocimientos().stream().map(conocimientoDTO -> {
			Conocimiento conocimiento = new Conocimiento();
			conocimiento.setPerfilConocimientoId(conocimientoDTO.getPerfilConocimientoId());

			if (Util.isEmpty(conocimientoDTO.getMaeConocimientoId())) {
				throw new NotFoundException(
						"hay registros de conocimiento sin el valor maeConocimientoId (el campo es obligatorio)");
			}
			Optional<MaeConocimiento> ma = maeConocimientoRepository.findById(conocimientoDTO.getMaeConocimientoId());
			if (!ma.isPresent()) {
				throw new NotFoundException("no existe el maeConocimientoId: (" + conocimientoDTO.getMaeConocimientoId()
						+ ") en Base de datos");
			}

			conocimiento.setMaeConocimiento(ma.get());
			conocimiento.setPerfil(perfil);
			conocimiento.setRegimemlaboralid(conocimientoDTO.getRegimenLaboralId());
			conocimiento.setHoras(conocimientoDTO.getHoras());
			conocimiento.setNivelDominioId(conocimientoDTO.getNivelDominioId());

			if (conocimientoDTO.getPerfilConocimientoId() == null) {
				conocimiento.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			} else {
				conocimiento.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
			}

			conocimiento.setEstadoRegistro(
					Util.isEmpty(conocimientoDTO.getEstado()) ? Constantes.ACTIVO : conocimientoDTO.getEstado());

			return conocimiento;
		}).collect(Collectors.toList());
	}

	/**
	 * SETT LIST DE FORMACION ACADEMICA
	 * 
	 * @param request
	 * @param perfil
	 * @param token
	 * @return
	 */
	private List<FormacionAcademica> settListFormacionAcademica(ReqBase<ReqCreaFormacionAcademica> request,
			Perfil perfil, MyJsonWebToken token) {
		return request.getPayload().getLstFormacionAcademica().stream().map(formacionAcademicaDTO -> {
			FormacionAcademica formacionAcademica = new FormacionAcademica();
			formacionAcademica.setFormacionAcademicaId(formacionAcademicaDTO.getFormacionAcademicaId());
			formacionAcademica.setPerfil(perfil);
			formacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			formacionAcademica.setCarreraFormacionAcademicas(
					settListCarreraFormacionAcademica(formacionAcademicaDTO, token, formacionAcademica));
			formacionAcademica.setNivelEducativoId(formacionAcademicaDTO.getNivelEducativoId());
			formacionAcademica.setEstadoNivelEducativoId(formacionAcademicaDTO.getEstadoNivelEducativoId());
			formacionAcademica.setSituacionAcademicaId(formacionAcademicaDTO.getSituacionAcademicaId());
			formacionAcademica.setEstadoSituacionAcademicaId(formacionAcademicaDTO.getEstadoSituacionAcademicaId());
			formacionAcademica.setNombreGrado(formacionAcademicaDTO.getNombreGrado());

			if (formacionAcademicaDTO.getFormacionAcademicaId() == null) {
				formacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			} else {
				formacionAcademica.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(), Instant.now());
			}
			formacionAcademica.setEstadoRegistro(Util.isEmpty(formacionAcademicaDTO.getEstado()) ? Constantes.ACTIVO
					: formacionAcademicaDTO.getEstado());

			return formacionAcademica;
		}).collect(Collectors.toList());
	}

	/**
	 * SETT LIST DE CARRERA FORMACION ACADEMICA
	 * 
	 * @param formacionAcademicaDTO
	 * @param token
	 * @param formacionAcademica
	 * @return
	 */
	private List<CarreraFormacionAcademica> settListCarreraFormacionAcademica(
			FormacionAcademicaDTO formacionAcademicaDTO, MyJsonWebToken token, FormacionAcademica formacionAcademica) {
		return formacionAcademicaDTO.getLstCarreraFormaAcademica().stream().map(carreraFormacionAcademicaDTO -> {
			CarreraFormacionAcademica carreraFormacionAcademica = new CarreraFormacionAcademica();
			carreraFormacionAcademica
					.setCarreraAcademicaId(carreraFormacionAcademicaDTO.getCarreraFormacionAcademicaId());
			carreraFormacionAcademica.setCarreraProfesional(
					carreraProfesionalRepositiry.findById(carreraFormacionAcademicaDTO.getCarreraId()).orElse(null));
			carreraFormacionAcademica.setFomarcionAcademica(formacionAcademica);
			if (carreraFormacionAcademicaDTO.getCarreraFormacionAcademicaId() == null) {
				carreraFormacionAcademica.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
			} else {
				carreraFormacionAcademica.setCampoSegUpd(Constantes.ACTIVO, token.getUsuario().getUsuario(),
						Instant.now());
			}
			carreraFormacionAcademica
					.setEstadoRegistro(Util.isEmpty(carreraFormacionAcademicaDTO.getEstado()) ? Constantes.ACTIVO
							: carreraFormacionAcademicaDTO.getEstado());
			return carreraFormacionAcademica;
		}).collect(Collectors.toList());
	}

	@Override
	public RespBase<RespObtienePerfil> obtienePerfilById(Long perfilId) {

		PerfilDTO perfilDTO = null;
		RespObtienePerfil payload = new RespObtienePerfil();
		Optional<Perfil> perfilFind = perfilRepository.findById(perfilId);
		if (perfilFind.isPresent()) {
			Perfil perfil = perfilFind.get();
			perfilDTO = PerfilFunction.convertirPerfil.apply(perfil);
		}

		payload.setPerfil(perfilDTO);

		return new RespBase<RespObtienePerfil>().ok(payload);
	}

	@Override
	public RespBase<RespObtienePerfil> cambiarEstado(Long perfilId, String estado, MyJsonWebToken token) {
		RespObtienePerfil payload = new RespObtienePerfil();
		RespBase<RespObtienePerfil> response = new RespBase<>();

		if (Util.isEmpty(perfilId)) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "perfilId es obligatorio");
			return response;
		}

		if (Util.isEmpty(estado)) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "estado es obligatorio");
			return response;
		}

		Optional<Perfil> perfilOptional = perfilRepository.findById(perfilId);
		if (perfilOptional.isPresent()) {
			Perfil perfil = perfilOptional.get();
			perfil.setEstadoRegistro(estado);
			perfil.setCampoSegUpd(estado, token.getUsuario().getUsuario(), Instant.now());
			perfilRepository.save(perfil);
		} else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
					"no se encontro el perfil con el id: " + perfilId);
			return response;
		}

		return new RespBase<RespObtienePerfil>().ok(payload);
	}

	/*
	 * @Override public RespBase<RespObtieneFormacionAcad>
	 * obtieneFormacionAcademicaById(Long perfilId) { PerfilDTO perfilDTO = null;
	 * List<FormacionAcademicaDTO> lstFormacionAcademicaDTO = new ArrayList<>();
	 * List<ConocimientoDTO> lstConocimientoDTO = new ArrayList<>();
	 * RespObtieneFormacionAcad payload = new RespObtieneFormacionAcad();
	 * Optional<Perfil> perfilFind = perfilRepository.findById(perfilId);
	 * if(perfilFind.isPresent()) { Perfil perfil = perfilFind.get(); perfilDTO =
	 * PerfilFunction.convertirPerfil.apply(perfil);
	 * 
	 * 
	 * 
	 * FormacionAcademica formacionFiltro = new FormacionAcademica();
	 * formacionFiltro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
	 * //formacionFiltro.setPerfilId(perfil.getPerfilId());
	 * 
	 * Example<FormacionAcademica> exampleFormacion = Example.of(formacionFiltro);
	 * List<FormacionAcademica> lstFormacion =
	 * formacionAcademicaRepository.findAll(exampleFormacion); if (lstFormacion !=
	 * null && Boolean.FALSE.equals(lstFormacion.isEmpty())) { for
	 * (FormacionAcademica f : lstFormacion) { FormacionAcademicaDTO formacion =
	 * PerfilFunction.convertirFormacionAcademica.apply(f);
	 * 
	 * List<CarreraFormacionAcademicaDTO> lstCarreraFormacionDTO = new
	 * ArrayList<>(); CarreraFormacionAcademica carreraFiltro = new
	 * CarreraFormacionAcademica();
	 * carreraFiltro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
	 * carreraFiltro.setFomarcionAcademica(f);
	 * 
	 * Example<CarreraFormacionAcademica> exampleCarrera =
	 * Example.of(carreraFiltro); List<CarreraFormacionAcademica> lstCarrera =
	 * carreraFormacionRepository.findAll(exampleCarrera); if(lstCarrera != null &&
	 * Boolean.FALSE.equals(lstCarrera.isEmpty())) { for(CarreraFormacionAcademica c
	 * : lstCarrera) { Optional<CarreraProfesional> carreraProfesionalFind =
	 * carreraProfesionalRepositiry.findById(null);
	 * if(carreraProfesionalFind.isPresent()) { CarreraProfesional
	 * carreraProfesional = carreraProfesionalFind.get();
	 * c.setDescripcion(carreraProfesional.getDescripcion());
	 * c.setMaeDetalleId(carreraProfesional.getIdDetalle()); }
	 * CarreraFormacionAcademicaDTO carrera =
	 * PerfilFunction.convertirCarreraFormacion.apply(c);
	 * lstCarreraFormacionDTO.add(carrera); }
	 * formacion.setLstCarreraFormaAcademica(lstCarreraFormacionDTO); }
	 * 
	 * lstFormacionAcademicaDTO.add(formacion); } }
	 * 
	 * Conocimiento conocimientoFiltro = new Conocimiento();
	 * conocimientoFiltro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
	 * //conocimientoFiltro.setPerfilId(perfilId);
	 * 
	 * Example<Conocimiento> exampleConocimiento = Example.of(conocimientoFiltro);
	 * List<Conocimiento> lstConocimiento =
	 * conocimientoRepository.findAll(exampleConocimiento); if (lstConocimiento !=
	 * null && Boolean.FALSE.equals(lstConocimiento.isEmpty())) { for (Conocimiento
	 * f : lstConocimiento) { ConocimientoDTO conocimiento =
	 * PerfilFunction.convertirConocimiento.apply(f);
	 * lstConocimientoDTO.add(conocimiento); } } }
	 * 
	 * payload.setPerfil(perfilDTO);
	 * payload.setLstFormacionAcademica(lstFormacionAcademicaDTO);
	 * payload.setLstConocimiento(lstConocimientoDTO);
	 * 
	 * 
	 * return new RespBase<RespObtieneFormacionAcad>().ok(payload); }
	 */
	@Override
	public RespBase<RespListaVacante> listarVacantes(Long baseId) {
		RespListaVacante respPayload = new RespListaVacante();
		Long vacantesDisponibles = perfilRepositoryJdbc.calcularVacantesDisponiblesPorBase(baseId);
		List<VacanteDTO> listaVacantes = perfilRepositoryJdbc.listarVacantes(baseId);
		respPayload.setVacantesDisponibles(vacantesDisponibles);
		respPayload.setItems(listaVacantes);
		return new RespBase<RespListaVacante>().ok(respPayload);
	}

	@Override
	public RespBase<VacanteDTO> cambiarEstadoVacante(Long basePerfilId, String estado, MyJsonWebToken token) {
		VacanteDTO payload = new VacanteDTO();
		RespBase<VacanteDTO> response = new RespBase<>();

		if (Util.isEmpty(basePerfilId)) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "basePerfilId es obligatorio");
			return response;
		}

		if (Util.isEmpty(estado)) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "estado es obligatorio");
			return response;
		}

		Optional<BasePerfil> baseOptional = basePerfilRepository.findById(basePerfilId);
		if (baseOptional.isPresent()) {
			BasePerfil basePerfil = baseOptional.get();
			basePerfil.setEstadoRegistro(estado);
			basePerfil.setCampoSegUpd(estado, token.getUsuario().getUsuario(), Instant.now());
			basePerfilRepository.save(basePerfil);

			payload.setBaseId(baseOptional.get().getBasePerfilId());
			payload.setEstadoRegistro(estado);
		} else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
					"no se encontro la base con el id: " + basePerfilId);
			return response;
		}

		return new RespBase<VacanteDTO>().ok(payload);

	}

	@Override
	public RespBase<RespObtieneLista<ObtenerPerfilConfigDTO>> buscarPerfilByFilter(Map<String, Object> parametroMap) {
		List<ObtenerPerfilConfigDTO> listaPerfilDTO = perfilRepositoryJdbc.buscarPerfilByFilter(parametroMap);

		Map<String, String> mapReqMin = new HashMap<>();
		List<MaestraDetalle> lstReqMin = maestraDetalleRespository.findDetalleByCodeCabecera("TIP_ESTADO_REQ_MINIMO");
		for (MaestraDetalle maestraCebecera : lstReqMin) {
			mapReqMin.put(maestraCebecera.getCodProg(), maestraCebecera.getDescripcion());
		}

		Map<String, String> mapEvaCur = new HashMap<>();
		List<MaestraDetalle> lstEvaCur = maestraDetalleRespository
				.findDetalleByCodeCabecera("TIP_ESTADO_EVA_CURRICULAR");
		for (MaestraDetalle maestra : lstEvaCur) {
			mapEvaCur.put(maestra.getCodProg(), maestra.getDescripcion());
		}

		for (ObtenerPerfilConfigDTO obtenerPerfilConfigDTO : listaPerfilDTO) {
			obtenerPerfilConfigDTO.setDescEstadoRm(mapReqMin.get(obtenerPerfilConfigDTO.getEstadoRm()));
			obtenerPerfilConfigDTO.setDescEstadoEc(mapEvaCur.get(obtenerPerfilConfigDTO.getEstadoEc()));
		}

		RespObtieneLista<ObtenerPerfilConfigDTO> respPayload = new RespObtieneLista<>();
		respPayload.setCount(listaPerfilDTO.size());
		respPayload.setItems(listaPerfilDTO);
		return new RespBase<RespObtieneLista<ObtenerPerfilConfigDTO>>().ok(respPayload);
	}

	@Override
	public RespBase<RespObtieneLista<RespPerfilDTO>> listarPerfilByConvocatoria(Long convocatoriaId) {

		RespObtieneLista<RespPerfilDTO> respPayload = new RespObtieneLista<>();
		Optional<Convocatoria> conv = convocatoriaRepository.findById(convocatoriaId);

		if (conv.isPresent()) {
			Convocatoria objConvocatoria = conv.get();

			BasePerfil filtro = new BasePerfil();
			filtro.setBase(new Base(objConvocatoria.getBase().getBaseId()));

			Example<BasePerfil> example = Example.of(filtro);
			List<BasePerfil> lstPerfilByConvocatoria = basePerfilRepository.findAll(example);

			Perfil filtPerfil = new Perfil();
			filtPerfil.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
			Example<Perfil> examplePerfil = Example.of(filtPerfil);

			List<Perfil> lstPerfiles = perfilRepository.findAll(examplePerfil);

			List<RespPerfilDTO> itemsDetalle = new ArrayList<>();
			lstPerfilByConvocatoria.forEach(d -> {
				itemsDetalle.add(new RespPerfilDTO(d.getPerfilId(), lstPerfiles));
			});

			respPayload.setCount(itemsDetalle.size());
			respPayload.setItems(itemsDetalle);

		}

		return new RespBase<RespObtieneLista<RespPerfilDTO>>().ok(respPayload);
	}
	
	@Override
	public RespBase<RespObtienePerfil> cambiarEstadoRevision(Long perfilId, Long estado, MyJsonWebToken token) {
		RespObtienePerfil payload = new RespObtienePerfil();
		RespBase<RespObtienePerfil> response = new RespBase<>();

		if (Util.isEmpty(perfilId)) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "perfilId es obligatorio");
			return response;
		}

		if (Util.isEmpty(estado)) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "estado es obligatorio");
			return response;
		}

		Optional<Perfil> perfilOptional = perfilRepository.findById(perfilId);
		if (perfilOptional.isPresent()) {
			Perfil perfil = perfilOptional.get();
			perfil.setIndEstadoRevision(estado);
			perfilRepository.save(perfil);
		} else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
					"no se encontro el perfil con el id: " + perfilId);
			return response;
		}

		return new RespBase<RespObtienePerfil>().ok(payload);
	}

}
