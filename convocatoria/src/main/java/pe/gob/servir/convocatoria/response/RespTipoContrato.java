package pe.gob.servir.convocatoria.response;


import java.util.List;

import lombok.Data;
import pe.gob.servir.convocatoria.request.dto.TipoContratoDTO;

@Data
public class RespTipoContrato {
	
	private List<TipoContratoDTO> lstTipoContrato;
	
}
