package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.jmapper.JMapper;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.MaestraConocimiento;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.repository.MaestraConocimientoRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqMaestraConocimiento;
import pe.gob.servir.convocatoria.request.dto.MaestraConocimientoDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespListaMaestraConocimiento;
import pe.gob.servir.convocatoria.response.RespMaestraConocimiento;
import pe.gob.servir.convocatoria.response.RespObtieneConocimiento;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.MaestraConocimientoService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;


@Service
public class MaestraConocimientoServiceImpl implements MaestraConocimientoService{

	@Autowired
	MaestraConocimientoRepository maestraConocimientoRepository;
	
	@Autowired
	MaestraDetalleRepository maestraDetalleRepository;
	
	@Transactional(transactionManager = "convocatoriaTransactionManager")
	@Override
	public RespBase<RespMaestraConocimiento> guardarMaestraConocimiento(ReqBase<ReqMaestraConocimiento> request,
			MyJsonWebToken token, Long maeConocimientoId) {
		
		RespBase<RespMaestraConocimiento> response = new RespBase<>();
		MaestraConocimiento maestraConocimiento = null;
		MaestraConocimientoDTO maestraConocimientoDTO = null;

		if (maeConocimientoId != null) {
			Optional<MaestraConocimiento> maestraConocimientoFind = maestraConocimientoRepository.findById(maeConocimientoId);

			if (maestraConocimientoFind.isPresent()) {
				maestraConocimiento = maestraConocimientoFind.get();
			} else {
				response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
						"No Existe el maeConocimientoId Ingresado");
				return response;
			}

			maestraConocimiento.setCampoSegUpd(maestraConocimiento.getEstadoRegistro(), token.getUsuario().getUsuario(),
					Instant.now());
		} else {
			maestraConocimiento = new MaestraConocimiento();
			maestraConocimiento.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
		}
		
		Optional<MaestraDetalle> tipoConocimientoOptional =  maestraDetalleRepository.
				findById(request.getPayload().getMaestraConocimiento().getTipoConocimientoId());
		
		if(!tipoConocimientoOptional.isPresent()) {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
					"El tipoConocimientoId ingresado es Invalido");
			return response;
		}
		
		maestraConocimiento.setTipoConocimientoId(request.getPayload().getMaestraConocimiento().getTipoConocimientoId());
		maestraConocimiento.setCategoriaConocimientoId(request.getPayload().getMaestraConocimiento().getCategoriaConocimientoId());
		maestraConocimiento.setDescripcion(request.getPayload().getMaestraConocimiento().getDescripcion());
		maestraConocimiento.setEntidadId(request.getPayload().getMaestraConocimiento().getEntidadId());
		

		maestraConocimientoRepository.save(maestraConocimiento);
		RespMaestraConocimiento payload = new RespMaestraConocimiento();
		maestraConocimientoDTO = obtenerMaestraConocimiento(maestraConocimiento.getMaeConocimientoId());
		payload.setMaestraConocimiento(maestraConocimientoDTO);

		
		return new RespBase<RespMaestraConocimiento>().ok(payload);
		
	}

	@Override
	public RespBase<RespListaMaestraConocimiento> obtenerMaestraConocimiento(Map<String, Object> parametroMap) {
		List<MaestraConocimiento> lstConcomientos = null;
		List<MaestraConocimientoDTO> lstConocimientoDTO = new ArrayList<>();
		List<MaestraDetalle> lstMaeTipConcimiento = new ArrayList<>();
		List<MaestraDetalle> lstMaeCatConocimiento = new ArrayList<>();

		MaestraConocimiento maeConocimiento = new MaestraConocimiento();
		if (!Util.isEmpty(parametroMap.get("tipoConocimiento")))
			maeConocimiento.setTipoConocimientoId((Long) parametroMap.get("tipoConocimiento"));
		if (!Util.isEmpty(parametroMap.get("categoriaConocimiento")))
			maeConocimiento.setCategoriaConocimientoId((Long) parametroMap.get("categoriaConocimiento"));
		if (!Util.isEmpty(parametroMap.get("descripcion")))
			maeConocimiento.setDescripcion((String) parametroMap.get("descripcion"));
		maeConocimiento.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase("descripcion")
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		Example<MaestraConocimiento> conocimientoExample = Example.of(maeConocimiento, matcher);
		lstConcomientos = maestraConocimientoRepository.findAll(conocimientoExample);

		lstMaeTipConcimiento = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TIPO_CONOCIMIENTO);
		lstMaeCatConocimiento = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_CATEGORIA_CONOCIMIENTO);

		Map<Long, String> mapTipoConoCodigo = lstMaeTipConcimiento.stream().filter(Objects::nonNull)
				.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getCodProg));
		
		Map<Long, String> mapTipoConoDescripcion = lstMaeTipConcimiento.stream().filter(Objects::nonNull)
				.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
		
		Map<Long, String> mapCateConoDescripcion = lstMaeCatConocimiento.stream().filter(Objects::nonNull)
				.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));


		lstConcomientos.stream().filter(Objects::nonNull).forEach(c -> {
			MaestraConocimientoDTO conocimiento = new MaestraConocimientoDTO();
			conocimiento.setMaeConocimientoId(c.getMaeConocimientoId());
			conocimiento.setTipoConocimientoId(c.getTipoConocimientoId());
			conocimiento.setCategoriaConocimientoId(c.getCategoriaConocimientoId());
			conocimiento.setDescripcion(c.getDescripcion());
			conocimiento.setEstado(c.getEstadoRegistro());
			conocimiento.setEntidadId(c.getEntidadId());
			conocimiento.setCodigoTipoConocimiento(mapTipoConoCodigo.get(c.getTipoConocimientoId()));
			conocimiento.setDescripcionTipo(mapTipoConoDescripcion.get(c.getTipoConocimientoId()));
			conocimiento.setDescripcionCategoria(mapCateConoDescripcion.get(c.getCategoriaConocimientoId()));
			lstConocimientoDTO.add(conocimiento);
		});

		RespListaMaestraConocimiento payload = new RespListaMaestraConocimiento();
		payload.setLstMaestraConocimiento(lstConocimientoDTO);

		return new RespBase<RespListaMaestraConocimiento>().ok(payload);
	}
	
	public MaestraConocimientoDTO obtenerMaestraConocimiento(Long maeConocimientoId) {
		MaestraConocimiento conocimiento = null;
		MaestraConocimientoDTO conocimientoDTO = null;

		Optional<MaestraConocimiento> conocimientoOptional = maestraConocimientoRepository.findById(maeConocimientoId);

		if (conocimientoOptional.isPresent()) {
			conocimiento = conocimientoOptional.get();
			
			JMapper<MaestraConocimientoDTO, MaestraConocimiento> conocimientoMapper = new JMapper<>(
					MaestraConocimientoDTO.class, MaestraConocimiento.class);

			conocimientoDTO = conocimientoMapper.getDestination(conocimiento);

		}
		
		return conocimientoDTO;
	}

	@Override
	public RespBase<RespObtieneConocimiento> obtenerMaestraConocimientoPorId(Long maeConocimientoId) {
		
		RespBase<RespObtieneConocimiento> response = new RespBase<>();
		MaestraConocimiento conocimiento = null;
		MaestraConocimientoDTO conocimientoDTO = null;
		List<MaestraDetalle> lstMaeTipConcimiento = new ArrayList<>();
		List<MaestraDetalle> lstMaeCatConocimiento = new ArrayList<>();
		
        if (Util.isEmpty(maeConocimientoId)) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "el id de bonificacion es obligatorio");
            return response;
        }

		Optional<MaestraConocimiento> conocimientoOptional = maestraConocimientoRepository.findById(maeConocimientoId);

		if (conocimientoOptional.isPresent()) {
			
			lstMaeTipConcimiento = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TIPO_CONOCIMIENTO);
			lstMaeCatConocimiento = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_CATEGORIA_CONOCIMIENTO);

			Map<Long, String> mapTipoConoDescripcion = lstMaeTipConcimiento.stream().filter(Objects::nonNull)
					.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
			
			Map<Long, String> mapCateConoDescripcion = lstMaeCatConocimiento.stream().filter(Objects::nonNull)
					.collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
			
			
			conocimiento = conocimientoOptional.get();
			
			JMapper<MaestraConocimientoDTO, MaestraConocimiento> conocimientoMapper = new JMapper<>(
					MaestraConocimientoDTO.class, MaestraConocimiento.class);

			conocimientoDTO = conocimientoMapper.getDestination(conocimiento);
			conocimientoDTO.setDescripcionTipo(mapTipoConoDescripcion.get(conocimiento.getTipoConocimientoId()));
			conocimientoDTO.setDescripcionCategoria(mapCateConoDescripcion.get(conocimiento.getCategoriaConocimientoId()));
			conocimientoDTO.setEstado(conocimiento.getEstadoRegistro());

		}else {
			 response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
	                    "Conocimiento no existe con el id: " + maeConocimientoId);
	            return response;
		}
		
		RespObtieneConocimiento payload = new RespObtieneConocimiento();
		payload.setConocimiento(conocimientoDTO);
	
		return new RespBase<RespObtieneConocimiento>().ok(payload);
	}

	@Override
	public RespBase<Object> eliminarConocimiento(MyJsonWebToken token, Long maeConocimientoId) {
		
		RespBase<Object> response = new RespBase<>();
		Optional<MaestraConocimiento> conocimientoFind =  maestraConocimientoRepository.findById(maeConocimientoId);
		if (conocimientoFind.isPresent()){
			MaestraConocimiento conocimiento = conocimientoFind.get();
			
			conocimiento.setCampoSegUpd(EstadoRegistro.INACTIVO.getCodigo(), token.getUsuario().getUsuario(),
					Instant.now());
			maestraConocimientoRepository.save(conocimiento);
			
			response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se eliminó el conocimiento correctamente ");
		}else {
			response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe el maeConocimientoId Ingresado");
		}
		return response;
		
	}
		
	
}
