package pe.gob.servir.convocatoria.request;

import javax.validation.constraints.NotNull;

import lombok.Data;
import pe.gob.servir.convocatoria.common.Constantes;

@Data
public class ReqEstadoComunicado {
	
	@NotNull(message = Constantes.CAMPO + " estadoId " + Constantes.ES_OBLIGATORIO)
	private Long estadoId;

	private String observacion;

	private Long gestorId;

	private Long coordinadorId;

	private String nombreGestor;

	private String nombreCoordinador;
	
	private Long estadoConvocatoriaId;
	/*@Pattern(regexp = "2|3", message = Constantes.CAMPO + " codigoEstadoConvocatoria " + Constantes.ES_INVALIDO + ", valores permitidos 2(DESIERTA), 3(CANCELADA)")
	private String codigoEstadoConvocatoria;
	*/

	/*@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "pt-PE", timezone = "Peru/East")
	private Date fechaPublicacion;
	 */
}
