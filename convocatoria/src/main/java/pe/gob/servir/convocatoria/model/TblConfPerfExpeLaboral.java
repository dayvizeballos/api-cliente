package pe.gob.servir.convocatoria.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tbl_conf_perf_expe_laboral", schema = "sch_convocatoria")
@NamedQuery(name="TblConfPerfExpeLaboral.findAll", query="SELECT t FROM TblConfPerfExpeLaboral t")
public class TblConfPerfExpeLaboral extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "conf_perf_expe_laboral_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_expe_laboral")
    @SequenceGenerator(name = "seq_conf_perf_expe_laboral", sequenceName = "seq_conf_perf_expe_laboral", schema = "sch_convocatoria", allocationSize = 1)
    private Long confPerfExpeLaboralId;

    @ManyToOne
    @JoinColumn(name = "perfil_id", nullable = false)
    private Perfil perfil;

    @Column(name = "tipo_experiencia", nullable = false)
    private Long tipoExperiencia;

    @Column(name = "desde_anios")
    private Long desdeAnios;

    @Column(name = "hasta_anios")
    private Long hastaAnios;

    @Column(name = "requisito_minimo")
    private Boolean requisitoMinimo;

    @Column(name = "puntaje")
    private Long puntaje;

    @Column(name = "nivel_academico")
    private String nivelAcademico;

    @Column(name = "nivel_educativo_id")
    private Long nivelEducativoId;

    @Column(name = "situacion_academica_id")
    private Long situacionAcademicaId;

    @Column(name = "nivel_educativo_desc")
    private String nivelEducativoDesc;

    @Column(name = "sit_academica_desc")
    private String situacionAcademicaDesc;

    @Column(name = "base_id")
    private Long baseId;

    public TblConfPerfExpeLaboral() {
        super();
    }
}
