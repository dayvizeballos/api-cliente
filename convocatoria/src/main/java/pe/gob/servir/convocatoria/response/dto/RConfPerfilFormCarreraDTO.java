package pe.gob.servir.convocatoria.response.dto;

import java.io.Serializable;
import java.util.List;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.AllArgsConstructor;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.TblConfPerfFormacCarrera;
import pe.gob.servir.convocatoria.request.dto.CarreraFormacionAcademicaOtrosGradosDTO;

@JGlobalMap
@Getter
@Setter
@SuppressWarnings("serial")
@AllArgsConstructor
public class RConfPerfilFormCarreraDTO implements Serializable {

	private Long carreraId;
	private Long nivelEducativoId;
	private String descNivelEducativo;
	private Long situacionAcademicaId;
	private String descAcademica;
	private String descTipo;
	private String descCarreras;
	private String carreraIds;
	private Boolean flagReqMin;
	private Integer puntaje;
	private List<CarreraFormacionAcademicaOtrosGradosDTO> lstOtrosGrados;
	
	public RConfPerfilFormCarreraDTO (TblConfPerfFormacCarrera oConfig) {
		this.carreraId = oConfig.getConfPerfFormacCarreraId();
		this.nivelEducativoId = oConfig.getNivelEducativoId();
		this.descNivelEducativo = oConfig.getDescripcionNivelEducativo();
		this.situacionAcademicaId = oConfig.getSituacionAcademicaId();
		this.descAcademica = oConfig.getDescripcionAcademica();
		this.descTipo = oConfig.getDescripcionNivelEducativo() + " " +oConfig.getDescripcionAcademica();
		this.descCarreras = oConfig.getDescripcionCarrera();
		this.carreraIds = oConfig.getCarreraIds();
		this.flagReqMin = oConfig.getRequisitoMinimo();
		this.puntaje = oConfig.getPuntaje();
	}
	
	public RConfPerfilFormCarreraDTO() {
		
	}
	
}
