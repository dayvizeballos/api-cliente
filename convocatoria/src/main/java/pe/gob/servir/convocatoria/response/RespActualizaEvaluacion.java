package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.Evaluacion;
import pe.gob.servir.convocatoria.model.EvaluacionEntidad;
import pe.gob.servir.convocatoria.model.Jerarquia;

@Getter
@Setter
public class RespActualizaEvaluacion {
	
	private Jerarquia jerarquia;
	
	private List<Evaluacion> listaEvaluacion;

	private List<EvaluacionEntidad> listaEvaluacionEntidad;
}
