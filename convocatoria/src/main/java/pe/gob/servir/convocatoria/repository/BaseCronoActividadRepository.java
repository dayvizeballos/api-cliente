package pe.gob.servir.convocatoria.repository;

import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import pe.gob.servir.convocatoria.model.BaseCronograma;
import pe.gob.servir.convocatoria.model.BaseCronogramaActividad;

import java.util.List;


public interface BaseCronoActividadRepository extends JpaRepository<BaseCronogramaActividad , Long> , JpaSpecificationExecutor<BaseCronogramaActividad> {
    @Query("select b from BaseCronogramaActividad b where b.baseCronograma.basecronogramaId = :basecronogramaId and b.estadoRegistro = '1' order by b.actividadId asc")
     List<BaseCronogramaActividad> findAllActividadesXCronograma(@Param("basecronogramaId") Integer basecronogramaId);


     List<BaseCronogramaActividad> findBybaseCronogramaAndEstadoRegistro(BaseCronograma baseCronograma , String activo);

}
