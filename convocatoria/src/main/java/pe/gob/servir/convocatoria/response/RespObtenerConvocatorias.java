package pe.gob.servir.convocatoria.response;

import lombok.Data;

import java.util.List;

@Data
public class RespObtenerConvocatorias {
    private List<RespConvocatoriaJob> convocatorias;
}
