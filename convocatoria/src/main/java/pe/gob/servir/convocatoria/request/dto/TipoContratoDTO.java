package pe.gob.servir.convocatoria.request.dto;

import lombok.Data;

@Data
public class TipoContratoDTO {
	
	private Long tipoContratoId;
	private String descPlantilla;
	private String codigoPlantilla;
	private String ruta;
	private Long regimenId;

}
