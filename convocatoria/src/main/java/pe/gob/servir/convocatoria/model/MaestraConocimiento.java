package pe.gob.servir.convocatoria.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbl_mae_conocimiento", schema = "sch_convocatoria")
@Getter
@Setter
public class MaestraConocimiento extends AuditEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mae_conocimiento")
	@SequenceGenerator(name = "seq_mae_conocimiento", sequenceName = "seq_mae_conocimiento", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "mae_conocimiento_id")
	private Long maeConocimientoId;
	
	@Column(name = "tipo_conocimiento_id")
	private Long tipoConocimientoId;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "entidad_id")
	private Long entidadId;
	
	@Column(name = "categoria_conocimiento_id")
	private Long categoriaConocimientoId;
	
	@Transient
	private String codigoTipoConocimiento;
	
	@Transient
	private String descripcionTipo;
	
	@Transient
	private String descripcionCategoria;
		
	@Transient
	private String estado;
}
