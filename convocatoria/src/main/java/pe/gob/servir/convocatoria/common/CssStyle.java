package pe.gob.servir.convocatoria.common;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.annotation.StyleCss;

@Getter
@Setter
public class CssStyle {
	
	@StyleCss(styleName = "width")
	private String width;
	@StyleCss(styleName = "height")
	private String height;
	@StyleCss(styleName = "text-align")
	private String textAlign;
	@StyleCss(styleName = "color")
	private String color;
	@StyleCss(styleName = "font-size")
	private String fontSize;
	@StyleCss(styleName = "font-family")
	private String fontFamily;
	@StyleCss(styleName = "font-style")
	private String fontStyle;
	

}
