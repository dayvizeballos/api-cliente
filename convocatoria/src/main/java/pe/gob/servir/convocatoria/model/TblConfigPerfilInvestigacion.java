package pe.gob.servir.convocatoria.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "tbl_conf_perf_investigacion", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
@NamedQuery(name="TblConfigPerfilInvestigacion.findAll", query="SELECT t FROM TblConfigPerfilInvestigacion t")
public class TblConfigPerfilInvestigacion extends AuditEntity implements Serializable {
	
		private static final long serialVersionUID = 1L;
	
	 	@Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conf_perf_investigacion")
	    @SequenceGenerator(name = "seq_conf_perf_investigacion", sequenceName = "seq_conf_perf_investigacion", schema = "sch_convocatoria", allocationSize = 1)
	    
	    @Column(name = "conf_investigacion_id")
	    private Long confInvestigacionId;
	    
		@JoinColumn(name = "perfil_id")
		@ManyToOne(optional = false)
		@JsonBackReference
		private Perfil perfil;

	    @Column(name = "requisito_id")
	    private Long requisitoId;

	    @Column(name = "descripcion")
	    private String descripcion;

	    @Column(name = "requisito_minimo")
	    private Boolean  requisitoMinimo;
	   	    
	    @Column(name = "base_id")
	    private Long baseId;
	    

}
