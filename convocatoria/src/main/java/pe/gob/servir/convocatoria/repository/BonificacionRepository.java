package pe.gob.servir.convocatoria.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.Bonificacion;

@Repository
public interface BonificacionRepository extends JpaRepository<Bonificacion,Long>  {
	
	@Query("SELECT b FROM Bonificacion b WHERE 1 = 1 \r\n" + 
			"AND b.bonificacionId=:idInformeDetalle AND b.estadoRegistro = '1'")
	public Bonificacion findByBonificacionEnableById(@Param("idInformeDetalle")Long idInformeDetalle);
	
	@Query("SELECT  b FROM  Bonificacion b WHERE b.bonificacionId = :bonificacionId ")
    public Optional<Bonificacion> findByIdBonificacion(@Param("bonificacionId")Long bonificacionId);
	
	@Query("SELECT  b FROM  Bonificacion b WHERE b.tipoInfoId = :idTipoInfo ")
    public List<Bonificacion> findByIdTipoInfo(@Param("idTipoInfo")Long idTipoInfo);

}
