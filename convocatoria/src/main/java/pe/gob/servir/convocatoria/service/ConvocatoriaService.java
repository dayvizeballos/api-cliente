package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.response.*;
import pe.gob.servir.convocatoria.response.dto.RespPerfilesConvocatoriaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;


import java.util.Date;

public interface ConvocatoriaService {


    RespBase<RespObtieneLista<ConvocatoriaDTO>> findConvocatorias(Long entidadId ,Long  regimen , Long etapa ,
                                                                 Date fechaIni , Date fechaFin ,
                                                                 Long estado , int pageNumber , int pageSize);
    
   RespBase<RespObtenerConvocatorias> listaConvocatoria(Date fechaDia);
    
    void registrarConvocatoria(Long baseId, MyJsonWebToken token);

    RespBase<RespToContratoConvenio> findDatosToContratosToConvenios(Long base , Long  postulante , Long perfil , Long entidad);

	RespBase<ConvocatoriaInfoDTO> obtenerConvocatoriaById(Long convocatoriaId);
	
	RespBase<RespObtieneLista<RespPerfilesConvocatoriaDTO>> listarPerfilesDetallePorConvocatoria(Long convocatoriaId);
	
	RespBase<RespEtapasEvaluacion> listarEtapasEvaluaciones(Long baseId);
	
	RespBase<Object> actualizarConvocatoriaEtapa(String fechaHoy);
	
	RespBase<RespObtenerConvEtapa> listarConvocatoriaCorreoEtapa(String fechaHoy);
	
	RespBase<RespObtenerConvEtapa> listarConvocatoriaCorreoEtapaProx(String fechaHoy);

	RespBase<DatosPuestoDTO> buscarDatosPuesto(Long baseId);

	RespBase<DatosReporteDTO> buscarInfoReportePostulante(Long baseId);
}

