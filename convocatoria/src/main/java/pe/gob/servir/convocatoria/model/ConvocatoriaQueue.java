package pe.gob.servir.convocatoria.model;


import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ConvocatoriaQueue {
    private Long convocatoriaId;
    private Long baseId;
}
