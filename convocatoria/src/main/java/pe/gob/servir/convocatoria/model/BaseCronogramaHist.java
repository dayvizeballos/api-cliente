package pe.gob.servir.convocatoria.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.audit.AuditableEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tbl_base_cronograma_hist", schema = "sch_base")
@Getter
@Setter
public class BaseCronogramaHist extends AuditEntity implements AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_base_cronograma_hist")
    @SequenceGenerator(name = "seq_base_cronograma_hist", sequenceName = "seq_base_cronograma_hist", schema = "sch_base", allocationSize = 1)
    @Column(name = "base_cronograma_hist_id")
    private Integer basecronogramaId;

    @JoinColumn(name = "etapa_id")
    @ManyToOne(optional = false)
    private MaestraDetalle etapa;

    @JoinColumn(name = "base_id")
    @ManyToOne(optional = false)
    private Base base;

    @JoinColumn(name = "convocatoria_id")
    @ManyToOne()
    private Convocatoria convocatoria;

    @Column(name = "descripcion_cronograma")
    private String descripcion;

    @Column(name = "responsable")
    private String responsable;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "periodo_ini")
    private Date periodoini;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "periodo_fin")
    private Date periodofin;

	/*
	@Column(name = "hora_ini")
	private String horaIni;
	
	@Column(name = "hora_fin")
	private String horaFin;
	 */

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "baseCronogramaHist")
    @Column(nullable = true)
    @JsonManagedReference
    private List<BaseCronogramaActividadHist> baseCronogramaActividadHistList;
}
