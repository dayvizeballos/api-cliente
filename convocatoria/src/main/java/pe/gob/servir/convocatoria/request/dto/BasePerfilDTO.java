package pe.gob.servir.convocatoria.request.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.Constantes;

@JGlobalMap
@Getter
@Setter
public class BasePerfilDTO {

	private Long basePerfilId;
	@NotNull(message = Constantes.CAMPO + " baseId " + Constantes.ES_OBLIGATORIO)
	private Long baseId;
	@NotNull(message = Constantes.CAMPO + " perfilId " + Constantes.ES_OBLIGATORIO)
	private Long perfilId;
	@Size(max = 50, message = Constantes.CAMPO + " jornadaLaboral " + Constantes.ES_INVALIDO + ", máximo 50 ")
	private String jornadaLaboral;
	private Integer vacante;
	private Double remuneracion;
	@Pattern(regexp = "1|0", message = Constantes.CAMPO + " indContratoIndeterminado " + Constantes.ES_INVALIDO + ", valores permitidos 1(ACTIVO), 0(INACTIVO)")
	private String indContratoIndeterminado;
	private Integer tiempoContrato;
	private Long condicionTrabajoId;
	@NotNull(message = Constantes.CAMPO + " sedeId " + Constantes.ES_OBLIGATORIO)
	private Long sedeId;
	@Size(max = 250, message = Constantes.CAMPO + " sedeDireccion " + Constantes.ES_INVALIDO + ", máximo 500 ")
	private String sedeDireccion;
	@Size(max = 500, message = Constantes.CAMPO + " condicionEsencial " + Constantes.ES_INVALIDO + ", máximo 500 ")
	private String condicionEsencial;
	@Size(max = 500, message = Constantes.CAMPO + " observaciones " + Constantes.ES_INVALIDO + ", máximo 500 ")
	private String observaciones;
	private Long modalidadContratoId;
	private String estado;
	//datos de Perfil
	private String nombrePerfil;
	
	@NotNull(message = Constantes.CAMPO + " baseHorarioDTOList " + Constantes.ES_OBLIGATORIO)
	private List<BaseHorarioDTO> baseHorarioDTOList;
	
    private Integer depaId;
    private String descDepa;
    private Integer provId;
    private String descProv;
    private Integer distId;
    private String descDist;
    
}
