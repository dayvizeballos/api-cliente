package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.gob.servir.convocatoria.model.ConfigPerfilFormacionCarreraDetalle;

public interface FormacionCarreraDetalleRepository extends JpaRepository<ConfigPerfilFormacionCarreraDetalle, Long> {

}
