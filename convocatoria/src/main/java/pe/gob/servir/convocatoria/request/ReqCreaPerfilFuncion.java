package pe.gob.servir.convocatoria.request;


import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.PerfilFuncionDTO;

@Getter
@Setter
public class ReqCreaPerfilFuncion {
	
	private PerfilFuncionDTO perfilFuncion;

}
