package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.TblConfPerfOtrosRequisitos;

@Repository
public interface TblConfPerfOtrosRequisitosRepository extends JpaRepository<TblConfPerfOtrosRequisitos, Long> {

}