package pe.gob.servir.convocatoria.feign.client;

import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(name = "postulanteApi", url = "${postulante.private.base.url}")
public interface PostulanteClient {

}
