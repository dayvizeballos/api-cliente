package pe.gob.servir.convocatoria.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BasePerfil;
import pe.gob.servir.convocatoria.model.Evaluacion;
import pe.gob.servir.convocatoria.model.EvaluacionEntidad;
import pe.gob.servir.convocatoria.model.Jerarquia;
import pe.gob.servir.convocatoria.repository.BaseCronogramaActividadRepository;
import pe.gob.servir.convocatoria.repository.BaseCronogramaRepository;
import pe.gob.servir.convocatoria.repository.BasePerfilRepository;
import pe.gob.servir.convocatoria.repository.BaseRepository;
import pe.gob.servir.convocatoria.repository.BaseRepositoryJdbc;
import pe.gob.servir.convocatoria.repository.EvaluacionEntidadRepository;
import pe.gob.servir.convocatoria.repository.EvaluacionEntidadRepositoryJdbc;
import pe.gob.servir.convocatoria.repository.EvaluacionRepository;
import pe.gob.servir.convocatoria.repository.EvaluacionRepositoryJdbc;
import pe.gob.servir.convocatoria.repository.JerarquiaRepository;
import pe.gob.servir.convocatoria.repository.MaestraCabeceraRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.request.ReqActualizaEvaluacion;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqCreaEvaluacion;
import pe.gob.servir.convocatoria.request.ReqParamEvaluacion;
import pe.gob.servir.convocatoria.request.dto.BaseDTO;
import pe.gob.servir.convocatoria.request.dto.BasePerfilBean;
import pe.gob.servir.convocatoria.request.dto.DetalleEvaluacionEntidadDTO;
import pe.gob.servir.convocatoria.request.dto.EvaluacionConvocatoriaDTO;
import pe.gob.servir.convocatoria.request.dto.EvaluacionDTO;
import pe.gob.servir.convocatoria.request.dto.JerarquiaDTO;
import pe.gob.servir.convocatoria.request.dto.MasterEvaluacionDTO;
import pe.gob.servir.convocatoria.request.dto.ModalidadDTO;
import pe.gob.servir.convocatoria.request.dto.RegimenDTO;
import pe.gob.servir.convocatoria.request.dto.TipoDTO;
import pe.gob.servir.convocatoria.response.RespActualizaEvaluacion;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespCreaEvaluacion;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.dto.RespObtenerPuntajesEvaluacionDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.EvaluacionService;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;


@Service
public class EvaluacionServiceImpl implements EvaluacionService {

    @Autowired
    private JerarquiaRepository jerarquiaRepository;

    @Autowired
    private EvaluacionRepository evaluacionRepository;

    @Autowired
    private EvaluacionEntidadRepository evaluacionEntidadRepository;

    @Autowired
    private MaestraDetalleRepository maestraDetalleRepository;

    @Autowired
    private EvaluacionRepositoryJdbc evaluacionRepositoryJdbc;

    
    @Autowired
    private BaseRepositoryJdbc baseRepositoryJdbc;
    
    @Autowired
    private EvaluacionEntidadRepositoryJdbc evaluacionEntidadRepositoryJdbc;

    @Autowired
    private BasePerfilRepository basePerfilRepository;

    @Autowired
    private BaseRepository baseRepository;

    @Autowired
    EvaluacionService evaluacionService;
    
    @Autowired
    BaseCronogramaRepository baseCronogramaRepository;
    
    @Autowired
    BaseCronogramaActividadRepository baseCronogramaActividadRepository;
    
    @Autowired
    MaestraCabeceraRepository maestraCabeceraRepository;
    

    @Transactional(transactionManager = "convocatoriaTransactionManager")
    @Override
    public RespBase<RespCreaEvaluacion> creaEvaluacion(ReqBase<ReqCreaEvaluacion> request, MyJsonWebToken token) {

        List<Jerarquia> listaJerarquia = new ArrayList<>();
        if (request.getPayload().getListaJerarquia() != null) {
            for (JerarquiaDTO jerarquiaDTO : request.getPayload().getListaJerarquia()) {
                Jerarquia jerarquia = new Jerarquia();
                jerarquia.setCodigoNivel1(jerarquiaDTO.getCodigoNivel1());
                jerarquia.setCodigoNivel2(jerarquiaDTO.getCodigoNivel2());
                jerarquia.setCodigoNivel3(jerarquiaDTO.getCodigoNivel3());
                jerarquia.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                jerarquiaRepository.save(jerarquia);

                List<Evaluacion> listaEvaluacion = new ArrayList<>();

                for (EvaluacionDTO evaluacionDTO : jerarquiaDTO.getListaEvaluacion()) {
                    Evaluacion evaluacion = new Evaluacion();
                    evaluacion.setJerarquiaId(jerarquia.getJerarquiaId());
                    evaluacion.setTipoEvaluacionId(evaluacionDTO.getTipoEvaluacionId());
                    evaluacion.setOrden(evaluacionDTO.getOrden());
                    evaluacion.setPeso(evaluacionDTO.getPeso());
                    evaluacion.setPuntajeMinimo(evaluacionDTO.getPuntajeMinimo());
                    evaluacion.setPuntajeMaximo(evaluacionDTO.getPuntajeMaximo());
                    evaluacion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    evaluacionRepository.save(evaluacion);
                    listaEvaluacion.add(evaluacion);
                }
                jerarquia.setListaEvaluacion(listaEvaluacion);
                listaJerarquia.add(jerarquia);
            }
        }

        RespCreaEvaluacion respPayload = new RespCreaEvaluacion();

        respPayload.setListaJerarquia(listaJerarquia);
        return new RespBase<RespCreaEvaluacion>().ok(respPayload);
    }

    @Transactional(transactionManager = "convocatoriaTransactionManager")
    @Override
    public RespBase<RespActualizaEvaluacion> actualizarEvaluacion(ReqBase<ReqActualizaEvaluacion> request,
                                                                  MyJsonWebToken token, Long jerarquiaId) {

        RespActualizaEvaluacion respPayload = new RespActualizaEvaluacion();
        RespBase<RespActualizaEvaluacion> response = new RespBase<>();
        Optional<Jerarquia> jerarquiaFind = jerarquiaRepository.findById(jerarquiaId);
        if (jerarquiaFind.isPresent()) {
            Jerarquia jerarquia = jerarquiaFind.get();
            List<Evaluacion> listaEvaluacion = new ArrayList<>();
            if (request.getPayload().getListaEvaluacion() != null) {
                for (EvaluacionDTO evaluacionDTO : request.getPayload().getListaEvaluacion()) {
                    Evaluacion evaluacion = null;
                    if (evaluacionDTO.getEvaluacionId() != null) {
                        Optional<Evaluacion> evaluacionFind = evaluacionRepository.
                                findById(evaluacionDTO.getEvaluacionId());
                        if (evaluacionFind.isPresent()) {
                            evaluacion = evaluacionFind.get();
                            evaluacion.setCampoSegUpd((Strings.isEmpty(evaluacionDTO.getEstado()) ? EstadoRegistro.ACTIVO.getCodigo() : evaluacionDTO.getEstado()), token.getUsuario().getUsuario(), Instant.now());
                        } else {
                            evaluacion = new Evaluacion();
                            evaluacion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        }

                    } else {
                        evaluacion = new Evaluacion();
                        evaluacion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    }

                    evaluacion.setJerarquiaId(jerarquia.getJerarquiaId());
                    evaluacion.setTipoEvaluacionId(evaluacionDTO.getTipoEvaluacionId());
                    evaluacion.setOrden(evaluacionDTO.getOrden());
                    evaluacion.setPeso(evaluacionDTO.getPeso());
                    evaluacion.setPuntajeMinimo(evaluacionDTO.getPuntajeMinimo());
                    evaluacion.setPuntajeMaximo(evaluacionDTO.getPuntajeMaximo());
                    evaluacionRepository.save(evaluacion);
                    listaEvaluacion.add(evaluacion);
                }
            }

            respPayload.setListaEvaluacion(listaEvaluacion);
            return new RespBase<RespActualizaEvaluacion>().ok(respPayload);
        } else {

            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "No existe la jerarquiaId Ingresada");
            return response;
        }


    }

    @Override
    public RespBase<RespActualizaEvaluacion> actualizarEvaluacioneEntidad(ReqBase<ReqActualizaEvaluacion> request, MyJsonWebToken token, Long jerarquiaId) {

        RespActualizaEvaluacion respPayload = new RespActualizaEvaluacion();
        RespBase<RespActualizaEvaluacion> response = new RespBase<>();
        Optional<Jerarquia> jerarquiaFind = jerarquiaRepository.findById(jerarquiaId);

        if (jerarquiaFind.isPresent()) {
            Jerarquia jerarquia = (Jerarquia) jerarquiaFind.get();
            List<EvaluacionEntidad> listaEvaluacion = new ArrayList<>();
            if (request.getPayload().getListaEvaluacion() != null) {
                for (EvaluacionDTO evaluacionDTO : request.getPayload().getListaEvaluacion()) {
                    EvaluacionEntidad evaluacion = null;
                    if (evaluacionDTO.getEvaluacionEntidad() != null) {
                        Optional<EvaluacionEntidad> evaluacionFind = evaluacionEntidadRepository.
                                findById(evaluacionDTO.getEvaluacionEntidad());
                        if (evaluacionFind.isPresent()) {
                            evaluacion = evaluacionFind.get();
                            evaluacion.setCampoSegUpd((Strings.isEmpty(evaluacionDTO.getEstado()) ? EstadoRegistro.ACTIVO.getCodigo() : evaluacionDTO.getEstado()), token.getUsuario().getUsuario(), Instant.now());
                        } else {
                            evaluacion = new EvaluacionEntidad();
                            evaluacion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                        }

                    } else {
                        evaluacion = new EvaluacionEntidad();
                        evaluacion.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
                    }

                    evaluacion.setJerarquiaId(jerarquia.getJerarquiaId());
                    evaluacion.setTipoEvaluacionId(evaluacionDTO.getTipoEvaluacionId());
                    evaluacion.setOrden(evaluacionDTO.getOrden());
                    evaluacion.setPeso(evaluacionDTO.getPeso());
                    evaluacion.setPuntajeMinimo(evaluacionDTO.getPuntajeMinimo());
                    evaluacion.setPuntajeMaximo(evaluacionDTO.getPuntajeMaximo());
                    evaluacion.setEntidadId(evaluacionDTO.getEntidadId());
                    evaluacionEntidadRepository.save(evaluacion);
                    listaEvaluacion.add(evaluacion);
                }
            }

            respPayload.setListaEvaluacionEntidad(listaEvaluacion);
            return new RespBase<RespActualizaEvaluacion>().ok(respPayload);
        } else {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE,
                    "No existe la jerarquiaId Ingresada");
            return response;
        }
    }

    /*
        @Override
        public RespBase<RespObtenerEvaluacion> obtenerEvaluacion(Long cabeceraId, Long modalidadId, Long tipoId, String estadoId) {
            List<MaestraDetalle> lista = null;
            List<MaestraDetalle> listaMod = null;
            List<MaestraDetalle> listaTipo = null;
            List<Evaluacion> listaEvaluacion = null;

            RespObtenerEvaluacion resPayload = new RespObtenerEvaluacion();

            if (cabeceraId != null) {
                lista = new ArrayList<>();
                Optional<MaestraDetalle> listaRegimen = maestraDetalleRepository.findById(cabeceraId);
                if (listaRegimen.isPresent()) {
                    MaestraDetalle reg = listaRegimen.get();
                    reg.setEstado(reg.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                    if (estadoId != null) {
                        List<Jerarquia> listaEstado = jerarquiaRepository.findEstadoById(reg.getMaeDetalleId(), estadoId);
                        listaMod = new ArrayList<>();
                        for (Jerarquia dtoModalidad : listaEstado) {
                            MaestraDetalle modalidad = null;
                            MaestraDetalle buscarModalidad = maestraDetalleRepository
                                    .findModalidadByid(reg.getMaeDetalleId(), dtoModalidad.getCodigoNivel2());
                            if (dtoModalidad.getCodigoNivel2().toString()
                                    .equals(buscarModalidad.getMaeDetalleId().toString())) {
                                modalidad = new MaestraDetalle();
                                modalidad.setMaeDetalleId(buscarModalidad.getMaeDetalleId());
                                modalidad.setMaeCabeceraId(buscarModalidad.getMaeCabeceraId());
                                modalidad.setDescripcion(buscarModalidad.getDescripcion());
                                modalidad.setDescripcionCorta(buscarModalidad.getDescripcionCorta());
                                modalidad.setEstadoRegistro(dtoModalidad.getEstadoRegistro());
                                modalidad.setEstado(dtoModalidad.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                                if (tipoId != null) {
                                    Optional<MaestraDetalle> buscarTipo = maestraDetalleRepository
                                            .findTipoByid(reg.getMaeDetalleId(), dtoModalidad.getCodigoNivel2(), tipoId);
                                    listaTipo = new ArrayList<>();
                                    if (buscarTipo.isPresent()) {
                                        MaestraDetalle tipo = buscarTipo.get();
                                        tipo.setEstadoRegistro(dtoModalidad.getEstadoRegistro());
                                        tipo.setEstado(
                                                dtoModalidad.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                                        Evaluacion eva = new Evaluacion();
                                        eva.setJerarquiaId(dtoModalidad.getJerarquiaId());
                                        Example<Evaluacion> exampleEvaluacion = Example.of(eva);
                                        List<Evaluacion> listaEva = evaluacionRepository.findAll(exampleEvaluacion);
                                        listaEvaluacion = new ArrayList<>();
                                        for (Evaluacion dtoEvaluacion : listaEva) {
                                            Evaluacion evaluacion = null;
                                            Evaluacion buscarEvaluacion = evaluacionRepository.findEvaluacion(
                                                    dtoEvaluacion.getTipoEvaluacionId(), dtoEvaluacion.getJerarquiaId());
                                            MaestraDetalle descripcionEvaluacion = evaluacionRepository.findMaestraDetalle(
                                                    dtoEvaluacion.getTipoEvaluacionId(), dtoEvaluacion.getJerarquiaId());
                                            if (dtoEvaluacion.getJerarquiaId().toString()
                                                    .equals(buscarEvaluacion.getJerarquiaId().toString())) {
                                                evaluacion = new Evaluacion();
                                                evaluacion.setEvaluacionId(dtoEvaluacion.getEvaluacionId());
                                                evaluacion.setJerarquiaId(dtoEvaluacion.getJerarquiaId());
                                                evaluacion.setTipoEvaluacionId(dtoEvaluacion.getTipoEvaluacionId());
                                                evaluacion.setPeso(dtoEvaluacion.getPeso());
                                                evaluacion.setPuntajeMinimo(dtoEvaluacion.getPuntajeMinimo());
                                                evaluacion.setPuntajeMaximo(dtoEvaluacion.getPuntajeMaximo());
                                                evaluacion.setDetalleEvaluacion(descripcionEvaluacion.getDescripcion());
                                                evaluacion.setEstadoRegistro(dtoEvaluacion.getEstadoRegistro());
                                                evaluacion
                                                        .setEstado(dtoEvaluacion.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                                                listaEvaluacion.add(evaluacion);
                                            }
                                            tipo.setListaEvaluacion(listaEvaluacion);
                                        }
                                        listaTipo.add(tipo);
                                    }

                                } else {
                                    MaestraDetalle tip = new MaestraDetalle();
                                    tip.setMaeDetalleId(dtoModalidad.getCodigoNivel3());
                                    Example<MaestraDetalle> exampleTipo = Example.of(tip);
                                    List<MaestraDetalle> listaTip = maestraDetalleRepository.findAll(exampleTipo);
                                    listaTipo = new ArrayList<>();
                                    for (MaestraDetalle dtoTipo : listaTip) {
                                        MaestraDetalle tipo = null;
                                        Optional<MaestraDetalle> buscarTipo = maestraDetalleRepository.findTipoByid(
                                                reg.getMaeDetalleId(), dtoModalidad.getCodigoNivel2(),
                                                dtoModalidad.getCodigoNivel3());
                                        if (dtoTipo.getMaeDetalleId().toString()
                                                .equals(buscarTipo.get().getMaeDetalleId().toString())) {
                                            tipo = new MaestraDetalle();
                                            tipo.setMaeDetalleId(dtoTipo.getMaeDetalleId());
                                            tipo.setMaeCabeceraId(dtoTipo.getMaeCabeceraId());
                                            tipo.setDescripcion(dtoTipo.getDescripcion());
                                            tipo.setDescripcionCorta(dtoTipo.getDescripcionCorta());
                                            tipo.setEstadoRegistro(dtoModalidad.getEstadoRegistro());
                                            tipo.setEstado(
                                                    dtoModalidad.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);

                                            Evaluacion eva = new Evaluacion();
                                            eva.setJerarquiaId(dtoModalidad.getJerarquiaId());
                                            Example<Evaluacion> exampleEvaluacion = Example.of(eva);
                                            List<Evaluacion> listaEva = evaluacionRepository.findAll(exampleEvaluacion);
                                            listaEvaluacion = new ArrayList<>();
                                            for (Evaluacion dtoEvaluacion : listaEva) {
                                                Evaluacion evaluacion = null;
                                                Evaluacion buscarEvaluacion = evaluacionRepository.findEvaluacion(
                                                        dtoEvaluacion.getTipoEvaluacionId(),
                                                        dtoEvaluacion.getJerarquiaId());
                                                MaestraDetalle descripcionEvaluacion = evaluacionRepository
                                                        .findMaestraDetalle(dtoEvaluacion.getTipoEvaluacionId(),
                                                                dtoEvaluacion.getJerarquiaId());
                                                if (dtoEvaluacion.getJerarquiaId().toString()
                                                        .equals(buscarEvaluacion.getJerarquiaId().toString())) {
                                                    evaluacion = new Evaluacion();
                                                    evaluacion.setEvaluacionId(dtoEvaluacion.getEvaluacionId());
                                                    evaluacion.setJerarquiaId(dtoEvaluacion.getJerarquiaId());
                                                    evaluacion.setTipoEvaluacionId(dtoEvaluacion.getTipoEvaluacionId());
                                                    evaluacion.setPeso(dtoEvaluacion.getPeso());
                                                    evaluacion.setPuntajeMinimo(dtoEvaluacion.getPuntajeMinimo());
                                                    evaluacion.setPuntajeMaximo(dtoEvaluacion.getPuntajeMaximo());
                                                    evaluacion.setDetalleEvaluacion(descripcionEvaluacion.getDescripcion());
                                                    evaluacion.setEstadoRegistro(dtoEvaluacion.getEstadoRegistro());
                                                    evaluacion.setEstado(
                                                            dtoEvaluacion.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                                                    listaEvaluacion.add(evaluacion);
                                                }
                                                tipo.setListaEvaluacion(listaEvaluacion);
                                            }
                                            listaTipo.add(tipo);
                                        }
                                        modalidad.setListaTipo(listaTipo);
                                    }
                                }
                                listaMod.add(modalidad);
                            }
                            reg.setListaDetalleModalidad(listaMod);
                        }
                        lista.add(reg);
                    } else {
                        if (modalidadId != null) {
                            List<Long> listaModalidad = jerarquiaRepository.findModalidadById(reg.getMaeDetalleId(),
                                    modalidadId);
                            listaMod = new ArrayList<>();
                            for (Long dtoModalidad : listaModalidad) {
                                MaestraDetalle modalidad = null;
                                MaestraDetalle buscarModalidad = maestraDetalleRepository
                                        .findModalidadByid(reg.getMaeDetalleId(), dtoModalidad);
                                if (dtoModalidad.toString()
                                        .equals(buscarModalidad.getMaeDetalleId().toString())) {
                                    modalidad = new MaestraDetalle();
                                    modalidad.setMaeDetalleId(buscarModalidad.getMaeDetalleId());
                                    modalidad.setMaeCabeceraId(buscarModalidad.getMaeCabeceraId());
                                    modalidad.setDescripcion(buscarModalidad.getDescripcion());
                                    modalidad.setDescripcionCorta(buscarModalidad.getDescripcionCorta());
                                    modalidad.setEstadoRegistro(buscarModalidad.getEstadoRegistro());
                                    modalidad.setEstado(
                                            buscarModalidad.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                                    List<Jerarquia> listaByTipo = jerarquiaRepository.findAllModalidad(reg.getMaeDetalleId(), dtoModalidad);
                                    listaTipo = new ArrayList<>();
                                    for (Jerarquia dtoModByTipo : listaByTipo) {
                                        if (tipoId != null) {
    //										Optional<Long> jerarquiaTipo = jerarquiaRepository.findTipoById(reg.getMaeDetalleId(), dtoModByTipo.getCodigoNivel2(), tipoId);
                                            Optional<MaestraDetalle> buscarTipo = maestraDetalleRepository.findTipoByid(
                                                    reg.getMaeDetalleId(), dtoModByTipo.getCodigoNivel2(), tipoId);
                                            if (buscarTipo.isPresent()) {
                                                if (dtoModByTipo.getCodigoNivel3().toString().equals(tipoId.toString())) {
                                                    MaestraDetalle tipo = buscarTipo.get();
                                                    tipo.setEstadoRegistro(tipo.getEstadoRegistro());
                                                    tipo.setEstado(
                                                            tipo.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO
                                                                    : Constantes.MENSAJE_INACTIVO);
                                                    Evaluacion eva = new Evaluacion();
                                                    eva.setJerarquiaId(dtoModByTipo.getJerarquiaId());
                                                    eva.setEstadoRegistro(dtoModByTipo.getEstadoRegistro());
                                                    Example<Evaluacion> exampleEvaluacion = Example.of(eva);
                                                    List<Evaluacion> listaEva = evaluacionRepository
                                                            .findAll(exampleEvaluacion);
                                                    listaEvaluacion = new ArrayList<>();
                                                    for (Evaluacion dtoEvaluacion : listaEva) {
                                                        Evaluacion evaluacion = null;
                                                        Evaluacion buscarEvaluacion = evaluacionRepository.findEvaluacion(
                                                                dtoEvaluacion.getTipoEvaluacionId(),
                                                                dtoEvaluacion.getJerarquiaId());
                                                        MaestraDetalle descripcionEvaluacion = evaluacionRepository
                                                                .findMaestraDetalle(dtoEvaluacion.getTipoEvaluacionId(),
                                                                        dtoEvaluacion.getJerarquiaId());
                                                        if (dtoEvaluacion.getJerarquiaId().toString()
                                                                .equals(buscarEvaluacion.getJerarquiaId().toString())) {
                                                            evaluacion = new Evaluacion();
                                                            evaluacion.setEvaluacionId(dtoEvaluacion.getEvaluacionId());
                                                            evaluacion.setJerarquiaId(dtoEvaluacion.getJerarquiaId());
                                                            evaluacion.setTipoEvaluacionId(
                                                                    dtoEvaluacion.getTipoEvaluacionId());
                                                            evaluacion.setPeso(dtoEvaluacion.getPeso());
                                                            evaluacion.setPuntajeMinimo(dtoEvaluacion.getPuntajeMinimo());
                                                            evaluacion.setPuntajeMaximo(dtoEvaluacion.getPuntajeMaximo());
                                                            evaluacion.setDetalleEvaluacion(
                                                                    descripcionEvaluacion.getDescripcion());
                                                            evaluacion.setEstadoRegistro(dtoEvaluacion.getEstadoRegistro());
                                                            evaluacion
                                                                    .setEstado(dtoEvaluacion.getEstadoRegistro().equals("1")
                                                                            ? Constantes.MENSAJE_ACTIVO
                                                                            : Constantes.MENSAJE_INACTIVO);
                                                            listaEvaluacion.add(evaluacion);
                                                        }
                                                        tipo.setListaEvaluacion(listaEvaluacion);
                                                    }
                                                    listaTipo.add(tipo);
                                                }
                                            }
                                            modalidad.setListaTipo(listaTipo);

                                        } else {
                                            MaestraDetalle tip = new MaestraDetalle();
                                            tip.setMaeDetalleId(dtoModByTipo.getCodigoNivel3());
                                            Example<MaestraDetalle> exampleTipo = Example.of(tip);
                                            List<MaestraDetalle> listaTip = maestraDetalleRepository.findAll(exampleTipo);
    //										listaTipo = new ArrayList<>();
                                            for (MaestraDetalle dtoTipo : listaTip) {
                                                MaestraDetalle tipo = null;
                                                Optional<MaestraDetalle> buscarTipo = maestraDetalleRepository.findTipoByid(
                                                        reg.getMaeDetalleId(), dtoModalidad,
                                                        dtoModByTipo.getCodigoNivel3());
                                                if (dtoTipo.getMaeDetalleId().toString()
                                                        .equals(buscarTipo.get().getMaeDetalleId().toString())) {
                                                    tipo = new MaestraDetalle();
                                                    tipo.setMaeDetalleId(dtoTipo.getMaeDetalleId());
                                                    tipo.setMaeCabeceraId(dtoTipo.getMaeCabeceraId());
                                                    tipo.setDescripcion(dtoTipo.getDescripcion());
                                                    tipo.setDescripcionCorta(dtoTipo.getDescripcionCorta());
                                                    tipo.setEstadoRegistro(dtoModByTipo.getEstadoRegistro());
                                                    tipo.setEstado(dtoModByTipo.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);

                                                    Evaluacion eva = new Evaluacion();
                                                    eva.setJerarquiaId(dtoModByTipo.getJerarquiaId());
                                                    eva.setEstadoRegistro(dtoModByTipo.getEstadoRegistro());
                                                    Example<Evaluacion> exampleEvaluacion = Example.of(eva);
                                                    List<Evaluacion> listaEva = evaluacionRepository.findAll(exampleEvaluacion);
                                                    listaEvaluacion = new ArrayList<>();
                                                    for (Evaluacion dtoEvaluacion : listaEva) {
                                                        Evaluacion evaluacion = null;
                                                        Evaluacion buscarEvaluacion = evaluacionRepository.findEvaluacion(
                                                                dtoEvaluacion.getTipoEvaluacionId(),
                                                                dtoEvaluacion.getJerarquiaId());
                                                        MaestraDetalle descripcionEvaluacion = evaluacionRepository
                                                                .findMaestraDetalle(dtoEvaluacion.getTipoEvaluacionId(),
                                                                        dtoEvaluacion.getJerarquiaId());
                                                        if (dtoEvaluacion.getJerarquiaId().toString()
                                                                .equals(buscarEvaluacion.getJerarquiaId().toString())) {
                                                            evaluacion = new Evaluacion();
                                                            evaluacion.setEvaluacionId(dtoEvaluacion.getEvaluacionId());
                                                            evaluacion.setJerarquiaId(dtoEvaluacion.getJerarquiaId());
                                                            evaluacion.setTipoEvaluacionId(dtoEvaluacion.getTipoEvaluacionId());
                                                            evaluacion.setPeso(dtoEvaluacion.getPeso());
                                                            evaluacion.setPuntajeMinimo(dtoEvaluacion.getPuntajeMinimo());
                                                            evaluacion.setPuntajeMaximo(dtoEvaluacion.getPuntajeMaximo());
                                                            evaluacion.setDetalleEvaluacion(
                                                                    descripcionEvaluacion.getDescripcion());
                                                            evaluacion.setEstadoRegistro(dtoEvaluacion.getEstadoRegistro());
                                                            evaluacion.setEstado(
                                                                    dtoEvaluacion.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                                                            listaEvaluacion.add(evaluacion);
                                                        }
                                                        tipo.setListaEvaluacion(listaEvaluacion);
                                                    }
                                                    listaTipo.add(tipo);
                                                }
                                                modalidad.setListaTipo(listaTipo);
                                            }
                                        }
                                    }

                                    listaMod.add(modalidad);
                                }
                                reg.setListaDetalleModalidad(listaMod);
                            }
    //						lista.add(reg);

                        } else {
                            List<Long> listaModalidad = jerarquiaRepository.findModalidad(reg.getMaeDetalleId());
                            listaMod = new ArrayList<>();

                            for (Long dtoModalidad : listaModalidad) {
                                MaestraDetalle modalidad = null;
                                List<Jerarquia> listaModalidadByTipo = jerarquiaRepository.findAllModalidad(reg.getMaeDetalleId(), dtoModalidad);
                                MaestraDetalle buscarModalidad = maestraDetalleRepository
                                        .findModalidadByid(reg.getMaeDetalleId(), dtoModalidad);
                                if (dtoModalidad.toString()
                                        .equals(buscarModalidad.getMaeDetalleId().toString())) {
                                    modalidad = new MaestraDetalle();
                                    modalidad.setMaeDetalleId(buscarModalidad.getMaeDetalleId());
                                    modalidad.setMaeCabeceraId(buscarModalidad.getMaeCabeceraId());
                                    modalidad.setDescripcion(buscarModalidad.getDescripcion());
                                    modalidad.setDescripcionCorta(buscarModalidad.getDescripcionCorta());
                                    modalidad.setOrden(buscarModalidad.getOrden());
                                    modalidad.setEstadoRegistro(buscarModalidad.getEstadoRegistro());
                                    modalidad.setEstado(
                                            buscarModalidad.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                                    listaTipo = new ArrayList<>();
                                    for (Jerarquia dtoByTipo : listaModalidadByTipo) {
                                        MaestraDetalle tip = new MaestraDetalle();
                                        tip.setMaeDetalleId(dtoByTipo.getCodigoNivel3());
                                        Example<MaestraDetalle> exampleTipo = Example.of(tip);
                                        List<MaestraDetalle> listaTip = maestraDetalleRepository.findAll(exampleTipo);

                                        for (MaestraDetalle dtoTipo : listaTip) {
                                            MaestraDetalle tipo = null;
                                            Optional<MaestraDetalle> buscarTipo = maestraDetalleRepository.findTipoByid(
                                                    reg.getMaeDetalleId(), dtoModalidad,
                                                    dtoByTipo.getCodigoNivel3());
                                            if (dtoTipo.getMaeDetalleId().toString()
                                                    .equals(buscarTipo.get().getMaeDetalleId().toString())) {
                                                tipo = new MaestraDetalle();
                                                tipo.setMaeDetalleId(dtoTipo.getMaeDetalleId());
                                                tipo.setMaeCabeceraId(dtoTipo.getMaeCabeceraId());
                                                tipo.setDescripcion(dtoTipo.getDescripcion());
                                                tipo.setDescripcionCorta(dtoTipo.getDescripcionCorta());
                                                tipo.setOrden(dtoTipo.getOrden());
                                                tipo.setEstadoRegistro(dtoByTipo.getEstadoRegistro());
                                                tipo.setEstado(
                                                        dtoByTipo.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);

                                                Evaluacion eva = new Evaluacion();
                                                eva.setJerarquiaId(dtoByTipo.getJerarquiaId());
                                                eva.setEstadoRegistro(dtoByTipo.getEstadoRegistro());
                                                Example<Evaluacion> exampleEvaluacion = Example.of(eva);
                                                List<Evaluacion> listaEva = evaluacionRepository.findAll(exampleEvaluacion);
                                                listaEvaluacion = new ArrayList<>();

                                                for (Evaluacion dtoEvaluacion : listaEva) {
                                                    Evaluacion evaluacion = null;
                                                    Evaluacion buscarEvaluacion = evaluacionRepository.findEvaluacion(
                                                            dtoEvaluacion.getTipoEvaluacionId(),
                                                            dtoEvaluacion.getJerarquiaId());
                                                    MaestraDetalle descripcionEvaluacion = evaluacionRepository
                                                            .findMaestraDetalle(dtoEvaluacion.getTipoEvaluacionId(),
                                                                    dtoEvaluacion.getJerarquiaId());
                                                    if (dtoEvaluacion.getJerarquiaId().toString()
                                                            .equals(buscarEvaluacion.getJerarquiaId().toString())) {
                                                        evaluacion = new Evaluacion();
                                                        evaluacion.setEvaluacionId(dtoEvaluacion.getEvaluacionId());
                                                        evaluacion.setJerarquiaId(dtoEvaluacion.getJerarquiaId());
                                                        evaluacion.setTipoEvaluacionId(dtoEvaluacion.getTipoEvaluacionId());
                                                        evaluacion.setPeso(dtoEvaluacion.getPeso());
                                                        evaluacion.setPuntajeMinimo(dtoEvaluacion.getPuntajeMinimo());
                                                        evaluacion.setPuntajeMaximo(dtoEvaluacion.getPuntajeMaximo());
                                                        evaluacion.setDetalleEvaluacion(descripcionEvaluacion.getDescripcion());
                                                        evaluacion.setOrden(dtoEvaluacion.getOrden());
                                                        evaluacion.setEstadoRegistro(dtoEvaluacion.getEstadoRegistro());
                                                        evaluacion.setEstado(
                                                                dtoEvaluacion.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO : Constantes.MENSAJE_INACTIVO);
                                                        listaEvaluacion.add(evaluacion);
                                                    }
                                                    tipo.setListaEvaluacion(listaEvaluacion);
                                                }
                                                listaTipo.add(tipo);
                                            }

                                        }

                                    }
                                    modalidad.setListaTipo(listaTipo);
                                }
                                listaMod.add(modalidad);
                            }
                            reg.setListaDetalleModalidad(listaMod);
                        }
                        lista.add(reg);
                    }
                    resPayload.setListaRegimenLaboral(lista);
                }
            } else {
                lista = new ArrayList<>();
                if (modalidadId != null || tipoId != null) {
                    MaestraDetalle regimen = new MaestraDetalle();
                    lista.add(regimen);
                } else {
                    List<Long> listarJerarquia = jerarquiaRepository.findAllJerarquia();


                    for (Long dto : listarJerarquia) {
                        MaestraDetalle regimen = new MaestraDetalle();
                        regimen.setMaeDetalleId(dto);
                        Example<MaestraDetalle> exampleRegimen = Example.of(regimen);
                        List<MaestraDetalle> listaRegimen = maestraDetalleRepository.findAll(exampleRegimen);
                        for (MaestraDetalle dtoRegimen : listaRegimen) {
                            MaestraDetalle reg = new MaestraDetalle();
                            reg.setMaeDetalleId(dtoRegimen.getMaeDetalleId());
                            reg.setMaeCabeceraId(dtoRegimen.getMaeCabeceraId());
                            reg.setDescripcionCorta(dtoRegimen.getDescripcionCorta());
                            reg.setEstadoRegistro(dtoRegimen.getEstadoRegistro());
                            reg.setEstado(dtoRegimen.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO
                                    : Constantes.MENSAJE_INACTIVO);

                            List<Long> listaModalidad = jerarquiaRepository.findModalidad(dto);
                            listaMod = new ArrayList<>();

                            for (Long dtoModalidad : listaModalidad) {
                                MaestraDetalle modalidad = null;
                                List<Jerarquia> listaModalidadByTipo = jerarquiaRepository.findAllModalidad(dto,
                                        dtoModalidad);
    //						List<String> listaEstado = jerarquiaRepository.findEstado(dto, dtoModalidad);
                                MaestraDetalle buscarModalidad = maestraDetalleRepository.findModalidadByid(dto,
                                        dtoModalidad);
                                if (dtoModalidad.toString().equals(buscarModalidad.getMaeDetalleId().toString())) {
                                    modalidad = new MaestraDetalle();
                                    modalidad.setMaeDetalleId(buscarModalidad.getMaeDetalleId());
                                    modalidad.setMaeCabeceraId(buscarModalidad.getMaeCabeceraId());
                                    modalidad.setDescripcion(buscarModalidad.getDescripcion());
                                    modalidad.setDescripcionCorta(buscarModalidad.getDescripcionCorta());
                                    modalidad.setEstadoRegistro(buscarModalidad.getEstadoRegistro());
                                    modalidad.setEstado(
                                            buscarModalidad.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO
                                                    : Constantes.MENSAJE_INACTIVO);
                                    listaTipo = new ArrayList<>();

                                    for (Jerarquia dtoByTipo : listaModalidadByTipo) {
                                        MaestraDetalle tip = new MaestraDetalle();
                                        tip.setMaeDetalleId(dtoByTipo.getCodigoNivel3());
                                        Example<MaestraDetalle> exampleTipo = Example.of(tip);
                                        List<MaestraDetalle> listaTip = maestraDetalleRepository.findAll(exampleTipo);

                                        for (MaestraDetalle dtoTipo : listaTip) {
                                            MaestraDetalle tipo = null;
                                            Optional<MaestraDetalle> buscarTipo = maestraDetalleRepository.findTipoByid(dto,
                                                    dtoModalidad, dtoByTipo.getCodigoNivel3());
                                            if (dtoTipo.getMaeDetalleId().toString()
                                                    .equals(buscarTipo.get().getMaeDetalleId().toString())) {
                                                tipo = new MaestraDetalle();
                                                tipo.setMaeDetalleId(dtoTipo.getMaeDetalleId());
                                                tipo.setMaeCabeceraId(dtoTipo.getMaeCabeceraId());
                                                tipo.setDescripcion(dtoTipo.getDescripcion());
                                                tipo.setDescripcionCorta(dtoTipo.getDescripcionCorta());
                                                tipo.setEstadoRegistro(dtoTipo.getEstadoRegistro());
                                                tipo.setEstado(
                                                        dtoTipo.getEstadoRegistro().equals("1") ? Constantes.MENSAJE_ACTIVO
                                                                : Constantes.MENSAJE_INACTIVO);

                                                Evaluacion eva = new Evaluacion();
                                                eva.setJerarquiaId(dtoByTipo.getJerarquiaId());
                                                eva.setEstadoRegistro(dtoByTipo.getEstadoRegistro());
                                                Example<Evaluacion> exampleEvaluacion = Example.of(eva);
                                                List<Evaluacion> listaEva = evaluacionRepository.findAll(exampleEvaluacion);
                                                listaEvaluacion = new ArrayList<>();

                                                for (Evaluacion dtoEvaluacion : listaEva) {
                                                    Evaluacion evaluacion = null;
                                                    Evaluacion buscarEvaluacion = evaluacionRepository.findEvaluacion(
                                                            dtoEvaluacion.getTipoEvaluacionId(),
                                                            dtoEvaluacion.getJerarquiaId());
                                                    MaestraDetalle descripcionEvaluacion = evaluacionRepository
                                                            .findMaestraDetalle(dtoEvaluacion.getTipoEvaluacionId(),
                                                                    dtoEvaluacion.getJerarquiaId());
                                                    if (dtoEvaluacion.getJerarquiaId().toString()
                                                            .equals(buscarEvaluacion.getJerarquiaId().toString())) {
                                                        evaluacion = new Evaluacion();
                                                        evaluacion.setEvaluacionId(buscarEvaluacion.getEvaluacionId());
                                                        evaluacion.setJerarquiaId(buscarEvaluacion.getJerarquiaId());
                                                        evaluacion.setTipoEvaluacionId(
                                                                buscarEvaluacion.getTipoEvaluacionId());
                                                        evaluacion.setPeso(buscarEvaluacion.getPeso());
                                                        evaluacion.setPuntajeMinimo(buscarEvaluacion.getPuntajeMinimo());
                                                        evaluacion.setPuntajeMaximo(buscarEvaluacion.getPuntajeMaximo());
                                                        evaluacion.setDetalleEvaluacion(
                                                                descripcionEvaluacion.getDescripcion());
                                                        evaluacion.setOrden(buscarEvaluacion.getOrden());
                                                        evaluacion.setEstadoRegistro(buscarEvaluacion.getEstadoRegistro());
                                                        evaluacion
                                                                .setEstado(buscarEvaluacion.getEstadoRegistro().equals("1")
                                                                        ? Constantes.MENSAJE_ACTIVO
                                                                        : Constantes.MENSAJE_INACTIVO);
                                                        listaEvaluacion.add(evaluacion);
                                                    }
                                                    tipo.setListaEvaluacion(listaEvaluacion);
                                                }
                                                listaTipo.add(tipo);
                                            }

                                        }
                                        modalidad.setListaTipo(listaTipo);
                                    }
                                    listaMod.add(modalidad);
                                }
                                reg.setListaDetalleModalidad(listaMod);
                            }

                            lista.add(reg);
                        }
                    }
                    resPayload.setListaRegimenLaboral(lista);
                }
            }
            return new RespBase<RespObtenerEvaluacion>().ok(resPayload);
        }
    */
    @Override
    @Transactional(transactionManager = "convocatoriaTransactionManager")
    public RespBase<Object> eliminarJerarquia(MyJsonWebToken token, Long codigoNivel1, Long codigoNivel2,
                                              Long codigoNivel3, String estado) {
        RespBase<Object> response = new RespBase<>();

        List<Jerarquia> lstJerarquia = new ArrayList<>();

        Jerarquia jerarquiaFilter = new Jerarquia();
        if ((codigoNivel2 == null || codigoNivel2 == 0) && (codigoNivel3 == null || codigoNivel3 == 0)) {
            jerarquiaFilter.setCodigoNivel1(codigoNivel1);
        } else if (codigoNivel3 == null || codigoNivel3 == 0) {
            jerarquiaFilter.setCodigoNivel1(codigoNivel1);
            jerarquiaFilter.setCodigoNivel2(codigoNivel2);
        } else {
            jerarquiaFilter.setCodigoNivel1(codigoNivel1);
            jerarquiaFilter.setCodigoNivel2(codigoNivel2);
            jerarquiaFilter.setCodigoNivel3(codigoNivel3);
        }

        jerarquiaFilter.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
        Example<Jerarquia> example = Example.of(jerarquiaFilter);
        List<Jerarquia> lstJerarquiaFilter = jerarquiaRepository.findAll(example);
        if (lstJerarquiaFilter.isEmpty()) {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe la jerarquia");
            return response;
        } else {
            for (Jerarquia jerarquia : lstJerarquiaFilter) {
                List<Evaluacion> lstEvaluacion = new ArrayList<>();
                jerarquia.setCampoSegUpd(estado, token.getUsuario().getUsuario(), Instant.now());
                jerarquiaRepository.save(jerarquia);

                Evaluacion evaluacionFilter = new Evaluacion();
                evaluacionFilter.setJerarquiaId(jerarquia.getJerarquiaId());

                Example<Evaluacion> exampleEvaluacion = Example.of(evaluacionFilter);
                List<Evaluacion> lstEvaluacionFilter = evaluacionRepository.findAll(exampleEvaluacion);

                if (lstEvaluacionFilter.isEmpty()) {
                    return response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No Existe Evaluaciones para esta jerarquia");
                } else {

                    for (Evaluacion evaluacion : lstEvaluacionFilter) {
                        evaluacion.setCampoSegUpd(estado, token.getUsuario().getUsuario(), Instant.now());
                        evaluacionRepository.save(evaluacion);
                        lstEvaluacion.add(evaluacion);
                    }
                }
                jerarquia.setListaEvaluacion(lstEvaluacion);
                lstJerarquia.add(jerarquia);

            }
            RespCreaEvaluacion respPayload = new RespCreaEvaluacion();
            respPayload.setListaJerarquia(lstJerarquia);
            return new RespBase<Object>().ok(respPayload);
        }

    }

    @Override
    public RespBase<RespObtieneLista<RegimenDTO>> findAllEvaluacion(Long regimenLaboral, Long modalidad, Long tipoAcceso, Long entidadId) {

        List<RegimenDTO> regimenDTOS = new ArrayList<>();
        RespObtieneLista<RegimenDTO> respPayload = new RespObtieneLista<>();

        if (!Util.isEmpty(entidadId)) {
            regimenDTOS = evaluacionEntidad(entidadId, regimenLaboral, modalidad, tipoAcceso) ;
            return new RespBase<RespObtieneLista<RegimenDTO>>().ok(settValores(regimenDTOS));
        }

        if (Util.isEmpty(entidadId)) {
            regimenDTOS = evaluacionServir(entidadId, regimenLaboral, modalidad, tipoAcceso);
            return new RespBase<RespObtieneLista<RegimenDTO>>().ok(settValores(regimenDTOS));
        }
        return new RespBase<RespObtieneLista<RegimenDTO>>().ok(respPayload);
    }

    /**
     * sett los valores de rpta respPayload list con la data y size
     *
     * @return
     */
    private RespObtieneLista<RegimenDTO> settValores(List<RegimenDTO> regimenDTOS) {
        RespObtieneLista<RegimenDTO> respPayload = new RespObtieneLista<>();
        respPayload.setCount(regimenDTOS.size());
        respPayload.setItems(regimenDTOS);
        return respPayload;
    }

    /**
     * list de evaluacion entidad
     *
     * @param entidad
     * @param codNivel1
     * @param codNivel2
     * @param codNivel3
     * @return
     */
    private List<RegimenDTO> evaluacionEntidad(Long entidad, Long codNivel1, Long codNivel2, Long codNivel3 )   {

        List<RegimenDTO> lisPrevia = new ArrayList<>();
        List<MasterEvaluacionDTO> listMaster = evaluacionRepositoryJdbc.listMasterEvaluacionDtos2(entidad, codNivel1, codNivel2, codNivel3);

        for (MasterEvaluacionDTO it : listMaster) {
            RegimenDTO re = new RegimenDTO();
            re.setCodigoNivel1(it.getCodigoNivel1());
            re.setDescripcionCorta(it.getDescripcionCorta());
            lisPrevia.add(re);
        }

        for (RegimenDTO re : lisPrevia) {
            List<ModalidadDTO> listMaster3 = evaluacionRepositoryJdbc.listMasterEvaluacionDtos3(entidad, re.getCodigoNivel1(), codNivel2, codNivel3);
            re.setListaDetalleModalidad(listMaster3);
        }

        for (RegimenDTO re : lisPrevia) {
            for (ModalidadDTO mo : re.getListaDetalleModalidad()) {
                List<TipoDTO> ls = evaluacionRepositoryJdbc.listMasterEvaluacionDtos4(entidad, mo.getCodNivel1(), mo.getCodNivel2(), codNivel3);
                mo.setListaTipo(ls);
            }
        }

        for (RegimenDTO re : lisPrevia) {
            for (ModalidadDTO mo : re.getListaDetalleModalidad()) {
                for (TipoDTO ti : mo.getListaTipo()) {
                    List<EvaluacionDTO> ls = evaluacionRepositoryJdbc.listallTiEvaluacionEntidadDtos(ti.getJerarquia(), Constantes.ACTIVO, entidad);
                    ti.setListaEvaluacion(ls);
                }
            }

        }

        return lisPrevia;
    }

    /**
     * list evaluacion servir
     *
     * @param entidad //deprecated
     * @param codNivel1
     * @param codNivel2
     * @param codNivel3
     * @return
     */
    private List<RegimenDTO> evaluacionServir(Long entidad, Long codNivel1, Long codNivel2, Long codNivel3) {
        List<RegimenDTO> lisPrevia = new ArrayList<>();
        List<MasterEvaluacionDTO> listMaster = evaluacionRepositoryJdbc.listMasterEvaluacionDtos2Servir(entidad, codNivel1, codNivel2, codNivel3);
        for (MasterEvaluacionDTO it : listMaster) {
            RegimenDTO re = new RegimenDTO();
            re.setCodigoNivel1(it.getCodigoNivel1());
            re.setDescripcionCorta(it.getDescripcionCorta());
            lisPrevia.add(re);
        }
        for (RegimenDTO re : lisPrevia) {
            List<ModalidadDTO> listMaster3 = evaluacionRepositoryJdbc.listMasterEvaluacionDtos3Servir(entidad, re.getCodigoNivel1(), codNivel2, codNivel3);
            re.setListaDetalleModalidad(listMaster3);
        }

        for (RegimenDTO re : lisPrevia) {
            for (ModalidadDTO mo : re.getListaDetalleModalidad()) {
                List<TipoDTO> ls = evaluacionRepositoryJdbc.listMasterEvaluacionDtos4Servir(entidad, mo.getCodNivel1(), mo.getCodNivel2(), codNivel3);
                mo.setListaTipo(ls);
            }
        }

        for (RegimenDTO re : lisPrevia) {
            for (ModalidadDTO mo : re.getListaDetalleModalidad()) {
                for (TipoDTO ti : mo.getListaTipo()) {
                    List<EvaluacionDTO> ls = evaluacionRepositoryJdbc.listallTiEvaluacionDtosServir(ti.getJerarquia(), Constantes.ACTIVO);
                    ti.setListaEvaluacion(ls);
                }
            }

        }

        return lisPrevia;
    }

	@Override
	//@Transactional(transactionManager = "convocatoriaTransactionManager")
	public RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> findEvaluacionEntidadDto(ReqParamEvaluacion reqParam ) {
		
		RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> request = new RespBase<>();
		RespObtieneLista<DetalleEvaluacionEntidadDTO> respPayload = new RespObtieneLista<>();
		List<DetalleEvaluacionEntidadDTO> listDetalle = new ArrayList<>();
		
		try {
			//BaseDTO base = baseRepositoryJdbc.getBase(reqParam.getRegimenId(), reqParam.getModalidadId(), reqParam.getTipoId(), reqParam.getEntidadId());
	        //if(Objects.nonNull(base)) {
	        	//List<Jerarquia> jerarquein = jerarquiaRepository.getJerarquia(base.getRegimenId(), base.getModalidadId(), base.getTipoId());
	        	List<Jerarquia> jerarquein = jerarquiaRepository.getJerarquia(reqParam.getRegimenId(), reqParam.getModalidadId(), reqParam.getTipoId());
	        	 if(Objects.nonNull(jerarquein)) {
	        		 listDetalle =  evaluacionEntidadRepositoryJdbc.buscarEEntidad(reqParam.getEntidadId(), jerarquein.get(0).getJerarquiaId());
	        		 if(Objects.nonNull(listDetalle) && !listDetalle.isEmpty()) {
	        			 
	        			 int pMinimo=0;
	        			 int pMax =0;
	        			 for (DetalleEvaluacionEntidadDTO detalleEDto : listDetalle) {
	        				 pMinimo += (detalleEDto.getPuntajeMin() * detalleEDto.getPeso()) / 100;
	        				 pMax += (detalleEDto.getPuntajeMax() * detalleEDto.getPeso()) / 100;
	        			 }
	        			 
	        			 DetalleEvaluacionEntidadDTO dto = new DetalleEvaluacionEntidadDTO();
	        			 dto.setDescripcion(Constantes.MSJ_PUNTAJE_TOTAL);
	        			 dto.setPeso(listDetalle.stream().collect(Collectors.summingInt(DetalleEvaluacionEntidadDTO::getPeso)));
	        			 dto.setPuntajeMin(pMinimo);
	        			 dto.setPuntajeMax(pMax);
	        			 listDetalle.add(dto);
	        			 
	        			 respPayload.setCount(listDetalle.size());
	        		     respPayload.setItems(listDetalle);
	        		 }else {
	        			 request = ParametrosUtil.setearResponse(request, Boolean.FALSE, "No se encontró registros");
	        			 return request;
	        		 }
	        	 }else {
	        		 request = ParametrosUtil.setearResponse(request, Boolean.FALSE, "No se encontró registros en Jerarquia");
        			 return request;
	        	 }
		} catch (Exception e) {
			 request = ParametrosUtil.setearResponse(request, Boolean.FALSE, "Ocurrio algún error "+ e.getMessage());
			 return request;
		}
		
		return new RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>>().ok(respPayload);
	}

	@Override
	@Transactional(transactionManager = "convocatoriaTransactionManager")
	public RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> findEvaluacionEntidadById(Long idBase,
			Long entidadId) {
		RespBase<RespObtieneLista<DetalleEvaluacionEntidadDTO>> request = new RespBase<>();
		
		BaseDTO b = baseRepositoryJdbc.getBase(idBase, entidadId);
		if(Objects.nonNull(b)) {
			ReqParamEvaluacion req = new ReqParamEvaluacion();
			req.setEntidadId(b.getEntidadId());
			req.setModalidadId(b.getModalidadId());
			req.setTipoId(b.getTipoId());
			req.setRegimenId(b.getRegimenId());
			
			request = findEvaluacionEntidadDto(req);
		}else {
			request = ParametrosUtil.setearResponse(request, Boolean.FALSE, "No se encontró registros en Base");
   			return request;
		}
		
		return request;
	}

    @Override
    public RespBase<RespObtenerPuntajesEvaluacionDTO> obtenerPuntajeEvaluacion(Long perfilId, Long regimenId, Long entidadId) {

        RespBase<RespObtenerPuntajesEvaluacionDTO> response = new RespBase<>();
        RespObtenerPuntajesEvaluacionDTO dto = null;
        BasePerfil filtro = new BasePerfil();
        filtro.setPerfilId(perfilId);
        filtro.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
        Example<BasePerfil> example = Example.of(filtro);

        List<BasePerfil> listaTblBasePerfil = this.basePerfilRepository.findAll(example);

        if (!listaTblBasePerfil.isEmpty()) {
            List<BasePerfilBean> listaBasePerfilBean = new ArrayList<>();
            List<BasePerfilBean> listaFilterBasePerfilBean = new ArrayList<>();
            for (BasePerfil basePerfil : listaTblBasePerfil) {
                Optional<Base> oBase = this.baseRepository.findById(basePerfil.getBase().getBaseId());
                if (oBase.isPresent()) {
                    BasePerfilBean basePerfilBean = this.setearBasePerfilBean(basePerfil, oBase.get());
                    listaBasePerfilBean.add(basePerfilBean);
                }
            }

            for (BasePerfilBean bpBean : listaBasePerfilBean) {
                if (bpBean.getPerfilId().equals(perfilId) &&
                    bpBean.getRegimenId().equals(regimenId) &&
                    bpBean.getEntidadId().equals(entidadId)) {
                    listaFilterBasePerfilBean.add(bpBean);
                }
            }

            if (!listaFilterBasePerfilBean.isEmpty()) {
                RespBase<RespObtieneLista<RegimenDTO>> evaluacionesResponse = this.evaluacionService.findAllEvaluacion(
                        listaFilterBasePerfilBean.get(0).getRegimenId(),
                        listaFilterBasePerfilBean.get(0).getModalidadId(),
                        listaFilterBasePerfilBean.get(0).getTipoId(),
                        listaFilterBasePerfilBean.get(0).getEntidadId()
                );
                if (evaluacionesResponse.getStatus().getSuccess() &&
                        evaluacionesResponse.getPayload().getItems().size() > 0) {
                    dto = this.setearPuntajesEvalByPerfilIdDTO(evaluacionesResponse.getPayload().getItems().get(0));
                } else {
                    response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No se encontro data en la tablas tbl_mae_detalle_id y tbl_evaluacion_entidad.");
                    return response;
                }
            } else {
                response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "No se encontro puntaje con los ids ingresados.");
                return response;
            }

        } else {
            response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "Id perfil no existe.");
            return response;
        }

        return new RespBase<RespObtenerPuntajesEvaluacionDTO>().ok(dto);
    }

    private RespObtenerPuntajesEvaluacionDTO setearPuntajesEvalByPerfilIdDTO (RegimenDTO regimenDto) {
        RespObtenerPuntajesEvaluacionDTO dto = new RespObtenerPuntajesEvaluacionDTO();
        if (Objects.nonNull(regimenDto.getListaDetalleModalidad()) &&
            Objects.nonNull(regimenDto.getListaDetalleModalidad().get(0).getListaTipo())) {

            for (EvaluacionDTO eval: regimenDto.getListaDetalleModalidad().get(0).getListaTipo().get(0).getListaEvaluacion()) {
                if (eval.getDetalleEvaluacion().equals("CURRICULAR")) {
                    dto.setEvaluacionId(eval.getEvaluacionId());
                    dto.setJerarquiaId(eval.getJerarquiaId());
                    dto.setTipoEvaluacionId(eval.getTipoEvaluacionId());
                    dto.setOrden(eval.getOrden());
                    dto.setPeso(eval.getPeso());
                    dto.setPuntajeMinimo(eval.getPuntajeMinimo());
                    dto.setPuntajeMaximo(eval.getPuntajeMaximo());
                    dto.setDetalleEvaluacion(eval.getDetalleEvaluacion());
                    dto.setEstado(eval.getEstado());
                    dto.setEntidadId(eval.getEntidadId());
                    dto.setEvaluacionEntidad(eval.getEvaluacionEntidad());
                    dto.setEvaluacionOrigenId(eval.getEvaluacionOrigenId());
                    break;
                }
            }
        }
        return dto;
    }

    private BasePerfilBean setearBasePerfilBean (BasePerfil basePerfil, Base base) {
        BasePerfilBean bpBean = new BasePerfilBean();
        bpBean.setBasePerfilId(basePerfil.getBasePerfilId());
        bpBean.setPerfilId(basePerfil.getPerfilId());
        bpBean.setBaseId(basePerfil.getBase().getBaseId());
        bpBean.setRegimenId(base.getRegimenId());
        bpBean.setModalidadId(base.getModalidadId());
        bpBean.setTipoId(base.getTipoId());
        bpBean.setEntidadId(base.getEntidadId());
        return bpBean;
    }
    
    @Override
	public RespBase<RespObtieneLista<EvaluacionConvocatoriaDTO>> buscarEvaluacionesConvocatoriaEntidad(Long idBase,
			Long entidadId) {
		RespBase<RespObtieneLista<EvaluacionConvocatoriaDTO>> request = new RespBase<>();
		
		BaseDTO b = baseRepositoryJdbc.getBase(idBase, entidadId);
		if(Objects.nonNull(b)) {
			ReqParamEvaluacion req = new ReqParamEvaluacion();
			req.setEntidadId(b.getEntidadId());
			req.setModalidadId(b.getModalidadId());
			req.setTipoId(b.getTipoId());
			req.setRegimenId(b.getRegimenId());
			
			request = buscarEvaluacionesBaseEntidad(req);
		}else {
			request = ParametrosUtil.setearResponse(request, Boolean.FALSE, "No se encontró registros en Base");
   			return request;
		}
		
		return request;
	}
    
	public RespBase<RespObtieneLista<EvaluacionConvocatoriaDTO>> buscarEvaluacionesBaseEntidad(ReqParamEvaluacion reqParam ) {
		
		RespBase<RespObtieneLista<EvaluacionConvocatoriaDTO>> request = new RespBase<>();
		RespObtieneLista<EvaluacionConvocatoriaDTO> respPayload = new RespObtieneLista<>();
		List<DetalleEvaluacionEntidadDTO> listDetalle = new ArrayList<>();
		List<EvaluacionConvocatoriaDTO> listaFinal = new ArrayList<>();
		try {
			List<Jerarquia> jerarquein = jerarquiaRepository.getJerarquia(reqParam.getRegimenId(), reqParam.getModalidadId(), reqParam.getTipoId());
	        if(Objects.nonNull(jerarquein)) {
	        	 listDetalle =  evaluacionEntidadRepositoryJdbc.buscarEEntidad(reqParam.getEntidadId(), jerarquein.get(0).getJerarquiaId());
	        	 for (DetalleEvaluacionEntidadDTO item : listDetalle) {
	        		 Optional<EvaluacionEntidad> evaluacion = evaluacionEntidadRepository.findById(item.getEvaluacionEntidadId()); 
	        		 Long id = evaluacion.isPresent()? evaluacion.get().getOrden() : evaluacion.get().getEvaluacionEntidadId(); 
	        		 listaFinal.add(new EvaluacionConvocatoriaDTO(item,id));
	        	 }
	        	 respPayload.setCount(listaFinal.size());
        		 respPayload.setItems(listaFinal);        		     
	        }else {
	        	 request = ParametrosUtil.setearResponse(request, Boolean.FALSE, "No se encontró registros.");
        		 return request;
	        }
		} catch (Exception e) {
			 request = ParametrosUtil.setearResponse(request, Boolean.FALSE, "Ocurrio algún error "+ e.getMessage());
			 return request;
		}		
		return new RespBase<RespObtieneLista<EvaluacionConvocatoriaDTO>>().ok(respPayload);
	}
    
}