package pe.gob.servir.convocatoria.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqComunicado;
import pe.gob.servir.convocatoria.request.ReqEstadoComunicado;
import pe.gob.servir.convocatoria.response.ConvocatoriaDTO;
import pe.gob.servir.convocatoria.response.ConvocatoriaInfoDTO;
import pe.gob.servir.convocatoria.response.DatosPuestoDTO;
import pe.gob.servir.convocatoria.response.DatosReporteDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespComunicado;
import pe.gob.servir.convocatoria.response.RespComunicadoDTO;
import pe.gob.servir.convocatoria.response.RespEtapasEvaluacion;
import pe.gob.servir.convocatoria.response.RespObtenerConvEtapa;
import pe.gob.servir.convocatoria.response.RespObtenerConvocatorias;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespToContratoConvenio;
import pe.gob.servir.convocatoria.response.RutaComunicadoDTO;
import pe.gob.servir.convocatoria.response.dto.RespPerfilesConvocatoriaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.ComunicadoService;
import pe.gob.servir.convocatoria.service.ConvocatoriaService;


@RestController
@Tag(name = "Convocatoria", description = "")
public class ConvocatoriaController {

    @Autowired
    ConvocatoriaService convocatoriaService;

    @Autowired
    ComunicadoService comunicadoService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Operation(summary = Constantes.SUM_OBT_LIST + "Convocatorias", description = Constantes.SUM_OBT_LIST + "Convocatorias por Entidad", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/convocatorias/{entidad}"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespObtieneLista<ConvocatoriaDTO>>> findConvocatorias(
            @PathVariable String access,
            @PathVariable Long entidad,
            @RequestParam(required = false) Long regimen,
            @RequestParam(required = false) Long etapa,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaIni,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaFin,
            @RequestParam(required = false) Long estado,
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "10") int size) {
        RespBase<RespObtieneLista<ConvocatoriaDTO>> response = convocatoriaService.findConvocatorias(entidad, regimen, etapa, fechaIni, fechaFin, estado, page, size);
        return ResponseEntity.ok(response);
    }

    @Operation(summary = "Crea un comunicado", description = "Por entidad / convocatoria / perfil ", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PostMapping(path = {Constantes.BASE_ENDPOINT + "/comunicados"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespComunicado>> creaComunicado(@PathVariable String access, @Valid @RequestBody ReqBase<ReqComunicado> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<RespComunicado> resp = comunicadoService.creaComunicado(request, jwt);
        return ResponseEntity.ok(resp);

    }
    
    
    @Operation(summary = Constantes.SUM_OBT_LIST + "Comunicados", description = Constantes.SUM_OBT_LIST + "Comunicados por Convocatoria", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/comunicados/{entidad}"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespObtieneLista<RespComunicadoDTO>>> findComunicados(
            @PathVariable String access,
            @PathVariable Long entidad,
            @RequestParam(required = false) Long convocatoria,
            @RequestParam(required = false) String perfil,
            @RequestParam(required = false) Long etapa,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaIni,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaFin,
            @RequestParam(required = false) Long tipoComunicado,
            @RequestParam(required = false) Long estado,
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "10") int size) {
    	
    	Map<String, Object> parametroMap = new HashMap<>();
    	parametroMap.put("entidad", entidad);
    	parametroMap.put("convocatoria",convocatoria);
    	parametroMap.put("perfil",perfil);
    	parametroMap.put("etapa",etapa);
    	parametroMap.put("fechaIni",fechaIni);
    	parametroMap.put("fechaFin",fechaFin);
    	parametroMap.put("tipoComunicado",tipoComunicado);
    	parametroMap.put("estado",estado);
    	parametroMap.put("page",page);
    	parametroMap.put("size",size);
        RespBase<RespObtieneLista<RespComunicadoDTO>> response = comunicadoService.findComunicado(parametroMap);
        return ResponseEntity.ok(response);
    }
    
    
	@Operation(summary = "Inactiva un comunicado", description = "Inactiva una comunicado", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@DeleteMapping(path = { Constantes.BASE_ENDPOINT + "/comunicado/{comunicadoId}" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> eliminarComunicado(@PathVariable String access,
			@PathVariable Long comunicadoId){
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> response = comunicadoService.eliminarComunicado(jwt, comunicadoId);
		return ResponseEntity.ok(response);
	}
	
    @Operation(summary = "Actualizar un comunicado", description = "Por entidad / convocatoria / perfil ", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/comunicado/{comunicadoId}"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespComunicado>> actualizarComunicado(@PathVariable String access, 
    		@PathVariable Long comunicadoId, @Valid @RequestBody ReqBase<ReqComunicado> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<RespComunicado> resp = comunicadoService.actualizaComunicado(request, jwt, comunicadoId);
        return ResponseEntity.ok(resp);

    }
    
	@Operation(summary = "get comunicado ", description = "get comunicado por Identificador ", tags = {""},
			security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
	@GetMapping(path = {Constantes.BASE_ENDPOINT + "/comunicado/{comunicadoId}"},
			produces = {MediaType.APPLICATION_JSON_VALUE})

	public ResponseEntity<RespBase<RespComunicado>> getComunicado(
				@PathVariable String access, 
				@PathVariable Long comunicadoId) {
		RespBase<RespComunicado> response = comunicadoService.obtenerComunicado(comunicadoId);
		
		 return ResponseEntity.ok(response);
		 }


    @Operation(summary = "Actualizar estado comunicado", description = "Actualizar estado comunicado", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @PutMapping(path = {Constantes.BASE_ENDPOINT + "/comunicado/estado/{comunicadoId}"},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespComunicado>> actualizarEstadoComunicado(@PathVariable String access,
    		@PathVariable Long comunicadoId, @Valid @RequestBody ReqBase<ReqEstadoComunicado> request) {
        MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
        RespBase<RespComunicado> resp = comunicadoService.actualizaEstadoComunicado(request, jwt, comunicadoId);
        return ResponseEntity.ok(resp);

    }
    
    @Operation(summary = Constantes.SUM_OBT_LIST + "Convocatorias por procesar", description = Constantes.SUM_OBT_LIST + "Convocatorias por procesar", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/convocatorias/job"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespObtenerConvocatorias>> findConvocatoriaProcesar(
            @PathVariable String access,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaHoy) {
        RespBase<RespObtenerConvocatorias> response = convocatoriaService.listaConvocatoria(fechaHoy);
        return ResponseEntity.ok(response);
    }


    @Operation(summary = Constantes.SUM_OBT_LIST + "datos para contratos y convenios", description = Constantes.SUM_OBT_LIST + "por id postulante , base , perfil ", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/datos"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespToContratoConvenio>> findDatosToContratosToConvenios(
            @PathVariable String access,
            @RequestParam(required = false) Long base,
            @RequestParam(required = false) Long postulante,
            @RequestParam(required = false) Long perfil,
            @RequestParam(required = false) Long entidad) {
        RespBase<RespToContratoConvenio>response =  convocatoriaService.findDatosToContratosToConvenios(base , postulante , perfil , entidad);
        return ResponseEntity.ok(response);
    }
    
    @Operation(summary = "Obtiene informacion de la convocatoria",
            description = "Obtiener informacion de la convocatoria", tags = {""}, security = {
            @SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/convocatoria/info/{baseId}"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<ConvocatoriaInfoDTO>> getInfoConvocatoria
            (@PathVariable String access, @PathVariable Long baseId) {
    	RespBase<ConvocatoriaInfoDTO> response = convocatoriaService.obtenerConvocatoriaById(baseId);
        return ResponseEntity.ok(response);
    }


    @Operation(summary = "Obtiene ruta del pdf comunicados",
            description = "Obtienen informacion de comunicados por base y/o perfil", tags = {""}, security = {
            @SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/comunicado/{baseId}/{perfilId}"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespObtieneLista<RutaComunicadoDTO>>> getRutaComunicados
            (@PathVariable String access, @PathVariable Long baseId , @PathVariable Long perfilId)  {
        RespBase<RespObtieneLista<RutaComunicadoDTO>> response = comunicadoService.getRutaComunicado(perfilId , baseId);
        return ResponseEntity.ok(response);
    }
    
    @Operation(summary = "Listar Perfiles para detalle convocatoria ",
			description = "Listar detalle para detalle convocatoria ", tags = {
			"" }, security = { @SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT
			+ "/convocatoria/listarPerfiles/{convocatoriaId}" }, produces = {
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespObtieneLista<RespPerfilesConvocatoriaDTO>>> listarPerfilesDetallePorConvocatoria(
			@PathVariable String access, @PathVariable Long convocatoriaId) {
		RespBase<RespObtieneLista<RespPerfilesConvocatoriaDTO>> response = convocatoriaService
				.listarPerfilesDetallePorConvocatoria(convocatoriaId);
		return ResponseEntity.ok(response);
	}
    
	@Operation(summary = "Listar etapas evaluacion - cronograma ", 
			description = "Listar etapas evaluacion - cronograma ", tags = {"" }, security = { @SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT
			+ "/convocatoria/listarEtapasEvaluaciones/{baseId}" }, produces = {
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<RespEtapasEvaluacion>> listarEtapaEvaluaciones(@PathVariable String access,
			@PathVariable Long baseId){
		RespBase<RespEtapasEvaluacion> response = convocatoriaService.listarEtapasEvaluaciones(baseId);
		return ResponseEntity.ok(response);
	}
	
	
	@Operation(summary = "Actualizar etapas convocatoria", description = "Actualizar etapas convocatoria", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/convocatoria/job/etapa" }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> actualizarEtapaConvocatoria(@PathVariable String access,
            @RequestParam(required = false) @JsonFormat(pattern = "yyyy-MM-dd") String fechaHoy){
		RespBase<Object> response = convocatoriaService.actualizarConvocatoriaEtapa(fechaHoy);
		return ResponseEntity.ok(response);
	}
	
    @Operation(summary = Constantes.SUM_OBT_LIST + "Convocatorias cambio etapa", description = Constantes.SUM_OBT_LIST + "Convocatorias cambio etapa", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/convocatoria/job/etapaCorreo"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespObtenerConvEtapa>> findConvoCambioEtapa(
            @PathVariable String access,
            @RequestParam(required = true) @JsonFormat(pattern = "yyyy-MM-dd") String fechaHoy) {
        RespBase<RespObtenerConvEtapa> response = convocatoriaService.listarConvocatoriaCorreoEtapa(fechaHoy);
        return ResponseEntity.ok(response);
    }
    
    @Operation(summary = Constantes.SUM_OBT_LIST + "Convocatorias cambio etapa proximo", description = Constantes.SUM_OBT_LIST + "Convocatorias cambio etapa proximo", tags = {""},
            security = {@SecurityRequirement(name = Constantes.BEARER_JWT)})
    @ApiResponses(value = {
            @ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
            @ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))}),
            @ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class))})})
    @GetMapping(path = {Constantes.BASE_ENDPOINT + "/convocatoria/job/proxEtapaCorreo"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RespBase<RespObtenerConvEtapa>> findConvoCambioEtapaProximo(
            @PathVariable String access,
            @RequestParam(required = true) @JsonFormat(pattern = "yyyy-MM-dd") String fechaHoy) {
        RespBase<RespObtenerConvEtapa> response = convocatoriaService.listarConvocatoriaCorreoEtapaProx(fechaHoy);
        return ResponseEntity.ok(response);
    }
    
	@GetMapping(path = { Constantes.BASE_ENDPOINT
			+ "/convocatoria/datosPuesto/{baseId}" }, produces = {
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<DatosPuestoDTO>> buscarDatosPuesto(@PathVariable String access,
			@PathVariable Long baseId){
		RespBase<DatosPuestoDTO> response = convocatoriaService.buscarDatosPuesto(baseId);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "obtener Informacion para el reporte de evaluacion de postulante", 
			description = "obtener Informacion para el reporte de evaluacion de postulante", tags = {"" }, security = { @SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT
			+ "/convocatoria/datosReporte/{baseId}" }, produces = {
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<DatosReporteDTO>> buscarInfoReportePostulante(@PathVariable String access,
			@PathVariable Long baseId){
		RespBase<DatosReporteDTO> response = convocatoriaService.buscarInfoReportePostulante(baseId);
		return ResponseEntity.ok(response);
	}
}

