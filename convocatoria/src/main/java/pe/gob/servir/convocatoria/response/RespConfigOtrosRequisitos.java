package pe.gob.servir.convocatoria.response;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespConfigOtrosRequisitos {
	    private Long confPerfOtrosRequisitosId;
	    private Long perfilId;
	    private String  descripcion;
	    private boolean  requisitosAdicionales;
	    private Long  experienciaDetalleId;
	    private Long  tipoDatosExperId;
	    private Long  requisitoId;
	    private Long  perfilExperienciaId;
}
