package pe.gob.servir.convocatoria.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.repository.InformeDetalleRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.BonificacionDTO;
import pe.gob.servir.convocatoria.request.dto.BonificacionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.InformeDetalleDTO;
import pe.gob.servir.convocatoria.response.RespCriterioEvaluacion;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class InformeDetalleRepositoryJdbcImpl implements InformeDetalleRepositoryJdbc {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InformeDetalleRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_FIND_INFORME_DETALLE = "select tmd.cod_prog as codProg ,  tid.informe_detalle_id as informeDetalleId , tmd.descripcion as nombretipoInfo , tid.tipo_info_id as tipoInfo , case when tid.ESTADO_REGISTRO = '1' then 'ACTIVO' else 'INACTIVO' end as  estado," +
            " upper(tid.titulo) as  titulo, tid.contenido as  contenido , tid.entidad_id  as entidadId from sch_base.tbl_informe_detalle tid join sch_convocatoria.tbl_mae_detalle tmd on tid.tipo_info_id = tmd.mae_detalle_id \n" ;

    private static final String SQL_FIND_BONIFICACION = "select tb.nivel_id as nivelId , aplica_id as aplicaId , tb.bonificacion_id as bonificacionId , tb.tipo_bonificacion as tipoBonificacion , tmd.descripcion as nombretipoBonificacion , " +
            "tb.titulo as titulo , tb.contenido as contenido , tb.estado_registro as estado, \n" +
            "tb.nombre_bonificacion as bonificacionNombre , tb.nivel as nivel , tb.aplica as aplica , tb.porcentaje_bono as porcentaje , tb.informe_detalle_id as informeDetalleId\n" +
            "from sch_convocatoria.tbl_bonificacion tb join sch_convocatoria.tbl_mae_detalle tmd on tb.tipo_bonificacion = tmd.mae_detalle_id \n" +
            "where tb.informe_detalle_id = :idInformeDetalle and tb.estado_registro = '1' order by tb.bonificacion_id asc ";

    private static final  String SQL_FIND_BONIFICACION_DETALLE = "select pe.bonificacion_detalle_id as bonificacionDetalleId , pe.bonificacion_id as bonificacionId  , pe.descripcion as descripcion , \n" +
            "pe.nivel_id as nivelId, pe.aplica_id as aplicaId , pe.porcentaje_bono as porcentajeBono , pe.estado_registro  as estado\n" +
            "from sch_convocatoria.tbl_bonificacion_detalle pe where pe.bonificacion_id = :bonificacion and pe.estado_registro = '1' order by pe.bonificacion_detalle_id asc ";
    /*
    select tmd3.descripcion as nivelDetalle , tb.nivel_id , tb.id_bonificacion as bonificacionId , tb.tipo_bonificacion as tipoBonificacion , tmd.descripcion as nombretipoBonificacion , tb.titulo as titulo , tb.contenido as contenido ,
    tb.nombre_bonificacion as bonificacionNombre , tb.nivel as nivel , tb.aplica as aplica , tb.porcentaje_bono as porcentaje , tb.id_informe_detalle as informeDetalleId
    from sch_convocatoria.tbl_bonificacion tb
    join sch_convocatoria.tbl_mae_detalle tmd on tb.tipo_bonificacion = tmd.mae_detalle_id
    left join sch_convocatoria.tbl_mae_detalle tmd3 on tb.nivel_id = tmd3.mae_detalle_id
    left join sch_convocatoria.tbl_mae_detalle tmd2 on tb.aplica_id = tmd2.mae_detalle_id
     */

    /*
        ;
     */

    @Override
    public List<InformeDetalleDTO> listAllInformeDetalleDto(Long entidad, Long tipoInforme, String estado , String titulo , Long idInforme) {
        List<InformeDetalleDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_INFORME_DETALLE);

            if (!Util.isEmpty(entidad)) sql.append(" where tid.entidad_id  = :entidad ");
            if (!Util.isEmpty(tipoInforme)) sql.append(" and tid.tipo_info_id = :tipoInforme ");
            if (!Util.isEmpty(estado)) sql.append(" and tid.estado_registro = :estado ");
            if (!Util.isEmpty(titulo)) sql.append(" and upper(tid.titulo) like upper('%'||:titulo||'%') ");
            if (!Util.isEmpty(idInforme)) sql.append(" and tid.informe_detalle_id = :idInforme ");
            sql.append(" order by tid.informe_detalle_id desc ");

            if (!Util.isEmpty(entidad)) objectParam.put("entidad", entidad);
            if (!Util.isEmpty(tipoInforme)) objectParam.put("tipoInforme", tipoInforme);
            if (!Util.isEmpty(estado)) objectParam.put("estado", estado.trim());
            if (!Util.isEmpty(titulo)) objectParam.put("titulo", titulo.trim());
            if (!Util.isEmpty(idInforme)) objectParam.put("idInforme", idInforme);

            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(InformeDetalleDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }


    @Override
    public List<BonificacionDTO> listAllBonificacionDto(Long idInformeDetalle) {
        List<BonificacionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BONIFICACION);
            if (!Util.isEmpty(idInformeDetalle)) objectParam.put("idInformeDetalle", idInformeDetalle);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(BonificacionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<BonificacionDetalleDTO> listAllBonificacionDetalleDtos(Long bonificacion) {
        List<BonificacionDetalleDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_BONIFICACION_DETALLE);
            if (!Util.isEmpty(bonificacion)) objectParam.put("bonificacion", bonificacion);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(BonificacionDetalleDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }


    private static final String SQL_FIND_LIST_INFORME_DETALLE = "select tmd.cod_prog as codProg ,  tid.informe_detalle_id as informeDetalleId , tmd.descripcion as nombretipoInfo , tid.tipo_info_id as tipoInfoId , case when tid.ESTADO_REGISTRO = '1' then 'ACTIVO' else 'INACTIVO' end as  estadoRegistro," +
            " upper(tid.titulo) as  titulo, tid.entidad_id  as entidadId from sch_base.tbl_informe_detalle tid join sch_convocatoria.tbl_mae_detalle tmd on tid.tipo_info_id = tmd.mae_detalle_id \n" ;

	@Override
	public List<RespCriterioEvaluacion> listInformeDetalle(Long entidad, Long tipoInforme, String estado) {
		List<RespCriterioEvaluacion> list = new ArrayList<>();
		try {
			StringBuilder sql = new StringBuilder();
			Map<String, Object> objectParam = new HashMap<>();
			sql.append(SQL_FIND_LIST_INFORME_DETALLE);

			if (!Util.isEmpty(entidad))
				sql.append(" where tid.entidad_id  = :entidad ");
			if (!Util.isEmpty(tipoInforme))
				sql.append(" and tid.tipo_info_id = :tipoInforme ");
			if (!Util.isEmpty(estado))
				sql.append(" and tid.estado_registro = :estado ");
			sql.append(" order by tid.informe_detalle_id desc ");

			if (!Util.isEmpty(entidad))
				objectParam.put("entidad", entidad);
			if (!Util.isEmpty(tipoInforme))
				objectParam.put("tipoInforme", tipoInforme);
			if (!Util.isEmpty(estado))
				objectParam.put("estado", estado.trim());

			list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam,
					BeanPropertyRowMapper.newInstance(RespCriterioEvaluacion.class));
			return list;
		} catch (Exception e) {
			LogManager.getLogger(this.getClass()).error(e);
			return list;
		}
	}
}
