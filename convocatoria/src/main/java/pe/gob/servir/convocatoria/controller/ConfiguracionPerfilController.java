package pe.gob.servir.convocatoria.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarActualizarConfigPerfilDTO;
import pe.gob.servir.convocatoria.request.dto.CarreraFormacionAcademicaOtrosGradosDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilExperienciaLaboralDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormCarreraDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormEspecialDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormEspecificDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPesoDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPublInvestDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilesDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.ConfigPerfilService;

@RestController
@Tag(name = "Configuracion Perfil", description = "")
public class ConfiguracionPerfilController {

	@Autowired
	private ConfigPerfilService configPerfilService;

	@Autowired
	private HttpServletRequest httpServletRequest;
	
	@Operation(summary = "Listar configuracion carreras de formación académica", description = "Listar configuracion carreras de formación académica", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/configuracion/carreras/{perfilId}/{baseId}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })		
	public ResponseEntity<RespBase<RespObtieneLista<RConfPerfilFormCarreraDTO>>> listarConfigFormCarrera(@PathVariable String access,
			@PathVariable Long perfilId, @PathVariable Long baseId) {
		RespBase<RespObtieneLista<RConfPerfilFormCarreraDTO>> response = configPerfilService.listarConfigFormCarrera(perfilId, baseId);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Listar experiencia laboral por perfil", description = "Experiencia laboral por perfil", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/configuracion/experiencialLaboralByPerfil/{perfilId}/{baseId}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })		
	public ResponseEntity<RespBase<List<RConfPerfilExperienciaLaboralDTO>>> listarExperienciaLaboralByPerfil(@PathVariable String access,
			@PathVariable Long perfilId,@PathVariable Long baseId) {
		RespBase<List<RConfPerfilExperienciaLaboralDTO>> response = configPerfilService.listarExperienciaLaboralByPerfil(perfilId,baseId);
		return ResponseEntity.ok(response);
	}	
	
	@Operation(summary = "Listar publicacion e investigacion por perfil", description = "Listar publicacion e investigacion por perfil", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/configuracion/investigacion/{perfilId}/{baseId}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })		
	public ResponseEntity<RespBase<RConfPerfilPublInvestDTO>> listarPubicacionInvestByPerfil(@PathVariable String access,
			@PathVariable Long perfilId,@PathVariable Long baseId) {
		RespBase<RConfPerfilPublInvestDTO> response = configPerfilService.listarPubicacionInvestByPerfil(perfilId,baseId);
		return ResponseEntity.ok(response);
	}	
	
	@Operation(summary = "Listar configuracion especialidades de formación académica", description = "Listar configuracion especialidades de formación académica", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/configuracion/especialidad/{perfilId}/{baseId}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })		
	public ResponseEntity<RespBase<RespObtieneLista<RConfPerfilFormEspecialDTO>>> listarConfigFormEspecialidad(@PathVariable String access,
			@PathVariable Long perfilId, @PathVariable Long baseId) {
		RespBase<RespObtieneLista<RConfPerfilFormEspecialDTO>> response = configPerfilService.listarConfigFormEspecializacion(perfilId, baseId);
		return ResponseEntity.ok(response);
	}

	@SuppressWarnings("rawtypes")
	@Operation(summary = "Listar especificaciones de formación académica", description = "Listar especificaciones para la validacion curricular", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/configuracion/especificaciones/{perfilId}/{baseId}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })		
	public ResponseEntity<RespBase<RespObtieneLista<RConfPerfilFormEspecificDTO>>> listarConfigEspecificaciones(@PathVariable String access,
			@PathVariable Long perfilId, @PathVariable Long baseId) {
		RespBase<RespObtieneLista<RConfPerfilFormEspecificDTO>> response = configPerfilService.listarEspecificacionesFormacion(perfilId, baseId);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = "Guarda o actualiza la configuracion de un perfil", description = "Guarda o actualiza la configuracion de un perfil", tags = { "" }, security = {
			@SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@PostMapping(path = { Constantes.BASE_ENDPOINT + "/configuracionPerfil" },
			consumes = { MediaType.APPLICATION_JSON_VALUE },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RespBase<Object>> guardarActualizarConfigPerfil(@PathVariable String access,
														                  @Valid @RequestBody ReqBase<ReqGuardarActualizarConfigPerfilDTO> request) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute(Constantes.JWT);
		RespBase<Object> response = configPerfilService.guardarActualizarConfigPerfil(jwt, request);
		return ResponseEntity.ok(response);
	}
	
	@Operation(summary = "Obtener pesos por seccion", description = "Obtener pesos por seccion", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/configuracion/pesos/{perfilId}/{baseId}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })		
	public ResponseEntity<RespBase<RespObtieneLista<RConfPerfilPesoDTO>>> listarConfigPesos(@PathVariable String access,
			@PathVariable Long perfilId, @PathVariable Long baseId) {
		RespBase<RespObtieneLista<RConfPerfilPesoDTO>> response = configPerfilService.listarConfigPesos(perfilId, baseId);
		return ResponseEntity.ok(response);
	}
	
	@SuppressWarnings("rawtypes")
	@Operation(summary = "Obtener formato de configuracion de perfil ", description = "Obtener formato de configuracion de perfil", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = { 
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/configuracion/perfilFormato/{perfilId}/{baseId}"},
				produces = { MediaType.APPLICATION_JSON_VALUE })		
	public ResponseEntity<RespBase<RConfPerfilesDTO>> obtenerConfiguracionPerfil(@PathVariable String access,
			@PathVariable Long perfilId, @PathVariable Long baseId) {
		RespBase<RConfPerfilesDTO> response = configPerfilService.obtenerConfiguracionPerfil(perfilId, baseId);
		return ResponseEntity.ok(response);
	}
	

	@Operation(summary = "Eliminacion logica de configuracion de Perfil", description = "Eliminacion logica de configuracion de Perfil", tags = {
	"" }, security = { @SecurityRequirement(name = Constantes.BEARER_JWT) })
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })
	@PutMapping(path = { Constantes.BASE_ENDPOINT + "/configuracion/actualizaEstado/{perfilId}/{baseId}" }, produces = { MediaType.APPLICATION_JSON_VALUE })

	public ResponseEntity<RespBase<Object>> actualizaEstadoConfiguracion(@PathVariable String access,
			@PathVariable Long perfilId, @PathVariable Long baseId) {
		MyJsonWebToken jwt = (MyJsonWebToken) httpServletRequest.getAttribute("jwt");
		RespBase<Object> resp = configPerfilService.actualizaEstadoConfiguracion(jwt, perfilId, baseId);
		return ResponseEntity.ok(resp);

	}
	
	@Operation(summary = "Listar configuracion carreras de formación académica otros grados", description = "Listar configuracion carreras de formación académica", tags = { "" },
			security = { @SecurityRequirement(name = Constantes.BEARER_JWT)})
	@ApiResponses(value = {
			@ApiResponse(responseCode = Constantes.SERVER_200, description = Constantes.OPERACION_EXITOSA),
			@ApiResponse(responseCode = Constantes.SERVER_400, description = Constantes.ERROR_VALIDACION, content = {
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }),
			@ApiResponse(responseCode = Constantes.SERVER_500, description = Constantes.ERROR_INTERNO, content = {	
					@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RespBase.class)) }) })	
	@GetMapping(path = { Constantes.BASE_ENDPOINT+"/configuracion/carrerasOtrosGrados/{perfilId}/{baseId}"}, 
				produces = { MediaType.APPLICATION_JSON_VALUE })		
	public ResponseEntity<RespBase<List<CarreraFormacionAcademicaOtrosGradosDTO>>> listarConfigFormCarreraOtrosGrados(@PathVariable String access,
			@PathVariable Long perfilId,@PathVariable Long baseId) {
		RespBase<List<CarreraFormacionAcademicaOtrosGradosDTO>> response = configPerfilService.obtieneFormacionOtrosGrados(perfilId,baseId);
		return ResponseEntity.ok(response);
	}


}
