package pe.gob.servir.convocatoria.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.model.Organigrama;

@Getter
@Setter
public class RespOrganigrama {

	private List<Organigrama> listaOrganigrama;
}
