package pe.gob.servir.convocatoria.repository;

import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.gob.servir.convocatoria.model.Base;

import java.time.Instant;
import java.util.Optional;

@Repository
public interface BaseRepository extends JpaRepository<Base, Long>{

    @Query("SELECT  b.baseId FROM  Base b WHERE b.baseId = :baseId ")
    Optional<Base> findByidBase(@Param("baseId")Long baseId);


    @Modifying
    @Query("update Base b set b.usuarioModificacion = :userMod , b.fechaModificacion = :fechMod, " +
            " b.comentarioEtapa1 = :etapa1 ,  b.comentarioEtapa2 = :etapa2 ,  b.comentarioEtapa3 = :etapa3 ,"+
            " b.comentarioEtapa4 = :etapa4 ,  b.comentarioEtapa5 = :etapa5 ,  b.comentarioEtapa6 = :etapa6 ,"+
            " b.condicionEtapa1 = :cond1 ,    b.condicionEtapa2 = :cond2 ,    b.condicionEtapa3 = :cond3,"+
            " b.condicionEtapa4 = :cond4 ,    b.condicionEtapa5 = :cond5 ,    b.condicionEtapa6 = :cond6 , b.etapaId = :idEtapa  where b.baseId = :baseId "
    )
    int  updateTipoId(@Param(value = "userMod") String userMod , @Param(value = "fechMod") Instant fechMod,
                      @Param(value = "etapa1") String etapa1 ,  @Param(value = "etapa2") String etapa2 ,  @Param(value = "etapa3") String etapa3,
                      @Param(value = "etapa4") String etapa4 ,  @Param(value = "etapa5") String etapa5 ,  @Param(value = "etapa6") String etapa6,
                      @Param(value = "cond1") String cond1 ,    @Param(value = "cond2") String cond2 ,    @Param(value = "etapa3") String cond3,
                      @Param(value = "cond4") String cond4 ,    @Param(value = "cond5") String cond5 ,    @Param(value = "etapa6") String cond6 ,  @Param(value = "idEtapa") Long idEtapa , @Param(value = "baseId") Long baseId);

    @Modifying
    @Query("update Base b set b.usuarioModificacion = :userMod , b.fechaModificacion = :fechMod, " +
            " b.comentarioEtapa1 = :etapa1 ,  b.comentarioEtapa2 = :etapa2 ,  b.comentarioEtapa3 = :etapa3 ,"+
            " b.comentarioEtapa4 = :etapa4 ,  b.comentarioEtapa5 = :etapa5 ,  b.comentarioEtapa6 = :etapa6 ,"+
            " b.condicionEtapa1 = :cond1 ,    b.condicionEtapa2 = :cond2 ,    b.condicionEtapa3 = :cond3,"+
            " b.condicionEtapa4 = :cond4 ,    b.condicionEtapa5 = :cond5 ,    b.condicionEtapa6 = :cond6 , b.etapaId = :idEtapa,"+
            " b.coordinadorId=:coordinadorId, b.nombreCoordinador=:nombreCoordinador  where b.baseId = :baseId "
    )
    int  updateTipoIdAndCordinador(@Param(value = "userMod") String userMod , @Param(value = "fechMod") Instant fechMod,
                      @Param(value = "etapa1") String etapa1 ,  @Param(value = "etapa2") String etapa2 ,  @Param(value = "etapa3") String etapa3,
                      @Param(value = "etapa4") String etapa4 ,  @Param(value = "etapa5") String etapa5 ,  @Param(value = "etapa6") String etapa6,
                      @Param(value = "cond1") String cond1 ,    @Param(value = "cond2") String cond2 ,    @Param(value = "etapa3") String cond3,
                      @Param(value = "cond4") String cond4 ,    @Param(value = "cond5") String cond5 ,    @Param(value = "etapa6") String cond6 ,
                      @Param(value = "idEtapa") Long idEtapa , @Param(value = "baseId") Long baseId, @Param(value = "coordinadorId") Long coordinadorId,
                      @Param(value = "nombreCoordinador") String nombreCoordinador);

    @Modifying
    @Query("update Base b set b.usuarioModificacion = :userMod , b.fechaModificacion = :fechMod, " +
            " b.comentarioEtapa1 = :etapa1 ,  b.comentarioEtapa2 = :etapa2 ,  b.comentarioEtapa3 = :etapa3 ,"+
            " b.comentarioEtapa4 = :etapa4 ,  b.comentarioEtapa5 = :etapa5 ,  b.comentarioEtapa6 = :etapa6 ,"+
            " b.condicionEtapa1 = :cond1 ,    b.condicionEtapa2 = :cond2 ,    b.condicionEtapa3 = :cond3,"+
            " b.condicionEtapa4 = :cond4 ,    b.condicionEtapa5 = :cond5 ,    b.condicionEtapa6 = :cond6 , b.observacionEtapa6 = :obs6 , b.etapaId = :idEtapa  where b.baseId = :baseId "
    )
    int  updateTipoIdEtapa6(@Param(value = "userMod") String userMod , @Param(value = "fechMod") Instant fechMod,
                      @Param(value = "etapa1") String etapa1 ,  @Param(value = "etapa2") String etapa2 ,  @Param(value = "etapa3") String etapa3,
                      @Param(value = "etapa4") String etapa4 ,  @Param(value = "etapa5") String etapa5 ,  @Param(value = "etapa6") String etapa6,
                      @Param(value = "cond1") String cond1 ,    @Param(value = "cond2") String cond2 ,    @Param(value = "etapa3") String cond3,
                      @Param(value = "cond4") String cond4 ,    @Param(value = "cond5") String cond5 ,    @Param(value = "etapa6") String cond6 ,  @Param(value = "obs6") String obs6, @Param(value = "idEtapa") Long idEtapa , @Param(value = "baseId") Long baseId);


    /*CAMBIA DE POR PUBLICAR A PUBLICADO JOB*/
    @Modifying
    @Query("update Base b set b.etapaId = :idEtapa, b.codigoConvocatoria = :codigoConvocatoria, b.url = :url ,"
    		+ "anio = :anio, correlativo = :correlativo where b.baseId = :baseId")
    int  updateCambioEstadoProceso(@Param(value = "idEtapa") Long idEtapa , @Param(value = "baseId") Long baseId,
    		@Param(value = "codigoConvocatoria") String codigoConvocatoria, @Param(value = "url") String url,
    		@Param(value = "anio") String anio, @Param(value = "correlativo") int correlativo);
    
    @Modifying
    @Query("update Base b set b.url = :url  where b.baseId = :baseId")
    int  updateUrlBase(@Param(value = "url") String url , @Param(value = "baseId") Long baseId);


}
