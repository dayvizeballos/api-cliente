package pe.gob.servir.convocatoria.response;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.ConocimientoDTO;
import pe.gob.servir.convocatoria.request.dto.FormacionAcademicaDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilDTO;

import java.util.List;

@Getter
@Setter
public class RespCreaFormacionAcademica {

	private PerfilDTO perfil;
	
	private List<FormacionAcademicaDTO> lstFormacionAcademica;
	
	private List<ConocimientoDTO> lstConocimiento;
}
