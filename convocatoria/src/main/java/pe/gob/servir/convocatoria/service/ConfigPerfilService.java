package pe.gob.servir.convocatoria.service;

import java.util.List;

import pe.gob.servir.convocatoria.request.ReqBase;
import pe.gob.servir.convocatoria.request.ReqGuardarActualizarConfigPerfilDTO;
import pe.gob.servir.convocatoria.request.dto.CarreraFormacionAcademicaOtrosGradosDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilExperienciaLaboralDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormCarreraDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormEspecialDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilFormEspecificDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPesoDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilPublInvestDTO;
import pe.gob.servir.convocatoria.response.dto.RConfPerfilesDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;

public interface ConfigPerfilService {

	RespBase<RespObtieneLista<RConfPerfilFormEspecificDTO>> listarEspecificacionesFormacion(Long perfilId, Long baseId);

	RespBase<RespObtieneLista<RConfPerfilFormCarreraDTO>> listarConfigFormCarrera(Long perfilId, Long baseId);

	RespBase<RespObtieneLista<RConfPerfilFormEspecialDTO>> listarConfigFormEspecializacion(Long perfilId, Long baseId);

	RespBase<Object> guardarActualizarConfigPerfil(MyJsonWebToken token,
			ReqBase<ReqGuardarActualizarConfigPerfilDTO> request);

	RespBase<RespObtieneLista> listarConfigFormacionAcademica(Long perfilId);

	RespBase<List<RConfPerfilExperienciaLaboralDTO>> listarExperienciaLaboralByPerfil(Long perfilId, Long baseId);
	
	RespBase<RConfPerfilPublInvestDTO> listarPubicacionInvestByPerfil(Long perfilId, Long baseId);

	RespBase<RespObtieneLista<RConfPerfilPesoDTO>> listarConfigPesos(Long perfilId, Long baseId);


	RespBase<List<CarreraFormacionAcademicaOtrosGradosDTO>> obtieneFormacionOtrosGrados(Long perfilId, Long baseId);

	RespBase<Object> actualizaEstadoConfiguracion(MyJsonWebToken token, Long perfilId, Long baseId);

	RespBase<RConfPerfilesDTO> obtenerConfiguracionPerfil(Long perfilId, Long baseId);


}
