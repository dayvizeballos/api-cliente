package pe.gob.servir.convocatoria.request;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.request.dto.BaseCronogramaDTO;
 

@Getter
@Setter
public class ReqActualizaBaseCronograma {

	private BaseCronogramaDTO basecronograma;
}
