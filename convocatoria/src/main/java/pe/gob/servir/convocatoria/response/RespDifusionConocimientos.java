package pe.gob.servir.convocatoria.response;

import lombok.Data;

@Data
public class RespDifusionConocimientos {
    private String idConocimiento;
    private String nombreConocimiento;
    private String horas;
    private String nivelDominio; //Identificador de Nivel de Dominio del Conocimiento (1:Basico 2:Intermedio 3:Avanzado)
}
