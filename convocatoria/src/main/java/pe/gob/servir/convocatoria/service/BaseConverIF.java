package pe.gob.servir.convocatoria.service;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.model.*;
import pe.gob.servir.convocatoria.request.ReqGuardarBaseEvaluacion;
import pe.gob.servir.convocatoria.request.ReqGuardarBonificacion;
import pe.gob.servir.convocatoria.request.ReqGuardarInformacionComp;
import pe.gob.servir.convocatoria.request.dto.*;
import pe.gob.servir.convocatoria.util.Util;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@FunctionalInterface
public interface BaseConverIF {
    public String main ();
    Function<DatosConcursoDTO , Base> converDtoConcursoBase = (DatosConcursoDTO p) -> {
        Base base = new Base();
        base.setRegimenId(p.getBaseRegimenId());
        base.setModalidadId(p.getBaseModalidadId());
        base.setTipoId(p.getBaseTipoId());
        base.setEntidadId(p.getBaseEntidadId());
        base.setGestorId(p.getBaseGestorId());
        base.setCoordinadorId(p.getBaseCoordinadorId());
        base.setNombreGestor(p.getNombreGestor());
        base.setNombreCoordinador(p.getNombreCoordinador());
        return base;
    };

    Function<DatosConcursoDTO , DatosConcurso> converDtoConcurso = (DatosConcursoDTO p ) -> {
        DatosConcurso concurso = new DatosConcurso();
        concurso.setDatoConcursoId(p.getDatoConcursoId());
        concurso.setNombre(p.getNombre());
        concurso.setObjetivo(p.getObjetivo());
        concurso.setOrganoResponsableId(p.getOrganoResponsableId());
        concurso.setUnidadOrganicaId(p.getUnidadOrganicaId());
        concurso.setOrganoEncargadoId(p.getOrganoEncargadoId());
        concurso.setCorreo(p.getCorreo());
        concurso.setNroVacantes(p.getNroVacantes());
        concurso.setInformeDetalleId(p.getInformeDetalleId());
        concurso.setTipoPracticaId(p.getTipoPracticaId());
        concurso.setInformeEspecificaId(p.getInformeEspecificaId());
        concurso.setTelefono(p.getTelefono());
        concurso.setAnexo(p.getAnexo());
        return concurso;
    };

    Function< DatosConcurso , DatosConcursoDTO> converDtoConcurso2 = (DatosConcurso p ) -> {
        DatosConcursoDTO concurso = new DatosConcursoDTO();
        concurso.setDatoConcursoId(p.getDatoConcursoId());
        concurso.setNombre(p.getNombre());
        concurso.setObjetivo(p.getObjetivo());
        concurso.setOrganoResponsableId(p.getOrganoResponsableId());
		concurso.setUnidadOrganicaId(p.getUnidadOrganicaId());
		concurso.setOrganoEncargadoId(p.getOrganoEncargadoId());
        concurso.setCorreo(p.getCorreo());
        concurso.setNroVacantes(p.getNroVacantes());
        concurso.setInformeDetalleId(p.getInformeDetalleId());
        return concurso;
    };
    

    Function <BasePerfilDTO, BasePerfil> convertDtoBasePerfil = (BasePerfilDTO p) -> {
    	BasePerfil basePerfil = new BasePerfil();
    	basePerfil.setPerfilId(p.getPerfilId());
    	basePerfil.setJornadaLaboral(p.getJornadaLaboral());
    	basePerfil.setVacante(p.getVacante());
    	basePerfil.setRemuneracion(p.getRemuneracion());
    	basePerfil.setIndContratoIndeterminado(p.getIndContratoIndeterminado());
    	basePerfil.setTiempoContrato(p.getTiempoContrato());
    	basePerfil.setCondicionTrabajoId(p.getCondicionTrabajoId());
    	basePerfil.setSedeId(p.getSedeId());
    	basePerfil.setSedeDireccion(p.getSedeDireccion());
    	basePerfil.setCondicionEsencial(p.getCondicionEsencial());
    	basePerfil.setObservaciones(p.getObservaciones());
    	basePerfil.setModalidadContratoId(p.getModalidadContratoId());
    	basePerfil.setDepaId(p.getDepaId());
    	basePerfil.setDescDepa(p.getDescDepa());
    	basePerfil.setProvId(p.getProvId());
    	basePerfil.setDescProv(p.getDescProv());
    	basePerfil.setDistId(p.getDistId());
    	basePerfil.setDescDist(p.getDescDist());
    	return basePerfil;
    	
    };
    
    Function<BaseHorarioDTO, BaseHorario> convertDtoBaseHorario = (BaseHorarioDTO p) -> {
    	BaseHorario baseHorario = new BaseHorario();
    	baseHorario.setBaseHorarioId(p.getBaseHorarioId());
    	baseHorario.setHoraIni(p.getHoraIni());
    	baseHorario.setHoraFin(p.getHoraFin());
    	baseHorario.setFrecuenciaId(p.getFrecuenciaId());
    	return baseHorario;
    };
    
    Function< ReqGuardarBaseEvaluacion , BaseEvaluacion> converReqBaseEvaluacion = (ReqGuardarBaseEvaluacion r ) -> {
        BaseEvaluacion bEvaluacion = new BaseEvaluacion();
		bEvaluacion.setObservacion(r.getObservacion());
        return bEvaluacion;
    };
    
    Function< BaseEvaluacion , BaseEvaluacionDTO> converBaseToDto = (BaseEvaluacion b ) -> {
    	BaseEvaluacionDTO bDto = new BaseEvaluacionDTO();
    	bDto.setBaseEvaluacionId(b.getBaseEvaluacionId());
    	bDto.setObservacion(b.getObservacion());
		bDto.setBaseId(b.getBase().getBaseId());
        return bDto;
    };
    

    Function< ReqGuardarBonificacion , Bonificacion> converReqGuardarBonificacion = (ReqGuardarBonificacion r) -> {
    	Bonificacion bonificacion = new Bonificacion();
    	BonificacionDetalle bonificacionDetalle = null;
    	//bonificacion.setBonificacionId(r.getBonificacionId());
    	bonificacion.setTipoInfoId(r.getBonificacionDTO().getTipoInfoId());
    	bonificacion.setTipoBonificacion(r.getBonificacionDTO().getTipoBonificacion());    	
    	bonificacion.setTitulo(r.getBonificacionDTO().getTitulo());
    	bonificacion.setContenido(r.getBonificacionDTO().getContenido());
    	List<BonificacionDetalle> bonificacionDetalleList = new ArrayList<>(); 
    	List<BonificacionDetalleDTO> bonificacionDetalleDTOList = r.getBonificacionDTO().getBonificacionDetalleDTOList();
    	for (int i = 0; i < bonificacionDetalleDTOList.size() ; i++) {
    		bonificacionDetalle = new BonificacionDetalle();
    		bonificacionDetalle.setBonificacion(bonificacion);
    		bonificacionDetalle.setDescripcion(bonificacionDetalleDTOList.get(i).getDescripcion());    		
    		bonificacionDetalle.setNivelId(bonificacionDetalleDTOList.get(i).getNivelId());
    		bonificacionDetalle.setAplicaId(bonificacionDetalleDTOList.get(i).getAplicaId());
    		bonificacionDetalle.setPorcentajeBono(bonificacionDetalleDTOList.get(i).getPorcentajeBono());
    		bonificacionDetalle.setCampoSegIns(r.getBonificacionDTO().getUsuario(), Instant.now());
    		bonificacionDetalleList.add(bonificacionDetalle);
		}
    	bonificacion.setBonificacionDetalleList(bonificacionDetalleList);
        return bonificacion;
    };
    
    Function< ReqGuardarBonificacion , Bonificacion> converReqActualizarrBonificacion = (ReqGuardarBonificacion r) -> {
    	Bonificacion bonificacion = new Bonificacion();
    	BonificacionDetalle bonificacionDetalle = null;
    	//bonificacion.setBonificacionId(r.getBonificacionId());
    	bonificacion.setTipoInfoId(r.getBonificacionDTO().getTipoInfoId());
    	bonificacion.setTipoBonificacion(r.getBonificacionDTO().getTipoBonificacion());    	
    	bonificacion.setTitulo(r.getBonificacionDTO().getTitulo());
    	bonificacion.setContenido(r.getBonificacionDTO().getContenido());
    	List<BonificacionDetalle> bonificacionDetalleList = new ArrayList<>(); 
    	List<BonificacionDetalleDTO> bonificacionDetalleDTOList = r.getBonificacionDTO().getBonificacionDetalleDTOList();
    	for (int i = 0; i < bonificacionDetalleDTOList.size() ; i++) {
    		bonificacionDetalle = new BonificacionDetalle();
    		bonificacionDetalle.setBonificacion(bonificacion);
    		bonificacionDetalle.setDescripcion(bonificacionDetalleDTOList.get(i).getDescripcion());    		
    		bonificacionDetalle.setNivelId(bonificacionDetalleDTOList.get(i).getNivelId());
    		bonificacionDetalle.setAplicaId(bonificacionDetalleDTOList.get(i).getAplicaId());
    		bonificacionDetalle.setPorcentajeBono(bonificacionDetalleDTOList.get(i).getPorcentajeBono());    		
    		bonificacionDetalleList.add(bonificacionDetalle);
		}
    	bonificacion.setBonificacionDetalleList(bonificacionDetalleList);
        return bonificacion;
    };
	
    
    Function< BonificacionDetalleDTO , BonificacionDetalle> converReqNuevoDetalleBonificacion = (BonificacionDetalleDTO bdDTO) -> {
    	
		BonificacionDetalle bonificacionDetalle = new BonificacionDetalle();
		bonificacionDetalle.setAplicaId(bdDTO.getAplicaId());
		bonificacionDetalle.setDescripcion(bdDTO.getDescripcion());    		
		bonificacionDetalle.setNivelId(bdDTO.getNivelId());
		bonificacionDetalle.setAplicaId(bdDTO.getAplicaId());
		bonificacionDetalle.setPorcentajeBono(bdDTO.getPorcentajeBono());    		
		
        return bonificacionDetalle;
    };
    
    Function< Bonificacion , BonificacionDTO> converBonificacionToDto = (Bonificacion b ) -> {
        
    	BonificacionDTO bDto = new BonificacionDTO();
    	bDto.setBonificacionId(b.getBonificacionId());
    	bDto.setTipoInfoId(b.getTipoInfoId());
    	bDto.setTipoBonificacion(b.getTipoBonificacion());
    	bDto.setTitulo(b.getTitulo());
    	bDto.setContenido(b.getContenido());
    	bDto.setEstadoRegistro(b.getEstadoRegistro());
    	
    	BonificacionDetalleDTO bonificacionDetalle = null;
    	List<BonificacionDetalle> bonificacionDetalleList = b.getBonificacionDetalleList();
    	List<BonificacionDetalleDTO> bonificacionDetalleDTOList = new ArrayList<>();
    	for (int i = 0; i < bonificacionDetalleList.size() ; i++) {
    		bonificacionDetalle = new BonificacionDetalleDTO();
    		bonificacionDetalle.setBonificacionDetalleId(bonificacionDetalleList.get(i).getBonificacionDetalleId());
    		bonificacionDetalle.setBonificacionId(bonificacionDetalleList.get(i).getBonificacion().getBonificacionId());
    		bonificacionDetalle.setDescripcion(bonificacionDetalleList.get(i).getDescripcion());    		
    		bonificacionDetalle.setNivelId(bonificacionDetalleList.get(i).getNivelId());
    		bonificacionDetalle.setAplicaId(bonificacionDetalleList.get(i).getAplicaId());
    		bonificacionDetalle.setPorcentajeBono(bonificacionDetalleList.get(i).getPorcentajeBono());
    		bonificacionDetalle.setEstadoRegistro(bonificacionDetalleList.get(i).getEstadoRegistro());
    		bonificacionDetalleDTOList.add(bonificacionDetalle);
		}
    	bDto.setBonificacionDetalleDTOList(bonificacionDetalleDTOList);
        return bDto;
    };
        
    Function< ReqGuardarInformacionComp , InformeComplement> converReqGuardarInfToInfCompl = (ReqGuardarInformacionComp r ) -> {
        
    	InformeComplement complement = new InformeComplement();
    	complement.setBaseId(r.getBaseId());
        return complement;
    };


	Function< MovimientoDTO , Movimiento> converMovimiento = (MovimientoDTO p ) -> {
		Movimiento mov = new Movimiento();
		mov.setBaseId(p.getBaseId());
		mov.setMovimientoId(p.getMovimientoId());
		mov.setEntidadId(p.getEntidadId());
		mov.setEstadoOldId(p.getEstadoOldId());
		mov.setEstadoNewId(p.getEstadoNewId());
		return mov;
	};

	Function<Movimiento, MovimientoDTO > converMovimientoDto = (Movimiento p ) -> {
		MovimientoDTO mov = new MovimientoDTO();
		mov.setBaseId(p.getBaseId());
		mov.setMovimientoId(p.getMovimientoId());
		mov.setEntidadId(p.getEntidadId());
		mov.setEstadoOldId(p.getEstadoOldId());
		mov.setEstadoNewId(p.getEstadoNewId());
		mov.setUserCreacion(p.getUsuarioCreacion());
		mov.setFechaCreacion(Util.instantToString(p.getFechaCreacion()));
		return mov;
	};

	Function< ObservacionDTO , Observacion> converObservacion = (ObservacionDTO p ) -> {
		Observacion obs = new Observacion();
		obs.setBaseObsId(p.getBaseObsId());
		obs.setEtapa(p.getEtapa());
		obs.setObservacion(p.getObservacion());
		return obs;
	};

	Function< Observacion , ObservacionDTO > converObservacionDto = (Observacion p ) -> {
		ObservacionDTO obs = new ObservacionDTO();
		obs.setBaseObsId(p.getBaseObsId());
		obs.setEtapa(p.getEtapa());
		obs.setObservacion(p.getObservacion());
		return obs;
	};
	
}
