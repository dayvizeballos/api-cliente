package pe.gob.servir.convocatoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.MaeConocimiento;

import java.util.Collection;
import java.util.List;

@Repository
public interface MaeConocimientoRepository  extends JpaRepository<MaeConocimiento,Long> {
    List<MaeConocimiento> findByDescripcionInAndEstadoRegistro(Collection<String> ages , String estado);
}
