package pe.gob.servir.convocatoria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.gob.servir.convocatoria.audit.AuditableEntity;


@Entity
@Table(name = "tbl_configuracion_maestra", schema = "sch_convocatoria")
@Getter
@Setter
@ToString
public class ConfiguracionMaestra extends AuditEntity implements AuditableEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_configuracion_maestra")
	@SequenceGenerator(name = "seq_configuracion_maestra", sequenceName = "seq_configuracion_maestra", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "configuracion_maestra_id")
	private Long configuracionMaestraId;
	
	@Column(name = "entidad_id")
	private Long entidadId;
	
	@Column(name = "mae_cabecera_id")
	private Long maeCabeceraId;
	
	@Column(name = "mae_detalle_id")
	private Long maeDetalleId;
	
	@Column(name = "mae_detalle_entidad_id")
	private Long maeDetalleEntidadId;
	
	@Column(name = "orden")
	private Long orden;	
	
}
