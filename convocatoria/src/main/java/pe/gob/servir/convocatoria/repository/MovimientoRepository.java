package pe.gob.servir.convocatoria.repository;

import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.Movimiento;

import java.util.Optional;

@Repository
public interface MovimientoRepository extends JpaRepository<Movimiento , Long> {

}
