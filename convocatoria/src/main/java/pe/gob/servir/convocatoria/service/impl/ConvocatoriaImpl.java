package pe.gob.servir.convocatoria.service.impl;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;

import org.apache.commons.collections.CollectionUtils;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.gob.servir.convocatoria.common.Constantes;
import pe.gob.servir.convocatoria.common.EstadoRegistro;
import pe.gob.servir.convocatoria.common.VariablesSistema;
import pe.gob.servir.convocatoria.feign.client.EntidadClient;
import pe.gob.servir.convocatoria.feign.client.MaestraApiClient;
import pe.gob.servir.convocatoria.feign.client.PostulanteClient;
import pe.gob.servir.convocatoria.feign.client.SeleccionClient;
import pe.gob.servir.convocatoria.model.Base;
import pe.gob.servir.convocatoria.model.BaseCronograma;
import pe.gob.servir.convocatoria.model.BaseCronogramaActividad;
import pe.gob.servir.convocatoria.model.BasePerfil;
import pe.gob.servir.convocatoria.model.Convocatoria;
import pe.gob.servir.convocatoria.model.Entidad;
import pe.gob.servir.convocatoria.model.MaestraDetalle;
import pe.gob.servir.convocatoria.model.Pais;
import pe.gob.servir.convocatoria.model.Perfil;
import pe.gob.servir.convocatoria.model.PerfilGrupo;
import pe.gob.servir.convocatoria.model.Postulante;
import pe.gob.servir.convocatoria.repository.BaseCronoActividadRepository;
import pe.gob.servir.convocatoria.repository.BaseCronogramaRepository;
import pe.gob.servir.convocatoria.repository.BasePerfilRepository;
import pe.gob.servir.convocatoria.repository.BaseRepository;
import pe.gob.servir.convocatoria.repository.ConvocatoriaRepository;
import pe.gob.servir.convocatoria.repository.MaestraCabeceraRepository;
import pe.gob.servir.convocatoria.repository.MaestraDetalleRepository;
import pe.gob.servir.convocatoria.repository.PerfilGrupoRepository;
import pe.gob.servir.convocatoria.repository.PerfilRepository;
import pe.gob.servir.convocatoria.request.dto.CorreoDTO;
import pe.gob.servir.convocatoria.response.ConvocatoriaDTO;
import pe.gob.servir.convocatoria.response.ConvocatoriaInfoDTO;
import pe.gob.servir.convocatoria.response.CronogramaConvocatoriaDTO;
import pe.gob.servir.convocatoria.response.DatosPuestoDTO;
import pe.gob.servir.convocatoria.response.DatosReporteDTO;
import pe.gob.servir.convocatoria.response.RespBase;
import pe.gob.servir.convocatoria.response.RespConvocatoriaJob;
import pe.gob.servir.convocatoria.response.RespEntidad;
import pe.gob.servir.convocatoria.response.RespEtapasEvaluacion;
import pe.gob.servir.convocatoria.response.RespObtenerConvEtapa;
import pe.gob.servir.convocatoria.response.RespEtapasEvaluacion.DetalleEtapaEvaluacion;
import pe.gob.servir.convocatoria.response.RespObtenerConvocatorias;
import pe.gob.servir.convocatoria.response.RespObtieneLista;
import pe.gob.servir.convocatoria.response.RespToContratoConvenio;
import pe.gob.servir.convocatoria.response.dto.RespPerfilDTO;
import pe.gob.servir.convocatoria.response.dto.RespPerfilesConvocatoriaDTO;
import pe.gob.servir.convocatoria.security.MyJsonWebToken;
import pe.gob.servir.convocatoria.service.ConvocatoriaService;
import pe.gob.servir.convocatoria.service.MaestraDetalleService;
import pe.gob.servir.convocatoria.util.DateUtil;
import pe.gob.servir.convocatoria.util.ParametrosUtil;
import pe.gob.servir.convocatoria.util.Util;

@Service
public class ConvocatoriaImpl implements ConvocatoriaService {

    @Autowired
    ConvocatoriaRepository convocatoriaRepository;

    @Autowired
    BaseRepository baseRepository;

    @Autowired
    MaestraDetalleRepository maestraDetalleRepository;

    @Autowired
    PerfilRepository perfilRepository;

    @Autowired
    BasePerfilRepository basePerfilRepository;

    @Autowired
    PostulanteClient postulanteClient;

    @Autowired
    SeleccionClient seleccionClient;

    @Autowired
    PerfilGrupoRepository perfilGrupoRepository;

    @Autowired
    MaestraApiClient maestraApiClient;

    @Autowired
    BaseCronogramaRepository baseCronogramaRepository;

    @Autowired
    EntidadClient entidadClient;
    
    @Autowired
    MaestraDetalleService maestraDetalleService;
    
    @Autowired
    BaseCronoActividadRepository baseCronoActividadRepository;
    
    @Autowired
	MaestraCabeceraRepository maestraCabeceraRepository;
    
    @Autowired
    VariablesSistema variableSistema;
    

    public static final String DD_MM_YYYY = "dd/MM/yyyy";
    public static final String YYYY_MM_DD = "yyyy-mm-dd";
    int correlativo = 0;
    int correlativoProx = 0;

    public RespBase<RespObtieneLista<ConvocatoriaDTO>> findConvocatorias(Long entidadId, Long regimen, Long etapa,
                                                                         Date fechaIni, Date fechaFin, Long estado, int page, int size) {
        RespObtieneLista<ConvocatoriaDTO> respPayload = new RespObtieneLista<>();
        Pageable pageable = PageRequest.of(page, size);

        Page<Convocatoria> convocatoria = convocatoriaRepository.findAll((root, cq, cb) -> {
            Predicate p = cb.conjunction();
            Join<Convocatoria, MaestraDetalle> etapaJ = root.join("etapa");
            Join<Convocatoria, Base> baseJ = root.join("base");
            Join<Convocatoria, MaestraDetalle> estadoJ = root.join("estadoConvocatoria");

            if (!Util.isEmpty(entidadId)) {
                p = cb.and(p, cb.equal(baseJ.get("entidadId"), entidadId));
            }

            if (!Util.isEmpty(regimen)) {
                p = cb.and(p, cb.equal(baseJ.get("regimenId"), regimen));
            }

            if (!Util.isEmpty(etapa)) {
                p = cb.and(p, cb.equal(etapaJ.get("maeDetalleId"), etapa));
            }

            if (!Util.isEmpty(estado)) {
                p = cb.and(p, cb.equal(estadoJ.get("maeDetalleId"), estado));
            }

            if (!Util.isEmpty(fechaIni) && !Util.isEmpty(fechaFin)) {
                p = cb.and(p, cb.between(
                        cb.function("to_char", String.class, root.get("fechaPublicacion"), cb.literal(DD_MM_YYYY)),
                        DateUtil.fmtDt(fechaIni), DateUtil.fmtDt(fechaFin)));
            }

            cq.orderBy(cb.desc(root.get("convocatoriaId")));
            return p;
        }, pageable);

        respPayload.setCount(convocatoria.getContent().size());
        respPayload.setTotal(convocatoria.getTotalElements());
        respPayload.setItems(settConvocatoriaDto(convocatoria.getContent()));
        return new RespBase<RespObtieneLista<ConvocatoriaDTO>>().ok(respPayload);

    }

    public static Criterion alwaysTrue() {
        return Restrictions.sqlRestriction("1=1");
    }

    /**
     * SETT DTO DE CONVOCATORIA
     *
     * @param ls
     * @return
     */
    private List<ConvocatoriaDTO> settConvocatoriaDto(List<Convocatoria> ls) {
        return ls.stream()
                .filter(convocatoria -> !convocatoria.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
                .map(convocatoria -> {
                    ConvocatoriaDTO dto = new ConvocatoriaDTO();
                    dto.setConvocatoriaId(convocatoria.getConvocatoriaId());
                    
                    String codigoConvocatoria = "-";
                    if(!Util.isEmpty(convocatoria.getCodigoConvocatoria())) {
                    	codigoConvocatoria = convocatoria.getCodigoConvocatoria();
                    }
                    
                    dto.setCodConvAndCodRegimen(codigoConvocatoria + "|"
                            + convocatoria.getRegimen().getDescripcionCorta());
                    dto.setFechaPublicacion(DateUtil.fmtDt(convocatoria.getFechaPublicacion()));
                    // dto.setVacantes(new Long(convocatoria.getBasePerfil().getVacante()));
                    dto.setEtapa(convocatoria.getEtapa().getDescripcion());
                    
                    String postulantes = "0";
                    if(!Util.isEmpty(convocatoria.getPostulantes())) {
                    	postulantes = convocatoria.getPostulantes();
                    }
                    
                    String califican = "0";
                    if(!Util.isEmpty(convocatoria.getCalifican())) {
                    	postulantes = convocatoria.getCalifican();
                    }
                    
                    dto.setPostulantesAndCalifican(postulantes + " | " + califican);
                    dto.setGestor(convocatoria.getBase().getNombreGestor());
                    dto.setCoordinador(convocatoria.getBase().getNombreCoordinador());
                    dto.setEstadoConvocatoria(convocatoria.getEstadoConvocatoria().getDescripcion());
                    dto.setBaseId(convocatoria.getBase().getBaseId());
                    dto.setEtapaId(convocatoria.getEtapa().getMaeDetalleId());
                    dto.setCodProgEtapa(convocatoria.getEtapa().getCodProg());
                    dto.setEstadoConvocatoriaId(convocatoria.getEstadoConvocatoria().getMaeDetalleId());
                    dto.setCodProgEstado(convocatoria.getEstadoConvocatoria().getCodProg());
                    Optional<MaestraDetalle> maestraDetalle = maestraDetalleRepository.findById(convocatoria.getBase().getRegimenId());
                    maestraDetalle.ifPresent(detalle -> {
                        dto.setCodProRegimen(detalle.getCodProg());
                        dto.setIdRegimen(detalle.getMaeDetalleId());
                    });
                    
                    List<BasePerfil> basePerfilList = basePerfilRepository.findbyBaseId(convocatoria.getBase());
                    int vacantes = 0;
                    for (BasePerfil basePerfil : basePerfilList) {
                    	vacantes = vacantes + basePerfil.getVacante();
                    }
                    dto.setVacantes (new Long (vacantes));

                    return dto;
                }).collect(Collectors.toList());
    }

    @Override
    public RespBase<RespObtenerConvocatorias> listaConvocatoria(Date fechaDia) {

        RespObtenerConvocatorias respPayload = new RespObtenerConvocatorias();

        List<Convocatoria> lstConvocatoria = convocatoriaRepository.findAll((root, cq, cb) -> {
            Predicate p = cb.conjunction();
            Join<Convocatoria, MaestraDetalle> estadoJ = root.join("estadoConvocatoria");

            if (!Util.isEmpty(fechaDia)) {
                p = cb.and(p, cb.equal(
                        cb.function("to_char", String.class, root.get("fechaPublicacion"), cb.literal(DD_MM_YYYY)),
                        DateUtil.fmtDt(fechaDia)));
                p = cb.and(p, cb.equal(estadoJ.get("codProg"), Constantes.COD_ESTADO_GENERADO));
            }
            return p;
        });

        respPayload.setConvocatorias(this.setConvocatoriaJob(lstConvocatoria));

        return new RespBase<RespObtenerConvocatorias>().ok(respPayload);

    }

    private List<RespConvocatoriaJob> setConvocatoriaJob(List<Convocatoria> ls) {
        return ls.stream()
                .filter(convocatoria -> !convocatoria.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
                .map(convocatoria -> {
                    RespConvocatoriaJob c = new RespConvocatoriaJob();
                    c.setBaseId(convocatoria.getBase().getBaseId());
                    c.setConvocatoriaId(convocatoria.getConvocatoriaId());
                    return c;
                }).collect(Collectors.toList());
    }

    @Override
    public void registrarConvocatoria(Long baseId, MyJsonWebToken token) {
        Base base = new Base();
        MaestraDetalle regimen = new MaestraDetalle();
        MaestraDetalle estado = new MaestraDetalle();
        MaestraDetalle etapaProceso = new MaestraDetalle();
        String sigaEntidad = null;
        List<MaestraDetalle> estadoConvocatoria = maestraDetalleRepository
                .findDetalleByCodProg(Constantes.COD_TABLA_EST_CONVOCATORIA, Constantes.COD_ESTADO_GENERADO);

        estado = estadoConvocatoria.get(0);

        List<MaestraDetalle> etapaProcesoLst = maestraDetalleRepository
                .findDetalleByCodProg(Constantes.COD_TABLA_ESTADO_CRONOGRAMA, Constantes.COD_EST_DIFUSION);

        etapaProceso = etapaProcesoLst.get(0);

        LocalDate date = LocalDate.now();

        Optional<Base> findBase = baseRepository.findById(baseId);
        if (findBase.isPresent()) {
            base = findBase.get();
        }

        Optional<MaestraDetalle> findRegimen = maestraDetalleRepository.findById(base.getRegimenId());
        if (findRegimen.isPresent()) {
            regimen = findRegimen.get();
        }

        RespBase<RespEntidad> response = entidadClient.obtieneEntidad(base.getEntidadId());
        if (response.getStatus().getSuccess()) {
            if (response.getPayload() != null || !response.getPayload().getEntidad().isEmpty()) {
                sigaEntidad = response.getPayload().getEntidad().get(0).getSigla();
            }
        }

        Convocatoria convocatoria = new Convocatoria();
        convocatoria.setEtapa(etapaProceso);
        convocatoria.setAnio(Long.valueOf(date.getYear()));
        convocatoria.setEntidadId(base.getEntidadId());
        convocatoria.setRegimen(regimen);
        convocatoria.setEstadoConvocatoria(estado);
        convocatoria.setIndiArchivo(Constantes.IND_FLAG_0);
        convocatoria.setIndiNotificacion(Constantes.IND_FLAG_0);
        convocatoria.setFechaPublicacion(base.getFechaPublicacion());
        convocatoria.setSiglaEntidad(sigaEntidad);
        convocatoria.setCampoSegIns(token.getUsuario().getUsuario(), Instant.now());
        convocatoria.setBase(base);

        convocatoriaRepository.save(convocatoria);

    }

    @Override
    public RespBase<RespToContratoConvenio> findDatosToContratosToConvenios(Long base, Long postulante, Long perfil,
                                                                            Long entidad) {
        Postulante resPostulante = null;
        Entidad respEntidad = null;
        BasePerfil basePerfil = null;
        Perfil perfil1 = null;
        RespToContratoConvenio respPayload = new RespToContratoConvenio();
        ObjectMapper objectMapper = new ObjectMapper();
        Base b = new Base();
        b.setBaseId(base);

        BasePerfil bp = new BasePerfil();
        bp.setBase(b);
        bp.setPerfilId(perfil);

        Example<BasePerfil> basePerfilExample = Example.of(bp);
        Optional<Perfil> perfilOptional = perfilRepository.findById(perfil);
        if (perfilOptional.isPresent()) {
            perfil1 = perfilOptional.get();
        }

        List<BasePerfil> lstBasePerfils = basePerfilRepository.findAll(basePerfilExample);

        if (!lstBasePerfils.isEmpty()) {
            basePerfil = lstBasePerfils.get(0);
        }

        RespBase<Postulante> respPost = seleccionClient.obtienePostulante(postulante);

        if (respPost.getStatus().getSuccess()) {
            resPostulante = objectMapper.convertValue(respPost.getPayload(), Postulante.class);

        }

        RespBase<RespEntidad> respEnt = entidadClient.obtieneEntidad(entidad);
        if (respEnt.getStatus().getSuccess()) {
            if (!Objects.requireNonNull(respEnt.getPayload()).getEntidad().isEmpty()) {
                respEntidad = objectMapper.convertValue(respEnt.getPayload().getEntidad().get(0), Entidad.class);

            }

        }

        RespBase<RespToContratoConvenio> resPayload = new RespBase<>();
        if (perfil1 != null && basePerfil != null && resPostulante != null && respEntidad != null) {
            resPayload.setPayload(settRespToContratoConvenio(perfil1, basePerfil, resPostulante, respEntidad));
        }

        return resPayload;
    }

    private RespToContratoConvenio settRespToContratoConvenio(Perfil perfil, BasePerfil basePerfil,
                                                              Postulante postulante, Entidad entidad) {

        RespToContratoConvenio resp = new RespToContratoConvenio();
        Optional<PerfilGrupo> familia = Optional.empty();
        Optional<PerfilGrupo> rol = Optional.empty();
        Optional<MaestraDetalle> nivelCategoria = Optional.empty();
        /* CONVOCATORIA */
        resp.setNumero(null);
        resp.setAnio(null);
        resp.setNombrePuesto(perfil.getNombrePuesto());
        if (perfil.getFamiliaPuestoId() != null) {
            familia = perfilGrupoRepository.findById(perfil.getFamiliaPuestoId());
        }

        if (perfil.getRolId() != null) {
            rol = perfilGrupoRepository.findById(perfil.getRolId());
        }

        if (perfil.getNivelCategoriaId() != null) {
            nivelCategoria = maestraDetalleRepository.findById(perfil.getNivelCategoriaId());
        }

        resp.setPerfilId(perfil.getPerfilId());
        resp.setBaseId(basePerfil.getBase().getBaseId());
        resp.setBasePerfilId(basePerfil.getBasePerfilId());

        Pais pais = null;

        familia.ifPresent(fam -> {
            resp.setFamilia(fam.getDescripcion());
            resp.setFamiliaId(perfil.getFamiliaPuestoId());
        });

        rol.ifPresent(ro -> {
            resp.setRol(ro.getDescripcion());
            resp.setRolId(perfil.getRolId());
        });

        resp.setNombreOrgano(perfil.getNombreOrgano());
        resp.setOrganoId(perfil.getOrganoId());
        resp.setNombreUniOrganica(perfil.getUnidadOrganica());
        resp.setUnidadOrganicaId(perfil.getUnidadOrganicaId());

        nivelCategoria.ifPresent(nivelCat -> {
            resp.setNivelCategoria(nivelCat.getDescripcion());
            resp.setNivelCategoriaId(perfil.getNivelCategoriaId());
        });

        resp.setSubNivelcat(perfil.getSubNivelsubCategoria());
        resp.setNroConPubMeritos(null);
        resp.setTipoTramiteId(null);
        resp.setTipoServicioId(null);
        resp.setPosCuadPuesto(null);
        resp.setLugarPrestacion(basePerfil.getDescDepa());
        resp.setSede(basePerfil.getSedeDireccion());
        resp.setCompeEconomica(basePerfil.getRemuneracion());
        resp.setJornalaboral(basePerfil.getJornadaLaboral());
        resp.setPeriodoPrueba(null);
        resp.setNroInforme(null);
        resp.setNormaAproProy(null);
        resp.setNroResolucion(null);
        resp.setFechaSubscripcion(null);
        resp.setFechaResolucion(null);
        resp.setFechaVinculacion(null);
        if (perfil.getTipoPracticaId()!= null){
            Optional<MaestraDetalle> tipoPractica = maestraDetalleRepository.findById(perfil.getTipoPracticaId());
            tipoPractica.ifPresent(tipo -> {
                resp.setTipoPractica(tipoPractica.get().getMaeDetalleId());
                resp.setDescTipoPractica(tipoPractica.get().getDescripcion());
            });
        }

        /* ENTIDAD */
        resp.setRazonSocial(entidad.getRazonSocial());
        resp.setEntidadId(entidad.getEntidadId());
        resp.setRuc(entidad.getNumeroDocumento());
        resp.setSiglas(entidad.getSigla());
        resp.setDireccion(entidad.getDireccion());
        resp.setResponsableOrh(null);
        resp.setPuestoResponsableOrh(null);
        resp.setResolResponOrh(null);
        resp.setNroDocResponsable(null);
        resp.setArticulo(null);
        resp.setNroNorma(null);

        /* POSTULANTE */
        resp.setPostulanteId(postulante.getIdPostulante());
        resp.setPostulanteSelId(postulante.getPostulanteSelId());
        resp.setNombres(postulante.getNombre());
        resp.setApellidos(postulante.getApellidos() == null ? null : postulante.getApellidos());
        resp.setDni(postulante.getNroDocumentos());
        resp.setTipoDoc(postulante.getTipoDoc());

        if (postulante.getIdPais() != null) {
            RespBase<RespObtieneLista<Pais>> respPais = maestraApiClient
                    .getPais(Integer.valueOf(postulante.getIdPais()), null);
            ObjectMapper objectMapper = new ObjectMapper();
            if (respPais.getStatus().getSuccess()) {
                if (!Objects.requireNonNull(respPais.getPayload()).getItems().isEmpty()) {
                    pais = objectMapper.convertValue(respPais.getPayload().getItems().get(0), Pais.class);

                }
                if (pais != null) {
                    if (pais.getNacionalidad() != null) {
                        resp.setNacionalidad(pais.getNacionalidad());
                    }
                }

            }
        }

        resp.setDomicilio(postulante.getDomicilio());
        resp.setFechaNacimiento(postulante.getFechaNacimiento());
        return resp;
    }

    @Override
    public RespBase<ConvocatoriaInfoDTO> obtenerConvocatoriaById(Long baseId) {
        ConvocatoriaInfoDTO info = null;
        Convocatoria convocatoria = new Convocatoria();
        Base base = new Base(baseId);
        convocatoria.setBase(base);
        convocatoria.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
        Example<Convocatoria> convocatoriaExample = Example.of(convocatoria);
        List<Convocatoria> listaConvocatoria = convocatoriaRepository.findAll(convocatoriaExample);
        if (listaConvocatoria != null && listaConvocatoria.size() > 0) {
            info = new ConvocatoriaInfoDTO(listaConvocatoria.get(0));
            BaseCronograma baseCronograma = new BaseCronograma();
            baseCronograma.setBaseId(base);
            baseCronograma.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
            Example<BaseCronograma> baseCronogramaExample = Example.of(baseCronograma);
            List<BaseCronograma> listaBaseCronograma = baseCronogramaRepository.findAll(baseCronogramaExample,
                    Sort.by("etapaId").ascending());
            List<CronogramaConvocatoriaDTO> listaCronograma = new ArrayList<>();
            for (BaseCronograma item : listaBaseCronograma) {
                listaCronograma.add(new CronogramaConvocatoriaDTO(item));
            }
            if (listaBaseCronograma != null && listaBaseCronograma.size() > 0) {
                info.setFechaInicioConvocatoria(ParametrosUtil.convertirDateToString(listaBaseCronograma.get(0).getPeriodoini(),
                        Constantes.FORMATO_FECHA_DD_MM_YYYY_HORA));
                info.setFechaFinConvocatoria(ParametrosUtil.convertirDateToString(listaBaseCronograma.get(listaBaseCronograma.size() - 1).getPeriodofin(),
                        Constantes.FORMATO_FECHA_DD_MM_YYYY_HORA));
            }
            info.setListaCronograma(listaCronograma);
        }
        return new RespBase<ConvocatoriaInfoDTO>().ok(info);
    }
    
    @Override
    public RespBase<RespObtieneLista<RespPerfilesConvocatoriaDTO>> listarPerfilesDetallePorConvocatoria(Long convocatoriaId){
    	RespObtieneLista<RespPerfilesConvocatoriaDTO> respPayload = new RespObtieneLista<>();
    	
    	Optional<Convocatoria> conv = convocatoriaRepository.findById(convocatoriaId);

		if (conv.isPresent()) {
			Convocatoria objConvocatoria = conv.get();

			BasePerfil filtro = new BasePerfil();
			filtro.setBase(new Base(objConvocatoria.getBase().getBaseId()));

			Example<BasePerfil> example = Example.of(filtro);
			List<BasePerfil> lstPerfilByConvocatoria = basePerfilRepository.findAll(example);

			Perfil filtPerfil = new Perfil();
			filtPerfil.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
			Example<Perfil> examplePerfil = Example.of(filtPerfil);

			List<Perfil> lstPerfiles = perfilRepository.findAll(examplePerfil);

			List<RespPerfilDTO> itemsDetalle = new ArrayList<>();
			lstPerfilByConvocatoria.forEach(d -> {
				itemsDetalle.add(new RespPerfilDTO(d.getPerfilId(), lstPerfiles));
			});

			respPayload.setCount(itemsDetalle.size());		
		}    	
    	return new RespBase<RespObtieneLista<RespPerfilesConvocatoriaDTO>>().ok(respPayload);
    }
    
	@Override
	public RespBase<RespEtapasEvaluacion> listarEtapasEvaluaciones(Long baseId) {
		RespEtapasEvaluacion response =new RespEtapasEvaluacion();		

		
		List<MaestraDetalle> lstMaeDet = maestraDetalleRepository.findDetalleByCodeCabecera("TIP_ETA_PRO");
		
		Map<Long,String> mapEtapaCodProg = lstMaeDet.stream().collect(Collectors.toMap(MaestraDetalle::getMaeDetalleId, MaestraDetalle::getDescripcion));
		
		Long etapaEvaluacionId = lstMaeDet.stream().filter(a -> "3".equals(a.getCodProg()) ).collect(Collectors.toList()).get(0).getMaeDetalleId();/** EVALUACION */
		Long etapaDifusionId = lstMaeDet.stream().filter(a -> "1".equals(a.getCodProg()) ).collect(Collectors.toList()).get(0).getMaeDetalleId();/** DIFUSION */
		Long etapaEleccionId = lstMaeDet.stream().filter(a -> "4".equals(a.getCodProg()) ).collect(Collectors.toList()).get(0).getMaeDetalleId();/** ELECCION */
		
		List<RespEtapasEvaluacion.DetalleEtapaEvaluacion> responseDto = new ArrayList<>();
		
		List<BaseCronograma> listBaseCrono = baseCronogramaRepository.listBaseCronograma(baseId);
		if (listBaseCrono != null && listBaseCrono.size() > 0  ) {
			
			DetalleEtapaEvaluacion resumenEva = new DetalleEtapaEvaluacion();
			for (BaseCronograma etapa : listBaseCrono) {
				resumenEva = new DetalleEtapaEvaluacion();
				resumenEva.setCronogramaId(etapa.getBasecronogramaId());
				resumenEva.setTipoEvaluacion(etapa.getEtapaId().getMaeDetalleId());
				resumenEva.setDescripcion(mapEtapaCodProg.get(etapa.getEtapaId().getMaeDetalleId()));//etapa.getDescripcion()
				resumenEva.setFlagcompletado(seEncuentraAntesDelRango( etapa.getPeriodofin()));
				resumenEva.setFechaInicio(Util.formatoFechaString(etapa.getPeriodoini(), DD_MM_YYYY) );
				resumenEva.setFechaFin(Util.formatoFechaString(etapa.getPeriodofin(), DD_MM_YYYY) );
				responseDto.add(resumenEva);
				
				if(etapaEvaluacionId == etapa.getEtapaId().getMaeDetalleId()) { // cuando solo sea TIPO ETAPA - EVALUACION
					List<BaseCronogramaActividad> listCronActivida = baseCronoActividadRepository.findAllActividadesXCronograma(etapa.getBasecronogramaId());
					if (listCronActivida != null && listCronActivida.size() > 0) {
						for (BaseCronogramaActividad cronActi : listCronActivida) {
							resumenEva = new DetalleEtapaEvaluacion();
							resumenEva.setTipoEvaluacion(null);
							resumenEva.setDescripcion(cronActi.getDescripcion());
							resumenEva.setFlagcompletado(seEncuentraAntesDelRango( cronActi.getFechaFin()));
							resumenEva.setFechaInicio(Util.formatoFechaString(cronActi.getFechaIni(), DD_MM_YYYY) );
							resumenEva.setFechaFin(Util.formatoFechaString(cronActi.getFechaFin(), DD_MM_YYYY) );
							responseDto.add(resumenEva);
						}
					}
				}
				
				if(etapaDifusionId == etapa.getEtapaId().getMaeDetalleId()) {
					response.setPeriodoInicio(Util.formatoFechaString(etapa.getPeriodoini(), DD_MM_YYYY));
				}
				if(etapaEleccionId == etapa.getEtapaId().getMaeDetalleId()) {
					response.setPeriodoFin(Util.formatoFechaString(etapa.getPeriodofin(), DD_MM_YYYY));
				}
			}
		}
		
		response.setListDetalleEtapaEvaluacion(responseDto);

		return new RespBase<RespEtapasEvaluacion>().ok(response);
	}
	
	
	public boolean seEncuentraAntesDelRango( Date periodoFin) {
		boolean result = false;
		
		Date fechaActual = ParametrosUtil.getFechaActualToDate();
		Date finFormat = ParametrosUtil.getOnlyDate(periodoFin);

		if ((fechaActual.before(finFormat) || fechaActual.compareTo(finFormat) == 0)) {
			result = Boolean.FALSE;
		} else {
			result = Boolean.TRUE;
		}
		
		return result;
	}
	
	/**
     * LISTAR CONVOCATORIAS PARA CAMBIO DE ETAPA
     *
     * @param fechaHoy
     * @return
     */
    public List<ConvocatoriaDTO> findConvocatoriaEtapa(String fechaHoy) {
    	//RespObtenerConvocatorias respPayload = new RespObtenerConvocatorias();
    	List<ConvocatoriaDTO> lstDto = null;
    	 List<BaseCronograma> lstBases = baseCronogramaRepository.findAll((root, cq, cb) -> {
             Predicate p = cb.conjunction();
             //Join<BaseCronograma, Base> baseJ = root.join("base");
             Join<BaseCronograma, Base> baseJ = root.join("baseId");
             p = cb.and(p, cb.lessThanOrEqualTo(
                     cb.function("to_char", String.class, root.get("periodoini"), cb.literal(YYYY_MM_DD)), //YYYY_MM_DD
                     fechaHoy));
             p = cb.and(p, cb.greaterThanOrEqualTo(
                             cb.function("to_char", String.class, root.get("periodofin"), cb.literal(YYYY_MM_DD)),
                             fechaHoy));
             //List<Long> lstB = Arrays.asList(new Long[]{70L, 63L});
             //p = cb.and(p, cb.equal(baseJ.get("baseId").in(lstB))); // prueba
             //p = cb.and(p, baseJ.get("baseId").in(lstB));
            
             cq.orderBy(cb.asc(root.get("baseId")),cb.asc(root.get("etapaId")));
             return p;
         });
    	
    	 
    	 lstDto =   lstBases.stream()
    	    	 .filter(baseCronograma -> !baseCronograma.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
    	    	 .map(cronograma -> {
    	    		 Convocatoria convocatoria = null;
    	    		 ConvocatoriaDTO dto = null;
    	    		 Optional<Convocatoria> convo = convocatoriaRepository.findByBase(cronograma.getBaseId());
    	    		 if(convo.isPresent()) {
    	         		convocatoria = convo.get();
    	         		dto = new ConvocatoriaDTO(); 
    	         		dto.setConvocatoriaId(convocatoria.getConvocatoriaId());
    	         		dto.setBaseId(cronograma.getBaseId().getBaseId());
    	         		dto.setEtapaId(convocatoria.getEtapa().getMaeDetalleId());
    	         		dto.setEtapaNewId(cronograma.getEtapaId().getMaeDetalleId());
    	         		dto.setFechaFinEleccion(cronograma.getPeriodofin());//por definir
    	         	}
    	    		 return dto;
    	    	 }).collect(Collectors.toList());
    	 
    	 return lstDto.stream()
    			 .filter(x -> x != null).
    			 collect(Collectors.toList());

    	 
    }
    
    /**
     * ACTUALIZAR ESTADO CULMINADO DE UNA CONVOCATORIA
     *
     * @param fechaHoy
     * @return
     */
    public void actualizaEstadoCulminado (String fechaHoy) {
    	List<ConvocatoriaDTO> lstDto = null;
    	List<MaestraDetalle> estado = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_EST_CONVOCATORIA);
    	
    	 List<BaseCronograma> lstBases = baseCronogramaRepository.findAll((root, cq, cb) -> {
    		Date fechaDate = null; 
    	    SimpleDateFormat formato = new SimpleDateFormat(YYYY_MM_DD);
    		fechaDate = DateUtil.parse(formato, fechaHoy);
    		 
             Predicate p = cb.conjunction();
             Join<BaseCronograma, MaestraDetalle> etapaJ = root.join("etapaId");
             p = cb.and(p, cb.equal(
                     cb.function("to_char", String.class, root.get("periodofin"), cb.literal(YYYY_MM_DD)),
                     DateUtil.fmtDt2(DateUtil.lessOrMoreDay(fechaDate, -1),YYYY_MM_DD)));
             p = cb.and(p, cb.equal(etapaJ.get("codProg"), Constantes.COD_EST_ELECCION));
             p = cb.and(p, cb.equal(root.get("estadoRegistro"), Constantes.ACTIVO));
             cq.orderBy(cb.asc(root.get("baseId")));
             return p;
         });
    	
    	 if (CollectionUtils.isNotEmpty(lstBases)) {
    		 lstBases.stream()
     		.forEach(m -> {
     			Convocatoria convocatoria = null;
     			//Etapa Nueva
				MaestraDetalle maeEstado = null;
				maeEstado = estado.stream().
		    			  filter(s -> s.getCodProg().equals(Constantes.COD_ESTADO_CULMINADA))
		    			  .findFirst().get();
     			
     			 Optional<Convocatoria> convo = convocatoriaRepository.findByBase(m.getBaseId());
         			if(convo.isPresent()) {
                 		convocatoria = convo.get();
                 		convocatoria.setEstadoConvocatoria(maeEstado);
                 		convocatoria.setCampoSegUpd(EstadoRegistro.ACTIVO.getCodigo(), "ADMIN", Instant.now());
                 		}
         			convocatoriaRepository.save(convocatoria);
     			});
     		}

    }
   
    /**
     * ACTUALIZAR ETAPA DE UNA CONVOCATORIA
     *
     * @param fechaHoy
     * @return
     */
    public void actualizaEstadoConvocatoria(List<ConvocatoriaDTO> lstConvocatoria,String fechaHoy) {
    	//Actualizar estado
    	 List<MaestraDetalle> etapa = maestraDetalleRepository.findDetalleByCodeCabecera(Constantes.COD_TABLA_ESTADO_CRONOGRAMA);
    	 
    	  Long etapaReclutamiento = etapa.stream().
    			  filter(s -> Constantes.COD_EST_RECLUTAMIENTO.equals(s.getCodProg()))
    			  .findFirst().get().getMaeDetalleId();
    	  
    	if (CollectionUtils.isNotEmpty(lstConvocatoria)) {
    		lstConvocatoria.stream()
    		.forEach(m -> {
    			Convocatoria convocatoria = null;
    			if(m.getEtapaId() != m.getEtapaNewId()) {
        			Optional<Convocatoria> otpConvocatoria =  convocatoriaRepository.findById(m.getConvocatoriaId());
        			if(otpConvocatoria.isPresent()) {
                		convocatoria = otpConvocatoria.get();
                		
                		//Fecha del dia
                		String fechaEtapa = Util.instantToString(Instant.now());
    			 		Date fechaDate = null;
    					SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    					fechaDate = DateUtil.parse(formato, fechaEtapa);
    					
    					//Etapa Nueva
    					MaestraDetalle maeEtapa = null;
    					maeEtapa = etapa.stream().
    			    			  filter(s -> m.getEtapaNewId().equals(s.getMaeDetalleId()))
    			    			  .findFirst().get();
    					
                		if(m.getEtapaNewId().equals(etapaReclutamiento)) {
        					
        					if(CollectionUtils.isEmpty(findExisteDifusion(fechaHoy, m.getBaseId()))) {
        						//Actualizar estado
        						convocatoria.setEtapa(maeEtapa);
        						convocatoria.setFechaCambioEtapa(fechaDate);
        					}
        				}else {
        					//Actualizar estado
        					convocatoria.setEtapa(maeEtapa);
    						convocatoria.setFechaCambioEtapa(fechaDate);
        				}
                	}
    				
        			convocatoriaRepository.save(convocatoria);
    			}
    		});
    	}
    }
    
    /**
     * OBTENER CONVOCATORIAS QUE ESTEN EN DIFUSION Y RECLUTAMIENTO
     *
     * @param fechaHoy
     * @return
     */
    public List<BaseCronograma> findExisteDifusion(String fechaHoy, Long baseId){
     List<BaseCronograma> lstBases = null;
   	  lstBases = baseCronogramaRepository.findAll((root, cq, cb) -> {
         Predicate p = cb.conjunction();
         Join<BaseCronograma, Base> baseJ = root.join("baseId");
         Join<BaseCronograma, MaestraDetalle> etapaJ = root.join("etapaId");
         //Join<BaseCronograma, Base> baseJ = root.join("base");
         if (!Util.isEmpty(baseId)) {
             p = cb.and(p, cb.equal(baseJ.get("baseId"), baseId));
         }

             p = cb.and(p, cb.equal(etapaJ.get("codProg"), Constantes.COD_EST_DIFUSION));

         if (!Util.isEmpty(fechaHoy)) {
	         p = cb.and(p, cb.greaterThanOrEqualTo(
	                         cb.function("to_char", String.class, root.get("periodofin"), cb.literal(YYYY_MM_DD)),
                            fechaHoy));
	         
         }
         return p;
     });
   	 
   	  return lstBases;
    }

    /**
     * ACTUALIZAR ETAPA CONVOCATORIAS Y ESTADO CULMINADO 
     *
     * @param fechaHoy
     * @return
     */
	@Override
	public RespBase<Object> actualizarConvocatoriaEtapa(String fechaHoy) {
		 RespBase<Object> response = new RespBase<>();
		 List<ConvocatoriaDTO> lstLista = new ArrayList<>();
	     lstLista = findConvocatoriaEtapa(fechaHoy);
	     try {
	    	 if (CollectionUtils.isNotEmpty(lstLista)) {
	    		 this.actualizaEstadoConvocatoria(lstLista, fechaHoy);
	    	 }
		     this.actualizaEstadoCulminado(fechaHoy);
		     
		     response = ParametrosUtil.setearResponse(response, Boolean.TRUE, "Se actualizo las etapas correctamente correctamente ");
		} catch (Exception e) {
			 response = ParametrosUtil.setearResponse(response, Boolean.FALSE, "Error al actualizar etapas de la convocatoria ");
		}
	    
	     return response;
	}

    /**
     * LISTAR CONVOCATORIAS A CAMBIAR DE ETAPA
     *
     * @param fechaHoy
     * @return
     */
	@Override
	public RespBase<RespObtenerConvEtapa> listarConvocatoriaCorreoEtapa(String fechaHoy) {
	
		RespObtenerConvEtapa  respPayload = new RespObtenerConvEtapa();
		List<CorreoDTO> lstConvoCorreo =  new ArrayList<>();
        List<Convocatoria> lstConvocatoria = convocatoriaRepository.findAll((root, cq, cb) -> {
            Predicate p = cb.conjunction();

            if (!Util.isEmpty(fechaHoy)) {
                p = cb.and(p, cb.equal(
                        cb.function("to_char", String.class, root.get("fechaCambioEtapa"), cb.literal(YYYY_MM_DD)),
                        fechaHoy));
                p = cb.and(p, cb.equal(root.get("estadoRegistro"), Constantes.ACTIVO));
                cq.orderBy(cb.asc(root.get("entidadId")));
            }
            return p;
        });
        
        correlativo = 0;
        
        lstConvoCorreo = lstConvocatoria.stream()
        .filter(convocatoria -> !convocatoria.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
        .map(convocatoria -> {
        	CorreoDTO c  = new CorreoDTO();
        	correlativo++;
            c.setBaseId(convocatoria.getBase().getBaseId());
            c.setConvocatoriaId(convocatoria.getConvocatoriaId());
            c.setCodigoConvocatoria(convocatoria.getCodigoConvocatoria());
            c.setEntidadId(convocatoria.getEntidadId());
            c.setGestor(convocatoria.getBase().getNombreGestor());
            c.setCoordinador(convocatoria.getBase().getNombreCoordinador());
            c.setNombreEntidad(convocatoria.getBase().getNombreEntidad());
            c.setRegimenId(convocatoria.getRegimen().getMaeDetalleId());
            c.setRegimen(convocatoria.getRegimen().getDescripcion());
            c.setEtapa(convocatoria.getEtapa().getDescripcion());
            c.setEtapaId(convocatoria.getEtapa().getMaeDetalleId());
            //c.setFechaCreacion(convocatoria.getFechaCreacion().toString());
            c.setFechaPublicacion(DateUtil.fmtDt(convocatoria.getFechaPublicacion()));
            c.setRnum(String.valueOf(correlativo));
            return c;
        }).collect(Collectors.toList());
		
        respPayload.setConvocatorias(lstConvoCorreo);

        return new RespBase<RespObtenerConvEtapa>().ok(respPayload);

	}

    /**
     * LISTAR CONVOCATORIAS PROXIMAS A CAMBIAR DE ETAPA
     *
     * @param fechaHoy
     * @return
     */
	@Override
	public RespBase<RespObtenerConvEtapa> listarConvocatoriaCorreoEtapaProx(String fechaHoy) {
		RespObtenerConvEtapa  respPayload = new RespObtenerConvEtapa();
		List<CorreoDTO> lstConvoCorreo =  new ArrayList<>();
		List<BaseCronograma> lstBases = null;
	   	  lstBases = baseCronogramaRepository.findAll((root, cq, cb) -> {
	   		 Date fechaDate; 
	         Predicate p = cb.conjunction();
	         //Join<BaseCronograma, Base> baseJ = root.join("baseId");
	         Join<BaseCronograma, MaestraDetalle> etapaJ = root.join("etapaId");
	         SimpleDateFormat formato = new SimpleDateFormat(YYYY_MM_DD);
	         fechaDate = DateUtil.parse(formato, fechaHoy);
	         
	         if (!Util.isEmpty(fechaHoy)) {
		         p = cb.and(p, cb.equal(
		                         cb.function("to_char", String.class, root.get("periodoini"), cb.literal(YYYY_MM_DD)), //YYYY_MM_DD
		                         DateUtil.fmtDt2(DateUtil.lessOrMoreDay(fechaDate, variableSistema.NOTIFICACION_DIAS_ETAPA),YYYY_MM_DD))); // DateUtil.fmtDt2(DateUtil.lessOrMoreDay(fechaDate, -1),YYYY_MM_DD))
		         p = cb.and(p, cb.notEqual(etapaJ.get("codProg"), Constantes.COD_EST_DIFUSION));
		         p = cb.and(p, cb.equal(root.get("estadoRegistro"), Constantes.ACTIVO));
		         
	         }
	         return p;
	     });
		
	   	correlativoProx = 0;  
	   	lstConvoCorreo =   lstBases.stream()
	    	    	 .filter(baseCronograma -> !baseCronograma.getEstadoRegistro().equalsIgnoreCase(Constantes.INACTIVO))
	    	    	 .map(cronograma -> {
	    	    		 Convocatoria convocatoria = null;
	    	    		 CorreoDTO dto = null;
	    	    		 Optional<Convocatoria> convo = convocatoriaRepository.findByBase(cronograma.getBaseId());
	    	    		 if(convo.isPresent()) {
	    	         		convocatoria = convo.get();
	    	         		dto = new CorreoDTO(); 
	    	         		correlativoProx++;
	    	         		 dto.setBaseId(convocatoria.getBase().getBaseId());
	    	                 dto.setConvocatoriaId(convocatoria.getConvocatoriaId());
	    	                 dto.setCodigoConvocatoria(convocatoria.getCodigoConvocatoria());
	    	                 dto.setEntidadId(convocatoria.getEntidadId());
	    	                 dto.setGestor(convocatoria.getBase().getNombreGestor());
	    	                 dto.setCoordinador(convocatoria.getBase().getNombreCoordinador());
	    	                 dto.setNombreEntidad(convocatoria.getBase().getNombreEntidad());
	    	                 dto.setRegimenId(convocatoria.getRegimen().getMaeDetalleId());
	    	                 dto.setRegimen(convocatoria.getRegimen().getDescripcion());
	    	                 dto.setEtapa(convocatoria.getEtapa().getDescripcion());
	    	                 dto.setEtapaId(convocatoria.getEtapa().getMaeDetalleId());
	    	                 dto.setEtapaProxima(cronograma.getEtapaId().getDescripcion());
	    	                 //c.setFechaCreacion(convocatoria.getFechaCreacion().toString());
	    	                 dto.setFechaPublicacion(DateUtil.fmtDt(convocatoria.getFechaPublicacion()));
	    	                 dto.setFechaEtapaProxima(DateUtil.fmtDt(cronograma.getPeriodoini()));
	    	                 dto.setRnum(String.valueOf(correlativoProx));
	    	         	}
	    	    		 return dto;
	    	    	 }).collect(Collectors.toList());
	    	 
	   	  
	    respPayload.setConvocatorias(lstConvoCorreo.stream()
	    		.filter(x -> x != null)
	    		.collect(Collectors.toList()));

        return new RespBase<RespObtenerConvEtapa>().ok(respPayload);
	}

	@Override
	public RespBase<DatosPuestoDTO> buscarDatosPuesto(Long baseId) {
		DatosPuestoDTO datosPuesto = null;
		BasePerfil basePerfil = new BasePerfil();
		basePerfil.setBase(new Base(baseId));
		basePerfil.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
		Example<BasePerfil> basePerfilExample = Example.of(basePerfil);
		List<BasePerfil> ltaBasePerfil = basePerfilRepository.findAll(basePerfilExample);
		for (BasePerfil item : ltaBasePerfil) {
			datosPuesto = new DatosPuestoDTO(item);
		}
		return new RespBase<DatosPuestoDTO>().ok(datosPuesto);
	}

	@Override
	public RespBase<DatosReporteDTO> buscarInfoReportePostulante(Long baseId) {
		DatosReporteDTO datosReporte = null;
		Optional<Base> baseOpt  = baseRepository.findById(baseId);
		if(baseOpt.isPresent()) {
			Base base = baseOpt.get();
			datosReporte = new DatosReporteDTO();
			datosReporte.setCondicion(buscarDetalleMaestra(base.getEtapaId()));
			datosReporte.setDesTipo(buscarDetalleMaestra(base.getTipoId()));
			datosReporte.setModalidadAcceso(buscarDetalleMaestra(base.getModalidadId()));
			datosReporte.setDescEtapa("Ev Conocimientos");
			datosReporte.setPeriodo("de 01/09/2021 hasta 15/10/2021");
		}
		return new RespBase<DatosReporteDTO>().ok(datosReporte);
	}
	
	public String buscarDetalleMaestra(Long id) {
		String descripcion = " ";
		Optional<MaestraDetalle> maestra = maestraDetalleRepository.findById(id);
		if(maestra.isPresent()) {
			descripcion = maestra.get().getDescripcion();
		}
		return descripcion;
	}
}