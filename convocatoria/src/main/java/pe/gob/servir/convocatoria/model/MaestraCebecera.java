package pe.gob.servir.convocatoria.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import pe.gob.servir.convocatoria.common.EstadoRegistro;

@Entity
@Table(name = "tbl_mae_cabecera", schema = "sch_convocatoria")
@Getter
@Setter
public class MaestraCebecera extends AuditEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mae_cabecera")
	@SequenceGenerator(name = "seq_mae_cabecera", sequenceName = "seq_mae_cabecera", schema = "sch_convocatoria", allocationSize = 1)
	@Column(name = "mae_cabecera_id")
	private Long maeCabeceraId;
	
	@Column(name = "codigo_cabecera")
	private String codigoCabecera;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "descripcion_corta")
	private String descripcionCorta;
	
	@Column(name = "solo_servir")
	private String soloServir;
	
	@Transient
	private List<MaestraDetalle> listMaestraDetalles;
	
	@Transient
	private List<MaestraDetalleEntidad> listaMaestraDetalleEntidads;
	
	public MaestraCebecera(String codigoCabecera) {
		this.codigoCabecera = codigoCabecera;
		this.setEstadoRegistro(EstadoRegistro.ACTIVO.getCodigo());
	}
	
	public MaestraCebecera() {
		
	}

}
