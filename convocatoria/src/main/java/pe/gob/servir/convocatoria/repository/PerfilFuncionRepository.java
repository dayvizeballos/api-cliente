package pe.gob.servir.convocatoria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import feign.Param;
import pe.gob.servir.convocatoria.model.FuncionDetalle;
import pe.gob.servir.convocatoria.model.PerfilFuncion;

@Repository
public interface PerfilFuncionRepository extends JpaRepository<PerfilFuncion, Long>{

	@Query("select fu.funcionDetalles from PerfilFuncion fu where fu.perfilFuncionId=:perfilFuncionId")
	 List<FuncionDetalle> listFuncDetalleByFuncion(@Param("perfilFuncionId") Long perfilFuncionId);
}
