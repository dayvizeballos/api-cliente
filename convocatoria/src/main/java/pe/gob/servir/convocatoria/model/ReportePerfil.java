package pe.gob.servir.convocatoria.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReportePerfil {

	private String nombrepuesto;
	private String organo; 
	private String unidadorganica;
	private String unidadfuncional;
	private String nivelorganizacional;
	private String puestoestructural;
	private String servidores;
	private String familia;
	private String rol;
	private String nivel;
	private String puestotipo;
	private String subnivel;
	private String codigopuesto;
	private String posicionespuesto;
	private String codigoposiciones;
	private String dependenciajerarquica;
	private String dependenciafuncional;
	private String  servidoresreporta;
	private String nroposicionescargo;
	private String posicionescargo;
	private String tipopracticas;
	private String condicion;
	private String mision;
	private List<ReportePerfilActividades> actividades;
	private String condAtipica;
	private String condperiodicidad;
	private String sustentoperiodicidad;
	private String condcoorinternas;
	private String gruposervidores;
	private String condcoorexternas;
	private String colegiatura;
	private String habprofesional;
	private List<ReportePerfilFormacion> formaciones;
	private List<ReportePerfilConocimiento> conocimientosTecnicos;
	private List<ReportePerfilConocimiento> conocimientosCursos;
	private List<ReportePerfilConocimiento> conocimientosProgramas;
	private List<ReportePerfilConocimiento> conocimientosOfimatica;
	private List<ReportePerfilConocimiento> conocimientosIdioma;
	private Integer anioexptotal;
	private Integer mesexptotal;
	private Integer anioexprequerida;
	private Integer mesexprequerida;
	private Integer anioexppublico;
	private Integer mesexppublico;
	private String aspectoscomplementarios;
	private String nivelminimo;
	private List<ReportePerfilExDetalle> expdetalleHabilidades;
	private List<ReportePerfilExDetalle> expdetalleRequisitos;
	private String logo;
	private Long regimenId;
	private Long entidadId;
	///
	private String jornadaTrabajo;
	private Double remuneracion;
	private Double remuneracionAnual;
	private String duracionContrato;
	private String modalidad;
	private String tipoPrestacion;
	private String sedeDireccion;
	 
}
