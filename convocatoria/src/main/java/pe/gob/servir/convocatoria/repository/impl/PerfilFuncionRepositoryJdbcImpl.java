package pe.gob.servir.convocatoria.repository.impl;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.gob.servir.convocatoria.repository.PerfilFuncionRepositoryJdbc;
import pe.gob.servir.convocatoria.request.dto.FuncionDetalleDTO;
import pe.gob.servir.convocatoria.request.dto.PerfilFuncionDTO;
import pe.gob.servir.convocatoria.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PerfilFuncionRepositoryJdbcImpl implements PerfilFuncionRepositoryJdbc {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PerfilFuncionRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }


    private static final String SQL_FIND_PERFIL_FUNCION = "select pf.lstservidorcivil as lstServidorCivil , pf.estado_registro as estado , pf.perfil_funcion_id as perfilFuncionId , pf.perfil_id as perfilId, pf.condicion_atipica as condicionAtipica , pf.periocidad_condicion_atipica_id as periocidadCondicionAtipicaId ,\n" +
            "pf.sustento_condicion_atipica as sustentoCondicionAtipica , pf.coordinacion_interna as coordinacionInterna  , pf.coordinacion_externa as coordinacionExterna, tmd.descripcion as codigoRegimen\n" +
            "from sch_convocatoria.tbl_perfil_funcion pf \n" +
            "join sch_convocatoria.tbl_perfil tp on pf.perfil_id = tp.perfil_id \n" +
            "join sch_convocatoria.tbl_mae_detalle tmd on tmd.mae_detalle_id = tp.regimen_laboral_id \n" +
            "where pf.perfil_id = :perfilId  and pf.estado_registro = '1' order by perfil_funcion_id desc";

    private static final String SQL_FIND_PERFIL_FUNCION_DETALLE = "select dt.descripcion , dt.orden as orden , dt.perfil_funcion_detalle_id as  funcionDetalleId , dt.estado_registro as estado from sch_convocatoria.tbl_perfil_funcion_detalle dt where perfil_funcion_id = :perfilFuncion and dt.estado_registro = '1' ";

    @Override
    public List<PerfilFuncionDTO> listPerfilFuncion(Long perfilId) {
        List<PerfilFuncionDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_PERFIL_FUNCION);
            if (!Util.isEmpty(perfilId)) objectParam.put("perfilId", perfilId);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(PerfilFuncionDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }
    }

    @Override
    public List<FuncionDetalleDTO> listPerfilFuncionDetalle(Long perfilFuncion) {
        List<FuncionDetalleDTO> list = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> objectParam = new HashMap<>();
            sql.append(SQL_FIND_PERFIL_FUNCION_DETALLE);
            if (!Util.isEmpty(perfilFuncion)) objectParam.put("perfilFuncion", perfilFuncion);
            list = this.namedParameterJdbcTemplate.query(sql.toString(), objectParam, BeanPropertyRowMapper.newInstance(FuncionDetalleDTO.class));
            return list;
        } catch (Exception e) {
            LogManager.getLogger(this.getClass()).error(e);
            return list;
        }

    }

}