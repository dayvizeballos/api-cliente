package pe.gob.servir.convocatoria.repository;

import pe.gob.servir.convocatoria.request.dto.MovimientoDTO;
import pe.gob.servir.convocatoria.request.dto.ObservacionDTO;

import java.util.List;

public interface MovimientoRepositoryJdbc {
    List<MovimientoDTO> listMovimientos (Long baseId , Long movId);
    List<ObservacionDTO> listObservacionDtos ( Long movId);
}
